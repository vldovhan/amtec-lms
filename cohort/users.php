<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cohort related management functions, this file needs to be included manually.
 *
 * @package    core_cohort
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../config.php');
require($CFG->dirroot.'/cohort/lib.php');
require_once($CFG->libdir.'/adminlib.php');

$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$page = optional_param('page', 0, PARAM_INT);

require_login();

$cohort = $DB->get_record('cohort', array('id'=>$id), '*', MUST_EXIST);
$context = context::instance_by_id($cohort->contextid, MUST_EXIST);

require_capability('moodle/cohort:manage', $context);

$PAGE->set_context($context);
$PAGE->set_url('/cohort/users.php', array('id'=>$id));
$PAGE->set_pagelayout('admin');

if ($returnurl) {
    $returnurl = new moodle_url($returnurl);
} else {
    $returnurl = new moodle_url('/cohort/index.php', array('contextid' => $cohort->contextid));
}

if (!empty($cohort->component)) {
    // We can not manually edit cohorts that were created by external systems, sorry.
    redirect($returnurl);
}

$members = cohort_get_cohort_members($cohort->id, $page, 25);
$count = '';
if ($members['allmembers'] > 0) {
    $count = ' ('.$members['totalmembers'].'/'.$members['allmembers'].')';
}

$params = array('page' => $page);
$params['id'] = $cohort->id;

$baseurl = new moodle_url('/cohort/users.php', $params);

if ($context->contextlevel == CONTEXT_COURSECAT) {
    $category = $DB->get_record('course_categories', array('id'=>$context->instanceid), '*', MUST_EXIST);
    navigation_node::override_active_url(new moodle_url('/cohort/index.php', array('contextid'=>$cohort->contextid)));
} else {
    navigation_node::override_active_url(new moodle_url('/cohort/index.php', array()));
}
$PAGE->navbar->add(get_string('assignedusers', 'local_manager'));

$PAGE->set_title($cohort->name.': '.get_string('assignedusers', 'local_manager'));
$PAGE->set_heading($COURSE->fullname);

echo $OUTPUT->header();
echo $OUTPUT->heading($cohort->name.': '.get_string('assignedusers', 'local_manager'));

// Output pagination bar.
echo $OUTPUT->paging_bar($members['totalmembers'], $page, 25, $baseurl);

$data = array();
$editcolumnisempty = true;
foreach($members['members'] as $user) {
    $line = array();
            
    $line[] = $OUTPUT->user_picture($user, array('width'=>'40', 'link'=>false)).fullname($user);
    $buttons = array();
    $editcolumnisempty = true;
    $line[] = implode(' ', $buttons);
    
    $data[] = $row = new html_table_row($line);
    if (!$cohort->visible) {
        $row->attributes['class'] = 'dimmed_text';
    }
}
$table = new html_table();
$table->head  = array(get_string('user_name', 'local_manager'));
$table->colclasses = array('leftalign name');

// Remove last column from $data.
foreach ($data as $row) {
    array_pop($row->cells);
}

$table->id = 'members';
$table->attributes['class'] = 'admintable generaltable';
$table->data  = $data;
echo html_writer::table($table);
echo $OUTPUT->paging_bar($members['totalmembers'], $page, 25, $baseurl);
echo $OUTPUT->footer();
