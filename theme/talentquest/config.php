<?php
/**
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
$THEME->name = 'talentquest';

$THEME->doctype = 'html5';
$THEME->parents = array('bootstrapbase');
$THEME->sheets = array('moodle', 'default', 'colorpicker', 'scrollbar');
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->enable_dock = false;
$THEME->editor_sheets = array();
$THEME->supportscssoptimisation = false;

$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_talentquest_process_css';
$THEME->parents_exclude_sheets = array('bootstrapbase'=>array('moodle'));

$THEME->plugins_exclude_sheets = array(
    'block' => array(
        'html',
    ),
);
/*$THEME->blockrtlmanipulations = array(
    'side-pre' => 'side-post',
    'side-post' => 'side-pre'
);*/


$THEME->layouts = array(
    // Most pages - if we encounter an unknown or a missing page type, this one is used.
    'base' => array(
        'file' => 'default.php',
        'regions' => array()
    ),
    'standard' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    // Course page
    'course' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    // Course page
    'coursecategory' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    'incourse' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    'frontpage' => array(
        'file' => 'default.php',
		'regions' => array('side-pre','side-post','side-menu'),
        'defaultregion' => 'side-post'
    ),
    'admin' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    'mydashboard' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    'mypublic' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    'login' => array(
        'file' => 'login.php',
        'regions' => array()
    ),
    // Pages that appear in pop-up windows - no navigation, no blocks, no header.
    'popup' => array(
        'file' => 'default.php',
        'regions' => array(),
        'options' => array('nofooter'=>true, 'nonavbar'=>true, 'noblocks'=>true, 'nocourseheaderfooter'=>true),
    ),
    // No blocks and minimal footer - used for legacy frame layouts only!
    'frametop' => array(
        'file' => 'default.php',
        'regions' => array(),
        'options' => array('nofooter', 'noblocks'=>true, 'nocoursefooter'=>true),
    ),
    // Embeded pages, like iframe embeded in moodleform
    'embedded' => array(
         'file' => 'embedded.php',
        'regions' => array(),
        'options' => array('nofooter'=>true, 'nonavbar'=>true, 'noblocks'=>true, 'nocourseheaderfooter'=>true),
    ),
    // Used during upgrade and install, and for the 'This site is undergoing maintenance' message.
    // This must not have any blocks, and it is good idea if it does not have links to
    // other places - for example there should not be a home link in the footer...
    'maintenance' => array(
        'file' => 'default.php',
        'regions' => array(),
        'options' => array('nofooter'=>true, 'nonavbar'=>true, 'noblocks'=>true, 'nocourseheaderfooter'=>true),
    ),
    // Should display the content and basic headers only.
    'print' => array(
        'file' => 'default.php',
        'regions' => array(),
        'options' => array('nofooter'=>true, 'nonavbar'=>false, 'noblocks'=>true, 'nocourseheaderfooter'=>true),
    ),
    // The pagelayout used when a redirection is occuring.
    'redirect' => array(
        'file' => 'embedded.php',
        'regions' => array(),
        'options' => array('nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nocourseheaderfooter'=>true),
    ),
    // The pagelayout used for reports.
    'report' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post'
    ),
    // The pagelayout used for safebrowser and securewindow.
    'secure' => array(
        'file' => 'default.php',
        'regions' => array('side-pre','side-post','side-menu','side-course'),
        'defaultregion' => 'side-post',
        'options' => array('nofooter'=>true, 'nonavbar'=>true, 'nocustommenu'=>true, 'nologinlinks'=>true, 'nocourseheaderfooter'=>true),
    ),
);