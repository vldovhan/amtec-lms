<?php
/**
 * @package   theme_talentquest
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>TalentQuest LMS</h2>
<p><img class=img-polaroid src="talentquest/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>Talentquest is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
</div></div>';

$string['configtitle'] = 'talentquest';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['menu'] = 'Resources menu';
$string['menudesc'] = 'Resources menu dropdown links';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.<br>
If the height of your logo is more than 75px add the following CSS rule to the Custom CSS box below.<br>
a.logo {height: 100px;} or whatever height in pixels the logo is.';

$string['pluginname'] = 'TalentQuest LMS';
$string['choosereadme'] = '<p>More is a theme that allows you to easily customise Moodle\'s look and feel directly from the web interface.</p>
<p>Visit the admin settings to change colors, add an image as a background, add your logo and more.</p>';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['backgroundhead'] = 'Head color';
$string['backgroundhead_desc'] = 'The head color to use for the background.';
$string['backgroundfoot'] = 'Footer colour';
$string['backgroundfoot_desc'] = 'The footer color to use for the background.';
$string['front_welcome_login_user'] = 'Welcome text';
$string['front_welcome_login_user_desc'] = 'The Welcome text to logged in user';
$string['front_welcome_no_login_user'] = 'Welcome text h1 login ';
$string['front_welcome_no_login_user_desc'] = 'The Welcome text h1 to not logged in user';
$string['front_welcome_full_no_login_user'] = 'Welcome text h3 login';
$string['front_welcome_full_no_login_user_desc'] = 'The Welcome text h3 to not logged in user';
$string['front_message_no_login_user'] = 'Message text to not logged in user';
$string['front_message_no_login_user_desc'] = 'The Welcome text on front page to not logged in user';
$string['no_course_info'] = 'No course information to show';
$string['not_found_course'] = 'No courses were found with the words ';
$string['course_progress'] = 'Course progress: ';
$string['grade'] = 'Avg grade: ';
$string['region-side-menu'] = 'Menu';
$string['region-side-course'] = 'Course';
$string['dashboard'] = 'Dashboard';
$string['login_welcome'] = 'Login welcome';
$string['login_welcome_desc'] = 'Welcome text on login page';
$string['courseadmin'] = 'Course administration';
$string['switchroleto'] = 'Switch role to...';
$string['restrictaccess'] = 'See Restrict Access to access';
$string['timezoneheader'] = 'Time Zones';
$string['enddateerror'] = 'End Date should not be less than Start Date';
$string['learningdashboard'] = 'Learning Dashboard';
$string['coursedetails'] = 'Course Details';
$string['details'] = '{$a} Details';
$string['settings'] = 'Settings';
$string['managecourses'] = 'Manage Courses';
$string['createcourse'] = 'Create New Course';
$string['managecategories'] = 'Manage Categories';
$string['managecourseoptions'] = 'Manage Custom Course Fields';
$string['import'] = 'Manage Backups';
$string['rolessetup'] = 'Roles Setup';
$string['profilefields'] = 'Profile Fields';
$string['editcourse'] = 'Edit Course';
$string['coursecompletion'] = 'Course Completion';
$string['resetcourse'] = 'Reset Course';
$string['backupcourse'] = 'Backup Course';
$string['restorecourse'] = 'Restore Course';
$string['enrollments'] = 'Enrollments';
$string['manageusers'] = 'Manage Users';
$string['coursetools'] = 'Course Tools';
$string['talentquest:viewallcourses'] = 'View all courses';
$string['termsofuse'] = 'Terms of use';
$string['privacypolicy'] = 'Privacy Policy';
$string['createuser'] = 'Create User';
$string['loggedinas'] = 'you are now logged in as';
$string['backtoown'] = 'Back to own profile';
$string['backtoown'] = 'Back to own profile';
$string['managebadges'] = 'Manage Badges';
$string['createbadge'] = 'Create Badge';
$string['managecoursecatalog'] = 'Manage Course Catalog';
    
