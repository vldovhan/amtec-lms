<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderers to align Moodle's HTML with that expected by Bootstrap
 *
 * @package    theme_talentquest
 * @copyright 2016 SEBALE, sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class theme_talentquest_core_renderer extends core_renderer {
    
    private $block_icons = array(
        'settings'=>'fa fa-cogs',
        'online_users'=>'fa fa-users',
        'courseadmin'=>'fa fa-wrench',
        'private_files'=>'fa fa-files-o'
    );
    
    public $mod_icons = array(
        'assign'=>'fa fa-check-square-o',
        'assignment'=>'fa fa-check-square-o',
        'chat'=>'fa fa-comment-o',
        'choice'=>'fa fa-question',
        'data'=>'fa fa-database',
        'data'=>'fa fa-database',
        'lti'=>'fa fa-external-link',
        'forum'=>'fa fa-comments-o',
        'glossary'=>'fa fa-newspaper-o',
        'lesson'=>'fa fa-puzzle-piece',
        'quiz'=>'fa fa-question-circle',
        'scorm'=>'fa fa-desktop',
        'survey'=>'fa fa-cubes',
        'wiki'=>'fa fa-archive',
        'workshop'=>'fa fa-users',
        'bigbluebuttonbn'=>'fa fa-video-camera',
        'facetoface'=>'fa fa-male',
        'adobeconnect'=>'fa fa-briefcase',
        'openmeetings'=>'fa fa-leaf',
        'tincanlaunch'=>'fa fa-bullhorn',
        'webexactivity'=>'fa fa-play-circle',
        'book'=>'fa fa-book',
        'resource'=>'fa fa-file-o',
        'folder'=>'fa fa-folder-open-o',
        'imscp'=>'fa fa-server',
        'label'=>'fa fa-tag',
        'page'=>'fa fa-file-text-o',
        'recordingsbn'=>'fa fa-file-video-o',
        'url'=>'fa fa-link',
        'feedback'=>'fa fa-commenting-o',
        'certificate'=>'fa fa-certificate',
        'scormcloud'=>'fa fa-cloud-upload',
    );

    /*public function block(block_contents $bc, $region) {
        global $PAGE;
        $bc = clone($bc); // Avoid messing up the object passed in.
        if (empty($bc->blockinstanceid) || !strip_tags($bc->title)) {
            $bc->collapsible = block_contents::NOT_HIDEABLE;
        }
        if (!empty($bc->blockinstanceid)) {
            $bc->attributes['data-instanceid'] = $bc->blockinstanceid;
        }
        $skiptitle = strip_tags($bc->title);
        if ($bc->blockinstanceid && !empty($skiptitle)) {
            $bc->attributes['aria-labelledby'] = 'instance-'.$bc->blockinstanceid.'-header';
        } else if (!empty($bc->arialabel)) {
            $bc->attributes['aria-label'] = $bc->arialabel;
        }
        if ($bc->dockable) {
            $bc->attributes['data-dockable'] = 1;
        }
        if ($bc->collapsible == block_contents::HIDDEN) {
            $bc->add_class('hidden');
        }
        if (!empty($bc->controls)) {
            $bc->add_class('block_with_controls');
        }


        if (empty($skiptitle)) {
            $output = '';
            $skipdest = '';
        } else {
            $output = html_writer::link('#sb-'.$bc->skipid, get_string('skipa', 'access', $skiptitle),
                array('class' => 'skip skip-block', 'id' => 'fsb-' . $bc->skipid));
            $skipdest = html_writer::span('', 'skip-block-to',
                array('id' => 'sb-' . $bc->skipid));
        }

        $output .= html_writer::start_tag('div', $bc->attributes);

        //if((strpos($PAGE->bodyclasses, 'dir-rtl') === false && $region == 'side-pre') || (strpos($PAGE->bodyclasses, 'dir-rtl') && $region == 'side-post'))
        if($region == 'side-pre')
            $output .= $this->block_header_side_pre($bc);
        elseif($region == 'side-course')
            $output .= $this->block_header_side_course($bc);
        else
            $output .= $this->block_header($bc);
        $output .= $this->block_content($bc);

        $output .= html_writer::end_tag('div');

        $output .= $this->block_annotation($bc);

        $output .= $skipdest;

        $this->init_block_hider_js($bc);
        return $output;
    }

    protected function block_header_side_pre(block_contents $bc) {
        $title = '';
        if ($bc->title) {
            $attributes = array();
            if ($bc->blockinstanceid) {
                $attributes['id'] = 'instance-'.$bc->blockinstanceid.'-header';
            }
            if(isset($this->block_icons[$bc->attributes['data-block']])){
                $icon = html_writer::tag('i', '', array('class'=>'heder-icon '.$this->block_icons[$bc->attributes['data-block']]));
            }else{
                //$words = explode(' ',clean_param($bc->title,PARAM_NOTAGS));
                //$letters = $words[0][0].((isset($words[1]))?$words[1][0]:'');
                //$icon = html_writer::tag('i', $letters, array('class'=>'heder-icon letters'));
                $icon = html_writer::tag('i', '', array('class'=>'heder-icon letters'));
            }
            $link = html_writer::link('#', $bc->title . $icon . html_writer::tag('i','',array('class'=>'fa fa-angle-left')),array('class'=>'clearfix'));
            $title = html_writer::tag('h2', $link, $attributes);
        }

        $blockid = null;
        if (isset($bc->attributes['id'])) {
            $blockid = $bc->attributes['id'];
        }
        $controlshtml = $this->block_controls($bc->controls, $blockid);

        $output = '';
        if ($title || $controlshtml) {
            $output .= html_writer::tag('div', html_writer::tag('div', $title . $controlshtml, array('class' => 'title')), array('class' => 'header'));
        }
        return $output;
    }

    protected function block_header_side_course(block_contents $bc) {
        $title = '';

        if ($bc->title) {
            $attributes = array();
            if ($bc->blockinstanceid) {
                $attributes['id'] = 'instance-'.$bc->blockinstanceid.'-header';
            }
            if(isset($this->block_icons[$bc->attributes['data-block']])){
                $icon = html_writer::tag('i', '', array('class'=>'heder-icon '.$this->block_icons[$bc->attributes['data-block']]));
            }else{
                //$words = explode(' ',clean_param($bc->title,PARAM_NOTAGS));
                //$letters = $words[0][0].((isset($words[1]))?$words[1][0]:'');
                //$icon = html_writer::tag('i', $letters, array('class'=>'heder-icon letters'));
                $icon = html_writer::tag('i', '', array('class'=>'heder-icon letters'));
            }
            $link = html_writer::link('#', $icon . html_writer::tag('span',$bc->title). html_writer::tag('i','',array('class'=>'fa fa-caret-down')),array('class'=>'clearfix'));
            $title = html_writer::tag('h2', $link, $attributes);
        }

        $blockid = null;
        if (isset($bc->attributes['id'])) {
            $blockid = $bc->attributes['id'];
        }
        $controlshtml = $this->block_controls($bc->controls, $blockid);

        $output = '';
        if ($title || $controlshtml) {
            $output .= html_writer::tag('div', html_writer::tag('div', $title . $controlshtml, array('class' => 'title')), array('class' => 'header'));
        }
        return $output;
    }*/
    
    /*
     * This renders the navbar.
     * Uses bootstrap compatible html.
     */
    public function navbar() {
        $items = $this->page->navbar->get_items();
        $itemcount = count($items);
        if ($itemcount === 0) {
            return '';
        }

        $htmlblocks = array();
        // Iterate the navarray and display each node
        $separator = get_separator();
        for ($i=0;$i < $itemcount;$i++) {
            $item = $items[$i];
            if ($item->type == global_navigation::TYPE_CATEGORY or $item->type == global_navigation::TYPE_SECTION) continue;
            $item->hideicon = true;
            if ($i===0) {
                $content = html_writer::tag('li', $this->render($item));
            } else {
                $content = html_writer::tag('li', $separator.$this->render($item));
            }
            $htmlblocks[] = $content;
        }

        //accessibility: heading for navbar list  (MDL-20446)
        $navbarcontent = html_writer::tag('span', get_string('pagepath'), array('class'=>'accesshide'));
        $navbarcontent .= html_writer::tag('nav', html_writer::tag('ul', join('', $htmlblocks)));
        // XHTML
        return $navbarcontent;
    }
    
    /**
     * Outputs a heading
     *
     * @param string $text The text of the heading
     * @param int $level The level of importance of the heading. Defaulting to 2
     * @param string $classes A space-separated list of CSS classes. Defaulting to null
     * @param string $id An optional ID
     * @return string the HTML to output.
     */
    public function heading($text, $level = 2, $classes = null, $id = null, $icon = '') {
        $level = (integer) $level;
        if ($level < 1 or $level > 6) {
            throw new coding_exception('Heading level must be an integer between 1 and 6.');
        }
        
        if ($level == 2){
            $classes = ($classes) ? $classes.' page-heading' : 'page-heading';
        }
        
        if (!empty($icon)){
            $classes = ($classes) ? $classes.' heading-with-icon' : 'heading-with-icon';
            $text = '<i class="heading-icon '.$icon.'"></i>'.$text;
        }
        
        return html_writer::tag('h' . $level, $text, array('id' => $id, 'class' => renderer_base::prepare_classes($classes)));
    }
    
    public function pix_icon($pix, $alt, $component='moodle', array $attributes = null) {
        
        if (isset($this->mod_icons[$component])){
            
            if (empty($attributes['class'])) {
                $attributes['class'] = 'smallicon';
            }

            // If the alt is empty, don't place it in the attributes, otherwise it will override parent alt text.
            if (!is_null($alt)) {
                $attributes['alt'] = $alt;

                // If there is no title, set it to the attribute.
                if (!isset($attributes['title'])) {
                    $attributes['title'] = $attributes['alt'];
                }
            } else {
                unset($attributes['alt']);
            }

            if (empty($attributes['title'])) {
                // Remove the title attribute if empty, we probably want to use the parent node's title
                // and some browsers might overwrite it with an empty title.
                unset($attributes['title']);
            }
            
            return html_writer::tag('span', '', array('class' => $attributes['class'].' '.$this->mod_icons[$component], 'alt' => $attributes['alt'], 'title' => $attributes['title']));
        } else {
            $icon = new pix_icon($pix, $alt, $component, $attributes);
            return $this->render($icon);   
        }
    }
    
}

