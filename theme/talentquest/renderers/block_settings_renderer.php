<?php

require_once($CFG->libdir. "/../blocks/settings/renderer.php");

class theme_talentquest_block_settings_renderer extends block_settings_renderer{
    public function settings_tree(settings_navigation $navigation) {
        global $PAGE;
        
        $coursecontext = context_course::instance($PAGE->course->id);
        $count = 0;
        foreach ($navigation->children as &$child) {
            $child->preceedwithhr = ($count!==0);
            if ($child->display) {
                $count++;
            }
        }
        //print_object($navigation->children->get_key_list());
        //print_object($navigation->children->get('courseadmin'));
        //$navigation->children->remove('courseadmin');
        //$PAGE->blocks->add_fake_block($navbc, reset($regions));
        /*$bc = new block_contents();
        $bc->title = html_writer::span(get_string('quiznavigation', 'quiz'), '', array('id' => 'mod_quiz_navblock_title'));
        $bc->content = $output->navigation_panel($panel);*/

        //print_object($PAGE->blocks->get_regions());
        $content = '';
        $id = 1000;
        $order = 2;
        foreach($navigation->children->get_key_list() as $key){
            $id++;
            if($key != 'modulesettings' and $key != 'courseadmin') continue;
            if(!$key) continue;
            
            $item = $navigation->children->get($key);
            $navigation->children->remove($key);
            $hidden = true;
            
            //if (isset($PAGE->course->id) and $PAGE->course->id > 1 and $key == 'courseadmin') $hidden = false;
            //if (isset($PAGE->cm->id) and $key == 'modulesettings') $hidden = false;
                
            $navigationattrs = array(
                'class' => 'block_tree list top-item'.(($hidden) ? ' hidden' : ''),
                'role' => 'tree');
            $content .= html_writer::start_tag('li', array('class' => 'course-nav-item open'));
            $content .= html_writer::tag('p', $item->text, array('class' => 'course-nav-item-title', 'onclick'=>'jQuery(this).parent().toggleClass("open");'));
            $content .= $this->navigation_node($item, $navigationattrs);
            $content .= html_writer::end_tag('li');

            
            //$PAGE->requires->js_call_amd("block_settings/settingsblock", 'init', $arguments);
            $order++;
        }
        
        if (!empty($content) and has_capability('moodle/course:manageactivities', $coursecontext)){
            $bc = new block_contents(); 
            $bc->attributes['id'] = 'inst'.$id;
            $bc->attributes['data-instanceid'] = $id;
            $bc->attributes['class'] = "block_settings block";
            $bc->attributes['style'] = "order:-10;";
            $bc->attributes['data-block'] = 'test';
            $bc->title = get_string('coursetools', 'theme_talentquest');
            $bc->content = $content;
            $bc->blockpositionid = 0;
            $bc->collapsible = false;
            $PAGE->blocks->add_fake_block($bc, 'side-post');
            $arguments = array(
                'instanceid' => $id,
            );
        }

        $navigationattrs = array(
            'class' => 'block_tree list',
            'role' => 'tree',
            'data-ajax-loader' => 'block_navigation/site_admin_loader');
        $content = $this->navigation_node($navigation, $navigationattrs);
        if (isset($navigation->id) && !is_numeric($navigation->id) && !empty($content)) {
            $content = $this->output->box($content, 'block_tree_box', $navigation->id);
        }


        return $content;
    }
    
    /**
     * Build the navigation node.
     *
     * @param navigation_node $node the navigation node object.
     * @param array $attrs list of attributes.
     * @param int $depth the depth, default to 1.
     * @return string the navigation node code.
     */
    protected function navigation_node(navigation_node $node, $attrs=array(), $depth = 1) {
        $items = $node->children;

        // exit if empty, we don't want an empty ul element
        if ($items->count()==0) {
            return '';
        }

        // array of nested li elements
        $lis = array();
        $number = 0;
        foreach ($items as $item) {
            $number++;
            if (!$item->display) {
                continue;
            }
            
            /*if ($item->key == 'turneditingonoff') {
                $item->text = 'Test';
            }*/

            $isbranch = ($item->children->count()>0  || $item->nodetype==navigation_node::NODETYPE_BRANCH);

            if ($isbranch) {
                $item->hideicon = true;
            }

            $content = $this->output->render($item);
            $id = $item->id ? $item->id : uniqid();
            $ulattr = ['id' => $id . '_group', 'role' => 'group'];
            $liattr = ['class' => [$item->get_css_type(), 'depth_'.$depth], 'tabindex' => '-1'];
            $pattr = ['class' => ['tree_item'], 'role' => 'treeitem'];
            $pattr += !empty($item->id) ? ['id' => $item->id] : [];
            $hasicon = (!$isbranch && $item->icon instanceof renderable);

            if ($isbranch) {
                $liattr['class'][] = 'contains_branch';
                if (!$item->forceopen || (!$item->forceopen && $item->collapse) || ($item->children->count() == 0
                        && $item->nodetype == navigation_node::NODETYPE_BRANCH)) {
                    $pattr += ['aria-expanded' => 'false'];
                } else {
                    $pattr += ['aria-expanded' => 'true'];
                }
                if ($item->requiresajaxloading) {
                    $pattr['data-requires-ajax'] = 'true';
                    $pattr['data-loaded'] = 'false';
                } else {
                    $pattr += ['aria-owns' => $id . '_group'];
                }
            } else if ($hasicon) {
                $liattr['class'][] = 'item_with_icon';
                $pattr['class'][] = 'hasicon';
            }
            if ($item->isactive === true) {
                $liattr['class'][] = 'current_branch';
            }
            if (!empty($item->classes) && count($item->classes) > 0) {
                $pattr['class'] = array_merge($pattr['class'], $item->classes);
            }
            $nodetextid = 'label_' . $depth . '_' . $number;

            // class attribute on the div item which only contains the item content
            $pattr['class'][] = 'tree_item';
            if ($isbranch) {
                $pattr['class'][] = 'branch';
            } else {
                $pattr['class'][] = 'leaf';
            }

            $liattr['class'] = join(' ', $liattr['class']);
            $pattr['class'] = join(' ', $pattr['class']);

            if (isset($pattr['aria-expanded']) && $pattr['aria-expanded'] === 'false') {
                $ulattr += ['aria-hidden' => 'true'];
            }

            $content = html_writer::tag('p', $content, $pattr) . $this->navigation_node($item, $ulattr, $depth + 1);
            if (!empty($item->preceedwithhr) && $item->preceedwithhr===true) {
                $content = html_writer::empty_tag('hr') . $content;
            }
            $liattr['aria-labelledby'] = $nodetextid;
            $content = html_writer::tag('li', $content, $liattr);
            $lis[] = $content;
        }

        if (count($lis)) {
            if (empty($attrs['role'])) {
                $attrs['role'] = 'group';
            }
            return html_writer::tag('ul', implode("\n", $lis), $attrs);
        } else {
            return '';
        }
    }
}