<?php
require_once($CFG->libdir. "/../question/renderer.php");

class theme_talentquest_core_question_bank_renderer extends core_question_bank_renderer{

    public function qbank_chooser($real, $fake, $course, $hiddenparams) {
        global $OUTPUT;

        // Start the form content.
        $formcontent = html_writer::start_tag('form', array('action' => new moodle_url('/question/question.php'),
            'id' => 'chooserform', 'method' => 'get'));

        // Add the hidden fields.
        $hiddenfields = '';
        $hiddenfields .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'category', 'id' => 'qbankcategory'));
        $hiddenfields .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'courseid', 'value' => $course->id));
        foreach ($hiddenparams as $k => $v) {
            $hiddenfields .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => $k, 'value' => $v));
        }
        $hiddenfields .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'sesskey', 'value' => sesskey()));
        $formcontent .= html_writer::div($hiddenfields, '', array('id' => 'typeformdiv'));

        // Put everything into one tag 'options'.
        $formcontent .= html_writer::start_tag('div', array('class' => 'options'));
        $formcontent .= html_writer::div(get_string('selectaqtypefordescription', 'question'), 'instruction');

        // Put all options into one tag 'qoptions' to allow us to handle scrolling.
        $formcontent .= html_writer::start_tag('div', array('class' => 'alloptions chose-modules'));


        $title = '';
        $title .= html_writer::empty_tag('input',array('id'=>'show_activities','type'=>'radio','class'=>'hidden','name'=>'tab','checked'=>'checked'));
        $title .= html_writer::empty_tag('input',array('id'=>'show_resources','type'=>'radio','class'=>'hidden','name'=>'tab'));

        $title .= html_writer::start_div('tabtree');
        $title .= html_writer::start_tag('ul');
            $title .= html_writer::tag('li',html_writer::tag('label',get_string('questions', 'question'),array('for'=>'show_activities')));
            $title .= html_writer::tag('li',html_writer::tag('label',get_string('other'),array('for'=>'show_resources')));
        $title .= html_writer::end_tag('ul');
        $title .= html_writer::end_div();

        $content = '';
            $content .= html_writer::div($this->qbank_chooser_types($real), 'activities');
            $content .= html_writer::div($this->qbank_chooser_types($fake), 'resources');
        $formcontent .= $title.$content;

        /*// First display real questions.
        //get_string($title, $identifier)
        $formcontent .= $this->qbank_chooser_title('questions', 'question');
        $formcontent .= $this->qbank_chooser_types($real);

        $formcontent .= html_writer::div('', 'separator');

        // Then fake questions.
        $formcontent .= $this->qbank_chooser_title('other');
        $formcontent .= $this->qbank_chooser_types($fake);*/

        // Options.
        $formcontent .= html_writer::end_tag('div');

        // Types.
        $formcontent .= html_writer::end_tag('div');

        // Add the form submission buttons.
        $submitbuttons = '';
        $submitbuttons .= html_writer::tag('input', '',
            array('type' => 'submit', 'name' => 'submitbutton', 'class' => 'submitbutton', 'value' => get_string('add')));
        $submitbuttons .= html_writer::tag('input', '',
            array('type' => 'submit', 'name' => 'addcancel', 'class' => 'addcancel', 'value' => get_string('cancel')));
        $formcontent .= html_writer::div($submitbuttons, 'submitbuttons');

        $formcontent .= html_writer::end_tag('form');

        // Wrap the whole form in a div.
        $formcontent = html_writer::tag('div', $formcontent, array('id' => 'chooseform'));

        // Generate the header and return the whole form.
        $header = html_writer::div(get_string('chooseqtypetoadd', 'question'), 'choosertitle hd');
        return $header . html_writer::div(html_writer::div($formcontent, 'choosercontainer'), 'chooserdialogue');
    }

}

?>