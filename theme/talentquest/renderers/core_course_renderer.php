<?php
require_once($CFG->libdir. "/../course/renderer.php");
class theme_talentquest_core_course_renderer extends core_course_renderer{
		
    public $tabs;
    
    public $mod_icons = array(
        'assign'=>'fa fa-check-square-o',
        'assignment'=>'fa fa-check-square-o',
        'chat'=>'fa fa-comment-o',
        'choice'=>'fa fa-question',
        'data'=>'fa fa-database',
        'data'=>'fa fa-database',
        'lti'=>'fa fa-external-link',
        'forum'=>'fa fa-comments-o',
        'glossary'=>'fa fa-newspaper-o',
        'lesson'=>'fa fa-puzzle-piece',
        'quiz'=>'fa fa-question-circle',
        'scorm'=>'fa fa-desktop',
        'survey'=>'fa fa-cubes',
        'wiki'=>'fa fa-archive',
        'workshop'=>'fa fa-users',
        'bigbluebuttonbn'=>'fa fa-video-camera',
        'facetoface'=>'fa fa-male',
        'adobeconnect'=>'fa fa-briefcase',
        'openmeetings'=>'fa fa-leaf',
        'tincanlaunch'=>'fa fa-bullhorn',
        'webexactivity'=>'fa fa-play-circle',
        'book'=>'fa fa-book',
        'resource'=>'fa fa-file-o',
        'folder'=>'fa fa-folder-open-o',
        'imscp'=>'fa fa-server',
        'label'=>'fa fa-tag',
        'page'=>'fa fa-file-text-o',
        'recordingsbn'=>'fa fa-file-video-o',
        'url'=>'fa fa-link',
        'feedback'=>'fa fa-commenting-o',
        'certificate'=>'fa fa-certificate',
        'scormcloud'=>'fa fa-cloud-upload',
    );
    
    public function course_section_cm_list_item($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
        $output = '';
        if ($modulehtml = $this->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {
            $completion_action = $this->course_activity_completion($course, $completioninfo, $mod, $displayoptions);
            $modclasses = 'activity ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;
            $output .= html_writer::tag('li', $completion_action.$modulehtml, array('class' => $modclasses, 'id' => 'module-' . $mod->id));
        }
        return $output;
    }
    
    public function course_activity_completion($course, &$completioninfo, cm_info $mod, $displayoptions = array()) {
        global $CFG;
        $output = '';
        
        if ($this->is_cm_conditionally_hidden($mod) and !$mod->uservisible){
            $attributes = array();
            $attributes['class'] = 'cm-conditionalhidden';
            $attributes['title'] = get_string('restrictaccess', 'theme_talentquest');
            
            $output = html_writer::tag('span', '<i class="ion-android-lock"></i>', $attributes);
            return $output;
        }
        
        if (!empty($displayoptions['hidecompletion']) || !isloggedin() || isguestuser() || !$mod->uservisible) {
            return $output;
        }
         
        $status = 'not-completed'; $action = ''; $method = 'manual'; $url = ''; $title = '';
        $completionicon = '';
        
        if ($completioninfo === null) {
            $completioninfo = new completion_info($course);
        }
        $completion = $completioninfo->is_enabled($mod);
        if ($completioninfo->is_enabled($mod)){
            if ($this->page->user_is_editing()) {
                return $output;
            }

            $completiondata = $completioninfo->get_data($mod, true);
            if ($completion == COMPLETION_TRACKING_MANUAL) {
                $method = 'manual';
                switch($completiondata->completionstate) {
                    case COMPLETION_INCOMPLETE:
                        $completionicon = 'manual-n';
                        $futureicon = 'manual-y';
                        $status = 'not-completed'; break;
                    case COMPLETION_COMPLETE:
                        $completionicon = 'manual-y';
                        $futureicon = 'manual-n';
                        $status = 'completed'; break;
                }
            } else { // Automatic
                $method = 'auto';
                switch($completiondata->completionstate) {
                    case COMPLETION_INCOMPLETE:
                        $completionicon = 'auto-n';
                        $status = 'not-completed'; break;
                    case COMPLETION_COMPLETE:
                        $completionicon = 'auto-y';
                        $status = 'completed'; break;
                    case COMPLETION_COMPLETE_PASS:
                        $completionicon = 'auto-pass';
                        $status = 'pass'; break;
                    case COMPLETION_COMPLETE_FAIL:
                        $completionicon = 'auto-fail';
                        $status = 'fail'; break;
                }
            }
            
            if ($status == 'not-completed' and $completiondata->viewed > 0){
                $status = 'pending';
            }
            
            
            $formattedname = $mod->get_formatted_name();
            $title = get_string('completion-alt-' . $completionicon, 'completion', $formattedname);
            $futuretitle = (isset($futureicon)) ? get_string('completion-alt-' . $futureicon, 'completion', $formattedname) : '';
            
            if ($completion == COMPLETION_TRACKING_MANUAL) {
                $title = get_string('completion-title-' . $completionicon, 'completion', $formattedname);
                $futuretitle = (isset($futureicon)) ? get_string('completion-title-' . $futureicon, 'completion', $formattedname) : '';
                $state =
                    $completiondata->completionstate == COMPLETION_COMPLETE
                    ? COMPLETION_COMPLETE
                    : COMPLETION_INCOMPLETE;
                $newstate =
                    $completiondata->completionstate == COMPLETION_COMPLETE
                    ? COMPLETION_INCOMPLETE
                    : COMPLETION_COMPLETE;
                // In manual mode the icon is a toggle form...
                $url = $CFG->wwwroot.'/course/togglecompletion.php?id='.$mod->id.'&fromajax=1&sesskey='.sesskey();
            }
        }
        $attributes = array();
        $attributes['class'] = 'cm-completion-toggler';
        $attributes['class'] .= ' '.$status;
        $attributes['class'] .= ' '.$method;
        if ($method == 'manual' and $url != ''){
            $attributes['onclick'] = 'courseToggleCompletion(this, "'.$url.'", '.$state.', '.$newstate.', "'.$title.'", "'.$futuretitle.'");';
        }
        if ($title != ''){
            $attributes['title'] = $title;
        }
        $output = html_writer::tag('span', '', $attributes);
        
        return $output;
    }
  
	public function frontpage_available_courses($params = array()) {
        global $CFG, $OUTPUT;			
        require_once($CFG->libdir. '/coursecatlib.php');

            $sort = (isset($params['sort'])) ? $params['sort'] : 'displayname';
            $sort_nav = (isset($params['sort']) and $params['sort'] == 'desc') ? -1 : 1;
            $limit = (isset($params['limit'])) ? $params['limit'] : $CFG->coursesperpage;
            $page = (isset($params['page'])) ? $params['page'] : 0;
            
            $displayoptions = array('sort' => array($sort => $sort_nav));
            $displayoptions['limit'] = $limit;
            $displayoptions['offset'] = $displayoptions['limit'] * $page;
            $displayoptions['course_tab'] = 'available';
            $displayoptions['recursive'] = true;
            
			$chelper = new coursecat_helper();
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
					set_courses_display_options($displayoptions);
			
			$chelper->set_attributes(array('class' => 'clearfix frontpage-course-list-all'));
			$courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
			
			/*foreach(enrol_get_my_courses('summary, summaryformat') as $id=>$val){
				unset($courses[$id]);
			}*/
        	
			$totalcount = coursecat::get(0)->get_courses_count($chelper->get_courses_display_options());
			if(!$totalcount){
				return '';
			}elseif($totalcount){			
                $output .= $this->coursecat_courses($chelper, $courses, $totalcount);
				if(count($courses) > $limit){
					$output .= html_writer::start_tag('div', array('class' => 'paging-morelink'));
					$output .= html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("all",1);',));			
					$output .= html_writer::end_tag('div');					
				}
			}
			return $output;
		}
    
	    public function frontpage_my_courses() {
			global $USER, $CFG, $DB, $OUTPUT;
			require_once($CFG->libdir. '/coursecatlib.php');
			
            if (!isloggedin() or isguestuser()) {
                return '';
            }

            $output = '';
            $category = coursecat::make_categories_list();
            $sortorder = 'fullname ASC';
            
            $courses  = enrol_get_my_courses('summary, summaryformat', $sortorder);
            if(count($courses) == 0){
                return '';
            }
            $rhosts   = array();
            $rcourses = array();
            if (!empty($CFG->mnet_dispatcher_mode) && $CFG->mnet_dispatcher_mode==='strict') {
                $rcourses = get_my_remotecourses($USER->id);
                $rhosts   = get_my_remotehosts();
            }

            if (!empty($courses) || !empty($rcourses) || !empty($rhosts)) {

                $chelper = new coursecat_helper();
                if (count($courses) > $CFG->frontpagecourselimit) {
                    // There are more enrolled courses than we can display, display link to 'My courses'.
                    $totalcount = count($courses);
                    $courses = array_slice($courses, 0, $CFG->frontpagecourselimit, true);

                } else {
                    // All enrolled courses are displayed, display link to 'All courses' if there are more courses in system.
                    $totalcount = $DB->count_records('course') - 1;
                }
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('course_tab' => 'my'));
                $chelper->set_attributes(array('class' => 'clearfix frontpage-course-list-enrolled'));
                $output .= $this->coursecat_courses($chelper, $courses, $totalcount);

                // MNET
                if (!empty($rcourses)) {
                    // at the IDP, we know of all the remote courses
                    $output .= html_writer::start_tag('div', array('class' => 'courses'));
                    foreach ($rcourses as $course) {
                        $output .= $this->frontpage_remote_course($course);
                    }
                    $output .= html_writer::end_tag('div'); // .courses
                } elseif (!empty($rhosts)) {
                    // non-IDP, we know of all the remote servers, but not courses
                    $output .= html_writer::start_tag('div', array('class' => 'courses'));
                    foreach ($rhosts as $host) {
                        $output .= $this->frontpage_remote_host($host);
                    }
                    $output .= html_writer::end_tag('div'); // .courses
                }
            }

            if((count(enrol_get_my_courses('summary, summaryformat')) - $CFG->frontpagecourselimit) >= $CFG->frontpagecourselimit){
                $output .= html_writer::start_tag('div', array('class' => 'paging-morelink'));
                $output .= html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("my",1);',));			
                $output .= html_writer::end_tag('div');					
            }
            return $output;
        }
    
		public function frontpage_available_courses_search($category, $search) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
				$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
						set_courses_display_options(array(
							'recursive' => true,
							'viewmoreurl' => new moodle_url('/course/index.php'),
							'viewmoretext' => new lang_string('fulllistofcourses')));
				$chelper->set_attributes(array('class' => 'frontpage-course-list-all'));
				if($category>=0){
					$courses_all = coursecat::get($category)->get_courses($chelper->get_courses_display_options());
				}else{
					$courses_all = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
				}
				
				foreach($courses_all as $course_all){
					 if($search == '' || strripos($course_all->fullname, $search) !== false || strripos($course_all->shortname, $search) !== false){
						$courses[$course_all->id] = $course_all; 
					 }	 
				 }
				 if(count($courses) == 0) {
					 $out = html_writer::tag('div',html_writer::tag('div',get_string('not_found_course', 'theme_talentquest')."'".$search."'", array('class' => 'alert alert-warning')), array('class' => 'courses frontpage-course-list-all'));
				 }else{
					 $out = $this->coursecat_courses($chelper, $courses);
				 }
				
				
				echo $out;
	}
    
	public function frontpage_my_courses_search($category, $search) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
				$courses_all  = enrol_get_my_courses('summary, summaryformat');
				 foreach($courses_all as $course_all){
					 if(($category == $course_all->category || $category == '') && ($search == '' ||strripos($course_all->fullname, $search) !== false || strripos($course_all->shortname, $search) !== false)){
						$courses[$course_all->id] = $course_all; 
					 }	 
				 }
				 
				if(count($courses) == 0) echo html_writer::tag('div',$OUTPUT->heading('Not Found'), array('class' => 'courses frontpage-course-list-enrolled'));
				$chelper = new coursecat_helper();
				$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
						set_courses_display_options(array(
							'recursive' => true,
							'limit' => $CFG->frontpagecourselimit,
							'viewmoreurl' => new moodle_url('/course/index.php'),
							'viewmoretext' => new lang_string('fulllistofcourses')));
				$chelper->set_attributes(array('class' => 'frontpage-course-list-enrolled'));
				$out = $this->coursecat_courses($chelper, $courses);
				
				echo $out;
	} 
    
	public function frontpage_available_courses_get_courses($step) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
			$chelper->set_attributes(array('id' => 'temp'));
			$chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->set_courses_display_options(array('recursive' => true));
			$courses = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
			$courses = array_splice($courses, ($CFG->frontpagecourselimit * $step));
			$output = '';
			$count = count($courses);
			
				if(count($courses) > $CFG->frontpagecourselimit){
					while(count($courses) > $CFG->frontpagecourselimit){
						array_pop($courses);
					}	
				}
			$output .= $this->coursecat_courses($chelper, $courses);
			if($count > $CFG->frontpagecourselimit){
				echo html_writer::start_tag('div', array('id' => 'temp2')).html_writer::start_tag('div', array('class' => 'paging-morelink')).html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("all", '.++$step.');',)).html_writer::end_tag('div').html_writer::end_tag('div');		
			}
			echo $output;
	} 
    
	public function frontpage_my_courses_get_courses($step) {
			global $CFG, $OUTPUT;			
			require_once($CFG->libdir. '/coursecatlib.php');
			$chelper = new coursecat_helper();
			$chelper->set_attributes(array('id' => 'temp'));
			$courses = enrol_get_my_courses('summary, summaryformat');
			$courses = array_splice($courses, ($CFG->frontpagecourselimit * $step));
			$output = '';	
			$count = count($courses);
			
				if(count($courses) > $CFG->frontpagecourselimit){	
					while(count($courses) > $CFG->frontpagecourselimit){
						array_pop($courses);
					}	 			
				}
			$output .= $this->coursecat_courses($chelper, $courses);
			echo $output;
			if($count > $CFG->frontpagecourselimit){
				echo html_writer::start_tag('div', array('id' => 'temp2')).html_writer::start_tag('div', array('class' => 'paging-morelink')).html_writer::tag('a', 'Show more' ,array('onclick' => 'ajax_get_courses("my", '.++$step.');',)).html_writer::end_tag('div').html_writer::end_tag('div');		
			}
    }
    
    
    protected function coursecat_coursebox(coursecat_helper $chelper, $course, $additionalclasses = '') {
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        
        $output .= html_writer::start_tag('div', array(
                    'class' => 'coursebox clearfix',
                    'data-courseid' => $course->id
                ));

            // display course image
            $contentimages = '';
            $image_files = array();
            $fs = get_file_storage();
            $course->context = context_course::instance($course->id, MUST_EXIST);
            $imgfiles = $fs->get_area_files($course->context->id, 'format_talentquest', 'thumbnail', $course->id);
            foreach ($imgfiles as $file) {
                $filename = $file->get_filename(); 
                $filetype = $file->get_mimetype(); 
                if ($filename == '.' or !$filetype) continue;
                $url = moodle_url::make_pluginfile_url($course->context->id, 'format_talentquest', 'thumbnail', $course->id, '/', $filename);
                $contentimages .= html_writer::empty_tag('img', array('src' => $url->out()));
            }
        
            if($contentimages == ""){
                $contentimages .= html_writer::empty_tag('img', array('src' => $CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg"));
            }
        
            $output .= html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)), html_writer::tag('div', '', array('class'=>'course-color', 'style'=>'background-color:#'.((isset($course->color) and !empty($course->color)) ? $course->color : 'aaaaaa'))).$contentimages);
        
            // course name
            $output .= html_writer::tag('h3', html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)) , $course->fullname), array('class' => 'coursename'));
        
            if ($course->startdate > 0 or $course->enddate > 0){
                $output .= html_writer::start_tag('ul', array('class' => 'course-dates clearfix'));
                    $output .= html_writer::tag('li', '<i class="fa fa-calendar"></i>', array('class' => 'course-dates-img'));
                    if ($course->startdate > 0){
                        $output .= html_writer::tag('li', 'Start Date '.date('m/j/Y', $course->startdate), array('class' => 'course-start-date'));
                    }
                    if ($course->enddate > 0){
                        $output .= html_writer::tag('li', 'End Date '.date('m/j/Y', $course->enddate), array('class' => 'course-end-date'));
                    }
                $output .= html_writer::end_tag('ul');
            }
            
            $teachers = $DB->get_record_sql("SELECT  u.firstname,u.lastname, COUNT(ra.id) AS count_teachers                    
                                                                                FROM {course} AS c
                                                                                JOIN {context} AS ct ON c.id = ct.instanceid
                                                                                JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                                                                                JOIN {user} AS u ON u.id = ra.userid
                                                                                WHERE c.id=".$course->id);
            $teacher = '';
            if($teachers->count_teachers == 1 && !empty($teachers->firstname)){
                $teacher = 'By '.$teachers->firstname.' '.$teachers->lastname;
            }elseif($teachers->count_teachers > 1){
                $teachers->count_teachers--;
                $teacher = 'By '.$teachers->firstname.' '.$teachers->lastname.' (and '.$teachers->count_teachers.' other)';
            } 	 			
            $output .= html_writer::tag('h4',$teacher, array('class' => 'course-teacher'));

            if($USER->id && isset($course->coursetype) and $course->coursetype == 'enrolled'){

                $data = $DB->get_record_sql("SELECT
                    (SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM  {grade_items} gi, {grade_grades} gg  WHERE gi.itemtype = 'course' AND gg.itemid = gi.id AND gi.courseid=$course->id AND gg.userid=$USER->id) AS average
                ");
                $completion_info = $this->get_course_completion($course);
                $progress = $completion_info->completion;

                $grade_info = html_writer::tag('div',
                                html_writer::tag('span',get_string('grade','theme_talentquest'),array('class' => 'name')).
                                html_writer::tag('span',intval($data->average).'%',array('class' => 'procent')).
                                html_writer::start_tag('div',array('class' => 'progres-bar')).
                                html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.intval($data->average).'%;')).
                                html_writer::end_tag('div'),
                                array('class'=>'clearfix')
                            );
                $progress_info = html_writer::tag('div',
                                    html_writer::tag('span',get_string('course_progress','theme_talentquest'),array('class' => 'name')).
                                    html_writer::tag('span',intval($progress).'%',array('class' => 'procent')).
                                    html_writer::start_tag('div',array('class' => 'progres-bar')).
                                    html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.$progress.'%;')).
                                    html_writer::end_tag('div'),
                                    array('class'=>'clearfix')
                                );
                if($completion_info->status == "completed"){
                    $output .= html_writer::tag('i','',array('class'=>'fa fa-check course-done'));
                }
                $output .= $OUTPUT->box($grade_info.$progress_info,'user-progress clearfix');

            }else{
                $users = $DB->get_record_sql("SELECT COUNT( DISTINCT ra.userid) AS count_users                    
                                                                            FROM {course} AS c
                                                                            JOIN {context} AS ct ON c.id = ct.instanceid
                                                                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id 
                                                                            JOIN {user} AS u ON u.id = ra.userid
                                                                            WHERE c.id=".$course->id);
                $count_user = '<i class="fa  fa-group"></i> '.$users->count_users.' users enrolled';

                if(has_capability('moodle/course:enrolreview', $course->context))
                    $output .= html_writer::tag('h4',html_writer::link(new moodle_url('/enrol/users.php',array('id'=>$course->id)),$count_user), array('class' => 'course-users'));
                else
                    $output .= html_writer::tag('h4',$count_user, array('class' => 'course-users'));
            }

        $output .= html_writer::end_tag('div');
        
        return $output;
             
    }
    
    protected function coursecat_coursebox_content(coursecat_helper $chelper, $course) {
        global $CFG,$PAGE;
		
        if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
            return '';
        }
        if ($course instanceof stdClass) {
            require_once($CFG->libdir. '/coursecatlib.php');
            $course = new course_in_list($course);
        }
        $content = '';

        // display course summary
        if ($course->has_summary()) {
            $content .= $chelper->get_course_formatted_summary($course,
                    array('overflowdiv' => true, 'noclean' => true, 'para' => false));
        }

        return $content;
	 }
    
    public function course_info_box(stdClass $course){
        global $CFG, $PAGE, $DB, $OUTPUT, $USER;
        $course = course_get_format($course)->get_course();
        require_once($CFG->dirroot. "/course/format/".$course->format."/lib.php");
        course_create_sections_if_missing($course, range(0, $course->numsections));
        $context = context_course::instance($course->id, MUST_EXIST);
        
        $modinfo = get_fast_modinfo($course);
        $modnames = get_module_types_names();
        $modnamesplural = get_module_types_names(true);
        $modnamesused = $modinfo->get_used_module_names();
        $mods = $modinfo->get_cms();
        $sections = $modinfo->get_section_info_all();
        
        if ($course->format == 'talentquest'){
            $course = course_get_format($course)->get_course();
            $course->completion_info = courseGetCompletion($course);
            course_create_sections_if_missing($course, range(0, $course->numsections));
            $course->completion_info = courseGetCompletion($course);

            $renderer = $PAGE->get_renderer('format_talentquest');
            $cfields = course_get_custom_fields($course);

            $course_description = $renderer->get_course_formatted_summary($course,
                                      array('overflowdiv' => true, 'noclean' => false, 'para' => false));   
            
            echo html_writer::start_tag('div', array('class'=>'course-content course-not-enrolled'));

            echo html_writer::start_tag('div', array('class'=>'course-title-box'));
            $course_image_url = get_course_image_url($course);
            echo html_writer::tag('div', '', array('class'=>'course-image', 'style'=>'background-image: url("'.$course_image_url.'");'));
            echo html_writer::start_tag('div', array('class'=>'course-title'));
            echo $course->fullname;
                /*if (count($cfields['list'])){
                    echo html_writer::start_tag('div', array('class'=>'course-custom-toggler'));
                        echo html_writer::tag('i', '', array('onclick'=>'toggleCourseCF();', 'class'=>'fa fa-angle-down  hidden test'));
                        echo html_writer::tag('i', '', array('onclick'=>'toggleCourseCF();', 'class'=>'fa fa-angle-up hidden test'));
                    echo html_writer::end_tag('div');
                }*/
            echo html_writer::end_tag('div');
            echo course_get_course_teachers($course);
            echo html_writer::end_tag('div');
            
            echo course_print_rating($course);

            
            if (!empty($course->startdate) or !empty($course->enddate)){
                echo html_writer::start_tag('div', array('class'=>'course-dates clearfix'));
                    if (!empty($course->startdate)){
                        echo html_writer::start_tag('div', array('class'=>'course-date-item'));
                            echo html_writer::tag('label', get_string('startdate', 'format_talentquest').':');
                            echo html_writer::tag('span', date('m/d/Y', $course->startdate), array('class'=>'course-date'));
                        echo html_writer::end_tag('div');
                    }
                    if (!empty($course->enddate)){
                        echo html_writer::start_tag('div', array('class'=>'course-date-item'));
                            echo html_writer::tag('label', get_string('enddate', 'format_talentquest').':');
                            echo html_writer::tag('span', date('m/d/Y', $course->enddate), array('class'=>'course-date'));
                        echo html_writer::end_tag('div');
                    }
                echo html_writer::end_tag('div');
            }

            if (!is_enrolled($context, $USER)){
                echo html_writer::tag('div', 'Start Course', array('class' => 'enroll-button btn', 'onclick'=>'location="'.$CFG->wwwroot.'/enrol/enroll.php?id='.$course->id.'"' ));
            }
               
            if (count($cfields['list'])){
                echo html_writer::start_tag('ul', array('class'=>'course-custom-list clearfix'));
                    foreach ($cfields['list'] as $fieldname=>$field){
                        echo html_writer::start_tag('li', array('class'=>'course-cutom-item'));
                            echo html_writer::tag('label', $field['title'].':');
                            echo html_writer::tag('span', $field['value']);
                        echo html_writer::end_tag('li');
                    }
                echo html_writer::end_tag('ul');
            }

            echo html_writer::start_tag('div', array('class'=>'nav-tabs-header'));
                echo html_writer::start_tag('ul', array('class'=>'nav-tabs nav-tabs-simple clearfix'));
                    echo html_writer::tag('li', '<a data-toggle="tab" href="#curriculum">'.get_string('curriculum', 'format_talentquest').'</a>', array('class'=>'header-curriculum active'));

                    if (!empty($course_description)){
                        echo html_writer::tag('li', '<a data-toggle="tab" href="#course-description">'.get_string('coursedescription', 'format_talentquest').'</a>', array('class'=>'header-course-description'));
                    }

                    if (count($cfields['tab'])){
                        foreach ($cfields['tab'] as $fieldname=>$field){
                            echo html_writer::tag('li', '<a data-toggle="tab" href="#'.$fieldname.'">'.$field['title'].'</a>', array('class'=>'header-'.$fieldname));
                        }
                    }

                echo html_writer::end_tag('ul');
            echo html_writer::end_tag('div');

            echo html_writer::start_tag('div', array('class'=>'tab-content'));

            echo html_writer::start_tag('div', array('id'=>'curriculum', 'class'=>'tab-pane active'));
                echo html_writer::start_tag('div', array('class'=>'course-curriculum-box'));
                if (!empty($displaysection)) {
                    $renderer->print_single_section_page($course, null, null, null, null, $displaysection);
                } else {
                    $renderer->print_multiple_section_page($course, null, null, null, null);
                }
                echo html_writer::end_tag('div');
            echo html_writer::end_tag('div');

            if (!empty($course_description)){
                echo html_writer::start_tag('div', array('id'=>'course-description', 'class'=>'tab-pane'));
                    echo $course_description;
                echo html_writer::end_tag('div');
            }

            if (count($cfields['tab'])){
                foreach ($cfields['tab'] as $fieldname=>$field){
                    echo html_writer::start_tag('div', array('id'=>$fieldname, 'class'=>'tab-pane'));
                        echo $field['value'];
                    echo html_writer::end_tag('div');
                }
            }

            echo html_writer::end_tag('div');
            echo html_writer::end_tag('div');
            echo "<script>jQuery('.activityinstance a').attr('href', 'javascript:void(0);')</script>";
            
            // Include course format js module
            $PAGE->requires->js('/course/format/talentquest/format.js');
            
        } else {
            
            $content = '';
            $content .= $this->output->box_start('generalbox info');
            $chelper = new coursecat_helper();
            $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
            $content .= $this->coursecat_coursebox($chelper, $course);
            $content .= $this->output->box_end();
            return $content;
        }
    }

    public function course_modchooser($modules, $course) {
        if (!$this->page->requires->should_create_one_time_item_now('core_course_modchooser')) {
            return '';
        }

        // Add the module chooser
        $this->page->requires->yui_module('moodle-course-modchooser',
            'M.course.init_chooser',
            array(array('courseid' => $course->id, 'closeButtonTitle' => get_string('close', 'editor')))
        );
        $this->page->requires->strings_for_js(array(
            'addresourceoractivity',
            'modchooserenable',
            'modchooserdisable',
        ), 'moodle');

        // Add the header
        $header = html_writer::tag('div', get_string('addresourceoractivity', 'moodle'),
            array('class' => 'hd choosertitle'));

        $formcontent = html_writer::start_tag('form', array('action' => new moodle_url('/course/jumpto.php'),
            'id' => 'chooserform', 'method' => 'post'));
        $formcontent .= html_writer::start_tag('div', array('id' => 'typeformdiv'));
        $formcontent .= html_writer::tag('input', '', array('type' => 'hidden', 'id' => 'course',
            'name' => 'course', 'value' => $course->id));
        $formcontent .= html_writer::tag('input', '', array('type' => 'hidden', 'name' => 'sesskey',
            'value' => sesskey()));
        $formcontent .= html_writer::end_tag('div');

        // Put everything into one tag 'options'
        $formcontent .= html_writer::start_tag('div', array('class' => 'options'));
        $formcontent .= html_writer::tag('div', get_string('selectmoduletoviewhelp', 'moodle'),
            array('class' => 'instruction'));
        // Put all options into one tag 'alloptions' to allow us to handle scrolling
        $formcontent .= html_writer::start_tag('div', array('class' => 'alloptions chose-modules'));

        // Activities
        $activities = array_filter($modules, create_function('$mod', 'return ($mod->archetype !== MOD_ARCHETYPE_RESOURCE && $mod->archetype !== MOD_ARCHETYPE_SYSTEM);'));
        $resources = array_filter($modules, create_function('$mod', 'return ($mod->archetype === MOD_ARCHETYPE_RESOURCE);'));

        $title = '';
        if (count($activities))
            $title .= html_writer::empty_tag('input',array('id'=>'show_activities','type'=>'radio','class'=>'hidden','name'=>'tab','checked'=>'checked'));
        if (count($resources))
            $title .= html_writer::empty_tag('input',array('id'=>'show_resources','type'=>'radio','class'=>'hidden','name'=>'tab'));

        $title .= html_writer::start_div('tabtree');
        $title .= html_writer::start_tag('ul');
            if (count($activities))
                $title .= html_writer::tag('li',html_writer::tag('label',get_string('activities'),array('for'=>'show_activities')));
            if (count($resources))
                $title .= html_writer::tag('li',html_writer::tag('label',get_string('resources'),array('for'=>'show_resources')));
        $title .= html_writer::end_tag('ul');
        $title .= html_writer::end_div();

        $content = '';
        if (count($activities))
            $content .= html_writer::div($this->course_modchooser_module_types($activities), 'activities');
        if (count($resources))
            $content .= html_writer::div($this->course_modchooser_module_types($resources), 'resources');
        $formcontent .= $title.$content;

        /*if (count($activities)) {
            $formcontent .= $this->course_modchooser_title('activities');
            $formcontent .= $this->course_modchooser_module_types($activities);
        }

        // Resources

        if (count($resources)) {
            $formcontent .= $this->course_modchooser_title('resources');
            $formcontent .= $this->course_modchooser_module_types($resources);
        }*/

        $formcontent .= html_writer::end_tag('div'); // modoptions
        $formcontent .= html_writer::end_tag('div'); // types

        $formcontent .= html_writer::start_tag('div', array('class' => 'submitbuttons'));
        $formcontent .= html_writer::tag('input', '',
            array('type' => 'submit', 'name' => 'submitbutton', 'class' => 'submitbutton', 'value' => get_string('add')));
        $formcontent .= html_writer::tag('input', '',
            array('type' => 'submit', 'name' => 'addcancel', 'class' => 'addcancel', 'value' => get_string('cancel')));
        $formcontent .= html_writer::end_tag('div');
        $formcontent .= html_writer::end_tag('form');

        // Wrap the whole form in a div
        $formcontent = html_writer::tag('div', $formcontent, array('id' => 'chooseform'));

        // Put all of the content together
        $content = $formcontent;

        $content = html_writer::tag('div', $content, array('class' => 'choosercontainer'));
        return $header . html_writer::tag('div', $content, array('class' => 'chooserdialoguebody'));
    }
    
    /**
     * Renders html to display a name with the link to the course module on a course page
     *
     * If module is unavailable for user but still needs to be displayed
     * in the list, just the name is returned without a link
     *
     * Note, that for course modules that never have separate pages (i.e. labels)
     * this function return an empty string
     *
     * @param cm_info $mod
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm_name_title(cm_info $mod, $displayoptions = array()) {
        $output = '';
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            // Nothing to be displayed to the user.
            return $output;
        }
        $url = $mod->url;
        if (!$url) {
            return $output;
        }

        //Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;
        // Avoid unnecessary duplication: if e.g. a forum name already
        // includes the word forum (or Forum, etc) then it is unhelpful
        // to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename),
                core_text::strtolower($altname))) {
            $altname = '';
        }
        // File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' '.$altname);
        }

        // For items which are hidden but available to current user
        // ($mod->uservisible), we show those as dimmed only if the user has
        // viewhiddenactivities, so that teachers see 'items which might not
        // be available to some students' dimmed but students do not see 'item
        // which is actually available to current student' dimmed.
        $linkclasses = '';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed';
                $textclasses .= ' dimmed_text';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
                // Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents').':'. $mod->modfullname);
            }
        } else {
            $linkclasses .= ' dimmed';
            $textclasses .= ' dimmed_text';
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        $groupinglabel = $mod->get_grouping_label($textclasses);

        // Display link itself.
        if (isset($this->mod_icons[$mod->modname])){
            $activityicon = html_writer::tag('span', '', array('class' => 'iconlarge activityicon '.$this->mod_icons[$mod->modname], 'alt' => ' ', 'role' => 'presentation'));
        } else {
            $activityicon = html_writer::empty_tag('img', array('src' => $mod->get_icon_url(),
                'class' => 'iconlarge activityicon', 'alt' => ' ', 'role' => 'presentation'));
        }
        $activitylink = $activityicon . $accesstext .
                html_writer::tag('span', $instancename . $altname, array('class' => 'instancename'));
        if ($mod->uservisible) {
            $output .= html_writer::link($url, $activitylink, array('class' => $linkclasses, 'onclick' => $onclick)) .
                    $groupinglabel;
        } else {
            // We may be displaying this just in order to show information
            // about visibility, without the actual link ($mod->uservisible)
            $output .= html_writer::tag('div', $activitylink, array('class' => $textclasses)) .
                    $groupinglabel;
        }
        return $output;
    }
    
}

?>