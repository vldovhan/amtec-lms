$(document).ready(function(){ 

    /*if (jQuery('.ColorPicker').length){
        jQuery('.ColorPicker').each(function(e){
            var currentId = jQuery(this).attr('id');
            jQuery(this).ColorPicker({
                onShow: function (colpkr) {
                    jQuery(colpkr).fadeIn(500);
                    return false;
                },
                onHide: function (colpkr) {
                    jQuery(colpkr).fadeOut(500);
                    return false;
                },
                onChange: function (hsb, hex, rgb) {
                    jQuery(colpkr).val(hex);
                },
                onBeforeShow: function () {
                    if (this.value.length){
                        jQuery(this).ColorPickerSetColor(this.value);
                    }
                }			
            });
        });   
    }*/

});

function togglePostSidebar(){
    jQuery("#page").toggleClass("bothside");
    jQuery(".toggle-post-side .fa").toggleClass("hidden");
    if (jQuery("#page").hasClass("bothside")){
        themeSetUserSettings('theme_right_sidebar', 1);
    } else {
        themeSetUserSettings('theme_right_sidebar', 0);
    }
}

function themeSetUserSettings(name, value){
    var url = jQuery('#site_wwwroot').attr('data-value');
    jQuery.get( url+'/theme/talentquest/ajax.php?action=set_user_preferences&name='+name+'&value='+value, function( data ) {});
}

function toggleSidePre(){
    jQuery(".left-sidebar").toggleClass("open");
}

