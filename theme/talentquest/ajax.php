<?php
global $CFG, $OUTPUT,$PAGE;		
require_once('../../config.php');
require_once('lib.php');

$action = required_param('action', PARAM_TEXT);

if($action == 'set_user_preferences'){
	$name = required_param('name', PARAM_TEXT);
	$value = required_param('value', PARAM_TEXT);
	
    set_user_preference($name, $value);
    echo '1';
	
}elseif($action == 'alert_mark'){
    $type = required_param('type', PARAM_TEXT);
    $id = required_param('id', PARAM_INT);

    if($type == 'alert'){
        $DB->delete_records('local_nots_alerts',array('id'=>$id,'userid'=>$USER->id));
    }elseif($type == 'announcement'){
        $record = $DB->get_record('local_sb_announcements',array('id'=>$id,'userto'=>$USER->id));
        if(isset($record->id)){
            $record->new = 0;
            $DB->update_record('local_sb_announcements', $record);
        }
    }
}elseif($action == 'get_notifications'){
    $output = theme_get_notifications();
    die(json_encode(array('output'=>$output)));
}elseif($action == 'set-user-lang'){
    require_once($CFG->libdir.'/gdlib.php');
    require_once($CFG->dirroot.'/user/editlib.php');
    require_once($CFG->dirroot.'/user/lib.php');
    
    $lang = required_param('lang', PARAM_TEXT);
    
    list($user, $course) = useredit_setup_preference_page($USER->id, 1);
    
    // If the specified language does not exist, use the site default.
    if (!get_string_manager()->translation_exists($lang, false)) {
        $lang = core_user::get_property_default('lang');
    }
    
    $user->lang = $lang;
    // Update user with new language.
    user_update_user($user, false, false);

    // Trigger event.
    \core\event\user_updated::create_from_userid($user->id)->trigger();

    if ($USER->id == $user->id) {
        $USER->lang = $lang;
    }
    die(json_encode(array('output'=>time())));
}elseif($action == 'set-user-timezone'){
    require_once($CFG->libdir.'/gdlib.php');
    require_once($CFG->dirroot.'/user/editlib.php');
    require_once($CFG->dirroot.'/user/lib.php');
    
    $timezone = required_param('timezone', PARAM_TEXT);
    
    list($user, $course) = useredit_setup_preference_page($USER->id, 1);
    
    $user->timezone = $timezone;
    // Update user with new timezone.
    user_update_user($user, false, false);

    // Trigger event.
    \core\event\user_updated::create_from_userid($user->id)->trigger();
    $USER->timezone = $timezone;

    die(json_encode(array('output'=>time())));
}
exit;
?>