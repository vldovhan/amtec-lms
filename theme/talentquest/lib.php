<?php
/**
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
function theme_talentquest_page_init(moodle_page $page) {
    user_preference_allow_ajax_update('fix-sidebar', PARAM_INT);
}

function theme_talentquest_settings_btn(){
    $output = '';
    /*$items = theme_get_settings_bar_items();
    
    if (count($items)){
        $output .= html_writer::tag('span', '<i class="fa fa fa-cog"></i>', array('class'=>'main-settings-btn btn', 'title'=>get_string('settings', 'theme_talentquest'), 'onclick'=>'jQuery(".settings-bar").slideToggle();'));
    
    }*/
    return $output;
}

function theme_talentquest_check_editing($style = false){
    global $USER, $DB, $PAGE;
    
    if (isloggedin()){
        if ((strstr($PAGE->url, '/enrol') or 
            strstr($PAGE->url, '/course/edit.php') or         
            strstr($PAGE->url, '/course/delete.php') or 
            strstr($PAGE->url, '/backup/'))
                or (!strstr($PAGE->url, '/course/view.php')
                    and !strstr($PAGE->url, '/mod/')
                    and !strstr($PAGE->url, '/my')
                    )
           ){

            $USER->editing = 0;

            if ($style) echo '<style>.breadcrumb-button .singlebutton {display:none;}</style>';
        }
    }
}

function theme_talentquest_settings_bar(){
    $output = '';
    
    $items = theme_get_settings_bar_items();
    
    if (count($items)){
        $output .= html_writer::start_tag('div', array('class'=>'settings-bar'));
            $output .= html_writer::start_tag('ul', array('class'=>'clearfix'));
                foreach ($items as $item){
                    $output .= html_writer::start_tag('li', array('title'=>$item['title'], 'class'=>(($item['active']) ? 'active' : '')));
                    
                    $item_content = html_writer::tag('i', '', array('class'=>$item['icon'])).' '.html_writer::tag('span', $item['title']);
                    $output .= html_writer::link($item['url'], $item_content);
                    $output .= html_writer::end_tag('li');
                }
            $output .= html_writer::end_tag('ul');
        $output .= html_writer::end_tag('div');
    }
    
    return $output;
}

function theme_get_settings_bar_items(){
    global $CFG, $COURSE, $DB, $USER, $PAGE;
    $items = array();
    $id = optional_param('id', 0, PARAM_INT);
    $systemcontext = context_system::instance();
    $active_button = theme_get_settings_bar_active_item();
    if ($PAGE->course->id > 1){
        $coursecontext = context_course::instance($PAGE->course->id);
    }
    
    if (strstr($PAGE->url, '/local/manager/courses') or 
        strstr($PAGE->url, '/course/format/talentquest/course_options.php') or 
        strstr($PAGE->url, '/course/format/talentquest/course_edit_options.php') or
        strstr($PAGE->url, '/course/edit.php') or 
        strstr($PAGE->url, '/backup/restorefile.php') or 
        strstr($PAGE->url, '/backup/restore.php') or 
        strstr($PAGE->url, '/backup/backupfilesedit.php') or 
        strstr($PAGE->url, '/backup/backupfilesedit.php') or 
        strstr($PAGE->url, '/course/format/talentquest/course_options.php')
       ){
     
        if(is_siteadmin() or has_capability('local/manager:managecourses', $systemcontext)){
            $items[] = array('title'=>get_string('managecourses', 'theme_talentquest'), 'active'=>(($active_button == 'managecourses') ? true : false), 'url'=>new moodle_url('/local/manager/courses/index.php'), 'icon'=>'fa fa-list-ol');
        }
        
        if(has_capability('format/talentquest:manage_options', $systemcontext)){
            $items[] = array('title'=>get_string('managecourseoptions', 'theme_talentquest'), 'active'=>(($active_button == 'managecourseoptions') ? true : false), 'url'=>new moodle_url('/course/format/talentquest/course_options.php'), 'icon'=>'fa fa-th-large');
        }
        if(has_capability('moodle/restore:restorecourse', $systemcontext)){
            $items[] = array('title'=>get_string('import', 'theme_talentquest'), 'active'=>(($active_button == 'restorecourse') ? true : false), 'url'=>new moodle_url('/backup/restorefile.php', array('contextid'=>$systemcontext->id)), 'icon'=>'fa fa-upload');
        }
    }
    if (strstr($PAGE->url, '/local/manager/categories') or strstr($PAGE->url, '/course/editcategory.php')){
        if(has_capability('moodle/category:manage', $systemcontext) and has_capability('local/manager:managecategories', $systemcontext)){
            $items[] = array('title'=>get_string('managecategories', 'theme_talentquest'), 'active'=>(($active_button == 'managecategories') ? true : false), 'url'=>new moodle_url('/local/manager/categories/index.php'), 'icon'=>'fa fa-list-alt');
        }
    }
        
    if (strstr($PAGE->url, '/local/coursecatalog/')){
        
        if(has_capability('local/coursecatalog:manage', $systemcontext) and get_config('local_coursecatalog', 'enabled')){
            $items[] = array('title'=>get_string('managecoursecatalog', 'theme_talentquest'), 'active'=>(($active_button == 'managecoursecatalog') ? true : false), 'url'=>new moodle_url('/local/coursecatalog/index.php'), 'icon'=>'fa fa-list');
        }
    }
    
    if (strstr($PAGE->url, '/local/manager/users') or 
        strstr($PAGE->url, '/user/') or 
        strstr($PAGE->url, '/cohort/') or 
        strstr($PAGE->url, '/admin/roles')){
        
        if(has_capability('local/manager:manageusers', $systemcontext)){
            $items[] = array('title'=>get_string('manageusers', 'theme_talentquest'), 'active'=>(($active_button == 'manageusers') ? true : false), 'url'=>new moodle_url('/local/manager/users/index.php'), 'icon'=>'fa fa-users');
        }
        if(has_capability('moodle/user:create', $systemcontext)){
            $items[] = array('title'=>get_string('createuser', 'theme_talentquest'), 'active'=>(($active_button == 'createuser') ? true : false), 'url'=>new moodle_url('/user/editadvanced.php?id=-1'), 'icon'=>'fa fa-pencil');
        }
        if(has_capability('moodle/role:manage', $systemcontext)){
            $items[] = array('title'=>get_string('rolessetup', 'theme_talentquest'), 'active'=>(($active_button == 'rolessetup') ? true : false), 'url'=>new moodle_url('/admin/roles/manage.php'), 'icon'=>'fa fa-user');
        }
        if(has_capability('moodle/user:editprofile', $systemcontext)){
            $items[] = array('title'=>get_string('profilefields', 'theme_talentquest'), 'active'=>(($active_button == 'profilefields') ? true : false), 'url'=>new moodle_url('/user/profile/index.php'), 'icon'=>'fa fa-cogs');
        }
        if(has_capability('moodle/cohort:manage', $systemcontext)){
            $items[] = array('title'=>get_string('manageusergroups', 'block_sb_admin_menu'), 'active'=>(($active_button == 'manageusergroups') ? true : false), 'url'=>new moodle_url('/cohort/index.php'), 'icon'=>'fa fa-users');
        }
    }
    
    /*if (strstr($PAGE->url, '/badges') and !strstr($PAGE->url, '/badges/mybadges.php') and has_capability('local/gamification:managebadges', $systemcontext)){
        
        if ($PAGE->course->id > 1 and has_capability('moodle/course:manageactivities', $coursecontext)){
            $items[] = array('title'=>get_string('managebadges', 'theme_talentquest'), 'active'=>(($active_button == 'managebadges') ? true : false), 'url'=>new moodle_url('/badges/index.php', array('type'=>$PAGE->url->get_param('type'), 'id'=>$PAGE->url->get_param('id'))), 'icon'=>'fa fa-list-ol');
            
            if (has_capability('moodle/badges:createbadge', $PAGE->context)){
                $items[] = array('title'=>get_string('createbadge', 'theme_talentquest'), 'active'=>(($active_button == 'createbadge') ? true : false), 'url'=>new moodle_url('/badges/newbadge.php', array('type'=>$PAGE->url->get_param('type'), 'id'=>$PAGE->url->get_param('id'))), 'icon'=>'fa fa-pencil-square-o');            
            }
        } else {
            $items[] = array('title'=>get_string('managebadges', 'theme_talentquest'), 'active'=>(($active_button == 'managebadges') ? true : false), 'url'=>new moodle_url('/badges/index.php', array('type'=>$PAGE->url->get_param('type'))), 'icon'=>'fa fa-list-ol');
            
            if (has_capability('moodle/badges:createbadge', $PAGE->context)){
                $items[] = array('title'=>get_string('createbadge', 'theme_talentquest'), 'active'=>(($active_button == 'createbadge') ? true : false), 'url'=>new moodle_url('/badges/newbadge.php', array('type'=>$PAGE->url->get_param('type'))), 'icon'=>'fa fa-pencil-square-o');
            }
        }
        
    }*/
    
    /*if ((strstr($PAGE->url, '/course/completion.php') or 
        (strstr($PAGE->url, '/course/edit.php') and $id > 0) or 
        strstr($PAGE->url, '/enrol/') or 
        strstr($PAGE->url, '/course/reset.php') or 
        strstr($PAGE->url, '/course/view.php') or 
        strstr($PAGE->url, '/backup/restorefile.php') or 
        strstr($PAGE->url, '/backup/backup.php')) and $PAGE->course->id > 1){
        
        $coursecontext = context_course::instance($PAGE->course->id);
        
        if(has_capability('moodle/course:create', $systemcontext)){
            $items[] = array('title'=>get_string('editcourse', 'theme_talentquest'), 'active'=>(($active_button == 'editcourse') ? true : false), 'url'=>new moodle_url('/course/edit.php', array('id'=>$PAGE->course->id)), 'icon'=>'fa fa-pencil-square-o');
        }
        if(is_siteadmin() or has_capability('moodle/course:manageactivities', $coursecontext)){
            $items[] = array('title'=>get_string('coursecompletion', 'theme_talentquest'), 'active'=>(($active_button == 'coursecompletion') ? true : false), 'url'=>new moodle_url('/course/completion.php', array('id'=>$PAGE->course->id)), 'icon'=>'fa fa-check-square-o');
        }
        if(has_capability('moodle/course:enrolconfig', $coursecontext)){
            $items[] = array('title'=>get_string('enrollments', 'theme_talentquest'), 'active'=>(($active_button == 'enrollments') ? true : false), 'url'=>new moodle_url('/enrol/instances.php', array('id'=>$PAGE->course->id)), 'icon'=>'fa fa-child');
        }
        if(has_capability('moodle/course:reset', $coursecontext)){
            $items[] = array('title'=>get_string('resetcourse', 'theme_talentquest'), 'active'=>(($active_button == 'resetcourse') ? true : false), 'url'=>new moodle_url('/course/reset.php', array('id'=>$PAGE->course->id)), 'icon'=>'fa fa-refresh');
        }
        if(has_capability('moodle/backup:backupcourse', $coursecontext)){
            $items[] = array('title'=>get_string('backupcourse', 'theme_talentquest'), 'active'=>(($active_button == 'backupcourse') ? true : false), 'url'=>new moodle_url('/backup/backup.php', array('id'=>$PAGE->course->id)), 'icon'=>'fa fa-download');
        }
        if(has_capability('moodle/restore:restorecourse', $coursecontext)){
            $items[] = array('title'=>get_string('restorecourse', 'theme_talentquest'), 'active'=>(($active_button == 'restorefile') ? true : false), 'url'=>new moodle_url('/backup/restorefile.php', array('contextid'=>$coursecontext->id)), 'icon'=>'fa fa-upload');
        }
    }*/
    
    return $items;
}

function theme_get_settings_bar_active_item(){
    global $PAGE;
    $id = optional_param('id', 0, PARAM_INT);
    $active_button = '';
    
    if (strstr($PAGE->url, '/local/manager/courses')){
        $active_button = 'managecourses';
    } elseif (strstr($PAGE->url, '/course/edit.php') and $id == 0){
        $active_button = 'createcourse';
    } elseif (strstr($PAGE->url, '/local/manager/categories/') or strstr($PAGE->url, '/course/editcategory.php')){
        $active_button = 'managecategories';
    } elseif (strstr($PAGE->url, '/local/coursecatalog/')){
        $active_button = 'managecoursecatalog';
    } elseif (strstr($PAGE->url, '/course/format/talentquest/course_options.php') or strstr($PAGE->url, '/course/format/talentquest/course_edit_options.php')){
        $active_button = 'managecourseoptions';
    } elseif (strstr($PAGE->url, '/backup/restore.php') or strstr($PAGE->url, '/backup/restorefile.php')){
        $active_button = 'restorecourse';
    } elseif (strstr($PAGE->url, '/local/manager/users/')){
        $active_button = 'manageusers';
    } elseif (strstr($PAGE->url, '/cohort/')){
        $active_button = 'manageusergroups';
    } elseif (strstr($PAGE->url, '/admin/roles')){
        $active_button = 'rolessetup';
    } elseif (strstr($PAGE->url, '/user/profile')){
        $active_button = 'profilefields';
    } elseif (strstr($PAGE->url, '/user/editadvanced.php?id=-1')){
        $active_button = 'createuser';
    } elseif (strstr($PAGE->url, '/course/completion.php')){
        $active_button = 'coursecompletion';
    } elseif (strstr($PAGE->url, '/enrol/')){
        $active_button = 'enrollments';
    } elseif (strstr($PAGE->url, '/course/reset.php')){
        $active_button = 'resetcourse';
    } elseif (strstr($PAGE->url, '/backup/backup.php')){
        $active_button = 'backupcourse';
    } elseif (strstr($PAGE->url, '/backup/restore.php') or strstr($PAGE->url, '/backup/restorefile.php')){
        $active_button = 'restorefile';
    } elseif (strstr($PAGE->url, '/badges/index.php')){
        $active_button = 'managebadges';
    } elseif (strstr($PAGE->url, '/badges/newbadge.php')){
        $active_button = 'createbadge';
    }
    
    return $active_button;
}

function theme_talentquest_process_css($css, $theme) {

    // Set the backgroundhead image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_talentquest_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_talentquest_set_customcss($css, $customcss);
    
    $systemcss = theme_talentquest_get_systemstyles();
    $css = theme_talentquest_set_systemcss($css, $systemcss);
	
    return $css;
}

function theme_talentquest_hex2rgba($hex, $type = '') {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
    if ($type == 'string'){
        $rgb = "$r, $g, $b";
    } else {
        $rgb = array($r, $g, $b);   
    }
    //return implode(",", $rgb); // returns the rgb values separated by commas
    return $rgb; // returns an array with the rgb values
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_talentquest_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */

function theme_talentquest_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_talentquest_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');    
    if ($context->contextlevel != CONTEXT_SYSTEM) {
        return false; 
    }
    $itemid = array_shift($args); // The first item in the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage(); 
    $file = $fs->get_file($context->id, 'theme_talentquest', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add talentquest specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_talentquest_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }
    
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote .= '<div class="footnote text-center">'.format_text($page->theme->settings->footnote).'</div>';
    }

    if (!empty($page->theme->settings->login_welcome)) {
        $return->login_welcome .= format_text($page->theme->settings->login_welcome);
    }

    return $return;
}

function theme_get_new_announcements($type = ''){
	global $DB, $USER;
	if ($type == ''){
		$announcements = $DB->get_records_sql("SELECT a.id as aid, a.type, a.title as atitle, a.body, a.startdate, a.new, a.color, c.fullname, c.id as cid, u.* FROM {local_sb_announcements} a 
										LEFT JOIN {course} c ON c.id = a.data 
										LEFT JOIN {user} u ON u.id = a.userfrom
									WHERE a.parentid > 0 AND a.userto = $USER->id AND a.startdate <= ".time()." AND (a.enddate = 0 OR a.enddate >= ".time().") AND a.state > 0 AND u.id > 0 AND u.deleted = 0 AND a.new > 0 ORDER BY a.new DESC, a.startdate DESC LIMIT 5");
		return $announcements;
	} else {
		$announcements = $DB->get_record_sql("SELECT COUNT(a.id) as count FROM {local_sb_announcements} a 
                                        LEFT JOIN {course} c ON c.id = a.data 
										LEFT JOIN {user} u ON u.id = a.userfrom
									WHERE a.parentid > 0 AND a.userto = $USER->id AND a.startdate <= ".time()." AND a.new > 0 AND (a.enddate = 0 OR a.enddate >= ".time().") AND a.state > 0 AND u.id > 0 AND u.deleted = 0");
		return $announcements->count;
	}
}

function theme_get_new_alerts($type = ''){
	global $DB, $USER;
	if ($type == ''){
		$alerts = $DB->get_records_sql("SELECT a.*, c.fullname FROM {local_nots_alerts} a
                                                LEFT JOIN {course} c ON c.id = a.courseid 
                                            WHERE a.userid = $USER->id 
                                        ORDER BY a.new DESC, a.timecreated DESC LIMIT 5");
		return $alerts;
	} else {
		$alerts = $DB->get_record_sql("SELECT COUNT(n.id) as count 
											FROM {local_nots_alerts} n 
										WHERE n.userid = $USER->id AND n.new = 1 
											ORDER BY n.timecreated DESC");
		return $alerts->count;
	}
}

function theme_print_notifications() {
    global $CFG;
    $output = '';
    
    $output .= html_writer::start_tag('div', array('class'=>'dropdown-menu notification-toggle', 'aria-labelledby'=>'notification-center', 'role'=>'menu'));
    $output .= html_writer::start_tag('div', array('class'=>'notification-panel'));
    $output .= html_writer::start_tag('div', array('class'=>'scroll-wrapper notification-body scrollable', 'style'=>'position: relative;'));
    $output .= html_writer::start_tag('div', array('class'=>'notification-body'));
    $output .= theme_get_notifications();
    $output .= html_writer::end_tag('div');                                
    $output .= html_writer::start_tag('div', array('class'=>'notification-footer text-center clearfix'));
    //$output .= html_writer::link($CFG->wwwroot.'/local/sb_announcements/index.php', 'Read all notifications');
    $output .= html_writer::link($CFG->wwwroot.'/local/sb_announcements/index.php', '<i class="fa fa-circle-o"></i>', array('title'=>'Refresh', 'onclick'=>'alertsRefresh();', 'class'=>'portlet-refresh text-black pull-right'));               
    $output .= html_writer::end_tag('div');
    $output .= html_writer::end_tag('div');
    $output .= html_writer::end_tag('div');
    $output .= html_writer::end_tag('div');
    return $output;
}


function theme_get_notifications() {
    global $DB, $CFG, $USER;
    $output = ''; $items = array();
    
    $announcements = theme_get_new_announcements();
    $alerts = theme_get_new_alerts();
    
    if (count($announcements) or count($alerts)){
        if (count($announcements)){
            foreach ($announcements as $item){
                $item->atype = 'announcement';
                $items[$item->startdate][] = $item;
            }
        }
        if (count($alerts)){
            foreach ($alerts as $item){
                $item->atype = 'alert';
                $items[$item->timecreated][] = $item;
            }
        }
        ksort($items);
    }
    
    if (count($items)){
        foreach($items as $date=>$items_array){
            foreach ($items_array as $item){
                $output .= html_writer::start_tag('div', array('class'=>'notification-item unread clearfix'));
                $output .= html_writer::start_tag('div', array('class'=>'heading'));
                
                if ($item->atype == 'alert'){
                    $output .= html_writer::start_tag('a', array('class'=>'text-complete pull-left text-danger', 'href'=>'#'));
                    $output .= html_writer::tag('i', '', array('class'=>'fa fa-bullhorn m-r-10','title'=>'Alert'));
                    $output .= html_writer::tag('span', $item->title, array('class'=>'bold'));
                    $output .= html_writer::end_tag('a');
                    $output .= html_writer::start_tag('div', array('class'=>'pull-right'));
                    $output .= html_writer::start_tag('div', array('class'=>'d16 circular inline m-r-10 toggle-more-details'));
                    $output .= html_writer::tag('i', '', array('class'=>'fa fa-angle-left', 'onclick'=>'alertsToggle(this);'));
                    $output .= html_writer::end_tag('div');
                    $output .= html_writer::tag('span', date('M d, h:i a', $date), array('class'=>'time'));
                    $output .= html_writer::end_tag('div');
                } else {
                    $output .= html_writer::start_tag('a', array('class'=>'text-complete pull-left '.(($item->color != '') ? 'text-'.$item->color : ''), 'href'=>'#'));
                    $output .= html_writer::tag('i', '', array('class'=>'fa fa-bell-o m-r-10','title'=>'Announcements'));
                    $output .= html_writer::tag('span', $item->atitle, array('class'=>'bold'));
                    $output .= html_writer::tag('span', fullname($item), array('class'=>'m-l-10'));
                    $output .= html_writer::end_tag('a');
                    $output .= html_writer::start_tag('div', array('class'=>'pull-right'));
                    $output .= html_writer::start_tag('div', array('class'=>'d16 circular inline m-r-10 toggle-more-details'));
                    $output .= html_writer::tag('i', '', array('class'=>'fa fa-angle-left', 'onclick'=>'alertsToggle(this);'));
                    $output .= html_writer::end_tag('div');
                    $output .= html_writer::tag('span', date('M d, h:i a', $date), array('class'=>'time'));
                    $output .= html_writer::end_tag('div');
                }
                
                $output .= html_writer::start_tag('div', array('class'=>'more-details'));
                $output .= html_writer::start_tag('div', array('class'=>'more-details-inner'));
                $output .= html_writer::tag('h5', $item->body, array('class'=>'semi-bold fs-14'));
                $output .= html_writer::start_tag('p', array('class'=>'small hint-text'));
                if ($item->fullname){
                    $output .= $item->fullname;
                }
                $output .= html_writer::end_tag('p');
                $output .= html_writer::end_tag('div');
                $output .= html_writer::end_tag('div');
                $output .= html_writer::end_tag('div');
                
                $output .= html_writer::start_tag('div', array('class'=>'option', 'data-toggle'=>'tooltip', 'data-placement'=>'left', 'data-original-title'=>'mark as read'));
                if ($item->atype == 'alert')
                    $output .= html_writer::tag('a', '', array('class'=>'mark', 'title'=>'Mark as read', 'onclick'=>"alertsMark('$item->atype',$item->id);"));
                else
                    $output .= html_writer::tag('a', '', array('class'=>'mark', 'title'=>'Mark as read', 'onclick'=>"alertsMark('$item->atype',$item->aid);"));
                $output .= html_writer::end_tag('div');
                
                $output .= html_writer::end_tag('div');
                //print_object($item);
            }
        }    
    } else {
        $output .= html_writer::tag('div', 'You do not have new notifications', array('class'=>'alert alert-success'));
    }
    return $output;
}

function theme_talentquest_lang_menu() {
    global $USER, $DB;
    $output = '';
    $options_lang =  get_string_manager()->get_list_of_translations(false);
    
    $langs = array();
    $settings = $DB->get_records('local_manager_langs');
    if (count($settings)){
        foreach ($settings as $setting){
            $langs[$setting->name] = $setting->state;
        }
    }
    
    if (count($options_lang)){
        $output .= html_writer::start_tag('ul', array('class'=>'moodle-lang-box'));
        foreach ($options_lang as $lang_code=>$lang_name){
            if (isset($langs[$lang_code]) and $langs[$lang_code] == 0) continue;
            
            if ($lang_code == $USER->lang)
                $lang_name .= html_writer::tag('i','',array('class'=>'fa fa-check'));

            $output .= html_writer::tag('li',html_writer::link('javascript:void(0);',$lang_name,array('onclick'=>'changeLang(this);','value'=>$lang_code)));
        }
        $output .= html_writer::end_tag('ul');
    }
    
    return $output;
}

function theme_talentquest_timezone_selector() {
    global $USER, $CFG;
    $output = '';
    
    if (!isset($CFG->forcetimezone) or (isset($CFG->forcetimezone) and $CFG->forcetimezone == 99)) {
        $choices = core_date::get_list_of_timezones($USER->timezone, true);
        
        if (count($choices)){
            $output .= html_writer::start_tag('div', array('class'=>'timezone-selector-box'));
            $output .= html_writer::tag('div', get_string('timezoneheader', 'theme_talentquest'), array('class'=>'sidebar-header'));
            $output .= html_writer::start_tag('div', array('class'=>'timezone-selector'));
            $output .= html_writer::start_tag('select', array('onchange'=>'changeTimezone(this);'));
                foreach ($choices as $id=>$name){
                    $params = array('value'=>$id);
                    if ($id == $USER->timezone) $params['selected'] = 'selected';
                    $output .= html_writer::tag('option', $name, $params);
                }
            
            $output .= html_writer::end_tag('select');
            $output .= html_writer::end_tag('div');
            $output .= html_writer::end_tag('div');
        }
    }
    
    return $output;
}

function theme_talentquest_pagetitle(){
    global $CFG, $COURSE, $PAGE, $OUTPUT, $THEME;
    
    $title = $THEME->theme_settings->system_name;
    
    /*if (isset($PAGE->cm->id) and !empty($PAGE->cm->modname)){
        $modname = get_string('pluginname', 'mod_'.$PAGE->cm->modname);
        $title = get_string('details', 'theme_talentquest', $modname);
    } elseif (isset($PAGE->course->id) and $PAGE->course->id > 1){
        $title = get_string('coursedetails', 'theme_talentquest');
    } elseif ($PAGE->pagetype == 'my-index'){
        $title = get_string('learningdashboard', 'theme_talentquest');
    } else {
        $title = $PAGE->heading;
    }*/
    
    return $title;
}


function theme_talentquest_set_systemcss($css, $systemcss) {
    $tag = '[[setting:systemcss]]';
    
    $replacement = $systemcss;
    if (is_null($replacement)) {
        $replacement = '';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

/**
 * All theme functions should start with theme_talentquest_
 * @deprecated since 2.5.1
 */
function talentquest_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_talentquest_
 * @deprecated since 2.5.1
 */
function talentquest_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_talentquest_
 * @deprecated since 2.5.1
 */
function talentquest_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

// generate system styles
function theme_talentquest_get_systemstyles(){
    global $DB, $CFG;
    $css = '';
    
    $styles = $DB->get_records('local_manager_styles', array('state'=>1));
    if (count($styles)){
        foreach ($styles as $style){
            if ($style->name == 'color1' and $style->value != ''){
                $css .= "body header > .inner {background-color: #$style->value; border-color: #$style->value;}";
            }
            if ($style->name == 'color2' and $style->value != '') {
                $css .= "body header .user .user-info {color: #$style->value;} ";
            }
            if ($style->name == 'color3' and $style->value != '') {
                $css .= "body .second-header {background-color: #$style->value;} ";
            }
            if ($style->name == 'color4' and $style->value != '') {
                $css .= "body .second-header .left {color: #$style->value;} ";
            }
            if ($style->name == 'color5' and $style->value != '') {
                $css .= "body .second-header .right {background-color: #$style->value;} ";
            }
            if ($style->name == 'color6' and $style->value != '') {
                $css .= "body .second-header .notification-list li a.header-links {color: #$style->value;} ";
            }
            if ($style->name == 'color7' and $style->value != '') {
                $css .= "body .second-header .right form input {background-color: #$style->value; border-color: #$style->value;} ";
            }
            if ($style->name == 'color8' and $style->value != '') {
                $css .= "body .second-header .right form input {color: #$style->value;} ";
            }
            if ($style->name == 'color9' and $style->value != '') {
                $css .= "body .head .pathway {background-color: #$style->value;} ";
            }
            if ($style->name == 'color10' and $style->value != '') {
                $css .= "body .head .pathway .breadcrumb-nav li {color: #$style->value;} ";
            }
            if ($style->name == 'color11' and $style->value != '') {
                $css .= "body .head .pathway .breadcrumb-nav a {color: #$style->value;} ";
            }
            if ($style->name == 'color12' and $style->value != '') {
                $css .= "body .head .pathway .breadcrumb-nav a:hover {color: #$style->value;} ";
            }
            if ($style->name == 'color13' and $style->value != '') {
                $css .= "body footer {border-color: #$style->value;} ";
            }
            if ($style->name == 'color14' and $style->value != '') {
                $css .= "body footer .copyright {color: #$style->value;} ";
                $css .= "body footer #custom_menu_1 .yui3-menuitem:not(:last-child) {border-color: #$style->value;} ";
            }
            if ($style->name == 'color15' and $style->value != '') {
                $css .= "body footer #custom_menu_1 .yui3-menuitem-content {color: #$style->value;} ";
                $css .= "body footer .right a.ng-binding {color: #$style->value;} ";
            }
            if ($style->name == 'color16' and $style->value != '') {
                $css .= "body footer #custom_menu_1 .yui3-menuitem-content:hover {color: #$style->value;} ";
                $css .= "body footer .right a.ng-binding:hover {color: #$style->value;} ";
            }
            if ($style->name == 'color17' and $style->value != '') {
                $css .= "body section.menu-sidebar {background-color: #$style->value;} ";
            }
            if ($style->name == 'color18' and $style->value != '') {
                $css .= "body .user-left_menu li.dashboard a {background-color: #$style->value;} ";
                $css .= "body .user-left_menu .active a, body .user-left_menu a:hover {background-color: #$style->value;} ";
            }
            if ($style->name == 'color19' and $style->value != '') {
                $css .= "body .user-left_menu li a {color: #$style->value !important;} ";
            }
            if ($style->name == 'color20' and $style->value != '') {
                $css .= "body .user-left_menu li a:hover, body .user-left_menu li a:hover i {color: #$style->value !important;} ";
                $css .= "body .user-left_menu li.dashboard, body .user-left_menu li.dashboard a i {color: #$style->value !important;} ";
                $css .= "body .user-left_menu .active a, body .user-left_menu .active a i, body .user-left_menu a:hover, body .user-left_menu a:hover i {color: #$style->value !important;} ";
            }
            if ($style->name == 'color21' and $style->value != '') {
                $css .= "body section.lang-sidebar {background-color: #$style->value;} ";
                $css .= "body section.left-sidebar {background-color: #$style->value;} ";
            }
            if ($style->name == 'color22' and $style->value != '') {
                $css .= "body .sidebar-header {color: #$style->value; border-color: #$style->value;} ";
                $css .= "body .sidebar-header {color: #$style->value; border-color: #$style->value;} ";
            }
            if ($style->name == 'color23' and $style->value != '') {
                $css .= "body .admin-menu li a {color: #$style->value !important;} ";
                $css .= "body .admin-menu .toggle-menu {border-color: #$style->value;} ";
                $css .= "body section.lang-sidebar .moodle-lang-box li a {color: #$style->value;} ";
                $css .= "body section.left-sidebar .block .block_tree .tree_item.branch:before, body section.left-sidebar .block .block_tree .collapsed .tree_item.branch:before {color: #$style->value;} ";
                $css .= "body section.left-sidebar .block .content, body section.left-sidebar .block .content a {color: #$style->value;} ";
            }
            if ($style->name == 'color24' and $style->value != '') {
                $css .= "body .admin-menu .toggle-menu li a:hover {background-color: #$style->value;} ";
                $css .= "body section.left-sidebar .block_settings.block .content .tree_item a:hover {background-color: #$style->value;} ";
            }
            if ($style->name == 'color25' and $style->value != '') {
                $css .= "body .admin-menu .toggle-menu li a:hover {color: #$style->value !important;} ";
                $css .= "body section.left-sidebar .block .content a:hover {color: #$style->value;} ";
                $css .= "body section.left-sidebar .block_settings.block .content .tree_item a:hover {color: #$style->value;} ";
                $css .= "body section.lang-sidebar .moodle-lang-box li a:hover {color: #$style->value;} ";
            }
            if ($style->name == 'color26' and $style->value != '') {
                $css .= "html, body {background-color: #$style->value;} ";
                $css .= "body section.page {background-color: #$style->value;} ";
            }
            if ($style->name == 'color27' and $style->value != '') {
                $css .= "body .page > div > .content {background-color: #$style->value;} ";
                $css .= "body.path-my .page #region-main {background-color: transparent !important;} ";
                $css .= "body.path-my .page #region-main aside {background-color: #$style->value !important;} ";
            }
            if ($style->name == 'color28' and $style->value != '') {
                $css .= "body .page .side-post .block {background-color: #$style->value;} ";
                $css .= "#block-region-content-left .block, #block-region-content-right .block {background-color: #$style->value;} ";
                $css .= "#block-region-content .block {background-color: #$style->value;} ";
                $css .= "body .block .header {background-color: #$style->value;} ";
                $css .= "body.path-my .page #region-main aside {background-color: #$style->value !important;} ";
                $css .= "body .frontpage-courses-list .courses .coursebox {background-color: #$style->value !important;} ";
                $css .= "body .nav-tabs > .active > a {background-color: transparent !important;} ";
            }
            if ($style->name == 'color29' and $style->value != '') {
                $css .= "body .side-post .block .header h2 {color: #$style->value;} ";
                $css .= "body .dashboard-calendar-header {color: #$style->value;} ";
                $css .= "body .block_sb_leaderboard.block .header h2 {color: #$style->value;} ";
                $css .= "body .block_sb_badges.block .header h2 {color: #$style->value;} ";
            }
            if ($style->name == 'color30' and $style->value != '') {
                $color30_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body button, body .button, body input.form-submit, body input[type="button"], body input[type="submit"], body input[type="reset"], .head .pathway input[type="submit"] {background-color: rgba('.$color30_rgb[0].', '.$color30_rgb[1].', '.$color30_rgb[2].', 1); border-color: #'.$style->value.'; } ';
                $css .= 'body button:hover, body .button:hover, body input.form-submit:hover, body input[type="button"]:hover, body input[type="submit"]:hover, body input[type="reset"]:hover, .head .pathway input[type="submit"]:hover {background-color: rgba('.$color30_rgb[0].', '.$color30_rgb[1].', '.$color30_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
                $css .= 'body button:focus, body .button:focus, body input.form-submit:focus, body input[type="button"]:focus, body input[type="submit"]:focus, body input[type="reset"]:focus, .head .pathway input[type="submit"]:focus {background-color: rgba('.$color30_rgb[0].', '.$color30_rgb[1].', '.$color30_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color31' and $style->value != '') {
                $css .= 'body button, body .button, body input.form-submit, body input[type="button"], body input[type="submit"], body input[type="reset"], .head .pathway input[type="submit"] {color: #'.$style->value.'; } ';
                $css .= 'body button:hover, body .button:hover, body input.form-submit:hover, body input[type="button"]:hover, body input[type="submit"]:hover, body input[type="reset"]:hover, .head .pathway input[type="submit"]:hover {color: #'.$style->value.';} ';
                $css .= 'body button:focus, body .button:focus, body input.form-submit:focus, body input[type="button"]:focus, body input[type="submit"]:focus, body input[type="reset"]:focus, .head .pathway input[type="submit"]:focus {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color32' and $style->value != '') {
                $color32_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body .btn, body .section-modchooser-link .section-modchooser-text {background-color: rgba('.$color32_rgb[0].', '.$color32_rgb[1].', '.$color32_rgb[2].', 1); border-color: #'.$style->value.'; } ';
                $css .= 'body .btn:hover, body .section-modchooser-link .section-modchooser-text:hover {background-color: rgba('.$color32_rgb[0].', '.$color32_rgb[1].', '.$color32_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
                $css .= 'body .btn:focus, body .section-modchooser-link .section-modchooser-text:focus {background-color: rgba('.$color32_rgb[0].', '.$color32_rgb[1].', '.$color32_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color33' and $style->value != '') {
                $css .= 'body .btn, body .section-modchooser-link .section-modchooser-text {color: #'.$style->value.'; } ';
                $css .= 'body .btn:hover, body .section-modchooser-link .section-modchooser-text:hover {color: #'.$style->value.';} ';
                $css .= 'body .btn:focus, body .section-modchooser-link .section-modchooser-text:focus {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color34' and $style->value != '') {
                $color34_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body .btn.btn-warning {background-color: rgba('.$color34_rgb[0].', '.$color34_rgb[1].', '.$color34_rgb[2].', 1); border-color: #'.$style->value.'; } ';
                $css .= 'body .btn.btn-warning:hover {background-color: rgba('.$color34_rgb[0].', '.$color34_rgb[1].', '.$color34_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
                $css .= 'body .btn.btn-warning:focus {background-color: rgba('.$color34_rgb[0].', '.$color34_rgb[1].', '.$color34_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color35' and $style->value != '') {
                $css .= 'body .btn.btn-warning {color: #'.$style->value.'; } ';
                $css .= 'body .btn.btn-warning:hover {color: #'.$style->value.';} ';
                $css .= 'body .btn.btn-warning:focus {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color36' and $style->value != '') {
                $color36_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body .btn.btn-success {background-color: rgba('.$color36_rgb[0].', '.$color36_rgb[1].', '.$color36_rgb[2].', 1); border-color: #'.$style->value.'; } ';
                $css .= 'body .btn.btn-success:hover {background-color: rgba('.$color36_rgb[0].', '.$color36_rgb[1].', '.$color36_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
                $css .= 'body .btn.btn-success:focus {background-color: rgba('.$color36_rgb[0].', '.$color36_rgb[1].', '.$color36_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color37' and $style->value != '') {
                $css .= 'body .btn.btn-success {color: #'.$style->value.'; } ';
                $css .= 'body .btn.btn-success:hover {color: #'.$style->value.';} ';
                $css .= 'body .btn.btn-success:focus {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color38' and $style->value != '') {
                $color38_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body .btn.btn-danger {background-color: rgba('.$color38_rgb[0].', '.$color38_rgb[1].', '.$color38_rgb[2].', 1); border-color: #'.$style->value.'; } ';
                $css .= 'body .btn.btn-danger:hover {background-color: rgba('.$color38_rgb[0].', '.$color38_rgb[1].', '.$color38_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
                $css .= 'body .btn.btn-danger:focus {background-color: rgba('.$color38_rgb[0].', '.$color38_rgb[1].', '.$color38_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color39' and $style->value != '') {
                $css .= 'body .btn.btn-danger {color: #'.$style->value.'; } ';
                $css .= 'body .btn.btn-danger:hover {color: #'.$style->value.';} ';
                $css .= 'body .btn.btn-danger:focus {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color40' and $style->value != '') {
                $css .= 'body .nav-tabs-simple > li:after {background-color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color41' and $style->value != '') {
                $css .= 'body .head .settings-bar {background-color: #'.$style->value.'; } ';
                $css .= 'body .head .settings-bar li a {border-color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color42' and $style->value != '') {
                $css .= 'body .head .settings-bar li a {color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color43' and $style->value != '') {
                $color43_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body .head .settings-bar li a:hover {background-color: rgba('.$color43_rgb[0].', '.$color43_rgb[1].', '.$color43_rgb[2].', 0.8); border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color44' and $style->value != '') {
                $css .= 'body .head .settings-bar li a:hover {color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color45' and $style->value != '') {
                $css .= 'body a, body img {color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color46' and $style->value != '') {
                $css .= 'body a:hover, body img:hover {color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color47' and $style->value != '') {
                $css .= 'body .moodle-actionmenu.show[data-enhanced] .menu {background-color: #'.$style->value.';border-color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color48' and $style->value != '') {
                $css .= 'body .moodle-actionmenu.show[data-enhanced] .menu a {background-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color49' and $style->value != '') {
                $css .= 'body .moodle-actionmenu.show[data-enhanced] .menu a:hover {background-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color50' and $style->value != '') {
                $css .= 'body .moodle-actionmenu.show[data-enhanced] .menu a:hover {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color51' and $style->value != '') {
                $css .= 'body .single-section h3.sectionname, body .course-content ul li.section .content .sectionname {background-color: #'.$style->value.'; border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color52' and $style->value != '') {
                $css .= 'body .single-section h3.sectionname, body .course-content ul li.section .content .sectionname {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color53' and $style->value != '') {
                $css .= 'body .single-section.single-section h3.sectionname, body .course-content ul li.section.current .sectionname {background-color: #'.$style->value.'; border-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color54' and $style->value != '') {
                $css .= 'body .single-section.single-section h3.sectionname, body .course-content ul li.section.current .sectionname {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color55' and $style->value != '') {
                $css .= 'body .course-content .section .activity .contentwithoutlink, body .section .activity .activityinstance a {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color56' and $style->value != '') {
                $css .= 'body .course-content .section .activity .contentwithoutlink:hover, body .section .activity .activityinstance a:hover {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color57' and $style->value != '') {
                $css .= 'body.path-login.notloggedin .page>div>.content {background-color: #'.$style->value.'; border-right: 1px solid #'.$style->value.';} ';
            }
            if ($style->name == 'color58' and $style->value != '') {
                $css .= 'body.pagelayout-login .login-welcome-text {background-color: #'.$style->value.';} ';
            }
            if ($style->name == 'color59' and $style->value != '') {
                $css .= 'body.pagelayout-login .login-welcome-text {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color60' and $style->value != '') {
                $css .= 'body.pagelayout-login .loginbox label {color: #'.$style->value.';} ';
            }
            if ($style->name == 'color61' and $style->value != '') {
                $color61_rgb = theme_talentquest_hex2rgba($style->value);
                $css .= 'body.pagelayout-login .loginbox .loginpanel input[type="submit"], body.pagelayout-login .loginbox .loginpanel input[type="submit"]{background-color: rgba('.$color61_rgb[0].', '.$color61_rgb[1].', '.$color61_rgb[2].', 1); border-color: #'.$style->value.'; } ';
                $css .= 'body.pagelayout-login .loginbox .loginpanel input[type="submit"]:hover, body.pagelayout-login .loginbox .loginpanel input[type="submit"]:focus {background-color: rgba('.$color61_rgb[0].', '.$color61_rgb[1].', '.$color61_rgb[2].', 0.8); border-color: #'.$style->value.'; } ';
            }
            if ($style->name == 'color62' and $style->value != '') {
                $css .= 'body.pagelayout-login .loginbox .loginpanel input[type="submit"], body.pagelayout-login .loginbox .loginpanel input[type="submit"] {color: #'.$style->value.';} ';
                $css .= 'body.pagelayout-login .loginbox .loginpanel input[type="submit"]:hover, body.pagelayout-login .loginbox .loginpanel input[type="submit"]:focus {color: #'.$style->value.';} ';
            }
        }
    }
    
    $loginbackground  = theme_talentquest_file(4, 'loginbackground');
    if (!empty($loginbackground)){
        $css .= 'body.pagelayout-login {background: rgba(0, 0, 0, 0) url('.$loginbackground.') no-repeat scroll 0 0 / cover;}';
    }
    
    return $css;
}

function theme_talentquest_get_logo() {
	global $SITE, $CFG, $THEME;
	$logo  = theme_talentquest_file(1, 'logo');
    
	if (is_object($logo)){
		return '<img src="'.$logo->out().'" title="'.$THEME->theme_settings->system_name.'" alt="'.$THEME->theme_settings->system_name.'"/>';
	}else{
		return '<span class="site-name">'.$THEME->theme_settings->system_name.'</span>';
	}
}

function theme_talentquest_file($id, $name, $component = 'theme_talentquest') {
	global $DB, $CFG, $USER, $SITE;

	$fs = get_file_storage();
	$files = $fs->get_area_files(context_system::instance()->id, $component, $name, $id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
	if (count($files) < 1) {
		return '';
	} else {
		$file = reset($files);
		unset($files);
		$path = '/'.context_system::instance()->id.'/'.$component.'/'.$name.'/'.$id.'/'.$file->get_filename();
		return moodle_url::make_file_url('/pluginfile.php', $path);
	}
}

function theme_talentquest_generate_favicon($type = ''){
    global $OUTPUT;
    
    if ($type == 'faviconpng'){
        $favicon = theme_talentquest_file(3, 'faviconpng');
    } else {
        $favicon = theme_talentquest_file(2, 'favicon');
    }
    
    if (is_object($favicon)){
        return $favicon->out();
    } else {
        return $OUTPUT->favicon();
    }
}

function theme_talentquest_layout_settings(){
    global $OUTPUT, $DB, $PAGE, $THEME;
    $settings = new stdClass();
    $styles = $DB->get_records('local_manager_styles', array('state'=>1));
    if (count($styles)){
        foreach ($styles as $style){
            $settings->{$style->name} = (!empty($style->value)) ? $style->value : $style->defaultvalue;
        }
    }
    $THEME->theme_settings = $settings;
    
    theme_talentquest_check_editing();
    
    return $settings;
}

