jQuery(window).ready(function(){
    
    if (jQuery('input.ColorPicker').length){
        jQuery('input.ColorPicker').each(function(e){
            var color = jQuery(this).val();
            var obj = jQuery(this);
            jQuery(this).ColorPicker({
                onChange: function (hsb, hex, rgb) {
                    obj.val(hex);
                },
                onBeforeShow: function () {
                    if (color.length){
                        obj.ColorPickerSetColor(color);
                    }
                }
            });
        });   
    }
    
    if (jQuery('.multi-select').length){
        $(".multi-select select").multipleSelect({
            filter: true
        });    
    }
    
    if (jQuery('input[name="search"][type="text"]:not(#tq-search), input[type="search"]').length){
        jQuery('input[name="search"][type="text"]:not(#tq-search), input[type="search"]').each(function(e){
            var input = jQuery(this);
            var el = '<span class="search-actions"><i class="fa fa-times" style="'+((input.val().length)?"":"display:none;")+'" onclick="searchProcess(this, 0);"></i><i class="fa fa-search" style="'+((input.val().length)?"display:none;":"")+'" onclick="searchProcess(this, 1);"></i></span>';
            
            input.after(el);
            
            input.keyup(function(e){
                if (input.val().length){
                    input.parent().find('.search-actions .fa-times').show();
                    input.parent().find('.search-actions .fa-search').hide();
                } else {
                    input.parent().find('.search-actions .fa-times').hide();
                    input.parent().find('.search-actions .fa-search').show();
                }
            });
        });
    }
    
    if (jQuery('.block_settings.block').length){
        jQuery('.site_navigation').show();
    }
    
    if (jQuery(".que .answer div input[type='radio']").length){
        jQuery(".que .answer div input[type='radio']").each(function(e){
            if (jQuery(this).prop( "checked")){
                jQuery(this).parent().addClass('active');    
            }
        });
    }
    
    jQuery(".que.truefalse .answer div, .que.calculatedmulti .answer div, .que.multichoice .answer div").click(function(e){
        if (!jQuery(this).find('input[type="radio"]').prop( "disabled")){
            jQuery(this).parent().find('div').removeClass('active');
            jQuery(this).addClass('active');
            jQuery(this).parent().find('input[type="radio"]').prop( "checked", false );
            jQuery(this).find('input[type="radio"]').prop( "checked", true );
        }
    });

    jQuery("section.left-sidebar .block .header h2 a, section.course-sidebar .block .header h2 a").click(function(e){
        e.preventDefault();
        var container = jQuery(jQuery(jQuery(jQuery(this).parent()).parent()).parent()).parent();
        var content = jQuery(container).children('.content');
        jQuery(container).toggleClass('hidden');
        if(jQuery(container).hasClass('hidden')) {
            jQuery(content).hide(300);
        }else{
            jQuery(content).show(300);
        }
    });
    jQuery("section.course-sidebar .block .header h2 a").click(function(e){
        if(!jQuery('section.course-sidebar').hasClass('open')){
            toggleCourseSide();
        }
    });

    $(document).click(function(event) {
        
        var target = event.target;

        if (!$(target).parents().is('.left-sidebar') && !$(target).parents().is('.menu-sidebar') && jQuery('.left-sidebar').hasClass('open') && !jQuery('.left-sidebar').hasClass('pinned')) {
            closeSidePre();
        }
        
        if (!$(target).parents().is('.lang-sidebar') && !$(target).parents().is('.menu-sidebar') && jQuery('.lang-sidebar').hasClass('open')) {
            closeSideLanguage();
        }
        
        /*if($('section.lang-sidebar').hasClass('open')){
            if (!$(target).closest("section.lang-sidebar").length && !$(target).closest(".user-left_menu li.language-menu").length){
                toggleSideLanguage();
            }
        }

        if ($(target).closest("section.menu-sidebar .settings").length || $(target).closest("section.left-sidebar").length)
            return;*/

        /*if($('section.left-sidebar').hasClass('open'))
            toggleSidePre();*/
    });

    //jQuery("section.course-sidebar .block.block_fake .content").hide(300);
    jQuery("section.course-sidebar .block.block_fake").addClass('hidden');
    //jQuery("section.left-sidebar .block:not(.block_talentquest_menu) .content").hide(0);
    //jQuery("section.left-sidebar .block:not(.block_talentquest_menu)").addClass('hidden');

    //jQuery("section.left-sidebar aside").scrollbar();
    
    jQuery('.fix-sidebar').click(function (e) {
        e.preventDefault();
        jQuery('.left-sidebar').toggleClass('fixed');
        var icon = jQuery(this).children('.fa');
        jQuery(icon).toggleClass('fa-circle-o');
        jQuery(icon).toggleClass('fa-dot-circle-o');

            M.util.set_user_preference('fix-sidebar', jQuery('.left-sidebar').hasClass('fixed')?1:0);

    });
    jQuery('section.left-sidebar .menu .dropdown-toggle').click(function (e) {
        e.preventDefault();
        jQuery('section.left-sidebar > .dropdown-menu').toggleClass('show');
        jQuery('section.left-sidebar .menu .dropdown').toggleClass('open');
    });
    jQuery('section.left-sidebar').mouseleave(function () {
        if(jQuery('section.left-sidebar > .dropdown-menu').hasClass('show') && !jQuery('.left-sidebar').hasClass('fixed')){
            jQuery('section.left-sidebar > .dropdown-menu').toggleClass('show');
            jQuery('section.left-sidebar .menu .dropdown').toggleClass('open');
        }
    });

    jQuery('.search-link,.search-box .close').click(function (e) {
        e.preventDefault();
        jQuery('.search-box').toggleClass('open');
    });

    jQuery('.right-sidebar .top-content label').click(function (e) {
        var element = jQuery(this).attr('for');
        jQuery('.right-sidebar .content-box > div').removeClass('open');
        jQuery('.right-sidebar .content-box > div.'+element).addClass('open');

        jQuery('.right-sidebar .top-content label').removeClass('active');
        jQuery(this).addClass('active');
    });

    jQuery('header .quickview-link, .right-sidebar .top-content .close').click(function (e) {
        e.preventDefault();
        jQuery('section.right-sidebar').toggleClass('open');
        jQuery('.right-sidebar .top-content label:first-child').click();
    });
    jQuery('section.right-sidebar .content-box .notes .first-content > ul li').click(function (e) {
        e.preventDefault();
        var id = jQuery(this).attr('data-id');
        jQuery('section.right-sidebar .content-box .notes .first-content').addClass('left');
        jQuery('section.right-sidebar .content-box .notes .second-content').removeClass('right');
        jQuery('section.right-sidebar .content-box .notes .second-content div[data-id="'+id+'"]').addClass('show');
    });
    jQuery('section.right-sidebar .content-box .notes .second-content .toolbar ul>li:first-child a').click(function (e) {
        e.preventDefault();
        jQuery('section.right-sidebar .content-box .notes .first-content').removeClass('left');
        jQuery('section.right-sidebar .content-box .notes .second-content').addClass('right');
        jQuery('section.right-sidebar .content-box .notes .second-content div').removeClass('show');
    });

    var number = 0;
    jQuery('input[type="checkbox"]:not(.hidden, .cmn-toggle)').each(function (e) {
        var id = jQuery(this).attr('id');
        if(id === undefined){
            number++;
            id = 'checkbox_'+number;
            jQuery(this).attr('id',id);
        }
        jQuery(this).after('<label for="'+id+'" class="checkbox"></label>');
    });
    $('.notification-panel').click(function(e){
        e.preventDefault();
        e.stopPropagation();
    });
    //jQuery('select').select2();
    
    jQuery('.section-modchooser-link').click(function () {
        jQuery('body').addClass('open-dialog');
        setTimeout(function () {
            jQuery('body').removeClass('lockscroll');
            jQuery('.open-dialog .moodle-dialogue-base .closebutton').click(function () {
                jQuery('.jschooser.open-dialog .moodle-dialogue-base .moodle-dialogue-lightbox').hide();
                jQuery('body').removeClass('open-dialog');
            });
        },500);
    });
});
$(window).load(function() {
    var number = 0;
    jQuery('.unmask input[type="checkbox"]').each(function (e) {
        var id = jQuery(this).attr('id');
        if(id === undefined){
            number++;
            id = 'checkbox_'+number;
            jQuery(this).attr('id',id);
        }
        jQuery(this).after('<label for="'+id+'" class="checkbox"></label>');
    });

    M.core.tooltip.prototype.alignpoints = ["tc", "cc"];
});
$(window).resize(function() {
    if($(window).width()<630 && $('.tab-content').hasClass('list-view')){
        toggleView();
    }
});

function searchProcess(obj, state){
    if (state == 0){
        jQuery(obj).parent().prev('input[name="search"][type="text"], input[type="search"]').val('');
        jQuery(obj).parent().prev('input[name="search"][type="text"], input[type="search"]').trigger('keyup').trigger('change');
        
        if (jQuery('.dataTable').length){
            var datatable = jQuery('.dataTable').dataTable();
            if (datatable.length){
                datatable.fnFilter(''); 
            }
        }
    }
    if (jQuery(obj).closest('form')){
        jQuery(obj).closest('form').submit();
    }
}

function toggleCourseNavigation(){
    jQuery(".block_kaltura_coursenav").toggleClass("open");
}

function togglePostSidebar(){
    jQuery("#page").toggleClass("bothside");
    jQuery(".toggle-post-side .fa").toggleClass("hidden");
    if (jQuery("#page").hasClass("bothside")){
        themeSetUserSettings('theme_right_sidebar', 1);
    } else {
        themeSetUserSettings('theme_right_sidebar', 0);
    }
}

function themeSetUserSettings(name, value){
    var url = jQuery('#site_wwwroot').attr('data-value');
    jQuery.get( url+'/theme/talentquest/ajax.php?action=set_user_preferences&name='+name+'&value='+value, function( data ) {});
}

function closeSidePre(){
    jQuery(".left-sidebar").removeClass("open");
    jQuery(".user-left_menu li").removeClass("active");
    if (jQuery(".left-sidebar").hasClass("pinned")){
        pinSidebar();
        jQuery(".left-sidebar").removeClass("open");
        jQuery(".user-left_menu li").removeClass("active");
    }
}

function toggleSidePre(mclass){
    if (jQuery(".left-sidebar").hasClass("pinned")){
        pinSidebar();
        jQuery(".left-sidebar").removeClass("open");
        jQuery(".user-left_menu li").removeClass("active");
    } else {
        if (jQuery(".user-left_menu li."+mclass).hasClass('active') && jQuery(".left-sidebar").hasClass('open')){
            jQuery(".user-left_menu li").removeClass("active");    
            jQuery(".left-sidebar").removeClass("open");
        } else if(!jQuery(".user-left_menu li."+mclass).hasClass('active') && jQuery(".left-sidebar").hasClass('open')){
            jQuery(".user-left_menu li").removeClass("active");
            jQuery(".user-left_menu li."+mclass).addClass("active");    
        } else {
            jQuery(".user-left_menu li").removeClass("active");
            jQuery(".left-sidebar").addClass("open");
            jQuery(".user-left_menu li."+mclass).addClass("active");
        }
    }
    if (mclass == 'manage'){
        jQuery(".left-sidebar").addClass('manage');
        jQuery(".left-sidebar").removeClass('settings');
    } else {
        jQuery(".left-sidebar").addClass('settings');
        jQuery(".left-sidebar").removeClass('manage');
    }
    
    if (jQuery(".lang-sidebar").hasClass('open')){
        closeSideLanguage();
    }
}

function toggleSideLanguage(){
    if (!jQuery(".lang-sidebar").hasClass('open') && !jQuery(".left-sidebar").hasClass('pinned') && jQuery(".left-sidebar").hasClass('open')){
        closeSidePre();
    }
    jQuery(".lang-sidebar").toggleClass("open");
    jQuery(".user-left_menu li.language-menu").toggleClass("active");   
}

function closeSideLanguage(){
    jQuery(".lang-sidebar").removeClass("open");
    jQuery(".user-left_menu li.language-menu").removeClass("active");   
}

function pinSidebar(){
    jQuery(".left-sidebar").toggleClass("pinned");
    if (!jQuery(".left-sidebar").hasClass("open")){
        jQuery(".left-sidebar").toggleClass("open");
    }
    
    if (jQuery(".left-sidebar").hasClass("pinned")){
        if (jQuery("section#page").hasClass('leftside')){
            jQuery("section#page").addClass("bothside");   
        }
        themeSetUserSettings('pin-sidebar', 1);
    } else {
        if (jQuery("section#page").hasClass('leftside')){
            jQuery("section#page").removeClass("bothside");
        }
        themeSetUserSettings('pin-sidebar', 0);
    }    
}

function toggleCourseCF(){
    jQuery(".course-custom-toggler .fa").toggleClass("hidden");
    jQuery(".course-custom-list").slideToggle();
}

function courseToggleCompletion(obj, url, state, newstate, title, newtitle){
    jQuery.get( url+'&completionstate='+newstate, function( data ) {
    });
    var newClick = 'courseToggleCompletion(this, "'+url+'", '+newstate+', '+state+', "'+newtitle+'", "'+title+'");';
    jQuery(obj).attr('title', newtitle);
    jQuery(obj).attr('onclick', newClick);
    if (newstate == 1){
        jQuery(obj).removeClass('not-completed');
        jQuery(obj).removeClass('pending');
        jQuery(obj).addClass('completed');
    } else {
        jQuery(obj).removeClass('completed');
        jQuery(obj).removeClass('pending');
        jQuery(obj).addClass('not-completed');
    }
}

function alertsToggle(element){
    $(element).toggleClass('rotate');
    var wrap = $($($(element).parent()).parent()).parent();
    $(wrap).toggleClass('open');
}
function alertsMark(type,id){
    jQuery.get( M.cfg.wwwroot+'/theme/talentquest/ajax.php?action=alert_mark&type='+type+'&id='+id, function( data ) {
        alertsRefresh();
    });
}
function alertsRefresh(){
    $('.notification-panel .notification-body .notification-body').append('<i class="fa fa-spin fa-spinner loading"></i>');
    jQuery.get( M.cfg.wwwroot+'/theme/talentquest/ajax.php?action=get_notifications', function( data ) {
        data = JSON.parse(data);
        $('.notification-panel .notification-body .notification-body').html(data.output);
    });
}

function alertsRefresh(){
    $('.notification-panel .notification-body .notification-body').append('<i class="fa fa-spin fa-spinner loading"></i>');
    jQuery.get( M.cfg.wwwroot+'/theme/talentquest/ajax.php?action=get_notifications', function( data ) {
        data = JSON.parse(data);
        $('.notification-panel .notification-body .notification-body').html(data.output);
    });
}

function forumToggleImportant(obj, forumid, discussionid){
    var state = 0;
    jQuery(obj).toggleClass('active');
    jQuery(obj).parent().parent().toggleClass('important');
    if (jQuery(obj).hasClass('active')){
        state = 1;    
    }
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/mod/forum/ajax.php?action=set-important&forumid='+forumid+'&discussionid='+discussionid +'&state='+state, function( data ) {
    });
}

function forumLike(obj, forumid, discussionid, postid){
    var state = 0;
    var likes = parseInt(jQuery(obj).find('span').text());
    jQuery(obj).toggleClass('liked');
    if (jQuery(obj).hasClass('liked')){
        state = 1;    
        likes++;
    } else {
        likes--;
    }
    jQuery(obj).find('span').text(likes)
    
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/mod/forum/ajax.php?action=set-like&forumid='+forumid+'&discussionid='+discussionid +'&postid='+postid+'&state='+state, function( data ) {
    });
}

function forumHelpful(obj, forumid, discussionid, postid){
    var state = 0;
    var count = parseInt(jQuery(obj).find('span').text());
    jQuery(obj).toggleClass('helpful');
    if (jQuery(obj).hasClass('helpful')){
        state = 1;    
        count++;
    } else {
        count--;
    }
    jQuery(obj).find('span').text(count)
    
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/mod/forum/ajax.php?action=set-helpful&forumid='+forumid+'&discussionid='+discussionid +'&postid='+postid+'&state='+state, function( data ) {
    });
}

function blogLike(obj, postid){
    var state = 0;
    var likes = parseInt(jQuery(obj).find('span').text());
    jQuery(obj).toggleClass('liked');
    if (jQuery(obj).hasClass('liked')){
        state = 1;    
        likes++;
    } else {
        likes--;
    }
    jQuery(obj).find('span').text(likes)
    
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/blog/ajax.php?action=set-like&postid='+postid+'&state='+state, function( data ) {
    });
}

function blogHelpful(obj, postid){
    var state = 0;
    var count = parseInt(jQuery(obj).find('span').text());
    jQuery(obj).toggleClass('helpful');
    if (jQuery(obj).hasClass('helpful')){
        state = 1;    
        count++;
    } else {
        count--;
    }
    jQuery(obj).find('span').text(count)
    
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/blog/ajax.php?action=set-helpful&postid='+postid+'&state='+state, function( data ) {
    });
}

function blogCommentLike(obj, commentid){
    var state = 0;
    var likes = parseInt(jQuery(obj).find('span').text());
    jQuery(obj).toggleClass('liked');
    if (jQuery(obj).hasClass('liked')){
        state = 1;    
        likes++;
    } else {
        likes--;
    }
    jQuery(obj).find('span').text(likes)
    
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/blog/ajax.php?action=set-comment-like&commentid='+commentid+'&state='+state, function( data ) {
    });
}

function blogCommentHelpful(obj, commentid){
    var state = 0;
    var count = parseInt(jQuery(obj).find('span').text());
    jQuery(obj).toggleClass('helpful');
    if (jQuery(obj).hasClass('helpful')){
        state = 1;    
        count++;
    } else {
        count--;
    }
    jQuery(obj).find('span').text(count)
    
    var newtitle = jQuery(obj).attr('data-newtitle');
    var title = jQuery(obj).attr('title');
    jQuery(obj).attr('data-newtitle', title);
    jQuery(obj).attr('title', newtitle);
    
    jQuery.get( M.cfg.wwwroot+'/blog/ajax.php?action=set-comment-helpful&commentid='+commentid+'&state='+state, function( data ) {
    });
}

function leaderboardLoad(){
    var contest = jQuery(".leaderboard-select").val();
    jQuery('.leaderboard-box').addClass('loader');
    jQuery('.leaderboard-box').html('<i class="fa fa-spin fa-spinner"></i>');
    jQuery('.leaderboard-box').load( M.cfg.wwwroot+'/local/gamification/ajax.php?action=load-lb&id='+contest, function() {
        jQuery('.leaderboard-box').removeClass('loader');
    });
}

function leaderboardSubmit(id){
    jQuery('.leaderboard-submit-btn').html('<i class="fa fa-spin fa-spinner"></i>');
    jQuery.get( M.cfg.wwwroot+'/local/gamification/ajax.php?action=submit&id='+id, function(data) {
        jQuery('.leaderboard-submit-btn').html(data);
        setTimeout(function() {
          jQuery(".leaderboard-submit-btn").remove();
        }, 3000);
    });
}

function changeLang(obj){
    var lang = jQuery(obj).attr('value');
    jQuery.get( M.cfg.wwwroot+'/theme/talentquest/ajax.php?action=set-user-lang&lang='+lang, function( data ) {
        location.reload();
    });
}

function changeTimezone(obj){
    var timezone = jQuery(obj).val();
    jQuery.get( M.cfg.wwwroot+'/theme/talentquest/ajax.php?action=set-user-timezone&timezone='+timezone, function( data ) {
        location.reload();
    });
}

function openCustomPopup(id, title, content){
    var output = '';
    output += '<div class="moodle-dialogue-base custom-popup-dialog" aria-hidden="false" id="'+id+'">';
        output += '<div style="" class="moodle-has-zindex chooserdialogue-course-modchooser yui3-widget yui3-panel moodle-dialogue yui3-widget-positioned yui3-widget-modal yui3-widget-stacked chooserdialogue yui3-dd-draggable moodle-dialogue-focused" tabindex="0">';
            output += '<div class="moodle-dialogue-wrap moodle-dialogue-content yui3-widget-stdmod" role="dialog">';
                output += '<div class="moodle-dialogue-hd yui3-widget-hd">'+title+'<span class="yui3-widget-buttons" ><button class="yui3-button closebutton" title="" onclick="closeCustomPopup(\''+id+'\');"></button></span></div>';
                    output += '<div class="moodle-dialogue-bd yui3-widget-bd">';
                    output += content;
                output += '</div>';
            output += '</div>';
        output += '</div>';
    output += '</div>';
    if (!jQuery('#'+id).length){
        jQuery('section#page').after(output);
    }
}

function closeCustomPopup(id){
    jQuery('#'+id).remove();
}
