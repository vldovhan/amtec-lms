$(window).ready(function(){
    
    if (jQuery('.ColorPicker').length){
    
        jQuery('.ColorPicker').each(function(e){
            var currentId = jQuery(this).attr('id');
            jQuery(this).ColorPicker({
                onShow: function (colpkr) {
                    jQuery(colpkr).fadeIn(500);
                    return false;
                },
                onHide: function (colpkr) {
                    jQuery(colpkr).fadeOut(500);
                    return false;
                },
                onChange: function (hsb, hex, rgb) {
                    jQuery('#'+currentId).val(hex);
                },
                onBeforeShow: function () {
                    if (this.value.length){
                        jQuery(this).ColorPickerSetColor(this.value);
                    }
                }			
            });
        });
    }
    
    if (jQuery('.block_settings.block').length){
        jQuery('.site_navigation').show();
    }
    
    if (jQuery(".que .answer div input[type='radio']").length){
        jQuery(".que .answer div input[type='radio']").each(function(e){
            if (jQuery(this).prop( "checked")){
                jQuery(this).parent().addClass('active');    
            }
        });
    }
    
    jQuery(".que.truefalse .answer div, .que.calculatedmulti .answer div, .que.multichoice .answer div").click(function(e){
        if (!jQuery(this).find('input[type="radio"]').prop( "disabled")){
            jQuery(this).parent().find('div').removeClass('active');
            jQuery(this).addClass('active');
            jQuery(this).parent().find('input[type="radio"]').prop( "checked", false );
            jQuery(this).find('input[type="radio"]').prop( "checked", true );
        }
    });

    jQuery("section.left-sidebar .block .header h2 a").click(function(e){
        e.preventDefault();
        var container = jQuery(jQuery(jQuery(jQuery(this).parent()).parent()).parent()).parent();
        var content = jQuery(container).children('.content');
        jQuery(container).toggleClass('hidden');
        if(jQuery(container).hasClass('hidden')) {
            jQuery(content).hide(300);
        }else{
            jQuery(content).show(300);
        }
    });
    jQuery("section.left-sidebar .block:not(.block_talentquest_menu) .content").hide(0);
    jQuery("section.left-sidebar .block:not(.block_talentquest_menu)").addClass('hidden');

    $("section.left-sidebar aside").scrollbar();
    
    $('.fix-sidebar').click(function (e) {
        e.preventDefault();
        $('.left-sidebar').toggleClass('fixed');
        var icon = jQuery(this).children('.fa');
        $(icon).toggleClass('fa-circle-o');
        $(icon).toggleClass('fa-dot-circle-o');

            M.util.set_user_preference('fix-sidebar', $('.left-sidebar').hasClass('fixed')?1:0);

    });
    $('section.left-sidebar .menu .dropdown-toggle').click(function (e) {
        e.preventDefault();
        $('section.left-sidebar > .dropdown-menu').toggleClass('show');
        $('section.left-sidebar .menu .dropdown').toggleClass('open');
    });
    $('section.left-sidebar').mouseleave(function () {
        if($('section.left-sidebar > .dropdown-menu').hasClass('show') && !$('.left-sidebar').hasClass('fixed')){
            $('section.left-sidebar > .dropdown-menu').toggleClass('show');
            $('section.left-sidebar .menu .dropdown').toggleClass('open');
        }
    });

    $('.search-link,.search-box .close').click(function (e) {
        e.preventDefault();
        $('.search-box').toggleClass('open');
    });

    $('.right-sidebar .top-content label').click(function (e) {
        var element = $(this).attr('for');
        $('.right-sidebar .content-box > div').removeClass('open');
        $('.right-sidebar .content-box > div.'+element).addClass('open');

        $('.right-sidebar .top-content label').removeClass('active');
        $(this).addClass('active');
    });

    $('header .quickview-link, .right-sidebar .top-content .close').click(function (e) {
        e.preventDefault();
        $('section.right-sidebar').toggleClass('open');
        $('.right-sidebar .top-content label:first-child').click();
    });
    $('section.right-sidebar .content-box .notes .first-content > ul li').click(function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        $('section.right-sidebar .content-box .notes .first-content').addClass('left');
        $('section.right-sidebar .content-box .notes .second-content').removeClass('right');
        $('section.right-sidebar .content-box .notes .second-content div[data-id="'+id+'"]').addClass('show');
    });
    $('section.right-sidebar .content-box .notes .second-content .toolbar ul>li:first-child a').click(function (e) {
        e.preventDefault();
        $('section.right-sidebar .content-box .notes .first-content').removeClass('left');
        $('section.right-sidebar .content-box .notes .second-content').addClass('right');
        $('section.right-sidebar .content-box .notes .second-content div').removeClass('show');
    });

    $('input[type="checkbox"]').each(function (e) {
        var id = $(this).attr('id');
        $(this).after('<label for="'+id+'" class="checkbox"></label>');
    });
    //$('select').select2();
});

function toggleCourseNavigation(){
    jQuery(".block_kaltura_coursenav").toggleClass("open");
}
