<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The embedded layout.
 *
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$theme_settings = theme_talentquest_layout_settings();
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$hassidemenu = $PAGE->blocks->region_has_content('side-menu', $OUTPUT);
if($PAGE->course->id == 1 and isset($USER->editing)){
	$editing = $USER->editing;
	$USER->editing = false;
	$hassidecourse = $PAGE->blocks->region_has_content('side-course', $OUTPUT);
	$USER->editing = $editing;
}else{
	$hassidecourse = $PAGE->blocks->region_has_content('side-course', $OUTPUT);
}

$pinned_sidebar = get_user_preferences('pin-sidebar',0,$USER);
$active_sidebar = get_user_preferences('active-sidebar', '', $USER);
$maincss = ($hassidepre and $hassidepost and (get_user_preferences('theme_right_sidebar') == null or get_user_preferences('theme_right_sidebar') > 0)) ? ' bothside ' : ' ';
$maincss .= ($hassidepre and !$hassidepost) ? ' leftside ' : '';
$maincss .= (!$hassidepre and $hassidepost) ? ' rightside ' : '';
$maincss .= ($pinned_sidebar) ? ' bothside ' : '';
$bodycss = ($hassidemenu) ? '' : 'nosidemenu';
$bodycss .= (is_siteadmin()) ? ' issiteadmin' : '';
/*$bodycss .= ($hassidecourse) ? 'courseside' : '';
$bodycss .= (($hassidecourse) and (get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0)) ? ' courseside-open' : '';*/
echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo theme_talentquest_pagetitle(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_talentquest_generate_favicon('favicon'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,400italic,700,900,700italic,500italic,500' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo $CFG->wwwroot;?>/theme/talentquest/style/rating.css" rel="stylesheet">
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.min.js"></script>
	<!--<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.scrollbar.min.js"></script>-->
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/colorpicker.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/rating.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/system.js"></script>
    <?php theme_talentquest_check_editing(true); ?>
</head>

<body <?php  echo  str_replace("dir-ltr", "",$OUTPUT->body_attributes(array('class'=>$bodycss))); ?>>
	<?php if($USER->id): ?>

		<?php echo ($hassidemenu) ? '<section class="menu-sidebar">'.$OUTPUT->blocks('side-menu', 'side-menu').'</section>' : ''; ?>

        <?php /*if ($hassidecourse) : ?>
            <section class="course-sidebar<?php echo (get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0) ? ' open' : ''?>">
                <?php echo $OUTPUT->blocks('side-course', 'side-course'); ?>
            </section>
        <?php endif; */?>
	<?php endif ;?>
    <?php include("includes/header.php"); ?>
    <?php if($USER->id): ?>
        <section class="left-sidebar settings<?php echo ($pinned_sidebar) ? ' pinned' : '' ?>" data-type-open="<?php echo $active_sidebar; ?>">
            <div class="sidebar-header">
				<nav class="user clearfix">
					<div class="user-info">
						<div class="user-img">
							<?php echo $OUTPUT->user_picture($USER, array('width'=>'50', 'link'=>false)); ?>
						</div>
						<span class="name right"><?php echo fullname($USER); ?></span>
					</div>
					<ul class = "menu clearfix">
						<li><a href="<?php echo $CFG->wwwroot; ?>" title="<?php echo get_string('dashboard','theme_talentquest'); ?>"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/home.png" alt=""><?php echo get_string('dashboard','theme_talentquest'); ?></a></li>
						<li><a href="#" title="Contact Us"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/headphone.png" alt="">Contact Us</a></li>
						<li><a href="<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo sesskey()?>" title="<?php echo get_string('logout'); ?>"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/power.png"><?php echo get_string('logout'); ?></a></li>
					</ul>
				</nav>
				<span class="title-manage">Manage</span>
				<span class="title-settings">Settings</span>
				<span class="sidebar-header-actions">
					<i class="toggler ion-pin" onclick="pinSidebar();"></i>
					<i class="ion-ios-close-outline closer" onclick="closeSidePre();"></i>
				</span>
			</div>
            <?php echo ($hassidepre) ? $OUTPUT->blocks('side-pre', 'side-pre') : ''; ?>
        </section>
    <?php endif ;?>
	<section class="head">
		<div class="inner clearfix">
			<div class="pathway">
				<?php //echo $html->heading; ?>
				<div id="page-navbar" class="clearfix">
					<nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                    <?php echo theme_talentquest_settings_btn(); ?>
					<div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
				</div>
				<div id="course-header">
					<?php echo $OUTPUT->course_header(); ?>
				</div>
			</div>
		</div>
        <?php echo theme_talentquest_settings_bar(); ?>
	</section>
	
	<section id="page" class="page inner clearfix <?php echo $maincss; ?>">
		<div class="clearfix">
			<div class="content clearfix"  id="region-main">
				 <?php
					echo $OUTPUT->course_content_header();
					echo $OUTPUT->main_content();
					echo $OUTPUT->course_content_footer();
				?>
			</div>
			<?php echo ($hassidepost) ? $OUTPUT->blocks('side-post', 'side-post') : ''; ?>
		</div>

        <?php include('includes/footer.php'); ?>
	</section>
</body>
</html>