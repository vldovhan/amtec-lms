<footer>
	<div class="inner clearfix">
		<div class="left">
            <?php if (isset($theme_settings->text1)) : ?>
			 <div class="copyright"><?php echo str_replace('{year}', date("Y"), $theme_settings->text1);?></div>
            <?php endif; ?>
            <?php if (isset($theme_settings->text2) or isset($theme_settings->text3)) :?>
                <nav class="menu clearfix">
                    <div id="custom_menu_1" class="yui3-menu yui3-menu-horizontal custom-menu" role="menu">
                        <div class="yui3-menu-content" role="presentation">
                            <ul class="first-of-type clearfix" role="presentation">
                                <?php if (isset($theme_settings->text2)) : ?>
                                    <li class="yui3-menuitem" role="presentation">
                                        <a class="yui3-menuitem-content" href="<?php echo $theme_settings->text2; ?>" title="Terms of use" role="menuitem" tabindex="0"><?php echo get_string('termsofuse', 'theme_talentquest'); ?></a>
                                    </li>
                                <?php endif; ?>
                                <?php if (isset($theme_settings->text3)) : ?>
                                    <li class="yui3-menuitem" role="presentation">
                                        <a class="yui3-menuitem-content" href="<?php echo $theme_settings->text3; ?>" title="Privacy Policy" role="menuitem" tabindex="-1"><?php echo get_string('privacypolicy', 'theme_talentquest'); ?></a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </nav>
            <?php endif; ?>
		</div>
		<nav class="right">
            <?php if (isset($theme_settings->text1)) : ?>
                <a href="<?php echo $theme_settings->text5; ?>" class="ng-binding"><?php echo $theme_settings->text4; ?></a>
            <?php endif; ?>
		</nav>
		<div class="clear"></div>
		<?php echo (isset($html->footnote) ? $html->footnote : '');?>
		<?php echo $OUTPUT->standard_footer_html();  ?>
		<?php echo $OUTPUT->standard_end_of_body_html() ?>
	</div>
</footer>