<header>
    <div id="site_wwwroot" class="hidden" data-value="<?php echo $CFG->wwwroot; ?>"></div>
    <?php echo $OUTPUT->standard_top_of_body_html() ?>
    <?php $html = theme_talentquest_get_html_for_settings($OUTPUT, $PAGE);?>
    <div class="inner clearfix">
        <div class="logo">
            <a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_talentquest_get_logo(); ?></a>
        </div>
        <?php if (isloggedin()): ?>
            <a href="#" class="quickview-link pull-right btn-link" data-toggle="quickview" data-toggle-element="#quickview"><i class="fa fa-th-list"></i></a>
            <nav class="user clearfix">
                <div id="google_translate_element" class="google-translate-element"></div>
                <?php echo $OUTPUT->user_picture($USER, array('width'=>'40', 'link'=>false)); ?>
                <div class="user-info">
                    <span class="name"><?php echo fullname($USER); ?></span>
                    <ul class = "clearfix">
                        <li><a href="<?php echo $CFG->wwwroot; ?>" title="<?php echo get_string('dashboard','theme_talentquest'); ?>"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/home.png" alt="<?php echo get_string('dashboard','theme_talentquest'); ?>"></a></li>
                        <li><a href="#" title="Contact Us"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/headphone.png" alt="Contact Us"></a></li>
                        <li class="dropdown">
                            <a title="Settings" data-toggle="dropdown" href="javascript:;"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/cog.png" alt="Settings"></a>
                            <ul class="dropdown-menu">
                                <?php /* ?><li class="logout notifications clearfix"><a href="<?php echo $CFG->wwwroot; ?>/local/notifications/index.php" title="<?php echo get_string('notifications', 'theme_talentquest'); ?>"><i class="fa fa-bell"></i> <?php echo get_string('notifications', 'theme_talentquest'); ?></a></li><?php */ ?>
                                <?php /*if (\core\session\manager::is_loggedinas()) : ?>
                                    <li class="logout clearfix"><a href="<?php echo $CFG->wwwroot; ?>/course/loginas.php?sesskey=<?php echo sesskey(); ?>" title="<?php echo get_string('backtoown', 'theme_talentquest'); ?>"><i class="fa fa-reply"></i> <?php echo get_string('backtoown', 'theme_talentquest'); ?></a></li>
                                <?php endif;*/ ?>
                                <li class="logout clearfix"><a href="<?php echo $CFG->wwwroot; ?>/login/logout.php?sesskey=<?php echo sesskey()?>" title="<?php echo get_string('logout'); ?>"><img src="<?php echo $CFG->wwwroot;?>/theme/talentquest/pix/power.png"><?php echo get_string('logout'); ?></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="mobile-toggler" onclick="toggleSidePre('settings');"><i class="fa fa-bars"></i></div>
            </nav>
        <?php endif; ?>
    </div>
</header>
<section class="lang-sidebar">
    <div class="sidebar-header">Language menu
        <span class="sidebar-header-actions">
            <i onclick="toggleSideLanguage();" class="ion-ios-close-outline closer"></i>
        </span>
    </div>
    <?php echo theme_talentquest_lang_menu(); ?>
    <?php echo theme_talentquest_timezone_selector(); ?>
</section>
<?php if (isloggedin()): ?>
    <div class="second-header">
        <div class="inner clearfix">
            <div class="left clearfix">
                <?php echo theme_talentquest_pagetitle(); ?>
            </div>
            <div class="right clearfix">
                <ul class="notification-list">
                    <li>
                        <div class="dropdown">
                            <a data-toggle="dropdown" class="fa fa-globe header-links" id="notification-center" href="javascript:;"><span class="bubble"></span></a>
                            <?php echo theme_print_notifications(); ?>
                        </div>
                    </li>
                    <?php /*
                    <li><a class="fa fa-link header-links" href="#"></a></li>
                    if ($hassidepost) : ?>
                        <li onclick="togglePostSidebar();" class="toggle-post-side"><a class="fa fa-long-arrow-left<?php echo (get_user_preferences('theme_right_sidebar') == 0) ? '' : ' hidden'?>" title="Show sidebar" href="#"></a><a class="fa fa-long-arrow-right<?php echo (get_user_preferences('theme_right_sidebar') == 0) ? ' hidden' : ''?>" href="#" title="Hide sidebar"></a></li>
                    <?php endif;*/ ?>
                </ul>
                <form action="<?php echo $CFG->wwwroot; ?>/blocks/course_catalog/search.php" id="tq_search_form">
                    <input type="text" placeholder="Search Learning Content..." id="tq-search" value="<?php echo optional_param('search', '', PARAM_RAW); ?>" name="search">
                    <button>
                        <i class="ion-ios-search-strong" onclick="jQuery('#tq_search_form').submit();"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (isloggedin() || false): ?>
    <div class="search-box">
        <div class="head">
            <div class="logo">
                <a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_talentquest_get_logo(); ?></a>
            </div>
            <i class="fa fa-times close" aria-hidden="true"></i>
        </div>
        <div class="input">
            <input spellcheck="false" autocomplete="off" placeholder="Search..." class="no-border overlay-search bg-transparent" id="search">
            <div class="checkbox">
                <input type="checkbox" checked="checked" value="1" id="checkboxn">
                <label for="checkboxn"><i class="fa fa-search"></i> Search within page</label>
            </div>
            <p>Press enter to search</p>
        </div>
        <div class="search-results m-t-40" style="display: block;">
            <p class="bold">Pages Search Results</p>
            <div class="clearfix">
                <div class="col">

                    <div class="">

                        <div class="icon">
                            <div>
                                <img width="50" height="50" alt="" src="http://pages.revox.io/dashboard/latest/html/assets/img/profiles/avatar.jpg">
                            </div>
                        </div>

                        <div class="text">
                            <h5 class="m-b-5"> on pages</h5>
                            <p class="hint-text">via john smith</p>
                        </div>
                    </div>


                    <div class="">

                        <div class="icon">
                            <div>T</div>
                        </div>

                        <div class="text">
                            <h5 class="m-b-5"> related topics</h5>
                            <p class="hint-text">via pages</p>
                        </div>
                    </div>


                    <div class="">

                        <div class="icon">
                            <div><i class="fa fa-headphones large-text "></i>
                            </div>
                        </div>

                        <div class="text">
                            <h5 class="m-b-5"> music</h5>
                            <p class="hint-text">via pagesmix</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
<?php endif; ?>
<div class="background-content">
    <div class="inner"></div>
</div>