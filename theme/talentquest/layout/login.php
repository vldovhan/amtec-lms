<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The embedded layout.
 *
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$theme_settings = theme_talentquest_layout_settings();
$html = theme_talentquest_get_html_for_settings($OUTPUT, $PAGE);

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo theme_talentquest_pagetitle(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_talentquest_generate_favicon('favicon'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,400italic,700,900,700italic,500italic,500' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.min.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/system.js"></script>
</head>

<body <?php  echo $OUTPUT->body_attributes(); ?>>
	
	<section id="page" class="page inner clearfix">
		<div class="clearfix">
			<div class="content clearfix"  id="region-main">
				<div class="login-logo">
					<a href="<?php echo $CFG->wwwroot; ?>" ><?php echo theme_talentquest_get_logo(); ?></a>
				</div>
				<div class="login-welcome-text">
					<?php echo (isset($theme_settings->text6)) ? $theme_settings->text6 : $html->login_welcome; ?>
				</div>
				 <?php	echo $OUTPUT->main_content();?>
				<?php include('includes/footer.php'); ?>
			</div>
		</div>
	</section>
</body>
</html>