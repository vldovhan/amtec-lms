<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The embedded layout.
 *
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$theme_settings = theme_talentquest_layout_settings();
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$hassidemenu = $PAGE->blocks->region_has_content('side-menu', $OUTPUT);
$pinned_sidebar = get_user_preferences('pin-sidebar',0,$USER);
$active_sidebar = get_user_preferences('active-sidebar', '', $USER);
$maincss = 'leftside';
$maincss .= ($hassidepre and $pinned_sidebar) ? ' bothside' : '';
$bodycss .= (is_siteadmin()) ? ' issiteadmin' : '';
/*$bodycss = ($hassidepost) ? 'courseside' : '';
$bodycss .= (($hassidepost) and (get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0)) ? ' courseside-open' : '';*/

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo theme_talentquest_pagetitle(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_talentquest_generate_favicon('favicon'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,400italic,700,900,700italic,500italic,500' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.min.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/system.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/colorpicker.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.scrollbar.min.js"></script>
    <?php theme_talentquest_check_editing(true); ?>
</head>
<body <?php  echo  str_replace("dir-ltr", "",$OUTPUT->body_attributes(array('class'=>$bodycss))); ?>>
	<?php if($USER->id): ?>
		<section class="right-sidebar">
			<nav class="top-content">
				<label for="notes">Notes</label>
				<label for="alerts">Alerts</label>
				<a href="" class="close">
					<i class="fa fa-times"></i>
				</a>
			</nav>
			<div class="content-box">
				<div class="notes">
					<div class="first-content">
						<div class="toolbar">
							<ul class="clearfix">
								<li><a href="#"><i class="fa fa-plus"></i></a></li>
								<li><a href="#"><i class="fa fa-trash-o"></i></a></li>
							</ul>
						</div>
						<ul>
							<li class="clearfix" data-id="1">
								<span class="preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, sapiente?</span>
								<i class="fa fa-chevron-right"></i>
								<span class="date">02.06.16</span>
							</li>
							<li class="clearfix" data-id="2">
								<span class="preview">Eaque error facere minima neque ratione repellat tenetur ullam vel.</span>
								<i class="fa fa-chevron-right"></i>
								<span class="date">02.06.16</span>
							</li>
							<li class="clearfix" data-id="3">
								<span class="preview">Adipisci eum fugit in laboriosam magni, nobis tempore voluptate voluptatibus?</span>
								<i class="fa fa-chevron-right"></i>
								<span class="date">02.06.16</span>
							</li>
							<li class="clearfix"  data-id="4">
								<span class="preview">A, architecto delectus deleniti esse magnam non nulla saepe sapiente.</span>
								<i class="fa fa-chevron-right"></i>
								<span class="date">02.06.16</span>
							</li>
							<li class="clearfix" data-id="5">
								<span class="preview">Doloribus modi nulla rerum, similique sunt tempore ullam vel veritatis.</span>
								<i class="fa fa-chevron-right"></i>
								<span class="date">02.06.16</span>
							</li>
						</ul>
					</div>
					<div class="second-content right">
						<div class="toolbar">
							<ul class="clearfix">
								<li><a href="#"><i class="fa fa-chevron-left"></i></a></li>
								<li><a href="#"><i class="fa fa-bold"></i></a></li>
								<li><a href="#"><i class="fa fa-italic"></i></a></li>
								<li><a href="#"><i class="fa fa-link"></i></a></li>
							</ul>
						</div>
						<div data-id="1">
							<p class="date">21st april 2014 2:13am</p>
							<p class="preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae, sapiente?</p>
						</div>
						<div data-id="2">
							<p class="date">21st april 2014 2:13am</p>
							<p class="preview">Eaque error facere minima neque ratione repellat tenetur ullam vel.</p>
						</div>
						<div data-id="3">
							<p class="date">21st april 2014 2:13am</p>
							<p class="preview">Adipisci eum fugit in laboriosam magni, nobis tempore voluptate voluptatibus?</p>
						</div>
						<div data-id="4">
							<p class="date">21st april 2014 2:13am</p>
							<p class="preview">A, architecto delectus deleniti esse magnam non nulla saepe sapiente.</p>
						</div>
						<div data-id="5">
							<p class="date">21st april 2014 2:13am</p>
							<p class="preview">Doloribus modi nulla rerum, similique sunt tempore ullam vel veritatis.</p>
						</div>
					</div>
				</div>
				<div class="alerts">
					<div class="toolbar">
						<ul class="clearfix">
							<li><a href="#"><i class="fa fa-ellipsis-h"></i></a></li>
							<li class="center">Notications</li>
							<li><a href="#"><i class="fa fa-search"></i></a></li>
						</ul>
					</div>
					<div class="items-box">
						<h3>Sever Status</h3>
						<ul>
							<li>
								<a href="#" class="clearfix">
									<i class="fa fa-circle text-warning"></i>
									<p class="text">David Nester Birthday</p>
									<p class="text-right">
										<span class="text-warning">Today <br></span>
										<span class="text-master">All Day</span>
									</p>
								</a>
							</li>
							<li>
								<a href="#" class="clearfix">
									<i class="fa fa-circle text-warning"></i>
									<p class="text">Meeting at 2:30</p>
									<p class="text-right">
										<span class="text-warning">Today <br></span>
										<span class="text-master">All Day</span>
									</p>
								</a>
							</li>
						</ul>
					</div>
					<div class="items-box">
						<h3>Social</h3>
						<ul>
							<li>
								<a href="#" class="clearfix">
									<i class="fa fa-circle text-complete"></i>
									<p class="text-full">
										<span class="text-master link">Jame Smith commented on your status<br></span>
										<span class="text-master">“Perfection Simplified - Company Revox"</span>
									</p>
								</a>
							</li>
							<li>
								<a href="#" class="clearfix">
									<i class="fa fa-circle text-complete"></i>
									<p class="text-full">
										<span class="text-master link">Jame Smith commented on your status<br></span>
										<span class="text-master">“Perfection Simplified - Company Revox"</span>
									</p>
								</a>
							</li>
						</ul>
					</div>
					<div class="items-box">
						<h3>Sever Status</h3>
						<ul>
							<li>
								<a href="#" class="clearfix">
									<i class="fa fa-circle text-danger"></i>
									<p class="text-full">
										<span class="text-master link">Jame Smith commented on your status<br></span>
										<span class="text-master">“Perfection Simplified - Company Revox"</span>
									</p>
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
        <section class="menu-sidebar">
            <?php echo ($hassidemenu) ? $OUTPUT->blocks('side-menu', 'side-menu') : ''; ?>
        </section>
	<?php endif ;?>
    <?php include("includes/header.php"); ?>
    <?php if($USER->id): ?>
        <section class="left-sidebar settings<?php echo ($pinned_sidebar) ? ' pinned' : '' ?>" data-type-open="<?php echo $active_sidebar; ?>">
            <div class="sidebar-header"><span class="title-manage">Manage</span><span class="title-settings">Settings</span><span class="sidebar-header-actions"><i class="toggler ion-pin" onclick="pinSidebar();"></i><i class="ion-ios-close-outline closer" onclick="closeSidePre();"></i></span></div>
            <?php echo ($hassidepre) ? $OUTPUT->blocks('side-pre', 'side-pre') : ''; ?>
        </section>
        <?php if ($hassidepost) : ?>
            <section class="course-sidebar<?php echo (get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0) ? ' open' : ''?>">
                <?php $OUTPUT->blocks('side-post', 'side-post'); ?>
            </section>
        <?php endif ;?>
    <?php endif ;?>
	<section class="head">
		<div class="inner clearfix">
			<div class="pathway">
				<?php //echo $html->heading; ?>
				<div id="page-navbar" class="clearfix">
					<nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
					<div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
				</div>
				<div id="course-header">
					<?php echo $OUTPUT->course_header(); ?>
				</div>
			</div>
		</div>
	</section>
	
	<section id="page" class="page inner clearfix <?php echo $maincss; ?>">
		<div class="clearfix">
			<div class="content clearfix"  id="region-main">
				 <?php
					echo $OUTPUT->course_content_header();
					echo $OUTPUT->main_content();
					echo $OUTPUT->course_content_footer();
				?>
			</div>
		</div>

        <?php include('includes/footer.php'); ?>
	</section>
</body>
</html>