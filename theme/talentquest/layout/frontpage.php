<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The embedded layout.
 *
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$theme_settings = theme_talentquest_layout_settings();
$hassidepre = $PAGE->blocks->region_has_content('side-pre', $OUTPUT);
$hassidepost = $PAGE->blocks->region_has_content('side-post', $OUTPUT);
$maincss = ($hassidepre and $hassidepost) ? 'bothside' : '';
$maincss = ($hassidepre and !$hassidepost) ? 'leftside' : $maincss;
$maincss = (!$hassidepre and $hassidepost) ? 'rightside' : $maincss;
$html = theme_talentquest_get_html_for_settings($OUTPUT, $PAGE);

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo theme_talentquest_pagetitle(); ?></title>
    <!-- Favicon image -->
    <link rel="shortcut icon" href="<?php echo theme_talentquest_generate_favicon('favicon'); ?>" />
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <link rel="icon" type="image/png" href="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>" sizes="32x32">
    <meta name="msapplication-TileImage" content="<?php echo theme_talentquest_generate_favicon('faviconpng'); ?>">
    <meta name="msapplication-TileColor" content="#f5f5f5">
    <!-- Favicon image -->
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300,400italic,700,900,700italic,500italic,500' rel='stylesheet' type='text/css'>
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
	<link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.min.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/system.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/colorpicker.js"></script>
	<script src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/jquery.scrollbar.min.js"></script>
    <?php theme_talentquest_check_editing(true); ?>
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>
	<?php include("includes/header.php"); ?>
        <section class="head">
            <div class="inner clearfix">
                <div class="pathway">
                    <div id="page-navbar" class="clearfix">
                        <nav class="breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
                        <div class="breadcrumb-button"><?php echo $OUTPUT->page_heading_button(); ?></div>
                    </div>
                    <div id="course-header">
                        <?php echo $OUTPUT->course_header(); ?>
                    </div>
                </div>
            </div>
        </section>
	</section>
	
	<section id="page" class="page inner clearfix">
        <?php echo ($hassidepre) ? $OUTPUT->blocks('side-pre', 'side-pre') : ''; ?>
		<div class="content front clearfix <?php echo $maincss; ?>"  id="region-main">
			<div class="home-intro" style=""><?php echo $SITE->summary;?></div>
			<?php echo "<!--".$OUTPUT->main_content()."-->"; 
				global $SESSION;
				$editing = $PAGE->user_is_editing();
				$courserenderer = $PAGE->get_renderer('core', 'course');

			/// Print Section or custom info
				$siteformatoptions = course_get_format($SITE)->get_format_options();
				$modinfo = get_fast_modinfo($SITE);
				$modnames = get_module_types_names();
				$modnamesplural = get_module_types_names(true);
				$modnamesused = $modinfo->get_used_module_names();
				$mods = $modinfo->get_cms();
            
				if (!empty($CFG->customfrontpageinclude)) {
					include($CFG->customfrontpageinclude);

				} else if ($siteformatoptions['numsections'] > 0) {
					if ($editing) {
						// make sure section with number 1 exists
						course_create_sections_if_missing($SITE, 1);
						// re-request modinfo in case section was created
						$modinfo = get_fast_modinfo($SITE);
					}
					$section = $modinfo->get_section_info(1);
					if (($section && (!empty($modinfo->sections[1]) or !empty($section->summary))) or $editing) {
						echo $OUTPUT->box_start('generalbox sitetopic');

						/// If currently moving a file then show the current clipboard
						if (ismoving($SITE->id)) {
							$stractivityclipboard = strip_tags(get_string('activityclipboard', '', $USER->activitycopyname));
							echo '<p><font size="2">';
							echo "$stractivityclipboard&nbsp;&nbsp;(<a href=\"course/mod.php?cancelcopy=true&amp;sesskey=".sesskey()."\">". get_string('cancel') .'</a>)';
							echo '</font></p>';
						}

						$context = context_course::instance(SITEID);

						// If the section name is set we show it.
						if (!is_null($section->name)) {
							echo $OUTPUT->heading(
								format_string($section->name, true, array('context' => $context)),
								2,
								'sectionname'
							);
						}

						$summarytext = file_rewrite_pluginfile_urls($section->summary, 'pluginfile.php', $context->id, 'course', 'section', $section->id);
						$summaryformatoptions = new stdClass();
						$summaryformatoptions->noclean = true;
						$summaryformatoptions->overflowdiv = true;

						echo format_text($summarytext, $section->summaryformat, $summaryformatoptions);

						if ($editing && has_capability('moodle/course:update', $context)) {
							$streditsummary = get_string('editsummary');
							echo "<a title=\"$streditsummary\" ".
								 " href=\"course/editsection.php?id=$section->id\"><img src=\"" . $OUTPUT->pix_url('t/edit') . "\" ".
								 " class=\"iconsmall\" alt=\"$streditsummary\" /></a><br /><br />";
						}

						$courserenderer = $PAGE->get_renderer('core', 'course');
						echo $courserenderer->course_section_cm_list($SITE, $section);

						echo $courserenderer->course_section_add_cm_control($SITE, $section->section);
						echo $OUTPUT->box_end();
					}
				}
				// Include course AJAX
				include_course_ajax($SITE, $modnamesused);

				if (isloggedin() and !isguestuser() and isset($CFG->frontpageloggedin)) {
					$frontpagelayout = $CFG->frontpageloggedin;
				} else {
					$frontpagelayout = $CFG->frontpage;
				}
            
				echo html_writer::empty_tag('div', array('class' => 'clear'));
				echo html_writer::end_tag('div');
				echo html_writer::start_tag('div', array('class' => 'tab-content'));
					foreach (explode(',',$frontpagelayout) as $v) {
					switch ($v) {     /// Display the main part of the front page.
						case FRONTPAGENEWS:
							if ($SITE->newsitems) { // Print forums only when needed
								require_once($CFG->dirroot .'/mod/forum/lib.php');

								if (! $newsforum = forum_get_course_forum($SITE->id, 'news')) {
									print_error('cannotfindorcreateforum', 'forum');
								}

								// fetch news forum context for proper filtering to happen
								$newsforumcm = get_coursemodule_from_instance('forum', $newsforum->id, $SITE->id, false, MUST_EXIST);
								$newsforumcontext = context_module::instance($newsforumcm->id, MUST_EXIST);
								$forumname = format_string($newsforum->name, true, array('context' => $newsforumcontext));
								// wraps site news forum in div container.
								echo html_writer::start_tag('div', array('id'=>'site-news-forum'));

								if (isloggedin()) {
									$SESSION->fromdiscussion = $CFG->wwwroot;
									$subtext = '';
									if (\mod_forum\subscriptions::is_subscribed($USER->id, $newsforum)) {
										if (!\mod_forum\subscriptions::is_forcesubscribed($newsforum)) {
											$subtext = get_string('unsubscribe', 'forum');
										}
									} else {
										$subtext = get_string('subscribe', 'forum');
									}
									$suburl = new moodle_url('/mod/forum/subscribe.php', array('id' => $newsforum->id, 'sesskey' => sesskey()));
									echo html_writer::tag('div', html_writer::link($suburl, $subtext), array('class' => 'subscribelink'));
								} else {
									echo $OUTPUT->heading($forumname);
								}

								forum_print_latest_discussions($SITE, $newsforum, $SITE->newsitems, 'plain', 'p.modified DESC');

								//end site news forum div container
								echo html_writer::end_tag('div');
							}
						break;

						case FRONTPAGEENROLLEDCOURSELIST:
							$mycourseshtml = $courserenderer->frontpage_my_courses();
                            if (!empty($mycourseshtml)) {
								//wrap frontpage course list in div container
								echo html_writer::start_tag('div', array('id'=>'frontpage-course-enroll-list'));
								echo $mycourseshtml;
								//end frontpage course list div container
								echo html_writer::end_tag('div');
								break;
							}
							// No "break" here. If there are no enrolled courses - continue to 'Available courses'.

						case FRONTPAGEALLCOURSELIST:
							$availablecourseshtml = $courserenderer->frontpage_available_courses();
							if (!empty($availablecourseshtml)) {
								//wrap frontpage course list in div container
								echo html_writer::start_tag('div', array('id'=>'frontpage-course-all-list'));
								echo $availablecourseshtml;
								//end frontpage course list div container
								echo html_writer::end_tag('div');
							}
						break;

						case FRONTPAGECATEGORYNAMES:
							//wrap frontpage category names in div container
							echo html_writer::start_tag('div', array('id'=>'frontpage-category-names'));
							echo $courserenderer->frontpage_categories_list();
							//end frontpage category names div container
							echo html_writer::end_tag('div');
						break;

						case FRONTPAGECATEGORYCOMBO:
							//wrap frontpage category combo in div container
							echo html_writer::start_tag('div', array('id'=>'frontpage-category-combo'));
							echo $courserenderer->frontpage_combo_list();
							//end frontpage category combo div container
							echo html_writer::end_tag('div');
						break;

					}
				}
				echo html_writer::end_tag('div');
				if ($editing && has_capability('moodle/course:create', context_system::instance())) {
					echo $courserenderer->add_new_course_button();
				}
			?>
		</div>
		<?php echo ($hassidepost) ? $OUTPUT->blocks('side-post', 'side-post') : ''; ?>
	</section>
	<?php include('includes/footer.php'); ?>
<nav class="hidden breadcrumb-nav"><?php echo $OUTPUT->navbar(); ?></nav>
<script>
	if($( '#region-main').children().length < 5){
		$('.home-intro').css("display","block");
	}
	$( '.for-tab:first-of-type').attr( 'checked', true );
		function ajax_search(id,type){
				var  search = $('#shortsearchbox_'+id).val();
				var  category = $('#menu_'+id+'_courses').val();
				if(type == 'search' && search == ''){
					return;
				}
					if(id == 'all'){
						var clas = '.frontpage-course-list-all';
						var block = '#frontpage-course-all-list';
					}else if(id == 'my'){
						var clas = '.frontpage-course-list-enrolled';
						var block = '#frontpage-course-enroll-list';
					}
						jQuery.ajax({
							url: '<?php echo $CFG->wwwroot; ?>/theme/talentquest/ajax.php?action=search&search='+search+'&type='+id+'&category='+category,
							dataType: "html",
							async:false,
							beforeSend: function(){
								jQuery(clas).html('<i class="search"></i>');
							}
						}).done(function( data ) {
								jQuery(clas).remove();
								jQuery(block).append(data);
								jQuery(block+' .paging-morelink').remove();
								jQuery(block+' .clear').remove();
								jQuery('.coursebox').css({'opacity':'0','transition':'opacity 2s ease 0s'});
								setTimeout(function(){ jQuery('.coursebox').css({'opacity':'1','-webkit-animation':'animation 2000ms linear both','animation':'animation 2000ms linear both'}); }, 10);
						});	
		}	
		
		function ajax_get_courses(id,step){
			if(id == 'all'){
				var clas = '.frontpage-course-list-all';
				var block = '#frontpage-course-all-list';
			}else if(id == 'my'){
				var clas = '.frontpage-course-list-enrolled';
				var block = '#frontpage-course-enroll-list';
			}
				jQuery.ajax({
					url: '<?php echo $CFG->wwwroot; ?>/theme/talentquest/ajax.php?action=get_courses&step='+step+'&type='+id,
					dataType: "html",
					async:false,
					beforeSend: function(){
						jQuery(clas).append('<i class="search"></i>');
						jQuery(block+' .paging-morelink').remove();
					}
				}).done(function( data ) {
						jQuery(clas+' .search').remove();
						jQuery(clas+' .clear').remove();
						jQuery(clas).append(data);
						jQuery(clas).append(jQuery('#temp').html());
						jQuery(clas).append('<div class="clear"></div>');
						jQuery(clas).append(jQuery('#temp2').html());
						jQuery('#temp, #temp2').remove();
						jQuery('.paging-morelink').css({'opacity':'0','transition':'opacity 2s ease 0s'});
						setTimeout(function(){ jQuery('.coursebox.last ~ .coursebox, .paging-morelink').css({'opacity':'1','-webkit-animation':'animation 2000ms linear both','animation':'animation 2000ms linear both'}); }, 10);
				});	
		}		
		
		$("#shortsearchbox_all").keyup(function(event){
			if(event.keyCode == 13){
				$("#all-courses label").click();
			}
		});
		$("#shortsearchbox_my").keyup(function(event){
			if(event.keyCode == 13){
				$("#my-courses label").click();
			}
		});
		var button1 = Y.one("#shortsearchbox_all");
		if(button1 != null){
			button1.on('key', function (e) {e.preventDefault();e.stopPropagation();}, 'enter');
		}
		var button2 = Y.one("#shortsearchbox_my");
		if(button2 != null){
			button2.on('key', function (e) {e.preventDefault();	e.stopPropagation();}, 'enter');
		}
</script>
</body>
</html>

