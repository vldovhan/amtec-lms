<?php
/**
 * @package   theme_talentquest
 * @copyright 2016 talentquest, talentquest.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {


    // Logo file setting.
    /*$name = 'theme_talentquest/logo';
    $title = get_string('logo','theme_talentquest');
    $description = get_string('logodesc', 'theme_talentquest');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);*/

    // Custom CSS file.
    $name = 'theme_talentquest/customcss';
    $title = get_string('customcss', 'theme_talentquest');
    $description = get_string('customcssdesc', 'theme_talentquest');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // login note setting.
    $name = 'theme_talentquest/login_welcome';
    $title = get_string('login_welcome', 'theme_talentquest');
    $description = get_string('login_welcome_desc', 'theme_talentquest');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_talentquest/footnote';
    $title = get_string('footnote', 'theme_talentquest');
    $description = get_string('footnotedesc', 'theme_talentquest');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
	
}
