<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * List of questions groups. Action for adding/editing/deleting a question group.
 *
 * @package mod_lesson
 * @copyright  2016 InveritaSoft
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 **/

require_once("../../config.php");
require_once($CFG->dirroot.'/mod/lesson/locallib.php');
require_once('editgroup_form.php');

// first get the preceeding page
$id = required_param('id', PARAM_INT);         // Course Module ID
$groupid = optional_param('groupid', 0, PARAM_INT);         // Question Group ID
$action = optional_param('action', false, PARAM_ALPHA);   // Action
$edit   = optional_param('edit', false, PARAM_BOOL);

$cm = get_coursemodule_from_id('lesson', $id, 0, false, MUST_EXIST);
$course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);

require_login($course, false, $cm);

$context = context_module::instance($cm->id);
require_capability('mod/lesson:edit', $context);

$url = new moodle_url('/mod/lesson/editgroup.php', array('id'=>$id));
$url->param('id', $id);
$PAGE->set_url($url);
$group_output = $PAGE->get_renderer('mod_lesson');

if ($groupid) {
	$group = $DB->get_record('lesson_question_groups', array('id' => $groupid), '*', MUST_EXIST);
}
if ($action == 'delete') {
	$DB->delete_records("lesson_question_groups", array("id"=>$groupid));
	redirect($url);
}

$mform = new editgroup_form($url);

if ($mform->is_cancelled()) {
	redirect($url);
	exit;
} else if ($fromform = $mform->get_data()) {
	$data = new stdClass;
	$data->lessonid = $cm->id;
	$data->name = $fromform->name;

	if ($groupid) {
		$data->id = $groupid;
		$DB->update_record('lesson_question_groups', $data);
	}
	else {
		$DB->insert_record('lesson_question_groups', $data);
	}

	redirect($url);
}

echo $group_output->header($lesson, $cm, '', false, null, get_string('question-group', 'lesson'));

if ($edit) {
	$PAGE->navbar->add(get_string($action, 'lesson'));

	$data = new stdClass;
	$data->id = $PAGE->cm->id;
	if ($groupid) {
		$data->name = $group->name;
		$data->groupid = $groupid;
	}

	$mform->set_data($data);
	$mform->display();

}
else {
	$groups = $DB->get_records_select('lesson_question_groups', 'lessonid=' . $cm->id, null, '', 'id, name');
	$group_content = $group_output->lessonGroups($groups);
	$url->param('edit', 1);
	$group_content .= html_writer::link($url, get_string('add-group', 'lesson'),
		array('class'=>'centerpadded lessonbutton standardbutton btn btn-primary'));

	echo $group_content;
}

echo $group_output->footer();