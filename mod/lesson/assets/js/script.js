function alertMessage(status, message) {
	var message_box = '<div class="alert alert-' + status + ' alert-block fade in " role="alert"> <button type="button" class="close" data-dismiss="alert">×</button>' + message + '</div>';
	return message_box;
}

(function($){
	$( document ).ready(function() {
		// Fixed page navigation if the content is too large
		if ($('#region-main').find('.box.contents').length) {
			var content_selector = '.box.contents';
		}
		else if ($('#region-main').find('.mform').length) {
			var content_selector = '.mform';
		}

		if (content_selector) {
			var content_position = $(content_selector).offset(),
				content_height = $(content_selector).height(),
				content_width = $('#region-main').width();

			var lesson_page_nav = $('.lesson-pagenav-buttons'),
				page_nav_position = $('.lesson-pagenav-buttons').offset(),
				end_of_content = content_position.top + content_height,
				dif = page_nav_position.top - end_of_content;

			if (end_of_content + dif > window.innerHeight) {
				lesson_page_nav.addClass('fixed_nav');
				lesson_page_nav.css('width', content_width + 'px');
				var $document = $( document );
				$document.scroll(function() {
					if ($document.scrollTop() + window.innerHeight > page_nav_position.top) {
						lesson_page_nav.removeClass('fixed_nav');
						lesson_page_nav.css('width', 'auto');
					}
					else {
						lesson_page_nav.addClass('fixed_nav');
						lesson_page_nav.css('width', content_width + 'px');
					}
				});
			}
		}

		// Table contents toogle right AND cookie to show/hide table
		var content_table = $('.content-table');
		content_table.insertBefore('.lesson-wrapper-table');

		if ($.cookie('content_table_collapsed') == 1) {
			content_table.addClass('collapsed');
		}

		$('.btn-content-table').on('click', function(){
			var content_table = $('.content-table');
			if (content_table.hasClass('collapsed')) {
				$.cookie('content_table_collapsed', null);
				content_table.removeClass('collapsed');
			}
			else {
				$.cookie('content_table_collapsed', 1);
				content_table.addClass('collapsed');
			}
		});

		// Question ajax
		$('#questions-group .mform .felement.fsubmit').append('<div class="preloader"></div>');
		$('#questions-group .mform').on('submit', function(e) {
			e.preventDefault();

			var form = $(this).closest('form'),
				form_data = form.serialize(),
				preloader = form.find('.preloader'),
				submit_block = form.find('.fitem_fsubmit'),
				progress_bar = $('.box.progress_bar');

			preloader.css('display', 'inline-block');

			$.ajax({
				type: 'POST',
				url: form.attr('action'),
				async: true,
				cache: false,
				dataType: "json",
				data: form_data + '&ajax=1',
				success: function (response, textStatus, jqXHR) {
					var message_answer = alertMessage(response.status, response.messages.answer);

					preloader.css('display', 'none');
					form.find('.alert-block').remove();
					console.log(response);
					if (response.status == 'success') {
						form.html(message_answer);
					} else if (response.status == 'error') {
						if (response.content.noanswer) {
							submit_block.prepend(message_answer);
						} else if (response.content.attemptsremaining) {
							submit_block.prepend(message_answer);
							form.prepend(alertMessage(response.status, response.messages.attemptsremaining));
						} else if (response.content.maxattemptsreached) {
							form.html(message_answer);
						} else {
							submit_block.prepend(message_answer);
						}
					} else {
						alert('There is no status response');
					}

					progress_bar.replaceWith(response.progress_bar);
				}
			});
		});

	});
})(jQuery);

