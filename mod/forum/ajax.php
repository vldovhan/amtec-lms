<?php

require_once('../../config.php');
require_once('lib.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$state	  = optional_param('state', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'set-important'){	
	$forumid	      = optional_param('forumid', 0, PARAM_INT);
	$discussionid	  = optional_param('discussionid', 0, PARAM_INT);
    
    if ($state > 0){
        $important = new stdClass();
        $important->forumid = $forumid;
        $important->discussionid = $discussionid;
        $important->userid = $USER->id;
        $important->type = 'important';
        $important->value = 1;
        $important->timemodified = time();
        $DB->insert_record('forum_user_activity', $important);
    } else {
        $DB->delete_records('forum_user_activity', array('forumid'=>$forumid, 'discussionid'=>$discussionid, 'userid'=>$USER->id, 'type'=>'important'));
    }
    
	echo time();
    
} elseif($action == 'set-like'){	
    
	$forumid	      = optional_param('forumid', 0, PARAM_INT);
	$discussionid	  = optional_param('discussionid', 0, PARAM_INT);
	$postid	          = optional_param('postid', 0, PARAM_INT);
    
    if ($state > 0){
        $like = new stdClass();
        $like->forumid = $forumid;
        $like->discussionid = $discussionid;
        $like->postid = $postid;
        $like->userid = $USER->id;
        $like->type = 'like';
        $like->value = 1;
        $like->timemodified = time();
        $DB->insert_record('forum_user_activity', $like);
    } else {
        $DB->delete_records('forum_user_activity', array('forumid'=>$forumid, 'discussionid'=>$discussionid, 'postid'=>$postid, 'userid'=>$USER->id, 'type'=>'like'));
    }
    
	echo time();
} elseif($action == 'set-helpful'){	
    
	$forumid	      = optional_param('forumid', 0, PARAM_INT);
	$discussionid	  = optional_param('discussionid', 0, PARAM_INT);
	$postid	          = optional_param('postid', 0, PARAM_INT);
    
    if ($state > 0){
        $helpful = new stdClass();
        $helpful->forumid = $forumid;
        $helpful->discussionid = $discussionid;
        $helpful->postid = $postid;
        $helpful->userid = $USER->id;
        $helpful->type = 'helpful';
        $helpful->value = 1;
        $helpful->timemodified = time();
        $DB->insert_record('forum_user_activity', $helpful);
    } else {
        $DB->delete_records('forum_user_activity', array('forumid'=>$forumid, 'discussionid'=>$discussionid, 'postid'=>$postid, 'userid'=>$USER->id, 'type'=>'helpful'));
    }
    
	echo time();
    
} elseif($action == 'set-rating'){	
    
	$forumid	      = optional_param('forumid', 0, PARAM_INT);
	$discussionid	  = optional_param('discussionid', 0, PARAM_INT);
	$postid	          = optional_param('postid', 0, PARAM_INT);
	$rate	          = optional_param('rate', 0, PARAM_INT);
    
    if ($rate > 0){
        $rating = $DB->get_record('forum_user_activity', array('forumid'=>$forumid, 'discussionid'=>$discussionid, 'postid'=>$postid, 'userid'=>$USER->id, 'type'=>'rate')); 
        if ($rating){
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->update_record('forum_user_activity', $rating);
        } else {
            $rating = new stdClass();
            $rating->forumid = $forumid;
            $rating->discussionid = $discussionid;
            $rating->postid = $postid;
            $rating->userid = $USER->id;
            $rating->type = 'rate';
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->insert_record('forum_user_activity', $rating);     
        }
    }
       
	echo time();
} 

exit;
