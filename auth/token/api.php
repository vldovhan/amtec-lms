<?php
require_once('../../config.php');

$t = optional_param('token', '', PARAM_TEXT);

$idnumber = optional_param('idnumber', '', PARAM_TEXT);
$idnumber = trim(core_text::strtolower($idnumber));

$username = optional_param('username', '', PARAM_USERNAME);
$username = trim(core_text::strtolower($username));

$error = '';
$result = '';

if($token = $DB->get_record('external_tokens', array('token'=>$t))){
	if (!empty($token->validuntil) and $token->validuntil < time()) {
        $error = 'invalidtimedtoken';
    }elseif (!empty($token->iprestriction) and !address_in_subnet(getremoteaddr(), $token->iprestriction)) {
        $error = 'invalidiptoken';
    }else{
    	if($idnumber){
    		$params = array('idnumber'=>$idnumber);
    	}else{
    		$params = array('username'=>$username);
    	}
	    if($user = $DB->get_record('user', $params, 'id,token')){
	        $user->token = md5(uniqid(rand(), 1));
			$DB->update_record('user', $user);

			$result = $user->token;
	    }else{
	        $error = 'invalidusertoken';
	    }
	}
}else{
    $error = 'invalidtoken';
}
echo json_encode(array('error'=> $error,'token'=> $result));
