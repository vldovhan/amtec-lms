<?php
require_once('../../config.php');

$token = optional_param('token', '', PARAM_TEXT);
$courseid = optional_param('courseid', 0, PARAM_INT);
$enrol = optional_param('enrol', 0, PARAM_INT);
$calendar = optional_param('calendar', 0, PARAM_INT);
$catalog = optional_param('catalog', 0, PARAM_INT);

if(isloggedin()){
	$user = $USER;
}elseif($data = $DB->get_record('user', array('token'=>$token),'id,username,auth')){
    $auth = $data->auth;
    $data->auth = 'token';
    $DB->update_record('user', $data);

    $user = authenticate_user_login($data->username, $token);

    $data->auth =  $auth;
    $DB->update_record('user', $data);

    if(!empty($user)){
    	complete_user_login($user);
        \core\session\manager::apply_concurrent_login_limit($user->id, session_id());

       if (empty($CFG->rememberusername) or ($CFG->rememberusername == 2)) {
            set_moodle_cookie('');
        } else {
            set_moodle_cookie($USER->username);
        }
    }else{
    	die("Error 101");
    }
}else{
    die("Error 102");
}

if($courseid) {
    $context = context_course::instance($courseid, IGNORE_MISSING);
    if (!is_enrolled($context, $user->id) and $enrol) {
        $plugin = enrol_get_plugin('manual');
        $student = get_archetype_roles('student');
        $student = reset($student);

        if($instance = $DB->get_record('enrol', array('courseid'=>$courseid, 'enrol'=>'manual'), '*')){
            $plugin->enrol_user($instance, $user->id, $student->id, time());
        }
    }
    redirect(new moodle_url('/course/view.php', array('id'=>$courseid)));
    exit;
}elseif ($calendar) {
    redirect(new moodle_url('/calendar/view.php', array()));
    exit;
}elseif ($catalog) {
    redirect(new moodle_url('/local/manager/courses/index.php', array()));
    exit;
}
redirect($CFG->wwwroot);
