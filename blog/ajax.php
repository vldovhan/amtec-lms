<?php

require_once('../config.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$postid	  = optional_param('postid', 0, PARAM_INT);
$commentid= optional_param('commentid', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$state	  = optional_param('state', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'set-like'){	
    if ($state > 0){
        $like = new stdClass();
        $like->postid = $postid;
        $like->userid = $USER->id;
        $like->type = 'like';
        $like->value = 1;
        $like->timemodified = time();
        $DB->insert_record('post_user_activity', $like);
    } else {
        $DB->delete_records('post_user_activity', array('postid'=>$postid, 'userid'=>$USER->id, 'type'=>'like'));
    }
    
	echo time();
} elseif($action == 'set-helpful'){	
    
    if ($state > 0){
        $helpful = new stdClass();
        $helpful->postid = $postid;
        $helpful->userid = $USER->id;
        $helpful->type = 'helpful';
        $helpful->value = 1;
        $helpful->timemodified = time();
        $DB->insert_record('post_user_activity', $helpful);
    } else {
        $DB->delete_records('post_user_activity', array('postid'=>$postid, 'userid'=>$USER->id, 'type'=>'helpful'));
    }
    
	echo time();
    
} elseif($action == 'set-rating'){	
    
	$rate	          = optional_param('rate', 0, PARAM_INT);
    
    if ($rate > 0){
        $rating = $DB->get_record('post_user_activity', array('postid'=>$postid, 'userid'=>$USER->id, 'type'=>'rate')); 
        if ($rating){
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->update_record('post_user_activity', $rating);
        } else {
            $rating = new stdClass();
            $rating->postid = $postid;
            $rating->userid = $USER->id;
            $rating->type = 'rate';
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->insert_record('post_user_activity', $rating);     
        }
    }
       
	echo time();
} elseif($action == 'set-comment-like'){	
    if ($state > 0){
        $like = new stdClass();
        $like->commentid = $commentid;
        $like->userid = $USER->id;
        $like->type = 'like';
        $like->value = 1;
        $like->timemodified = time();
        $DB->insert_record('comments_user_activity', $like);
    } else {
        $DB->delete_records('comments_user_activity', array('commentid'=>$postid, 'userid'=>$USER->id, 'type'=>'like'));
    }
    
	echo time();
} elseif($action == 'set-comment-helpful'){	
    
    if ($state > 0){
        $helpful = new stdClass();
        $helpful->commentid = $commentid;
        $helpful->userid = $USER->id;
        $helpful->type = 'helpful';
        $helpful->value = 1;
        $helpful->timemodified = time();
        $DB->insert_record('comments_user_activity', $helpful);
    } else {
        $DB->delete_records('comments_user_activity', array('postid'=>$postid, 'userid'=>$USER->id, 'type'=>'helpful'));
    }
    
	echo time();
} 

exit;
