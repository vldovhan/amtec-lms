jQuery(document).ready(function(){
    jQuery('.combo-header li').click(function(e){
        jQuery('.combo-header li').removeClass('active');
        jQuery(this).addClass('active');
        
        var blockname = jQuery(this).attr('data-type-header');
        jQuery('.combo-content li').removeClass('active');
        jQuery('.combo-content li[data-type-content="'+blockname+'"]').addClass('active');
    });
    jQuery('.combo-select').change(function(e){
        var blockname = jQuery(this).val();
        jQuery('.combo-content li').removeClass('active');
        jQuery('.combo-content li[data-type-content="'+blockname+'"]').addClass('active');
    });
});
