<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Handles displaying the calendar block.
 *
 * @package    block_sb_combo
 * @copyright  2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_sb_combo extends block_base {

    protected $courses = null;
    protected $blocks = array();
    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_combo');
    }
	
    function hide_header() {
        return true;
    }
    
    function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT, $PAGE;
		
		if ($this->content !== null) {
            return $this->content;
        }
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $blocksarray = array('sb_calendar', 'timeline', 'sb_timeline');
        $blocks = array();
        
        foreach($blocksarray as $blockname){
            $block = block_instance($blockname, $this->instance);
            if ($block){
                $blocks[$blockname] = $block;
            }
        }
        
        if (count($blocks)){
            $this->blocks = $blocks;
            $this->content->text .= $this->get_blocks_header_select();
            $this->content->text .= $this->get_blocks_content(true);
            $PAGE->requires->js('/blocks/sb_combo/javascript/script.js');
        }
        
        /*if ($PAGE->user_is_editing()) {
            $this->content->footer .= html_writer::link(new moodle_url('/blocks/sb_combo/manage.php'), get_string('manage_blocks', 'block_sb_combo'), array('title'=>get_string('manage_blocks', 'block_sb_combo'), 'class'=>'manage-link'));
        }*/
        
        
        return $this->content;
    }
    
    public function get_blocks_header_select(){
        global $CFG, $DB, $USER, $OUTPUT;
        $output = '';
        
        $blocks = $this->blocks;
        if (!count($blocks)) return $output;
        
        $output .= html_writer::start_tag('select', array('class'=>'combo-select'));
        $i = 0;
        foreach ($blocks as $blockname=>$block){
            $params = array('value'=>$blockname);
            if ($i == 0) $params['selected'] = 'selected';
            $output .= html_writer::tag('option', $block->title, $params);
            $i++;
        }
        $output .= html_writer::end_tag('select');
        
        return $output;
    }
    
    public function get_blocks_header_tabs($blocks){
        global $CFG, $DB, $USER, $OUTPUT;
        $output = '';
        
        $blocks = $this->blocks;
        if (!count($blocks)) return $output;
        
        $output .= html_writer::start_tag('div', array('class'=>'combo-header'));
        $output .= html_writer::start_tag('ul', array('class'=>'clearfix'));
        $i = 0;
        foreach ($blocks as $blockname=>$block){
            $output .= html_writer::start_tag('li', array('data-type-header'=>$blockname, 'class'=>'header-item'.(($i == 0) ? ' active' : '')));
                $output .= html_writer::tag('a', $block->title);
            $output .= html_writer::end_tag('li');    
            $i++;
        }
        $output .= html_writer::end_tag('ul');
        $output .= html_writer::end_tag('div');
        
        return $output;
    }
    
    public function get_blocks_content($with_header = false){
        global $CFG, $DB, $USER, $OUTPUT;
        $output = '';
        
        $blocks = $this->blocks;
        if (!count($blocks)) return $output;
        
        $output .= html_writer::start_tag('div', array('class'=>'combo-content'));
        $output .= html_writer::start_tag('ul', array('class'=>'clearfix'));
        $i = 0;
        foreach ($blocks as $blockname=>$block){
            $output .= html_writer::start_tag('li', array('data-type-content'=>$blockname, 'class'=>'content-item'.(($i == 0) ? ' active' : '')));
            $content = $block->get_content();
            $output .= (isset($content->text)) ? $content->text : '';
            $output .= html_writer::end_tag('li');
            $i++;
        }
        $output .= html_writer::end_tag('ul');
        $output .= html_writer::end_tag('div');
        
        return $output;
    }
    
}


