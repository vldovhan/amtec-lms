<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block Combo
 *
 * @package    block_sb_combo
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['sb_combo:addinstance'] = 'Add a new Combo block';
$string['sb_combo:myaddinstance'] = 'Add a new combo block to My home';
$string['pluginname'] = 'Calendar/Timeline Combo Block';
$string['blocktitle'] = 'Combo Block';
$string['manage_blocks'] = 'Manage blocks';
