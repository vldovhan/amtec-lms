<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Handles displaying the sb coursenav.
 *
 * @package    block_sb_coursenav
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_sb_coursenav_sections extends block_base {

    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_coursenav_sections');
    }
	function hide_header() {
        return true;
    }
    function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
		$attributes['style'] = 'order:1000;';
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT;
		
		//if ($this->content !== null) {
            return $this->content;
        //}
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
		
		$course = $this->page->course;
		$cm 	= $this->page->cm;
		$issite = ($course->id == SITEID);
		$context = context_course::instance($course->id, MUST_EXIST);
        
        $is_enrolled = (is_enrolled($context, $USER));
        
        if ($course->id == 1) return $this->content;
		require_once($CFG->dirroot.'/course/lib.php');
        $course = course_get_format($course)->get_course();
        $category = $DB->get_record('course_categories', array('id'=>$course->category));
		
		$modfullnames = array(); $archetypes = array();
		
		$displaysection = optional_param('section', 0, PARAM_INT);
		$modinfo = get_fast_modinfo($course);
        $toggler = html_writer::tag('span', '<i class="fa fa-angle-left'.((get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0) ? ' active' : '').'"></i><i class="fa fa-angle-right'.((get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0) ? '' : ' active').'"></i>', array('class'=>'course-nav-toggler', 'onclick'=>'toggleCourseSide();'));
        
		$format_oprions = $DB->get_record('course_format_options', array('courseid'=>$course->id, 'format'=>$course->format, 'name'=>'numsections'));
        
        $this->content->text .= html_writer::start_tag('div', array('class'=>'course-navigation-block'));

        $this->content->text .= html_writer::start_tag('div', array('class'=>'course-content-box'));
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) continue;
            if ($section > $format_oprions->value) break;
            
            if (!$thissection->uservisible) {
                $formattedinfo = \core_availability\info::format_info(
                        $thissection->availableinfo, $thissection->course);
                $o .= html_writer::div($formattedinfo, 'availabilityinfo');
            } else if (has_capability('moodle/course:viewhiddensections', $context) && !empty($CFG->enableavailability) && $thissection->visible) {
                $ci = new \core_availability\info_section($thissection);
                $fullinfo = $ci->get_full_information();
                if ($fullinfo) {
                    $formattedinfo = \core_availability\info::format_info(
                            $fullinfo, $thissection->course);
                }
            }
            
            if (!$thissection->uservisible and empty($fullinfo)) continue;
            
            $this->content->text .= html_writer::start_tag('ul', array('class'=>'course-section-box'.(((isset($cm->section) and $cm->section == $thissection->id) || ($displaysection > 0 and $displaysection == $section)) ? ' open' : '')));
            $this->content->text .= html_writer::tag('span', $section, array('class'=>'section-counter'));
            $this->content->text .= '<a href="javascript:void(0);" title="'.get_section_name($course, $thissection).'" alt="'.get_section_name($course, $thissection).'" class="section-header">'.get_section_name($course, $thissection).'<i class="fa fa-caret-down"></i></a>';
            
            $this->content->text .= html_writer::tag('div', ((strlen(strip_tags($thissection->summary)) > 100) ? substr(strip_tags($thissection->summary), 0, 100).'...' : strip_tags($thissection->summary)) , array('class'=>'section-summary'));
            
            if (!empty($modinfo->sections[$thissection->section])) {
                foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                    $mod = $modinfo->cms[$modnumber];
                    $url = $modinfo->cms[$modnumber]->url;
                    if ($is_enrolled and !$mod->visible and !has_capability('moodle/course:viewhiddenactivities', $mod->context)) continue;
                    $this->content->text .= html_writer::start_tag('li', array('class'=>((isset($cm->id) and $cm->id == $mod->id) ? 'current' : '')));
                    $this->content->text .= '<a href="'.(($is_enrolled) ? $url : 'javascript:void(0);').'" title="'.$mod->get_formatted_name().'" alt="'.$mod->get_formatted_name().'"><img src="'.$mod->get_icon_url().'" class="course-nav-mod-icon" alt="'.$mod->get_formatted_name().'" /> '.$mod->get_formatted_name().'</a>';
                    $this->content->text .= html_writer::end_tag('li');
                }
            }

            $this->content->text .= html_writer::end_tag('ul');
        }
        $this->content->text .= html_writer::end_tag('div');
        
        $this->content->text .= '<script>
                                jQuery(".course-section-box .section-header").click(function(e){
                                    jQuery(this).parent().toggleClass("open");
                                });
								</script>';
        
        // course progress
        if ($is_enrolled){
            $course_progress = $this->get_course_progress($course);
            if ($course_progress->status == 'pending' or $course_progress->status == 'inprogress' or $course_progress->status == 'completed'){
                $this->content->text .= html_writer::start_tag('div', array('class'=>'course-nav-progressbox'));
                $this->content->text .= html_writer::tag('div', $course_progress->completeditems.'/'.$course_progress->allitems, array('class'=>'course-completion-items', 'style'=>(intval($course_progress->completion) > 10) ? 'margin-left: calc('.intval($course_progress->completion).'% - 25px)' : '-15px;'));
                $this->content->text .= html_writer::start_tag('div', array('class'=>'clearfix'));
                $this->content->text .= html_writer::start_tag('div', array('class' => 'progres-bar'));
                $this->content->text .= html_writer::tag('div', '', array('class' => 'progres','style'=>'width:'.$course_progress->completion.'%;'));
                $this->content->text .= html_writer::end_tag('div');
                $this->content->text .= html_writer::end_tag('div');
                $this->content->text .= html_writer::end_tag('div');
            }
        }
        
        $this->content->text .= html_writer::start_tag('ul', array('class'=>'course-content-toggledbox'));
        
        $this->content->text .= html_writer::start_tag('li');
            $content = html_writer::tag('i','',array('class'=>'fa fa-list-ul','title'=>'Course navigation'));
        $this->content->text .= html_writer::link('javascript:void(0);', $content, array('title'=>'Course navigation', 'onclick'=>'toggleCourseSide();'));
        $this->content->text .= html_writer::end_tag('li');
        
        $this->content->text .= html_writer::end_tag('ul');
        

        
        // end block wrapper
        $this->content->text .= html_writer::end_tag('div');
        return $this->content;
    }
    
    public function get_course_progress($course){
        global $CFG, $PAGE, $DB, $USER, $OUTPUT;				

        require_once("{$CFG->libdir}/completionlib.php");
        require_once($CFG->dirroot.'/course/lib.php');

        $result = new stdClass();
        $result->completion = 0;
        $result->status = 'notyetstarted';
        $result->allitems = 0;
        $result->completeditems = 0;

        $context = context_course::instance($course->id);
        // Can edit settings?
        $can_edit = has_capability('moodle/course:update', $context);

        if ($can_edit){

            $completion = $DB->get_record_sql("SELECT c.id, sc.students_count, cc.completions_count
                FROM {course} c 
                    LEFT JOIN (SELECT c.id, COUNT(ra.id) AS students_count FROM {course} c LEFT JOIN {context} ct ON c.id = ct.instanceid LEFT JOIN {role_assignments} ra ON ra.contextid = ct.id WHERE ra.roleid = 5 ) sc ON sc.id = c.id 
                    LEFT JOIN (SELECT c.id, COUNT(ra.id) AS completions_count FROM {course} c LEFT JOIN {context} ct ON c.id = ct.instanceid LEFT JOIN {role_assignments} ra ON ra.contextid = ct.id AND ra.roleid = 5 LEFT JOIN {course_completions} cc ON cc.course = c.id AND cc.userid = ra.userid WHERE cc.timecompleted IS NOT NULL ) cc ON cc.id = c.id 
                WHERE c.id = $course->id");
            if ($completion->completions_count > 0){
                $result->completion = round(($completion->completions_count / $completion->students_count) * 100);
            }
            $result->allitems = ($completion->students_count > 0) ? $completion->students_count : 0;
            $result->completeditems = ($completion->completions_count > 0) ? $completion->completions_count : 0;
            $result->status = 'pending';
        } else {

            // Get course completion data.
            $info = new completion_info($course);

            // Load criteria to display.        
            $completions = $info->get_completions($USER->id);

            if ($info->is_tracked_user($USER->id)) {

                // For aggregating activity completion.
                $activities = array();
                $activities_complete = 0;
                $activities_viewed = 0;

                // For aggregating course prerequisites.
                $prerequisites = array();
                $prerequisites_complete = 0;

                // Flag to set if current completion data is inconsistent with what is stored in the database.
                $pending_update = false;

                // Loop through course criteria.
                foreach ($completions as $completion) {
                    $criteria = $completion->get_criteria();
                    $complete = $completion->is_complete();

                    if (!$pending_update && $criteria->is_pending($completion)) {
                        $pending_update = true;
                    }

                    // Activities are a special case, so cache them and leave them till last.
                    if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                        $activities[$criteria->moduleinstance] = $complete;

                        if ($complete) {
                            $activities_complete++;
                        }

                        continue;
                    }

                    // Prerequisites are also a special case, so cache them and leave them till last.
                    if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                        $prerequisites[$criteria->courseinstance] = $complete;

                        if ($complete) {
                            $prerequisites_complete++;
                        }

                        continue;
                    }
                }

                $itemsCompleted  = $activities_complete + $prerequisites_complete;
                $itemsCount      = count($activities) + count($prerequisites);

                // Aggregate completion.
                if (count($itemsCount) > 0) {
                    $result->completion = round(($itemsCompleted / $itemsCount) * 100);
                }
                
                $result->allitems = ($itemsCount > 0) ? $itemsCount : 0;
                $result->completeditems = ($itemsCompleted > 0) ? $itemsCompleted : 0;

                // Is course complete?
                $coursecomplete = $info->is_course_complete($USER->id);

                // Load course completion.
                $params = array(
                    'userid' => $USER->id,
                    'course' => $course->id
                );
                $ccompletion = new completion_completion($params);

                // Has this user completed any criteria?
                $criteriacomplete = $info->count_course_user_data($USER->id);

                if ($pending_update) {
                    $status = 'pending';
                } else if ($coursecomplete) {
                    $status = 'completed';
                } else if (!$criteriacomplete && !$ccompletion->timestarted) {
                    $status = 'notyetstarted';
                } else {
                    $status = 'inprogress';
                }

                $result->status = $status;
            }
            if ($result->status == 'notyetstarted'){
                $viewed = $DB->get_record_sql("SELECT COUNT(cmc.id) as viewed 
                                                FROM {course_modules_completion} cmc
                                                    WHERE cmc.coursemoduleid IN (SELECT id FROM {course_modules} WHERE course = $course->id) AND cmc.userid = $USER->id AND (cmc.viewed > 0 OR cmc.completionstate > 0)");
                if ($viewed->viewed){
                    $result->status = 'inprogress';
                }
            }
        }

        return $result;
    }
}


