<?php
/**
 * course_catalog block rendrer
 *
 * @package    block_course_catalog
 * @copyright  2015 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class block_course_catalog_renderer extends plugin_renderer_base {
    
    public $coursesperpage = 12;
    public $catalog_enabled = 0;
    public $catalogs = array();
    public $plans = array();
    public $run = 0;
    
    public function set_data(){
        global $USER, $OUTPUT, $CFG, $DB, $PAGE;
        
        if (!$this->run){
            require_once($CFG->dirroot. '/local/coursecatalog/locallib.php');
            $this->catalog_enabled = get_config('local_coursecatalog', 'enabled');
            if ($this->catalog_enabled){
                $this->catalogs = catalog_get_user_catalogs('list');
            }    
            
            if (get_config('local_plans', 'enabled_plans') or get_config('local_plans', 'enabled_certifications')){
                $allplans = $DB->get_records('local_plans');
                if (count($allplans)){
                    foreach($allplans as $plan){
                        $plans[$plan->id] = $plan;
                    }
                    $this->plans = $plans;
                }    
            }
            
            $this->run = 1;
        }
    }

    public function course_catalog() {
		global $USER, $OUTPUT, $CFG, $DB, $PAGE;
		
        $html = '';
        
        require_once($CFG->libdir. '/coursecatlib.php');
        
        $this->set_data();
        
        $courserenderer = $PAGE->get_renderer('core', 'course');
        $mycourseshtml = $this->catalog_print_my_courses();
        $availablecourseshtml = $this->catalog_print_available_courses();
        $available_learning_plans_html = '';
        $certifications_html = '';
        
        if (get_config('local_plans', 'enabled_plans')){
            $available_learning_plans_html = $this->catalog_print_available_learning_plans();
        }
        if (get_config('local_plans', 'enabled_certifications')){
            $certifications_html = $this->catalog_print_certifications();    
        }
        
        $categories = coursecat::make_categories_list();
        $user_view = get_user_preferences('catalog-view-type');
        
        if (empty($mycourseshtml) and empty($availablecourseshtml) and empty($available_learning_plans_html) and empty($certifications_html)) {
            return $html;
        }
        
        $PAGE->requires->js('/blocks/course_catalog/javascript/script.js', true);
        $html .= html_writer::start_tag('div', array('class'=>'nav-tabs-box'));
        
        $html .= html_writer::start_tag('div', array('class'=>'nav-tabs-header'));
            $html .= html_writer::start_tag('ul', array('class'=>'nav-tabs nav-tabs-simple clearfix'));
        
            $html .= html_writer::start_tag('li', array('class'=>'form-filter'));
                $html .= html_writer::start_tag('form', array('id' => 'catalog_coursesearch', 'class' => 'coursesearch clearfix', 'method' => 'get', 'action'=>$CFG->wwwroot.'/blocks/course_catalog/ajax.php'));
        
                // course actions block
                $html .= html_writer::start_tag('fieldset', array('class' => 'courseactionsbox clearfix', 'id'=>'course_actions'));
                
                /*$html .= html_writer::start_tag('span', array('class' => 'action-filter', 'onclick'=>'toggleFilter();'));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-align-left ', 'title'=>'Filter'));
                $html .= html_writer::end_tag('span');*/
        
                /*$html .= html_writer::start_tag('span', array('class' => 'action-sort', 'onclick'=>'toggleSort();'));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-alpha-asc', 'title'=>'Sort'));
                $html .= html_writer::end_tag('span');*/
        
                $html .= html_writer::start_tag('span', array('class' => 'action-view', 'onclick'=>'toggleView();'));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-th-list'.(($user_view != 'list') ? ' active' : ''), 'title'=>get_string('listview', 'block_course_catalog')));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-th-large'.(($user_view == 'list') ? ' active' : ''), 'title'=>get_string('gridview', 'block_course_catalog')));
                $html .= html_writer::end_tag('span');
        
                $html .= html_writer::end_tag('fieldset');
                
                // course filter block
                $user_filter = array('category'=>get_user_preferences('catalog-filter-category'), 'status'=>get_user_preferences('catalog-filter-status'), 'courses_status'=>get_user_preferences('catalog-filter-courses_status'), 'catalog'=>get_user_preferences('catalog-filter-catalog'));
                $complete_options = array(1=>get_string('completed', 'block_course_catalog'), 2=>get_string('notstarted', 'block_course_catalog'), 3=>get_string('inprogress', 'block_course_catalog'), 4=>get_string('finished', 'block_course_catalog'));
                $html .= html_writer::start_tag('fieldset', array('class' => 'coursfilterbox invisiblefieldset active clearfix', 'id'=>'course_filter'));
                if ($this->catalog_enabled){
                    $html .= html_writer::tag('span', get_string('catalog', 'block_course_catalog'));
                    $html .= html_writer::select($this->catalogs, 'catalog', $user_filter['catalog'], get_string('allcatalogs', 'block_course_catalog'),array('onchange' => 'catalogFilter("catalog");', 'id'=>'catalog_sort_catalog'));
                }
                $html .= html_writer::tag('span', get_string('category'));
                $html .= html_writer::select($categories, 'courses_cat', $user_filter['category'], get_string('allcategories', 'block_course_catalog'),array('onchange' => 'catalogFilter("category");', 'id'=>'catalog_sort_category'));
                $html .= html_writer::tag('span', get_string('coursestatus', 'block_course_catalog'));
                $html .= html_writer::select($complete_options, 'courses_status', $user_filter['courses_status'], get_string('showall', 'block_course_catalog'),array('onchange' => 'catalogFilter("courses_status");', 'id'=>'catalog_sort_courses_status'));
                $html .= html_writer::end_tag('fieldset');
        
                // course sort block
                $user_sort = array(
                    'sort-field'=> (get_user_preferences('catalog-sort-field')) ? get_user_preferences('catalog-sort-field') : 'fullname', 
                    'sort-nav'=> (get_user_preferences('catalog-sort-nav')) ? get_user_preferences('catalog-sort-nav') : 'ASC'
                );
        
                $html .= html_writer::start_tag('fieldset', array('class' => 'coursesortbox invisiblefieldset clearfix active', 'id'=>'course_sort'));
                
                $html .= html_writer::start_tag('span', array('class' => 'fullname'.(($user_sort['sort-field'] == 'fullname') ? ' active' : ''), 'onclick'=>'catalogSortCourses("fullname");'));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-asc'.(($user_sort['sort-field'] == 'fullname' and $user_sort['sort-nav'] == 'ASC') ? ' active' : '')));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-desc'.(($user_sort['sort-field'] == 'fullname' and $user_sort['sort-nav'] == 'DESC') ? ' active' : '')));
                $html .= get_string('coursename', 'block_course_catalog');
                $html .= html_writer::end_tag('span');
        
                /*$html .= html_writer::start_tag('span', array('class' => 'status'.(($user_sort['sort-field'] == 'status') ? ' active' : ''), 'onclick'=>'catalogSortCourses("status");'));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-asc'.(($user_sort['sort-field'] == 'status' and $user_sort['sort-nav'] == 'ASC') ? ' active' : '')));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-desc'.(($user_sort['sort-field'] == 'status' and $user_sort['sort-nav'] == 'DESC') ? ' active' : '')));
                $html .= 'Status';
                $html .= html_writer::end_tag('span');*/
        
                $html .= html_writer::start_tag('span', array('class' => 'startdate'.(($user_sort['sort-field'] == 'startdate') ? ' active' : ''), 'onclick'=>'catalogSortCourses("startdate");'));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-asc'.(($user_sort['sort-field'] == 'startdate' and $user_sort['sort-nav'] == 'ASC') ? ' active' : '')));
                $html .= html_writer::tag('i', '', array('class'=>'fa fa-sort-desc'.(($user_sort['sort-field'] == 'startdate' and $user_sort['sort-nav'] == 'DESC') ? ' active' : '')));
                $html .= get_string('startdate', 'block_course_catalog');
                $html .= html_writer::end_tag('span');
                $html .= html_writer::empty_tag('input', array('type'=>'hidden', 'id'=>'course_sort', 'name'=>'sort', 'value'=>'fullname'));
                $html .= html_writer::empty_tag('input', array('type'=>'hidden', 'id'=>'course_sortnav', 'name'=>'sortnav', 'value'=>'asc'));
        
                $html .= html_writer::end_tag('fieldset');
                
                $html .= html_writer::end_tag('form');
            $html .= html_writer::end_tag('li');

            if (!empty($mycourseshtml)) {
                $html .= html_writer::tag('li', '<a data-toggle="tab" href="#mycourses">'.get_string('mycourses').'</a>', array('class'=>'header-mycourses active'));
            }
        
            if (!empty($certifications_html)) {
                $html .= html_writer::tag('li', '<a data-toggle="tab" href="#catalog_certifications">'.get_string('mycertifications','block_course_catalog').'</a>', array('class'=>'header-allcertifications'));
            }
        
            if ($this->catalog_enabled){
                if (!empty($availablecourseshtml)) {
                    $html .= html_writer::tag('li', '<a data-toggle="tab" href="#availablecourses">'.get_string('coursecatalog','block_course_catalog').'</a>', array('class'=>'header-allcourses'.((empty($mycourseshtml)) ? ' active' : '')));
                }

                if (!empty($available_learning_plans_html)) {
                    $html .= html_writer::tag('li', '<a data-toggle="tab" href="#available_learning_plans">'.get_string('available_learning_plan_catalog','block_course_catalog').'</a>', array('class'=>'header-allplans'));
                }
            } else {
                if (!empty($availablecourseshtml)) {
                    $html .= html_writer::tag('li', '<a data-toggle="tab" href="#availablecourses">'.get_string('availablecourses').'</a>', array('class'=>'header-allcourses'.((empty($mycourseshtml)) ? ' active' : '')));
                }

                if (!empty($available_learning_plans_html)) {
                    $html .= html_writer::tag('li', '<a data-toggle="tab" href="#available_learning_plans">'.get_string('available_learning_plans','block_course_catalog').'</a>', array('class'=>'header-allplans'));
                }
            }
        
            $html .= html_writer::tag('li', '<a data-toggle="tab" href="#searchcourses">'.get_string('searchresults').'</a>', array('class'=>'header-searchcourses hidden'));
        
            $html .= html_writer::end_tag('ul');
        $html .= html_writer::end_tag('div');
        
        $user_view = get_user_preferences('catalog-view-type');
        $html .= html_writer::start_tag('div', array('class'=>'tab-content'.(($user_view == 'list') ? ' list-view' : ' grid-view')));
        
            if (!empty($mycourseshtml)) {
                //wrap frontpage course list in div container
                $html .= html_writer::start_tag('div', array('id'=>'mycourses', 'class'=>'tab-pane active'));
                    $html .= html_writer::start_tag('div', array('id'=>'frontpage-course-enroll-list', 'class'=>'frontpage-courses-list'));
                        $html .= html_writer::start_tag('div', array('class' => 'courses clearfix frontpage-course-list-enrolled'));
                            $html .= $mycourseshtml;
                        $html .= html_writer::end_tag('div');
                    //end frontpage course list div container
                    $html .= html_writer::end_tag('div');
                $html .= html_writer::end_tag('div');
            }
        
            if (!empty($certifications_html)) {
                //wrap frontpage course list in div container
                $html .= html_writer::start_tag('div', array('id'=>'catalog_certifications', 'class'=>'tab-pane'));
                    $html .= html_writer::start_tag('div', array('id'=>'frontpage-certifications-all-list', 'class'=>'frontpage-courses-list'));
                    $html .= html_writer::start_tag('div', array('class' => 'courses clearfix frontpage-plan-list-all'));
                        $html .= $certifications_html;
                    $html .= html_writer::end_tag('div');
                    //end frontpage course list div container
                    $html .= html_writer::end_tag('div');
                $html .= html_writer::end_tag('div');
            }
            // No "break" here. If there are no enrolled courses - continue to 'Available courses'.

            if (!empty($availablecourseshtml)) {
                //wrap frontpage course list in div container
                $html .= html_writer::start_tag('div', array('id'=>'availablecourses', 'class'=>'tab-pane'.((empty($mycourseshtml)) ? ' active' : '')));
                    $html .= html_writer::start_tag('div', array('id'=>'frontpage-course-all-list', 'class'=>'frontpage-courses-list'));
                    $html .= html_writer::start_tag('div', array('class' => 'courses clearfix frontpage-course-list-all'));
                        $html .= $availablecourseshtml;
                    $html .= html_writer::end_tag('div');
                    //end frontpage course list div container
                    $html .= html_writer::end_tag('div');
                $html .= html_writer::end_tag('div');
            }

            if (!empty($available_learning_plans_html)) {
                //wrap frontpage course list in div container
                $html .= html_writer::start_tag('div', array('id'=>'available_learning_plans', 'class'=>'tab-pane'));
                    $html .= html_writer::start_tag('div', array('id'=>'frontpage-plan-all-list', 'class'=>'frontpage-courses-list'));
                    $html .= html_writer::start_tag('div', array('class' => 'courses clearfix frontpage-plan-list-all'));
                        $html .= $available_learning_plans_html;
                    $html .= html_writer::end_tag('div');
                    //end frontpage course list div container
                    $html .= html_writer::end_tag('div');
                $html .= html_writer::end_tag('div');
            }
        
            $html .= html_writer::start_tag('div', array('id'=>'searchcourses', 'class'=>'tab-pane'));
                $html .= html_writer::start_tag('div', array('id'=>'frontpage-course-search-list', 'class'=>'frontpage-courses-list'));
                $html .= html_writer::start_tag('div', array('class' => 'courses clearfix frontpage-course-list-search'));
                    $html .= '';
                $html .= html_writer::end_tag('div');
                $html .= html_writer::end_tag('div');
            $html .= html_writer::end_tag('div');
        
        $html .= html_writer::end_tag('div');
        
        $html .= html_writer::end_tag('div');
        
				
        return $html;
    }
    
    public function catalog_print_course($course){
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        
        $output .= html_writer::start_tag('div', array(
                    'class' => 'coursebox clearfix',
                    'data-courseid' => $course->id
                ));

            // display course image
            $contentimages = ''; $image_url = '';
            $image_files = array();
            $fs = get_file_storage();
            $imgfiles = $fs->get_area_files($course->context->id, 'format_talentquest', 'thumbnail', $course->id);
            foreach ($imgfiles as $file) {
                $filename = $file->get_filename(); 
                $filetype = $file->get_mimetype(); 
                if ($filename == '.' or !$filetype) continue;
                $url = moodle_url::make_pluginfile_url($course->context->id, 'format_talentquest', 'thumbnail', $course->id, '/', $filename);
                //$contentimages .= html_writer::empty_tag('img', array('src' => $url->out()));
                $image_url = $url->out();
            }
        
            if($image_url == ""){
                //$contentimages .= html_writer::empty_tag('img', array('src' => $CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg"));
                $image_url =$CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg";
            }
        
            if($USER->id && $course->coursetype == 'enrolled'){
                $completion_info = $this->get_course_completion($course);
                
                if($completion_info->status == "completed"){
                    $contentimages .= html_writer::tag('span','',array('class'=>'course-completed', 'title'=>get_string('completed', 'block_course_catalog')));
                } elseif (isset($course->enrol) and $course->enrol == 'approval' and $course->enrolstatus > 1){
                    $contentimages .= html_writer::tag('span','',array('class'=>'course-pending', 'title'=>get_string('pendingforapproval', 'block_course_catalog')));
                }
            }
        
            $output .= html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)), html_writer::tag('div', '', array('class'=>'course-color', 'style'=>'background-color:#'.(($course->color) ? $course->color : 'aaaaaa'))).$contentimages, array('style'=>'background:url("'.$image_url.'") no-repeat scroll center center; background-size: cover;'));
        
            // course name
            $output .= html_writer::start_tag('h3', array('class' => 'coursename'));
            $output .= html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)), $course->fullname);
            
            // course dates
            if ($course->enddate > 0){
                $output .= html_writer::tag('span', get_string('duedate', 'block_course_catalog').' '.date('m/j/Y', $course->enddate), array('class' => 'course-dates'));
            } elseif ($course->startdate > 0){
                $output .= html_writer::tag('span', get_string('startdate', 'block_course_catalog').' '.date('m/j/Y', $course->startdate), array('class' => 'course-dates'));
            }
        
            $output .= html_writer::end_tag('h3');
            
            $teacher = '';
            if($course->count_teachers == 1 && !empty($course->teacher)){
                $teacher = get_string('by', 'block_course_catalog').' '.$course->teacher;
            }elseif($course->count_teachers > 1){
                $course->count_teachers--;
                $teacher = get_string('by', 'block_course_catalog').' '.$course->teacher.get_string('andother', 'block_course_catalog', $course->count_teachers);
            } 	 			
            if (!empty($teacher)){
                $output .= html_writer::tag('h4',$teacher, array('class' => 'course-teacher'));
            }   
        
            if (!empty($course->categoryname)){
                $output .= html_writer::tag('h4', get_string('category', 'block_course_catalog').$course->categoryname, array('class' => 'course-category'));
            }

            if($USER->id && $course->coursetype == 'enrolled'){

                $data = $DB->get_record_sql("SELECT
                    (SELECT (gg.finalgrade/gg.rawgrademax)*100 FROM  {grade_items} gi, {grade_grades} gg  WHERE gi.itemtype = 'course' AND gg.itemid = gi.id AND gi.courseid=$course->id AND gg.userid=$USER->id) AS average
                ");
                $progress = $completion_info->completion;

                $progress_info = html_writer::tag('div',
                                    html_writer::tag('span',get_string('course_progress','theme_talentquest'),array('class' => 'name')).
                                    html_writer::tag('span',intval($progress).'%',array('class' => 'procent')).
                                    html_writer::start_tag('div',array('class' => 'progres-bar')).
                                    html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.$progress.'%;')).
                                    html_writer::end_tag('div'),
                                    array('class'=>'clearfix')
                                );
                $grade_info = html_writer::tag('div',
                                html_writer::tag('span',get_string('avggrade','block_course_catalog'),array('class' => 'name')).
                                html_writer::tag('span',intval($data->average).'%',array('class' => 'procent')),
                                array('class'=>'clearfix avg-grade')
                            );
                
                $output .= $OUTPUT->box($progress_info.$grade_info,'user-progress clearfix');
                
                if ($course->enrol == 'plans' and !empty($course->planname) and (!isset($course->plans) or (isset($course->plans) and empty($course->plans)))){
                    $output .= html_writer::tag('div', html_writer::tag('span', ($course->plantype > 0) ? get_string('certification', 'block_course_catalog') : get_string('learningplan', 'block_course_catalog')).html_writer::link(new moodle_url('/local/plans/view.php',array('id'=>$course->planid)),$course->planname), array('class'=>'course-plan', 'title'=>$course->planname));
                }
                
            } else {
                $users = $DB->get_record_sql("SELECT COUNT( DISTINCT ra.userid) AS count_users                    
                                                                            FROM {course} AS c
                                                                            JOIN {context} AS ct ON c.id = ct.instanceid
                                                                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id 
                                                                            JOIN {user} AS u ON u.id = ra.userid
                                                                            WHERE c.id=".$course->id);
                $count_user = '<i class="fa fa-group"></i> '.$users->count_users.' users enrolled';

                if(has_capability('moodle/course:enrolreview', $course->context))
                    $output .= html_writer::tag('h4',html_writer::link(new moodle_url('/enrol/users.php',array('id'=>$course->id)),$count_user), array('class' => 'course-users'));
                else
                    $output .= html_writer::tag('h4',$count_user, array('class' => 'course-users'));
            }
        
            if (!empty($course->plans) and count($this->plans)){
                $plans = explode(',', $course->plans);
                $courseplans = array(); $planstitle = array();
                if (count($plans)){
                    foreach ($plans as $plan){
                        if (!isset($this->plans[$plan])) continue;
                        $courseplans[$this->plans[$plan]->type][$plan] = html_writer::link(new moodle_url('/local/plans/view.php',array('id'=>$plan)), $this->plans[$plan]->name);
                        $planstitle[$this->plans[$plan]->type][$plan] = $this->plans[$plan]->name;
                    }
                    if (count($courseplans[0])){
                        $output .= html_writer::tag('div', html_writer::tag('span', get_string('learningplan', 'block_course_catalog')).implode(', ', $courseplans[0]), array('class'=>'course-plan', 'title'=>implode(', ', $planstitle[0])));          
                    }
                    if (count($courseplans[1])){
                        $output .= html_writer::tag('div', html_writer::tag('span', get_string('certification', 'block_course_catalog')).implode(', ', $courseplans[1]), array('class'=>'course-certification', 'title'=>implode(', ', $planstitle[1])));          
                    }
                }
            }

        $output .= html_writer::end_tag('div');
        
        return $output;
    }

    public function catalog_print_plan($plan){
        $context = context_system::instance();
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        
        $output .= html_writer::start_tag('div', array(
            'class' => 'coursebox clearfix',
            'data-courseid' => $plan->id
        ));

        // display course image
        $contentimages = ''; $image_url = '';
        $image_files = array();
        $fs = get_file_storage();
        if($plan->image){
            $url = moodle_url::make_pluginfile_url($context->id,'local_plans','image', $plan->id,'/', $plan->image);
            $image_url = $url->out();
        }

        if($image_url == ""){
            $image_url =$CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg";
        }

        $output .= html_writer::link(new moodle_url('/local/plans/view.php', array('id' => $plan->id)), html_writer::tag('div', '', array('class'=>'course-color', 'style'=>'background-color:#'.(($plan->color) ? $plan->color : 'aaaaaa'))).$contentimages, array('style'=>'background:url("'.$image_url.'") no-repeat scroll center center; background-size: cover;'));
        
        // course name
        $output .= html_writer::tag('h3', html_writer::link(new moodle_url('/local/plans/view.php',array('id'=>$plan->id)) , $plan->name), array('class' => 'coursename'));

        // plan dates
        if ($plan->enddate > 0){
            $output .= html_writer::tag('span', get_string('duedate', 'block_course_catalog').' '.date('m/j/Y', $plan->enddate), array('class' => 'course-dates'));
        } elseif ($plan->startdate > 0){
            $output .= html_writer::tag('span', get_string('startdate', 'block_course_catalog').' '.date('m/j/Y', $plan->startdate), array('class' => 'course-dates'));
        }
        
        if (isset($plan->categoryname) and !empty($plan->categoryname)){
            $output .= html_writer::tag('h4', get_string('category', 'block_course_catalog').$plan->categoryname, array('class' => 'course-category'));
        }

        $users = $DB->get_record_sql("SELECT COUNT( DISTINCT pu.id) AS count_users FROM {local_plans_users} AS pu WHERE pu.planid=".$plan->id);

        if(has_capability('local/plans:enrol', $context))
            $output .= html_writer::tag('h4',html_writer::link(new moodle_url('/local/plans/enrol.php',array('id'=>$plan->id,'type'=>$plan->type)),$count_user), array('class' => 'course-users', 'style'=>'margin-top:5px;'));
        else
            $output .= html_writer::tag('h4',$count_user, array('class' => 'course-users'));
        
        $count_courses = '<i class="fa fa-book"></i> '. (($plan->courses) ? $plan->courses : 0) .get_string('coursesassigned', 'block_course_catalog');
        $output .= html_writer::tag('h4', $count_courses, array('class' => 'courses-assigned'));
        
        if (!empty($plan->certifications) and count($this->plans)){
            $certifications = explode(',', $plan->certifications);
            $coursecertifications = array(); $certificationstitle = array();
            if (count($certifications)){
                foreach ($certifications as $certification){
                    if (!isset($this->plans[$certification])) continue;
                    $coursecertifications[$certification] = html_writer::link(new moodle_url('/local/plans/view.php',array('id'=>$certification)), $this->plans[$certification]->name);
                    $certificationstitle[$certification] = $this->plans[$certification]->name;
                }
                if (count($coursecertifications)){
                    $output .= html_writer::tag('div', html_writer::tag('span', get_string('certification', 'block_course_catalog')).implode(', ', $coursecertifications), array('class'=>'course-certification', 'title'=>implode(', ', $certificationstitle)));          
                }
            }
        }

        $output .= html_writer::end_tag('div');

        return $output;
    }
    
    public function catalog_print_certification($certification){
        $context = context_system::instance();
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';

        $output .= html_writer::start_tag('div', array(
            'class' => 'coursebox clearfix',
            'data-courseid' => $certification->id
        ));

        // display course image
        $contentimages = ''; $image_url = '';
        $image_files = array();
        $fs = get_file_storage();
        if($certification->image){
            $url = moodle_url::make_pluginfile_url($context->id,'local_plans','image', $certification->id,'/', $certification->image);
            $image_url = $url->out();
        }

        if($image_url == ""){
            $image_url =$CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg";
        }
                
        if($certification->completed){
            $contentimages .= html_writer::tag('span','',array('class'=>'course-completed'));
            $progress = 100;
        } else {
            $progress = $this->get_plan_progress($certification);
        }

        $output .= html_writer::link(new moodle_url('/local/plans/view.php', array('id' => $certification->id)), html_writer::tag('div', '', array('class'=>'course-color', 'style'=>'background-color:#aaa')).$contentimages, array('style'=>'background:url("'.$image_url.'") no-repeat scroll center center; background-size: cover;'));
        
        // course name
        $output .= html_writer::tag('h3', html_writer::link(new moodle_url('/local/plans/view.php',array('id'=>$certification->id)) , $certification->name), array('class' => 'coursename'));

        // plan dates
        if ($certification->enddate > 0){
            $output .= html_writer::tag('span', get_string('duedate', 'block_course_catalog').' '.date('m/j/Y', $certification->enddate), array('class' => 'course-dates'));
        } elseif ($certification->startdate > 0){
            $output .= html_writer::tag('span', get_string('startdate', 'block_course_catalog').' '.date('m/j/Y', $certification->startdate), array('class' => 'course-dates'));
        }
        
        $count_courses = '<i class="fa fa-book"></i> '. (($certification->courses) ? $certification->courses : 0) .get_string('coursesassigned', 'block_course_catalog');
        $output .= html_writer::tag('h4', $count_courses, array('class' => 'courses-assigned'));
        
        $plans_courses = '<i class="fa fa-list-ul"></i> '. (($certification->plans) ? $certification->plans : 0) .get_string('plansassigned', 'block_course_catalog');
        $output .= html_writer::tag('h4', $plans_courses, array('class' => 'courses-assigned'));
        
        $progress_info = html_writer::tag('div',
                            html_writer::tag('span',get_string('progress','block_course_catalog'),array('class' => 'name')).
                            html_writer::tag('span',intval($progress).'%',array('class' => 'procent')).
                            html_writer::start_tag('div',array('class' => 'progres-bar')).
                            html_writer::tag('div','',array('class' => 'progres','style'=>'width:'.$progress.'%;')).
                            html_writer::end_tag('div'),
                            array('class'=>'clearfix')
                        );
        
        $output .= $OUTPUT->box($progress_info,'user-progress clearfix');

        $output .= html_writer::end_tag('div');

        return $output;
    }
    
    public function get_course_completion($course) {
        global $CFG, $PAGE, $DB, $USER, $OUTPUT;				
        
        require_once("{$CFG->libdir}/completionlib.php");
        
        $result = new stdClass();
        $result->completion = 0;
        $result->status = 'notyetstarted';

        // Get course completion data.
        $info = new completion_info($course);

        // Load criteria to display.
        $completions = $info->get_completions($USER->id);

        if ($info->is_tracked_user($USER->id)) {

            // For aggregating activity completion.
            $activities = array();
            $activities_complete = 0;

            // For aggregating course prerequisites.
            $prerequisites = array();
            $prerequisites_complete = 0;

            // Flag to set if current completion data is inconsistent with what is stored in the database.
            $pending_update = false;

            // Loop through course criteria.
            foreach ($completions as $completion) {
                $criteria = $completion->get_criteria();
                $complete = $completion->is_complete();

                if (!$pending_update && $criteria->is_pending($completion)) {
                    $pending_update = true;
                }

                // Activities are a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                    $activities[$criteria->moduleinstance] = $complete;

                    if ($complete) {
                        $activities_complete++;
                    }

                    continue;
                }

                // Prerequisites are also a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                    $prerequisites[$criteria->courseinstance] = $complete;

                    if ($complete) {
                        $prerequisites_complete++;
                    }

                    continue;
                }
            }

            $itemsCompleted  = $activities_complete + $prerequisites_complete;
            $itemsCount      = count($activities) + count($prerequisites);

            // Aggregate completion.
            if ($itemsCount > 0) {
                $result->completion = round(($itemsCompleted / $itemsCount) * 100);
            }

            // Is course complete?
            $coursecomplete = $info->is_course_complete($USER->id);

            // Load course completion.
            $params = array(
                'userid' => $USER->id,
                'course' => $course->id
            );
            $ccompletion = new completion_completion($params);

            // Has this user completed any criteria?
            $criteriacomplete = $info->count_course_user_data($USER->id);

            if ($pending_update) {
                $status = 'pending';
            } else if ($coursecomplete) {
                $status = 'completed';
                $result->completion = 100;
            } else if (!$criteriacomplete && !$ccompletion->timestarted) {
                $status = 'notyetstarted';
            } else {
                $status = 'inprogress';
            }

            $result->status = $status;
        }
        
        return $result;
    }
    
    public function get_plan_progress($plan){
        global $DB, $CFG, $USER;
        $progress = 0;

        $courses = $DB->get_records_sql("SELECT pc.id, pc.courseid, cc.timecompleted 
                                                FROM {local_plans_courses} pc 
                                            LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.timecompleted > 0 AND cc.userid = $USER->id
                                                WHERE pc.planid = $plan->id");

        $all_courses = 0; $completed_courses = 0;
        if (count($courses)){
            foreach ($courses as $course){
                $all_courses++;
                if ($course->timecompleted) $completed_courses++;
            }
            if ($completed_courses > 0){
                $progress = ($completed_courses/$all_courses)*100;
            }
        }

        return $progress;
    }
    
    public function catalog_print_my_courses($search = '', $page = 0){
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        
        if (!isloggedin() or isguestuser()) {
            return $output;
        }
        
        $my_courses = $this->catalog_get_my_courses($search, $this->coursesperpage, $page);
        if (count($my_courses['courses'])){
            foreach($my_courses['courses'] as $course){
                $output .= $this->catalog_print_course($course);
            }
            if ($my_courses['total'] > $this->coursesperpage * ($page+1)){
                $output .= html_writer::start_tag('div', array('class'=>'load-more-box clearfix'));
                    $output .= html_writer::tag('button', get_string('loadmore', 'block_course_catalog'), array('class'=>'btn btn-success', 'onclick'=>'catalogLoadMore('.($page+1).', "enrolled");'));
                $output .= html_writer::end_tag('div');
            }
        } else {
            $output .= html_writer::tag('div', get_string('nocourses', 'block_course_catalog'), array('class'=>'alert alert-block alert-success'));
        }
        
        return $output;
    }
    
    public function catalog_print_available_courses($search = '', $page = 0){
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        $courses = $this->catalog_get_available_courses($search, $this->coursesperpage, $page);
        
        if (count($courses['courses'])){
            foreach($courses['courses'] as $course){
                $output .= $this->catalog_print_course($course);
            }
            if ($courses['total'] > $this->coursesperpage * ($page+1)){
                $output .= html_writer::start_tag('div', array('class'=>'load-more-box clearfix'));
                    $output .= html_writer::tag('button', get_string('loadmore', 'block_course_catalog'), array('class'=>'btn btn-success', 'onclick'=>'catalogLoadMore('.($page+1).', "available");'));
                $output .= html_writer::end_tag('div');
            }
        } else {
            $output .= html_writer::tag('div', get_string('nocourses', 'block_course_catalog'), array('class'=>'alert alert-block alert-success'));
        }
        
        return $output;
    }
    
    public function catalog_print_available_learning_plans($search = '', $page = 0){
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        $plans = $this->catalog_get_learning_plans($search, $this->coursesperpage, $page);
        
        if (count($plans['courses'])){
            foreach($plans['courses'] as $plan){
                $output .= $this->catalog_print_plan($plan);
            }
            if ($plans['total'] > $this->coursesperpage * ($page+1)){
                $output .= html_writer::start_tag('div', array('class'=>'load-more-box clearfix'));
                    $output .= html_writer::tag('button', get_string('loadmore', 'block_course_catalog'), array('class'=>'btn btn-success', 'onclick'=>'catalogLoadMore('.($page+1).', "available_plans");'));
                $output .= html_writer::end_tag('div');
            }
        } else {
            $output .= html_writer::tag('div', get_string('noplans', 'block_course_catalog'), array('class'=>'alert alert-block alert-success'));
        }
        
        return $output;
    }
    
    public function catalog_print_certifications($search = '', $page = 0){
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        $certifications = $this->catalog_get_certifications($search, $this->coursesperpage, $page);
        
        if (count($certifications['courses'])){
            foreach($certifications['courses'] as $certification){
                $output .= $this->catalog_print_certification($certification);
            }
            if ($certifications['total'] > $this->coursesperpage * ($page+1)){
                $output .= html_writer::start_tag('div', array('class'=>'load-more-box clearfix'));
                    $output .= html_writer::tag('button', get_string('loadmore', 'block_course_catalog'), array('class'=>'btn btn-success', 'onclick'=>'catalogLoadMore('.($page+1).', "certifications");'));
                $output .= html_writer::end_tag('div');
            }
        } else {
            return $output;
        }
        
        return $output;
    }
    
    public function catalog_print_search_courses($search = '', $page = 0){
        global $CFG, $DB, $PAGE, $OUTPUT, $USER;
        $output = '';
        $courses = $this->catalog_search_courses($search, $this->coursesperpage, $page);
        
        if (count($courses['courses'])){
            foreach($courses['courses'] as $course){
                $output .= $this->catalog_print_course($course);
            }
            if ($courses['total'] > $this->coursesperpage * ($page+1)){
                $output .= html_writer::start_tag('div', array('class'=>'load-more-box clearfix'));
                    $output .= html_writer::tag('button', get_string('loadmore', 'block_course_catalog'), array('class'=>'btn btn-success', 'onclick'=>'catalogLoadMore('.($page+1).', "search");'));
                $output .= html_writer::end_tag('div');
            }
        } else {
            $output .= html_writer::tag('div', get_string('nocourses', 'block_course_catalog'), array('class'=>'alert alert-block alert-success'));
        }
        
        return $output;
    }
    
    function catalog_get_my_courses($search = '', $limit = 20, $page = 0) {
        global $DB, $USER, $CFG;
        
        $filter = array(
                            'category'=>get_user_preferences('catalog-filter-category'), 
                            'course_status'=>get_user_preferences('catalog-filter-courses_status'),
                            'status'=>get_user_preferences('catalog-filter-status')
                        );
        
        if ($this->catalog_enabled){
            $filter['catalog'] = get_user_preferences('catalog-filter-catalog');
        }
        
        $sorting = array(
                    'field'=> (get_user_preferences('catalog-sort-field')) ? get_user_preferences('catalog-sort-field') : 'fullname', 
                    'sort_nav'=> (get_user_preferences('catalog-sort-nav')) ? get_user_preferences('catalog-sort-nav') : 'ASC'
                );
        
        // Guest account does not have any courses
        if (isguestuser() or !isloggedin()) {
            return(array());
        }
        
        $basefields = array('id', 'category', 'sortorder',
                            'shortname', 'fullname', 'idnumber',
                            'startdate', 'visible',
                            'groupmode', 'groupmodeforce', 'cacherev', 'summary', 'format');
        $coursefields = 'c.' .join(',c.', $basefields);
        
        $cfields = array('enddate', 'status');
        $custom_fields = $DB->get_records('course_format_tq_cfields', array('state'=>1));
        if (count($custom_fields)){
            foreach ($custom_fields as $field){
                if (isset($basefields[$field->name])) continue;
                $cfields[] = $field->name;
            }
        }

        // courses sorting
        $sjoin = ""; $orderby = "";
        if (count($sorting)) {
            $sort        = (isset($sorting['field'])) ? trim($sorting['field']) : 'fullname';
            $sort_nav    = (isset($sorting['sort_nav'])) ? trim($sorting['sort_nav']) : 'ASC';
            
            if (in_array($sort, $basefields)){
                $orderby = "ORDER BY c.$sort $sort_nav";
            } elseif (in_array($sort, $cfields)){
                $sjoin = " LEFT JOIN {course_format_options} co ON co.courseid = c.id AND co.name = '".$sort."' ";
                $orderby = " ORDER BY co.value $sort_nav";
            }
        } else {
            $orderby = "ORDER BY c.fullname";
        }
        
        $fjoin = ""; $fwhere = " AND c.status = 1 "; $catalogjoin = '';
        // course catalog
        if ($this->catalog_enabled){
            $catalogjoin .= " LEFT JOIN {local_catalog_courses} lcc ON lcc.courseid = c.id";
            $catalogjoin .= " LEFT JOIN {local_coursecatalog} lc ON lc.id = lcc.catalogid";
        }
        
        // courses filter
        if (count($filter)) {
            $fcategory     = (isset($filter['category'])) ? trim($filter['category']) : '';
            $fstatus     = (isset($filter['status'])) ? trim($filter['status']) : '';
            
            if ($this->catalog_enabled){
                $fcatalog     = (isset($filter['catalog'])) ? trim($filter['catalog']) : '';
                if ($filter['catalog'] != ''){
                    $fwhere .= " AND (lcc.catalogid = ".$filter['catalog'].")";   
                }
            }
            
            if ($filter['category'] != ''){
                $fwhere .= " AND (c.category = '".$filter['category']."' OR cc.path = '/".$filter['category']."' OR cc.path LIKE '%/".$filter['category']."/%')";   
            }
            $now = time();
            if ($filter['course_status'] == 1){
                $fjoin = " LEFT JOIN {course_completions} ccom ON ccom.course = c.id AND ccom.userid = ".$USER->id;
                 $fwhere .= " AND ccom.timecompleted IS NOT NULL ";
            }elseif($filter['course_status'] == 2){ //not started
                $fwhere .= " AND c.startdate > $now";
            }elseif($filter['course_status'] == 3){ // in progress
                $fwhere .= " AND (c.enddate = 0 OR c.enddate > $now) ";
                $fwhere .= " AND c.startdate < $now";
            }elseif($filter['course_status'] == 4){ // ended
                $fwhere .= " AND (c.enddate > 0 AND c.enddate < $now)";
            }
        }
        
        $sql_mods = "";
        /*$modules = $DB->get_records_sql("SELECT id, name FROM {$CFG->prefix}modules WHERE visible = 1");
        $sql_mods = " LEFT JOIN {assign} a ON a.course = c.id";
        if (count($modules)){
            foreach ($modules as $module){
                $mod_prefix = "module_".$module->name;
                $sql_mods .= " LEFT JOIN {".$module->name."} $mod_prefix ON $mod_prefix.course = c.id";
            }
        }*/
        
        // course search
        $swhere = '';
        if (!empty($search)){
            $search_words = explode(' ', $search);
            $search_pieces = array();
            if (count($search_words)){
                foreach ($search_words as $sword){
                    $search_pieces[] = "cc.name LIKE '%$sword%'";
                    $search_pieces[] = "cc.description LIKE '%$sword%'";
                    $search_pieces[] = "cfo.value LIKE '%$sword%'";
                    $search_pieces[] = "c.shortname LIKE '%$sword%'";
                    $search_pieces[] = "c.fullname LIKE '%$sword%'";
                    $search_pieces[] = "c.summary LIKE '%$sword%'";
                    $search_pieces[] = "t.teacher LIKE '%$sword%'";
                    $search_pieces[] = "en.planname LIKE '%$sword%'";
                    /*if (count($modules)){
                        foreach ($modules as $module){
                            $mod_prefix = "module_".$module->name;
                            $search_pieces[] = "$mod_prefix.name LIKE '%$sword%'";
                            $search_pieces[] = "$mod_prefix.intro LIKE '%$sword%'";
                        }
                    }*/
                }
            }
            if (count($search_pieces)){
               $swhere = " AND (".implode(" OR ", $search_pieces).")";
            }
        }
        
        $wheres = array("c.id <> :siteid");
        $params = array('siteid'=>SITEID);
        $wheres[] = "c.visible > 0";
        $wheres[] = "cc.visible > 0";

        if (isset($USER->loginascontext) and $USER->loginascontext->contextlevel == CONTEXT_COURSE) {
            // list _only_ this course - anything else is asking for trouble...
            $wheres[] = "courseid = :loginas";
            $params['loginas'] = $USER->loginascontext->instanceid;
        }

        $ccselect = ', ' . context_helper::get_preload_record_columns_sql('ctx');
        $ccjoin = "LEFT JOIN {context} ctx ON (ctx.instanceid = c.id AND ctx.contextlevel = :contextlevel)";
        $params['contextlevel'] = CONTEXT_COURSE;
        $wheres = implode(" AND ", $wheres);

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT $coursefields $ccselect, cc.name as categoryname, cc.color, en.planname, en.plantype, en.planid, en.enrol, en.enrolstatus, t.teacher, t.count_teachers
                  FROM {course} c
                  JOIN (SELECT DISTINCT e.courseid, ue.status as enrolstatus, lp.name as planname, lp.type as plantype, lp.id as planid, e.enrol
                          FROM {enrol} e
                          JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                          LEFT JOIN {local_plans} lp ON lp.id = e.customint1
                         WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                       ) en ON (en.courseid = c.id)
                  LEFT JOIN {course_categories} cc ON cc.id = c.category
                  LEFT JOIN (SELECT c.id, CONCAT(u.firstname, ' ', u.lastname) as teacher, COUNT(ra.id) AS count_teachers 
                                FROM {course} AS c
                            JOIN {context} AS ct ON c.id = ct.instanceid
                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                            JOIN {user} AS u ON u.id = ra.userid
                            GROUP BY c.id) t ON t.id = c.id
               $catalogjoin
               $ccjoin               
               $sjoin
               $fjoin
                 LEFT JOIN {course_format_options} cfo ON cfo.courseid = c.id
                 $sql_mods
                 WHERE $wheres $fwhere $swhere
               GROUP BY c.id $orderby";
        $params['userid']  = $USER->id;
        $params['active']  = ENROL_USER_ACTIVE;
        $params['enabled'] = ENROL_INSTANCE_ENABLED;
        $params['now1']    = round(time(), -2); // improves db caching
        $params['now2']    = $params['now1'];
       
        $start = $limit * $page;        
        $courses = $DB->get_records_sql($sql, $params, $start, $limit);

        $sql = "SELECT c.id
                  FROM {course} c
                  JOIN (SELECT DISTINCT e.courseid
                          FROM {enrol} e
                          JOIN {user_enrolments} ue ON (ue.enrolid = e.id AND ue.userid = :userid)
                         WHERE ue.status = :active AND e.status = :enabled AND ue.timestart < :now1 AND (ue.timeend = 0 OR ue.timeend > :now2)
                       ) en ON (en.courseid = c.id)
                  LEFT JOIN {course_categories} cc ON cc.id = c.category
                  LEFT JOIN (SELECT c.id, CONCAT(u.firstname, ' ', u.lastname) as teacher, COUNT(ra.id) AS count_teachers 
                                FROM {course} AS c
                            JOIN {context} AS ct ON c.id = ct.instanceid
                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                            JOIN {user} AS u ON u.id = ra.userid
                            GROUP BY c.id) t ON t.id = c.id
               $catalogjoin
               $ccjoin
               $sjoin
               $fjoin
               $sql_mods
                 LEFT JOIN {course_format_options} cfo ON cfo.courseid = c.id
                 WHERE $wheres $fwhere $swhere GROUP BY c.id $orderby";
        $courses_count = $DB->get_records_sql($sql, $params);
        $total = count($courses_count);
        
        // preload contexts and check visibility
        foreach ($courses as $id=>$course) {
            context_helper::preload_from_record($course);
            $context = context_course::instance($course->id, IGNORE_MISSING);
            if (!$course->visible) {
                if (!$context) {
                    unset($courses[$id]);
                    continue;
                }
                if (!has_capability('moodle/course:viewhiddencourses', $context)) {
                    unset($courses[$id]);
                    continue;
                }
            }
            
            $all_course = course_get_format($course)->get_course();
            $all_course->context = $context;
            $all_course->coursetype = 'enrolled';
            $all_course->color = $course->color;
            $all_course->categoryname = $course->categoryname;
            $all_course->enrol = $course->enrol;
            $all_course->enrolstatus = $course->enrolstatus;
            $all_course->planname = $course->planname;
            $all_course->planid = $course->planid;
            $all_course->plantype = $course->plantype;
            $all_course->teacher = $course->teacher;
            $all_course->count_teachers = $course->count_teachers;
            $courses[$id] = $all_course;
        }

        return array('total'=>$total, 'courses'=>$courses);
    }
    
    function catalog_get_available_courses($search = '', $limit = 20, $page = 0) {
        global $DB, $USER, $CFG;
       
        // Guest account does not have any courses
        if (isguestuser() or !isloggedin()) {
            return(array());
        }
        
        $filter = array(
                            'category'=>get_user_preferences('catalog-filter-category'),
                            'course_status'=>get_user_preferences('catalog-filter-courses_status'),
                            'status'=>get_user_preferences('catalog-filter-status')
                        );
        
        if ($this->catalog_enabled){
            $filter['catalog'] = get_user_preferences('catalog-filter-catalog');
        }
        $sorting = array(
                    'field'=> (get_user_preferences('catalog-sort-field')) ? get_user_preferences('catalog-sort-field') : 'fullname', 
                    'sort_nav'=> (get_user_preferences('catalog-sort-nav')) ? get_user_preferences('catalog-sort-nav') : 'ASC'
                );
        
        $mycourses = enrol_get_my_courses();
        $my_courses_list = array();
        if (count($mycourses)){
            foreach ($mycourses as $mcourse){
                $my_courses_list[$mcourse->id] = $mcourse->id;
            }
        }
        
        $basefields = array('id', 'category', 'sortorder',
                            'shortname', 'fullname', 'idnumber',
                            'startdate', 'visible',
                            'groupmode', 'groupmodeforce', 'cacherev', 'summary', 'format');
        $coursefields = 'c.' .join(',c.', $basefields);
        
        $cfields = array('enddate', 'status');
        $custom_fields = $DB->get_records('course_format_tq_cfields', array('state'=>1));
        if (count($custom_fields)){
            foreach ($custom_fields as $field){
                if (isset($basefields[$field->name])) continue;
                $cfields[] = $field->name;
            }
        }
        
        // courses sorting
        $sjoin = ""; $orderby = "";
        if (count($sorting)) {
            $sort        = (isset($sorting['field'])) ? trim($sorting['field']) : 'fullname';
            $sort_nav    = (isset($sorting['sort_nav'])) ? trim($sorting['sort_nav']) : 'ASC';
            
            if (in_array($sort, $basefields)){
                $orderby = "ORDER BY c.$sort $sort_nav";
            } elseif (in_array($sort, $cfields)){
                $sjoin = " LEFT JOIN {course_format_options} co ON co.courseid = c.id AND co.name = '".$sort."' ";
                $orderby = " ORDER BY co.value $sort_nav";
            }
        } else {
            $orderby = "ORDER BY c.fullname";
        }
        
        $fjoin = ""; $fwhere = " AND c.status = 1 "; $catalogjoin = '';
        if ($this->catalog_enabled){
            $catalogjoin .= " LEFT JOIN {local_catalog_courses} lcc ON lcc.courseid = c.id";
            $catalogjoin .= " LEFT JOIN {local_coursecatalog} lc ON lc.id = lcc.catalogid";
            $fwhere .= " AND lc.visible > 0 ";
            if (count($this->catalogs)){
                $mycatalogs = array_keys($this->catalogs);
                $fwhere .= " AND lc.id IN (".implode(', ', $mycatalogs).") ";
            } else {
                $fwhere .= " AND lc.id = 0 ";
            }
        }
        
        // courses filter
        if (count($filter)) {
            $fcategory   = (isset($filter['category'])) ? trim($filter['category']) : '';
            $fstatus     = (isset($filter['status'])) ? trim($filter['status']) : '';
            if ($this->catalog_enabled){
                $fcatalog     = (isset($filter['catalog'])) ? trim($filter['catalog']) : '';
                if ($fcatalog != ''){
                    $fwhere .= " AND (lcc.catalogid = ".$filter['catalog'].")";   
                }
            }
            
            if ($fcategory != ''){
                $fwhere .= " AND (c.category = '".$filter['category']."' OR cc.path = '/".$filter['category']."' OR cc.path LIKE '%/".$filter['category']."/%')";   
            }
            $now = time();
            /*if ($filter['course_status'] == 1){
                $fjoin = " LEFT JOIN {course_completions} cc ON cc.course = c.id AND cc.userid = ".$USER->id;
                $fwhere .= " AND cc.timecompleted IS NOT NULL ";
            }else*/
            if($fstatus == 2){ //not started
                $fwhere .= " AND c.startdate > $now";
            }elseif($filter['course_status'] == 3){ // in progress
                $fwhere .= " AND (c.enddate = 0 OR c.enddate > $now) ";
                $fwhere .= " AND c.startdate < $now";
            }elseif($filter['course_status'] == 4){ // ended
                $fwhere .= " AND (c.enddate > 0 AND c.enddate < $now)";
            }
        }
        
        $sql_mods = "";
        /*$modules = $DB->get_records_sql("SELECT id, name FROM {$CFG->prefix}modules WHERE visible = 1");
        $sql_mods = " LEFT JOIN {assign} a ON a.course = c.id";
        if (count($modules)){
            foreach ($modules as $module){
                $mod_prefix = "module_".$module->name;
                $sql_mods .= " LEFT JOIN {".$module->name."} $mod_prefix ON $mod_prefix.course = c.id";
            }
        }*/
        
        // course search
        $swhere = '';
        if (!empty($search)){
            $search_words = explode(' ', $search);
            $search_pieces = array();
            if (count($search_words)){
                foreach ($search_words as $sword){
                    $search_pieces[] = "cc.name LIKE '%$sword%'";
                    $search_pieces[] = "cc.description LIKE '%$sword%'";
                    $search_pieces[] = "cfo.value LIKE '%$sword%'";
                    $search_pieces[] = "c.shortname LIKE '%$sword%'";
                    $search_pieces[] = "c.fullname LIKE '%$sword%'";
                    $search_pieces[] = "c.summary LIKE '%$sword%'";
                    $search_pieces[] = "t.teacher LIKE '%$sword%'";
                    /*if (count($modules)){
                        foreach ($modules as $module){
                            $mod_prefix = "module_".$module->name;
                            $search_pieces[] = "$mod_prefix.name LIKE '%$sword%'";
                            $search_pieces[] = "$mod_prefix.intro LIKE '%$sword%'";
                        }
                    }*/
                }
            }
            if (count($search_pieces)){
               $swhere = " AND (".implode(" OR ", $search_pieces).")";
            }
        }
        
        $wheres = array("c.id <> :siteid");
        $params = array('siteid'=>SITEID);
        $wheres[] = "c.visible > 0";
        $wheres[] = "cc.visible > 0";
        if (count($my_courses_list)){
            $wheres[] = "c.id NOT IN (".implode(', ', $my_courses_list).")";
        }
        $wheres = implode(" AND ", $wheres);

        if (isset($USER->loginascontext) and $USER->loginascontext->contextlevel == CONTEXT_COURSE) {
            // list _only_ this course - anything else is asking for trouble...
            $wheres[] = "courseid = :loginas";
            $params['loginas'] = $USER->loginascontext->instanceid;
        }

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT $coursefields, cc.name as categoryname, cc.color, t.teacher, t.count_teachers, GROUP_CONCAT(DISTINCT p.planid SEPARATOR ',') AS plans
                  FROM {course} c
                  LEFT JOIN {course_categories} cc ON cc.id = c.category
               $catalogjoin
               $sjoin
               $fjoin
               $sql_mods
                 LEFT JOIN {course_format_options} cfo ON cfo.courseid = c.id
                 LEFT JOIN (SELECT planid, courseid FROM {local_plans_courses}) p ON p.courseid = c.id 
                 LEFT JOIN (SELECT c.id, CONCAT(u.firstname, ' ', u.lastname) as teacher, COUNT(ra.id) AS count_teachers 
                                FROM {course} AS c
                            JOIN {context} AS ct ON c.id = ct.instanceid
                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                            JOIN {user} AS u ON u.id = ra.userid
                            GROUP BY c.id) t ON t.id = c.id
                 WHERE $wheres $fwhere $swhere
               GROUP BY c.id $orderby";
        
        $start = $limit * $page;        
        $courses = $DB->get_records_sql($sql, $params, $start, $limit);
        
        $sql = "SELECT c.id
                  FROM {course} c
                  LEFT JOIN {course_categories} cc ON cc.id = c.category
               $catalogjoin
               $sjoin
               $fjoin
               $sql_mods
                 LEFT JOIN {course_format_options} cfo ON cfo.courseid = c.id
                 LEFT JOIN (SELECT planid, courseid FROM {local_plans_courses}) p ON p.courseid = c.id 
                 LEFT JOIN (SELECT c.id, CONCAT(u.firstname, ' ', u.lastname) as teacher, COUNT(ra.id) AS count_teachers 
                                FROM {course} AS c
                            JOIN {context} AS ct ON c.id = ct.instanceid
                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                            JOIN {user} AS u ON u.id = ra.userid
                            GROUP BY c.id) t ON t.id = c.id
                 WHERE $wheres $fwhere $swhere
               GROUP BY c.id $orderby";
        $courses_count = $DB->get_records_sql($sql, $params);
        $total = count($courses_count);
        
        // preload contexts and check visibility
        foreach ($courses as $id=>$course) {
            context_helper::preload_from_record($course);
            $context = context_course::instance($course->id, IGNORE_MISSING);
            if (!$course->visible) {
                if (!$context) {
                    unset($courses[$id]);
                    continue;
                }
                if (!has_capability('moodle/course:viewhiddencourses', $context)) {
                    unset($courses[$id]);
                    continue;
                }
            }
            $all_course = course_get_format($course)->get_course();
            $all_course->context = $context;
            $all_course->coursetype = 'available';
            $all_course->categoryname = $course->categoryname;
            $all_course->color = $course->color;
            $all_course->teacher = $course->teacher;
            $all_course->plans = $course->plans;
            $all_course->count_teachers = $course->count_teachers;
            $courses[$id] = $all_course;
        }

        return array('total'=>$total, 'courses'=>$courses);
    }

    function catalog_get_learning_plans($search = '', $limit = 20, $page = 0) {
        global $DB, $USER, $CFG;

        // Guest account does not have any courses
        if (isguestuser() or !isloggedin()) {
            return(array());
        }

        $filter = array(
                            'category'=>get_user_preferences('catalog-filter-category'),
                        );
        if ($this->catalog_enabled){
            $filter['catalog'] = get_user_preferences('catalog-filter-catalog');
        }
        $sorting = array(
                    'field'=> (get_user_preferences('catalog-sort-field')) ? get_user_preferences('catalog-sort-field') : 'name',
                    'sort_nav'=> (get_user_preferences('catalog-sort-nav')) ? get_user_preferences('catalog-sort-nav') : 'ASC'
                );


        // courses sorting
        $orderby = "";
        if (count($sorting)) {
            $sort        = (isset($sorting['field']) && $sorting['field'] != 'fullname') ? trim($sorting['field']) : 'name';
            $sort_nav    = (isset($sorting['sort_nav'])) ? trim($sorting['sort_nav']) : 'ASC';

            $orderby = "ORDER BY p.$sort $sort_nav";
        } else {
            $orderby = "ORDER BY p.name";
        }


        // plan search
        $swhere = '';
        if (!empty($search)){
            $search_words = explode(' ', $search);
            $search_pieces = array();
            if (count($search_words)){
                foreach ($search_words as $sword){
                    $search_pieces[] = "p.name LIKE '%$sword%'";
                    $search_pieces[] = "p.description LIKE '%$sword%'";
                }
            }
            if (count($search_pieces)){
               $swhere .= " AND (".implode(" OR ", $search_pieces).")";
            }
        }
        
        $catalogjoin = ''; $fwhere = '';
        if ($this->catalog_enabled){
            $catalogjoin .= " LEFT JOIN {local_catalog_courses} lcc ON lcc.planid = p.id";
            $catalogjoin .= " LEFT JOIN {local_coursecatalog} lc ON lc.id = lcc.catalogid";
            $fwhere .= " AND lc.visible > 0 ";
            if (count($this->catalogs)){
                $mycatalogs = array_keys($this->catalogs);
                $fwhere .= " AND lc.id IN (".implode(', ', $mycatalogs).") ";
            } else {
                $fwhere .= " AND lc.id = 0 ";
            }
        }
        
        // plan filter
        if (count($filter)) {
            if ($this->catalog_enabled){
                $fcatalog     = (isset($filter['catalog'])) ? trim($filter['catalog']) : '';
                if ($fcatalog != ''){
                    $fwhere .= " AND (lcc.catalogid = ".$filter['catalog'].")";   
                }
            }
            
            $fcategory     = (isset($filter['category'])) ? trim($filter['category']) : '';
            if ($filter['category'] != ''){
                $fwhere .= " AND (p.category = '".$filter['category']."' OR cc.path = '/".$filter['category']."' OR cc.path LIKE '%/".$filter['category']."/%')";   
            }
        }

        $params = array();
        $wheres = "p.visible > 0 AND p.type = 0 AND p.id NOT IN (SELECT DISTINCT e.customint1 FROM {user_enrolments} ue LEFT JOIN {enrol} e ON e.id = ue.enrolid WHERE ue.userid = $USER->id AND e.enrol = 'plans') ";

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT p.*, pc.courses, cc.name as categoryname, cc.color, GROUP_CONCAT(DISTINCT c.certification SEPARATOR ',') AS certifications
                  FROM {local_plans} p
                    LEFT JOIN (SELECT planid, COUNT(id) as courses FROM {local_plans_courses} GROUP BY planid) pc ON pc.planid = p.id 
                    LEFT JOIN (SELECT certification, planid FROM {local_plans_certifications}) c ON c.planid = p.id 
                    LEFT JOIN {course_categories} cc ON cc.id = p.category
                    $catalogjoin
                 WHERE $wheres $fwhere $swhere GROUP BY p.id
                $orderby";

        $start = $limit * $page;
        $plans = $DB->get_records_sql($sql, $params, $start, $limit);

        $sql = "SELECT p.*
                  FROM {local_plans} p
                    LEFT JOIN (SELECT planid, COUNT(id) as courses FROM {local_plans_courses} GROUP BY planid) pc ON pc.planid = p.id 
                    LEFT JOIN {course_categories} cc ON cc.id = p.category
                    $catalogjoin
                 WHERE $wheres $fwhere $swhere
                $orderby";
        $plans_count = $DB->get_records_sql($sql, $params);
        $total = count($plans_count);
        
        return array('total'=>$total, 'courses'=>$plans);
    }
    
    function catalog_get_certifications($search = '', $limit = 20, $page = 0) {
        global $DB, $USER, $CFG;

        // Guest account does not have any courses
        if (isguestuser() or !isloggedin()) {
            return(array());
        }

        $filter = array();
        $sorting = array(
                    'field'=> (get_user_preferences('catalog-sort-field')) ? get_user_preferences('catalog-sort-field') : 'name',
                    'sort_nav'=> (get_user_preferences('catalog-sort-nav')) ? get_user_preferences('catalog-sort-nav') : 'ASC'
                );


        // courses sorting
        $orderby = "";
        if (count($sorting)) {
            $sort        = (isset($sorting['field']) && $sorting['field'] != 'fullname') ? trim($sorting['field']) : 'name';
            $sort_nav    = (isset($sorting['sort_nav'])) ? trim($sorting['sort_nav']) : 'ASC';

            $orderby = "ORDER BY p.$sort $sort_nav";
        } else {
            $orderby = "ORDER BY p.name";
        }


        // plan search
        $swhere = '';
        if (!empty($search)){
            $search_words = explode(' ', $search);
            $search_pieces = array();
            if (count($search_words)){
                foreach ($search_words as $sword){
                    $search_pieces[] = "p.name LIKE '%$sword%'";
                    $search_pieces[] = "p.description LIKE '%$sword%'";
                }
            }
            if (count($search_pieces)){
               $swhere .= " AND (".implode(" OR ", $search_pieces).")";
            }
        }
        
        $wheres = array();
        $params = array();
        $wheres[] = "p.type = 1";
        $wheres = implode(" AND ", $wheres);
        $wheres .= " AND p.id IN (SELECT DISTINCT e.customint1 FROM {user_enrolments} ue LEFT JOIN {enrol} e ON e.id = ue.enrolid WHERE ue.userid = $USER->id AND e.enrol = 'plans') ";

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT p.*, pc.courses, cp.plans, lpc.id as completed
                  FROM {local_plans} p
                    LEFT JOIN (SELECT planid, COUNT(id) as courses FROM {local_plans_courses} GROUP BY planid) pc ON pc.planid = p.id 
                    LEFT JOIN (SELECT certification, COUNT(id) as plans FROM {local_plans_certifications} GROUP BY certification) cp ON cp.certification = p.id 
                    LEFT JOIN {local_plans_completions} lpc ON lpc.userid = $USER->id AND lpc.planid = p.id 
                 WHERE $wheres $swhere
                $orderby";

        $start = $limit * $page;
        $certificatoions = $DB->get_records_sql($sql, $params, $start, $limit);

        $all = $DB->get_records_sql($sql, $params);
        
        $sql = "SELECT p.*
                  FROM {local_plans} p
                 WHERE $wheres $swhere
                $orderby";
        $certificatoions_count = $DB->get_records_sql($sql, $params);
        $total = count($certificatoions_count);
        
        return array('total'=>$total, 'courses'=>$certificatoions);
    }
    
    function catalog_search_courses($search = '', $limit = 20, $page = 0) {
        global $DB, $USER, $CFG;
       
        // Guest account does not have any courses
        if (isguestuser() or !isloggedin()) {
            return(array());
        }
        
        $filter = array(
                            'category'=>get_user_preferences('catalog-filter-category'),
                            'course_status'=>get_user_preferences('catalog-filter-courses_status'),
                            'status'=>get_user_preferences('catalog-filter-status')
                        );
        if ($this->catalog_enabled){
            $filter['catalog'] = get_user_preferences('catalog-filter-catalog');
        }
        $sorting = array(
                    'field'=> (get_user_preferences('catalog-sort-field')) ? get_user_preferences('catalog-sort-field') : 'fullname', 
                    'sort_nav'=> (get_user_preferences('catalog-sort-nav')) ? get_user_preferences('catalog-sort-nav') : 'ASC'
                );
        
        $mycourses = enrol_get_my_courses();
        $my_courses_list = array();
        if (count($mycourses)){
            foreach ($mycourses as $mcourse){
                $my_courses_list[$mcourse->id] = $mcourse->id;
            }
        }
        
        $basefields = array('id', 'category', 'sortorder',
                            'shortname', 'fullname', 'idnumber',
                            'startdate', 'visible',
                            'groupmode', 'groupmodeforce', 'cacherev', 'summary', 'format');
        $coursefields = 'c.' .join(',c.', $basefields);
        
        $cfields = array('enddate', 'status');
        $custom_fields = $DB->get_records('course_format_tq_cfields', array('state'=>1));
        if (count($custom_fields)){
            foreach ($custom_fields as $field){
                if (isset($basefields[$field->name])) continue;
                $cfields[] = $field->name;
            }
        }
        
        // courses sorting
        $sjoin = ""; $orderby = "";
        if (count($sorting)) {
            $sort        = (isset($sorting['field'])) ? trim($sorting['field']) : 'fullname';
            $sort_nav    = (isset($sorting['sort_nav'])) ? trim($sorting['sort_nav']) : 'ASC';
            
            if (in_array($sort, $basefields)){
                $orderby = "ORDER BY c.$sort $sort_nav";
            } elseif (in_array($sort, $cfields)){
                $sjoin = " LEFT JOIN {course_format_options} co ON co.courseid = c.id AND co.name = '".$sort."' ";
                $orderby = " ORDER BY co.value $sort_nav";
            }
        } else {
            $orderby = "ORDER BY c.fullname";
        }
        
        // course catalog
        $fwhere = " AND c.status = 1 "; $catalogjoin = '';
        if ($this->catalog_enabled){
            $catalogjoin .= " LEFT JOIN {local_catalog_courses} lcc ON lcc.courseid = c.id";
            $catalogjoin .= " LEFT JOIN {local_coursecatalog} lc ON lc.id = lcc.catalogid";
            $fwhere .= " AND lc.visible > 0 ";
            if (count($this->catalogs)){
                $mycatalogs = array_keys($this->catalogs);
                $fwhere .= " AND lc.id IN (".implode(', ', $mycatalogs).") ";
            } else {
                $fwhere .= " AND lc.id = 0 ";
            }
        }
        
        // courses filter
        $fjoin = "";
        if (count($filter)) {
            if ($this->catalog_enabled){
                $fcatalog     = (isset($filter['catalog'])) ? trim($filter['catalog']) : '';
                if ($fcatalog != ''){
                    $fwhere .= " AND (lcc.catalogid = ".$filter['catalog'].")";   
                }
            }
            
            $fcategory     = (isset($filter['category'])) ? trim($filter['category']) : '';
            $fstatus     = (isset($filter['status'])) ? trim($filter['status']) : '';
            
            if ($filter['category'] != ''){
                $fwhere .= " AND (c.category = '".$filter['category']."' OR cc.path = '/".$filter['category']."' OR cc.path LIKE '%/".$filter['category']."/%')";   
            }
            $now = time();
            if ($filter['course_status'] == 1){
                $fjoin = " LEFT JOIN {course_completions} ccom ON ccom.course = c.id AND ccom.userid = ".$USER->id;
                $fwhere .= " AND ccom.timecompleted IS NOT NULL ";
            }elseif($filter['course_status'] == 2){ //not started
                $fwhere .= " AND c.startdate > $now";
            }elseif($filter['course_status'] == 3){ // in progress
                $fwhere .= " AND (c.enddate = 0 OR c.enddate > $now) ";
                $fwhere .= " AND c.startdate < $now";
            }elseif($filter['course_status'] == 4){ // ended
                $fwhere .= " AND (c.enddate > 0 AND c.enddate < $now)";
            }
        }
        
        $sql_mods = "";
       
        
        // course search
        $swhere = '';
        if (!empty($search)){
            $search_words = explode(' ', $search);
            $search_pieces = array();
            if (count($search_words)){
                foreach ($search_words as $sword){
                    $search_pieces[] = "cc.name LIKE '%$sword%'";
                    $search_pieces[] = "cc.description LIKE '%$sword%'";
                    $search_pieces[] = "cfo.value LIKE '%$sword%'";
                    $search_pieces[] = "c.shortname LIKE '%$sword%'";
                    $search_pieces[] = "c.fullname LIKE '%$sword%'";
                    $search_pieces[] = "c.summary LIKE '%$sword%'";
                    $search_pieces[] = "t.teacher LIKE '%$sword%'";
                }
            }
            if (count($search_pieces)){
               $swhere = " AND (".implode(" OR ", $search_pieces).")";
            }
        }
        
        $wheres = array("c.id <> :siteid");
        $params = array('siteid'=>SITEID);
        $wheres[] = "c.visible > 0";
        $wheres[] = "cc.visible > 0";
        $wheres = implode(" AND ", $wheres);

        if (isset($USER->loginascontext) and $USER->loginascontext->contextlevel == CONTEXT_COURSE) {
            // list _only_ this course - anything else is asking for trouble...
            $wheres[] = "courseid = :loginas";
            $params['loginas'] = $USER->loginascontext->instanceid;
        }

        //note: we can not use DISTINCT + text fields due to Oracle and MS limitations, that is why we have the subselect there
        $sql = "SELECT $coursefields, cc.name as categoryname, cc.color, t.teacher, t.count_teachers, GROUP_CONCAT(DISTINCT p.planid SEPARATOR ',') AS plans
                  FROM {course} c
                  LEFT JOIN {course_categories} cc ON cc.id = c.category
                  LEFT JOIN (SELECT planid, courseid FROM {local_plans_courses}) p ON p.courseid = c.id 
                  LEFT JOIN (SELECT c.id, CONCAT(u.firstname, ' ', u.lastname) as teacher, COUNT(ra.id) AS count_teachers 
                                FROM {course} AS c
                            JOIN {context} AS ct ON c.id = ct.instanceid
                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                            JOIN {user} AS u ON u.id = ra.userid
                            GROUP BY c.id) t ON t.id = c.id
               $catalogjoin
               $ccjoin
               $sjoin
               $fjoin
               $sql_mods
                 LEFT JOIN {course_format_options} cfo ON cfo.courseid = c.id
                 WHERE $wheres $fwhere $swhere
               GROUP BY c.id $orderby";
        
        $start = $limit * $page;        
        $courses = $DB->get_records_sql($sql, $params, $start, $limit);
        
        $sql = "SELECT c.id
                  FROM {course} c
                  LEFT JOIN {course_categories} cc ON cc.id = c.category
                  LEFT JOIN (SELECT c.id, CONCAT(u.firstname, ' ', u.lastname) as teacher, COUNT(ra.id) AS count_teachers 
                                FROM {course} AS c
                            JOIN {context} AS ct ON c.id = ct.instanceid
                            JOIN {role_assignments} AS ra ON ra.contextid = ct.id AND ra.roleid=3 
                            JOIN {user} AS u ON u.id = ra.userid
                            GROUP BY c.id) t ON t.id = c.id
               $catalogjoin
               $ccjoin
               $sjoin
               $fjoin
               $sql_mods
                 LEFT JOIN {course_format_options} cfo ON cfo.courseid = c.id
                 WHERE $wheres $fwhere $swhere
               GROUP BY c.id $orderby";
        $courses_count = $DB->get_records_sql($sql, $params);
        $total = count($courses_count);
        
        // preload contexts and check visibility
        foreach ($courses as $id=>$course) {
            context_helper::preload_from_record($course);
            $context = context_course::instance($course->id, IGNORE_MISSING);
            if (!$course->visible) {
                if (!$context) {
                    unset($courses[$id]);
                    continue;
                }
                if (!has_capability('moodle/course:viewhiddencourses', $context)) {
                    unset($courses[$id]);
                    continue;
                }
            }
            
            $all_course = course_get_format($course)->get_course();
            if (in_array($course->id, $my_courses_list)){
                $all_course->coursetype = 'enrolled';
            } else {
                $all_course->coursetype = 'available';
            }
            $all_course->context = $context;
            $all_course->categoryname = $course->categoryname;
            $all_course->color = $course->color;
            $all_course->plans = $course->plans;
            $all_course->teacher = $course->teacher;
            $all_course->count_teachers = $course->count_teachers;
            $courses[$id] = $all_course;
        }

        return array('total'=>$total, 'courses'=>$courses);
    }

}
