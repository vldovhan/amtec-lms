<?php


$string['pluginname'] = 'Course Catalog';

$string['dashboard'] = 'Dashboard';
$string['available_learning_plans'] = 'Learning Plans';
$string['available_learning_plan_catalog'] = 'Learning Plan Catalog';
$string['coursecatalog'] = 'Course Catalog';
$string['duedate'] = 'Due Date';
$string['startdate'] = 'Start Date';
$string['by'] = 'By: ';
$string['andother'] = ' (and {$a} other)';
$string['category'] = 'Category: ';
$string['avggrade'] = 'Average Grade: ';
$string['coursename'] = 'Name';
$string['listview'] = 'List View';
$string['gridview'] = 'Grid View';
$string['startdate'] = 'Start Date';
$string['completed'] = 'Completed';
$string['notstarted'] = 'Not started';
$string['inprogress'] = 'In progress';
$string['pendingforapproval'] = 'Pending for approval';
$string['finished'] = 'Finished';
$string['learningplan'] = 'Learning Plan: ';
$string['certification'] = 'Certification: ';
$string['coursesassigned'] = ' courses assigned';
$string['noplans'] = 'No Learning Plans';
$string['loadmore'] = 'Load More';
$string['nocourses'] = 'No Courses';
$string['coursestatus'] = 'Course status';
$string['allcategories'] = 'All categories';
$string['showall'] = 'Show All';
$string['course_catalog:myaddinstance'] = 'Add Course Catalog block';
$string['course_catalog:addinstance'] = 'Add Course Catalog block';
$string['catalog'] = 'Course Catalog';
$string['allcatalogs'] = 'All Catalogs';
$string['progress'] = 'Progress';
$string['certifications'] = 'Certifications';
$string['mycertifications'] = 'My Certifications';
$string['plansassigned'] = ' plans assigned';




