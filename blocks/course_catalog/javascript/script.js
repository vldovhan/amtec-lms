$(document).ready(function(){ 
    jQuery("#tq-search").keyup(function(e){
        catalogLoadCourses(0);
    });
    if (jQuery("#tq-search").val().length){
        catalogLoadCourses(0);   
    }
});

function toggleView(){
    jQuery(".courseactionsbox .action-view i").toggleClass("active");
    if (jQuery(".block_course_catalog .tab-content").hasClass('list-view')){
        jQuery(".block_course_catalog .tab-content").removeClass("list-view");
        jQuery(".block_course_catalog .tab-content").addClass("grid-view");
        var view = 'grid';
    } else {
        jQuery(".block_course_catalog .tab-content").addClass("list-view");
        jQuery(".block_course_catalog .tab-content").removeClass("grid-view");
        var view = 'list';
        
    }
    catalogSetSettings('catalog-view-type', view, false);
}

function toggleFilter(){
    jQuery(".form-filter .coursesortbox").removeClass("active");
    jQuery(".form-filter .coursfilterbox").toggleClass("active");
}

function toggleSort(){
    jQuery(".form-filter .coursesortbox").toggleClass("active");
    jQuery(".form-filter .coursfilterbox").removeClass("active");
}

function catalogSortCourses(type, dnav){
    jQuery('.coursesortbox span').removeClass('active');
    if (jQuery('.coursesortbox .'+type+' .fa-sort-asc').hasClass('active')){
        jQuery('.coursesortbox .fa').removeClass('active');
        jQuery('.coursesortbox .'+type+' .fa-sort-desc').addClass('active');
        var nav = 'DESC';
    } else {
        jQuery('.coursesortbox .fa').removeClass('active');
        jQuery('.coursesortbox .'+type+' .fa-sort-asc').addClass('active');
        var nav = 'ASC';
    }
    jQuery('.coursesortbox .'+type).addClass('active');
    nav = (dnav) ? dnav : nav;
    
    catalogSetSettings('catalog-sort-field', type, false);
    catalogSetSettings('catalog-sort-nav', nav, true);
}

function catalogFilter(type){
    var value = jQuery('#catalog_sort_'+type).val();
    if (value){
        catalogSetSettings('catalog-filter-'+type, value, true);
    } else {
        catalogSetSettings('catalog-filter-'+type, '', true);
    }
}

function catalogSetSettings(name, value, update){
    var url = jQuery('#catalog_coursesearch').attr('action');
    jQuery.get( url+'?action=set_user_preferences&name='+name+'&value='+value, function( data ) {
        if (update){
            catalogLoadCourses(0);       
        }
    });
}

function catalogLoadCourses(page){
    var url = jQuery('#catalog_coursesearch').attr('action');
    var search = jQuery("#tq-search").val();
    if (search.length > 0){
        search = search.replace(' ', '__');
    }
    
    if (search.length){
        jQuery(".header-mycourses").addClass('hidden');
        jQuery(".header-allcourses").addClass('hidden');
        jQuery(".header-allplans").addClass('hidden');
        jQuery(".header-allcertifications").addClass('hidden');
        jQuery(".header-searchcourses").removeClass('hidden').addClass('active');
        jQuery("#mycourses").addClass('hidden');
        jQuery("#availablecourses").addClass('hidden');
        jQuery("#available_learning_plans").addClass('hidden');
        jQuery("#catalog_certifications").addClass('hidden');
        jQuery("#searchcourses").removeClass('hidden').addClass('active');
        
        if (jQuery("#frontpage-course-search-list").length){
            $("#frontpage-course-search-list .courses").addClass('loader').html('<i class="fa fa-spin fa-spinner"></i>').load( url+'?action=catalog-load-courses&type=search&page='+page+'&search='+search, function(e) {
                $("#frontpage-course-search-list .courses").removeClass('loader');
            });
        }
        
    } else {
        jQuery(".header-mycourses").removeClass('hidden');
        jQuery(".header-allcourses").removeClass('hidden');
        jQuery(".header-allplans").removeClass('hidden');
        jQuery(".header-allcertifications").removeClass('hidden');
        jQuery(".header-searchcourses").addClass('hidden').removeClass('active');
        jQuery(".header-searchcourses").addClass('hidden').removeClass('active');
        jQuery("#mycourses").removeClass('hidden');
        jQuery("#availablecourses").removeClass('hidden');
        jQuery("#available_learning_plans").removeClass('hidden');
        jQuery("#catalog_certifications").removeClass('hidden');
        jQuery("#searchcourses").addClass('hidden').removeClass('active');
        
        if (jQuery("#frontpage-course-enroll-list").length){
            $("#frontpage-course-enroll-list .courses").addClass('loader').html('<i class="fa fa-spin fa-spinner"></i>').load( url+'?action=catalog-load-courses&type=enrolled&page='+page+'&search='+search, function(e) {
                $("#frontpage-course-enroll-list .courses").removeClass('loader');
            });
        }
        if (jQuery("#frontpage-course-all-list").length){
            $("#frontpage-course-all-list .courses").addClass('loader').html('<i class="fa fa-spin fa-spinner"></i>').load( url+'?action=catalog-load-courses&type=available&page='+page+'&search='+search, function(e) {
                $("#frontpage-course-all-list .courses").removeClass('loader');
            });
        }   
        if (jQuery("#frontpage-plan-all-list").length){
            $("#frontpage-plan-all-list .courses").addClass('loader').html('<i class="fa fa-spin fa-spinner"></i>').load( url+'?action=catalog-load-courses&type=available_plans&page='+page+'&search='+search, function(e) {
                $("#frontpage-plan-all-list .courses").removeClass('loader');
            });
        }   
        if (jQuery("#frontpage-certifications-all-list").length){
            $("#frontpage-certifications-all-list .courses").addClass('loader').html('<i class="fa fa-spin fa-spinner"></i>').load( url+'?action=catalog-load-courses&type=certifications&page='+page+'&search='+search, function(e) {
                $("#frontpage-certifications-all-list .courses").removeClass('loader');
            });
        }   
    }
}

function catalogLoadMore(page, type){
    var url = jQuery('#catalog_coursesearch').attr('action');
    var search = jQuery("#tq-search").val();
    if (search.length > 0){
        search = search.replace(' ', '__');
    }
    
    if (search.length){
        jQuery(".header-mycourses").addClass('hidden');
        jQuery(".header-allcourses").addClass('hidden');
        jQuery(".header-allplans").addClass('hidden');
        jQuery(".header-allcertifications").addClass('hidden');
        jQuery(".header-searchcourses").removeClass('hidden').addClass('active');
        jQuery("#mycourses").addClass('hidden');
        jQuery("#availablecourses").addClass('hidden');
        jQuery("#available_learning_plans").addClass('hidden');
        jQuery("#catalog_certifications").addClass('hidden');
        jQuery("#searchcourses").removeClass('hidden').addClass('active');
        
        if (jQuery("#frontpage-course-search-list").length){
            $("#frontpage-course-search-list .courses .load-more-box").remove();
            jQuery("#frontpage-course-search-list .courses").addClass('loader').append('<div class="fa-loader"><i class="fa fa-spin fa-spinner"></i></div>');

            jQuery.ajax({ type: "GET",   
                 url: url+'?action=catalog-load-courses&type=search&page='+page+'&search='+search,   
                 success : function(html){
                     $("#frontpage-course-search-list .courses").append(html);
                     $("#frontpage-course-search-list .courses").removeClass('loader');
                     $("#frontpage-course-search-list .courses .fa-loader").remove();
                 }
            });
        }
        
    } else {
        jQuery(".header-mycourses").removeClass('hidden');
        jQuery(".header-allcourses").removeClass('hidden');
        jQuery(".header-allplans").removeClass('hidden');
        jQuery(".header-allcertifications").removeClass('hidden');
        jQuery(".header-searchcourses").addClass('hidden').removeClass('active');
        jQuery("#mycourses").removeClass('hidden');
        jQuery("#availablecourses").removeClass('hidden');
        jQuery("#available_learning_plans").removeClass('hidden');
        jQuery("#catalog_certifications").removeClass('hidden');
        jQuery("#searchcourses").addClass('hidden').removeClass('active');
        
        if (jQuery("#frontpage-course-enroll-list").length && type == 'enrolled'){
            $("#frontpage-course-enroll-list .courses .load-more-box").remove();
            jQuery("#frontpage-course-enroll-list .courses").addClass('loader').append('<div class="fa-loader"><i class="fa fa-spin fa-spinner"></i></div>');

            jQuery.ajax({ type: "GET",   
                 url: url+'?action=catalog-load-courses&type=enrolled&page='+page+'&search='+search,   
                 success : function(html){
                     $("#frontpage-course-enroll-list .courses").append(html);
                     $("#frontpage-course-enroll-list .courses").removeClass('loader');
                     $("#frontpage-course-enroll-list .courses .fa-loader").remove();
                 }
            });
        }
        if (jQuery("#frontpage-course-all-list").length && type == 'available'){
            $("#frontpage-course-all-list .courses .load-more-box").remove();
            $("#frontpage-course-all-list .courses").addClass('loader').append('<div class="fa-loader"><i class="fa fa-spin fa-spinner"></i></div>');
            jQuery.ajax({ type: "GET",   
                 url: url+'?action=catalog-load-courses&type=available&page='+page+'&search='+search,   
                 success : function(html){
                     $("#frontpage-course-all-list .courses").append(html);
                     $("#frontpage-course-all-list .courses").removeClass('loader');
                     $("#frontpage-course-all-list .courses .fa-loader").remove();
                 }
            });
        }
        if (jQuery("#frontpage-plan-all-list").length && type == 'available_plans'){
            $("#frontpage-plan-all-list .courses .load-more-box").remove();
            $("#frontpage-plan-all-list .courses").addClass('loader').append('<div class="fa-loader"><i class="fa fa-spin fa-spinner"></i></div>');
            jQuery.ajax({ type: "GET",
                 url: url+'?action=catalog-load-courses&type=available_plans&page='+page+'&search='+search,
                 success : function(html){
                     $("#frontpage-plan-all-list .courses").append(html);
                     $("#frontpage-plan-all-list .courses").removeClass('loader');
                     $("#frontpage-plan-all-list .courses .fa-loader").remove();
                 }
            });
        }
        if (jQuery("#frontpage-certifications-all-list").length && type == 'available_plans'){
            $("#frontpage-certifications-all-list .courses .load-more-box").remove();
            $("#frontpage-certifications-all-list .courses").addClass('loader').append('<div class="fa-loader"><i class="fa fa-spin fa-spinner"></i></div>');
            jQuery.ajax({ type: "GET",
                 url: url+'?action=catalog-load-courses&type=certifications&page='+page+'&search='+search,
                 success : function(html){
                     $("#frontpage-certifications-all-list .courses").append(html);
                     $("#frontpage-certifications-all-list .courses").removeClass('loader');
                     $("#frontpage-certifications-all-list .courses .fa-loader").remove();
                 }
            });
        }
    }
}
