<?php
global $CFG, $OUTPUT,$PAGE;		
require_once('../../config.php');

$action = required_param('action', PARAM_TEXT);

if($action == 'set_user_preferences'){
	$name = required_param('name', PARAM_TEXT);
	$value = required_param('value', PARAM_TEXT);
	
    set_user_preference($name, $value);
    echo '1';
	
}elseif($action == 'catalog-load-courses'){
	
    $type = required_param('type', PARAM_TEXT);
	$page = optional_param('page', 0, PARAM_INT);
	$search = optional_param('search', '', PARAM_TEXT);
    
    require_once($CFG->dirroot.'/course/lib.php');
    require_once($CFG->dirroot.'/course/format/lib.php');
    require_once($CFG->dirroot.'/course/format/talentquest/lib.php');
    
    $PAGE->set_pagelayout('course');
	$PAGE->set_course($SITE);

	$renderer = $PAGE->get_renderer('block_course_catalog');
    $renderer->set_data();
    
    if ($search != ''){
        $search = str_replace('__', ' ', $search);
    }

	if($type == 'enrolled'){
		$output = $renderer->catalog_print_my_courses($search, $page);
	}elseif($type == 'available'){
		$output = $renderer->catalog_print_available_courses($search, $page);
	}elseif ($type == 'search'){
	    $output = $renderer->catalog_print_search_courses($search, $page);
	}elseif ($type == 'available_plans'){
	    $output = $renderer->catalog_print_available_learning_plans($search, $page);
    }elseif ($type == 'certifications'){
	    $output = $renderer->catalog_print_certifications($search, $page);
	}
    echo $output;
}
exit;
?>