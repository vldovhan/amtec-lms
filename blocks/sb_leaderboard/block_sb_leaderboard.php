<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Handles displaying the leaderboard block.
 *
 * @package    block_sb_leaderboard
 * @copyright  2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_sb_leaderboard extends block_base {

    protected $courses = null;
    protected $blocks = array();
    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_leaderboard');
    }
	
    function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT, $PAGE;
		
		if ($this->content !== null) {
            return $this->content;
        }
        
        if (!get_config('local_gamification', 'enabled')){
            return $this->content;
        }
        
        $PAGE->requires->jquery();
        require_once($CFG->dirroot.'/local/gamification/lib.php');
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $leaderboards = gamification_get_leaderboards();
        
        if (count($leaderboards)){
            $this->content->text .= html_writer::start_tag('div', array('class' => 'leaderboard-selection'));
            if (count($leaderboards) > 1){
                $leaderboard = array_values($leaderboards)[0];
                //$this->content->text .= html_writer::tag('label', get_string('selectleaderboard', 'block_sb_leaderboard'));
                $this->content->text .= html_writer::start_tag('select', array('class' => 'leaderboard-select', 'name'=>'leaderboard-select', 'onchange'=>'leaderboardLoad()'));
                foreach ($leaderboards as $item){
                    $params = array();
                    $params['value'] = $item->id;
                    if ($leaderboard->id == $item->id) {$params['selected'] = 'selected';}
                    $this->content->text .= html_writer::tag('option', $item->name, $params);
                }
                $this->content->text .= html_writer::end_tag('select');
            } else {
                $leaderboard = array_values($leaderboards)[0];
                $this->content->text .= html_writer::tag('div', $leaderboard->name, array('class' => 'leaderboard-name'));
            }
            $this->content->text .= html_writer::end_tag('div');
            
            if ($leaderboard){
                $this->content->text .= gamification_print_leaderboard($leaderboard);   
            }
        }
        
        return $this->content;
    }
}


