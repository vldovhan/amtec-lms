<?php
define('AJAX_SCRIPT', true);
require_once("../../config.php");

$id         = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);


if($action == 'loaddayevents') {
	
    require_once($CFG->dirroot.'/calendar/lib.php');
  
    $date		= optional_param('date', '', PARAM_RAW);
    $output = ''; $events = array();
    
    if ($COURSE->id == SITEID) {
        $filtercourse = calendar_get_default_courses();
    } else {
        $filtercourse = array($courseid => $this->page->course);
    }

    list($courses, $group, $user) = calendar_set_filters($filtercourse);

    $date = (!empty($date)) ? strtotime($date) : time();

    $utime = strtotime(userdate($date));
    if ($date < $utime){
        $datetrashold = $utime-$date;
    } elseif ($date > $utimestart){
        $datetrashold = $date-$utime;
    } else {
        $datetrashold = 0;
    }
    
    $starttime = $date-$datetrashold;
    $endtime = $date+86399+$datetrashold;
    
    $all_events = calendar_get_events($starttime, $endtime, $user, $group, $courses);
    
    if (!empty($all_events)) {
        foreach ($all_events as $eventid => $event) {
             $starttime = $date; $endtime = $date+86399;
            
            if (($event->timestart >= $starttime and $event->timestart <= $endtime) or 
               ($event->timeduration > 0 and (($starttime >= $event->timestart and $starttime <= ($event->timestart+$event->timeduration)) or ($endtime >= $event->timestart and $endtime <= ($event->timestart+$event->timeduration))))){
                    $events[$eventid] = calendar_add_event_metadata($event);
            }
            
            if (!empty($event->modulename)) {
                $cm = get_coursemodule_from_instance($event->modulename, $event->instance);
                if (!\core_availability\info_module::is_user_visible($cm, 0, false)) {
                    unset($events[$eventid]);
                }
            }
        }
    }
    
    $output .= '<div class="day-title">'.get_string('eventsfor', 'block_sb_calendar').((date('Y-m-d', $starttime) == date('Y-m-d')) ? 'Today ' : '').date('F d Y', $starttime).'<i class="ion-ios-close-outline" onclick="closeEvents();"></i></div>';
    
    if (count($events) > 0){
        $output .= '<ul>';
            foreach($events as $event){
                $output .= '<li>';
                    $output .= '<div class="event-time-box">';
                        $output .= '<span>'.date('h:i a', $event->timestart).'</span>';
                        $output .= date('m.d', $event->timestart).'<br />'.date('Y', $event->timestart);
                    $output .= '</div>';
                    $output .= '<div class="event-body">';
                        $output .= '<div class="title">'.(($event->referer) ? $event->referer : html_writer::link(new moodle_url("/calendar/view.php", array('view'=>'day', 'time'=>$event->timestart)), $event->name)).'</div>';
                        $output .= strip_tags($event->description);
                        if (strlen(strip_tags($event->description)) > 110){
                            $output .= '<i class="fa fa-chevron-up"></i>';
                            $output .= '<i class="fa fa-chevron-down"></i>';
                        }
                    $output .= '</div>';
                $output .= '</li>';
            }
        $output .= '</ul>';
        $output .= '<script>jQuery(".event-body .fa").click(function(e){ jQuery(this).parent().toggleClass("open");});</script>';
    } else {
        $output .= '<div class="alert alert-success">'.((date('Y-m-d', $starttime) == date('Y-m-d')) ? get_string('noeventsfortoday', 'block_sb_calendar') : get_string('noevents', 'block_sb_calendar')).'</div>';
    }
    
    $output .= html_writer::tag('div', '<a href="'.$CFG->wwwroot.'/calendar/event.php?action=new&course=1" class="btn">'.get_string('addevents', 'block_sb_calendar').'</a>', array('class'=>'add-btn'));
    
    echo $output;

}

exit;