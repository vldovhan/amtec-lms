<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block Calendar
 *
 * @package    block_sb_calendar
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

class block_sb_calendar_renderer extends plugin_renderer_base {

    public function sb_calendar($events = array()) {
        global $CFG, $PAGE;
        
        $output = '';
        
        $output .= html_writer::start_tag('div', array('class' => 'dashboard-calendar-box'));
        
        $output .= html_writer::tag('div', get_string('upcomingevents', 'block_sb_calendar'), array('class' => 'dashboard-calendar-header'));
        $output .= html_writer::tag('div', '', array('class' => 'dashboard-calendar', 'id' => 'dashboard_calendar'));
        
        $output .= html_writer::start_tag('div', array('class' => 'dashboard-calendar-events'));
                        
        if (count($events['currentdayevents']) > 0) {
            $output .= html_writer::tag('div', get_string('eventsfor', 'block_sb_calendar').'Today '.date('F d Y').'<i class="ion-ios-close-outline" onclick="closeEvents();"></i>', array('class' => 'day-title'));
            $output .= html_writer::start_tag('ul');
            
            foreach($events['currentdayevents'] as $event) {
                $output .= html_writer::start_tag('li');
                
                    $output .= html_writer::start_tag('div', array('class'=>'event-time-box'));
                        $output .= html_writer::tag('span', date('h:i a', $event->timestart));
                        $output .= date('m.d', $event->timestart).'<br />'.date('Y', $event->timestart);
                    $output .= html_writer::end_tag('div');
                
                    $output .= html_writer::start_tag('div', array('class'=>'event-body'));
                        $output .= html_writer::tag('div', ((isset($event->referer)) ? $event->referer : html_writer::link(new moodle_url("/calendar/view.php", array('view'=>'day', 'time'=>$event->timestart)), $event->name)), array('class'=>'title'));
                        $output .= strip_tags($event->description);
                        if (strlen(strip_tags($event->description)) > 110) {
                            $output .= html_writer::tag('i', '', array('class'=>'fa fa-chevron-up'));
                            $output .= html_writer::tag('i', '', array('class'=>'fa fa-chevron-down'));
                        }
                    $output .= html_writer::end_tag('div');
                                                    
                $output .= html_writer::end_tag('li');
            }
            
            $output .= html_writer::end_tag('ul');
        } else {
            $output .= html_writer::tag('div', get_string('noeventsfortoday', 'block_sb_calendar'), array('class'=>'alert alert-success'));
        }
        
        $output .= html_writer::tag('div', '<a href="'.$CFG->wwwroot.'/calendar/event.php?action=new&course=1" class="btn">'.get_string('addevents', 'block_sb_calendar').'</a>', array('class'=>'add-btn'));

        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
        
        $output .= '<link href="'.$CFG->wwwroot.'/blocks/sb_calendar/assets/css/fullcalendar.min.css" type="text/css" media="screen" rel="stylesheet" />';
        $output .= '<script src="'.$CFG->wwwroot.'/blocks/sb_calendar/assets/javascript/moment.min.js" type="text/javascript"></script>';
        $output .= '<script src="'.$CFG->wwwroot.'/blocks/sb_calendar/assets/javascript/fullcalendar.min.js" type="text/javascript"></script>';

        $events_data = array();
        if (isset($events['events']) and count($events['events']) > 0){
            foreach($events['events'] as $event){
                $events_data[] = '{id : '.$event->id.', title: "'.addslashes($event->name).'", start: "'.date('Y-m-d h:i:s', $event->timestart).'", allDay :"true", eventBackgroundColor:"#008096"}';
            }
        }
        $events_data = implode(',', $events_data);
        
        $output .= '<script>
            jQuery(".event-body .fa").click(function(e){ jQuery(this).parent().toggleClass("open");});
            var date = new Date();
            var calendar = jQuery("#dashboard_calendar").fullCalendar({
                header: {
                    left: "prev",
                    center: "title",
                    right: "next"
                },
                dayClick: function(date, jsEvent, view) {
                    jQuery(".fc-day").removeClass("active");
                    jQuery(this).addClass("active");
                    var day = jQuery(this).attr("data-date");
                    jQuery(".dashboard-calendar-events").html("<div class=\'loading\'><i class=\'fa fa-spin fa-spinner\'></div>").load("'.$CFG->wwwroot.'/blocks/sb_calendar/ajax.php?action=loaddayevents&date="+day);
                },
                editable: false,
                disableDragging: true,
                contentHeight: 250,
                handleWindowResize: true,
                events: ['.$events_data.']
            });
            function closeEvents(){
                if (jQuery(".dashboard-calendar-events .day-title").length){
                    jQuery(".dashboard-calendar-events .day-title").remove();
                }
                if (jQuery(".dashboard-calendar-events .alert").length){
                    jQuery(".dashboard-calendar-events .alert").remove();
                }
                if (jQuery(".dashboard-calendar-events ul").length){
                    jQuery(".dashboard-calendar-events ul").remove();
                }
            }
        </script>';
        
        return $output;
    }

}
