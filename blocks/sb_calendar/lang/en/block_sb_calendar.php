<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block Calendar
 *
 * @package    block_sb_calendar
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['sb_calendar:addinstance'] = 'Add a new Calendar block';
$string['sb_calendar:myaddinstance'] = 'Add a new calendar block to My home';
$string['pluginname'] = 'Advanced Calendar';
$string['blocktitle'] = 'Calendar';
$string['noeventsfortoday'] = 'No events for today.';
$string['noevents'] = 'No events for this day.';
$string['addevents'] = 'Add Events to Calendar';
$string['eventsfor'] = 'Events for ';
$string['upcomingevents'] = 'Upcoming Events';
