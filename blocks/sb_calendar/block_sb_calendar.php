<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.



require_once($CFG->dirroot.'/calendar/lib.php');

/**
 * Handles displaying the calendar block.
 *
 * @package    block_sb_calendar
 * @copyright  2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_sb_calendar extends block_base {

    protected $timestart = null;
    protected $courses = null;
    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_calendar');
    }
	
    function hide_header() {
        return true;
    }
    
    function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT;
		
		if ($this->content !== null) {
            return $this->content;
        }
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $courseid = $this->page->course->id;
        $issite = ($courseid == SITEID);

        if ($issite) {
            // Being displayed at site level. This will cause the filter to fall back to auto-detecting
            // the list of courses it will be grabbing events from.
            $filtercourse = calendar_get_default_courses();
        } else {
            // Forcibly filter events to include only those from the particular course we are in.
            $filtercourse = array($courseid => $this->page->course);
        }

        list($courses, $group, $user) = calendar_set_filters($filtercourse);
        $events = $this->get_events($courses, $group, $user, $courseid);
            
        $renderer = $this->page->get_renderer('block_sb_calendar');
        $this->content->text = $renderer->sb_calendar($events);
        
        return $this->content;
    }
    
    
    public function get_events($courses, $groups, $users, $courseid = false) {
        global $CFG, $OUTPUT, $PAGE;

        $allevents = array(); $currentdayevents = array();
        $events = calendar_get_events(time()-31536000, time()+31536000, $users, $groups, $courses);

        // Set event course class for course events
        if (!empty($events)) {
            foreach ($events as $eventid => $event) {
                if ($event->eventtype == 'due') $event->timestart = strtotime(userdate($event->timestart));
                $events[$eventid] = calendar_add_event_metadata($event);
                if (!empty($event->modulename)) {
                    $cm = get_coursemodule_from_instance($event->modulename, $event->instance);
                    if (!\core_availability\info_module::is_user_visible($cm, 0, false)) {
                        unset($events[$eventid]);
                    }
                }
            }
        }
        $starttime = mktime (0, 0, 0, date("n"), date("j"), date("Y")); $endtime = $starttime+86399;
        
        foreach($events as $event){
            $allevents[date('Y-m-d', $event->timestart)] = $event;
            
            if (date('Y-m-d', $event->timestart) == date('Y-m-d') or ($event->timeduration > 0 and (($starttime >= $event->timestart and $starttime <= ($event->timestart+$event->timeduration)) or ($endtime >= $event->timestart and $endtime <= ($event->timestart+$event->timeduration))))){
                $currentdayevents[] = $event;
            }
            if ($event->timeduration > 0){
                if ($event->timeduration/86400 > 1){
                    $days = round($event->timeduration/86400);
                    for($i = 1; $i <= $days; $i++){
                        $event_period = clone($event);
                        $event_period->timestart = $event_period->timestart+(86400*$i);
                        $allevents[date('Y-m-d', $event_period->timestart)] = $event_period;
                    }
                }
                $event_period = clone($event);
                $event_period->timestart = $event_period->timestart+$event_period->timeduration;
                $allevents[date('Y-m-d', $event_period->timestart)] = $event_period;
            }
        }
        
        return array('events' => $allevents, 'currentdayevents' => $currentdayevents);
    }
    
}


