<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Handles displaying the calendar block.
 *
 * @package    block_sb_activity_overview
 * @copyright  2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class block_sb_activity_overview extends block_base {

    protected $courses = null;
    protected $blocks = array();
    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_activity_overview');
    }
    
    public function instance_allow_multiple() {
      return false;
    }
	
    function hide_header() {
        return false;
    }
    
    function html_attributes() {
		$attributes = parent::html_attributes();
		$attributes['class'] .= ' no_border_block block_' . $this->name();
        $attributes['style'] = 'order:10;';
        return $attributes;
    }
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $DB, $USER, $OUTPUT, $PAGE;
		
		if ($this->content !== null) {
            return $this->content;
        }
        $output = '';
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $user = $this->get_learner();
        if (!$user) return $output;
        
        $user->timespend_avg = $this->calcAvg($user->timespend_site, $user->avg->timespend_site);
        $user->visits_avg = $this->calcAvg($user->visits_site, $user->avg->visits_site);
        $user->grade_avg = $this->calcAvg($user->grade, $user->avg->grade_site);        
        $user->enrolled =  count($user->courses);
        $user->progress =  ($user->completed and $user->enrolled ) ? (($user->completed / $user->enrolled )* 100) : 0;
        $user->enrols =  ($user->available_courses and $user->enrolled ) ? (($user->enrolled / $user->available_courses)* 100) : 0;
        
        $output .= html_writer::start_tag('div', array('class'=>'overview-body'));
            $output .= html_writer::start_tag('ul', array('class'=>'overview-list'));
            
                $output .= html_writer::start_tag('li', array('class'=>'list-item'));
                    $output .= html_writer::start_tag('div', array('class'=>'title clearfix'));
                        $output .= 'Enrolled courses';
                        $output .= html_writer::tag('span', $user->enrolled.' / '.((int)$user->available_courses), array('class'=>'title-data'));
                    $output .= html_writer::end_tag('div');     
                    $output .= html_writer::start_tag('div', array('class'=>'item-data clearfix'));
                        $output .= html_writer::start_tag('div', array('class'=>'item-progress'));
                            $output .= html_writer::tag('div', '', array('class'=>'item-progress-data', 'style'=>'width:'.((int)$user->enrols).'%'));
                        $output .= html_writer::end_tag('div');    
                    $output .= html_writer::end_tag('div');
                $output .= html_writer::end_tag('li');    
        
                $output .= html_writer::start_tag('li', array('class'=>'list-item'));
                    $output .= html_writer::start_tag('div', array('class'=>'title clearfix'));
                        $output .= 'Total time on site';
                        $output .= html_writer::tag('span', $this->idate($user->timespend_avg[2]), array('class'=>'title-data'));
                    $output .= html_writer::end_tag('div');
                    $output .= html_writer::start_tag('div', array('class'=>'item-data clearfix'));
                        $output .= html_writer::tag('div', ($user->timespend_site) ? $this->idate($user->timespend_site) : $this->idate($user->timespend_avg[2]), array('class'=>'item-stats-data'));
                        $output .= html_writer::tag('div', '<i class="fa fa-sort-'.$user->timespend_avg[1].'"></i>'.(($user->timespend_avg[0]) ? $user->timespend_avg[0].'%' : '0%'), array('class'=>'item-stats-percentage '.$user->timespend_avg[1]));
                    $output .= html_writer::end_tag('div');
                $output .= html_writer::end_tag('li');
        
                $output .= html_writer::start_tag('li', array('class'=>'list-item'));
                    $output .= html_writer::start_tag('div', array('class'=>'title clearfix'));
                        $output .= 'Total visits on site';
                        $output .= html_writer::tag('span', $user->visits_avg[2], array('class'=>'title-data'));
                    $output .= html_writer::end_tag('div');     
                    $output .= html_writer::start_tag('div', array('class'=>'item-data clearfix'));
                        $output .= html_writer::tag('div', (($user->visits_site) ? (int)$user->visits_site : $user->visits_avg[2]), array('class'=>'item-stats-data'));
                        $output .= html_writer::tag('div', '<i class="fa fa-sort-'.$user->visits_avg[1].'"></i>'.(($user->visits_avg[0]) ? $user->visits_avg[0].'%' : '0%'), array('class'=>'item-stats-percentage '.$user->visits_avg[1]));
                    $output .= html_writer::end_tag('div');
                $output .= html_writer::end_tag('li');
        
                $output .= html_writer::start_tag('li', array('class'=>'list-item'));
                    $output .= html_writer::start_tag('div', array('class'=>'title clearfix'));
                        $output .= 'Average grade for all courses';
                        $output .= html_writer::tag('span', $user->grade_avg[2], array('class'=>'title-data'));
                    $output .= html_writer::end_tag('div');     
                    $output .= html_writer::start_tag('div', array('class'=>'item-data clearfix'));
                        $output .= html_writer::tag('div', round($user->grade, 2), array('class'=>'item-stats-data'));
                        $output .= html_writer::tag('div', '<i class="fa fa-sort-'.$user->grade_avg[1].'"></i>'.(($user->grade_avg[0]) ? $user->grade_avg[0].'%' : '0%'), array('class'=>'item-stats-percentage '.$user->grade_avg[1]));
                    $output .= html_writer::end_tag('div');
                $output .= html_writer::end_tag('li');
        
            $output .= html_writer::end_tag('ul');
        $output .= html_writer::end_tag('div');
        
        
        $this->content->text = $output;
        
        return $this->content;
    }
    
    function get_learner(){
		global $USER, $CFG, $DB;
        
        $userid = $USER->id;

        $user = $DB->get_record_sql("SELECT
            u.*,
            cx.id as context,
            count(c.id) as completed,
            gc.grade,
            lit.timespend_site, lit.visits_site,
            lit2.timespend_courses, lit2.visits_courses,
            lit3.timespend_modules, lit3.visits_modules,
            (SELECT count(c.id) FROM {$CFG->prefix}course c LEFT JOIN {$CFG->prefix}course_categories cc ON cc.id = c.category WHERE c.visible = 1 AND c.category > 0 AND (cc.type = 'custom' or (cc.type = 'system' AND cc.visible > 0))) as available_courses
            FROM {$CFG->prefix}user u
                LEFT JOIN {$CFG->prefix}course_completions c ON c.timecompleted > 0 AND c.userid = u.id
                LEFT JOIN {$CFG->prefix}context cx ON cx.instanceid = u.id AND contextlevel = 30
                LEFT JOIN (SELECT g.userid, AVG( (g.finalgrade/g.rawgrademax)*100) AS grade FROM {$CFG->prefix}grade_items gi, {$CFG->prefix}grade_grades g WHERE gi.itemtype = 'course' AND g.itemid = gi.id AND g.finalgrade IS NOT NULL AND g.userid = $userid) as gc ON gc.userid = u.id
                LEFT JOIN (SELECT userid, sum(timespend) as timespend_site, sum(visits) as visits_site FROM {$CFG->prefix}local_intelliboard_tracking WHERE userid = $userid) lit ON lit.userid = u.id
                LEFT JOIN (SELECT userid, sum(timespend) as timespend_courses, sum(visits) as visits_courses FROM {$CFG->prefix}local_intelliboard_tracking WHERE courseid > 0 AND userid = $userid) lit2 ON lit2.userid = u.id
                LEFT JOIN (SELECT userid, sum(timespend) as timespend_modules, sum(visits) as visits_modules FROM {$CFG->prefix}local_intelliboard_tracking WHERE page = 'module' AND userid = $userid) lit3 ON lit3.userid = u.id
            WHERE u.id = $userid");

        if($user->id){
            $user->avg = $DB->get_record_sql("SELECT a.timespend_site, a.visits_site, c.grade_site FROM
            (SELECT
                    round(avg(b.timespend_site),0) as timespend_site,
                    round(avg(b.visits_site),0) as visits_site
                FROM (SELECT sum(timespend) as timespend_site, sum(visits) as visits_site
                    FROM {$CFG->prefix}local_intelliboard_tracking
                    WHERE userid NOT IN (SELECT distinct userid FROM {$CFG->prefix}role_assignments WHERE roleid NOT IN (5)) and userid != $user->id
                    GROUP BY userid) as b) a,
                (SELECT round(AVG(b.grade),0) AS grade_site FROM (SELECT AVG( (g.finalgrade/g.rawgrademax)*100) AS grade
                FROM {$CFG->prefix}grade_items gi, {$CFG->prefix}grade_grades g
                WHERE gi.itemtype = 'course' AND g.itemid = gi.id AND g.finalgrade IS NOT NULL AND
                g.userid NOT IN (SELECT distinct userid FROM {$CFG->prefix}role_assignments WHERE roleid NOT IN (5)) and g.userid != $user->id GROUP BY g.userid) b) c");


            $user->data = $DB->get_records_sql("SELECT uif.id, uif.name, uid.data
                    FROM
                        {$CFG->prefix}user_info_field uif,
                        {$CFG->prefix}user_info_data uid
                    WHERE uif.id = uid.fieldid and uid.userid = $user->id
                    ORDER BY uif.name");

            $user->grades = $DB->get_records_sql("SELECT g.id, gi.itemmodule, round(AVG( (g.finalgrade/g.rawgrademax)*100),2) AS grade
                    FROM
                        {$CFG->prefix}grade_items gi,
                        {$CFG->prefix}grade_grades g
                    WHERE  gi.itemtype = 'mod' AND g.itemid = gi.id AND g.finalgrade IS NOT NULL and g.userid = $user->id
                    GROUP BY gi.itemmodule ORDER BY g.timecreated DESC");
            
            $user->courses = $DB->get_records_sql("SELECT
					SQL_CALC_FOUND_ROWS ue.id,
					ue.userid,
					round(((cmc.completed/cmm.modules)*100), 0) as completion,
					c.id as cid,
					c.fullname
							FROM {$CFG->prefix}user_enrolments as ue
								LEFT JOIN {$CFG->prefix}enrol as e ON e.id = ue.enrolid
								LEFT JOIN {$CFG->prefix}course as c ON c.id = e.courseid
								LEFT JOIN {$CFG->prefix}course_completions as cc ON cc.timecompleted > 0 AND cc.course = e.courseid and cc.userid = ue.userid
								LEFT JOIN (SELECT cm.course, count(cm.id) as modules FROM {$CFG->prefix}course_modules cm WHERE cm.visible = 1 AND cm.completion > 0 GROUP BY cm.course) as cmm ON cmm.course = c.id
								LEFT JOIN (SELECT cm.course, cmc.userid, count(cmc.id) as completed FROM {$CFG->prefix}course_modules cm, {$CFG->prefix}course_modules_completion cmc WHERE cmc.coursemoduleid = cm.id AND cm.visible  =  1 AND cmc.completionstate = 1 GROUP BY cm.course, cmc.userid) as cmc ON cmc.course = c.id AND cmc.userid = ue.userid
							WHERE ue.userid = $user->id GROUP BY e.courseid");

        }else{
            return false;
        }
		
		return $user;
	}
    
    function calcAvg($val, $avg){
		$calc = ($avg and $val) ? round((((($val - $avg) / $avg)*100)/10),1) : '';
		$calc = ($calc > 100) ? 100 : $calc;
		$calc = ($calc < -100) ? -100 : $calc;
		$avg = ($avg) ? round($avg, 2) : 0;
		if($calc == 0){
			$class = '';
		}elseif($calc > 0){
			$class = 'asc';
		}else{
			$class = 'desc';
		}

		return array($calc, $class, $avg);
	}
            
    function idate($t,$f=':'){
		if($t < 0){
			return "00:00:00";
		}
		if(floor($t/3600) > 999){
			return floor($t/3600) . ' h';
		}
		return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
	}
    
}


