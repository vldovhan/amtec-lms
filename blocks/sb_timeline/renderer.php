<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_sb_timeline
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

class block_sb_timeline_renderer extends plugin_renderer_base {

    public function sb_timeline($data = array(), $issite = true, $course) {
        global $CFG, $PAGE, $OUTPUT;
        
        $output = '';
        
        list($data_modules, $smodules) = $data;
        $timeline_limit = 10;
        
        $output .= '<div class="graph">';
		$output .= '<div class="timeline-header clearfix"><h2>'.get_string('pluginname', 'block_sb_timeline').'</h2></div>';
			$output .= '<div class="timeline">';
			$k=1; $i=0; $year = 0; foreach($smodules as $cmid=>$date) {
				$module = $data_modules[$cmid];
				if ($i == $timeline_limit) break;
				if($module->date != 'no-date'){
                    if ($year != date('Y', $module->date)){
                        $year = date('Y', $module->date);
                        $output .= '<div class="timesecion">'.$year.'</div>';
                    }
				} elseif($i==0 and $year == 0) {
				    $output .= '<div class="timesecion">'.date('Y').'</div>';
				}
				$output .= '<div class="timeslot clearfix'.(($k%2==0) ? ' alt' : '').'">';
					$output .= '<div class="task">'.(($issite) ? '<span>'.$module->fullname.'</span>' : '').'<a href="'.$CFG->wwwroot.'/mod/'.$module->name.'/view.php?id='.$module->id.'">'.$module->title.'</a><p><strong>Type: </strong>'.$module->name.'</p><div class="arrow"></div></div>';
					$output .= '<div class="icon">'.$OUTPUT->pix_icon('icon', $module->title, $module->name).'</div>';
					$output .= '<div class="time">'.(($module->date == 'no-date') ? 'No due date available' : 'Due: '.date('m/d/Y', strtotime(userdate($module->date))).'<br />'.date('h:i A', strtotime(userdate($module->date)))).'</div>';
				$output .= '</div>';
				$k++;$i++;
			}
            if (count($data_modules)){
                $output .= '<div class="timesecion ts-bottom">';
                    if (count($data_modules) > $timeline_limit) {
                        $output .= '<span id="load_more" class="load-more" onclick="'.(($issite) ? 'timelineLoadMore();' : 'timelineLoadMore();').'" st="'.$timeline_limit.'" pos="'.$k.'" >More</span>';
                    } else {
                        $output .= '<span class="end">'.(($course->startdate) ? date('Y', $course->startdate) : date('Y')) .'</span>';
                    }
                $output .= '</div>';
            } else {
                $output .= '<div class="alert alert-success">Nothing to display</div>';
            }
			$output .= '</div>';
		$output .= '</div>';
		
		
        
        
        return $output;
    }

}
