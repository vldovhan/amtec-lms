<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_sb_timeline
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_sb_timeline extends block_base {

    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_sb_timeline');
    }
	function hide_header() {
        return true;
    }
	
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }
	
	public function instance_allow_multiple() {
		return true;
	}

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $USER, $OUTPUT, $DB;
		
		if ($this->content !== null) {
            return $this->content;
        }
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $course = $this->page->course;
		$cm 	= $this->page->cm;
		$issite = ($course->id == SITEID);
        $context = context_course::instance($course->id, MUST_EXIST);
		
        $data = $this->get_modules();
        
        $renderer = $this->page->get_renderer('block_sb_timeline');
        $this->content->text = $renderer->sb_timeline($data, $issite, $course);
		
        return $this->content;
    }
    
    public function get_modules(){
        global $CFG, $USER, $OUTPUT, $DB;
        
        $course = $this->page->course;
		$cm 	= $this->page->cm;
		$issite = ($course->id == SITEID);
        $context = context_course::instance($course->id, MUST_EXIST);
		
		if ($issite) { 
			$modules = $DB->get_records_sql("SELECT DISTINCT(cm.id), m.name, c.fullname, cm.instance, cm.added, cm.completionexpected
                    FROM {course_modules} cm
                LEFT JOIN {modules} m ON m.id = cm.module
                LEFT JOIN {course_sections} cs ON cs.id = cm.section AND cs.course = cm.course
                LEFT JOIN {course} c ON c.id = cm.course
                LEFT JOIN {enrol} e ON e.courseid = c.id 
                LEFT JOIN {user_enrolments} ue ON ue.enrolid = e.id 
                LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = $USER->id AND cmc.completionstate = 1
                            WHERE ue.userid = $USER->id AND cs.section > 0 AND cm.visible > 0 AND cmc.id IS NULL
                                 ORDER BY cm.added ASC");
		} else {
			$modules = $DB->get_records_sql("SELECT DISTINCT(cm.id), m.name, c.fullname, cm.instance, cm.added, cm.completionexpected
                    FROM {course_modules} cm
                LEFT JOIN {modules} m ON m.id = cm.module
                LEFT JOIN {course_sections} cs ON cs.id = cm.section AND cs.course = cm.course
                LEFT JOIN {course} c ON c.id = cm.course
                LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = $USER->id AND cmc.completionstate = 1
                            WHERE c.id = $course->id AND cs.section > 0 AND cm.visible > 0 AND cmc.id IS NULL
                                 ORDER BY cm.added ASC");
		}
		
        $smodules = array();
		$data_modules = array();
        $typed_modules = array(); $sorted_modules = array();
        foreach ($modules as $module){
            $typed_modules[$module->name][$module->instance] = $module->instance;
        }
        if (count($typed_modules) > 0){
            foreach ($typed_modules as $tmodule_name=>$tmodule_ids){
                if (count($tmodule_ids) > 0){
                    $tmodules = $DB->get_records_sql("SELECT * FROM {".$tmodule_name."} WHERE id IN (".implode(', ', $tmodule_ids).")");
                    foreach ($tmodules as $tmodule){
                        $sorted_modules[$tmodule_name][$tmodule->id] = $tmodule;
                    }
                }
            }
        }
		foreach ($modules as $module){
            $mod = isset($sorted_modules[$module->name][$module->instance]) ? $sorted_modules[$module->name][$module->instance] : null;
			$module->title = isset($mod->name) ? $mod->name : '';
            if($module->completionexpected){
				$module->date = $module->completionexpected;
            }elseif(isset($mod->duedate) and $mod->duedate != '0'){
				$module->date = $mod->duedate;
            }elseif(isset($mod->timeclose) and $mod->timeclose != '0'){
				$module->date = $mod->timeclose;
			}else{
				$module->date = 'no-date';
			}
            
			if ($module->date != 'no-date' and $module->date <= time()) continue;
			
			$data_modules[$module->id] = $module;
			$smodules[$module->id] = $module->date;
		}
		asort($smodules);
        
        return array($data_modules, $smodules);
    }
}


