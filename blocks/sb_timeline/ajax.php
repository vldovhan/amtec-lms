<?php

require_once('../../config.php');

$id		    = optional_param('id', 0, PARAM_INT);
$type		= optional_param('type', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);

if($action == 'load_timeline') {
    
	$timeline_limit = 10;
    $start		= optional_param('start', 0, PARAM_INT);
	$k		= optional_param('k', 0, PARAM_INT);
	$result = '';
    
    if ($id > 1){
    
        $modules = $DB->get_records_sql("SELECT cm.id, m.name, c.fullname, cm.instance, cm.added, cm.completionexpected, cmc.id as cmcid  
                                                        FROM {course_modules} cm
                                                    LEFT JOIN {modules} m ON m.id = cm.module
                                                    LEFT JOIN {course_sections} cs ON cs.id = cm.section AND cs.course = cm.course
                                                    LEFT JOIN {course} c ON c.id = cm.course
                                                    LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = $USER->id AND cmc.completionstate = 1 AND cm.section > 0 
                                                                WHERE c.id = $id AND cmc.id IS NULLAND cs.section > 0 AND cm.visible > 0 
                                                                     ORDER BY cm.added ASC");
        $smodules = array();
        $data_modules = array();
        foreach ($modules as $module){
            $mod = $DB->get_record_sql("SELECT * FROM {".$module->name."} WHERE id = ".$module->instance);
            $module->title = $mod->name;
            if($module->completionexpected){
                $module->date = $module->completionexpected;
            }elseif(isset($mod->timeclose) and $mod->timeclose != '0'){
                $module->date = $mod->timeclose;
            }elseif(isset($mod->duedate) and $mod->duedate != '0'){
                $module->date = $mod->duedate;
            }else{
                $module->date = 'no-date';
            }
            if ($module->date != 'no-date' and $module->date <= time()) continue;
            
            $data_modules[$module->id] = $module;
            $smodules[$module->id] = $module->date;
        }
        asort($smodules);
    } else {
        $modules = $DB->get_records_sql("SELECT cm.id, m.name, c.fullname, cm.instance, cm.added, cm.completionexpected, cmc.id as cmcid 
													FROM {course_modules} cm
												LEFT JOIN {modules} m ON m.id = cm.module
                                                LEFT JOIN {course_sections} cs ON cs.id = cm.section AND cs.course = cm.course
												LEFT JOIN {course} c ON c.id = cm.course
												LEFT JOIN {enrol} e ON e.courseid = c.id 
												LEFT JOIN {user_enrolments} ue ON ue.enrolid = e.id 
												LEFT JOIN {course_modules_completion} cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = $USER->id AND cmc.completionstate = 1
															WHERE ue.userid = $USER->id AND cmc.id IS NULL AND cs.section > 0 AND cm.visible > 0 
																 ORDER BY cm.added ASC");
	
        $smodules = array();
        $data_modules = array();
        foreach ($modules as $module){
            $mod = $DB->get_record_sql("SELECT * FROM {".$module->name."} WHERE id = ".$module->instance);
            $module->title = $mod->name;
            if(isset($mod->timeclose) and $mod->timeclose != '0'){
                $module->date = $mod->timeclose;
            }elseif(isset($mod->duedate) and $mod->duedate != '0'){
                $module->date = $mod->duedate;
            }elseif($module->completionexpected){
                $module->date = $module->completionexpected;
            }else{
                $module->date = 'no-date';
            }
            if ($module->date != 'no-date' and $module->date <= time()) continue;
            
            $data_modules[$module->id] = $module;
            $smodules[$module->id] = $module->date;
        }
        asort($smodules);
    }
	
	$year = 0; $i=0;
	foreach($smodules as $cmid=>$date) {
		$module = $data_modules[$cmid];
		$i++;
		if ($i <= $start) continue;
		if ($i > $start+$timeline_limit) break;
		$result .= '<div class="timeslot clearfix'.(($k%2==0) ? ' alt' : '').'">
			<div class="task">
				<a href="'.$CFG->wwwroot.'/mod/'.$module->name.'/view.php?id='.$module->id.'">'.$module->title.'</a>
				<p><strong>Type:</strong> '.$module->name.'</p>
				<div class="arrow"></div>
			</div>							
			<div class="icon">
				'.$OUTPUT->pix_icon('icon', $module->title, $module->name).'
			</div>
			<div class="time">'.(($module->date == 'no-date') ? 'No due date available' : 'Due: '.date('m/d/Y', strtotime(userdate($module->date))).'<br />'.date('h:i A', strtotime(userdate($module->date)))).'</div>
		</div>';
		$k++;
	}
    if (count($data_modules)){
        $result .= '<div class="timesecion ts-bottom">';
            if ($timeline_limit+$start < count($data_modules)) {
                $result .= '<span id="load_more" class="load-more" onclick="timelineLoadMore();" st="'.($timeline_limit+$start).'" pos="'.$k.'" >More</span>';
            } else {
                $result .= '<span class="end">'.(($course->startdate) ? date('Y', $course->startdate) : date('Y')) .'</span>';
            }
        $result .= '</div>';
    } else {
        $result .= '<div class="alert alert-success">Nothing to display</div>';
    }
	echo $result;
}
exit;
