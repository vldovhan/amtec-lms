<?php
/**
 * Schools statistic
 *
 * @package    block_talentquest_statistic
 * @copyright  2015 SEBALE (http://sebale.net)
 */

defined('MOODLE_INTERNAL') || die();

$capabilities = array(

    'block/talentquest_menu:myaddinstance' => array(
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'user' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:config'
    ),

    'block/talentquest_menu:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:config'
    )
);
