<?php
/**
 * talentquest_menu block rendrer
 *
 * @package    block_talentquest_menu
 * @copyright  2015 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class block_talentquest_menu_renderer extends plugin_renderer_base {

    public function talentquest_menu() {
		global $USER, $OUTPUT,$CFG,$DB,$PAGE;
		
        $html = '';
        $html .= html_writer::start_tag('ul', array('class'=>'user-left_menu'));
        
            $html .= html_writer::start_tag('li', array('class'=>'dashboard'));
                $content = html_writer::tag('i','',array('class'=>'fa fa-graduation-cap','title'=>get_string('dashboard','block_talentquest_menu')));
                $html .= html_writer::link(new moodle_url('/my/'),$content,array('title'=>get_string('dashboard','block_talentquest_menu')));
            $html .= html_writer::end_tag('li');
			
            /*$html .= html_writer::start_tag('li', array('class'=>'slide-out'));
                $content = html_writer::tag('i','',array('class'=>'fa fa-list-alt','title'=>get_string('courses','block_talentquest_menu')));
                $html .= html_writer::link(new moodle_url('/my/'),$content,array('title'=>get_string('courses','block_talentquest_menu'), 'data-toggle'=>'dropdown'));
            $html .= html_writer::end_tag('li');*/
			
            $html .= html_writer::start_tag('li', array('class'=>((strstr($PAGE->url, '/calendar/')) ? 'active' : '')));
                $content = html_writer::tag('i','',array('class'=>'fa fa-calendar','title'=>get_string('calendar', 'calendar')));
                $html .= html_writer::link(new moodle_url('/calendar/view.php', array('view'=>'month')),$content,array('title'=>get_string('calendar', 'calendar')));
            $html .= html_writer::end_tag('li');
        
            if (get_config('local_gamification', 'enabled')){        
                $html .= html_writer::start_tag('li', array('class'=>((strstr($PAGE->url, '/badges/mybadges.php')) ? 'active' : '')));
                    $content = html_writer::tag('i','',array('class'=>'fa fa-trophy','title'=>get_string('badges')));
                    $html .= html_writer::link(new moodle_url('/badges/mybadges.php'),$content,array('title'=>get_string('badges')));
                $html .= html_writer::end_tag('li');
            }
         
            $html .= html_writer::start_tag('li', array('class'=>'settings'.((get_user_preferences('pin-sidebar',0,$USER)) ? ' active' : '')));
                $content = html_writer::tag('i','',array('class'=>'fa fa-cogs','title'=>'Manage'));
                $html .= html_writer::link('javascript:void(0);',$content,array('title'=>'Manage', 'onclick'=>'toggleSidePre("settings");'));
            $html .= html_writer::end_tag('li');

            $html .= html_writer::start_tag('li', array('class'=>'language-menu','id'=>'language-menu'));
                $content = html_writer::tag('i','',array('class'=>'fa fa-globe','title'=>'Language'));
                $html .= html_writer::link('javascript:void(0);',$content,array('title'=>'Language', 'onclick'=>'toggleSideLanguage(this);'));
            $html .= html_writer::end_tag('li');

            /*if($PAGE->course->id > 1){
                $html .= html_writer::start_tag('li',array('class'=>'course_nav togle '.((get_user_preferences('theme_courseside_sidebar') == null or get_user_preferences('theme_courseside_sidebar') > 0) ? ' active' : '')));
                    $content = html_writer::tag('i', '', array('class' => 'fa fa-angle-right', 'title' => 'Course navigation'));
                    $html .= html_writer::link('#', $content, array('title' => 'Course navigation', 'onclick' => 'toggleCourseSide();', 'class' => 'course-nav-toggler'));
                $html .= html_writer::end_tag('li');

                $html .= html_writer::start_tag('li',array('class'=>'course_nav'));
                    $content = html_writer::tag('i', '', array('class' => 'fa fa-laptop', 'title' => 'Blog'));
                    $html .= html_writer::link('#', $content, array('title' => 'Blog'));
                $html .= html_writer::end_tag('li');

                $html .= html_writer::start_tag('li',array('class'=>'course_nav'));
                    $content = html_writer::tag('i', '', array('class' => 'fa fa-bar-chart', 'title' => 'Grages'));
                    $html .= html_writer::link('#', $content, array('title' => 'Grages'));
                $html .= html_writer::end_tag('li');
            }*/
			
        $html .= html_writer::end_tag('ul');
        
        
        $html .= $this->get_menu_blocks();
				
        return $html;
    }
    
    public function get_menu_blocks() {
		global $USER, $OUTPUT,$CFG,$DB;
        
        $html = '';
        $active_sidebar = get_user_preferences('active-sidebar', '', $USER);
        
        $html .= html_writer::start_tag('div', array('class'=>'sibebar-menu-box'));
           
            $html .= html_writer::start_tag('div', array('class'=>'content-block'.(($active_sidebar == 'manage') ? '' : ' hidden'), 'id'=>'sm-manage'));
                $html .= html_writer::start_tag('ul', array('class'=>'simple-menu'));
                    $html .= html_writer::start_tag('li');
                        $html .= html_writer::link(new moodle_url('/local/manage/categories.php'),'<i class="fa fa-circle-o"></i>Categories',array('title'=>'Categories'));
                    $html .= html_writer::end_tag('li');
                    $html .= html_writer::start_tag('li');
                        $html .= html_writer::link(new moodle_url('/local/manage/courses.php'),'<i class="fa fa-circle-o"></i>'.get_string('courses'),array('title'=>get_string('courses')));
                    $html .= html_writer::end_tag('li');
                    $html .= html_writer::start_tag('li');
                        $html .= html_writer::link(new moodle_url('/local/manage/users.php'),'<i class="fa fa-circle-o"></i>'.get_string('users'),array('title'=>get_string('users')));
                    $html .= html_writer::end_tag('li');
                $html .= html_writer::end_tag('ul');
            $html .= html_writer::end_tag('div');
        
        $html .= html_writer::end_tag('div');
    
        return $html;
    }
}
