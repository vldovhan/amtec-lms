<?php


$string['pluginname'] = 'Talentquest menu';
$string['talentquest_menu:addinstance'] = 'Add Talentquest menu block';
$string['talentquest_menu:myaddinstance'] = 'Add Talentquest menu block';

$string['setting_display_dashboard'] = 'Display dashboard';
$string['setting_display_notifications'] = 'Display notifications';
$string['setting_display_report_card'] = 'Display report card';
$string['setting_display_search'] = 'Display search';
$string['setting_display_logout'] = 'Display logout';
$string['setting_display_courses'] = 'Display courses';

$string['dashboard'] = 'Dashboard';
$string['notifications'] = 'Notifications';
$string['report_card'] = 'Report card';
$string['search'] = 'Search';
$string['logout'] = 'Log out';
$string['courses'] = 'Courses';
$string['grades_boock'] = 'Gradebook';
$string['blog'] = 'Blog';



