<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * course_overview block settings
 *
 * @package    block_course_overview
 * @copyright  2012 Adam Olley <adam.olley@netspot.com.au>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
	$settings->add(new admin_setting_configcheckbox('block_talentquest_menu/talentquest_menu_display_dashboard', get_string('setting_display_dashboard', 'block_talentquest_menu'),'', 1));
	$settings->add(new admin_setting_configcheckbox('block_talentquest_menu/talentquest_menu_display_courses', get_string('setting_display_courses', 'block_talentquest_menu'),'', 1));
	$settings->add(new admin_setting_configcheckbox('block_talentquest_menu/talentquest_menu_display_notifications', get_string('setting_display_notifications', 'block_talentquest_menu'),'', 1));
	$settings->add(new admin_setting_configcheckbox('block_talentquest_menu/talentquest_menu_display_report_card', get_string('setting_display_report_card', 'block_talentquest_menu'),'', 1));
	$settings->add(new admin_setting_configcheckbox('block_talentquest_menu/talentquest_menu_display_search', get_string('setting_display_search', 'block_talentquest_menu'),'', 1));
	$settings->add(new admin_setting_configcheckbox('block_talentquest_menu/talentquest_menu_display_logout', get_string('setting_display_logout', 'block_talentquest_menu'),'', 1));
}
