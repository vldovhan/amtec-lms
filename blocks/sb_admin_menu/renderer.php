<?php
/**
 * sb_admin_menu block rendrer
 *
 * @package    block_sb_admin_menu
 * @copyright  2015 SEBALE (http://sebale.net)
 */
defined('MOODLE_INTERNAL') || die;

class block_sb_admin_menu_renderer extends plugin_renderer_base {

    public function sb_admin_menu() {
		global $USER, $OUTPUT, $CFG, $DB, $COURSE;
        
        $systemcontext   = context_system::instance();
        if ($COURSE->id > 1){
            $coursecontext = context_course::instance($COURSE->id);
        }
		
        $html = '';
        
            if(has_capability('local/manager:manageusers', $systemcontext) or has_capability('moodle/cohort:view', $systemcontext)){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-users'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('users','block_sb_admin_menu'), array('title'=>get_string('users','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));
                    
                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));
                    if(has_capability('local/manager:manageusers', $systemcontext)){
                        $html .= html_writer::start_tag('li');                
                            $html .= html_writer::link(new moodle_url('/local/manager/users/index.php'), get_string('manageusers','block_sb_admin_menu'), array('title'=>get_string('manageusers','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');
                    }
                    if(has_capability('moodle/cohort:view', $systemcontext)){
                        $html .= html_writer::start_tag('li');                
                            $html .= html_writer::link(new moodle_url('/cohort/index.php'), get_string('manageusergroups','block_sb_admin_menu'), array('title'=>get_string('manageusergroups','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');
                    }
                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
        
            if(has_capability('local/gamification:manage', $systemcontext) or has_capability('local/gamification:view', $systemcontext)){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-trophy'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('gamification','block_sb_admin_menu'), array('title'=>get_string('gamification','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));
                    
                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));
                        if (has_capability('local/gamification:view', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/gamification/index.php'), get_string('leaderboardsresults','block_sb_admin_menu'), array('title'=>get_string('leaderboardsresults','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                        if (has_capability('local/gamification:manage', $systemcontext) or has_capability('local/gamification:managebadges', $systemcontext) ){
                            $html .= html_writer::start_tag('li'); 
                                $params = array('type'=>1);
                                $html .= html_writer::link(new moodle_url('/badges/index.php', $params), get_string('managebadges','block_sb_admin_menu'), array('title'=>get_string('managebadges','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                        if (has_capability('local/gamification:manage', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/gamification/contests.php'), get_string('gamificationcontests','block_sb_admin_menu'), array('title'=>get_string('gamificationcontests','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                    $html .= html_writer::end_tag('ul');     
                $html .= html_writer::end_tag('li');
            }
        
            if(has_capability('local/manager:managecourses', $systemcontext) or (has_capability('moodle/competency:competencymanage', $systemcontext)) or (has_capability('local/coursecatalog:manage', $systemcontext) and get_config('local_coursecatalog', 'enabled')) or (has_capability('local/plans:manage', $systemcontext) and get_config('local_plans', 'enabled_plans')) or (has_capability('local/plans:enrol', $systemcontext) and get_config('local_plans', 'enabled_plans')) /*or $COURSE->id > 1 and has_capability('moodle/course:enrolreview', $coursecontext)*/ or (has_capability('moodle/category:manage', $systemcontext) and has_capability('local/manager:managecategories', $systemcontext)) or (has_capability('local/transcripts:view', $systemcontext) and get_config('local_transcripts', 'enabled')) or has_capability('local/manager:manageapprovals', $systemcontext)){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-book'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('coursesandcurriculum','block_sb_admin_menu'), array('title'=>get_string('coursesandcurriculum','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));
                    
                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));
                        
                        if(has_capability('local/manager:managecourses', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/manager/courses/index.php'), get_string('managecourses','block_sb_admin_menu'), array('title'=>get_string('managecourses','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                
                        if(has_capability('moodle/category:manage', $systemcontext) and has_capability('local/manager:managecategories', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/manager/categories/index.php'), get_string('managecoursecategories', 'block_sb_admin_menu'), array('title'=>get_string('managecoursecategories', 'block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                
                        if(has_capability('local/coursecatalog:manage', $systemcontext) and get_config('local_coursecatalog', 'enabled')){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/coursecatalog/index.php'),
                                get_string('managecoursecatalogs', 'block_sb_admin_menu'), array('title'=>get_string('managecoursecatalogs', 'block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                
                        /*$html .= html_writer::start_tag('li');                
                            $html .= html_writer::link('#', get_string('manageclassrooms','block_sb_admin_menu'), array('title'=>get_string('manageclassrooms','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');*/
                
                        if (has_capability('moodle/competency:competencymanage', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/admin/tool/lp/competencyframeworks.php', array('pagecontextid'=>1)), get_string('managecompetencies','block_sb_admin_menu'), array('title'=>get_string('managecompetencies','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                
                        /*$html .= html_writer::start_tag('li');                
                            $html .= html_writer::link('#', get_string('managegrades','block_sb_admin_menu'), array('title'=>get_string('managecompetencies','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');*/
                
                        if ((has_capability('local/plans:manage', $systemcontext) or has_capability('local/plans:enrol', $systemcontext)) and get_config('local_plans', 'enabled_plans')) {
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/plans/index.php'), get_string('managelearningplans','block_sb_admin_menu'), array('title'=>get_string('managelearningplans','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                        
                        if (has_capability('local/certificate:manage', $systemcontext)) {
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/certificate/certificates.php'), get_string('managecertificates', 'block_sb_admin_menu'), array('title'=>get_string('managecertificates', 'block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        } elseif (has_capability('local/certificate:view', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/certificate/user-certificate.php'), get_string('managecertificates', 'block_sb_admin_menu'), array('title'=>get_string('managecertificates', 'block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }
                
                        /*if ($COURSE->id > 1 and has_capability('moodle/course:enrolreview', $coursecontext)){
                            $html .= html_writer::start_tag('li'); 
                                $html .= html_writer::link(new moodle_url('/enrol/instances.php', array('id'=>$COURSE->id)), get_string('manageenrollmentrules','block_sb_admin_menu'), array('title'=>get_string('manageenrollmentrules','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }*/
                
                        if (has_capability('local/transcripts:view', $systemcontext) and get_config('local_transcripts', 'enabled')){
                            $html .= html_writer::start_tag('li');                
                                if (has_capability('local/transcripts:report', $systemcontext)){
                                    $html .= html_writer::link(new moodle_url('/local/transcripts/index.php'), get_string('managetranscripts','block_sb_admin_menu'), array('title'=>get_string('managetranscripts','block_sb_admin_menu')));
                                } else {
                                    $html .= html_writer::link(new moodle_url('/local/transcripts/index.php'), get_string('viewtranscripts','block_sb_admin_menu'), array('title'=>get_string('viewtranscripts','block_sb_admin_menu')));
                                }
                            $html .= html_writer::end_tag('li');
                        }
                
                        if (has_capability('local/manager:manageapprovals', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/manager/approvals/index.php'), get_string('manageapprovals', 'local_manager'), array('title'=>get_string('manageapprovals', 'local_manager')));
                            $html .= html_writer::end_tag('li');
                        }

                       
                    $html .= html_writer::end_tag('ul');     
                $html .= html_writer::end_tag('li');
            }
        
            if (has_capability('block/sb_admin_menu:ecommerce', $systemcontext)){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-credit-card'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('ecommerce','block_sb_admin_menu'), array('title'=>get_string('ecommerce','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));

                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));
                        $html .= html_writer::start_tag('li');                
                            $html .= html_writer::link(new moodle_url('/admin/settings.php', array('section'=>'enrolsettingspaypal')), get_string('manageecommerce','block_sb_admin_menu'), array('title'=>get_string('manageecommerce','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');
                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
        
            if ((has_capability('local/etraining:view', $systemcontext) and get_config('local_etraining', 'enabled')) or (has_capability('local/plans:manage', $systemcontext) and get_config('local_plans', 'enabled_certifications'))){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-certificate'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('certifications','block_sb_admin_menu'), array('title'=>get_string('certifications','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));

                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));

                        if (has_capability('local/etraining:view', $systemcontext) and get_config('local_etraining', 'enabled')){
                            $html .= html_writer::start_tag('li');           
                                if (has_capability('local/etraining:manage', $systemcontext)){
                                    $html .= html_writer::link(new moodle_url('/local/etraining/index.php'), get_string('manageetraining','block_sb_admin_menu'), array('title'=>get_string('manageetraining','block_sb_admin_menu')));
                                } else{
                                    $html .= html_writer::link(new moodle_url('/local/etraining/index.php'), get_string('managemyetraining','block_sb_admin_menu'), array('title'=>get_string('managemyetraining','block_sb_admin_menu')));
                                }
                            $html .= html_writer::end_tag('li');
                        }

                        if (has_capability('local/plans:manage', $systemcontext) and get_config('local_plans', 'enabled_certifications')){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/plans/index.php?type=1'), get_string('managecertifications', 'block_sb_admin_menu'), array('title'=>get_string('managecertifications', 'block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }

                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
        
            if (has_capability('local/notifications:manage', $systemcontext) or (has_capability('local/sb_announcements:manage', $systemcontext) and get_config('local_sb_announcements', 'enabled'))){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-commenting-o'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('communications','block_sb_admin_menu'), array('title'=>get_string('communications','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));

                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));

                        if (has_capability('local/notifications:manage', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/notifications/notifications.php'), get_string('managenotifications','block_sb_admin_menu'), array('title'=>get_string('managenotifications','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }

                        if (has_capability('local/sb_announcements:manage', $systemcontext) and get_config('local_sb_announcements', 'enabled')){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/sb_announcements/announcements.php'), get_string('manageannouncements','block_sb_admin_menu'), array('title'=>get_string('manageannouncements','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }

                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
        
            if (has_capability('moodle/blog:view', $systemcontext) || as_capability('local/manager:blogsettings', $systemcontext)){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-comments-o'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('social','block_sb_admin_menu'), array('title'=>get_string('social','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));

                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));

                    if (has_capability('moodle/blog:view', $systemcontext)){
                        $html .= html_writer::start_tag('li'); 
                            $html .= html_writer::link(new moodle_url('/blog/index.php', array('courseid'=>1)), get_string('manageblogs','block_sb_admin_menu'), array('title'=>get_string('viewblogs','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');
                    }
                
                    if (has_capability('local/manager:blogsettings', $systemcontext)){
                        $html .= html_writer::start_tag('li'); 
                            $html .= html_writer::link(new moodle_url('/local/manager/settings/blog.php'), get_string('blogsettings','local_manager'), array('title'=>get_string('blogsettings','local_manager')));
                        $html .= html_writer::end_tag('li');
                    }

                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
            
            if (has_capability('local/intelliboard:manage', $systemcontext) or (has_capability('local/intelliboard:view', $systemcontext))){
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-table'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('report','block_sb_admin_menu'), array('title'=>get_string('report','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));

                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));

                        $html .= html_writer::start_tag('li'); 
                            $html .= html_writer::link(new moodle_url('/local/intelliboard/index.php'), get_string('intelliboard','block_sb_admin_menu'), array('title'=>get_string('intelliboard','block_sb_admin_menu')));
                        $html .= html_writer::end_tag('li');            
                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
        
            if (has_capability('block/sb_admin_menu:manage', $systemcontext) or (has_capability('local/manager:appearance', $systemcontext)) or (has_capability('local/manager:languages', $systemcontext))){
        
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-cogs'));
                    $html .= html_writer::link('javascript:void(0);', $icon.get_string('systemsettings','block_sb_admin_menu'), array('title'=>get_string('systemsettings','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));

                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));

                        if (has_capability('block/sb_admin_menu:manage', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/blocks/sb_admin_menu/manage.php'), get_string('managefeatures','block_sb_admin_menu'), array('title'=>get_string('managefeatures','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }

                        if (has_capability('local/manager:appearance', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/manager/settings/appearance.php'), get_string('manageappearance','block_sb_admin_menu'), array('title'=>get_string('manageappearance','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }

                        if (has_capability('local/manager:languages', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/manager/settings/languages.php'), get_string('managelanguages','block_sb_admin_menu'), array('title'=>get_string('managelanguages','block_sb_admin_menu')));
                            $html .= html_writer::end_tag('li');
                        }

                    $html .= html_writer::end_tag('ul');
                $html .= html_writer::end_tag('li');
            }
        
            
        
            /*if ((has_capability('local/certificate:manage', $systemcontext) or has_capability('local/certificate:view', $systemcontext)) and get_config('local_certificate', 'enabled')) {
                $html .= html_writer::start_tag('li', array('class'=>'expanded open'));                
                    $html .= html_writer::link('javascript:void(0);', get_string('certificates','block_sb_admin_menu'), array('title'=>get_string('certificates','block_sb_admin_menu'), 'onclick'=>'jQuery(this).parent().toggleClass("open");'));
                    
                    $html .= html_writer::start_tag('ul', array('class'=>'toggle-menu'));
                        if (has_capability('local/certificate:manage', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/certificate/certificates.php'), get_string('certificates', 'local_certificate'), array('title'=>get_string('certificates', 'local_certificate')));
                            $html .= html_writer::end_tag('li');
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/certificate/templates.php'), get_string('templates', 'local_certificate'), array('title'=>get_string('templates', 'local_certificate')));
                            $html .= html_writer::end_tag('li');
                        }
                        if (has_capability('local/certificate:view', $systemcontext)){
                            $html .= html_writer::start_tag('li');                
                                $html .= html_writer::link(new moodle_url('/local/certificate/user-certificate.php'), get_string('user_certificates', 'local_certificate'), array('title'=>get_string('user_certificates', 'local_certificate')));
                            $html .= html_writer::end_tag('li');
                        }
                    $html .= html_writer::end_tag('ul');
                
                $html .= html_writer::end_tag('li');
            }*/
        
        if (!empty($html)){
            $html = html_writer::start_tag('ul', array('class'=>'admin-menu')) . $html . html_writer::end_tag('ul');   
        }
        
        return $html;
    }
    
}
