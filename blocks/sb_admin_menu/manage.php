<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays external information about a course
 * @package    sb_admin_menu
 * @copyright  1999 onwards Martin Dougiamas  http://dougiamas.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("locallib.php");

$config       = optional_param('config', '', PARAM_RAW);
$action       = optional_param('action', '', PARAM_RAW);
$plugin       = optional_param('plugin', '', PARAM_RAW);
$state        = optional_param('state', 0, PARAM_INT);

$systemcontext = context_system::instance();
require_login();
require_capability('block/sb_admin_menu:manage', $systemcontext);

if ($action == 'set_config'){
    set_config($config, $state, $plugin);
    
    if ($plugin == 'local_gamification'){
        $CFG->enablebadges = $state;
        set_config('enablebadges', $state);
    }
    
    echo '1';
    exit;
}

$title = get_string('managefeatures','block_sb_admin_menu');
$PAGE->set_url('/blocks/sb_admin_menu:manage');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$PAGE->navbar->add($title, new moodle_url('/blocks/sb_admin_menu/manage.php'));

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo manage_functionality();

echo $OUTPUT->footer();

