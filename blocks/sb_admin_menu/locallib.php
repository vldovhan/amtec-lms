<?php 

function manage_functionality() {
    global $USER, $OUTPUT,$CFG,$DB;

    $systemcontext   = context_system::instance();

    $html = '';
    $html .= html_writer::start_tag('ul', array('class'=>'admin-menu-manage'));

        $value = get_config('local_coursecatalog', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('pluginname','local_coursecatalog');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_coursecatalog', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_coursecatalog", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_sb_announcements', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('announcements','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_sb_announcements', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_sb_announcements", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_certificate', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('certificates','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_certificate', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_certificate", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_plans', 'enabled_plans');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('learningplans','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_plans', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_plans", "enabled_plans");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_plans', 'enabled_certifications');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('managecertifications','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'enabled_certifications', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_plans", "enabled_certifications");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
       
        $value = get_config('local_gamification', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('gamification','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_gamification', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_gamification", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_etraining', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('etraining','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_etraining', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_etraining", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_transcripts', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('transcripts','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_transcripts', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_transcripts", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
        $value = get_config('local_ecommerce', 'enabled');
        $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            $html .= get_string('ecommerce','block_sb_admin_menu');
            $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                $html .= html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_ecommerce', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_ecommerce", "enabled");');
                    if ($value) $params['checked'] = 'checked';
                    $html .= html_writer::tag('input', '', $params);
                    $html .= html_writer::tag('span', '');
                $html .= html_writer::end_tag('label');
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('li');
    
    $html .= html_writer::end_tag('ul');
    
    $html .= '<script>
        function toggleFunctionality(obj, plugin, config){
            var state = (jQuery(obj).prop("checked"))?1:0;
            
            jQuery.get("'.$CFG->wwwroot.'/blocks/sb_admin_menu/manage.php?action=set_config&state="+state+"&config="+config+"&plugin="+plugin);
            jQuery(obj).parent().parent().parent().toggleClass("disabled");
        }
    </script>';

    return $html;
}

?>