<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_timeline
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

class block_timeline extends block_base {

    /**
     * Initialise the block.
     */
    public function init() {
        $this->title = get_string('pluginname', 'block_timeline');
    }
	function hide_header() {
        return true;
    }
	
    /**
     * Return preferred_width.
     *
     * @return int
     */
    public function preferred_width() {
        return 210;
    }
	
	public function instance_allow_multiple() {
		return true;
	}

    /**
     * Return the content of this block.
     *
     * @return stdClass the content
     */
    public function get_content() {
        global $CFG, $USER, $OUTPUT, $DB;
		
		if ($this->content !== null) {
            return $this->content;
        }
		
		$this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';
        
        $course = $this->page->course;
		$issite = ($course->id == SITEID);
        
        $data = $this->get_courses();
        
        $renderer = $this->page->get_renderer('block_timeline');
        $this->content->text = $renderer->timeline($data, $issite);
		
        return $this->content;
    }
    
    public function get_courses(){
        global $CFG, $USER, $OUTPUT, $DB;
        $courses = array(); $courses_without_deadline = array();
        
        $mycourses = enrol_get_my_courses('enddate, status, visible', 'enddate ASC, fullname ASC');
        
        if (count($mycourses)){
            foreach ($mycourses as $course){
               if (!$course->status or !$course->visible) continue;
               if ($course->enddate > 0 and $course->enddate < time()) continue;
               if ($course->enddate > 0){
                   $courses[$course->enddate][] = $course;
               } else {
                   $courses_without_deadline[] = $course;
               }
            }
            if (count($courses_without_deadline)){
                $courses += array(0=>$courses_without_deadline);
            }
        }
        
        return $courses;
    }
}


