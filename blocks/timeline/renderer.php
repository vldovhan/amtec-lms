<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_timeline
 * @copyright 2016 SEBALE, SEBALE.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

class block_timeline_renderer extends plugin_renderer_base {

    public function timeline($data = array(), $issite = true) {
        global $CFG, $PAGE, $OUTPUT;
        
        $output = '';
        
        $output .= html_writer::start_tag('div', array('class'=>'course-timeline-box'));
        $output .= html_writer::tag('div', '<h2>'.get_string('upcomingevents', 'block_timeline').'</h2>', array('class'=>'timeline-header'));
        	
            if (count($data)){ $olddate = 1;
                $output .= html_writer::start_tag('div', array('class'=>'timeline-box'));
                foreach($data as $date=>$courses){
                    foreach($courses as $course){
                        $output .= html_writer::start_tag('div', array('class'=>'timeline-secion clearfix'));
                        $output .= html_writer::tag('span', (($date > 0) ? date('M d, Y', $date) : get_string('nodate', 'block_timeline')), array('class'=>'enddate-box'));
                        
                        $output .= html_writer::start_tag('div', array('class'=>'coursename'));
                        $output .= html_writer::link(new moodle_url('/course/view.php', array('id' => $course->id)), $course->fullname);
                        $output .= html_writer::end_tag('div');
                        
                        $output .= html_writer::end_tag('div');
                    }
                    if ($date > 0 and date('m', $date) != date('m', $olddate)){
                        $output .= html_writer::tag('div', '', array('class'=>'divider'));
                    }
                    $olddate = $date;
                }
                $output .= html_writer::end_tag('div');
            } else {
                $output .= html_writer::tag('div', get_string('nothingtodisplay', 'block_timeline'), array('class'=>'alert alert-success'));
            }
        
		$output .= html_writer::end_tag('div');
		
		
        
        
        return $output;
    }

}
