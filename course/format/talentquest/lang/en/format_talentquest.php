<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_talentquest', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_talentquest
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['currentsection'] = 'This topic';
$string['editsection'] = 'Edit topic';
$string['deletesection'] = 'Delete topic';
$string['sectionname'] = 'Topic';
$string['pluginname'] = 'TalentQuest format';
$string['section0name'] = 'General';
$string['page-course-view-talentquest'] = 'Any course main page in talentquest format';
$string['page-course-view-talentquest-x'] = 'Any course page in talentquest format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';
$string['courseoptionedit'] = 'Edit Custom Course Fields';
$string['courseoptioncreate'] = 'Create Custom Course Fields';
$string['title'] = 'Title';
$string['required_field'] = 'This field is required!';
$string['textbox'] = 'Text';
$string['select'] = 'Drop-down';
$string['multiselect'] = 'Multiple Select';
$string['date'] = 'Date';
$string['datetime'] = 'Date and Time';
$string['checkbox'] = 'Single Checkbox';
$string['checkboxes'] = 'Multiple Checkboxes';
$string['editor'] = 'Editor';
$string['type'] = 'Field type';
$string['values'] = 'Field values (one per line)';
$string['notrequired'] = 'Not required';
$string['required'] = 'Required';
$string['optional'] = 'Optional';
$string['visibility'] = 'Visibility';
$string['course_options'] = 'Manage Custom Course Fields';
$string['create_new'] = 'Create New Field';
$string['actions'] = 'Actions';
$string['courseoptionsdroot'] = 'Custom Course Fields';
$string['courseoptionsmanage'] = 'Manage Custom Course Fields';
$string['formattalentquestroot'] = 'TalentQuest course format';
$string['shortname'] = 'Short name (unique)';
$string['nameexists'] = 'The name already exists';
$string['resumecourse'] = 'Resume Course';
$string['startcourse'] = 'Start Course';
$string['viewcourse'] = 'View Course';
$string['save'] = 'Save';
$string['search'] = 'Search';
$string['course_enddate'] = 'Course end date';
$string['active'] = 'Active';
$string['inactive'] = 'Inactive';
$string['status'] = 'Status';
$string['thumbnailfile'] = 'Course thumbnail image';
$string['textarea'] = 'Extended Text';
$string['courseinfofile'] = 'Course info file';
$string['curriculum'] = 'Course curriculum';
$string['coursedescription'] = 'Course description';
$string['courseprogress'] = 'Course Progress';
$string['event_course_rated'] = 'Course rated';
$string['event_course_rated_description'] = 'The user with id \'{$a->userid}\' rated course with id {$a->courseid}.';
$string['gradessummary'] = 'Grades Summary';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['certification'] = 'Certification';
$string['coursenotstarts'] = 'Course not started yet';
$string['courseoverdue'] = 'Course is overdue';
$string['courseblog'] = 'Course Blog';
$string['gragessummary'] = 'Grades Summary';
$string['talentquest:manage_options'] = 'Manage Custom Course Fields';
$string['donotdisplay'] = 'Do not display';
$string['coursefield'] = 'Course field';
$string['coursetab'] = 'Course tab';
$string['displayoncoursepage'] = 'Display as';
