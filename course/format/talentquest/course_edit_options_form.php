<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $course_option;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $course_option  = $this->_customdata['course_option'];
        $context        = $this->_customdata['context'];
        
        $this->id  = $id;
        $this->context = $context;
        $this->course_option = $course_option;

        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('text', 'name', get_string('shortname', 'format_talentquest'), 'maxlength="254"  size="50"');
        $mform->addRule('name', get_string('required_field', 'format_talentquest'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('text', 'title', get_string('title', 'format_talentquest'), 'maxlength="254"  size="50"');
        $mform->addRule('title', get_string('required_field', 'format_talentquest'), 'required', null, 'client');
        $mform->setType('title', PARAM_TEXT);
        
        $choices = array();
        $choices['text'] = get_string('textbox', 'format_talentquest');
        $choices['select'] = get_string('select', 'format_talentquest');
        $choices['multiselect'] = get_string('multiselect', 'format_talentquest');
        $choices['date'] = get_string('date', 'format_talentquest');
        $choices['datetime'] = get_string('datetime', 'format_talentquest');
        $choices['checkbox'] = get_string('checkbox', 'format_talentquest');
        $choices['checkboxes'] = get_string('checkboxes', 'format_talentquest');
        $choices['textarea'] = get_string('textarea', 'format_talentquest');
        $choices['editor'] = get_string('editor', 'format_talentquest');
        $mform->addElement('select', 'type', get_string('type', 'format_talentquest'), $choices, 'class="co_type"');
        $mform->setDefault('type', 'text');
        
        $mform->addElement('textarea', 'options', get_string('values', 'format_talentquest'), 'class="co_values"');
        $mform->setType('options', PARAM_RAW);
        
        $choices = array();
        $choices['0'] = get_string('notrequired', 'format_talentquest');
        $choices['1'] = get_string('required', 'format_talentquest');
        $mform->addElement('select', 'required', get_string('required', 'format_talentquest'), $choices);
        $mform->setDefault('required', 0);
        
        $choices = array();
        $choices['0'] = get_string('inactive');
        $choices['1'] = get_string('active');
        $mform->addElement('select', 'state', get_string('visibility', 'format_talentquest'), $choices);
        $mform->setDefault('state', 1);
        
        $choices = array();
        $choices[''] = get_string('donotdisplay', 'format_talentquest');
        $choices['list'] = get_string('coursefield', 'format_talentquest');
        $choices['tab'] = get_string('coursetab', 'format_talentquest');
        $mform->addElement('select', 'display', get_string('displayoncoursepage', 'format_talentquest'), $choices);
        $mform->setDefault('display', 1);
    
        $this->add_action_buttons(get_string('cancel'), (($id > 0) ? get_string('save', 'format_talentquest') : get_string('create')));

        // Finally set the current form data
        $this->set_data($course_option);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $data = (object)$data;
        $field = $DB->get_record('course_format_tq_cfields', array('id' => $data->id));
        
        $errors = parent::validation($data, $files);
        
        if ($field and $field->name !== $data->name) {
            // Check new name does not exist.
            if ($DB->record_exists('course_format_tq_cfields', array('name' => $data->name))) {
                $errors['name'] = get_string('nameexists', 'format_talentquest');
            }
        }

        return $errors;
    }
}

