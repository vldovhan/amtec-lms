<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the talentquest course format.
 *
 * @package format_talentquest
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since Moodle 2.3
 */


defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot.'/course/format/renderer.php');

/**
 * Basic renderer for talentquest format.
 *
 * @copyright 2012 Dan Poltawski
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_talentquest_renderer extends format_section_renderer_base {

    /**
     * Constructor method, calls the parent constructor
     *
     * @param moodle_page $page
     * @param string $target one of rendering target constants
     */
    public function __construct(moodle_page $page, $target) {
        parent::__construct($page, $target);

        // Since format_talentquest_renderer::section_edit_controls() only displays the 'Set current section' control when editing mode is on
        // we need to be sure that the link 'Turn editing mode on' is available for a user who does not have any other managing capability.
        $page->set_other_editing_capability('moodle/course:setcurrentsection');
    }

    /**
     * Generate the starting container html for a list of sections
     * @return string HTML to output.
     */
    protected function start_section_list() {
        return html_writer::start_tag('ul', array('class' => 'topics'));
    }

    /**
     * Generate the closing container html for a list of sections
     * @return string HTML to output.
     */
    protected function end_section_list() {
        return html_writer::end_tag('ul');
    }

    /**
     * Generate the title for this section page
     * @return string the page title
     */
    protected function page_title() {
        return get_string('topicoutline');
    }

    /**
     * Generate the edit control items of a section
     *
     * @param stdClass $course The course entry from DB
     * @param stdClass $section The course_section entry from DB
     * @param bool $onsectionpage true if being printed on a section page
     * @return array of edit control items
     */
    protected function section_edit_control_items($course, $section, $onsectionpage = false) {
        global $PAGE;

        if (!$PAGE->user_is_editing()) {
            return array();
        }

        $coursecontext = context_course::instance($course->id);

        if ($onsectionpage) {
            $url = course_get_url($course, $section->section);
        } else {
            $url = course_get_url($course);
        }
        $url->param('sesskey', sesskey());

        $isstealth = $section->section > $course->numsections;
        $controls = array();
        if (!$isstealth && $section->section && has_capability('moodle/course:setcurrentsection', $coursecontext)) {
            if ($course->marker == $section->section) {  // Show the "light globe" on/off.
                $url->param('marker', 0);
                $markedthistopic = get_string('markedthistopic');
                $highlightoff = get_string('highlightoff');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marked',
                                               'name' => $highlightoff,
                                               'pixattr' => array('class' => '', 'alt' => $markedthistopic),
                                               'attr' => array('class' => 'editing_highlight', 'title' => $markedthistopic));
            } else {
                $url->param('marker', $section->section);
                $markthistopic = get_string('markthistopic');
                $highlight = get_string('highlight');
                $controls['highlight'] = array('url' => $url, "icon" => 'i/marker',
                                               'name' => $highlight,
                                               'pixattr' => array('class' => '', 'alt' => $markthistopic),
                                               'attr' => array('class' => 'editing_highlight', 'title' => $markthistopic));
            }
        }

        $parentcontrols = parent::section_edit_control_items($course, $section, $onsectionpage);

        // If the edit key exists, we are going to insert our controls after it.
        if (array_key_exists("edit", $parentcontrols)) {
            $merged = array();
            // We can't use splice because we are using associative arrays.
            // Step through the array and merge the arrays.
            foreach ($parentcontrols as $key => $action) {
                $merged[$key] = $action;
                if ($key == "edit") {
                    // If we have come to the edit key, merge these controls here.
                    $merged = array_merge($merged, $controls);
                }
            }

            return $merged;
        } else {
            return array_merge($controls, $parentcontrols);
        }
    }
    
    public function get_course_formatted_summary($course, $options = array()) {
        global $CFG;
        
        require_once($CFG->libdir. '/filelib.php');
        if (!$course->summary) {
            return '';
        }
        $options = (array)$options;
        $context = context_course::instance($course->id);
        
        $summary = file_rewrite_pluginfile_urls($course->summary, 'pluginfile.php', $context->id, 'course', 'summary', null);
        $summary = format_text($summary, $course->summaryformat, $options, $course->id);
        
        return $summary;
    }
    
    public function get_course_formatted_field($course, $field, $filearea, $options = array()) {
        global $CFG;
        
        require_once($CFG->libdir. '/filelib.php');
        if (!$course->{$field}) {
            return '';
        }
        $options = (array)$options;
        $context = context_course::instance($course->id);
        
        $text = file_rewrite_pluginfile_urls($course->{$field}, 'pluginfile.php', $context->id, 'format_talentquest', $filearea, null);
        $text = format_text($text, 1, $options, $course->id);
        
        return $text;
    }
    
     /**
     * Generate the display of the header part of a section before
     * course modules are included
     *
     * @param stdClass $section The course_section entry from DB
     * @param stdClass $course The course entry from DB
     * @param bool $onsectionpage true if being printed on a single-section page
     * @param int $sectionreturn The section to return to after an action
     * @return string HTML to output.
     */
    protected function section_header($section, $course, $onsectionpage, $sectionreturn=null) {
        global $PAGE, $CFG;

        $o = '';
        $currenttext = '';
        $sectionstyle = '';
        $context = context_course::instance($course->id);
        $availability = $this->section_availability_message($section,
                has_capability('moodle/course:viewhiddensections', $context));

        if ($section->section != 0) {
            // Only in the non-general sections.
            if (!$section->visible) {
                $sectionstyle = ' hidden';
            } else if (course_get_format($course)->is_section_current($section)) {
                $sectionstyle = ' current';
            }
        }
        
        if (!$section->uservisible) {
            $sectionstyle .= ' restrict-access';
        } elseif (has_capability('moodle/course:viewhiddensections', $context) && $availability) {
            $sectionstyle .= ' restrict-access';
        }

        $o.= html_writer::start_tag('li', array('id' => 'section-'.$section->section,
            'class' => 'section main clearfix'.$sectionstyle, 'role'=>'region',
            'aria-label'=> get_section_name($course, $section)));

        // Create a span that contains the section title to be used to create the keyboard section move menu.
        $o .= html_writer::tag('span', get_section_name($course, $section), array('class' => 'hidden sectionname'));

        $leftcontent = $this->section_left_content($section, $course, $onsectionpage);
        $o.= html_writer::tag('div', $leftcontent, array('class' => 'left side'));

        $rightcontent = $this->section_right_content($section, $course, $onsectionpage);
        $o.= html_writer::tag('div', $rightcontent, array('class' => 'right side'));
        $o.= html_writer::start_tag('div', array('class' => 'content'));

        // When not on a section page, we display the section titles except the general section if null
        $hasnamenotsecpg = (!$onsectionpage && ($section->section != 0 || !is_null($section->name)));

        // When on a section page, we only display the general section title, if title is not the default one
        $hasnamesecpg = ($onsectionpage && ($section->section == 0 && !is_null($section->name)));

        $classes = ' accesshide';
        if ($hasnamenotsecpg || $hasnamesecpg) {
            $classes = '';
        }
        
        $sectionname = html_writer::tag('span', $this->section_title($section, $course));
        $o.= $this->output->heading($sectionname, 3, 'sectionname' . $classes);

        $o.= html_writer::start_tag('div', array('class' => 'summary'));
        $o.= $this->format_summary_text($section);
        $o.= html_writer::end_tag('div');

        $o .= $availability;

        return $o;
    }
    
    public function print_tq_course_header($course, $context, $activetab = 'course') {
        global $PAGE, $CFG, $DB, $USER, $OUTPUT;
        
        require_once($CFG->dirroot.'/course/format/talentquest/lib.php');
        $o = '';
        
        $cfields = course_get_custom_fields($course);

        $course_description = $this->get_course_formatted_summary($course,
                                  array('overflowdiv' => true, 'noclean' => false, 'para' => false));    

        $o .= html_writer::start_tag('div', array('class'=>'course-content'));
        $o .= html_writer::start_tag('div', array('class'=>'course-title-box'));
        $course_image_url = get_course_image_url($course);
        $o .= html_writer::tag('div', '', array('class'=>'course-image', 'style'=>'background-image: url("'.$course_image_url.'");'));
        $o .= html_writer::start_tag('div', array('class'=>'course-title'));
        $o .= $course->fullname;
            if (count($cfields['list'])){
                $o .= html_writer::start_tag('div', array('class'=>'course-custom-toggler'));
                    $o .= html_writer::tag('i', '', array('onclick'=>'toggleCourseCF();', 'class'=>'fa fa-angle-down hidden'));
                    $o .= html_writer::tag('i', '', array('onclick'=>'toggleCourseCF();', 'class'=>'fa fa-angle-up'));
                $o .= html_writer::end_tag('div');
            }
        $o .= html_writer::end_tag('div');
        $o .= course_get_course_teachers($course);
        $o .= html_writer::end_tag('div');

        $o .= course_print_rating($course, is_enrolled($context, $USER));

        if(is_enrolled($context, $USER)){
            if ($course->completion_info->status == 'pending' or $course->completion_info->status == 'inprogress' or $course->completion_info->status == 'completed' or $course->completion_info->status == 'notyetstarted'){
                $progress_info = html_writer::tag('label', get_string('courseprogress', 'format_talentquest').':') .
                    html_writer::tag('div', 
                    html_writer::start_tag('div', array('class' => 'progres-bar')).
                    html_writer::tag('div', html_writer::tag('span', intval($course->completion_info->completion).'%',array('class' => 'procent')), array('class' => 'progres','style'=>'width:'.$course->completion_info->completion.'%;')).
                    html_writer::end_tag('div'),
                        array('class'=>'clearfix progress-wrapper'));
                $o .= html_writer::tag('div', $progress_info, array('class' => 'course-progress'));
            }
        }

        if (!empty($course->startdate) or !empty($course->enddate)){
            $o .= html_writer::start_tag('div', array('class'=>'course-dates clearfix'));
                if (!empty($course->startdate)){
                    $o .= html_writer::start_tag('div', array('class'=>'course-date-item'));
                        $o .= html_writer::tag('label', get_string('startdate', 'format_talentquest').':');
                        $o .= html_writer::tag('span', date('m/d/Y', $course->startdate), array('class'=>'course-date'));
                    $o .= html_writer::end_tag('div');
                }
                if (!empty($course->enddate)){
                    $o .= html_writer::start_tag('div', array('class'=>'course-date-item'));
                        $o .= html_writer::tag('label', get_string('enddate', 'format_talentquest').':');
                        $o .= html_writer::tag('span', date('m/d/Y', $course->enddate), array('class'=>'course-date'));
                    $o .= html_writer::end_tag('div');
                }
            $o .= html_writer::end_tag('div');
        }

        if (count($cfields['list'])){
            $o .= html_writer::start_tag('ul', array('class'=>'course-custom-list clearfix'));
                foreach ($cfields['list'] as $fieldname=>$field){
                    $o .= html_writer::start_tag('li', array('class'=>'course-cutom-item'));
                        $o .= html_writer::tag('label', $field['title'].':');
                        $o .= html_writer::tag('span', $field['value']);
                    $o .= html_writer::end_tag('li');
                }
            $o .= html_writer::end_tag('ul');
        }

        $o .= html_writer::start_tag('div', array('class'=>'nav-tabs-header'));
            $o .= html_writer::start_tag('ul', array('class'=>'nav-tabs nav-tabs-simple clearfix'));
                $o .= html_writer::tag('li', '<a data-toggle="tab" href="#curriculum">'.get_string('curriculum', 'format_talentquest').'</a>', array('class'=>'header-curriculum'.(($activetab == 'curriculum') ? ' active' : ''), 'onclick'=>'location="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=curriculum"'));

                if (!empty($course_description)){
                    $o .= html_writer::tag('li', '<a data-toggle="tab" href="#course-description">'.get_string('coursedescription', 'format_talentquest').'</a>', array('class'=>'header-course-description'.(($activetab == 'course-description') ? ' active' : ''), 'onclick'=>'location="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab=course-description"'));
                }

                $o .= html_writer::tag('li', '<a data-toggle="tab" href="'.$CFG->wwwroot.'/blog/index.php?courseid='.$course->id.'">'.get_string('courseblog', 'format_talentquest').'</a>', array('class'=>'header-course-description'.(($activetab == 'blog') ? ' active' : ''), 'onclick'=>'location="'.$CFG->wwwroot.'/blog/index.php?courseid='.$course->id.'"'));

                $o .= html_writer::tag('li', '<a data-toggle="tab" href="'.$CFG->wwwroot.'/course/format/talentquest/grades.php?id='.$course->id.'">'.get_string('gragessummary', 'format_talentquest').'</a>', array('class'=>'header-course-description'.(($activetab == 'gradessummary') ? ' active' : ''), 'onclick'=>'location="'.$CFG->wwwroot.'/course/format/talentquest/grades.php?id='.$course->id.'"'));

                if (count($cfields['tab'])){
                    foreach ($cfields['tab'] as $fieldname=>$field){
                        $o .= html_writer::tag('li', '<a data-toggle="tab" href="#'.$fieldname.'">'.$field['title'].'</a>', array('class'=>'header-'.$fieldname.(($activetab == $fieldname) ? ' active' : ''), 'onclick'=>'location="'.$CFG->wwwroot.'/course/view.php?id='.$course->id.'&tab='.$fieldname.'"'));
                    }
                }

            $o .= html_writer::end_tag('ul');
        $o .= html_writer::end_tag('div');
        $o .= html_writer::end_tag('div');

        return $o;
    }
}
