<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * talentquest config settings script.
 *
 * @package    format_talentquest
 * @author     talentquest
 * @copyright  2016 talentquest, talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('format_talentquest', get_string('pluginname', 'format_talentquest'));

$ADMIN->add('root', new admin_category('formattalentquestroot', get_string('formattalentquestroot', 'format_talentquest')));
				
$ADMIN->add('formattalentquestroot', new admin_externalpage('courseoptionsmanage', get_string('courseoptionsmanage', 'format_talentquest'), $CFG->wwwroot.'/course/format/talentquest/course_options.php', 'format/talentquest:manage_options'));

