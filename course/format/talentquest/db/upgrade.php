<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade code containing changes to the plugin data table.
 *
 * @package    format_talentquest
 * @author     talentquest
 * @copyright  2016 talentquest, talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_format_talentquest_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

	// Define table course_format_tq_cfields to be created.
	$table = new xmldb_table('course_format_tq_cfields');

	// Adding fields to table course_format_tq_cfields.
	$table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
    $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('title', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
    $table->add_field('options', XMLDB_TYPE_TEXT, null, null, null, null, null);
	$table->add_field('required', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0');
	$table->add_field('state', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1');
	$table->add_field('sortorder', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
    
	// Adding keys to table course_format_tq_cfields.
	$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));

	// Conditionally launch create table for course_format_tq_cfields.
	if (!$dbman->table_exists($table)) {
		$dbman->create_table($table);
	}

    return true;
}
