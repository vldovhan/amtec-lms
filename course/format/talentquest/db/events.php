<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade code containing changes to the plugin data table.
 *
 * @package    format_talentquest
 * @author     talentquest
 * @copyright  2016 talentquest, talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$observers = array(

    // User logging out.
    array(
        'eventname' => '\format_talentquest\event\format_talentquest_course_rated',
        'callback'    => 'core_badges_observer::rating_criteria_review',
    ),
    array(
        'eventname' => '\mod_forum\event\discussion_created',
        'callback'    => 'core_badges_observer::forum_criteria_review',
    ),
    array(
        'eventname' => '\mod_forum\event\post_created',
        'callback'    => 'core_badges_observer::forumpost_criteria_review',
    ),
    array(
        'eventname' => '\core\event\blog_entry_created',
        'callback'    => 'core_badges_observer::blog_criteria_review',
    ),
    array(
        'eventname' => '\core\event\blog_comment_created',
        'callback'    => 'core_badges_observer::blogcomment_criteria_review',
    ),
    array(
        'eventname' => '\local_plans\event\local_plans_completed',
        'callback'  => 'core_badges_observer::plan_criteria_review',
    ),
    array(
        'eventname' => '\local_gamification\event\local_gamification_submitted',
        'callback'  => 'core_badges_observer::gamification_criteria_review',
    ),
);
