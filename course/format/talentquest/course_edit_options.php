<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * talentquest version file.
 *
 * @package    format_talentquest
 * @author     talentquest
 * @copyright  2016 talentquest, talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../../config.php');
require_once('course_edit_options_form.php');

$systemcontext   = context_system::instance();
require_login();
require_capability('format/talentquest:manage_options', $systemcontext);

$id = optional_param('id', 0, PARAM_INT); // Option id.

$USER->editing = 0;
$PAGE->set_pagelayout('admin');
$pageparams = array('id' => $id);
$PAGE->set_url('/course/format/talentquest/course_edit_options.php', $pageparams);
$PAGE->set_context($systemcontext);

if ($id > 0) {
    $course_option = $DB->get_record('course_format_tq_cfields', array('id'=>$id));
    if ($course_option->type == 'select' or $course_option->type == 'multiselect' or $course_option->type == 'checkboxes'){
        $options = unserialize($course_option->options);
        $course_option->options = implode("", $options);
    } else {
        $course_option->options = '';
    }
} else {
    $course_option = new stdClass();
    $course_option->id = null;
}

// First create the form.
$args = array(
    'id' => $id,
    'course_option' => $course_option,
    'context'=>$systemcontext
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/course/format/talentquest/course_options.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    $data->name = trim(strtolower(str_replace(' ', '_', $data->name)));
    if ($data->type == 'select' or $data->type == 'multiselect' or $data->type == 'checkboxes'){
        $options = explode("\n", trim($data->options));
        $data->options = serialize($options);
    } else {
        $data->options = '';
    }
    if ($id > 0) {
        // Update existing announcement.
        $DB->update_record('course_format_tq_cfields', $data);
    } else {
        // Add new announcement.
        $last_record = $DB->get_record_sql("SELECT id, sortorder FROM {course_format_tq_cfields} ORDER BY sortorder DESC LIMIT 1");
        $data->sortorder = (isset($last_record->sortorder)) ? ++$last_record->sortorder : 0;
        $course_option->id = $DB->insert_record('course_format_tq_cfields', $data);
    }
    
    redirect(new moodle_url('/course/format/talentquest/course_options.php'));
}

// Print the form.

$site = get_site();
$title = ($id > 0) ? get_string('courseoptionedit', 'format_talentquest') : get_string('courseoptioncreate', 'format_talentquest');

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

?>
<script>
    jQuery("#id_type").change(function(e){
       if (jQuery(this).val() == 'select' || jQuery(this).val() == 'multiselect' || jQuery(this).val() == 'checkboxes'){
           jQuery("#fitem_id_options").removeClass("hidden");
       } else {
           jQuery("#fitem_id_options").addClass("hidden");
       }
    });
    <?php if ((isset($course_option->type) and ($course_option->type == 'text' or $course_option->type == 'date' or $course_option->type == 'datetime' or $course_option->type == 'checkbox' or $course_option->type == 'editor')) or !isset($course_option->type)) : ?>
        jQuery("#fitem_id_options").addClass("hidden");
    <?php endif; ?>
</script>

<?php 
echo $OUTPUT->footer();
