<?php
/**
 * The format_talentquest viewed event.
 *
 * @package   format_talentquest
 * @author    SEBALE
 * @copyright 2016 TalentQuest
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 */

namespace format_talentquest\event;
defined('MOODLE_INTERNAL') || die();

class format_talentquest_course_rated extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'с';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'format_talentquest';
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_course_rated', 'format_talentquest');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        $a = (object) array('userid' => $this->userid, 'courseid' => $this->objectid);
        return get_string('event_meeting_created_description', 'format_talentquest', $a);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array
     */
    protected function get_legacy_logdata() {
        return(array($this->courseid, 'course', 'course rated',
                'view.php?pageid=' . $this->objectid, get_string('event_course_rated', 'format_talentquest'), $this->contextinstanceid));
    }

    /**
     * Get URL related to the action.
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/course/view.php', array('id' => $this->objectid));
    }
}