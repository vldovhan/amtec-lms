<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Admin-only code to delete a course utterly.
 *
 * @package core_course
 * @copyright 2002 onwards Martin Dougiamas (http://dougiamas.com)
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../config.php');
require_once($CFG->dirroot . '/course/lib.php');
require_once($CFG->dirroot . '/course/format/talentquest/lib.php');
require_once $CFG->dirroot . '/grade/lib.php';

$id = required_param('id', PARAM_INT); // Course ID.

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);
$context = context_course::instance($course->id);
$course = course_get_format($course)->get_course();

require_login();

$categorycontext = context_coursecat::instance($course->category);
$PAGE->set_url('/course/grades.php', array('id' => $id));
$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$PAGE->set_course($course);

$course = course_get_format($course)->get_course();
$course->completion_info = courseGetCompletion($course);
$course->datesavailability = course_datesavailability($course);
$renderer = $PAGE->get_renderer('format_talentquest');

$title = get_string('gradessummary', 'format_talentquest');
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);
echo $OUTPUT->header();
//echo $OUTPUT->heading($title);
echo $renderer->print_tq_course_header($course, $context, 'gradessummary');


$grades = '';

$hidden_filter = ' AND cm.visible = 1 ';
$time_order = 'gf.timemodified ASC';
$cm_order = 'ORDER BY '.$time_order;

$where = '';

$can_edit = false;
if (has_capability('moodle/course:manageactivities', $context)){
    $can_edit = true;
}

$grades = '<div class="gradesummary-box">';

if ($can_edit){
    $users = $DB->get_records_sql("SELECT u.*, ccc.gradepass, cmc.cmcnums, ci.id as compl_enabled, ue.timecreated as enrolled, gc.avarage, cc.timecompleted as complete, c.id as cid, c.fullname as course, c.timemodified as start_date 
                            FROM {$CFG->prefix}user_enrolments as ue
                                LEFT JOIN {$CFG->prefix}user as u ON u.id = ue.userid
                                LEFT JOIN {$CFG->prefix}enrol as e ON e.id = ue.enrolid
                                LEFT JOIN {$CFG->prefix}course as c ON c.id = e.courseid
                                LEFT JOIN {$CFG->prefix}context as cx ON cx.instanceid = e.courseid AND cx.contextlevel = 50
                                LEFT JOIN {$CFG->prefix}role_assignments ra ON ra.contextid = cx.id AND ra.userid = ue.userid 
                                LEFT JOIN {$CFG->prefix}course_completions as cc ON cc.course = e.courseid
                                LEFT JOIN {$CFG->prefix}course_completion_criteria as ccc ON ccc.course = e.courseid AND ccc.criteriatype = 6
                                LEFT JOIN (SELECT * FROM {$CFG->prefix}course_completion_criteria WHERE id > 0 GROUP BY course) as ci ON ci.course = e.courseid
                                LEFT JOIN (SELECT cm.course, cmc.userid, count(cmc.id) as cmcnums FROM {$CFG->prefix}course_modules cm, {$CFG->prefix}course_modules_completion cmc WHERE cmc.coursemoduleid = cm.id $hidden_filter AND cm.completion > 0 AND cm.section > 0 GROUP BY cm.course, cmc.userid) as cmc ON cmc.course = c.id AND cmc.userid = u.id
                                LEFT JOIN (SELECT gi.courseid, g.userid, AVG( (g.finalgrade/g.rawgrademax)*100 ) AS avarage FROM {grade_items} gi LEFT JOIN {grade_grades} g ON g.itemid = gi.id LEFT JOIN {course_modules} cm ON cm.instance = gi.iteminstance LEFT JOIN {modules} m ON m.id = cm.module WHERE gi.itemname != '' AND g.finalgrade IS NOT NULL AND cm.course = gi.courseid AND cm.section > 0 AND gi.itemmodule = m.name $hidden_filter GROUP BY gi.courseid, g.userid) as gc ON gc.courseid = c.id AND gc.userid = u.id
                                    WHERE (ra.roleid = 5 OR ra.roleid IS NULL) AND e.courseid = $course->id and u.id > 0 AND u.deleted = 0 $where GROUP BY ue.userid, e.courseid");
    $grades .= '<div class="clearfix">';
   
    $grades .= '<div class="info-legeng clearfix">
                    <p><span class="current"></span>Current Grade</p>
                    <p><span class="avarage"></span>Class Avg</p>
                    <p><span class="goal"></span>Goal Grade</p>
                </div></div>';
    $grades .= '<table width="100%" class="generaltable grade-summary">';
    $grades .= '<thead>
                <tr>
                    <th align="left" class="active asc"><span>Student</span></th>
                    <th align="center"><span>Progress</span></th>
                    <th class="center" style="text-align:center;"><span>Letter</span></th>
                    <th class="center" style="text-align:center;"><span>Completed</span></th>
                    <th class="center" style="text-align:center;"><span>Score</span></th>
                    <th class="center" style="text-align:center;"><span>Details</span></th>
                </tr>
                </thead>';
    $grades .= '<tbody>';

    $users_avg = array();
    foreach($users as $item){
        if($course->id == $item->cid){
            $users_avg[$item->id] = ($item->avarage > 0) ? round($item->avarage, 0) : '-';
        }
    }

    $sum = 0; $all = 0; 
    foreach($users as $item){
        if($course->id == $item->cid){
            if($item->avarage){
                $sum = (int)$item->avarage + $sum;
                $all++;
            }
        }
    }
    $avarage = ($sum and $all) ? $sum/$all : 0;

    foreach($users as $item){


        $grades .= '<tr>
                        <td>
                            <div class="clearfix gradebook-username">
                                <span class="user-pic">'.$OUTPUT->user_picture($item, array('size'=>30)).'</span><a target="_blank" href="'.$CFG->wwwroot.'/user/profile.php?id=' . $item->id .' ">' . $item->lastname .', '. $item->firstname .'</a>
                            </div>
                        </td>
                        <td class="center">
                            <div class="info-progress">
                                <span class="current" style="width:' . ((int)$users_avg[$item->id]) .'%"></span>
                                <span class="avarage" style="width:' .( (int)$avarage) .'%"></span>
                                <span class="goal" style="width:' . ((int)$item->gradepass) .'%"></span>
                            </div>
                            <div class="info-hidden">Current grade: ' . ((int)$users_avg[$item->id]) .'% | Goal Grade ' . ( (int)$avarage) .'% | Class Avg: ' .  ((int)$item->gradepass) .'%</div>
                        </td>
                        <td class="center" style="text-align:center;">' .  get_grade_letter($users_avg[$item->id]) .'</td>
                        <td class="center" style="text-align:center;">' .  (((int)$item->cmcnums > 1) ? (int)$item->cmcnums.' activities' : (int)$item->cmcnums.' activity') .'</td>
                        <td class="center info-avarage" style="text-align:center;" id="avarage' . $item->id .'">' .  $users_avg[$item->id] .'</td>
                        <td class="center"><button class="viewdetails" value="' . $item->id .'"><i class="fa fa-angle-down"></i></button></td>
                    </tr>';
    }
    $grades .= '</tbody>';
    $grades .= '</table>';
    $can_see_gradebook = true;
}else{
    $rows = $DB->get_records_sql("SELECT cm.id, gg.rawgrademax, gg.rawgrademin, cm.course, gf.id AS itemid, (gg.finalgrade / gg.rawgrademax) *100 AS avarage, cm.instance, m.name AS module, cmc.completionstate, cmc.timemodified AS completiondate, gc.fullname as category, gf.categoryid
                                    FROM mdl_course_modules cm
                                        LEFT JOIN mdl_modules m ON m.id = cm.module
                                        LEFT JOIN mdl_user AS u ON u.id = $USER->id
                                        LEFT JOIN mdl_grade_items AS gf ON gf.iteminstance = cm.instance
                                        LEFT JOIN mdl_grade_categories AS gc ON gc.id = gf.categoryid
                                        LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gf.id AND gg.userid = u.id
                                        LEFT JOIN mdl_course_modules_completion AS cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = u.id
                                    WHERE cm.course = $course->id AND gf.courseid = cm.course AND gf.itemmodule = m.name AND cm.section > 0 $hidden_filter ORDER BY gf.timemodified DESC");
    
    $grades .= '<table class="generaltable grade-summary">';
    $grades .= '<thead>';
    $grades .= '<th class="left">Activity</th>';
    $grades .= '<th class="center">Type</th>';
    $grades .= '<th class="center" style="text-align:center;">Grade</th>';
    $grades .= '<th class="center" style="text-align:center;">Status</th>';
    $grades .= '<th class="right">Completed On</th>';
    $grades .= '</thead>';
    $grades .= '<tbody>';

    $categories = array();
    $cat_usergrades = array();
    $cat_usergrades_num = array();
    foreach($rows as $item){
        $categories[$item->categoryid] = $item->category;
        if ($item->avarage > 0){
            if (isset($cat_usergrades_num[$item->categoryid])){
                $cat_usergrades[$item->categoryid] += $item->avarage;
                $cat_usergrades_num[$item->categoryid]++;
            } else {
                $cat_usergrades[$item->categoryid] = $item->avarage;
                $cat_usergrades_num[$item->categoryid] = 1;
            }
        }
        $ins = $DB->get_record_sql("SELECT ins.name FROM mdl_$item->module ins WHERE ins.id = $item->instance");

        $grades .= '<tr>';
        $grades .= '<td>'.$ins->name.'</td>';
        $grades .= '<td>'.ucfirst($item->module).'</td>';
        $grades .= '<td class="center info-avarage">'.(($item->avarage > 0) ? round($item->avarage, 0) : '-').'</td>';
        $grades .= '<td class="center">
                        '.(($item->completionstate == 0) ? 'Incomplete' : '') .'
                        '.(($item->completionstate == 1) ? 'Completed' : '') .'
                        '.(($item->completionstate == 2) ? 'Passed' : '') .'
                        '.(($item->completionstate == 3) ? 'Failed' : '') .'
        </td>';
        $grades .= '<td class="right date">'. (($item->completionstate == 1 or $item->completionstate == 2) ? date('d M Y h:i', $item->completiondate) : '-') .'</td>';
        $grades .= '</tr>';
    }

    $grades .= '</tbody>';
    $grades .= '</table>';

    $grades .= '';
    $can_see_gradebook = false;
}
$grades .= '</div>';
echo $grades;
?>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/jquery.circliful.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/datatables/DataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/datatables/TableTools.min.js"></script>
<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/datatables/ZeroClipboard.js"></script>
<link href="<?php echo $CFG->wwwroot; ?>/theme/talentquest/style/jquery.dataTables.css" type="text/css" media="screen" rel="stylesheet" />
<script>
    var report = jQuery('.grade-summary').dataTable({
            "sDom": 'T<"clear">lfrtip',
            "sPaginationType": "full_numbers",
            "bPaginate": true,
            "bLengthChange": true,
            "bFilter": false,
            "bSort": true,
            "bInfo": false,
            "bAutoWidth": false,
            "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
            'iDisplayLength': -1,
            "oTableTools": {
             "aButtons": []
             }
        });
    
    
    jQuery('.info-stats').each(function(){
        var value = jQuery(this).html();
        var style = jQuery(this).attr('value');
        jQuery(this).html('<span class="stats '+style+'"><span></span><p>'+value+'</p></span>');
        jQuery('.stats span', this).width(value);
    });
    jQuery('.info-avarage').each(function(){
        var value = jQuery(this).html();
        jQuery(this).html('<div class="avarage-stat" data-dimension="35" data-text="'+value+'" data-info="" data-width="4" data-fontsize="10" data-percent="'+value+'" data-fgcolor="#61a9dc" data-bgcolor="#eee"></div>');
        jQuery('.avarage-stat', this).circliful();
    });
    jQuery('.grade-summary').on('click', '.viewdetails', function () {
        var button = $(this);
        var id = $(this).attr('value');
        var nTr = $(this).parents('tr')[0];
        if ( report.fnIsOpen(nTr) ){
            report.fnClose( nTr );
            jQuery(button).html('<i class="fa fa-angle-down"></i>');
            jQuery(button).next().hide();
        }else{
            jQuery.ajax({
                url: '<?php echo $CFG->wwwroot; ?>/course/format/talentquest/ajax.php?action=get_gs_activity&id=<?php echo $course->id; ?>&userid='+id,
                dataType: "html",
                beforeSend: function(){
                    jQuery(button).html('<i class="fa fa-refresh fa-spin"></i>');
                }
            }).done(function( data ) {
                jQuery(button).html('<i class="fa fa-angle-up"></i>');
                report.fnOpen( nTr, data, 'details' );
                jQuery(button).next().show();
            });

        }
    });
    
</script>

<?php
echo $OUTPUT->footer();

