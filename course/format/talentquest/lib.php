<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains main class for the course format Topic
 *
 * @since     Moodle 2.0
 * @package   format_talentquest
 * @copyright 2009 Sam Hemelryk
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot. '/course/format/lib.php');

/**
 * Main class for the talentquest course format
 *
 * @package    format_talentquest
 * @copyright  2012 Marina Glancy
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_talentquest extends format_base {

    /**
     * Returns true if this course format uses sections
     *
     * @return bool
     */
    public function uses_sections() {
        return true;
    }

    /**
     * Returns the display name of the given section that the course prefers.
     *
     * Use section name is specified by user. Otherwise use default ("Topic #")
     *
     * @param int|stdClass $section Section object from database or just field section.section
     * @return string Display name that the course format prefers, e.g. "Topic 2"
     */
    public function get_section_name($section) {
        $section = $this->get_section($section);
        if ((string)$section->name !== '') {
            return format_string($section->name, true,
                array('context' => context_course::instance($this->courseid)));
        } else {
            return $this->get_default_section_name($section);
        }
    }

    /**
     * Returns the default section name for the talentquest course format.
     *
     * If the section number is 0, it will use the string with key = section0name from the course format's lang file.
     * If the section number is not 0, the base implementation of format_base::get_default_section_name which uses
     * the string with the key = 'sectionname' from the course format's lang file + the section number will be used.
     *
     * @param stdClass $section Section object from database or just field course_sections section
     * @return string The default value for the section name.
     */
    public function get_default_section_name($section) {
        if ($section->section == 0) {
            // Return the general section.
            return get_string('section0name', 'format_talentquest');
        } else {
            // Use format_base::get_default_section_name implementation which
            // will display the section name in "Topic n" format.
            return parent::get_default_section_name($section);
        }
    }

    /**
     * The URL to use for the specified course (with section)
     *
     * @param int|stdClass $section Section object from database or just field course_sections.section
     *     if omitted the course view page is returned
     * @param array $options options for view URL. At the moment core uses:
     *     'navigation' (bool) if true and section has no separate page, the function returns null
     *     'sr' (int) used by multipage formats to specify to which section to return
     * @return null|moodle_url
     */
    public function get_view_url($section, $options = array()) {
        global $CFG;
        $course = $this->get_course();
        $url = new moodle_url('/course/view.php', array('id' => $course->id));

        $sr = null;
        if (array_key_exists('sr', $options)) {
            $sr = $options['sr'];
        }
        if (is_object($section)) {
            $sectionno = $section->section;
        } else {
            $sectionno = $section;
        }
        if ($sectionno !== null) {
            if ($sr !== null) {
                if ($sr) {
                    $usercoursedisplay = COURSE_DISPLAY_MULTIPAGE;
                    $sectionno = $sr;
                } else {
                    $usercoursedisplay = COURSE_DISPLAY_SINGLEPAGE;
                }
            } else {
                $usercoursedisplay = $course->coursedisplay;
            }
            if ($sectionno != 0 && $usercoursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                $url->param('section', $sectionno);
            } else {
                if (empty($CFG->linkcoursesections) && !empty($options['navigation'])) {
                    return null;
                }
                $url->set_anchor('section-'.$sectionno);
            }
        }
        return $url;
    }

    /**
     * Returns the information about the ajax support in the given source format
     *
     * The returned object's property (boolean)capable indicates that
     * the course format supports Moodle course ajax features.
     *
     * @return stdClass
     */
    public function supports_ajax() {
        $ajaxsupport = new stdClass();
        $ajaxsupport->capable = true;
        return $ajaxsupport;
    }

    /**
     * Loads all of the course sections into the navigation
     *
     * @param global_navigation $navigation
     * @param navigation_node $node The course node within the navigation
     */
    public function extend_course_navigation($navigation, navigation_node $node) {
        global $PAGE;
        // if section is specified in course/view.php, make sure it is expanded in navigation
        if ($navigation->includesectionnum === false) {
            $selectedsection = optional_param('section', null, PARAM_INT);
            if ($selectedsection !== null && (!defined('AJAX_SCRIPT') || AJAX_SCRIPT == '0') &&
                $PAGE->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)) {
                $navigation->includesectionnum = $selectedsection;
            }
        }

        // check if there are callbacks to extend course navigation
        parent::extend_course_navigation($navigation, $node);

        // We want to remove the general section if it is empty.
        $modinfo = get_fast_modinfo($this->get_course());
        $sections = $modinfo->get_sections();
        if (!isset($sections[0])) {
            // The general section is empty to find the navigation node for it we need to get its ID.
            $section = $modinfo->get_section_info(0);
            $generalsection = $node->get($section->id, navigation_node::TYPE_SECTION);
            if ($generalsection) {
                // We found the node - now remove it.
                $generalsection->remove();
            }
        }
    }

    /**
     * Custom action after section has been moved in AJAX mode
     *
     * Used in course/rest.php
     *
     * @return array This will be passed in ajax respose
     */
    function ajax_section_move() {
        global $PAGE;
        $titles = array();
        $course = $this->get_course();
        $modinfo = get_fast_modinfo($course);
        $renderer = $this->get_renderer($PAGE);
        if ($renderer && ($sections = $modinfo->get_section_info_all())) {
            foreach ($sections as $number => $section) {
                $titles[$number] = $renderer->section_title($section, $course);
            }
        }
        return array('sectiontitles' => $titles, 'action' => 'move');
    }

    /**
     * Returns the list of blocks to be automatically added for the newly created course
     *
     * @return array of default blocks, must contain two keys BLOCK_POS_LEFT and BLOCK_POS_RIGHT
     *     each of values is an array of block names (for left and right side columns)
     */
    public function get_default_blocks() {
        return array(
            BLOCK_POS_LEFT => array(),
            BLOCK_POS_RIGHT => array()
        );
    }

    /**
     * Definitions of the additional options that this course format uses for course
     *
     * talentquest format uses the following options:
     * - coursedisplay
     * - numsections
     * - hiddensections
     *
     * @param bool $foreditform
     * @return array of options
     */
    public function course_format_options($foreditform = false) {
        global $CFG, $DB;
        static $courseformatoptions = false;
        $id = optional_param('id', 0, PARAM_INT);
        $course = $DB->get_record('course', array('id'=>$id));

        if (isset($course->id)){
            $course = get_course($id);
            $coursecontext = context_course::instance($course->id);
            $format_options = $this->get_course_format_data($course->id);
        } else {
            $course = null;
            $coursecontext = null;
            $format_options = null;
        }
        $editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$coursecontext, 'subdirs'=>0);

        $custom_fields = $this->get_course_format_custom_fields();
        if ($courseformatoptions === false) {
            $courseconfig = get_config('moodlecourse');
            $courseformatoptions = array(
                'numsections' => array(
                    'default' => (isset($format_options['numsections'])) ? $format_options['numsections'] : $courseconfig->numsections,
                    'type' => PARAM_INT,
                ),
                'hiddensections' => array(
                    'default' => (isset($format_options['hiddensections'])) ? $format_options['hiddensections'] : $courseconfig->hiddensections,
                    'type' => PARAM_INT,
                ),
                'coursedisplay' => array(
                    'default' => (isset($format_options['coursedisplay'])) ? $format_options['coursedisplay'] : $courseconfig->coursedisplay,
                    'type' => PARAM_INT,
                ),
                'thumbnail' => array(
                    'default' => 0,
                    'type' => PARAM_INT,
                ),
            );
            if (count($custom_fields) > 0){
                foreach($custom_fields as $field){
                    $courseformatoptions[$field->name] = array(
                        'default' => '',
                        'type' => PARAM_RAW,
                    );
                }
            }
        }
        if ($foreditform && !isset($courseformatoptions['coursedisplay']['label'])) {
            $courseconfig = get_config('moodlecourse');
            $max = $courseconfig->maxsections;
            if (!isset($max) || !is_numeric($max)) {
                $max = 52;
            }
            $sectionmenu = array();
            for ($i = 0; $i <= $max; $i++) {
                $sectionmenu[$i] = "$i";
            }
            $courseformatoptionsedit = array(
                'numsections' => array(
                    'label' => new lang_string('numberweeks'),
                    'element_type' => 'hidden',
                    'default' => (isset($format_options['numsections'])) ? $format_options['numsections'] : $courseconfig->numsections,
                ),
                'hiddensections' => array(
                    'label' => new lang_string('hiddensections'),
                    'element_type' => 'hidden',
                    'default' => (isset($format_options['hiddensections'])) ? $format_options['hiddensections'] : $courseconfig->hiddensections,
                ),
                'coursedisplay' => array(
                    'label' => new lang_string('coursedisplay'),
                    'element_type' => 'hidden',
                    'default' => (isset($format_options['coursedisplay'])) ? $format_options['coursedisplay'] : $courseconfig->coursedisplay,
                ),
                'thumbnail' => array(
                    'label' => get_string('thumbnailfile', 'format_talentquest'),
                    'element_type' => 'hidden',
                ),
            );
            if (count($custom_fields) > 0){
                foreach($custom_fields as $field){
                    if ($field->type == 'select'){
                        $options = array(''=>'');
                        if (count(@unserialize($field->options) > 0)){
                            $field_options = unserialize($field->options);
                            foreach($field_options as $option){
                                $option = trim($option);
                                $options[$option] = $option;
                            }
                        }
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'select',
                            'default' => '',
                            'type' => PARAM_RAW,
                            'element_attributes' => array($options, 'class="course-custom-field"'),
                        );
                    } elseif ($field->type == 'multiselect'){
                        $options = array();
                        if (count(@unserialize($field->options) > 0)){
                            $field_options = unserialize($field->options);
                            foreach($field_options as $option){
                                $option = trim($option);
                                $options[$option] = $option;
                            }
                        }
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'select',
                            'element_attributes' => array($options, 'class="course-custom-field"'),
                        );
                    } elseif ($field->type == 'date'){
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'date_selector',
                            'default' => '',
                            'element_attributes' => array('class="course-custom-field"'),
                        );
                    } elseif ($field->type == 'datetime'){
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'date_time_selector',
                            'default' => '',
                            'element_attributes' => array('class="course-custom-field"'),
                        );
                    } elseif ($field->type == 'checkbox'){
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'advcheckbox',
                            'default' => '',
                        );
                    } elseif ($field->type == 'checkboxes'){
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'group',
                            'default' => '',
                        );
                    } elseif ($field->type == 'editor'){
                        file_prepare_standard_editor($course, $field->name, $editoroptions, $coursecontext, 'format_talentquest', $field->name.'file', (($course->id) ? $course->id : 0));
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => 'editor',
                            'element_attributes' => array(null, $editoroptions),
                        );
                    } elseif ($field->type == 'text'){
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => $field->type,
                            'default' => '',
                            'type' => PARAM_RAW,
                            'element_attributes' => array('class="course-custom-field"'),
                        );
                    } elseif ($field->type == 'textarea'){
                        $courseformatoptions[$field->name] = array(
                            'label' => $field->title,
                            'element_type' => $field->type,
                            'default' => '',
                            'type' => PARAM_RAW,
                            'element_attributes' => array('class="course-custom-field"'),
                        );
                    }
                }
            }
            $courseformatoptions = array_merge_recursive($courseformatoptions, $courseformatoptionsedit);
        }
        return $courseformatoptions;
    }

    public function get_course_format_custom_fields(){
        global $DB, $CFG;

        $fields = $DB->get_records_sql("SELECT * FROM {course_format_tq_cfields} WHERE state > 0 ORDER BY sortorder");

        return $fields;
    }

    /**
     * Adds format options elements to the course/section edit form.
     *
     * This function is called from {@link course_edit_form::definition_after_data()}.
     *
     * @param MoodleQuickForm $mform form the elements are added to.
     * @param bool $forsection 'true' if this is a section edit form, 'false' if this is course edit form.
     * @return array array of references to the added form elements.
     */
    public function create_edit_form_elements(&$mform, $forsection = false) {
        global $DB, $CFG;

        $elements = parent::create_edit_form_elements($mform, $forsection);
        $id = optional_param('id', 0, PARAM_INT);

        $course = $DB->get_record('course', array('id'=>$id));

        if (isset($course->id)){
            $course = get_course($id);
            $coursecontext = context_course::instance($course->id);
            $course_format_data = $this->get_course_format_data($course->id);
        } else {
            $course = null;
            $coursecontext = null;
            $course_format_data = null;
        }
        $courseconfig = get_config('moodlecourse');
        $mform->setDefault('numsections', (isset($course_format_data['numsections'])) ? $format_options['numsections'] : $courseconfig->numsections);
        $mform->setDefault('hiddensections', (isset($course_format_data['hiddensections'])) ? $format_options['hiddensections'] : $courseconfig->hiddensections);
        $mform->setDefault('coursedisplay', (isset($course_format_data['coursedisplay'])) ? $format_options['coursedisplay'] : $courseconfig->coursedisplay);
        
        $filesoptions = course_overviewfiles_options($course);
        $filesoptions['accepted_types'] = array('.jpg', '.gif', '.png');
        $editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$coursecontext, 'subdirs'=>0);

        $overviewfilesoptions = course_overviewfiles_options($course);
        $overviewfilesoptions['accepted_types'] = array('.jpg', '.gif', '.png', 'video');

        $thumbnail = $mform->getElementValue('thumbnail');
        $thumb_element = $mform->addElement('filemanager', 'thumbnail_file', get_string('thumbnailfile', 'format_talentquest'), null, $filesoptions);
        array_unshift($elements, $thumb_element);
        $mform->setDefault('thumbnail_file', (is_array($thumbnail)) ? 0 : $thumbnail);

        $custom_fields = $this->get_course_format_custom_fields();

        if (count($custom_fields) > 0){
            foreach($custom_fields as $field){
                if ($field->type == 'multiselect'){
                    $mform->getElement($field->name)->setMultiple(true);
                    $field_value = $mform->getElementValue($field->name);
                    if (isset($field_value[0]) and $field_value[0] != '') {
                        $mform->setDefault($field->name, unserialize($field_value[0]));
                    }
                } elseif ($field->type == 'checkboxes'){
                    $element = $mform->getElement($field->name);
                    $checkboxes = array(); $checked_values = array();
                    if (isset($course_format_data[$field->name]) and $course_format_data[$field->name] != ''){
                        $checked_values = unserialize($course_format_data[$field->name]);
                    }
                    if (count(@unserialize($field->options) > 0)){
                        $field_options = unserialize($field->options);
                        foreach($field_options as $option){
                            $option = trim($option);
                            $checkboxes[] = $mform->createElement('checkbox', $option, '', $option);
                            if (in_array($option, $checked_values)){
                                $mform->setDefault($field->name.'['.$option.']', true);
                            }
                        }
                        $element->setElements($checkboxes);
                    }
                } elseif ($field->type == 'editor'){
                    if (isset($course_format_data[$field->name])){
                        $mform->getElement($field->name)->setValue(array('text' => $course_format_data[$field->name]));
                    }
                }

                if ($field->required == 0) continue;
                if ($mform->elementExists($field->name)){
                    $mform->addRule($field->name, get_string('required_field', 'format_talentquest'), 'required', null, 'client');
                }
            }
        }

        return $elements;
    }

    /**
     * Updates format options for a course
     *
     * In case if course format was changed to 'talentquest', we try to copy options
     * 'coursedisplay', 'numsections' and 'hiddensections' from the previous format.
     * If previous course format did not have 'numsections' option, we populate it with the
     * current number of sections
     *
     * @param stdClass|array $data return value from {@link moodleform::get_data()} or array with data
     * @param stdClass $oldcourse if this function is called from {@link update_course()}
     *     this object contains information about the course before update
     * @return bool whether there were any changes to the options values
     */
    public function update_course_format_options($data, $oldcourse = null) {
        global $DB, $CFG;
        $data = (array)$data;
        $data['thumbnail'] = $data['thumbnail_file'];

        if ($oldcourse !== null) {
            $oldcourse = (array)$oldcourse;
            $options = $this->course_format_options();
            foreach ($options as $key => $unused) {
                if (!array_key_exists($key, $data)) {
                    if (array_key_exists($key, $oldcourse)) {
                        $data[$key] = $oldcourse[$key];
                    } else if ($key === 'numsections') {
                        // If previous format does not have the field 'numsections'
                        // and $data['numsections'] is not set,
                        // we fill it with the maximum section number from the DB
                        $maxsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                            WHERE course = ?', array($this->courseid));
                        if ($maxsection) {
                            // If there are no sections, or just default 0-section, 'numsections' will be set to default
                            $data['numsections'] = $maxsection;
                        }
                    }
                }
            }
        }

        $course = get_course($this->courseid);
        $coursecontext = context_course::instance($course->id);
        $editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$coursecontext, 'subdirs'=>0);

        $custom_fields = $this->get_course_format_custom_fields();
        if (count($custom_fields) > 0){
            $post_data = array();
            foreach($custom_fields as $field){
                if ($field->type == 'checkbox'){
                    $data[$field->name] = (isset($data[$field->name])) ? $data[$field->name] : 0;
                } elseif ($field->type == 'multiselect'){
                    $post_data[$field->name] = (isset($data[$field->name])) ? $data[$field->name] : optional_param_array($field->name, array(), PARAM_RAW);
                    $data[$field->name] = (count($post_data[$field->name]) > 0) ? serialize($post_data[$field->name]) : '';
                } elseif ($field->type == 'checkboxes'){
                    $vals = (isset($data[$field->name])) ? $data[$field->name] : optional_param_array($field->name, array(), PARAM_RAW);
                    $post_data[$field->name] = array();
                    if (count($vals)){
                        foreach ($vals as $k=>$v){
                            $post_data[$field->name][] = $k;
                        }
                    }
                    $data[$field->name] = (count($post_data[$field->name]) > 0) ? serialize($post_data[$field->name]) : '';
                } elseif ($field->type == 'editor'){
                    $post_data[$field->name] = (isset($data[$field->name])) ? $data[$field->name] : optional_param_array($field->name, array(), PARAM_RAW);
                    $data[$field->name] = (isset($post_data[$field->name]['text'])) ? $post_data[$field->name]['text'] : '';
                }
            }
        }

        $changed = $this->update_format_options($data);
        if ($changed && array_key_exists('numsections', $data)) {
            // If the numsections was decreased, try to completely delete the orphaned sections (unless they are not empty).
            $numsections = (int)$data['numsections'];
            $maxsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                        WHERE course = ?', array($this->courseid));
            for ($sectionnum = $maxsection; $sectionnum > $numsections; $sectionnum--) {
                if (!$this->delete_section($sectionnum, false)) {
                    break;
                }
            }
        }

        $fs = get_file_storage();
        $fs->delete_area_files($coursecontext->id, 'format_talentquest', 'thumbnail', $this->courseid);
        file_save_draft_area_files($data['thumbnail'], $coursecontext->id, 'format_talentquest', 'thumbnail', $this->courseid, array('subdirs' => 0, 'maxfiles' => 1));

        if (count($custom_fields) > 0){
            foreach($custom_fields as $field){
                if ($field->type != 'editor') continue;
                file_postupdate_standard_editor($course, $field->name, $editoroptions, $coursecontext, 'format_talentquest', $field->name.'file', $course->id);
            }
        }

        return $changed;
    }

    /**
     * Whether this format allows to delete sections
     *
     * Do not call this function directly, instead use {@link course_can_delete_section()}
     *
     * @param int|stdClass|section_info $section
     * @return bool
     */
    public function can_delete_section($section) {
        return true;
    }

    public function get_course_format_data($courseid) {
        global $DB;
        $data = array();
        $course_data = $DB->get_records('course_format_options', array('courseid'=>$courseid, 'format'=>'talentquest'));
        if (count($course_data)){
            foreach ($course_data as $item){
                $data[$item->name] = $item->value;
            }
        }

        return $data;
    }
}

function format_talentquest_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');
    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_COURSE) {
        return false;
    }
    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'format_talentquest', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

function courseGetCompletion($course) {
    global $CFG, $PAGE, $DB, $USER, $OUTPUT;

    require_once("{$CFG->libdir}/completionlib.php");
    require_once($CFG->dirroot.'/course/lib.php');

    $result = new stdClass();
    $result->completion = 0;
    $result->status = 'notyetstarted';

    $context = context_course::instance($course->id);
    // Can edit settings?
    $can_edit = has_capability('moodle/course:update', $context);

    if ($can_edit){

        $completion = $DB->get_record_sql("SELECT c.id, sc.students_count, cc.completions_count
            FROM {course} c 
                LEFT JOIN (SELECT c.id, COUNT(ra.id) AS students_count FROM {course} c LEFT JOIN {context} ct ON c.id = ct.instanceid LEFT JOIN {role_assignments} ra ON ra.contextid = ct.id WHERE ra.roleid = 5 ) sc ON sc.id = c.id 
                LEFT JOIN (SELECT c.id, COUNT(ra.id) AS completions_count FROM {course} c LEFT JOIN {context} ct ON c.id = ct.instanceid LEFT JOIN {role_assignments} ra ON ra.contextid = ct.id AND ra.roleid = 5 LEFT JOIN {course_completions} cc ON cc.course = c.id AND cc.userid = ra.userid WHERE cc.timecompleted IS NOT NULL ) cc ON cc.id = c.id 
            WHERE c.id = $course->id");

        if ($completion->completions_count > 0 and $completion->students_count > 0){
            $result->completion = round(($completion->completions_count / $completion->students_count) * 100);
        }
        $result->status = 'pending';
    } else {

        // Get course completion data.
        $info = new completion_info($course);

        // Load criteria to display.
        $completions = $info->get_completions($USER->id);

        if ($info->is_tracked_user($USER->id)) {

            // For aggregating activity completion.
            $activities = array();
            $activities_complete = 0;
            $activities_viewed = 0;

            // For aggregating course prerequisites.
            $prerequisites = array();
            $prerequisites_complete = 0;

            // Flag to set if current completion data is inconsistent with what is stored in the database.
            $pending_update = false;

            // Loop through course criteria.
            foreach ($completions as $completion) {
                $criteria = $completion->get_criteria();
                $complete = $completion->is_complete();

                if (!$pending_update && $criteria->is_pending($completion)) {
                    $pending_update = true;
                }

                // Activities are a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_ACTIVITY) {
                    $activities[$criteria->moduleinstance] = $complete;

                    if ($complete) {
                        $activities_complete++;
                    }

                    continue;
                }

                // Prerequisites are also a special case, so cache them and leave them till last.
                if ($criteria->criteriatype == COMPLETION_CRITERIA_TYPE_COURSE) {
                    $prerequisites[$criteria->courseinstance] = $complete;

                    if ($complete) {
                        $prerequisites_complete++;
                    }

                    continue;
                }
            }

            $itemsCompleted  = $activities_complete + $prerequisites_complete;
            $itemsCount      = count($activities) + count($prerequisites);

            // Aggregate completion.
            if (count($itemsCount) > 0) {
                $result->completion = round(($itemsCompleted / $itemsCount) * 100);
            }

            // Is course complete?
            $coursecomplete = $info->is_course_complete($USER->id);

            // Load course completion.
            $params = array(
                'userid' => $USER->id,
                'course' => $course->id
            );
            $ccompletion = new completion_completion($params);

            // Has this user completed any criteria?
            $criteriacomplete = $info->count_course_user_data($USER->id);

            if ($pending_update) {
                $status = 'pending';
            } else if ($coursecomplete) {
                $status = 'completed';
                $result->completion = 100;
            } else if (!$criteriacomplete && !$ccompletion->timestarted) {
                $status = 'notyetstarted';
            } else {
                $status = 'inprogress';
            }

            $result->status = $status;
        }
        if ($result->status == 'notyetstarted'){
            $viewed = $DB->get_record_sql("SELECT COUNT(cmc.id) as viewed 
                                            FROM {course_modules_completion} cmc
                                                WHERE cmc.coursemoduleid IN (SELECT id FROM {course_modules} WHERE course = $course->id) AND cmc.userid = $USER->id AND (cmc.viewed > 0 OR cmc.completionstate > 0)");
            if ($viewed->viewed){
                $result->status = 'inprogress';
            }
        }
    }

    return $result;
}

function course_get_course_progress($course){

}

function courseGetUserFinalgrade($user = 0, $course) {
    global $CFG, $USER;
    $userGrade = array();

    require_once($CFG->dirroot.'/grade/querylib.php');
    $user = (isset($user->id)) ? $user : $USER;

    $grade = grade_get_course_grade($user->id, $course->id);
    $userGrade['grade'] = ($grade->grade) ? $grade->grade : $grade->grade->str_grade;
    $userGrade['percentage'] = ($grade->grade) ? round( ( floatval($grade->grade) / floatval($grade->item->grademax) ) *100 ).'%' : $grade->str_grade;

    return $userGrade;
}

function course_get_course_teachers($course){
    global $CFG, $DB, $OUTPUT;

    $teachers = $DB->get_records_sql("SELECT u.*
                                            FROM {user} AS u 
                                                LEFT JOIN {role_assignments} AS ra ON u.id = ra.userid
                                                LEFT JOIN {context} AS ct ON ra.contextid = ct.id AND ra.roleid=3 
                                                LEFT JOIN {course} AS c ON c.id = ct.instanceid    
                                            WHERE c.id=".$course->id);
    $course_teachers = array(); $output_teachers = '';
    if(count($teachers)){
        foreach ($teachers as $teacher){
            $course_teachers[] = fullname($teacher);
        }
        $output_teachers = 'By '.implode(', ', $course_teachers);
    }
    $output = html_writer::tag('div', $output_teachers, array('class'=>'course-teachers-box'));
    return $output;
}

function course_print_rating($course, $is_enrolled = false){
    global $CFG, $DB, $USER;
    $output = '';

    $course_rating = $DB->get_record_sql("SELECT ROUND(SUM(r.value)/COUNT(r.id)) as rate FROM {course_rating} r LEFT JOIN {user} u ON u.id = r.userid WHERE r.courseid = $course->id AND u.deleted = 0 GROUP BY r.courseid");

    if ($is_enrolled){
        $output .= html_writer::start_tag('div', array('class'=>'star-rating course-star-rating', 'id'=>'course_star_rating'));
        $params = array('type'=>'radio', 'name'=>'rate', 'class'=>'rating');

        for ($i = 1; $i <=5; $i++){
            $params['value'] = $i;
            if (isset($params['checked'])) unset($params['checked']);
            if (isset($course_rating->rate) and $course_rating->rate == $i) $params['checked'] = 'checked';
            $output .= html_writer::empty_tag('input', $params);
        }

        $output .= html_writer::end_tag('div');

        $output .= '<script>
            jQuery("#course_star_rating").rating(function(vote, event){
                 jQuery.ajax({
                    url: "'.$CFG->wwwroot.'/course/format/talentquest/ajax.php?action=set-rating&id='.$course->id.'",
                    type: "GET",
                    data: {rate: vote},
                });
            });
        </script>';
    } else {
        $output .= html_writer::start_tag('div', array('class'=>'star-rating course-star-rating'));
        $output .= html_writer::start_tag('div', array('class'=>'stars'));

        for ($i = 1; $i <=5; $i++){
            $params = array('class'=>'stars', 'title'=>$i, 'class'=>'star');
            if (isset($course_rating->rate) and $course_rating->rate >= $i) $params['class'] .= ' fullStar';
            $output .= html_writer::tag('a', '', $params);
        }

        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
    }

    return $output;
}


function course_get_custom_fields($course){
    global $CFG, $DB;

    $fields = array('list'=>array(), 'tab'=>array());
    $course_custom_fields = $DB->get_records_sql('SELECT * FROM {course_format_tq_cfields} WHERE state > 0 and display != "" ORDER BY sortorder');

    if (count($course_custom_fields)){
        foreach($course_custom_fields as $field){
            if (isset($course->{$field->name}) and !empty($course->{$field->name})){
                if ($field->type == 'multiselect' or $field->type == 'checkboxes'){
                    $values = unserialize($course->{$field->name});
                    if (count($values) and is_array($values)){
                        $value = implode(', ', $values);
                    }
                } elseif ($field->type == 'date'){
                    $value = date('d M Y', $course->{$field->name});
                } elseif ($field->type == 'datetime'){
                    $value = date('g:i a, d M Y', $course->{$field->name});
                } elseif ($field->type == 'checkbox'){
                    $value = 'Yes';
                } elseif ($field->type == 'editor'){
                    $value = $course->{$field->name};
                    /*$value = get_course_formatted_field($course, $field->name, $field->name.'file',
                            array('overflowdiv' => true, 'noclean' => false, 'para' => false));*/
                } else {
                    $value = $course->{$field->name};
                }
                if ($value != ''){
                    $fields[$field->display][$field->name] = array('title'=>$field->title, 'value'=>$value);
                }
            }
        }
    }
    return $fields;
}

function get_course_formatted_field($course, $field, $filearea, $options = array()) {
    global $CFG;

    require_once($CFG->libdir. '/filelib.php');
    if (!$course->{$field}) {
        return '';
    }
    $options = (array)$options;
    $context = context_course::instance($course->id);

    $text = file_rewrite_pluginfile_urls($course->{$field}, 'pluginfile.php', $context->id, 'format_kaltura', $filearea, null);
    $text = format_text($text, 1, $options, $course->id);

    return $text;
}

function get_grade_letter($sum){
    $letter = '';
    if($sum >= 93){
        $letter = "A";
    }elseif($sum >= 90){
        $letter = "A-";
    }elseif($sum >= 87){
        $letter = "B+";
    }elseif($sum >= 83){
        $letter = "B";
    }elseif($sum >= 80){
        $letter = "B-";
    }elseif($sum >= 77){
        $letter = "C+";
    }elseif($sum >= 73){
        $letter = "C";
    }elseif($sum >= 70){
        $letter = "C-";
    }elseif($sum >= 67){
        $letter = "D+";
    }elseif($sum >= 63){
        $letter = "D";
    }elseif($sum >= 60){
        $letter = "D-";
    }elseif($sum < 60 and $sum > 0){
        $letter = "F";
    } else {
        $letter = "-";
    }
    return $letter;
}

function get_course_image($course){
    // display course image
    $context = context_course::instance($course->id);
    $contentimages = '';
    $image_files = array();
    $fs = get_file_storage();
    $imgfiles = $fs->get_area_files($context->id, 'format_talentquest', 'thumbnail', $course->id);
    foreach ($imgfiles as $file) {
        $filename = $file->get_filename();
        $filetype = $file->get_mimetype();
        if ($filename == '.' or !$filetype) continue;
        $url = moodle_url::make_pluginfile_url($context->id, 'format_talentquest', 'thumbnail', $course->id, '/', $filename);
        $contentimages .= html_writer::empty_tag('img', array('src' => $url->out()));
    }

    if($contentimages == ""){
        $contentimages .= html_writer::empty_tag('img', array('src' => $CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg"));
    }

    return $contentimages;
}

function get_course_image_url($course){
    global $CFG, $DB;
    // display course image
    $context = context_course::instance($course->id);
    $imageurl = '';
    $image_files = array();
    $fs = get_file_storage();
    $imgfiles = $fs->get_area_files($context->id, 'format_talentquest', 'thumbnail', $course->id);
    foreach ($imgfiles as $file) {
        $filename = $file->get_filename();
        $filetype = $file->get_mimetype();
        if ($filename == '.' or !$filetype) continue;
        $url = moodle_url::make_pluginfile_url($context->id, 'format_talentquest', 'thumbnail', $course->id, '/', $filename);
        $imageurl = $url->out();
    }

    if($imageurl == ""){
        $imageurl .= $CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg";
    }

    return $imageurl;
}

function get_certifications_list(){
    global $CFG, $DB;
    $list = array();

    $plans = $DB->get_records_sql("SELECT * FROM {local_plans} WHERE visible = 1 AND type = 1 ORDER BY name");
    if (count($plans)){
        foreach ($plans as $plan){
            $list[$plan->id] = $plan->name;
        }
    }
    return $list;
}

function get_course_certifications($courseid){
    global $CFG, $DB;
    $list = array();

    $certifications = $DB->get_records_sql("SELECT p.id FROM {local_plans_courses} pc LEFT JOIN {local_plans} p ON p.id = pc.planid WHERE pc.courseid = $courseid AND p.type = 1");
    if (count($certifications)){
        foreach($certifications as $certification){
            $list[] = $certification->id;
        }
    }
    return $list;
}

function course_update_certifications($course, $courseid){
    global $DB;

    if (isset($course->certifications)){
        $certifications = $DB->get_records_sql("SELECT pc.id FROM {local_plans_courses} pc LEFT JOIN {local_plans} p ON p.id = pc.planid WHERE pc.courseid = $courseid AND p.type = 1");
        if (count($certifications)){
            foreach($certifications as $certification){
                $DB->delete_records('local_plans_courses', array('id'=>$certification->id));
            }
        }
        if (count($course->certifications)){
            foreach ($course->certifications as $certification){
                $new_relation = new stdClass();
                $new_relation->planid = $certification;
                $new_relation->courseid = $courseid;
                $new_relation->timecreated = time();
                $DB->insert_record('local_plans_courses', $new_relation);
            }
        }
    }
}

function course_datesavailability($course){

    if (has_capability('moodle/course:manageactivities', context_course::instance($course->id))){
        return '';
    }

    $now = time();
    if ($course->startdate > $now){
        return get_string('coursenotstarts', 'format_talentquest');
    } elseif ($course->enddate > 0 and $course->enddate < $now){
        return get_string('courseoverdue', 'format_talentquest');
    }

    return '';
}

function format_check_availability($course, $context){
    global $DB, $CFG, $USER;
    
    if (!isloggedin()) array('enrolled'=>false, 'message'=>'', 'available'=>false);
    
    $status = array('enrolled'=>is_enrolled($context, $USER), 'message'=>'', 'available'=>true);
    return $status;
    $enrolls = $DB->get_records_sql("SELECT ue.id, ue.status, ue.timestart, ue.timeend, e.enrol, e.customint1 FROM {user_enrolments} ue 
                                        LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                            WHERE e.courseid = $course->id AND ue.userid = $USER->id");
    
    if (count($enrolls)){
        foreach ($enrolls as $enrol){
            if ($enrol->enrol == 'approval' and $enrol->status > 0){
                $status['message'] = get_string('pendingmessage', 'enrol_approval');
                $status['available'] = false;
                //$status['redirecturl'] = $CFG->wwwroot.'/enrol/course.php?id='.$course->id;
            } elseif ($enrol->enrol == 'plans'){
                $plan = $DB->get_record('local_plans', array('id'=>$enrol->customint1));
                if ($plan){
                    $courserecord = $DB->get_record('local_plans_courses', array('planid'=>$plan->id, 'courseid'=>$course->id));
                    if (!empty($courserecord->restriction)){
                        $completions = $DB->get_records_sql("SELECT * FROM {course_completions} WHERE userid = :userid AND timecompleted > 0 AND course IN ($courserecord->restriction)", array('userid'=>$USER->id));
                        if (!$completions or count($completions) == 0){
                            $status['available'] = false;
                        } else {
                            $restrictions = explode(',',$courserecord->restriction);
                            if (count($completions) != count($restrictions)){
                                $status['available'] = false;
                            }
                        }
                        if (!$status['available']){
                            $restrictcourses = $DB->get_records_sql("SELECT c.*, cc.id as completion FROM {course} c LEFT JOIN {course_completions} cc ON cc.course = c.id AND cc.userid = :userid AND cc.timecompleted > 0 LEFT JOIN {local_plans_courses} pc ON pc.planid = :planid AND pc.courseid = c.id WHERE c.id IN ($courserecord->restriction) ORDER BY pc.sortorder", array('userid'=>$USER->id, 'planid'=>$plan->id));
                            $status['message'] = get_string('pendingmessage', 'local_plans');
                            if (count($restrictcourses)){
                                $status['message'] = '<p>'.get_string('pendingmessage', 'local_plans').':</p>';
                                $status['message'] .= '<ul>';
                                foreach($restrictcourses as $rc){
                                    if ($rc->completion) continue;
                                    $status['message'] .= '<li><a href="'.$CFG->wwwroot.'/course/view.php?id='.$rc->id.'">'.$rc->fullname.'</a></li>';
                                }
                                $status['message'] .= '</ul>';
                            }
                        }
                    }
                }
            }
        }   
    }
    
    return (object)$status;
}

