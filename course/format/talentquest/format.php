<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * talentquest course format.  Display the whole course as "talentquest" made of modules.
 *
 * @package format_talentquest
 * @copyright 2016 SEBALE
 * @author sebale.net
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/completionlib.php');
// Horrible backwards compatible parameter aliasing..
if ($topic = optional_param('topic', 0, PARAM_INT)) {
    $url = $PAGE->url;
    $url->param('section', $topic);
    debugging('Outdated topic param passed to course/view.php', DEBUG_DEVELOPER);
    redirect($url);
}

$context = context_course::instance($course->id);

// End backwards-compatible aliasing..
$tab = optional_param('tab', 'curriculum', PARAM_RAW);

if (($marker >=0) && has_capability('moodle/course:setcurrentsection', $context) && confirm_sesskey()) {
    $course->marker = $marker;
    course_set_marker($course->id, $marker);
}

// make sure all sections are created
$course = course_get_format($course)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));
$course->completion_info = courseGetCompletion($course);
$course->datesavailability = course_datesavailability($course);

$renderer = $PAGE->get_renderer('format_talentquest');
$cfields = course_get_custom_fields($course);

$course_description = $renderer->get_course_formatted_summary($course,
						  array('overflowdiv' => true, 'noclean' => false, 'para' => false));    

echo html_writer::start_tag('div', array('class'=>'course-title-box'));
$course_image_url = get_course_image_url($course);
echo html_writer::tag('div', '', array('class'=>'course-image', 'style'=>'background-image: url("'.$course_image_url.'");'));
echo html_writer::start_tag('div', array('class'=>'course-title'));
echo $course->fullname;
    if (count($cfields['list'])){
        echo html_writer::start_tag('div', array('class'=>'course-custom-toggler'));
            echo html_writer::tag('i', '', array('onclick'=>'toggleCourseCF();', 'class'=>'fa fa-angle-down'));
            echo html_writer::tag('i', '', array('onclick'=>'toggleCourseCF();', 'class'=>'fa fa-angle-up hidden'));
        echo html_writer::end_tag('div');
    }
echo html_writer::end_tag('div');
echo course_get_course_teachers($course);
echo html_writer::end_tag('div');

echo course_print_rating($course, is_enrolled($context, $USER));

if(is_enrolled($context, $USER)){
    if ($course->completion_info->status == 'pending' or $course->completion_info->status == 'inprogress' or $course->completion_info->status == 'completed' or $course->completion_info->status == 'notyetstarted'){
        $progress_info = html_writer::tag('label', get_string('courseprogress', 'format_talentquest').':') .
            html_writer::tag('div', 
            html_writer::start_tag('div', array('class' => 'progres-bar')).
            html_writer::tag('div', html_writer::tag('span', intval($course->completion_info->completion).'%',array('class' => 'procent')), array('class' => 'progres','style'=>'width:'.$course->completion_info->completion.'%;')).
            html_writer::end_tag('div'),
                array('class'=>'clearfix progress-wrapper'));
        echo html_writer::tag('div', $progress_info, array('class' => 'course-progress'));
    }
}

if (!empty($course->startdate) or !empty($course->enddate)){
    echo html_writer::start_tag('div', array('class'=>'course-dates clearfix'));
        if (!empty($course->startdate)){
            echo html_writer::start_tag('div', array('class'=>'course-date-item'));
                echo html_writer::tag('label', get_string('startdate', 'format_talentquest').':');
                echo html_writer::tag('span', date('m/d/Y', $course->startdate), array('class'=>'course-date'));
            echo html_writer::end_tag('div');
        }
        if (!empty($course->enddate)){
            echo html_writer::start_tag('div', array('class'=>'course-date-item'));
                echo html_writer::tag('label', get_string('enddate', 'format_talentquest').':');
                echo html_writer::tag('span', date('m/d/Y', $course->enddate), array('class'=>'course-date'));
            echo html_writer::end_tag('div');
        }
    echo html_writer::end_tag('div');
}

if (count($cfields['list'])){
    echo html_writer::start_tag('ul', array('class'=>'course-custom-list clearfix'));
        foreach ($cfields['list'] as $fieldname=>$field){
            echo html_writer::start_tag('li', array('class'=>'course-cutom-item'));
                echo html_writer::tag('label', $field['title'].':');
                echo html_writer::tag('span', $field['value']);
            echo html_writer::end_tag('li');
        }
    echo html_writer::end_tag('ul');
}

echo html_writer::start_tag('div', array('class'=>'nav-tabs-header'));
    echo html_writer::start_tag('ul', array('class'=>'nav-tabs nav-tabs-simple clearfix'));
        echo html_writer::tag('li', '<a data-toggle="tab" href="#curriculum">'.get_string('curriculum', 'format_talentquest').'</a>', array('class'=>'header-curriculum'.(($tab == 'curriculum') ? ' active' : '')));
        
        if (!empty($course_description)){
            echo html_writer::tag('li', '<a data-toggle="tab" href="#course-description">'.get_string('coursedescription', 'format_talentquest').'</a>', array('class'=>'header-course-description'.(($tab == 'course-description') ? ' active' : '')));
        }

        echo html_writer::tag('li', '<a data-toggle="tab" href="'.$CFG->wwwroot.'/blog/index.php?courseid='.$course->id.'">'.get_string('courseblog', 'format_talentquest').'</a>', array('class'=>'header-course-description', 'onclick'=>'location="'.$CFG->wwwroot.'/blog/index.php?courseid='.$course->id.'"'));

        echo html_writer::tag('li', '<a data-toggle="tab" href="'.$CFG->wwwroot.'/course/format/talentquest/grades.php?id='.$course->id.'">'.get_string('gragessummary', 'format_talentquest').'</a>', array('class'=>'header-course-description', 'onclick'=>'location="'.$CFG->wwwroot.'/course/format/talentquest/grades.php?id='.$course->id.'"'));

        if (count($cfields['tab'])){
            foreach ($cfields['tab'] as $fieldname=>$field){
                echo html_writer::tag('li', '<a data-toggle="tab" href="#'.$fieldname.'">'.$field['title'].'</a>', array('class'=>'header-'.$fieldname.(($tab == $fieldname) ? ' active' : '')));
            }
        }

    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div');

echo html_writer::start_tag('div', array('class'=>'tab-content'));

    echo html_writer::start_tag('div', array('id'=>'curriculum', 'class'=>'tab-pane'.(($tab == 'curriculum') ? ' active' : '')));
        //echo html_writer::start_tag('div', array('class'=>'course-curriculum-box'.((!empty($course->datesavailability)) ? ' locked' : '')));
        echo html_writer::start_tag('div', array('class'=>'course-curriculum-box'));
        
        /*if (!empty($course->datesavailability)) {
            echo html_writer::start_tag('div', array('class'=>'locked-description-box'));
                echo html_writer::tag('i', '', array('class'=>'fa fa-lock'));
                echo html_writer::tag('span', $course->datesavailability, array('class'=>'locked-description'));
            echo html_writer::end_tag('div');
        }*/
            
        if (!empty($displaysection)) {
            $renderer->print_single_section_page($course, null, null, null, null, $displaysection);
        } else {
            $renderer->print_multiple_section_page($course, null, null, null, null);
        }
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('div');
    
    if (!empty($course_description)){
        echo html_writer::start_tag('div', array('id'=>'course-description', 'class'=>'tab-pane'.(($tab == 'course-description') ? ' active' : '')));
            echo $course_description;
        echo html_writer::end_tag('div');
    }

    if (count($cfields['tab'])){
        foreach ($cfields['tab'] as $fieldname=>$field){
            echo html_writer::start_tag('div', array('id'=>$fieldname, 'class'=>'tab-pane'.(($tab == $fieldname) ? ' active' : '')));
                echo $field['value'];
            echo html_writer::end_tag('div');
        }
    }

echo html_writer::end_tag('div');

// Include course format js module
$PAGE->requires->js('/course/format/talentquest/format.js');
