<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * talentquest version file.
 *
 * @package    format_talentquest
 * @author     talentquest
 * @copyright  2016 talentquest, talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../../config.php');
require ('course_edit_options_table.php');

$search     = optional_param('search', '', PARAM_RAW);
$filter     = optional_param('filter', '1', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

$USER->editing = 0;
$systemcontext   = context_system::instance();
require_login();
require_capability('format/talentquest:manage_options', $systemcontext);
$title = get_string('course_options', 'format_talentquest');

if ($action == 'delete' and $id) {
    $DB->delete_records('course_format_tq_cfields', array('id'=>$id));
    redirect(new moodle_url("/course/format/talentquest/course_options.php"));
} elseif ($action == 'show' and $id){
    $course_option = $DB->get_record('course_format_tq_cfields', array('id'=>$id));
    if ($course_option){
        $course_option->state = 1;
        $DB->update_record('course_format_tq_cfields', $course_option);
    }
    redirect(new moodle_url("/course/format/talentquest/course_options.php"));
} elseif($action == 'hide' and $id){
    $course_option = $DB->get_record('course_format_tq_cfields', array('id'=>$id));
    if ($course_option){
        $course_option->state = 0;
        $DB->update_record('course_format_tq_cfields', $course_option);
    }
    redirect(new moodle_url("/course/format/talentquest/course_options.php"));
} elseif($action == 'move'){
    $eid         = optional_param('eid', 0, PARAM_INT);
    $moveafter   = optional_param('moveafter', 0, PARAM_INT);
    
    $moving_field = $DB->get_record("course_format_tq_cfields", array('id'=>$eid));
    
    if ($moveafter == 0){
        
        $moving_field->sortorder = 0;
        $DB->update_record("course_format_tq_cfields", $moving_field);
        
        $cfields = $DB->get_records_sql("SELECT id, sortorder FROM {course_format_tq_cfields} WHERE id != $eid ORDER BY sortorder");
        
        $i = 1;
        foreach ($cfields as $field){
            $field->sortorder = $i++;
            $DB->update_record("course_format_tq_cfields", $field);
        }    
    } else {
        $moveafter_field = $DB->get_record("course_format_tq_cfields", array('id'=>$moveafter));
        
        $moving_field->sortorder = ++$moveafter_field->sortorder;
        $DB->update_record("course_format_tq_cfields", $moving_field);
        
        $cfields = $DB->get_records_sql("SELECT id, sortorder FROM {course_format_tq_cfields} WHERE id != $eid ORDER BY sortorder");
        
        $i = 0;
        foreach ($cfields as $field){
            $field->sortorder = $i++;
            $DB->update_record("course_format_tq_cfields", $field);
            if ($field->id == $moveafter_field->id) $i++;
        }    
    }
    
    exit;
}

$PAGE->set_url(new moodle_url("/course/format/talentquest/course_options.php", array()));
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new course_options_table('table', $search, $filter);
$table->attributes['class'] .= ' sorting';
$table->is_collapsible = false;
$table->sortable(true, 'sortorder');

if (!$table->is_downloading()) {
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    echo html_writer::start_tag("div", array('class'=>'course-options-box'));
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'course_options-search-form'));
    echo html_writer::start_tag('select', array('name' => 'filter', 'class'=>'status-filter', 'onchange'=>'jQuery(this).closest("form").submit();'));
    echo html_writer::tag('option', get_string('show_all'), (($filter == '-1') ? array('value'=>'-1', 'selected'=>'selected') : array('value'=>'-1')));
    echo html_writer::tag('option', get_string('active'), (($filter == '1') ? array('value'=>'1', 'selected'=>'selected') : array('value'=>'1')));    
    echo html_writer::tag('option', get_string('inactive'), (($filter == '0') ? array('value'=>'0', 'selected'=>'selected') : array('value'=>'0')));    
    echo html_writer::end_tag('select');
    echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'format_talentquest').' ...', 'value' => $search));
    /*echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search', 'format_talentquest')));*/
    
    $url = new moodle_url('/course/format/talentquest/course_edit_options.php');
    echo html_writer::link($url, '<i class="fa fa-plus"></i> '.get_string('create_new', 'format_talentquest'), array('class'=>'btn btn-warning'));
    
    echo html_writer::end_tag("form");
}

$table->out(20, true);

if (!$table->is_downloading()) {
    echo html_writer::end_tag("div");
    echo $OUTPUT->footer();
?>

<script src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/jquery-sortable.js" type="text/javascript"></script>
<script>
    jQuery('table.sorting').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        handle: '.move-action',
        horizontal: false,
        placeholder: '<tr class=\"placeholder\"><td colspan="4"></td></tr>',
        onDrag: function (item, group, _super) {
            item.addClass('active');
            jQuery('table.sorting tr').addClass('not-active');
        },
        onDrop: function  (item, container, _super) {
            jQuery('table.sorting tr').removeClass('not-active');
            item.removeClass('active');
            newIndex = item.index()
            var eid = item.attr('class').replace(" dragged", ""); var prep = newIndex;
            if (prep > 0){
                var row = jQuery('table.sorting tr').eq(prep);
                var new_eid = parseInt(row.attr('class'));
            } else {
                new_eid = 0;
            }
            
            if (new_eid || newIndex == 0){
                $.ajax({
                    type: 'GET',
                    url: '<?php echo $CFG->wwwroot; ?>/course/format/talentquest/course_options.php?action=move&eid='+eid+'&moveafter='+new_eid
                });
            }
            _super(item)
        },
        onCancel: function (item, container, _super, event) {
            jQuery('table.sorting tr').removeClass('not-active');
            item.removeClass('active');
            _super(item)
        },
    });
</script>
<?php
}

