<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * talentquest format file.
 *
 * @package    format_talentquest
 * @author    talentquest
 * @copyright 2016 talentquest, talentquest.com
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class course_options_table extends table_sql {
    function __construct($uniqueid, $search, $filter) {
		global $CFG, $USER;

        parent::__construct($uniqueid);

        $columns = array('id', 'sortorder', 'title', 'type', 'required', 'actions');
        $this->no_sorting('title');
        $this->no_sorting('actions');
        $this->no_sorting('required');
        $this->no_sorting('type');
        $this->define_columns($columns);
        $headers = array(
            'ID',
            '#',
            get_string('title', 'format_talentquest'),
            get_string('type', 'format_talentquest'),
            get_string('required', 'format_talentquest'),
            get_string('actions', 'format_talentquest'));
       
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (title LIKE '%$search%' OR options LIKE '%$search%')" : "";
        $fields = "id, title, type, required, state, sortorder ";
        $from = "{course_format_tq_cfields} ";
        $where = 'id > 0'.$sql_search;
        if ($filter != '-1' and $filter != ''){
            $where .= ' AND state = '.$filter;
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/course/format/talentquest/course_options.php");
    }
    
    function col_type($values) {
        $choices = array();
        $choices['text'] = get_string('textbox', 'format_talentquest');
        $choices['select'] = get_string('select', 'format_talentquest');
        $choices['multiselect'] = get_string('multiselect', 'format_talentquest');
        $choices['date'] = get_string('date', 'format_talentquest');
        $choices['datetime'] = get_string('datetime', 'format_talentquest');
        $choices['checkbox'] = get_string('checkbox', 'format_talentquest');
        $choices['checkboxes'] = get_string('checkboxes', 'format_talentquest');
        $choices['textarea'] = get_string('textarea', 'format_talentquest');
        $choices['editor'] = get_string('editor', 'format_talentquest');
        
        return (isset($choices[$values->type])) ? $choices[$values->type] : '';
    }
    
    function col_required($values) {
        return ($values->required) ? get_string('required', 'format_talentquest') : get_string('optional', 'format_talentquest');
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strshow  = get_string('show');
      $strhide  = get_string('inactive');
      $strmove  = get_string('active');

        $edit = array();
        $aurl = new moodle_url('/course/format/talentquest/course_edit_options.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/course/format/talentquest/course_options.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')));
        
        if ($values->state > 0){
            $aurl = new moodle_url('/course/format/talentquest/course_options.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/course/format/talentquest/course_options.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall'))); 
        }
        
        $edit[] = $OUTPUT->action_icon('javascript:void(0);', new pix_icon('t/move', $strmove, 'core', array('class' => 'iconsmall move-action')));
        
      return implode('', $edit);
    }
    
    function print_row($row, $classname = '') {
        echo $this->get_row_html($row, $classname.(($classname != '') ? ' ' : '').((isset($row[0])) ? $row[0] : ''));
    }
}
