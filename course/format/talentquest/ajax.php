<?php

require_once('../../../config.php');
require_once('lib.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$state	  = optional_param('state', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'set-rating' and $id){	
    
	$rate	          = optional_param('rate', 0, PARAM_INT);
    
    if ($rate > 0){
        $rating = $DB->get_record('course_rating', array('courseid'=>$id, 'userid'=>$USER->id)); 
        if ($rating){
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->update_record('course_rating', $rating);
        } else {
            $rating = new stdClass();
            $rating->courseid = $id;
            $rating->userid = $USER->id;
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->insert_record('course_rating', $rating);     
        }
    }
    
    $event_properties = array('userid' => $USER->id, 'objectid' => $id, 'relateduserid'=>$USER->id, 'context'=>context_course::instance($id), 'courseid'=>$id);
    $event = \format_talentquest\event\format_talentquest_course_rated::create($event_properties);
    $event->trigger();
       
	echo time();
    
} if ($action == 'get_gs_activity' and $id > 0) {
    
    $userid		= optional_param('userid', 0, PARAM_INT);
    $course = $DB->get_record('course', array('id'=>$id));
	
	$rows = $DB->get_records_sql("SELECT cm.id, gg.rawgrademax, gg.rawgrademin, cm.course, gf.id AS itemid, (gg.finalgrade / gg.rawgrademax) *100 AS avarage, gg.finalgrade, cm.instance, m.name AS module, cmc.completionstate, cmc.timemodified AS completiondate
									FROM mdl_course_modules cm
										LEFT JOIN mdl_modules m ON m.id = cm.module
										LEFT JOIN mdl_user AS u ON u.id = $userid
										LEFT JOIN mdl_grade_items AS gf ON gf.iteminstance = cm.instance
										LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gf.id AND gg.userid = u.id
										LEFT JOIN mdl_course_modules_completion AS cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = u.id 
									WHERE cm.course = $id AND gf.courseid = cm.course AND gf.itemmodule = m.name AND cm.section > 0 AND cm.visible = 1 ORDER BY gf.timemodified DESC");
	
	$html = '';
	if($rows){
		$html .= '<table class="generaltable grade-summary-details" width="100">';
		$html .= '<thead>';
		$html .= '<th class="left">Activity</th>';
		$html .= '<th class="center">Type</th>';
		$html .= '<th class="center" style="text-align:center;">Grade</th>';
		$html .= '<th class="center"" style="text-align:center;">Status</th>';
		$html .= '<th class="right">Completed On</th>';
		$html .= '</thead>';
		$html .= '<tbody>';
		
		foreach($rows as $item){
			$ins = $DB->get_record_sql("SELECT ins.name FROM mdl_$item->module ins WHERE ins.id = $item->instance");
			
			$html .= '<tr>';
			$html .= '<td>'.$ins->name.'</td>';
			$html .= '<td>'.ucfirst($item->module).'</td>';
			$html .= '<td class="center info-avarage">'.(($item->avarage > 0) ? round($item->avarage, 0) : '-').'</td>';
			$html .= '<td class="center">
                        '.(($item->completionstate == 0) ? 'Incomplete' : '') .'
                        '.(($item->completionstate == 1) ? 'Completed' : '') .'
                        '.(($item->completionstate == 2) ? 'Passed' : '') .'
                        '.(($item->completionstate == 3) ? 'Failed' : '') .'
                    </td>';
			$html .= '<td class="right date">'. (($item->completionstate == 1 or $item->completionstate == 2) ? date('d M Y h:i', $item->completiondate) : '-') .'</td>';
			$html .= '</tr>';
		}
		
		$html .= '</tbody>';
		$html .= '</table>';
		
	}else{
		$html = 'Data not found';
	}
	die($html);
}

exit;
