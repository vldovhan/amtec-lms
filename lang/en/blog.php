<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for core subsystem 'blog'
 *
 * @package    core_blog
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addnewentry'] = 'Add Blog Post'; // SEBALE
$string['addnewexternalblog'] = 'Register an external blog';
$string['assocdescription'] = 'If you are writing about a course and/or activity modules, select them here.';
$string['associated'] = 'Associated {$a}';
$string['associatewithcourse'] = 'Blog about course {$a->coursename}';
$string['associatewithmodule'] = 'Blog about {$a->modtype}: {$a->modname}';
$string['association'] = 'Association';
$string['associations'] = 'Associations';
$string['associationunviewable'] = 'This post cannot be viewed by others until a course is associated with it or the \'Publish to\' field is changed';
$string['autotags'] = 'Add these tags';
$string['autotags_help'] = 'Enter one or more local tags (separated by commas) that you want to automatically add to each blog post copied from the external blog into your local blog.';
$string['backupblogshelp'] = 'If enabled then blogs will be included in SITE automated backups';
$string['blockexternalstitle'] = 'External blogs';
$string['blocktitle'] = 'Blog tags block title';
$string['blog'] = 'Blog';
$string['blogaboutthis'] = 'Create blog post to this {$a->type}';
$string['blogaboutthiscourse'] = 'Add an post to this course';
$string['blogaboutthismodule'] = 'Add an post to this {$a}';
$string['blogadministration'] = 'Blog administration';
$string['blogdeleteconfirm'] = 'Delete the blog post \'{$a}\'?';
$string['blogdisable'] = 'Blogging is disabled!';
$string['blogentries'] = 'Blog Posts';
$string['blogentriesabout'] = 'Blog Posts of {$a}';
$string['blogentriesbygroupaboutcourse'] = 'Blog posts of {$a->course} by {$a->group}';
$string['blogentriesbygroupaboutmodule'] = 'Blog posts of {$a->mod} by {$a->group}';
$string['blogentriesbyuseraboutcourse'] = 'Blog posts of {$a->course} by {$a->user}';
$string['blogentriesbyuseraboutmodule'] = 'Blog posts of this {$a->mod} by {$a->user}';
$string['blogentrybyuser'] = 'Blog post by {$a}';
$string['blogpreferences'] = 'Blog preferences';
$string['blogs'] = 'Blogs';
$string['blogscourse'] = 'Course blogs';
$string['blogssite'] = 'Site blogs';
$string['blogtags'] = 'Blog tags';
$string['cannotviewcourseblog'] = 'You do not have the required permissions to view blogs in this course';
$string['cannotviewcourseorgroupblog'] = 'You do not have the required permissions to view blogs in this course/group';
$string['cannotviewsiteblog'] = 'You do not have the required permissions to view all site blogs';
$string['cannotviewuserblog'] = 'You do not have the required permissions to read user blogs';
$string['configexternalblogcrontime'] = 'How often Moodle checks the external blogs for new posts.';
$string['configmaxexternalblogsperuser'] = 'The number of external blogs each user is allowed to link to their Moodle blog.';
$string['configuseblogassociations'] = 'Enables the association of blog posts with courses and course modules.';
$string['configuseexternalblogs'] = 'Enables users to specify external blog feeds. Moodle regularly checks these blog feeds and copies new posts to the local blog of that user.';
$string['courseblog'] = 'Course blog: {$a}';
$string['courseblogdisable'] = 'Course blogs are not enabled';
$string['courseblogs'] = 'Users can only see blogs for people who share a course';
$string['deleteblogassociations'] = 'Delete blog associations';
$string['deleteblogassociations_help'] = 'If ticked then blog posts will no longer be associated with this course or any course activities or resources.  The blog posts themselves will not be deleted.';
$string['deleteentry'] = 'Delete post';
$string['deleteexternalblog'] = 'Unregister this external blog';
$string['deleteotagswarn'] = 'Are you sure you want to remove these tags from all blog posts and remove it from the system?';
$string['description'] = 'Description';
$string['description_help'] = 'Enter a sentence or two summarising the contents of your external blog. (If no description is supplied, the description recorded in your external blog will be used).';
$string['donothaveblog'] = 'You do not have your own blog, sorry.';
$string['editentry'] = 'Edit Blog Post'; // SEBALE
$string['editexternalblog'] = 'Edit this external blog';
$string['emptybody'] = 'Post can not be empty';
$string['emptyrssfeed'] = 'The URL you entered does not point to a valid RSS feed';
$string['emptytitle'] = 'Blog Title can not be empty';
$string['emptyurl'] = 'You must specify a URL to a valid RSS feed';
$string['entrybody'] = 'Post';
$string['entrybodyonlydesc'] = 'Post';
$string['entryerrornotyours'] = 'This post is not yours';
$string['entrysaved'] = 'Your post has been saved';
$string['entrytitle'] = 'Title';
$string['eventblogentriesviewed'] = 'Blog posts viewed';
$string['eventblogassociationadded'] = 'Blog association created';
$string['evententryadded'] = 'Blog post added';
$string['evententrydeleted'] = 'Blog post deleted';
$string['evententryupdated'] = 'Blog post updated';
$string['externalblogcrontime'] = 'External blog cron schedule';
$string['externalblogdeleteconfirm'] = 'Unregister this external blog?';
$string['externalblogdeleted'] = 'External blog unregistered';
$string['externalblogs'] = 'External blogs';
$string['feedisinvalid'] = 'This feed is invalid';
$string['feedisvalid'] = 'This feed is valid';
$string['filterblogsby'] = 'Filter posts by...';
$string['filtertags'] = 'Filter tags';
$string['filtertags_help'] = 'You can use this feature to filter the posts you want to use. If you specify tags here (separated by commas) then only posts with these tags will be copied from the external blog.';
$string['groupblog'] = 'Group blog: {$a}';
$string['groupblogdisable'] = 'Group blog is not enabled';
$string['groupblogentries'] = 'Blog posts associated with {$a->coursename} by group {$a->groupname}';
$string['groupblogs'] = 'Users can only see blogs for people who share a group';
$string['incorrectblogfilter'] = 'Incorrect blog filter type specified';
$string['intro'] = 'This RSS feed was automatically generated from one or more blogs.';
$string['invalidgroupid'] = 'Invalid group ID';
$string['invalidurl'] = 'This URL is unreachable';
$string['linktooriginalentry'] = 'Link to original blog post';
$string['maxexternalblogsperuser'] = 'Maximum number of external blogs per user';
$string['myprofileuserblogs'] = 'View all blog posts';
$string['name'] = 'Name';
$string['name_help'] = 'Enter a descriptive name for your external blog. (If no name is supplied, the title of your external blog will be used).';
$string['noentriesyet'] = 'No visible posts here';
$string['noguestpost'] = 'Guest can not post blogs!';
$string['nopermissionstodeleteentry'] = 'You lack the permissions required to delete this blog post';
$string['norighttodeletetag'] = 'You have no rights to delete this tag - {$a}';
$string['nosuchentry'] = 'No such blog post';
$string['notallowedtoedit'] = 'You are not allowed to edit this post';
$string['numberofentries'] = 'Posts: {$a}';
$string['numberoftags'] = 'Number of tags to display';
$string['pagesize'] = 'Blog posts per page';
$string['permalink'] = 'Permalink';
$string['personalblogs'] = 'Users can only see their own blog';
$string['preferences'] = 'Blog preferences';
$string['publishto'] = 'Status';
$string['publishto_help'] = 'There are 3 options:

* Yourself (draft) - Only you and the administrators can see this post
* Anyone on this site - Anyone who is registered on this site can read this post
* Anyone in the world - Anyone, including guests, could read this post';
$string['publishtocourse'] = 'Users sharing a course with you';
$string['publishtocourseassoc'] = 'Members of the associated course';
$string['publishtocourseassocparam'] = 'Members of {$a}';
$string['publishtogroup'] = 'Users sharing a group with you';
$string['publishtogroupassoc'] = 'Your group members in the associated course';
$string['publishtogroupassocparam'] = 'Your group members in {$a}';
$string['publishtonoone'] = 'Draft';
$string['publishtosite'] = 'Publish';
$string['published'] = 'Published'; // SEBALE
$string['publishtoworld'] = 'Anyone in the world';
$string['readfirst'] = 'Read this first';
$string['relatedblogentries'] = 'Related blog entries';
$string['retrievedfrom'] = 'Retrieved from';
$string['rssfeed'] = 'Blog RSS feed';
$string['searchterm'] = 'Search: {$a}';
$string['settingsupdatederror'] = 'An error has occurred, blog preference setting could not be updated';
$string['siteblogheading'] = 'Site blog';
$string['siteblogdisable'] = 'Site blog is not enabled';
$string['siteblogs'] = 'All site users can see all blog entries';
$string['tagdatelastused'] = 'Date tag was last used';
$string['tagparam'] = 'Tag: {$a}';
$string['tags'] = 'Tags';
$string['tagsort'] = 'Sort the tag display by';
$string['tagtext'] = 'Tag text';
$string['timefetched'] = 'Time of last sync';
$string['timewithin'] = 'Display tags used within this many days';
$string['updateentrywithid'] = 'Updating post';
$string['url'] = 'RSS feed URL';
$string['url_help'] = 'Enter the RSS feed URL for your external blog.';
$string['useblogassociations'] = 'Enable blog associations';
$string['useexternalblogs'] = 'Enable external blogs';
$string['userblog'] = 'User blog: {$a}';
$string['userblogentries'] = 'Blog posts by {$a}';
$string['valid'] = 'Valid';
$string['viewallblogentries'] = 'All posts about this {$a}';
$string['viewallmodentries'] = 'View all posts about this {$a->type}';
$string['viewallmyentries'] = 'View all of my posts';
$string['viewentriesbyuseraboutcourse'] = 'View posts about this course by {$a}';
$string['viewblogentries'] = 'Posts about this {$a->type}';
$string['viewblogsfor'] = 'View all Posts for...';
$string['viewcourseblogs'] = 'View all Posts for this course';
$string['viewgroupblogs'] = 'View Posts for group...';
$string['viewgroupentries'] = 'Group Posts';
$string['viewmodblogs'] = 'View Posts for module...';
$string['viewmodentries'] = 'Module Posts';
$string['viewmyentries'] = 'My Posts';
$string['viewmyentriesaboutmodule'] = 'View my Posts about this {$a}';
$string['viewmyentriesaboutcourse'] = 'View my Posts about this course';
$string['viewsiteentries'] = 'View all Posts';
$string['viewuserentries'] = 'View all Posts by {$a}';
$string['worldblogs'] = 'The world can read Posts set to be world-accessible';
$string['wrongpostid'] = 'Wrong blog post id';
$string['page-blog-edit'] = 'Blog editing pages';
$string['page-blog-index'] = 'Blog listing pages';
$string['page-blog-x'] = 'All blog pages';

// Deprecated since Moodle 2.9.
$string['siteblog'] = 'Site blog: {$a}';

// SEBALE
$string['showmyblogs'] = 'Show My Blogs';
$string['showallblogs'] = 'Show All Blogs';
$string['myblogs'] = 'My Blogs';
$string['course_blog'] = 'Course Blog';
$string['createcoursepost'] = 'Create New Course Post';
$string['createpost'] = 'Create New Post';

