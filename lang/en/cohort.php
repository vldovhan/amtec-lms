<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'cohort', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package    core_cohort
 * @subpackage cohort
 * @copyright  2010 Petr Skoda (info@skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['addcohort'] = 'Add new User Group';
$string['allcohorts'] = 'All User Groups';
$string['anycohort'] = 'Any';
$string['assign'] = 'Assign';
$string['assignto'] = 'User Group \'{$a}\' members';
$string['backtocohorts'] = 'Back to User Groups';
$string['bulkadd'] = 'Add to User Group';
$string['bulknocohort'] = 'No available User Groups found';
$string['categorynotfound'] = 'Category <b>{$a}</b> not found or you don\'t have permission to create a User Group there. The default context will be used.';
$string['cohort'] = 'User Group';
$string['cohorts'] = 'User Groups';
$string['cohortsin'] = '{$a}: available User Groups';
$string['assigncohorts'] = 'Assign User Group members';
$string['component'] = 'Source';
$string['contextnotfound'] = 'Context <b>{$a}</b> not found or you don\'t have permission to create a User Group there. The default context will be used.';
$string['csvcontainserrors'] = 'Errors were found in CSV data. See details below.';
$string['csvcontainswarnings'] = 'Warnings were found in CSV data. See details below.';
$string['csvextracolumns'] = 'Column(s) <b>{$a}</b> will be ignored.';
$string['currentusers'] = 'Current users';
$string['currentusersmatching'] = 'Current users matching';
$string['defaultcontext'] = 'Default context';
$string['delcohort'] = 'Delete User Group';
$string['delconfirm'] = 'Do you really want to delete User Group \'{$a}\'?';
$string['description'] = 'Description';
$string['displayedrows'] = '{$a->displayed} rows displayed out of {$a->total}.';
$string['duplicateidnumber'] = 'User Group with the same ID number already exists';
$string['editcohort'] = 'Edit User Group';
$string['editcohortidnumber'] = 'Edit User Group ID';
$string['editcohortname'] = 'Edit User Group name';
$string['eventcohortcreated'] = 'User Group created';
$string['eventcohortdeleted'] = 'User Group deleted';
$string['eventcohortmemberadded'] = 'User added to a User Group';
$string['eventcohortmemberremoved'] = 'User removed from a User Group';
$string['eventcohortupdated'] = 'User Group updated';
$string['external'] = 'External User Group';
$string['idnumber'] = 'User Group ID';
$string['memberscount'] = 'User Group size';
$string['name'] = 'Name';
$string['namecolumnmissing'] = 'There is something wrong with the format of the CSV file. Please check that it includes column names.';
$string['namefieldempty'] = 'Field name can not be empty';
$string['newnamefor'] = 'New name for User Group {$a}';
$string['newidnumberfor'] = 'New ID number for User Group {$a}';
$string['nocomponent'] = 'Created manually';
$string['potusers'] = 'Potential users';
$string['potusersmatching'] = 'Potential matching users';
$string['preview'] = 'Preview';
$string['removeuserwarning'] = 'Removing users from a User Group may result in unenrolling of users from multiple courses which includes deleting of user settings, grades, group membership and other user information from affected courses.';
$string['selectfromcohort'] = 'Select members from User Group';
$string['systemcohorts'] = 'System User Groups';
$string['unknowncohort'] = 'Unknown User Group ({$a})!';
$string['uploadcohorts'] = 'Upload User Groups';
$string['uploadedcohorts'] = 'Uploaded {$a} User Groups';
$string['useradded'] = 'User added to User Group "{$a}"';
$string['search'] = 'Search';
$string['searchcohort'] = 'Search User Group';
$string['uploadcohorts_help'] = 'User Groups may be uploaded via text file. The format of the file should be as follows:

* Each line of the file contains one record
* Each record is a series of data separated by commas (or other delimiters)
* The first record contains a list of fieldnames defining the format of the rest of the file
* Required fieldname is name
* Optional fieldnames are idnumber, description, descriptionformat, visible, context, category, category_id, category_idnumber, category_path
';
$string['visible'] = 'Visible';
$string['visible_help'] = "Any User Group can be viewed by users who have 'moodle/cohort:view' capability in the User Group context.<br/>
Visible User Groups can also be viewed by users in the underlying courses.";
