<?php
require_once('../config.php');

$action = optional_param('action', 'send_request', PARAM_RAW);
$url = optional_param('url', '', PARAM_RAW);

if($action == 'send_request' and $url != ''){
	$curl = $CFG->wwwprotocol.$url.'/MasterCopy/clearcache.php?action=clear_cache';
	$ch = curl_init($curl);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_POSTFIELDS, 'action=clear_cache');
	$result = curl_exec($ch);
	curl_close($ch);
	if (!$result){
		echo json_encode(array('status'=>'0'));
	}
} elseif ($action == 'clear_cache') {
	theme_reset_all_caches();
	purge_all_caches();
	echo json_encode(array('status'=>'1'));
}

exit;