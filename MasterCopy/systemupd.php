<?php
require_once('../config.php');
require_once($CFG->dirroot.'/user/lib.php');
require_once($CFG->libdir.'/upgradelib.php');  // general upgrade/install related functions

$data = array();
foreach ($_POST as $key=>$value){
	$data[$key] = $value;
}
if (count($data) > 0){
	$data = (object)$data;
}

if ($CFG->domain != $CFG->maindomain){
	$admin = $DB->get_record_sql('SELECT * FROM {user} WHERE id = 2 LIMIT 1');
	if (isset($admin->id) and $admin->id > 0){
		$usernew = $admin;
		$usernew->username = (!empty($data->username)) ? $data->username : $admin->username;
		$usernew->password = hash_internal_user_password($data->password);
		$usernew->firstname = (!empty($data->firstname)) ? $data->firstname : $admin->firstname;
		$usernew->lastname = (!empty($data->lastname)) ? $data->lastname : $admin->lastname;
		$usernew->timezone = (!empty($data->zone)) ? $data->zone : $admin->timezone;

		user_update_user($usernew, false);
		
		$settings = $DB->get_records('system_settings');
		$set = array('systemId', 'name', 'url', 'url_type', 'enabled');
		if (count($settings) > 0){
			$asettings = array();
			foreach ($settings as $setting){
				$asettings[$setting->name] = $setting;
			}
		}
		foreach ($set as $item){
			$new_setting = new stdClass();
			$new_setting->name = $item;
			switch ($item){
				case 'systemId':
					$new_setting->value = $data->id;
					break;
				case 'name':
					$new_setting->value = $data->name;
					break;
				case 'url':
					$new_setting->value = $data->url;
					break;
				case 'url_type':
					$new_setting->value = ($data->other_domain > 0) ? 'domain' : 'subdomain';	
					break;	
                case 'enabled':
					$new_setting->value = ($data->state > 0) ? 1 : 0;	
					break;	
				default :
					$new_setting->value = '';
			}
			
			if (isset($asettings[$item])){
				$new_setting->id = $asettings[$item]->id;
				$DB->update_record('system_settings', $new_setting);
			} else {
				$DB->insert_record('system_settings', $new_setting);
			}
		}
        
		if (!empty($data->zone) and $timezone = $DB->get_record('config', array('name'=>'timezone'))){
			$timezone->value = $data->zone;
			$DB->update_record('config', $timezone);
		}
		if ($course_system = $DB->get_record('course', array('id'=>1))){
			$course_system->fullname = $data->name;
			$course_system->shortname = $data->name;
			$DB->update_record('course', $course_system);
		}
	}
	upgrade_noncore(true);
	purge_all_caches();
}
