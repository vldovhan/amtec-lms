<?php
require_once('../config.php');

$action = optional_param('action', 'send_request', PARAM_RAW);
$url = optional_param('url', '', PARAM_RAW);

if($action == 'send_request' and $url != ''){
	$curl = $CFG->wwwprotocol.$url.'/MasterCopy/cron_moodle.php?action=run_cron';
	$ch = curl_init($curl);
	curl_setopt($ch,CURLOPT_POST, 1);
	curl_setopt($ch,CURLOPT_POSTFIELDS, 'action=run_cron');
	$result = curl_exec($ch);
	curl_close($ch);
	if (!$result){
		echo json_encode(array('status'=>'0'));
	}
} elseif ($action == 'run_cron') {
	$upd = file_get_contents($CFG->wwwroot."/admin/cron.php");
	echo json_encode(array('status'=>'1'));
	
	/*$to      = 'rubensauf@gmail.com';
	$subject = 'Cron testing';
	$message = $CFG->wwwroot;
	$headers = 'From: rubensauf@gmail.com' . "\r\n" .
		'Reply-To: rubensauf@gmail.com' . "\r\n";

	mail($to, $subject, $message, $headers);*/
}
exit;