<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the manual badge award criteria type class
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Manual badge award criteria
 *
 */
class award_criteria_plans extends award_criteria {

    /* @var int Criteria [BADGE_CRITERIA_TYPE_PLANS] */
    public $criteriatype = BADGE_CRITERIA_TYPE_PLANS;

    public $required_param = 'plan';
    public $optional_params = array();

    /**
     * Gets role name.
     * If no such role exists this function returns null.
     *
     * @return string|null
     */
    private function get_plans() {
        global $DB, $PAGE;
        
        $now = time();
        $plans = array();
        $rec = $DB->get_records_sql("SELECT * FROM {local_plans} 
                                        WHERE visible > 0 
                                            AND type = 0 
                                            AND (
                                                (startdate > 0 AND enddate > 0 AND startdate >= $now AND enddate <= $now) OR 
                                                (startdate > 0 AND enddate = 0 AND startdate >= $now) OR 
                                                (startdate = 0 AND enddate > 0 AND enddate <= $now) OR 
                                                (startdate = 0 AND enddate = 0)
                                            )
                                        ORDER BY name");
        if (count($rec)) {
            foreach ($rec as $item){
                $plans[$item->id] = $item->name;
            }
        }
        return $plans;
    }

    /**
     * Add appropriate new criteria options to the form
     *
     */
    public function get_options(&$mform) {
        global $PAGE;
        $options = '';
        $none = true;

        $plans = $this->get_plans();
        $plansids = array_keys($this->params);
        $existing = array();
        $missing = array();

        if ($this->id !== 0) {
            $existing = array_keys($this->params);
            $missing = array_diff($existing, $plansids);
        }

        if (!empty($missing)) {
            $mform->addElement('header', 'category_errors', get_string('criterror', 'badges'));
            $mform->addHelpButton('category_errors', 'criterror', 'badges');
            foreach ($missing as $m) {
                $this->config_options($mform, array('id' => $m, 'checked' => true, 'name' => get_string('error:planmissing', 'badges'), 'error' => true));
                $none = false;
            }
        }
        
        if (!empty($plans)) {
            $mform->addElement('header', 'first_header', $this->get_title());
            $mform->addHelpButton('first_header', 'criteria_' . $this->criteriatype, 'badges');
            foreach ($plans as $planid=>$planname) {
                $checked = false;
                if (in_array($planid, $existing)) {
                    $checked = true;
                }
                $this->config_options($mform, array('id' => $planid, 'checked' => $checked, 'name' => $planname, 'error' => false));
                $none = false;
            }
        }

        // Add aggregation.
        if (!$none) {
            $mform->addElement('header', 'aggregation', get_string('method', 'badges'));
            $agg = array();
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('allmethodmanual', 'badges'), 1);
            $agg[] =& $mform->createElement('static', 'none_break', null, '<br/>');
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('anymethodmanual', 'badges'), 2);
            $mform->addGroup($agg, 'methodgr', '', array(' '), false);
            if ($this->id !== 0) {
                $mform->setDefault('agg', $this->method);
            } else {
                $mform->setDefault('agg', BADGE_CRITERIA_AGGREGATION_ANY);
            }
        }

        return array($none, get_string('noparamstoadd', 'badges'));
    }

    /**
     * Get criteria details for displaying to users
     *
     * @return string
     */
    public function get_details($short = '') {
        global $OUTPUT;
        $output = array();
        $plans = $this->get_plans();
        
        foreach ($this->params as $p) {
            $str = (isset($plans[$p['plan']])) ? $plans[$p['plan']] : false;
            if (!$str) {
                $output[] = $OUTPUT->error_text(get_string('error:nosuchplan', 'badges'));
            } else {
                $output[] = '<b>'.$str.'</b>';
            }
        }

        if ($short) {
            return implode(', ', $output);
        } else {
            return html_writer::alist($output, array(), 'ul');
        }
    }

    /**
     * Review this criteria and decide if it has been completed
     *
     * @param int $userid User whose criteria completion needs to be reviewed.
     * @param bool $filtered An additional parameter indicating that user list
     *        has been reduced and some expensive checks can be skipped.
     *
     * @return bool Whether criteria is complete
     */
    public function review($userid, $filtered = false) {
        global $DB;

        if (empty($this->params)) {
            return false;
        }

        $overall = false;
        foreach ($this->params as $param) {
            $crit = $DB->get_record('local_plans_completions', array('planid' => $param['plan'], 'userid' => $userid));
            if ($this->method == BADGE_CRITERIA_AGGREGATION_ALL) {
                if (!$crit) {
                    return false;
                } else {
                    $overall = true;
                    continue;
                }
            } else {
                if (!$crit) {
                    $overall = false;
                    continue;
                } else {
                    return true;
                }
            }
        }
        return $overall;
    }

    /**
     * Returns array with sql code and parameters returning all ids
     * of users who meet this particular criterion.
     *
     * @return array list($join, $where, $params)
     */
    public function get_completed_criteria_sql() {
        $join = '';
        $where = '';
        $params = array();

        if ($this->method == BADGE_CRITERIA_AGGREGATION_ANY) {
            foreach ($this->params as $param) {
                $plandata[] = " pc.planid = :completedplan{$param['plan']} ";
                $params["completedplan{$param['plan']}"] = $param['plan'];
            }
            if (!empty($plandata)) {
                $extraon = implode(' OR ', $plandata);
                $join = " JOIN {local_plans_completions} pc ON pc.userid = u.id
                            AND ({$extraon})";
            }
            return array($join, $where, $params);
        } else {
            foreach ($this->params as $param) {
                $join .= " LEFT JOIN {local_plans_completions} pc{$param['plan']} ON
                          pc{$param['plan']}.userid = u.id AND
                          pc{$param['plan']}.planid = :completedplan{$param['plan']} ";
                $where .= " AND pc{$param['plan']}.planid IS NOT NULL ";
                $params["completedplan{$param['plan']}"] = $param['plan'];
            }
            return array($join, $where, $params);
        }
    }
}
