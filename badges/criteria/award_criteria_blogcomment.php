<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the courseset completion badge award criteria type class
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

defined('MOODLE_INTERNAL') || die();
require_once('award_criteria_course.php');

/**
 * Badge award criteria -- award on courseset completion
 *
 */
class award_criteria_blogcomment extends award_criteria {

    private $courseid;
    /* @var int Criteria [BADGE_CRITERIA_TYPE_BLOGCOMMENT] */
    public $criteriatype = BADGE_CRITERIA_TYPE_BLOGCOMMENT;
    public $required_param = 'course';
    public $optional_params = array();
    
    public function __construct($record) {
        global $DB;
        parent::__construct($record);

        $course = $DB->get_record_sql('SELECT c.id, c.enablecompletion, c.cacherev, c.startdate
                        FROM {badge} b INNER JOIN {course} c ON b.courseid = c.id
                        WHERE b.id = :badgeid ', array('badgeid' => $this->badgeid));
        $this->courseid = (isset($course->id)) ? $course->id : 0;
    }
    
    /**
     * Get criteria details for displaying to users
     *
     * @return string
     */
    public function get_details($short = '') {
        global $DB, $OUTPUT;
        $output = array();
        foreach ($this->params as $p) {
            if (!isset($p['course'])) continue;
            $coursename = $DB->get_field('course', 'fullname', array('id' => $p['course']));
            if (!$coursename) {
                if ($p['course'] == -1) {
                    $str = html_writer::tag('b', '"' . get_string('site') . '"');
                } else {
                    $str = $OUTPUT->error_text(get_string('error:nosuchcourse', 'badges'));
                }
            } else {
                $str = html_writer::tag('b', '"' . $coursename . '"');
            }
            $output[] = $str;
        }

        if ($short) {
            return implode(', ', $output);
        } else {
            return html_writer::alist($output, array(), 'ul');
        }
    }

    public function get_courses(&$mform) {
        global $DB, $CFG;
        require_once($CFG->dirroot . '/course/lib.php');
        $buttonarray = array();

        // Get courses with enabled completion.
        if ($this->courseid > 0){
            $courses = array();
            $courses[] = $DB->get_record('course', array('id' => $this->courseid));
        } else {
            if (has_capability('theme/talentquest:viewallcourses', context_system::instance())){
                $courses = $DB->get_records('course', array('enablecompletion' => COMPLETION_ENABLED));    
            } else {
                $courses = enrol_get_my_courses('enablecompletion', 'fullname');
            }
        }
        if (!empty($courses)) {
            require_once($CFG->libdir . '/coursecatlib.php');
            $list = coursecat::make_categories_list();

            $select = array();
            $selected = array();
            foreach ($courses as $c) {
                $select[-1] = get_string('site');
                $select[$c->id] = $list[$c->category] . ' / ' . format_string($c->fullname, true, array('context' => context_course::instance($c->id)));
            }

            if ($this->id !== 0) {
                $selected = array_keys($this->params);
            }
            $settings = array('multiple' => 'multiple', 'size' => 20, 'style' => 'width:300px');
            $mform->addElement('select', 'courses', get_string('addcourse', 'badges'), $select, $settings);
            $mform->addRule('courses', get_string('requiredcourse', 'badges'), 'required');
            $mform->addHelpButton('courses', 'addcourse', 'badges');

            $buttonarray[] =& $mform->createElement('submit', 'submitcourse', get_string('addcourse', 'badges'));
            $buttonarray[] =& $mform->createElement('submit', 'cancel', get_string('cancel'));
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);

            $mform->addElement('hidden', 'addcourse', 'addcourse');
            $mform->setType('addcourse', PARAM_TEXT);
            if ($this->id !== 0) {
                $mform->setDefault('courses', $selected);
            }
            $mform->setType('agg', PARAM_INT);
        } else {
            $mform->addElement('static', 'nocourses', '', get_string('error:nocourses', 'badges'));
            $buttonarray[] =& $mform->createElement('submit', 'cancel', get_string('continue'));
            $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        }
    }

    public function add_courses($params = array()) {
        global $DB;
        $t = $DB->start_delegated_transaction();
        if ($this->id !== 0) {
            $critid = $this->id;
        } else {
            $fordb = new stdClass();
            $fordb->criteriatype = $this->criteriatype;
            $fordb->method = BADGE_CRITERIA_AGGREGATION_ALL;
            $fordb->badgeid = $this->badgeid;
            $critid = $DB->insert_record('badge_criteria', $fordb, true, true);
        }
        if ($critid) {
            foreach ($params as $p) {
                $newp = new stdClass();
                $newp->critid = $critid;
                $newp->name = 'course_' . $p;
                $newp->value = $p;
                if (!$DB->record_exists('badge_criteria_param', array('critid' => $critid, 'name' => $newp->name))) {
                    $DB->insert_record('badge_criteria_param', $newp, false, true);
                }
            }
        }
        $t->allow_commit();
        return $critid;
    }

    /**
     * Add appropriate new criteria options to the form
     *
     */
    public function get_options(&$mform) {
        global $DB, $OUTPUT;
        $none = true;

        $mform->addElement('header', 'first_header', $this->get_title());
        $mform->addHelpButton('first_header', 'criteria_' . $this->criteriatype, 'badges');
        
        if (has_capability('theme/talentquest:viewallcourses', context_system::instance())){
            $courses = $DB->get_records('course', array('enablecompletion' => COMPLETION_ENABLED));    
        } else {
            $courses = enrol_get_my_courses('enablecompletion', 'fullname');
        }

        if ($courses) {
            $mform->addElement('submit', 'addcourse', get_string('addcourse', 'badges'), array('class' => 'addcourse'));
        }
        
        $prefix = $this->required_param . '_';

        // In courseset, print out only the ones that were already selected.
        foreach ($this->params as $p) {
            if (!isset($p['course'])) continue;
            if ($course = $DB->get_record('course', array('id' => $p['course']))) {
                $coursecontext = context_course::instance($course->id);
                $param = array(
                        'id' => $course->id,
                        'checked' => true,
                        'name' => format_string($course->fullname, true, array('context' => $coursecontext)),
                        'error' => false
                );
                $none = false;
            } elseif ($p['course'] == '-1') {
                $param = array(
                        'id' => -1,
                        'checked' => true,
                        'name' => get_string('site'),
                        'error' => false
                );
                $none = false;
            } else {
                $params = array('id' => $p['course'], 'checked' => true,
                        'name' => get_string('error:nosuchcourse', 'badges'), 'error' => true);
            }
            
            $parameter = array();
            if ($param['error']) {
                $parameter[] =& $mform->createElement('advcheckbox', $prefix . $param['id'], '',
                        $OUTPUT->error_text($param['name']), null, array(0, $param['id']));
                $mform->addGroup($parameter, 'param_' . $prefix . $param['id'], '', array(' '), false);
            } else {
                $parameter[] =& $mform->createElement('advcheckbox', $prefix . $param['id'], '', $param['name'], null, array(0, $param['id']));
                $mform->addGroup($parameter, 'param_' . $prefix . $param['id'], '', array(' '), false);
            }

            // Set default values.
            $mform->setDefault($prefix . $param['id'], $param['checked']);
        }

        // Add aggregation.
        if (!$none) {
            $mform->addElement('header', 'aggregation', get_string('method', 'badges'));
            $agg = array();
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('allmethodcourseset', 'badges'), 1);
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('anymethodcourseset', 'badges'), 2);
            $mform->addGroup($agg, 'methodgr', '', array('<br/>'), false);
            if ($this->id !== 0) {
                $mform->setDefault('agg', $this->method);
            } else {
                $mform->setDefault('agg', BADGE_CRITERIA_AGGREGATION_ANY);
            }
        }

        return array($none, get_string('noparamstoadd', 'badges'));
    }
    
    /**
     * Review this criteria and decide if it has been completed
     *
     * @param int $userid User whose criteria completion needs to be reviewed.
     * @param bool $filtered An additional parameter indicating that user list
     *        has been reduced and some expensive checks can be skipped.
     *
     * @return bool Whether criteria is complete
     */
    public function review($userid, $filtered = false) {
        global $DB;
        foreach ($this->params as $param) {
            $course =  new stdClass();
            $course->id = ($param['course'] > 0) ? $param['course'] : 0;

            $post = $DB->get_record_sql("SELECT COUNT(c.id) as comment FROM {comments} c LEFT JOIN {post} p ON p.id = c.itemid WHERE p.module = 'blog' AND p.courseid = $course->id AND p.publishstate = 'site' AND c.component = 'blog' AND c.userid = $userid");
            
            $overall = false;
            if ($this->method == BADGE_CRITERIA_AGGREGATION_ALL) {
                if (isset($post->comment) and $post->comment > 0) {
                    $overall = true;
                    continue;
                } else {
                    return false;
                }
            } else {
                if (isset($post->comment) and $post->comment > 0) {
                    return true;
                } else {
                    $overall = false;
                    continue;
                }
            }
        }

        return $overall;
    }

    /**
     * Returns array with sql code and parameters returning all ids
     * of users who meet this particular criterion.
     *
     * @return array list($join, $where, $params)
     */
    public function get_completed_criteria_sql() {
        $join = '';
        $where = '';
        $params = array();

        if ($this->method == BADGE_CRITERIA_AGGREGATION_ANY) {
            foreach ($this->params as $param) {
                if (!isset($param['course']) or (isset($param['course']) and $param['course'] == '')) continue;
                $param['course'] = ($param['course'] > 0) ? $param['course'] : 0;
                $coursedata[] = " b.courseid = :blogcourseid{$param['course']} ";
                $params["blogcourseid{$param['course']}"] = $param['course'];
            }
            if (!empty($coursedata)) {
                $extraon = implode(' OR ', $coursedata);
                $join = " JOIN {comments} c ON c.userid = u.id AND c.component = 'blog'
                          LEFT JOIN {post} b ON b.id = c.itemid AND b.module = 'blog' AND b.publishstate = 'site' AND ({$extraon})";
            }
            return array($join, $where, $params);
        } else {
            foreach ($this->params as $param) {
                if (!isset($param['course']) or (isset($param['course']) and $param['course'] == '')) continue;
                $param['course'] = ($param['course'] > 0) ? $param['course'] : 0;
                $join .= " LEFT JOIN {comments} c{$param['course']} ON
                          c{$param['course']}.userid = u.id AND c{$param['course']}.component = 'blog'";
                $join .= " LEFT JOIN {post} b{$param['course']} ON b{$param['course']}.id = c{$param['course']}.itemid AND
                          b{$param['course']}.courseid = :blogcourseid{$param['course']}";
                $where .= " b{$param['course']}.module = 'blog' AND b{$param['course']}.publishstate = 'site' ";
                $params["blogcourseid{$param['course']}"] = $param['course'];
            }
            return array($join, $where, $params);
        }
    }
}
