<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the manual badge award criteria type class
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Manual badge award criteria
 *
 */
class award_criteria_gcontest extends award_criteria {
    private $courseid = 0;
    
    /* @var int Criteria [BADGE_CRITERIA_TYPE_GCONTEST] */
    public $criteriatype = BADGE_CRITERIA_TYPE_GCONTEST;

    public $required_param = 'contest';
    public $optional_params = array('place');
    
    public function __construct($record) {
        global $DB;
        parent::__construct($record);

        $course = $DB->get_record_sql('SELECT c.id
                        FROM {badge} b INNER JOIN {course} c ON b.courseid = c.id
                        WHERE b.id = :badgeid ', array('badgeid' => $this->badgeid));
        $this->courseid = (isset($course->id)) ? $course->id : 0;
    }

    /**
     * Gets contests.
     * If no such role exists this function returns null.
     *
     * @return string|null
     */
    private function get_contests() {
        global $DB, $PAGE, $USER;
        
        $now = time();
        $contests = array();
        if ($this->courseid > 0){
            $where = "(gc.goal > 1 AND (gc.criteria = 'any' OR (gc.criteria = 'manual' AND gcc.courseid = $this->courseid)))";
        } else {
            $where = "(gc.goal = 1 OR (gc.goal > 1 AND (gc.criteria = 'any' OR (gc.criteria = 'manual' AND gcc.courseid IN (
                                                        SELECT c.id FROM {user_enrolments} ue
                                                            LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                                            LEFT JOIN {course} c ON c.id = e.courseid
                                                                WHERE ue.userid = $USER->id
                                                    ))) OR gc.userid = $USER->id))";
        }
        
        $rec = $DB->get_records_sql("SELECT gc.* 
                                            FROM {local_gm_contests} gc
                                        LEFT JOIN {local_gm_courses} gcc ON gcc.instanceid = gc.id
                                            WHERE  ((gc.startdate = 0 AND gc.enddate = 0) OR 
                                                    (gc.startdate >= ".time()." AND gc.enddate = 0) OR 
                                                    (gc.enddate <= ".time()." AND gc.startdate = 0) OR 
                                                    (gc.startdate >=".time()." AND gc.enddate <= ".time().")) AND
                                                    gc.status > 0 AND 
                                                    $where
                                                GROUP BY gc.id
                                                ORDER BY gc.name ASC");
        if (count($rec)) {
            foreach ($rec as $item){
                $contests[$item->id] = $item->name;
            }
        }
        return $contests;
    }

    /**
     * Add appropriate new criteria options to the form
     *
     */
    public function get_options(&$mform) {
        global $PAGE;
        $options = '';
        $none = true;

        $contests = $this->get_contests();
        $contestsids = array_keys($this->params);
        $existing = array();
        $missing = array();

        if ($this->id !== 0) {
            $existing = array_keys($this->params);
            $missing = array_diff($existing, $contestsids);
        }

        if (!empty($missing)) {
            $mform->addElement('header', 'category_errors', get_string('criterror', 'badges'));
            $mform->addHelpButton('category_errors', 'criterror', 'badges');
            foreach ($missing as $m) {
                $this->config_options($mform, array('id' => $m, 'checked' => true, 'name' => get_string('error:contestmissing', 'badges'), 'error' => true));
                $none = false;
            }
        }
        
        if (!empty($contests)) {
            $mform->addElement('header', 'first_header', $this->get_title());
            foreach ($contests as $contestid=>$contestname) {
                $checked = false;
                if (in_array($contestid, $existing)) {
                    $checked = true;
                }
                
                $param = array('id' => $contestid,
                        'checked' => $checked,
                        'name' => $contestname,
                        'error' => false
                        );

                if ($this->id !== 0 && isset($this->params[$contestid]['place'])) {
                    $param['place'] = $this->params[$contestid]['place'];
                }

                $this->config_options($mform, $param);
                $none = false;
            }
        }

        // Add aggregation.
        if (!$none) {
            $mform->addElement('header', 'aggregation', get_string('method', 'badges'));
            $agg = array();
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('allmethodmanual', 'badges'), 1);
            $agg[] =& $mform->createElement('static', 'none_break', null, '<br/>');
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('anymethodmanual', 'badges'), 2);
            $mform->addGroup($agg, 'methodgr', '', array(' '), false);
            if ($this->id !== 0) {
                $mform->setDefault('agg', $this->method);
            } else {
                $mform->setDefault('agg', BADGE_CRITERIA_AGGREGATION_ANY);
            }
        }

        return array($none, get_string('noparamstoadd', 'badges'));
    }

    /**
     * Get criteria details for displaying to users
     *
     * @return string
     */
    public function get_details($short = '') {
        global $OUTPUT;
        $output = array();
        $contests = $this->get_contests();
        
        foreach ($this->params as $p) {
            $str = (isset($contests[$p['contest']])) ? $contests[$p['contest']] : false;
            if (!$str) {
                $output[] = $OUTPUT->error_text(get_string('error:nosuchcontest', 'badges'));
            } else {
                $str = '<b>'.$str.'</b>';
                if (isset($p['place'])) {
                    $str .= get_string('criteria_descr_place', 'badges', $p['place']);
                }
                $output[] = $str;
            }
        }

        if ($short) {
            return implode(', ', $output);
        } else {
            return html_writer::alist($output, array(), 'ul');
        }
    }

    /**
     * Review this criteria and decide if it has been completed
     *
     * @param int $userid User whose criteria completion needs to be reviewed.
     * @param bool $filtered An additional parameter indicating that user list
     *        has been reduced and some expensive checks can be skipped.
     *
     * @return bool Whether criteria is complete
     */
    public function review($userid, $filtered = false) {
        global $DB;

        if (empty($this->params)) {
            return false;
        }

        $overall = false;
        foreach ($this->params as $param) {
            $crit = $DB->get_record('local_gm_results', array('contestid' => $param['contest'], 'userid' => $userid, 'place'=>$param['place'], 'status'=>0));
            if ($this->method == BADGE_CRITERIA_AGGREGATION_ALL) {
                if (!$crit) {
                    return false;
                } else {
                    $overall = true;
                    continue;
                }
            } else {
                if (!$crit) {
                    $overall = false;
                    continue;
                } else {
                    return true;
                }
            }
        }
        return $overall;
    }

    /**
     * Returns array with sql code and parameters returning all ids
     * of users who meet this particular criterion.
     *
     * @return array list($join, $where, $params)
     */
    public function get_completed_criteria_sql() {
        $join = '';
        $where = '';
        $params = array();

        if ($this->method == BADGE_CRITERIA_AGGREGATION_ANY) {
            foreach ($this->params as $param) {
                $plandata[] = " (gr.contestid = :submittedcontest{$param['contest']} AND gr.place = :contestplace{$param['contest']} AND gr.status = 0 )";
                $params["submittedcontest{$param['contest']}"] = $param['contest'];
                $params["contestplace{$param['contest']}"] = $param['place'];
            }
            if (!empty($plandata)) {
                $extraon = implode(' OR ', $plandata);
                $join = " JOIN {local_gm_results} gr ON gr.userid = u.id
                            AND ({$extraon})";
            }
            return array($join, $where, $params);
        } else {
            foreach ($this->params as $param) {
                $join .= " LEFT JOIN {local_gm_results} gr{$param['contest']} ON
                          gr{$param['contest']}.userid = u.id AND
                          gr{$param['contest']}.contestid = :submittedcontest{$param['contest']} 
                          AND gr{$param['contest']}.place = :contestplace{$param['contest']} 
                          AND gr{$param['contest']}.status = 0 ";
                $where .= " AND gr{$param['contest']}.contestid IS NOT NULL ";
                $params["submittedcontest{$param['contest']}"] = $param['contest'];
                $params["contestplace{$param['contest']}"] = $param['place'];
            }
            return array($join, $where, $params);
        }
    }
}
