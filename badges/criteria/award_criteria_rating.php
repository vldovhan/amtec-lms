<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the activity badge award criteria type class
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/completionlib.php');

/**
 * Badge award criteria -- award on activity completion
 *
 */
class award_criteria_rating extends award_criteria {

    /* @var int Criteria [BADGE_CRITERIA_TYPE_RATING] */
    public $criteriatype = BADGE_CRITERIA_TYPE_RATING;

    private $courseid;
    private $course;

    public $required_param = 'course';
    public $optional_params = array('rate', 'bydate');

    public function __construct($record) {
        global $DB;
        parent::__construct($record);

        $this->course = $DB->get_record_sql('SELECT c.id, c.enablecompletion, c.cacherev, c.startdate
                        FROM {badge} b INNER JOIN {course} c ON b.courseid = c.id
                        WHERE b.id = :badgeid ', array('badgeid' => $this->badgeid));
        $this->courseid = $this->course->id;
    }

    /**
     * Get criteria description for displaying to users
     *
     * @return string
     */
    public function get_details($short = '') {
        global $DB, $OUTPUT;
        $param = reset($this->params);

        $course = $DB->get_record('course', array('id' => $param['course']));
        if (!$course) {
            $str = $OUTPUT->error_text(get_string('error:nosuchcourse', 'badges'));
        } else {
            $options = array('context' => context_course::instance($course->id));
            $str = html_writer::tag('b', '"' . format_string($course->fullname, true, $options) . '"');
            if (isset($param['bydate'])) {
                $str .= get_string('criteria_descr_bydate', 'badges', userdate($param['bydate'], get_string('strftimedate', 'core_langconfig')));
            }
            if (isset($param['rate'])) {
                $str .= get_string('criteria_descr_rate', 'badges', $param['rate']);
            }
        }
        return $str;
    }

    /**
     * Add appropriate new criteria options to the form
     *
     */
    public function get_options(&$mform) {
        global $DB;
        $param = array();

        if ($this->id !== 0) {
            $param = reset($this->params);
        } else {
            $param['course'] = $mform->getElementValue('course');
            $mform->removeElement('course');
        }
        
        $course = $DB->get_record('course', array('id' => $this->courseid));
        
        $mform->addElement('header', 'criteria_course', $this->get_title());
        $mform->addHelpButton('criteria_course', 'criteria_' . $this->criteriatype, 'badges');
        $parameter = array();
        $parameter[] =& $mform->createElement('static', 'mrate_', null, get_string('minrate', 'badges'));
        $parameter[] =& $mform->createElement('text', 'rate_' . $param['course'], '', array('size' => '5'));
        $parameter[] =& $mform->createElement('static', 'complby_' . $param['course'], null, get_string('ratebydate', 'badges'));
        $parameter[] =& $mform->createElement('date_selector', 'bydate_' . $param['course'], '', array('optional' => true));
        $mform->setType('rate_' . $param['course'], PARAM_INT);
        $mform->addGroup($parameter, 'param_' . $param['course'], '', array(' '), false);

        $mform->disabledIf('bydate_' . $param['course'] . '[day]', 'bydate_' . $param['course'] . '[enabled]', 'notchecked');
        $mform->disabledIf('bydate_' . $param['course'] . '[month]', 'bydate_' . $param['course'] . '[enabled]', 'notchecked');
        $mform->disabledIf('bydate_' . $param['course'] . '[year]', 'bydate_' . $param['course'] . '[enabled]', 'notchecked');

        // Set existing values.
        if (isset($param['bydate'])) {
            $mform->setDefault('bydate_' . $param['course'], $param['bydate']);
        }

        if (isset($param['rate'])) {
            $mform->setDefault('rate_' . $param['course'], $param['rate']);
        }

        // Add hidden elements.
        $mform->addElement('hidden', 'course_' . $course->id, $course->id);
        $mform->setType('course_' . $course->id, PARAM_INT);
        $mform->addElement('hidden', 'agg', BADGE_CRITERIA_AGGREGATION_ALL);
        $mform->setType('agg', PARAM_INT);

        $none = false;
        $message = '';
        
        return array($none, $message);
    }

    /**
     * Review this criteria and decide if it has been completed
     *
     * @param int $userid User whose criteria completion needs to be reviewed.
     * @param bool $filtered An additional parameter indicating that user list
     *        has been reduced and some expensive checks can be skipped.
     *
     * @return bool Whether criteria is complete
     */
    public function review($userid, $filtered = false) {
        global $DB;
        $course = $this->course;

        if ($this->course->startdate > time()) {
            return false;
        }

        foreach ($this->params as $param) {
            $check_rate = true;
            $check_date = true;
            
            $rate = $DB->get_record('course_rating', array('courseid'=>$course->id, 'userid'=>$userid));
            if (!$rate){
                $check_rate = false;
                $check_date = false;
            } else {
                if (isset($param['rate'])){
                    $check_grade = ($rate->value >= $param['rate']);
                }
                
                if (!$filtered && isset($param['bydate'])) {
                    $date = $rate->timemodified;
                    $check_date = ($date <= $param['bydate']);
                }
            }

            if ($check_grade && $check_date) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns array with sql code and parameters returning all ids
     * of users who meet this particular criterion.
     *
     * @return array list($join, $where, $params)
     */
    public function get_completed_criteria_sql() {
        // We have only one criterion here, so taking the first one.
        $coursecriteria = reset($this->params);

        $join = " LEFT JOIN {course_rating} cr ON cr.userid = u.id";
        $where = ' AND cr.courseid = :courseid ';
        $params['courseid'] = $this->courseid;

        // Add rate parameter.
        if (isset($param['rate'])) {
            $where .= ' AND cr.value >= :rate';
            $params['rate'] = $coursecriteria['rate'];
        }
        
        // Add by date parameter.
        if (isset($param['bydate'])) {
            $where .= ' AND cr.timemodified <= :ratetime';
            $params['ratetime'] = $coursecriteria['bydate'];
        }

        return array($join, $where, $params);
    }
}
