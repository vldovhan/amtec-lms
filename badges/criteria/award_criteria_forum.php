<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the activity badge award criteria type class
 *
 * @package    core
 * @subpackage badges
 * @copyright  2012 onwards Totara Learning Solutions Ltd {@link http://www.totaralms.com/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @author     Yuliya Bozhko <yuliya.bozhko@totaralms.com>
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->libdir . '/completionlib.php');

/**
 * Badge award criteria -- award on activity completion
 *
 */
class award_criteria_forum extends award_criteria {

    /* @var int Criteria [BADGE_CRITERIA_TYPE_FORUM] */
    public $criteriatype = BADGE_CRITERIA_TYPE_FORUM;

    private $courseid;
    private $course;

    public $required_param = 'module';
    public $optional_params = array();
    
    public function __construct($record) {
        global $DB;
        parent::__construct($record);

        $this->course = $DB->get_record_sql('SELECT c.id, c.format, c.cacherev, c.startdate
                        FROM {badge} b INNER JOIN {course} c ON b.courseid = c.id
                        WHERE b.id = :badgeid ', array('badgeid' => $this->badgeid));
        $this->courseid = $this->course->id;
    }

    /**
     * Gets the module instance from the database and returns it.
     * If no module instance exists this function returns false.
     *
     * @return stdClass|bool
     */
    private function get_mod_instance($cmid) {
        global $DB;
        
        return get_coursemodule_from_id('forum', $cmid);
    }

    /**
     * Get criteria description for displaying to users
     *
     * @return string
     */
    public function get_details($short = '') {
        global $DB, $OUTPUT;
        $output = array();
        foreach ($this->params as $p) {
            if (empty($p['module'])) continue;
            $mod = self::get_mod_instance($p['module']);
            if (!$mod) {
                $str = $OUTPUT->error_text(get_string('error:nosuchmod', 'badges'));
            } else {
                $str = html_writer::tag('b', '"' . get_string('modulename', $mod->modname) . ' - ' . $mod->name . '"');
                if (isset($p['bydate'])) {
                    $str .= get_string('criteria_descr_bydate', 'badges', userdate($p['bydate'], get_string('strftimedate', 'core_langconfig')));
                }
            }
            $output[] = $str;
        }

        if ($short) {
            return implode(', ', $output);
        } else {
            return html_writer::alist($output, array(), 'ul');
        }
    }

    /**
     * Add appropriate new criteria options to the form
     *
     */
    public function get_options(&$mform) {
        $none = true;
        $existing = array();
        $missing = array();

        $course = $this->course;
        $mods = $this->get_forums($course);
        
        $mids = array();
        if (count($mods)){
            foreach ($mods as $mod) {
                $mids[] = $mod->id;
            }    
        }
        
        if ($this->id !== 0) {
            $existing = array_keys($this->params);
            if (count($existing)){
                foreach ($existing as $k=>$v){
                    if (empty($v)) unset($existing[$k]);
                }
            }
            $missing = array_diff($existing, $mids);
        }

        if (!empty($missing)) {
            $mform->addElement('header', 'category_errors', get_string('criterror', 'badges'));
            $mform->addHelpButton('category_errors', 'criterror', 'badges');
            foreach ($missing as $m) {
                $this->config_options($mform, array('id' => $m, 'checked' => true,
                        'name' => get_string('error:nosuchmod', 'badges'), 'error' => true));
                $none = false;
            }
        }

        if (!empty($mods)) {
            $mform->addElement('header', 'first_header', $this->get_title());
            foreach ($mods as $mod) {
                $checked = false;
                if (in_array($mod->id, $existing)) {
                    $checked = true;
                }
                $param = array('id' => $mod->id,
                        'checked' => $checked,
                        'name' => get_string('modulename', $mod->modname) . ' - ' . $mod->name,
                        'error' => false
                        );

                $this->config_options($mform, $param);
                $none = false;
            }
        }

        // Add aggregation.
        if (!$none) {
            $mform->addElement('header', 'aggregation', get_string('method', 'badges'));
            $agg = array();
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('allmethodactivity', 'badges'), 1);
            $agg[] =& $mform->createElement('radio', 'agg', '', get_string('anymethodactivity', 'badges'), 2);
            $mform->addGroup($agg, 'methodgr', '', array('<br/>'), false);
            if ($this->id !== 0) {
                $mform->setDefault('agg', $this->method);
            } else {
                $mform->setDefault('agg', BADGE_CRITERIA_AGGREGATION_ANY);
            }
        }

        return array($none, get_string('error:noactivities', 'badges'));
    }
    
    public function get_forums($course){
        global $DB, $CFG;
        
        require_once($CFG->dirroot.'/course/lib.php');
        $modinfo = get_fast_modinfo($course);
        $mods = array();
        
        $format_oprions = $DB->get_record('course_format_options', array('courseid'=>$course->id, 'format'=>$course->format, 'name'=>'numsections'));
        
        foreach ($modinfo->get_section_info_all() as $section => $thissection) {
            if ($section == 0) continue;
            if ($section > $format_oprions->value) break;
            if (!empty($modinfo->sections[$thissection->section])) {
                foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                    if ($modinfo->cms[$modnumber]->modname != 'forum') continue;
                    $mods[] = $modinfo->cms[$modnumber];
                }
            }
        }
        
        return $mods;
    }

    /**
     * Review this criteria and decide if it has been completed
     *
     * @param int $userid User whose criteria completion needs to be reviewed.
     * @param bool $filtered An additional parameter indicating that user list
     *        has been reduced and some expensive checks can be skipped.
     *
     * @return bool Whether criteria is complete
     */
    public function review($userid, $filtered = false) {
        global $DB;
        
        if ($this->course->startdate > time()) {
            return false;
        }

        
        $overall = false;
        foreach ($this->params as $param) {
            $cm = new stdClass();
            $cm->id = $param['module'];
            if (!$cm->id) continue;
            
            $cm = $DB->get_record('course_modules', array('id'=>$param['module']));
            $forum = $DB->get_record('forum', array('id'=>$cm->instance));
            
            $discussion = $DB->get_records('forum_discussions', array('course'=>$this->course->id, 'forum'=>$forum->id, 'userid'=>$userid));
            
            if ($this->method == BADGE_CRITERIA_AGGREGATION_ALL) {
                if (count($discussion)) {
                    $overall = true;
                    continue;
                } else {
                    return false;
                }
            } else {
                if ($discussion) {
                    return true;
                } else {
                    $overall = false;
                    continue;
                }
            }
        }

        return $overall;
    }

    /**
     * Returns array with sql code and parameters returning all ids
     * of users who meet this particular criterion.
     *
     * @return array list($join, $where, $params)
     */
    public function get_completed_criteria_sql() {
        $join = '';
        $where = '';
        $params = array();

        return array($join, $where, $params);
    }
}
