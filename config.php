<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

//error_reporting(E_ALL);ini_set('display_errors', '1');$CFG->debug = 32767;  $CFG->debugdisplay = true;

$CFG->disableonclickaddoninstall = true;
$CFG->updateautocheck = false;

$CFG->maindomain= 'amtec';
$CFG->siteurl	= 'amtec.sebale.net';
$CFG->domain    = parse_domain();
$CFG->dbtype    = 'mysqli';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = parse_domain(1);
$CFG->dbuser    = 'root';
$CFG->dbpass    = 'H3b8d@fbIcnj3hks9@fds';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->wwwprotocol = 'http://';
$CFG->wwwroot   = $CFG->wwwprotocol.$_SERVER['SERVER_NAME'];
$CFG->dataroot  = '/home/amtec/public_html/moodledatas/'.parse_domain(1);
$CFG->admin     = 'admin';


$CFG->directorypermissions = 0777;

check_domain_exists();

require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!

function parse_domain($clean = 0){
	global $CFG;
	$mdomain = $_SERVER['SERVER_NAME'];
	$mdomain = str_replace('www.', '', $mdomain);
	if ($mdomain == $CFG->siteurl){
		$result = $CFG->maindomain;
	} else if (strripos($mdomain , $CFG->siteurl)){
		$bits = explode('.', $mdomain);
		if (count($bits) > 0){
			$result = $bits[0];
		}
	} else {
		if ($clean > 0){
			$result = str_replace('.', '_', $mdomain);
		} else {
			$result = $mdomain;
		}
	}
	return $result;
}

function check_domain_exists(){
	global $CFG;
	if (!is_dir($CFG->dataroot)){
		header('Location: '.$CFG->wwwprotocol.$CFG->siteurl); exit;
	}
}
