<?php

class CustomfieldsModel extends Model
{
    public function getCustomFields($params = array())
    {
        $conditions = $this->conditions($params);
        $sql = "SELECT * FROM pfx_custom_fields".(($conditions != '') ? " WHERE $conditions" : "");
        
        return $this->select($sql, $params);
    }
    
    public function getCustomField($params)
    {
      $conditions = $this->conditions($params);
      $sql = "SELECT * FROM pfx_custom_fields WHERE $conditions";
      return $this->one($sql, $params);
    }
    public function processCustomFields($type, $data = array(), $instanceid = 0)
    {
      if (count($data) and $instanceid and $type != ''){
          $cfields = $this->getCustomFields($type);
          $fields = array();
          if (count($cfields)){
              foreach($cfields as $field){
                 $fields[$field->name] = $field->id;
              }
          }
          foreach($data as $key=>$val){
              if (isset($fields[$key])){
                  $sql = "SELECT * FROM pfx_custom_data WHERE instanceid = :instanceid AND fieldid = :fieldid";
                  $new_field = $this->one($sql, array('instanceid'=>$instanceid, 'fieldid'=>$fields[$key]));
                  if (isset($new_field->id)){
                      $new_field->value = $val;
                      $new_field->timemodified = time();
                      $this->update("custom_data", $new_field, array('id'=>$new_field->id));
                  } else {
                      $new_field = new stdClass();
                      $new_field->instanceid = $instanceid;
                      $new_field->fieldid = $fields[$key];
                      $new_field->type = $type;
                      $new_field->value = $val;
                      $new_field->timemodified = time();
                      $this->insert("custom_data", $new_field);
                  }
              }
          }
      }
    }
    public function getFieldsData($type, $instanceid = 0)
    {
        $data = array();
        
        $sql = "SELECT * FROM pfx_custom_data WHERE instanceid = :instanceid AND type = :type";
        $fields_data = $this->select($sql, array('instanceid'=>$instanceid, 'type'=>$type));
        
        if (count($fields_data)){
            foreach ($fields_data as $field){
                $data[$field->fieldid] = $field->value;
            }
        }
        
        return $data;
    }
    public function deleteFieldsData($type, $instanceid = 0)
    {
        $sql = "SELECT * FROM pfx_custom_data WHERE instanceid = :instanceid AND type = :type";
        $fields_data = $this->select($sql, array('instanceid'=>$instanceid, 'type'=>$type));
        
        if (count($fields_data)){
            foreach ($fields_data as $field){
                $this->delete('custom_data', array("id"=>$field->id));
            }
        }
    }
    public function deleteCustomData($fieldid = 0)
    {
        $sql = "SELECT * FROM pfx_custom_data WHERE fieldid = :fieldid";
        $fields_data = $this->select($sql, array('fieldid'=>$fieldid));
        
        if (count($fields_data)){
            foreach ($fields_data as $field){
                $this->delete('custom_data', array("id"=>$field->id));
            }
        }
    }
    public function updateCustomField($data, $params)
    {
      return $this->update("custom_fields", $data, $params);
    }
    public function insertCustomField($data)
    {
      return $this->insert("custom_fields", $data);
    }
    public function deleteCustomField($id)
    {
      $this->delete('custom_fields', array("id"=>$id));
    }
}
