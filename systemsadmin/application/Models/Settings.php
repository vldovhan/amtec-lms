<?php

class SettingsModel extends Model
{
    public function getSettings()
    {
        $sql = "SELECT * FROM pfx_settings ORDER BY title";
        return $this->select($sql);
    }
    public function getSetting($params)
    {
      $conditions = $this->conditions($params);
      $sql = "SELECT * FROM pfx_settings WHERE $conditions";
      return $this->one($sql, $params);
    }
    public function updateSetting($data, $params)
    {
      return $this->update("settings", $data, $params);
    }
    public function insertSetting($data)
    {
      return $this->insert("settings", $data);
    }
    public function deleteSetting($id)
    {
      $this->delete('settings', array("id"=>$id));
    }
}
