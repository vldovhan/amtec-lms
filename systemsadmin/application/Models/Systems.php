<?php

class SystemsModel extends Model
{
    public function getSystems($id)
    {
        $sql = "SELECT * FROM pfx_users WHERE id = :id";
        $user = $this->one($sql, array('id'=>$id));
        
        if ($user->role == 'admin'){
            $sql = "SELECT s.*, CONCAT(s.firstname, ' ', s.lastname) as username 
                        FROM pfx_systems s
                            LEFT JOIN pfx_users u ON u.id = s.userid";
            return $this->select($sql);
        } else {
            $sql = "SELECT s.*, CONCAT(s.firstname, ' ', s.lastname) as username 
                        FROM pfx_systems s
                            LEFT JOIN pfx_users u ON u.id = s.userid
                        WHERE userid = :id";
            return $this->select($sql, array('id'=>$id));
        }
    }
    public function getSystem($params)
    {
      $conditions = $this->conditions($params);
      $sql = "SELECT * FROM pfx_systems WHERE $conditions";
      return $this->one($sql, $params);
    }
    public function getSystemsAttr($params)
    {
        $conditions = $this->conditions($params);
        $sql = "SELECT * FROM pfx_systems WHERE $conditions";
        return $this->select($sql, $params);
    }
    public function updateSystem($data, $params)
    {
      return $this->update("systems", $data, $params);
    }
    public function insertSystem($data)
    {
      return $this->insert("systems", $data);
    }
    public function deleteSystem($id)
    {
      $this->delete('systems', array("id"=>$id));
    }
    public function canDelete($system)
    {
        
      $domain = $this->prepareDomain($system->url);
      $this->reConnect($domain);
        
      $users = $this->one("SELECT COUNT(u.id) as ucount FROM mdl_user u WHERE u.deleted = 0 AND u.id > 2");
      $courses = $this->one("SELECT COUNT(c.id) as ccount FROM mdl_course c WHERE c.id > 1");
        
      $this->reConnect();
      
      return array('courses'=>(($courses->ccount) ? $courses->ccount : 0), 'users'=>(($users->ucount) ? $users->ucount : 0), 'candelete'=>(($courses->ccount > 0 or $users->ucount > 0) ? false : true));
    }
}
