<?php

class CountriesModel extends Model
{
    public function getCountries($id)
    {
        $sql = "SELECT * FROM pfx_countries ORDER BY nicename";
        return $this->select($sql);
    }
    public function getCountry($params)
    {
      $conditions = $this->conditions($params);
      $sql = "SELECT * FROM pfx_countries WHERE $conditions";
      return $this->one($sql, $params);
    }
    public function getCountriesList()
    {
        $countries = array();
        $sql = "SELECT * FROM pfx_countries ORDER BY nicename";
        $all_countries = $this->select($sql);
        
        foreach ($all_countries as $country){
            $countries[$country->iso] = $country->nicename;
        }
        
        return $countries;
    }
}
