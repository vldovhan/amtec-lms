<?php

class CustomData extends Model
{
    public function getCustomData($id)
    {
        $sql = "SELECT * FROM pfx_users WHERE id = :id";
        $user = $this->one($sql, array('id'=>$id));
        
        if ($user->role == 'admin'){
            $sql = "SELECT s.*, b.timecreated as backuptime 
                        FROM pfx_systems s 
                            LEFT JOIN (SELECT systemid, timecreated FROM pfx_backups ORDER BY timecreated DESC) b ON b.systemid = s.id
                        GROUP BY s.id ORDER BY s.name ASC";
            return $this->select($sql);
        } else {
            $sql = "SELECT s.*, b.timecreated as backuptime 
                        FROM pfx_systems s 
                            LEFT JOIN (SELECT systemid, timecreated FROM pfx_backups ORDER BY timecreated DESC) b ON b.systemid = s.id
                            LEFT JOIN pfx_users u ON u.id = s.userid
                        WHERE WHERE s.userid = :id
                        GROUP BY s.id ORDER BY s.name ASC";
            return $this->select($sql, array('id'=>$id));
        }
    }
    public function getCustomData($params)
    {
      $conditions = $this->conditions($params);
      $sql = "SELECT * FROM pfx_custom_data WHERE $conditions";
      return $this->one($sql, $params);
    }
    public function updateBackup($data, $params)
    {
      return $this->update("backups", $data, $params);
    }
    public function insertBackup($data)
    {
      return $this->insert("backups", $data);
    }
    public function deleteBackup($id)
    {
      $this->delete('backups', array("id"=>$id));
    }
}
