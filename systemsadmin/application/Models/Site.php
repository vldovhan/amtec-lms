<?php

class SiteModel extends Model
{
  public function getUsersNotify($id)
  {
      $sql = "SELECT u.* FROM pfx_users u";
      return $this->select($sql, array('id'=>$id));
  }
  public function getUserById($id)
  {
      $sql = "SELECT u.* FROM pfx_users u WHERE u.id = :id";
      return $this->one($sql, array('id'=>$id));
  }
  public function search($id, $keyword)
  {
      $sql = "SELECT a.* FROM ((SELECT id, name, email as contact, '0' as type FROM pfx_users WHERE owner = :owner AND plan IN (SELECT groupid FROM pfx_usergroups_partners WHERE userid = :owner))
              UNION
              (SELECT id, name, url as contact, '1' as type FROM pfx_servers WHERE userid = :owner)) AS a
              WHERE a.name LIKE :keyword ORDER BY a.name";
      return $this->select($sql, array('owner'=>$id,'keyword'=>"%$keyword%"));
  }
  public function getUsersByDate($id)
  {
      $ext = 86400;
      $timestart = strtotime('-7 days');
      $timefinish = time();
      $sql = "SELECT floor(timecreated / $ext) * $ext as timecreated, COUNT(id) as users
    		FROM pfx_users
    			WHERE id = :id AND timecreated BETWEEN $timestart AND $timefinish 
    				GROUP BY floor(timecreated / $ext) * $ext
    					ORDER BY timecreated ASC";
      return $this->select($sql, array('id'=>$id));
  }
    
  public function getStatsSystems($user)
  {
    $where = " WHERE id > 0";
    if ($user->role != 'admin'){
        $where .= " AND userid = ".$user->id;
    }
      
    $stats = array();
    $systems_all = $this->one("SELECT COUNT(id) as acount FROM pfx_systems $where");
    $stats['systems_all'] = ($systems_all->acount) ? $systems_all->acount : 0;
    
    $systems_month = $this->one("SELECT COUNT(id) as acount FROM pfx_systems $where AND timecreated BETWEEN ".mktime(0, 0, 0, date("n"), 1, date("Y"))." AND ".mktime(23, 59, 59, date("n"), date("t"), date("Y")));
    $stats['systems_month'] = ($systems_month->acount) ? $systems_month->acount : 0;
    
    $systems_today = $this->one("SELECT COUNT(id) as acount FROM pfx_systems $where AND timecreated BETWEEN ".mktime(0, 0, 0, date("n"), date("j"), date("Y"))." AND ".mktime(23, 59, 59, date("n"), date("j"), date("Y")));
    $stats['systems_today'] = ($systems_today->acount) ? $systems_today->acount : 0;
    
    Session::set('statsSystems', $stats);
      
    return $stats;
  }
    
  public function getStatsUsers($user)
  {
    $filter = Session::get('dashboardFilter');
    $where = " WHERE id > 0 ";
    if ($user->role != 'admin'){
        $where .= " AND userid = ".$user->id;
    }
    if ($filter > 0){
        $where .= " AND id = ".$filter;
    }
      
    $stats = array();
    $systems = $this->select("SELECT * FROM pfx_systems $where ORDER BY id ASC");
    $count_users_all = 0; $count_users_month = 0; $count_users_today = 0;
      
    if (count($systems) > 0){
        foreach($systems as $system){
            $domain = $this->prepareDomain($system->url);
            if (!$this->systemExists($domain)) continue;
            $this->reConnect($domain);
            
            $users_all = $this->one("SELECT COUNT(u.id) as ucount FROM mdl_user u WHERE u.deleted = 0 AND u.id > 1");
            $count_users_all += (isset($users_all->ucount)) ? $users_all->ucount : 0;

            $users_month = $this->one("SELECT COUNT(u.id) as ucount FROM mdl_user u WHERE u.deleted = 0 AND u.timecreated BETWEEN ".mktime(0, 0, 0, date("n"), 1, date("Y"))." AND ".mktime(23, 59, 59, date("n"), date("t"), date("Y"))." AND u.deleted = 0 AND u.id > 1");
            $count_users_month += (isset($users_month->ucount)) ? $users_month->ucount : 0;

            $users_today = $this->one("SELECT COUNT(u.id) as ucount FROM mdl_user u WHERE u.deleted = 0 AND u.timecreated BETWEEN ".mktime(0, 0, 0, date("n"), date("j"), date("Y"))." AND ".mktime(23, 59, 59, date("n"), date("j"), date("Y"))." AND u.deleted = 0 AND u.id > 1");
            $count_users_today += (isset($users_today->ucount)) ? $users_today->ucount : 0;
        }
        $this->reConnect();
    }
    
    $stats['users_all'] = $count_users_all;
    $stats['users_month'] = $count_users_month;
    $stats['users_today'] = $count_users_today;
    Session::set('statsUsers', $stats);
      
    return $stats;
  }
    
  public function getStatsCourses($user)
  {
    $filter = Session::get('dashboardFilter');
    $where = " WHERE id > 0 ";
    if ($user->role != 'admin'){
        $where .= " AND userid = ".$user->id;
    }  
    if ($filter > 0){
        $where .= " AND id = ".$filter;
    }
      
    $systems = $this->select("SELECT * FROM pfx_systems $where ORDER BY id ASC");
    $stats = array();
    $count_courses_all = 0; $count_courses_month = 0; $count_courses_today = 0;
      
    if (count($systems) > 0){
        foreach($systems as $system){
            $domain = $this->prepareDomain($system->url);
            if (!$this->systemExists($domain)) continue;
            $this->reConnect($domain);
            
            $courses_all = $this->one("SELECT COUNT(c.id) as ccount FROM mdl_course c WHERE c.id > 1");
            $count_courses_all += (isset($courses_all->ccount)) ? $courses_all->ccount : 0;
            
            $courses_month = $this->one("SELECT COUNT(c.id) as ccount FROM mdl_course c WHERE c.id > 1 AND c.timecreated BETWEEN ".mktime(0, 0, 0, date("n"), 1, date("Y"))." AND ".mktime(23, 59, 59, date("n"), date("t"), date("Y")));
            $count_courses_month += (isset($courses_month->ccount)) ? $courses_month->ccount : 0;

            $courses_today = $this->one("SELECT COUNT(c.id) as ccount FROM mdl_course c WHERE c.id > 1 AND c.timecreated BETWEEN ".mktime(0, 0, 0, date("n"), date("j"), date("Y"))." AND ".mktime(23, 59, 59, date("n"), date("j"), date("Y")));
            $count_courses_today += (isset($courses_today->ccount)) ? $courses_today->ccount : 0;
        }
        $this->reConnect();
    }
      
    $stats['courses_all'] = $count_courses_all;
    $stats['courses_month'] = $count_courses_month;
    $stats['courses_today'] = $count_courses_today;
    Session::set('statsCourses', $stats);
      
    return $stats;
  }
    
  public function getStatsSpace($user)
  {    
    $stats = array();
    $disk_used_space = 0;
      
    $disk_free_space = disk_free_space(MOODLEDATA_DIR);
    $disk_total_space = disk_total_space(MOODLEDATA_DIR);
    $disk_used_space = $disk_total_space-$disk_free_space;
    $disk_percentage = round(($disk_used_space / $disk_total_space) * 100);
      
    $stats['space_free'] = $this->formatBytes($disk_free_space, 2, 'num');
    $stats['space_all'] = $this->formatBytes($disk_total_space, 2, 'num');
    $stats['space_usage'] = $this->formatBytes($disk_used_space, 2, 'num');
    $stats['disk_percentage'] = $disk_percentage;
    $stats['full_space_val'] = $this->formatBytes($disk_total_space, 2, 'text');
    $stats['usage_space_val'] = $this->formatBytes($disk_used_space, 2, 'text');
    $stats['free_space_val'] = $this->formatBytes($disk_free_space, 2, 'text');
    Session::set('statsSpac', $stats);
      
    return $stats;
  }
    
  function formatBytes($bytes, $precision = 2, $type = 'full') { 
        $units = array('B', 'KB', 'MB', 'GB', 'TB'); 

        $bytes = max($bytes, 0); 
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024)); 
        $pow = min($pow, count($units) - 1); 

        // Uncomment one of the following alternatives
        $bytes /= pow(1024, $pow);
        // $bytes /= (1 << (10 * $pow)); 

        if ($type == 'text'){
            return $units[$pow];     
        } elseif ($type = 'num') {
            return round($bytes, $precision); 
        } else {
            return round($bytes, $precision) . ' ' . $units[$pow]; 
        }
        
    } 
}
