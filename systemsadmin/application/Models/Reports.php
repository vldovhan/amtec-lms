<?php

class ReportsModel extends Model
{
	const SYSTEM_ADMIN = 'admin';
	const TENANT_ADMIN = 'tenant';

	public static function getRoles()
	{
		return [
			self::SYSTEM_ADMIN,
			self::TENANT_ADMIN
		];
	}

	public function getBillingReport($systems, $userid)
	{
        $data = array();
        
        if (count($systems)){
            foreach ($systems as $system){
                $datasystem = new stdClass();
                $datasystem->id = $system->id;
                $datasystem->tenantname = $system->name;
                $datasystem->contact = $system->firstname.' '.$system->lastname;
                
                $domain = $this->prepareDomain($system->url);
                $this->reConnect($domain);

                $users_all = $this->one("SELECT COUNT(u.id) as ucount FROM mdl_user u WHERE u.deleted = 0 AND u.id > 1");
                $datasystem->users_all = (isset($users_all->ucount)) ? $users_all->ucount : 0;
                
                $users_active = $this->one("SELECT COUNT(u.id) as ucount FROM mdl_user u WHERE u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 AND u.id > 1");
                $datasystem->users_active = (isset($users_active->ucount)) ? $users_active->ucount : 0;


				$nowtime = time();
				$period =  $nowtime - (60 * 60 * 24 * 30);
				
				$unique_logins = $this->one("SELECT COUNT(DISTINCT it.userid) as logincount FROM mdl_local_intelliboard_tracking it
												LEFT JOIN mdl_user u ON it.userid = u.id
												WHERE u.deleted = 0
												AND u.suspended = 0
												AND u.confirmed = 1
												AND it.userid > 1
												AND it.lastaccess > $period
				 								AND it.lastaccess > $system->timecreated
												");

				$datasystem->unique_logins = (isset($unique_logins->logincount)) ? $unique_logins->logincount : 0;

                $data[$system->id] = $datasystem;
            }    
            $this->reConnect();
        }
        
		return $data;
	}
    
    public function getBillingDetails($system, $params)
	{
        $output = array();
        
        if (isset($system->id)){
            
            $aColumns = array( 'idnumber', 'name', 'email', 'timespend', 'visits');
            $bColumns = array('idnumber'=>'u.idnumber', 'name'=>array ('u.firstname', 'u.lastname'), 'email'=>'u.email', 'timespend'=>'lit.timespend', 'visits'=>'lit.visits');

            /* Indexed column (used for fast and accurate table cardinality) */
            $sIndexColumn = "idnumber";
            
            
            /* 
             * Paging
             */
            $sLimit = "";
            if ( isset( $params['iDisplayStart'] ) && $params['iDisplayLength'] != '-1' ){
                $sLimit = "LIMIT ".intval( $params['iDisplayStart'] ).", ".$params['iDisplayLength'];
            }

            /*
             * Ordering
             */
            $sOrder = "";
            if ( isset( $_GET['iSortCol_0'] ) ){
                $sOrder = "ORDER BY  ";
                for ( $i=0 ; $i<intval( $params['iSortingCols'] ) ; $i++ ){
                    if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                        $sOrder .= ((is_array($bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]])) ? $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]][0] : $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]]).
                            ($_GET['sSortDir_'.$i]==='asc' ? ' asc' : ' desc') .", ";
                    }
                }
                $sOrder = substr_replace( $sOrder, "", -2 );
                if ( $sOrder == "ORDER BY" ){
                    $sOrder = "";
                }
            }
            
            /* 
             * Filtering
             */
            $sWhere = " WHERE u.id > 1 AND u.deleted = 0 AND u.confirmed > 0 ";
            if ($_GET['sSearch_0'] == '' or $_GET['sSearch_0'] == 1){
                $sWhere .= " AND u.suspended = 0 ";
            } elseif ($_GET['sSearch_0'] != '-1'){
                $sWhere .= " AND u.suspended = 1 ";
            }
            
             /* 
             * Search
             */
            if ( $params['sSearch'] != "" ){
                $sWhere .= "AND (";
                for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                    if ($aColumns[$i] == 'actions' or $aColumns[$i] == 'active') continue;
                    if (is_array($bColumns[$aColumns[$i]])){
                        foreach ($bColumns[$aColumns[$i]] as $col){
                            $sWhere .= $col." LIKE '%".$params['sSearch']."%' OR ";
                        }
                    } else {
                        $sWhere .= $bColumns[$aColumns[$i]]." LIKE '%".$params['sSearch']."%' OR ";
                    }
                }
                $sWhere = substr_replace( $sWhere, "", -3 );
                $sWhere .= ')';
            }

            $domain = $this->prepareDomain($system->url);
            $this->reConnect($domain);
            
            $sql = "SELECT u.id, u.idnumber, u.username, u.email, CONCAT(u.firstname, ' ', u.lastname) as name, u.managerid, lit.timespend, lit.visits
                    FROM mdl_user u
                        LEFT JOIN (SELECT userid, sum(timespend) as timespend, sum(visits) as visits FROM mdl_local_intelliboard_tracking GROUP BY userid) lit ON lit.userid = u.id
                    $sWhere";

            $data = $this->select($sql.' '.$sOrder.' '.$sLimit);
            
            $sql_count = "SELECT COUNT(u.id) as datacount
                    FROM mdl_user u
                        LEFT JOIN (SELECT userid, sum(timespend) as timespend, sum(visits) as visits FROM mdl_local_intelliboard_tracking GROUP BY userid) lit ON lit.userid = u.id
                    $sWhere";
            $data_count = $this->one($sql_count);
            
            $iFilteredTotal = $data_count->datacount;
            $iTotal = $data_count->datacount;
            
            /*
             * Output
             */
            $output = array(
                "sEcho" => intval($params['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
            );
            
            foreach ($data as $aRow){
                $aRow = (array)$aRow;
                $row = array();
                for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                    if ($aColumns[$i] == 'timespend'){
                        $row[] = $this->timedate($aRow[ $aColumns[$i] ]);
                    } elseif ($aColumns[$i] == 'visits'){
                        $row[] = (int)$aRow[ $aColumns[$i] ];
                    } else {
                        $row[] = $aRow[ $aColumns[$i] ];
                    }
                }
                $row['DT_RowId'] = "row_".$aRow['id'];
                $output['aaData'][] = $row;
            }
            
            $this->reConnect();
        }
        
		return $output;
	}

	public function accessRulses() {
		return [
			self::SYSTEM_ADMIN => [
				'allow' => '*'
			],
			self::TENANT_ADMIN => [
				'allow' => [
					'users',
					'home',
					'systems',
					'login',
					'profile',
					'error'
				]
			]
		];
	}

	/**
	 * Return true if current user has access to controller ($name)
	 *
	 * @param $name string Controller name in lower case to check user access
	 * @return bool
	 */
	public function hasAccess($id, $access)
	{
		$user = $this->getUser(['id' => $id]);
		$access_rules = $this->accessRulses();
		$role = $user->role ? $user->role : self::TENANT_ADMIN;

		if (isset($access_rules[$role])) {
			$allow = $access_rules[$role]['allow'];
			if ($allow == '*') {
				return true;
			} else if (is_array($allow)) {
				foreach ($allow as $controller) {
					if ($controller == $access) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public function isSystemAdmin($role) {

		if ($role == self::SYSTEM_ADMIN) {
			return true;
		}

		return false;
	}
    
    function timedate($t,$f=':'){
        if($t < 0){
            return "00:00:00";
        }
        if(floor($t/3600) > 999){
            return floor($t/3600) . ' h';
        }
        return sprintf("%02d%s%02d%s%02d", floor($t/3600), $f, ($t/60)%60, $f, $t%60);
    }


}
