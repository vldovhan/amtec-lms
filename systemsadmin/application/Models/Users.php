<?php

class UsersModel extends Model
{
	const SYSTEM_ADMIN = 'admin';
	const TENANT_ADMIN = 'tenant';

	public static function getRoles()
	{
		return [
			self::SYSTEM_ADMIN,
			self::TENANT_ADMIN
		];
	}

	public function getUsers($id)
	{
		$sql = "SELECT u.* FROM pfx_users u ORDER BY u.firstname, u.lastname";

		return $this->select($sql);
	}

	/*public function getUsersServer($params)
	{
		$sql = "SELECT u.id,u.name,u.email, s.id as server
				  FROM pfx_users u
				  LEFT JOIN pfx_servers s ON s.userid = u.id AND s.parentid = :id
				  WHERE u.owner = :owner AND u.plan IN (SELECT groupid FROM pfx_usergroups_partners WHERE userid = :owner)";

		return $this->select($sql, $params);
	}*/
	public function getUser($params)
	{
		$conditions = $this->conditions($params);
		$sql = "SELECT * FROM pfx_users WHERE $conditions";
		return $this->one($sql, $params);
	}

	/*public function getUserServers($params)
	{
		$sql = "SELECT s.id, s.name, s.url, sq.id as assigned
				  FROM intelli_servers s
				  LEFT JOIN intelli_users u ON u.id = :id
				  LEFT JOIN intelli_servers sq ON sq.parentid = s.id and sq.userid = u.id
				WHERE s.userid = :userid";

		return $this->select($sql, $params);
	}*/
	public function findUser($data)
	{
		$params = array();
		$params['email'] = ($data['email']) ? $data['email'] : '';
		$params['password'] = ($data['password']) ? md5($data['password']) : '';
		$sql = "SELECT u.* FROM pfx_users u WHERE u.email = :email AND u.password = :password";
		return $this->one($sql, $params);
	}

	public function updateUser($data, $params)
	{
		return $this->update("users", $data, $params);
	}

	public function insertUser($data)
	{
		return $this->insert("users", $data);
	}

	/*public function assignUser($data)
	{
	  return $this->insert("user_usergroup_map", $data);
	}
	public function updateUserGroup($data, $params)
	{
	  return $this->update("user_usergroup_map", $data, $params);
	}*/
	public function deleteUser($id)
	{
		$this->delete('users', array("id" => $id));
	}


	public function accessRulses() {
		return [
			self::SYSTEM_ADMIN => [
				'allow' => '*'
			],
			self::TENANT_ADMIN => [
				'allow' => [
					'users',
					'home',
					'systems',
					'login',
					'profile',
					'error'
				]
			]
		];
	}

	/**
	 * Return true if current user has access to controller ($name)
	 *
	 * @param $name string Controller name in lower case to check user access
	 * @return bool
	 */
	public function hasAccess($id, $access)
	{
		$user = $this->getUser(['id' => $id]);
		$access_rules = $this->accessRulses();
		$role = $user->role ? $user->role : self::TENANT_ADMIN;

		if (isset($access_rules[$role])) {
			$allow = $access_rules[$role]['allow'];
			if ($allow == '*') {
				return true;
			} else if (is_array($allow)) {
				foreach ($allow as $controller) {
					if ($controller == $access) {
						return true;
					}
				}
			}
		}

		return false;
	}

	public function isSystemAdmin($role) {

		if ($role == self::SYSTEM_ADMIN) {
			return true;
		}

		return false;
	}


}
