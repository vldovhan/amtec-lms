<?php

class File
{
    
    public static function listDirectory($dir){
		$result = array();
        $root = scandir($dir);

        foreach($root as $value) {
            if($value === '.' || $value === '..') {
                continue;
            }
            if(is_file("$dir$value")) {
                $result[] = "$dir$value";
                continue;
            }
            if(is_dir("$dir$value")) {
                $result[] = "$dir$value/";
            }
            foreach(File::listDirectory("$dir$value/") as $value) {
                $result[] = $value;
            }
        }
		return $result;
	}
	
    public static function deleteDir($path) {
		// Open the source directory to read in files
        $i = new DirectoryIterator($path);
        foreach($i as $f) {
            if($f->isFile()) {
                unlink($f->getRealPath());
            } else if(!$f->isDot() && $f->isDir()) {
                File::deleteDir($f->getRealPath());
            }
        }
        rmdir($path);
	}
    
    public static function deleteFile($path) {
        if (is_file($path)){
          unlink($path);
        }
	}
	
	public static function Copy($src, $dest){
		if(!is_dir($src)) return false;
	 
		// If the destination directory does not exist create it
		if(!is_dir($dest)) { 
			if(!mkdir($dest)) {
				// If the destination directory could not be created stop processing
				return false;
			}    
		}
	 
		// Open the source directory to read in files
		$i = new DirectoryIterator($src);
		foreach($i as $f) {
			if($f->isFile()) {
				copy($f->getRealPath(), "$dest/" . $f->getFilename());
			} else if(!$f->isDot() && $f->isDir()) {
				File::Copy($f->getRealPath(), "$dest/$f");
			}
		}
		return true;
	}
    
    public static function fileDownload($filename) {
		if (file_exists($filename)) {
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename='.basename($filename));
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($filename));
			readfile($filename);
		} else {
			echo 'File not found';
		}
	}

}
