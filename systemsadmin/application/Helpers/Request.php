<?php

class Request
{
    /**
     * Safer and better access to $_POST.
     *
     * @param  string   $key
     * @static static method
     *
     * @return mixed
     */
    public static function clean($value = '', $type = '')
   	{
   		switch($type) {
   			case 'int': return (int) $value; break;
   			case 'string': return (string) $value; break;
   			case 'bool': return (bool) $value; break;
   			case 'array': return (array) $value; break;
   			case 'object': return (object) $value; break;
   			case 'alnum': return preg_replace("/[^0-9a-zA-Z ]/", "", $value); break;
   			case 'commalist': return preg_replace("/[^0-9,]/", "", $value); break;
   			default: return $value;
   		}
   	}
    public static function post($key, $output = '')
    {
        return self::clean((array_key_exists($key, $_POST)? $_POST[$key]: null), $output);
    }
    /**
     * Safer and better access to $_FILES.
     *
     * @param  string   $key
     * @static static method
     *
     * @return mixed
     */
    public static function files($key)
    {
        return array_key_exists($key, $_FILES)? $_FILES[$key]: null;
    }
    /**
     * Safer and better access to $_GET.
     *
     * @param  string   $key
     * @static static method
     *
     * @return mixed
     */
    public static function query($key)
    {
        return array_key_exists($key, $_GET)? $_GET[$key]: null;
    }
    /**
     * Detect if request is Ajax.
     *
     * @static static method
     *
     * @return boolean
     */
    public static function isAjax()
    {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])) {
            return strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';
        }
        return false;
    }
    /**
     * Detect if request is POST request.
     *
     * @static static method
     *
     * @return boolean
     */
    public static function isPost()
    {
        return $_SERVER["REQUEST_METHOD"] === "POST";
    }
    /**
     * Detect if request is GET request.
     *
     * @static static method
     *
     * @return boolean
     */
    public static function isGet()
    {
        return $_SERVER["REQUEST_METHOD"] === "GET";
    }
}
