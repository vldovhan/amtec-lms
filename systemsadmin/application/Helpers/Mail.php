<?php

class Mail
{

	public static function sendResetMail($user)
	{
		$mail = new PHPMailer();
        $mail->isHTML(true);
		$mail->setFrom('noreply@talentquest.com', 'TalentQuest');
		$mail->addAddress($user->email, $user->firstname . ' ' . $user->lastname);
		$mail->Subject = 'Password reset';

		$mail->Body    = 'Here is your password: '. $user->passwordClear ."\n". 'You can change it in your profile.';

		if($mail->send()) {
			return true;
		}
		else {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		return false;
	}
    
    public static function sendConfirmationMail($system, $status, $step)
	{
		$mail = new PHPMailer();
		$mail->isHTML(true);
		$mail->setFrom('noreply@talentquest.com', 'TalentQuest');
		$mail->addAddress($system->email, $system->firstname . ' ' . $system->lastname);
		$mail->Subject = 'TalentQuest Confirmation Email';

        if ($status == 'success'){
            $mail->Body    = '<p>System '. $system->name .' was successfully created.</p><p>You can login to LMS <a href="'. URL_PROTOCOL.$system->url.MAINDOMAIN.'">link</a></p><br />TalentQuest';
        } else {
            $mail->Body    = '<p>On creating system '. $system->name .' we get error on step '.$step.'.</p><br />TalentQuest';
        }
        
		if($mail->send()) {
			return true;
		}
		else {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		}

		return false;
	}

    public static function sendTestMail($num)
	{
		$mail = new PHPMailer();
		$mail->isHTML(true);
		$mail->setFrom('noreply@talentquest.com', 'TalentQuest');
		$mail->addAddress('volodymyr.dovhan@sebale.net', 'Vlad Dovhan');
		$mail->Subject = 'TalentQuest Testing Email ('.$num.')';

        $mail->Body    = '<p>Testing sending emails</p><br />TalentQuest';
        
		if($mail->send()) {
			return true;
		}
		else {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		}
		return false;
	}
}
