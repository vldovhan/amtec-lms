<?php

class Core
{
	public static function days($start, $end)
	{
		$datediff = $end - $start;
		$days = floor($datediff / (60 * 60 * 24));
		return "$days day" . (($days > 1) ? 's' : '');
	}

	public static function db($class, $method, $params = null)
	{
		if ($class and $method) {
			require_once(APP . 'Models/' . ucfirst($class) . '.php');
			$instance = ucfirst($class) . 'Model';
			$model = new $instance();
			return $model->{$method}($params);
		} else {
			return array();
		}
	}

	public static function user($key = '')
	{
		$user = Session::get('user');
		if ($key) {
			if (isset($user->{$key})) {
				return $user->{$key};
			} else {
				return null;
			}
		} else {
			return $user;
		}

	}

	public static function p($data = array(), $exit = true)
	{
		print('<hr><pre>');
		print_r($data);
		print('</pre><hr>');
		if ($exit) {
			exit;
		}
	}

	public static function extract($data, $key)
	{
		if (is_object($data)) {
			return (isset($data->{$key})) ? $data->{$key} : null;
		} elseif (is_array($data)) {
			return (isset($data[$key])) ? $data[$key] : null;
		} else {
			return null;
		}
	}

	public static function validatePassword($password, $words = [])
	{
		if (!preg_match('/^(?=.*\d)(?=.*[@#\-_$%^&+=§!\?])(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#\-_$%^&+=§!\?]{8,20}$/', $password)) {
			return false;
		}

		$predictable_words = ['password', 'qwerty'];
		$predictable_words = array_merge($words, $predictable_words);

		foreach ($predictable_words as $word) {
			if (strpos(strtolower($password), strtolower($word)) !== false) {
				return false;
			}
		}

		return true;
	}

	public static function validateUrl($url)
	{
		if (filter_var($url, FILTER_VALIDATE_URL) === false) {
			return false;
		}

		$url = str_replace('www.', '', $url);
		$url = str_replace('http://', '', $url);
		$url = str_replace('https://', '', $url);

		$patterns = ['@', '/', '*', '_', '$', ':', ';', '?', '!', '|', '<', '>', ','];

		foreach ($patterns as $pattern) {
			if (strpos($url, $pattern) !== false) {
				return false;
			}
		}

		return true;
	}

	public static function invalidPasswordMessage()
	{
		return 'Invalid password</br>
		Minimum number of 8 characters</br>
     • Password cannot contain predictable or easily guessed words or phrases (e.g. first name, last name, "password")</br>
     • Should contain at least 1 lowercase letter</br>
     • Should contain at least 1 uppercase letter</br>
     • Should contain at least 1 special character</br>
     • Should contain at least 1 number';
	}
}
