<div class="card">
  <div class="header hidden">
  <legend><?php echo (empty($user)) ? 'User information' : $user->firstname.' '.$user->firstname; ?></legend>
  </div>
  <div class="content">
      <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'users/save', 'class'=>'form-horizontal systems-form', 'id' => 'users-form')); ?>
            <?php echo Form::hidden(array('name'=>'form[id]', 'data'=> $user)); ?>
        <div class="form-header">Administrator Information</div>
            <div class="errors"></div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="firstname">First name:<span class="required">*</span></label>
                <div class="col-sm-10">
                    <input type="text" name="form[firstname]" id="firstname" class="form-control" value="<?php echo (isset($user->firstname)) ? $user->firstname : ''; ?>" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="lastname">Last name:<span class="required">*</span></label>
                <div class="col-sm-10">
                    <input type="text" name="form[lastname]" id="lastname" class="form-control" value="<?php echo (isset($user->lastname)) ? $user->lastname : ''; ?>" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="email">Email:<span class="required">*</span></label>
                <div class="col-sm-10">
                    <input type="text" name="form[email]" id="email" class="form-control" value="<?php echo (isset($user->email)) ? $user->email : ''; ?>" required="required" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="password">Password:<span class="required">*</span></label>
                <div class="col-sm-10">
                    <input type="password" name="form[password]" id="cb_id_password" class="form-control" value="<?php echo (isset($user->passwordClear)?$user->passwordClear:false); ?>" required="required" autocomplete="false"/>
                    <span class="ajax-result ajax-result-password"></span>
                </div>
            </div>

              <div class="form-group">
                  <label class="col-sm-2 control-label" for="сpassword">Confirm Password:<span class="required">*</span></label>
                  <div class="col-sm-10">
                      <input type="password" style="display:inline-block;" id="cpassword" name="form[cpassword]" class="form-control" value="<?php echo (isset($user->passwordClear)?$user->passwordClear:false); ?>" required="required" />
                      <span class="ajax-result ajax-result-cpassword"></span>
                  </div>
              </div>
      
      <?php if (isset($issystemadmin) && $issystemadmin): ?>
          <div class="form-group">
              <label class="col-sm-2 control-label" for="issystemadmin">System Admin</label>
              <div class="col-sm-10">
                  <input type="checkbox" name="form[issystemadmin]" value="1" id="issystemadmin" style="margin-top:15px;" <?php echo (isset($user->issystemadmin) and $user->issystemadmin) ? 'checked' : ''?> />
              </div>
          </div>
      <?php endif; ?>


        <?php if (count($cfields)) : ?>
            <div class="form-header">Additional Information</div>
            <?php foreach ($cfields as $field) : ?>
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="<?php echo $field->name; ?>"><?php echo $field->title; ?>:<?php echo ($field->required > 0) ? '<span class="required">*</span>' : ''?></label>
                    <div class="col-sm-10">
                        <?php if ($field->fieldtype == 'text') : ?>
                            <input type="text" id="<?php echo $field->name; ?>" name="form[custom_fields][<?php echo $field->name; ?>]" class="form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" value="<?php echo (isset($cfieldsData[$field->id])) ? $cfieldsData[$field->id] : ''; ?>" placeholder="<?php echo $field->defaultvalue; ?>" <?php echo ($field->required > 0) ? 'required="required"' : ""; ?> />
                        <?php elseif ($field->fieldtype == 'textbox') : ?>
                            <textarea id="<?php echo $field->name; ?>" class="form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" name="form[custom_fields][<?php echo $field->name; ?>]" <?php echo ($field->required > 0) ? 'required="required"' : ""; ?>><?php echo (isset($cfieldsData[$field->id])) ? $cfieldsData[$field->id] : $field->defaultvalue; ?></textarea>
                        <?php elseif ($field->fieldtype == 'dropdown') : ?>
                            <select name="form[custom_fields][<?php echo $field->name; ?>]" id="<?php echo $field->name; ?>" class="form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" <?php echo ($field->required > 0) ? 'required="required"' : ""; ?> >
                                <option></option>
                                <?php if ($field->options != '') : ?>
                                    <?php $options = unserialize($field->options); ?>
                                    <?php foreach ($options as $v) : ?>
                                        <option value="<?php echo trim($v); ?>"<?php echo ((isset($cfieldsData[$field->id]) and $cfieldsData[$field->id] == trim($v)) or $field->defaultvalue == trim($v)) ? ' selected' : ''; ?>><?php echo trim($v); ?></option>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </select>
                        <?php elseif ($field->fieldtype == 'datetime') : ?>
                            <input type="text" id="<?php echo $field->name; ?>" name="form[custom_fields][<?php echo $field->name; ?>]" class="datepicker form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" value="<?php echo (isset($cfieldsData[$field->id])) ? $cfieldsData[$field->id] : ''; ?>" placeholder="<?php echo $field->defaultvalue; ?>" <?php echo ($field->required > 0) ? 'required="required"' : ""; ?> />
                        <?php endif; ?>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php endif; ?>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <?php echo Form::button(array('value'=>'Save', 'class'=>'btn btn-primary'),false); ?>
                  <?php echo Form::button(array('value'=>'Cancel','type'=>'button', 'onclick'=>'window.location="'.URL.'users"', 'class'=>'btn btn-default'),false); ?>
              </div>
            </div>
      <?php echo Form::close(); ?>
  </div>
</div>
<script>

    jQuery('#users-form').submit(function(e){
        var error = false;

        jQuery.ajax({
            type: "POST",
            url: '<?php echo URL; ?>users/check_data',
            data: jQuery('#users-form').serialize(),
            dataType: "json",
            async:false,
        }).done(function( data ) {
            jQuery('.errors').html('');
            if (data.status == 'error') {
                error = true;
                jQuery.each(data.errors, function (key, value) {
                    jQuery('.errors').append('<p class="alert alert-danger">' + value + '</p>');
                })
            }
        });

        if (error){
            e.preventDefault();
        }
    });

var password = $('#cb_id_password');
var cpassword = $('#cpassword');
var trigger = $('<i class="showhidepass ion-eye"></i>');
trigger.click(function(){
  if($(this).hasClass('ion-eye')){
    $(this).attr("class","showhidepass ion-eye-disabled");
    $(password).attr("type","text");
  }else{
    $(this).attr("class","showhidepass ion-eye");
    $(password).attr("type","password");
  }
});
var ctrigger = $('<i class="showhidepass ion-eye"></i>');
ctrigger.click(function(){
    if($(this).hasClass('ion-eye')){
        $(this).attr("class","showhidepass ion-eye-disabled");
        $(cpassword).attr("type","text");
    }else{
        $(this).attr("class","showhidepass ion-eye");
        $(cpassword).attr("type","password");
    }
});
$(password).after(trigger);
$(cpassword).after(ctrigger);

$('.datepicker').datepicker();
</script>
