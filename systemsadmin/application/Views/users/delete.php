<div class="card">
<div class="header">
<legend>Delete <?php echo $user->firstname; ?> <?php echo $user->lastname; ?>?</legend>
</div>
<div class="content">
    <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'users/delete/'.$user->id)); ?>
          <?php echo Form::hidden(array('name'=>'confirm', 'value'=> 1)); ?>
          <?php echo Form::button(array('value'=>'Delete', 'class'=>'btn btn-primary'),false); ?>
          <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'users"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
    <?php echo Form::close(); ?>
</div>
</div>
