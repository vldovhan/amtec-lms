<div class="card">
  <div class="header">
    Assign <strong><?php echo $user->name; ?></strong> to Moodle connections
  </div>
  <div class="table-list content table-responsive table-full-width">
      <table class="table">
      <?php foreach($servers as $server): ?>
        <tr>
            <td>
                <i class="icon ion-soup-can"></i>
                <?php echo $server->name; ?>
                <p><?php echo $server->url; ?></p>
            </td>
            <td width="60">
            <?php if($server->assigned): ?>
              <a href="<?php echo URL; ?>servers/unassign/<?php echo $server->id; ?>/<?php echo $user->id; ?>" class="btn btn-wd btn-danger">Unassign</a>
            <?php else: ?>
              <a href="<?php echo URL; ?>servers/assign/<?php echo $server->id; ?>/<?php echo $user->id; ?>" class="btn btn-wd btn-warning">Assign</a>
            <?php endif; ?>
            </td>
        </tr>
      <?php endforeach; ?>
    </table>
  </div>
</div>
