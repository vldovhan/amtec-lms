<div class="card">
	<div class="header hidden">
		<h4 class="title">Users</h4>
		<p class="category">This is a list of all users</p>

	</div>
	<div class="content table-responsive table-full-width datatable">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th align="center">Role</th>
				<th align="center">Created</th>
				<th width="110" align="center">Actions</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($users as $user): ?>
				<tr>
					<td><?php echo $user->firstname . ' ' . $user->lastname ?></td>
					<td><?php echo $user->email ?></td>
                    <td align="center">
                    <?php echo ($user->role == 'admin') ? 'System Admin' : 'Tenant Admin'; ?></td>
					<td align="center"><?php echo ($user->registerDate > 1) ? date("m/d/Y", strtotime($user->registerDate)) : '-'; ?></td>
					<td align="center">
                        <?php if ($current_user->issystemadmin or $current_user->id == $user->id): ?>
						  <a href="<?php echo URL; ?>users/edit/<?php echo $user->id; ?>"><i title="Edit" class="ion-ios-compose-outline"></i></a>
                        <?php endif; ?>
						<?php if ($current_user->id != $user->id and $current_user->issystemadmin): ?>
				            <a href="<?php echo URL; ?>users/delete/<?php echo $user->id; ?>"><i title="Delete" class="ion-ios-trash-outline"></i></a>
						<? endif; ?>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<script>
	$(document).ready(function () {
		$('.table').DataTable({
			"bAutoWidth": false,
			"dom": '<"toolbar">frtip',
			"sPaginationType": "full_numbers",
			"bLengthChange": false
		});
		$("div.toolbar").html('<a class="btn btn-success btn-fill btn-wd" href="<?php echo URL; ?>users/add"><i class="ion-person-add"></i> Create Administrator</a>');
	});
</script>
