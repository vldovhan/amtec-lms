<div class="container">
    <h2>Page not found</h2>
    <p>We are sorry but the page you are looking for does not exist. Please come back again or return to the <a href="<?php echo URL; ?>">homepage</a>.</p>
</div>
