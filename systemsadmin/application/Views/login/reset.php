<div class="login-wrapper">
	<div class="bg-pic">
		<img src="<?php echo URL; ?>/assets/img/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" />
		<div class="bg-caption">
			<h2>talentquest</h2>
			<p>Systems Admin © <?php echo date('Y'); ?></p>
		</div>
	</div>
	<div class="bg-white">
		<div class="login-container">
			<h2>talentquest</h2>
			<div class="login-top-text">Reset password</div>
			<div class="login-form-wrapper">
				<?php if (!$email_sent): ?>
					<?php if ($error): ?>
						<div class="alert alert-danger" role="alert"><?php echo $error; ?></div><br>
					<?php endif; ?>
					<?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'login/reset')); ?>
					<?php echo Form::hidden(array('name'=>'reset', 'value'=> 1), false); ?>
					<?php echo Form::input(array('name'=>'email', 'label'=>'Your Email', 'required'=>true, 'class'=>'form-control', 'placeholder'=>'Email')); ?>
					<?php echo Form::button(array('value'=>'Send', 'class'=>'btn btn-primary'),false); ?>
					<?php echo Form::close(); ?>
				<?php else: ?>
					<div class="alert alert-success" role="alert">Password was reset. Check your e-mail</div><br>
				<?php endif; ?>
				<a href="<?php echo URL ?>login">Login</a>
			</div>
		</div>
	</div>
</div>