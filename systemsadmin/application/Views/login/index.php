<div class="login-wrapper">
    <div class="bg-pic">
        <img src="<?php echo URL; ?>/assets/img/new-york-city-buildings-sunrise-morning-hd-wallpaper.jpg" />
        <div class="bg-caption hidden">
            <h2>amtec</h2>
            <p>Systems Admin © <?php echo date('Y'); ?></p>
        </div>
    </div>
    <div class="bg-white">
        <div class="login-container">
            <div class="logo">
                <a href="<?php echo URL; ?>">
                    <img src="<?php echo URL; ?>/assets/img/logo.png" alt="LMS Tenant Management System">
                </a>
            </div>
            <h2>LMS Tenant Management System</h2>
            <div class="login-form-wrapper">
                <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'login/', )); ?>
                      <?php echo Form::input(array('name'=>'email', 'label'=>'LOGIN', 'required'=>true, 'class'=>'form-control', 'placeholder'=>'Email')); ?>
                      <?php echo Form::input(array('name'=>'password', 'type'=>'password', 'label'=>'PASSWORD', 'class'=>'form-control', 'placeholder'=>'Credentials')); ?>
                      <?php echo Form::button(array('value'=>'Login', 'class'=>'btn btn-primary'),false); ?>
                <?php /* ?><a href="<?php echo URL ?>login/reset">Reset password</a><?php */ ?>
                <?php echo Form::open(); ?>
            </div>
        </div>    
    </div>
</div>
