<div class="card">
  <div class="content">
      <div class="updating-system-box">
        <ul class="d-steps">
            <?php $k = 1; foreach($systems as $item) : ?>
                <li id="<?php echo str_replace('.', '_', $item->url); ?>" class="d-steps-item" style="display:block;"><span class="item-num"><?php echo $k++; ?></span> <?php echo $item->name; ?><span class="status-box"></span></li>
            <?php endforeach; ?>
        </ul>
      </div>
  </div>
</div>
<script>
	<?php foreach ($systems as $item) : ?>
		<?php if ($item->other_domain > 0) {
			$host = $item->url;
		} else {
			$host = $item->url.MAINDOMAIN;
		} ?>
		var url = "<?php echo WWWPROTOCOL.$host;?>/admin/index.php";
		var id = '#<?php echo str_replace('.', '_', $item->url); ?>';
        <?php if ($item->state > 0) : ?>
            jQuery.ajax({
                type: 'GET',
                data: 'confirmplugincheck=1&cache=0',
                url: url,
                async: false,
                dataType: "json",
                beforeSend: function(){
                    jQuery(id+" .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                },
                 error: function (request, status, error) {
                    jQuery.ajax({
                        url: '<?php echo URL; ?>systems/sendrequest/update_profile/<?php echo $item->id; ?>',
                        dataType: "json",
                        async:false
                        }).done(function( data ) {										
                            jQuery(id).addClass("completed");
                            jQuery(id+" .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                        });
                }
            }).done(function( data ) {
                jQuery.ajax({
                    url: '<?php echo URL; ?>systems/sendrequest/update_profile/<?php echo $item->id; ?>',
                    dataType: "json",
                    async:false
                    }).done(function( data ) {										
                        jQuery(id).addClass("completed");
                        jQuery(id+" .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                    });
            });
        <?php else : ?>
            jQuery(id).addClass("warning");
            jQuery(id+" .status-box").html("<i class=\"ion-ios-minus-outline yellow\" title=\"Inactive\"></i>");
        <?php endif; ?>
	<?php endforeach; ?>
</script>

