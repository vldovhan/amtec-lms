<div class="card">
  <div class="header clearfix hidden">
    <h4 class="title">Update LMS DB's</h4>
    <p class="category">Here you can update Moodle Database settings on all your systems. Click "Update DB" to process.</p>
  </div>
    <div class="content">
        <a href="<?php echo URL;?>updatedb/process" class="btn btn-warning">Update DB</a>
    </div>
  </div>
</div>