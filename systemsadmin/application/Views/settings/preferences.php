<div class="card">
  <div class="header">
  <legend>Dashboard settings</legend>
  </div>
  <div class="content">
      <style>
        .header-preview{background-color: #<?php echo $options->h2; ?>; color: #<?php echo $options->h4; ?>;line-height: 42px; margin:0 50px 50px; height: 42px; overflow: hidden;}
        .header-preview .server{background-color: #<?php echo $options->h3; ?>;  width: 270px; float: left;padding: 0 20px;}
        .header-preview .logo{float: left;padding: 0 20px;}
        .header-preview .logo a{color: #<?php echo $options->h4; ?>;}
        .header-preview .menu{float: right;padding: 0 20px;text-align: right;}
        .header-preview .menu i{font-size: 18px; margin-left: 10px;}
        .form-control{background: #fff none repeat scroll 0 0 !important;color: #565656 !important;}
      </style>
      <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'settings/save', 'class'=>'form-horizontal')); ?>
            <?php if (count($settings)) : ?>
                <?php foreach ($settings as $setting) : ?>
                    <?php $params = array('name'=>'form['.$setting->name.']', 'label'=>$setting->title); 
                        $params['value'] = ($setting->value) ? $setting->value : $setting->defaultvalue;
                        $params['data-input-type'] = $setting->type;
                        $params['class'] = 'form-control'.(($setting->type == 'color') ? ' jscolor' : '');
                    ?>
                    <?php echo Form::input($params); ?>        
                <?php endforeach; ?>
            <?php endif; ?>
            
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <?php echo Form::button(array('value'=>'Save', 'class'=>'btn btn-primary'),false); ?>
                  <?php echo Form::button(array('value'=>'Cancel','type'=>'button', 'onclick'=>'window.location="'.URL.'users"', 'class'=>'btn btn-default'),false); ?>
              </div>
            </div>
      <?php echo Form::close(); ?>
  </div>
</div>
<script src="<?php echo URL; ?>assets/js/jscolor.js"></script>
