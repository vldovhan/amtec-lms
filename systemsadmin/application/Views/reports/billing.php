<div class="card">
	<div class="content table-responsive table-full-width datatable">
		<table class="table table-striped">
			<thead>
			<tr>
				<th>Tenant Name</th>
				<th>Primary Contact</th>
				<th align="center">Total Users</th>
				<th align="center">Active Users</th>
				<th align="center">Unique Logins</th>
				<th width="110" align="center">Actions</th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($data as $item): ?>
				<tr>
					<td><?php echo $item->tenantname ?></td>
					<td><?php echo $item->contact ?></td>
					<td align="center"><?php echo $item->users_all ?></td>
					<td align="center"><?php echo $item->users_active ?></td>
					<td align="center"><?php echo $item->unique_logins ?></td>
					<td align="center">
				        <a href="<?php echo URL; ?>reports/billingdetails/<?php echo $item->id; ?>"><i title="Billing Details" class="ion-navicon"></i></a>
					</td>
				</tr>
			<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>
<script>
	$(document).ready(function () {
		var table = $('.table').dataTable({
			"bAutoWidth": false,
			"sPaginationType": "full_numbers",
			"aLengthMenu": [[-1, 15, 50, 100, 500, 1000], ["All", 15, 50, 100, 500, 1000]],
            "iDisplayLength": 15,
            dom: 'Bfrtlp',
            buttons: [
               {
                   extend: 'excel',
                   footer: false,
                   exportOptions: {
                        columns: [0,1,2,3,4]
                    }
               },
               {
                   extend: 'csv',
                   footer: false,
                   exportOptions: {
                        columns: [0,1,2,3,4]
                    }
               },
               {
                   extend: 'pdf',
                   footer: false,
                   exportOptions: {
                        columns: [0,1,2,3,4]
                    }
               },
               {
                   extend: 'print',
                   footer: false,
                   exportOptions: {
                        columns: [0,1,2,3,4]
                    }
               },
            ]
		});
	});
</script>
