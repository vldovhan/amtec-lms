<div class="card">
	<div class="header hidden">
		<h4 class="title">Users</h4>
		<p class="category">This is a list of all users</p>

	</div>
	<div class="content table-responsive table-full-width datatable">
		<table class="table table-striped">
			<thead>
			<tr>
				<th value="idnumber">Employee ID</th>
				<th value="name">Name</th>
				<th value="email">Email</th>
				<th value="timespend" align="center">Time Spent</th>
				<th value="visits" align="center">Visits</th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>
<script>
    var users = $('.table').dataTable({
        "bAutoWidth": false,
        "bProcessing": false,
        "bServerSide": true,
        "bStateSave": false,
        "sAjaxSource": "<?php echo URL; ?>reports/reportdata/billing/<?php echo $system->id; ?>/",
        "sPaginationType": "full_numbers",
        "aLengthMenu": [[-1, 15, 50, 100, 500, 1000], ["All", 15, 50, 100, 500, 1000]],
        "iDisplayLength": 15,
        dom: 'Bfrtlp',
            buttons: [
               'excel', 'csv', 'pdf', 'print'
            ]
    });
 
    var filter = '1';
    if (users.fnSettings().aoPreSearchCols[0].sSearch.length){
        filter = users.fnSettings().aoPreSearchCols[0].sSearch;
    }
    
    jQuery('.dataTables_filter input').after('<select id="table_filter" class="form-control" style="margin-left: 7px;" onchange="users.fnFilter( $(this).val(), 0 )"><option value="-1" '+((filter=='-1') ? "selected" : "")+'>Show All</option><option value="1" '+((filter=='1') ? "selected" : "")+'>Active</option><option value="0" '+((filter=='0') ? "selected" : "")+'>Inactive</option></select>');
</script>

