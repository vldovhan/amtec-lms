<div class="card">
<div class="header">
<legend>Delete Custom Field <?php echo $cfield->title; ?>?</legend>
</div>
<div class="content">
    <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'customfields/delete/'.$cfield->id)); ?>
          <?php echo Form::hidden(array('name'=>'confirm', 'value'=> 1)); ?>
          <?php echo Form::button(array('value'=>'Delete', 'class'=>'btn btn-primary'),false); ?>
          <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'customfields"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
    <?php echo Form::close(); ?>
</div>
</div>
