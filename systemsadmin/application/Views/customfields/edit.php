<div class="card">
  <div class="header hidden">
  <legend><?php echo (empty($user)) ? 'Edit Custom Field' : 'Create Custom Field'; ?></legend>
  </div>
  <div class="content">
      <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'customfields/save', 'class'=>'form-horizontal cfields-form')); ?>
            <?php echo Form::hidden(array('name'=>'form[id]', 'data'=> $cfield)); ?>
        
            <?php echo Form::input(array('name'=>'form[name]', 'label'=>'Name (unique)', 'data'=> $cfield, 'required'=>true, 'class'=>'form-control field-name')); ?>
            <?php echo Form::input(array('name'=>'form[title]', 'label'=>'Title', 'data'=> $cfield, 'required'=>true, 'class'=>'form-control')); ?>
            <?php echo Form::select(array('name'=>'form[type]', 'label'=>'Type', 'data'=> array('user'=>'User', 'system'=>'System'), 'required'=>true, 'class'=>'form-control', 'value'=>((isset($cfield->type) ? $cfield->type : '' )))); ?>
            <?php echo Form::select(array('name'=>'form[fieldtype]', 'label'=>'Field Type', 'data'=> array('text'=>'Text', 'textbox'=>'Textbox', 'dropdown'=>'Dropdown', 'datetime' => 'Date'), 'required'=>true, 'class'=>'form-control field-type', 'value'=>((isset($cfield->fieldtype) ? $cfield->fieldtype : '' )))); ?>
            <?php echo Form::textarea(array('name'=>'form[options]', 'label'=>'Option values<br>(one per line)', 'data'=> $cfield, 'class'=>'form-control field-options')); ?>
            <?php echo Form::select(array('name'=>'form[required]', 'label'=>'Required', 'data'=> array('0'=>'Not required', '1'=>'Required'), 'class'=>'form-control', 'value'=>((isset($cfield->required) ? $cfield->required : '' )))); ?>
            <?php echo Form::textarea(array('name'=>'form[defaultvalue]', 'label'=>'Default Value)', 'data'=> $cfield, 'class'=>'form-control')); ?>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                  <?php echo Form::button(array('value'=>'Save', 'class'=>'btn btn-primary'),false); ?>
                  <?php echo Form::button(array('value'=>'Cancel','type'=>'button', 'onclick'=>'window.location="'.URL.'customfields"', 'class'=>'btn btn-default'),false); ?>
              </div>
            </div>
      <?php echo Form::close(); ?>
  </div>
</div>
<script>
    <?php if (!isset($cfield->fieldtype) or (isset($cfield->fieldtype) and $cfield->fieldtype != 'dropdown')) : ?>
        jQuery('.field-options').parent().parent().hide();
    <?php endif; ?>
    jQuery('.field-type').click(function(e){
       if (jQuery(this).val() == 'dropdown'){
           jQuery('.field-options').parent().parent().show();
       } else {
           jQuery('.field-options').parent().parent().hide();
       }
    });
</script>
