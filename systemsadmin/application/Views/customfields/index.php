<div class="card">
  <div class="header hidden">
    <h4 class="title">Custom Fields</h4>
    <p class="category">This is a list of all custom fields</p>
  </div>
  <div class="content table-responsive table-full-width datatable">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Title</th>
          <th>Type</th>
          <th>Field Type</th>
          <th align="center">Status</th>
          <th width="100" align="center">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (count($cfields)) : ?>
            <?php foreach($cfields as $item): ?>
              <tr>
                <td><?php echo $item->title ?></td>
                <td><?php echo ucfirst($item->type) ?></td>
                <td><?php echo (isset($fieldstypes[$item->fieldtype])) ? $fieldstypes[$item->fieldtype] : ucfirst($item->fieldtype) ?></td>
                <td align="center">
                    <?php if($item->state): ?>
                      <i title="Enabled" class="ion-ios-circle-filled"></i>
                    <?php else: ?>
                      <i title="Disable" class="ion-ios-circle-outline"></i>
                    <?php endif; ?>
                  </td>
                <td align="center">
                    <?php if($item->state): ?>
                        <a href="<?php echo URL; ?>customfields/disable/<?php echo $item->id; ?>" class="green"><i title="Inactive" class="ion-eye"></i></a>
                    <?php else: ?>
                        <a href="<?php echo URL; ?>customfields/enable/<?php echo $item->id; ?>" class="green"><i title="Active" class="ion-eye-disabled"></i></a>
                    <?php endif; ?>
                    <a href="<?php echo URL; ?>customfields/edit/<?php echo $item->id; ?>"><i title="Edit" class="ion-ios-compose-outline"></i></a>
                    <a href="<?php echo URL; ?>customfields/delete/<?php echo $item->id; ?>"><i title="Delete" class="ion-ios-trash-outline"></i></a>
                </td>
              </tr>
          <?php endforeach; ?>
      <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
$(document).ready(function(){
    $('.table').DataTable({
      "bAutoWidth": false,
			"dom": '<"toolbar">frtip',
			"sPaginationType": "full_numbers",
      "bLengthChange": false
    });
    $("div.toolbar").html('<a class="btn btn-success btn-fill btn-wd" href="<?php echo URL; ?>customfields/add"></i> Create new field</a>');
});
</script>
