<div class="card">
  <div class="content">
      <div class="updating-system-box">
        <ul class="d-steps">
            <?php $k = 1; foreach($systems as $item) : ?>
                <li id="<?php echo str_replace('.', '_', $item->url); ?>" class="d-steps-item" style="display:block;"><span class="item-num"><?php echo $k++; ?></span> <?php echo $item->name; ?><span class="status-box"></span></li>
            <?php endforeach; ?>
        </ul>
      </div>
  </div>
</div>
<script>
	<?php foreach ($systems as $item) : ?>
		<?php if ($item->other_domain > 0) : ?>
			var rurl = "<?php echo $item->url; ?>";
		<?php else : ?>
			var rurl = "<?php echo $item->url; ?><?php echo MAINDOMAIN;?>";
		<?php endif; ?>
		var url = "<?php echo WWWPROTOCOL.MAINHOST;?>/MasterCopy/clearcache.php";
		var id = '#<?php echo str_replace('.', '_', $item->url); ?>';
    
        <?php if ($item->state > 0) : ?>
            jQuery.ajax({
                type: 'POST',
                data: 'action=send_request&url='+rurl,
                url: url,
                async: false,
                dataType: "json",
                beforeSend: function(){
                    jQuery(id+" .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                },
                 error: function (request, status, error) {
                    jQuery(id).addClass("error");
                    jQuery(id+" .status-box").html("<i class=\"ion-ios-close-outline red\" title=\"Error\"></i>");
                }
            }).done(function( data ) {
                if (data.status == 1){
                    jQuery(id).addClass("completed");
                    jQuery(id+" .status-box").html("<i class=\"ion-ios-checkmark-outline green\" title=\"Completed\"></i>");
                } else {
                    jQuery(id).addClass("error");
                    jQuery(id+" .status-box").html("<i class=\"ion-ios-close-outline red\" title=\"Error\"></i>");
                }
            });    
        <?php else : ?>
            jQuery(id).addClass("warning");
            jQuery(id+" .status-box").html("<i class=\"ion-ios-minus-outline yellow\" title=\"Inactive\"></i>");
        <?php endif; ?>
        
	<?php endforeach; ?>
</script>
