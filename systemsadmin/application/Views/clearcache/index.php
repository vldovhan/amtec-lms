<div class="card">
  <div class="header clearfix hidden">
    <h4 class="title">Clear Cache</h4>
    <p class="category">Here you can clear Moodle cache in all your systems. Click "Clear Cache" to process.</p>
  </div>
    <div class="content">
        <a href="<?php echo URL;?>clearcache/process" class="btn btn-warning">Clear Cache</a>
    </div>
  </div>
</div>