<div class="card">
<div class="header">
<legend>Delete backup from <?php echo date('h:i a, d M Y', $backup->timecreated); ?>?</legend>
</div>
<div class="content">
    <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'backups/delete/'.$backup->id)); ?>
          <?php echo Form::hidden(array('name'=>'confirm', 'value'=> 1)); ?>
          <?php echo Form::button(array('value'=>'Delete', 'class'=>'btn btn-primary'),false); ?>
          <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'backups/details/'.$system->id.'"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
    <?php echo Form::close(); ?>
</div>
</div>
