<div class="card">
  <div class="header">
  <legend>
      Creating System Backup
  </legend>
  </div>
  <div class="content">
      <div class="updating-system-box">
        <ul class="d-steps">
			<li id="db" class="d-steps-item"><span class="item-num">1</span> Creating Database Copy<span class="status-box"></span></li>
			<li id="md" class="d-steps-item active"><span class="item-num">2</span> Creating Data Copy<span class="status-box"></span></li>
			<li id="completed" class="d-steps-item completed-item clearfix"><span class="completed-text green">Backup successfully Completed!</span><input type="button" name="Continue" class="btn btn-success" value="Continue" onclick="location='<?php echo URL;?>backups/details/<?php echo $system->id; ?>'" style="float:right; margin:-5px;" /></li>
			<li id="error" class="d-steps-item completed-item"><span class="completed-text red">Error!</span> <span class="error-text"></span></li>
		</ul>
      </div>
  </div>
</div>
<script>
    var error = false;
    jQuery("#db").addClass("visible");
    if (error == false){
        jQuery.ajax({ //creating database dump
            url: '<?php echo URL; ?>backups/create_db_dump/<?php echo $backup->id; ?>',
            dataType: "json",
            beforeSend: function(){
                jQuery("#db .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
            }
        }).done(function( data ) {
            if (data.status == 1){
                jQuery("#db").addClass("completed");
                jQuery("#db .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                // creating moodledata zip archive
                jQuery("#md").addClass("visible");
                jQuery.ajax({
                    url: '<?php echo URL; ?>/backups/create_md_dump/<?php echo $backup->id; ?>',
                    dataType: "json",
                    beforeSend: function(){
                        jQuery("#md .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                    }
                }).done(function( data ) {
                    if (data.status == 1){
                        jQuery("#md").addClass("completed");
                        jQuery("#md .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                        jQuery("#completed").addClass("visible");
                    } else {
                        jQuery("#md").addClass("error");
                        jQuery("#md .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                        error = data.error;
                        jQuery("#error").addClass("visible");
                        jQuery("#error .completed-text").text(error);
                    }
                });
                // end creating moodledata zip archive
            } else {
                jQuery("#db").addClass("error");
                jQuery("#db .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                error = data.error;
                jQuery("#error").addClass("visible");
                jQuery("#error .completed-text").text(error);
            }
            // end creating database dump
        });
    }
</script>