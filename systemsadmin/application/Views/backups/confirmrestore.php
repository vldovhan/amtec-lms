<div class="card">
<div class="header">
<legend>Are you shure want to restore system <?php echo $system->name; ?> from <?php echo date('h:i a, d M Y', $backup->timecreated); ?> backup?</legend>
</div>
<div class="content">
    <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'backups/restore/'.$backup->id)); ?>
          <?php echo Form::hidden(array('name'=>'confirm', 'value'=> 1)); ?>
          <?php echo Form::button(array('value'=>'Restore', 'class'=>'btn btn-primary'),false); ?>
          <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'backups/details/'.$system->id.'"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
    <?php echo Form::close(); ?>
</div>
</div>
