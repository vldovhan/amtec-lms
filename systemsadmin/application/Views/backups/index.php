<div class="card">
  <div class="header hidden">
    <h4 class="title">Backups</h4>
    
  </div>
  <div class="content table-responsive table-full-width datatable">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Instance Name</th>
          <th>Last backup</th>
          <th width="70" align="center">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($backups as $item): ?>
          <tr>
            <td><?php echo $item->name ?></td>
            <td><?php echo ($item->backuptime) ? date('h:i a, d M Y', $item->backuptime) : 'Not available'; ?></td>
            <td align="center">
                <a href="<?php echo URL; ?>backups/details/<?php echo $item->id; ?>"><i title="Details" class="ion-ios-photos-outline"></i></a>
            </td>
          </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
$(document).ready(function(){
    $('.table').DataTable({
      "bAutoWidth": false,
			"dom": '<"toolbar">frtip',
			"sPaginationType": "full_numbers",
      "bLengthChange": false
    });
});
</script>
