<div class="card">
  <div class="header hidden">
    <h4 class="title">Backups Details: <?php echo $system->name; ?></h4>
    
  </div>
  <div class="content table-responsive table-full-width datatable">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Backup Time</th>
          <th>Backup Type</th>
          <th>Restored</th>
          <th width="100" align="center">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (count($backups)) : ?>
            <?php foreach($backups as $item): ?>
              <tr>
                <td><?php echo ($item->timecreated) ? date('h:i a, d M Y', $item->timecreated) : 'Not available'; ?></td>
                <td><?php echo ($item->btype) ? ucfirst($item->btype) : 'Not available'; ?></td>
                <td><?php echo ($item->restored) ? date('h:i a, d M Y', $item->restored) : 'Not available'; ?></td>
                <td align="center">
                    <a href="<?php echo URL; ?>backups/download/database/<?php echo $item->id; ?>"><i title="Download Database" class="ion-social-buffer-outline"></i></a>
                    <a href="<?php echo URL; ?>backups/download/moodledata/<?php echo $item->id; ?>"><i title="Download Moodledata" class="ion-android-archive"></i></a>
                    <a href="<?php echo URL; ?>backups/restore/<?php echo $item->id; ?>"><i title="Restore system" class="ion-ios-loop-strong"></i></a>
                    <a href="<?php echo URL; ?>backups/delete/<?php echo $item->id; ?>"><i title="Delete" class="ion-ios-trash-outline"></i></a>
                </td>
              </tr>
          <?php endforeach; ?>
      <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
$(document).ready(function(){
    $('.table').DataTable({
      "bAutoWidth": false,
			"dom": '<"toolbar">frtip',
			"sPaginationType": "full_numbers",
      "bLengthChange": false
    });
    $("div.toolbar").html('<a class="btn btn-success btn-fill btn-wd" href="<?php echo URL; ?>backups/backup/<?php echo $system->id; ?>">Create Restore Point</a>');
});
</script>
