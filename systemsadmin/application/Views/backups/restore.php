<div class="card">
  <div class="header">
  <legend>
      Restore System Backup from <?php echo date('h:i a, d M Y', $backup->timecreated); ?>
  </legend>
  </div>
  <div class="content">
      <div class="updating-system-box">
        <ul class="d-steps">
			<li id="db_b" class="d-steps-item"><span class="item-num">1</span> Creating Database Copy<span class="status-box"></span></li>
			<li id="md_b" class="d-steps-item active"><span class="item-num">2</span> Creating Data Copy<span class="status-box"></span></li>
			<li id="db_r" class="d-steps-item"><span class="item-num">3</span> Restore Database<span class="status-box"></span></li>
			<li id="md_r" class="d-steps-item active"><span class="item-num">5</span> Restore Data<span class="status-box"></span></li>
			<li id="cache" class="d-steps-item active"><span class="item-num">6</span> Clear Cache<span class="status-box"></span></li>
			<li id="completed" class="d-steps-item completed-item clearfix"><span class="completed-text green">Completed!</span><input type="button" name="Continue" class="btn btn-success" value="Continue" onclick="location='<?php echo URL;?>backups/details/<?php echo $system->id; ?>'" style="float:right; margin:-5px;" /></li>
			<li id="error" class="d-steps-item completed-item"><span class="completed-text red">Error!</span> <span class="error-text"></span></li>
		</ul>
      </div>
  </div>
</div>
<script>
    var error = false;
    jQuery("#db_b").addClass("visible");
    if (error == false){
        jQuery.ajax({ //creating database dump
            url: '<?php echo URL; ?>backups/create_db_dump/<?php echo $newbackup->id; ?>',
            dataType: "json",
            beforeSend: function(){
                jQuery("#db_b .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
            }
        }).done(function( data ) {
            if (data.status == 1){
                jQuery("#db_b").addClass("completed");
                jQuery("#db_b .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                // creating moodledata zip archive
                jQuery("#md_b").addClass("visible");
                jQuery.ajax({
                    url: '<?php echo URL; ?>/backups/create_md_dump/<?php echo $newbackup->id; ?>',
                    dataType: "json",
                    beforeSend: function(){
                        jQuery("#md_b .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                    }
                }).done(function( data ) {
                    if (data.status == 1){
                        jQuery("#md_b").addClass("completed");
                        jQuery("#md_b .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                        // restore database
                            jQuery("#db_r").addClass("visible");
                            jQuery.ajax({
                                url: '<?php echo URL; ?>/backups/restore_db_dump/<?php echo $backup->id; ?>',
                                dataType: "json",
                                async:false,
                                beforeSend: function(){
                                    jQuery("#db_r .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                                }
                            }).done(function( data ) {
                                if (data.status == 1){
                                    jQuery("#db_r").addClass("completed");
                                    jQuery("#db_r .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                                    // restoring data
                                    jQuery("#md_r").addClass("visible");
                                    jQuery.ajax({
                                        url: '<?php echo URL; ?>/backups/restore_md/<?php echo $backup->id; ?>',
                                        dataType: "json",
                                        async:false,
                                        beforeSend: function(){
                                            jQuery("#md_r .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                                        }
                                    }).done(function( data ) {
                                        if (data.status == 1){
                                            jQuery("#md_r").addClass("completed");
                                            jQuery("#md_r .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                                            // clear cache
                                            jQuery("#cache").addClass("visible");
                                            <?php if ($system->other_domain > 0) : ?>
                                                var rurl = "<?php echo $system->url; ?>";
                                            <?php else : ?>
                                                var rurl = "<?php echo $system->url; ?><?php echo MAINDOMAIN; ?>";
                                            <?php endif; ?>
                                            var url = "<?php echo WWWPROTOCOL.MAINHOST;?>/MasterCopy/clearcache.php";
                                            jQuery.ajax({
                                                type: 'POST',
                                                data: 'action=send_request&url='+rurl,
                                                url: url,
                                                async: false,
                                                dataType: "json",
                                                beforeSend: function(){
                                                    jQuery("#cache .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
                                                },
                                            }).done(function( data ) {
                                                if (data.status == 1){
                                                    jQuery("#cache").addClass("completed");
                                                    jQuery("#cache .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
                                                    jQuery("#completed").addClass("visible");
                                                } else {
                                                    jQuery("#cache").addClass("error");
                                                    jQuery("#cache .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                                                    error = data.error;
                                                    jQuery("#error").addClass("visible");
                                                    jQuery("#error .completed-text").text(error);
                                                }
                                            });
                                            // end clear cache
                                        } else {
                                            jQuery("#md_r").addClass("error");
                                            jQuery("#md_r .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                                            error = data.error;
                                            jQuery("#error").addClass("visible");
                                            jQuery("#error .completed-text").text(error);
                                        }
                                    });
                                    // end restoring data
                                } else {
                                    jQuery("#db_r").addClass("error");
                                    jQuery("#db_r .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                                    error = data.error;
                                    jQuery("#error").addClass("visible");
                                    jQuery("#error .completed-text").text(error);
                                }
                            });
                        // end restoring database
                    } else {
                        jQuery("#md_b").addClass("error");
                        jQuery("#md_b .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                        error = data.error;
                        jQuery("#error").addClass("visible");
                        jQuery("#error .completed-text").text(error);
                    }
                });
                // end creating moodledata zip archive
            } else {
                jQuery("#db_b").addClass("error");
                jQuery("#db_b .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
                error = data.error;
                jQuery("#error").addClass("visible");
                jQuery("#error .completed-text").text(error);
            }
            // end creating database dump
        });
    }
</script>