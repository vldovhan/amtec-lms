<div class="card">
  <div class="header">
    Search "<?php echo $keyword; ?>"
  </div>
  <div class="table-list content table-responsive <?php echo ($results)?'table-full-width':'' ?>">
        <?php if($results): ?>
          <table class="table">
        <?php foreach($results as $item): ?>
          <tr>
              <td>
                  <i class="icon <?php echo ($item->type)?'ion-soup-can':'ion-person-stalker'; ?>"></i>
                  <h4><?php echo $item->name; ?></h4>
                  <p><?php echo $item->contact; ?></p>
              </td>
              <td align="center">
                <?php echo ($item->type)?'Moodle connection':'Customer'; ?>
              </td>
              <td width="50">
                  <a class="btn btn-wd btn-info" href="<?php echo URL; ?><?php echo (($item->type)?'servers/edit/':'users/edit/').$item->id; ?>">Details</a>
              </td>
          </tr>
        <?php endforeach; ?>
        </table>
      <?php else: ?>
        <div class="alert alert-warning">No data</div>
      <?php endif; ?>

  </div>
</div>
