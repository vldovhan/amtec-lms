<div class="dashboard-header">
    <h4 class="title">Total <small><?php echo date("d F Y"); ?></small>
        <select name="dashbord-filter" class="dashbord-filter" id="dashbord_filter">
            <option value="0" <?php echo ($dashboardFilter == 0) ? 'selected' : ''?>>All Tenants</option>
            <?php if (count($systems)) : ?>
                <?php foreach ($systems as $system) : ?>
                    <option value="<?php echo $system->id; ?>" <?php echo ($dashboardFilter == $system->id) ? 'selected' : ''?>><?php echo $system->name; ?></option>
                <?php endforeach; ?>
            <?php endif; ?>
        </select>
    </h4>
    <div class="row">
        <div class="col-xs-6 stats-item-block systems getStatsSystems">
            <div class="card">
                <div class="panel-actions"><a href="javascript:void(0);" onclick="stats_refresh('getStatsSystems');" class="btn-panel" title="refresh"><i class="ion-ios-refresh-empty"></i></a></div>
                <div class="title">Instances</div>
                <div class="stats-icon"><i class="ion-ios-albums-outline"></i></div>
                <ul class="clearfix">
                    <li>
                        <p class="ellipsis">Total Instances</p>
                        <p class="numbers" id="systems_all"><?php echo ($statsSystems['systems_all'] > 0) ? $statsSystems['systems_all'] : '0'; ?></p>
                    </li>
                    <li>
                        <p class="ellipsis">Instances Created This Month</p>
                        <p class="numbers" id="systems_month"><?php echo ($statsSystems['systems_month'] > 0) ? $statsSystems['systems_month'] : '0'; ?></p>
                    </li>
                    <li>
                        <p class="ellipsis">Instances Created Today</p>
                        <p class="numbers" id="systems_today"><?php echo ($statsSystems['systems_today'] > 0) ? $statsSystems['systems_today'] : '0'; ?></p>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-xs-6 stats-item-block users getStatsUsers">
            <div class="card">
                <div class="panel-actions"><a href="javascript:void(0);" onclick="stats_refresh('getStatsUsers');" class="btn-panel" title="refresh"><i class="ion-ios-refresh-empty"></i></a></div>
                <div class="title">Users</div>
                <div class="stats-icon"><i class="ion-ios-people-outline"></i></div>
                <ul class="clearfix">
                    <li>
                        <p class="ellipsis">Total Users</p>
                        <p class="numbers" id="users_all"><?php echo ($statsUsers['users_all'] > 0) ? $statsUsers['users_all'] : '0'; ?></p>
                    </li>
                    <li>
                        <p class="ellipsis">Users Created This Month</p>
                        <p class="numbers" id="users_month"><?php echo ($statsUsers['users_month'] > 0) ? $statsUsers['users_month'] : '0'; ?></p>
                    </li>
                    <li>
                        <p class="ellipsis">Users Created Today</p>
                        <p class="numbers" id="users_today"><?php echo ($statsUsers['users_today'] > 0) ? $statsUsers['users_today'] : '0'; ?></p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6 stats-item-block courses getStatsCourses">
            <div class="card">
                <div class="panel-actions"><a href="javascript:void(0);" onclick="stats_refresh('getStatsCourses');" class="btn-panel" title="refresh"><i class="ion-ios-refresh-empty"></i></a></div>
                <div class="title">Courses</div>
                <div class="stats-icon"><i class="ion-ios-bookmarks-outline"></i></div>
                <ul class="clearfix">
                    <li>
                        <p class="ellipsis">Total Courses</p>
                        <p class="numbers" id="courses_all"><?php echo ($statsCourses['courses_all'] > 0) ? $statsCourses['courses_all'] : '0'; ?></p>
                    </li>
                    <li>
                        <p class="ellipsis">Courses Created This Month</p>
                        <p class="numbers" id="courses_month"><?php echo ($statsCourses['courses_month'] > 0) ? $statsCourses['courses_month'] : '0'; ?></p>
                    </li>
                    <li>
                        <p class="ellipsis">Courses Created Today</p>
                        <p class="numbers" id="courses_today"><?php echo ($statsCourses['courses_today'] > 0) ? $statsCourses['courses_today'] : '0'; ?></p>
                    </li>
                </ul>
            </div>
        </div>
        <?php /* ?>
        <div class="col-xs-6 stats-item-block space getStatsSpace">
            <div class="card">
                <div class="panel-actions"><a href="javascript:void(0);" onclick="stats_refresh('getStatsSpace');" class="btn-panel" title="refresh"><i class="ion-ios-refresh-empty"></i></a></div>
                <div class="title">Disk Space</div>
                <div class="easyPieChart" data-size="100" data-percent="<?php echo ($statsSpace['disk_percentage'] > 0) ? $statsSpace['disk_percentage'] : '0'; ?>" data-scalecolor="#555" data-trackcolor="#555" data-barcolor="#232332">
                    <span class="percent"><?php echo ($statsSpace['disk_percentage'] > 0) ? $statsSpace['disk_percentage'] : '0'; ?>%</span>
                </div>                    
                <div class="disk-statistic">
                    <ul class="clearfix">
                        <li>
                            <p class="ellipsis">Total Space <span class="full-space-val">(<?php echo $statsSpace['full_space_val']; ?>)</span></p>
                            <p class="numbers spacenumbers" id="space_all"><?php echo ($statsSpace['space_all'] > 0) ? $statsSpace['space_all'] : '0'; ?></p>
                        </li>
                        <li>
                            <p class="ellipsis">Used Space <span class="usage-space-val">(<?php echo $statsSpace['usage_space_val']; ?>)</span></p>
                            <p class="numbers spacenumbers" id="space_usage"><?php echo ($statsSpace['space_usage'] > 0) ? $statsSpace['space_usage'] : '0'; ?></p>
                        </li>
                        <li>
                            <p class="ellipsis">Free Space <span class="free-space-val">(<?php echo $statsSpace['free_space_val']; ?>)</span></p>
                            <p class="numbers spacenumbers" id="space_free"><?php echo ($statsSpace['space_free'] > 0) ? $statsSpace['space_free'] : '0'; ?></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <?php */ ?>
    </div>
</div>

<script type="text/javascript" src="<?php echo URL; ?>/assets/js/countUp.min.js"></script>
<script type="text/javascript" src="<?php echo URL; ?>/assets/js/jquery.easypiechart.min.js"></script>
<script>
    $('.easyPieChart').easyPieChart();
    stats_start();
    
    $('#dashbord_filter').change(function(e){
        //stats_refresh('getStatsSystems');
        stats_refresh('getStatsUsers');
        stats_refresh('getStatsCourses');
        //stats_refresh('getStatsSpace');
    });
    
    function stats_start(type){
        var options = {
					useEasing : true,
					useGrouping : true,
					separator : ',',
					decimal : '.',
					prefix : '', 
					suffix : ''
				}
        var end = 0;
        if (typeof type != 'undefined'){
            $('.stats-item-block.'+type+' .card .numbers').each(function(e){
                if (type == 'getStatsSpace') end = 1;
                var count = $(this).text();
                var id = $(this).attr('id');
                var item = new countUp(id, 0, count, end, 2.5, options);
                item.start();
            });
        } else {
            $('.card .numbers').each(function(e){
                if ($(this).hasClass('spacenumbers')) end = 1;
                var count = $(this).text();
                var id = $(this).attr('id');
                var item = new countUp(id, 0, count, end, 2.5, options);
                item.start();
            });
        }
    }
    function stats_refresh(type){
        var filter = jQuery('#dashbord_filter').val();
        jQuery.ajax({
            url: '<?php echo URL; ?>home/refreshstats/'+type+'/'+filter,
            async: false,
            dataType: "json",
            beforeSend: function(){
                $('.stats-item-block.'+type).append('<i class="ion-loader ion-load-c"></i>');
                $('.stats-item-block.'+type+' .card').addClass('loading');
            },
            error: function (request, status, error) {
                $('.stats-item-block.'+type+' .ion-loader').remove();
                $('.stats-item-block.'+type+' .card').removeClass('loading')
            }
        }).done(function( data ) {
            for (var key in data) { 
                var val = data[key];
                $('#'+key).text(val);
            } 
            $('.stats-item-block.'+type+' .ion-loader').remove();
            $('.stats-item-block.'+type+' .card').removeClass('loading');
            stats_start(type);
            if (key == 'disk_percentage'){
                $('.easyPieChart').data('easyPieChart').update(val);
                $('.easyPieChart .percent').text(val+'%');
            }
        });
    }
</script>