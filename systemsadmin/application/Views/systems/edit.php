<div class="card">
  <div class="content">
      <form action="<?php echo URL . 'systems/save'; ?>", class="form-horizontal systems-form" method="POST" id="systems-form" enctype="multipart/form-data">
		<input type="hidden" name="form[id]" value="<?php echo (isset($system->id)) ? $system->id : 0 ; ?>" />
		<fieldset class="fserver">
			<div class="form-header">Account Information</div>
			<div class="errors"></div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="name">System Name:<span class="required">*</span></label>
                <div class="col-sm-10">
				    <input type="text" id="name" name="form[name]" class="form-control requ" value="<?php echo (isset($system->name)) ? $system->name : ''; ?>" />
                </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="url">Landing URL:<span class="required">*</span></label>
                <div class="col-sm-10">
				    <input type="text" style="display:inline-block;" id="url" name="form[url]" class="form-control requ" value="<?php echo (isset($system->url)) ? $system->url : ''; ?>" <?php echo (isset($system->id)) ? 'disabled="disabled"' : '' ?> /><span class="domain-mask"><?php if (!isset($system->id) || (isset($system->id) and $system->other_domain == 0)) : ?><span class="domain-mask-text"><?php echo MAINDOMAIN; ?></span><?php endif; ?><span class="ajax-result ajax-result-url"></span></span>
                </div>
				<?php if (isset($system->id)) : ?>
					<input type="hidden" name="form[url]" value="<?php echo (isset($system->url)) ? $system->url : ''; ?>" />
				<?php endif; ?>
			</div>	
            <div class="form-group" style="display:none; min-height:25px;<?php echo (isset($system->id)) ? 'display:none;' : ''?>" >
				<label class="col-sm-2 control-label" for="other_domain">External Domain:</label>
                <div class="col-sm-10">
                    <label><input type="checkbox" name="form[other_domain]" value="1" id="other_domain" style="margin-top:15px;" <?php echo (isset($system->id) and $system->other_domain > 0) ? 'checked' : ''?> autocomplete="off" /><span></span></label>
                </div>
			</div>
            
			<div class="form-header">TalentQuest System Admin Account</div>
            <div class="form-group">
				<label class="col-sm-2 control-label" for="firstname">First Name:</label>
                <div class="col-sm-10">
				    <input type="text" id="firstname" name="form[firstname]" class="form-control" value="<?php echo (isset($system->firstname)) ? $system->firstname : ''; ?>" />
                </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="lastname">Last Name:</label>
                <div class="col-sm-10">
				    <input type="text" id="lastname" name="form[lastname]" class="form-control" value="<?php echo (isset($system->lastname)) ? $system->lastname : ''; ?>" />
                </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="username">Username:<span class="required">*</span></label>
                <div class="col-sm-10">
				    <input type="text" id="username" name="form[username]" class="form-control requ" value="<?php echo (isset($system->username)) ? $system->username : 'admin'; ?>"  autocomplete="off"/>
                </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="password">Password:<span class="required">*</span></label>
                <div class="col-sm-10">
				    <input type="password" style="display:inline-block;" id="password" name="form[password]" class="form-control password requ" value="<?php echo (isset($system->password)) ? $system->password : ''; ?>" />
                    <span class="ajax-result ajax-result-password"></span>
                </div>
			</div>
            <div class="form-group">
				<label class="col-sm-2 control-label" for="сpassword">Password Confirmation:<span class="required">*</span></label>
                <div class="col-sm-10">
				    <input type="password" style="display:inline-block;" id="cpassword" name="form[cpassword]" class="form-control cpassword requ" value="<?php echo (isset($system->password)) ? $system->password : ''; ?>" />
                    <span class="ajax-result ajax-result-cpassword"></span>
                </div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="email">Confirmation Email:</label>
				<div class="col-sm-10">
                    <input type="text" style="display:inline-block;" id="email" name="form[email]" class="form-control" value="<?php echo (isset($system->email)) ? $system->email : ''; ?>" />  <span class="ajax-result ajax-result-email"></span>
                </div>
			</div>
            
			<div class="form-header">LMS Settings</div>
			<div class="form-group">
				<label class="col-sm-2 control-label" for="backup">Time Zone:</label>
                <div class="col-sm-10">
					<select name="form[zone]" id="zone" class="combobox form-control" >
						<option></option>
						<?php foreach ($zones as $k=>$v) : ?>
							<option value="<?php echo $v; ?>" <?php if (isset($system->zone) and $system->zone == $v) { echo 'selected'; } elseif (!isset($system->zone) and $v == 'America/New_York') {echo 'selected';} else { echo '';}; ?>><?php echo $v; ?></option>
						<?php endforeach; ?>
					</select>
                </div>
			</div>
            <?php if (count($cfields)) : ?>
                <div class="form-header">Additional Information</div>
                <?php foreach ($cfields as $field) : ?>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="<?php echo $field->name; ?>"><?php echo $field->title; ?>:<?php echo ($field->required > 0) ? '<span class="required">*</span>' : ''?></label>
                        <div class="col-sm-10">
                            <?php if ($field->fieldtype == 'text') : ?>
                                <input type="text" id="<?php echo $field->name; ?>" name="form[custom_fields][<?php echo $field->name; ?>]" class="form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" value="<?php echo (isset($cfieldsData[$field->id])) ? $cfieldsData[$field->id] : ''; ?>" placeholder="<?php echo $field->defaultvalue; ?>" />
                            <?php elseif ($field->fieldtype == 'textbox') : ?>
                                <textarea id="<?php echo $field->name; ?>" class="form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" name="form[custom_fields][<?php echo $field->name; ?>]"><?php echo (isset($cfieldsData[$field->id])) ? $cfieldsData[$field->id] : $field->defaultvalue; ?></textarea>
                            <?php elseif ($field->fieldtype == 'dropdown') : ?>
                                <select name="form[custom_fields][<?php echo $field->name; ?>]" id="<?php echo $field->name; ?>" class="form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" >
                                    <option></option>
                                    <?php if ($field->options != '') : ?>
                                        <?php $options = unserialize($field->options); ?>
                                        <?php foreach ($options as $v) : ?>
                                            <option value="<?php echo trim($v); ?>"<?php echo ((isset($cfieldsData[$field->id]) and $cfieldsData[$field->id] == trim($v)) or $field->defaultvalue == trim($v)) ? ' selected' : ''; ?>><?php echo trim($v); ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
							<?php elseif ($field->fieldtype == 'datetime') : ?>
								<input type="text" id="<?php echo $field->name; ?>" name="form[custom_fields][<?php echo $field->name; ?>]" class="datepicker form-control<?php echo ($field->required > 0) ? " requ" : ""; ?>" value="<?php echo (isset($cfieldsData[$field->id])) ? $cfieldsData[$field->id] : ''; ?>" placeholder="<?php echo $field->defaultvalue; ?>" />
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
            <div class="form-group form-actions">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit" name="button" value="Save" class="btn btn-primary" id="btn_save" />
                    <input type="button" name="button" value="Cancel" onclick="window.location='<?php echo URL.'systems';?>'" class="btn btn-default cancel" />
                </div>
            </div>
		</fieldset>
	</form>
  </div>
</div>
<script src="<?php echo URL; ?>assets/js/jquery.maskedinput.min.js"></script>
<script>
	jQuery(".phone").mask("***-***-****");

	jQuery('#other_domain').change(function(e){
		if (jQuery(this).prop('checked')){
			jQuery('.domain-mask-text').hide();
		} else {
			jQuery('.domain-mask-text').show();
		}
	});
    jQuery('#systems-form').submit(function(e){
		var error = false;
		jQuery('#systems-form .form-control').each(function(e){
			if (jQuery(this).hasClass('requ')){
				var value = jQuery(this).val();
				if (value == ''){
					error = true;
					jQuery(this).addClass('req');
				} else {
					jQuery(this).removeClass('req');
				}
			}
		});
		jQuery.ajax({
			type: "POST",
			url: '<?php echo URL; ?>systems/check_data',
			data: jQuery('#systems-form').serialize(),
			dataType: "json",
			async:false,
		}).done(function( data ) {
			jQuery('.errors').html('');
			if (data.status == 'error') {
				error = true;
				console.log(jQuery(".errors").offset().top);
				jQuery('.main').animate({
					scrollTop: jQuery(".errors").offset().top
				}, 1000);
				jQuery.each(data.errors, function (key, value) {
					jQuery('.errors').append('<p class="alert alert-danger">' + value + '</p>');
				})
			}
		});

		if (error){
			e.preventDefault();
		}
	});

	jQuery('.datepicker').datepicker();

	var password = $('#password');
	var cpassword = $('#cpassword');
	var trigger = $('<i class="showhidepass ion-eye"></i>');
	trigger.click(function(){
		if($(this).hasClass('ion-eye')){
			$(this).attr("class","showhidepass ion-eye-disabled");
			$(password).attr("type","text");
		}else{
			$(this).attr("class","showhidepass ion-eye");
			$(password).attr("type","password");
		}
	});
	var ctrigger = $('<i class="showhidepass ion-eye"></i>');
	ctrigger.click(function(){
		if($(this).hasClass('ion-eye')){
			$(this).attr("class","showhidepass ion-eye-disabled");
			$(cpassword).attr("type","text");
		}else{
			$(this).attr("class","showhidepass ion-eye");
			$(cpassword).attr("type","password");
		}
	});
	$(password).after(trigger);
	$(cpassword).after(ctrigger);
</script>
