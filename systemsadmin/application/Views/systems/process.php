<div class="card">
  <div class="content">
      <div class="updating-system-box">
        <?php if ($system->created == 0) :  ?>
            <ul class="d-steps">
                <li id="alias" class="d-steps-item"><span class="item-num">1</span> Creating Domain<span class="status-box"></span></li>
                <li id="db" class="d-steps-item active"><span class="item-num">2</span> Creating Database<span class="status-box"></span></li>
                <li id="files" class="d-steps-item"><span class="item-num">3</span> Generating scripts<span class="status-box"></span></li>
                <li id="user" class="d-steps-item"><span class="item-num">4</span> Creating System Profile<span class="status-box"></span></li>
                <li id="completed" class="d-steps-item completed-item clearfix"><span class="completed-text green">Completed!</span> Now you can login to new LMS <a href="<?php echo ($system->other_domain > 0) ? WWWPROTOCOL.$system->url : WWWPROTOCOL.$system->url.MAINDOMAIN ?>" target="_blank" >here</a> and complete system description.<input type="button" name="Continue" class="btn btn-primary" value="Continue" onclick="location='<?php echo URL;?>systems'" style="float:right; margin:-5px;" /></li>
                <li id="error" class="d-steps-item completed-item"><span class="completed-text red">Error!</span> <span class="error-text"></span></li>
            </ul>
        <?php elseif ($system->created > 0) : ?>
            <ul class="d-steps">
                <li id="user" class="d-steps-item"><span class="item-num">1</span> Updating System Profile<span class="status-box"></span></li>
                <li id="completed" class="d-steps-item completed-item clearfix"><span class="completed-text green">Completed!</span> System successfully updated.<input type="button" name="Continue" class="btn btn-primary" value="Continue" onclick="location='<?php echo URL;?>systems'" style="float:right; margin:-5px;" /></li>
                <li id="error" class="d-steps-item completed-item"><span class="completed-text red">Error!</span> <span class="error-text"></span></li>
            </ul>
        <?php endif; ?>
      </div>
  </div>
</div>
<?php if ($system->created == 0) :  ?>
	<script>
		var error = false;
		jQuery("#alias").addClass("visible");
		if (error == false){
			jQuery.ajax({ //creating alias
				url: '<?php echo URL; ?>systems/sendrequest/create_alias/<?php echo $system->id; ?>',
				dataType: "json",
				beforeSend: function(){
					jQuery("#alias .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
				}
			}).done(function( data ) {
				if (data.status == 1){
					jQuery("#alias").addClass("completed");
					jQuery("#alias .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
					// creating database and importing dump
					jQuery("#db").addClass("visible");
					jQuery.ajax({
						url: '<?php echo URL; ?>systems/sendrequest/create_db/<?php echo $system->id; ?>',
						dataType: "json",
						beforeSend: function(){
							jQuery("#db .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
						}
					}).done(function( data ) {
						if (data.status == 1){
							jQuery("#db").addClass("completed");
							jQuery("#db .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
							// generating scripts
							jQuery("#files").addClass("visible");
							jQuery.ajax({
								url: '<?php echo URL; ?>systems/sendrequest/generate_script/<?php echo $system->id; ?>',
								dataType: "json",
								async:false,
								beforeSend: function(){
									jQuery("#files .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
								}
							}).done(function( data ) {
								if (data.status == 1){
									jQuery("#files").addClass("completed");
									jQuery("#files .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
									// creating profile
									jQuery("#user").addClass("visible");
									jQuery.ajax({
										url: '<?php echo URL; ?>systems/sendrequest/create_profile/<?php echo $system->id; ?>',
										dataType: "json",
										async:false,
										beforeSend: function(){
											jQuery("#user .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
										}
									}).done(function( data ) {
										if (data.status == 1){
											jQuery("#user").addClass("completed");
											jQuery("#user .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
											jQuery("#completed").addClass("visible");
                                            sendProcessMessage(<?php echo $system->id ;?>, 'success', 4);
										} else {
											jQuery("#user").addClass("error");
											jQuery("#user .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
											error = data.error;
											jQuery("#error").addClass("visible");
											jQuery("#error .completed-text").text(error);
                                            sendProcessMessage(<?php echo $system->id ;?>, 'error', 4);
										}
									});
									// end creating profile
								} else {
									jQuery("#files").addClass("error");
									jQuery("#files .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
									error = data.error;
									jQuery("#error").addClass("visible");
									jQuery("#error .completed-text").text(error);
                                    sendProcessMessage(<?php echo $system->id ;?>, 'error', 3);
								}
							});
							// end generating scripts
						} else {
							jQuery("#db").addClass("error");
							jQuery("#db .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
							error = data.error;
							jQuery("#error").addClass("visible");
							jQuery("#error .completed-text").text(error);
                            sendProcessMessage(<?php echo $system->id ;?>, 'error', 2);
						}
					});
					// end creating database and importing dump
				} else {
					jQuery("#alias").addClass("error");
					jQuery("#alias .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
					error = data.error;
					jQuery("#error").addClass("visible");
					jQuery("#error .completed-text").text(error);
                    sendProcessMessage(<?php echo $system->id ;?>, 'error', 1);
				}
			});
		}
	</script>
<?php elseif ($system->created > 0) : ?>
	<script>
		var error = false;
		if (error == false){
			jQuery("#user").addClass("visible");
			jQuery.ajax({
				url: '<?php echo URL; ?>systems/sendrequest/update_profile/<?php echo $system->id; ?>',
				dataType: "json",
				beforeSend: function(){
					jQuery("#user .status-box").html("<i class=\"ion-loader ion-load-c\"></i>");
				}
			}).done(function( data ) {
				if (data.status == 1){
					jQuery("#user").addClass("completed");
					jQuery("#user .status-box").html("<i class=\"ion-ios-checkmark-outline green\"></i>");
					jQuery("#completed").addClass("visible");
				} else {
					jQuery("#user").addClass("error");
					jQuery("#user .status-box").html("<i class=\"ion-ios-close-outline red\"></i>");
					error = data.error;
					jQuery("#error").addClass("visible");
					jQuery("#error .completed-text").text(error);
				}
			});
		}
		
	</script>
<?php endif; ?>
<script>
    function sendProcessMessage(id, status, step){        
        jQuery.ajax({
            url: '<?php echo URL; ?>systems/sendconfirmation/'+id+'/'+status+'/'+step,
            dataType: "json",
        });
    }
</script>

