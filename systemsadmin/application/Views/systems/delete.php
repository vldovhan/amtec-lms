<div class="card">
<div class="header">
<legend>Delete <?php echo $system->name; ?>?</legend>
</div>
<div class="content">
    <?php if ($system->options['candelete']) : ?>
        <p>Are you sure you want to delete the instance?</p>
        <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'systems/delete/'.$system->id)); ?>
              <?php echo Form::hidden(array('name'=>'confirm', 'value'=> 1)); ?>
              <?php echo Form::button(array('value'=>'Delete', 'class'=>'btn btn-primary'),false); ?>
              <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'systems"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
        <?php echo Form::close(); ?>
    <?php else : ?>
        <?php if ($system->state > 0) : ?>
            <p>This instance has <strong><?php echo $system->options['users']; ?></strong> users and <strong><?php echo $system->options['courses']; ?></strong> courses. You can only disable it.</p>
            <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'systems/disable/'.$system->id)); ?>
                  <?php echo Form::button(array('value'=>'Disable', 'class'=>'btn btn-primary'),false); ?>
                  <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'systems"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
            <?php echo Form::close(); ?>
        <?php else : ?>
            <p>This instance has <strong><?php echo $system->options['users']; ?></strong> users and <strong><?php echo $system->options['courses']; ?></strong> courses.</p>
            <?php echo Form::open(array('method'=>'POST', 'action'=> URL . 'systems/enable/'.$system->id)); ?>
                  <?php echo Form::button(array('value'=>'Enable', 'class'=>'btn btn-primary'),false); ?>
                  <?php echo Form::button(array('value'=>'Cancel','onclick'=>'window.location="'.URL.'systems"','type'=>'button', 'class'=>'btn btn-default'),false); ?>
            <?php echo Form::close(); ?>
        <?php endif; ?>
    <?php endif; ?>
</div>
</div>
