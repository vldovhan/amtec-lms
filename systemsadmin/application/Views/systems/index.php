<div class="card">
  <div class="content table-responsive table-full-width datatable">
    <table class="table table-striped">
      <thead>
        <tr>
          <th>Instance Name</th>
          <th>Url</th>
          <th align="center">Status</th>
          <th align="center">Date created</th>
          <th align="center" align="center">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php if (count($systems)) : ?>
            <?php foreach($systems as $system): ?>
            <tr>
              <td><?php echo $system->name ?></td>
              <td><a href="<?php echo URL_PROTOCOL.$system->url.MAINDOMAIN ?>" target="_blank"><?php echo URL_PROTOCOL.$system->url.MAINDOMAIN ?></a></td>
              <td align="center">
                <?php if($system->state): ?>
                  <i title="Enabled" class="ion-ios-circle-filled"></i>
                <?php else: ?>
                  <i title="Disable" class="ion-ios-circle-outline"></i>
                <?php endif; ?>
              </td>
              <td align="center"><?php echo date("m/d/Y", $system->timecreated); ?></td>
              <td align="center">
                  <?php if($system->state): ?>
                    <a href="<?php echo URL; ?>systems/disable/<?php echo $system->id; ?>" class="green"><i title="Inactive" class="ion-eye"></i></a>
                  <?php else: ?>
                      <a href="<?php echo URL; ?>systems/enable/<?php echo $system->id; ?>" class="green"><i title="Active" class="ion-eye-disabled"></i></a>
                  <?php endif; ?>
                  <a href="<?php echo URL; ?>systems/edit/<?php echo $system->id; ?>"><i title="Edit" class="ion-ios-compose-outline"></i></a>
                  <a href="<?php echo URL; ?>systems/delete/<?php echo $system->id; ?>"><i title="Delete" class="ion-ios-trash-outline"></i></a>
              </td>
            </tr>
          <?php endforeach; ?>
      <?php endif; ?>
      </tbody>
    </table>
  </div>
</div>
<script>
$(document).ready(function(){
    $('.table').DataTable({
      "bAutoWidth": false,
			"dom": '<"toolbar">frtip',
			"sPaginationType": "full_numbers",
      "bLengthChange": false
    });
    $("div.toolbar").html('<a class="btn btn-success btn-fill btn-wd" href="<?php echo URL; ?>systems/add"><i class="ion-ios-albums-outline"></i> Create Instance</a>');
});
</script>
