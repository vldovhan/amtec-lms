<?php

class Customfields extends Controller
{
    public $fieldstypes = array('text'=>'Text', 'textbox'=>'Textbox', 'dropdown'=>'Dropdown', 'datetime' => 'Date');
    
    public function index()
    {
      $cfields = $this->loadModel('Customfields')->getCustomFields();
      $this->view->set('cfields', $cfields);
      $this->view->heading("Custom Fields");
      $this->view->set('fieldstypes', $this->fieldstypes);
      $this->view->render();
    }
    public function add()
    {
      $this->view->heading("Add Custom Field");
      $this->view->set("cfield", null);
      $this->view->render('edit');
    }
    public function edit($id)
    {
      $cfieldsModel = $this->loadModel('Customfields');
      $cfield = $cfieldsModel->getCustomField(array('id'=>$id));
      if(!$cfield){
        $this->redirect("customfields/", array("danger"=>"Invalid ID"));
      }
      
      if ($cfield->fieldtype == 'dropdown'){
          $options = unserialize($cfield->options);
          $cfield->options = implode("", $options);
      } else {
          $cfield->options = '';
      }
      
      $this->view->heading("Edit Custom Field");
      $this->view->set("cfield", $cfield);
      $this->view->render('edit');
    }
    public function delete($id)
    {
      $confirm = Request::post('confirm');
      $cfieldsModel = $this->loadModel('Customfields');

      if($id and $confirm){
        if($cfield = $cfieldsModel->getCustomField(array('id'=>$id))){
          $cfieldsModel->deleteCustomField($cfield->id);
          $cfieldsModel->deleteCustomData($cfield->id);

          $this->redirect("customfields/", array("success"=>"The Custom Field was successfully deleted"));
        }else{
          $this->redirect("customfields/", array("danger"=>"Invalid ID"));
        }
      }
      $cfield = $cfieldsModel->getCustomField(array('id'=>$id));
      $this->view->heading("Delete Custom Field");
      $this->view->set("cfield", $cfield);
      $this->view->render('delete');
    }

    public function save()
    {
      $form = Request::post('form', 'object');

      if($form->name and $form->title and $form->fieldtype){
        $cfieldsModel = $this->loadModel('Customfields');
          
        if ($form->fieldtype == 'dropdown'){
            $options = explode("\n", trim($form->options));
            $form->options = serialize($options);
        } else {
            $form->options = '';
        }
        $form->name = strtolower(str_replace(' ', '_', trim($form->name)));
        
        if(isset($form->id) and $form->id){

          if($cfield = $cfieldsModel->getCustomField(array('id'=>$form->id))){
            
            $cfieldsModel->updateCustomField($form, array('id'=>$form->id));
            $this->redirect("customfields/", array("success"=>"Custom Field was successfully saved"));
          }else{
            $this->redirect("customfields/edit/$form->id", array("danger"=>"Invalid ID"));
          }
              
        } else {
          
          $existed = $cfieldsModel->getCustomField(array("name"=>$form->name));

          if(!empty($existed)){
            $this->redirect("customfields/add", array("danger"=>"Custom Field Exist"));
          }
          
          $form->id = $cfieldsModel->insertCustomField($form, 'custom_fields');

          if($form->id){
            $this->redirect("customfields/", array("success"=>"New Custom Field was successfully created"));
          }else{
            $this->redirect("customfields/add", array("danger"=>"Erorr code 102"));
          }
        }
      }else{
        $this->redirect("customfields/add", array("danger"=>"Erorr code 101"));
      }
    }
    public function enable($id)
    {
      $cfield = $this->loadModel('Customfields')->getCustomField(array('id'=>$id));
      if(!$cfield){
        $this->redirect("customfields", array("danger"=>"Invalid ID"));
      }
      $cfield->state = 1;
      $this->loadModel('Customfields')->updateCustomField($cfield, array('id'=>$cfield->id));
      $this->redirect("customfields", array("success"=>"The Custom Field was successfully enabled"));
    }
    public function disable($id)
    {
      $cfield = $this->loadModel('Customfields')->getCustomField(array('id'=>$id));
      if(!$cfield){
        $this->redirect("customfields", array("danger"=>"Invalid ID"));
      }
      $cfield->state = 0;
      $this->loadModel('Customfields')->updateCustomField($cfield, array('id'=>$cfield->id));
      $this->redirect("customfields", array("success"=>"The Custom Field was successfully disabled"));
    }
}
