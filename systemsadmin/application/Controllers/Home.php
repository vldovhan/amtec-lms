<?php

class Home extends Controller
{
    public function index()
    {
        $model = $this->loadModel('Site');
        $this->view->heading("Dashboard");
        
        $systems = $this->loadModel('Systems')->getSystems($this->user->id);
        $this->view->set('systems', $systems);
        
        $dashboardFilter = (Session::get('dashboardFilter')) ? Session::get('dashboardFilter') : 0;
        $this->view->set('dashboardFilter', $dashboardFilter);
        $statsSystems = (Session::get('statsSystems')) ? Session::get('statsSystems') : $model->getStatsSystems($this->user);
        $this->view->set('statsSystems', $statsSystems);
        $statsUsers = (Session::get('statsUsers')) ? Session::get('statsUsers') : $model->getStatsUsers($this->user);
        $this->view->set('statsUsers', $statsUsers);
        $statsCourses = (Session::get('statsCourses')) ? Session::get('statsCourses') : $model->getStatsCourses($this->user);
        $this->view->set('statsCourses', $statsCourses);
        $statsSpace = (Session::get('statsSpace')) ? Session::get('statsSpace') : $model->getStatsSpace($this->user);
        $this->view->set('statsSpace', $statsSpace);
        $this->view->render();
    }
    
    public function refreshstats($type, $filter = 0)
    {
        Session::set('dashboardFilter', $filter);
        $model = $this->loadModel('Site');
        
        $stats = $model->$type($this->user);
        echo json_encode($stats);
        exit;
    }
    
    public function search()
    {
      $keyword = Request::query('keyword');
      $results = $this->loadModel('Site')->search($this->user->id, $keyword);
      $this->view->set('keyword', $keyword);
      $this->view->set('results', $results);
      $this->view->heading("Search");
      $this->view->render('search');
    }
}
