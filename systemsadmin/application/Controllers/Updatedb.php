<?php

class Updatedb extends Controller
{
    public function index()
    {
      $systems = $this->loadModel('Systems')->getSystems($this->user->id);
      $this->view->set("systems", $systems);
      $this->view->heading("Update LMS Databases");
      $this->view->render();
    }
    
    public function process()
    {
      $systems = $this->loadModel('Systems')->getSystems($this->user->id);
      $this->view->set("systems", $systems);
      $this->view->heading("Update LMS Databases");
      $this->view->render('process');
    }
}
