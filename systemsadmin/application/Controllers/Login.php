<?php

class Login extends Controller
{
    public function index()
    {
      $email = Request::post("email");
      $password = Request::post("password");

      if($email and $password){
          $user = $this->loadModel('Users')->findUser(array('email'=>$email, 'password'=>$password));
          if ($user){
              Session::set('user', $user);
              $this->redirect("", array("success"=>"Welcome to Systems admin panel!"));
          } else {
              Session::set('user', 'null');
              $this->redirect("login/", array("danger"=>"Wrong Email or Password!"));
          }
      }

      $this->view->layout('login');
      $this->view->render();
    }
    
    public function logout()
    {
      Session::set('user', 'null');
      $this->redirect("login");
    }

    public function reset()
	{
		$reset = Request::post('reset');
		$email = Request::post('email');
		$email_sent = false;
		$error = false;

		if ($reset) {
			$user = $this->loadModel('Users')->getUser(array('email'=>$email));
			if ($user){
				if (Mail::sendResetMail($user)) {
					$email_sent = true;
				}
				else {
					$error = "Password was not reset. Try again later";
				}
			} else {
				$error = "There is no user with this email in database";
			}
		}

		$this->view->set("error", $error);
		$this->view->set("email_sent", $email_sent);
		$this->view->layout('login');
		$this->view->render('reset');
	}
    
}
