<?php

class Settings extends Controller
{
    public $settings    = null;
    
    public function index()
    {
      $this->view->heading("Settings");
      $this->view->render();
    }
    public function preferences()
    {
      $settings = (isset($this->settings)) ? $this->settings : array();
      
      $this->view->set('settings', $settings);
      $this->view->heading("Preferences");
      $this->view->render('preferences');
    }
    public function save()
    {
      $model = $this->loadModel('Settings');
      $settings = Request::post('form', 'object');
        
      foreach ($settings as $key=>$val){
          $setting = $model->getSetting(array('name'=>$key));
          if ($setting and $setting->value != $val){
              $setting->value = $val;
              $model->updateSetting($setting, array('id'=>$setting->id));        
          }
      }
      $this->redirect("settings", array("success"=>"Settings was successfully updated"));
    }
    public function getSettings()
    {
      if (count($this->settings) > 0) return $this->settings;
        
      $settings = array();
      $model = $this->loadModel('Settings');
      $db_settings = $model->getSettings();
      if (count($db_settings)){
          foreach ($db_settings as $setting){
            $settings[$setting->name] = $setting;
          }
      }
      return $settings;
    }
}
