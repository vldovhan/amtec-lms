<?php

class Backups extends Controller
{
    public function index()
    {
      $backups = $this->loadModel('Backups')->getBackups($this->user->id);
      $this->view->set("backups", $backups);
      $this->view->heading("Backups");
      $this->view->render();
    }
    public function details($id)
    {
      $backups = $this->loadModel('Backups')->getSystemBackups(array('systemid'=>$id));
      $system = $this->loadModel('Systems')->getSystem(array('id'=>$id));
      if(!$system){
        $this->redirect("backups", array("danger"=>"Invalid system id"));
      }
      $this->view->heading("Backups Details - " . $system->name);
      $this->view->set("backups", $backups);
      $this->view->set("system", $system);
      $this->view->render('details');
    }
    public function delete($id)
    {
      $confirm = Request::post('confirm');
      $backupModel = $this->loadModel('Backups');
      $backup = $backupModel->getBackup(array('id'=>$id));
      $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
      if($id and $confirm){
        if($backup and $system){  
          $domain = str_replace('.', '_', $system->url);
          $md_dir = BACKUPS_DIR.'moodledata/'.$domain."/".$domain.'_'.$backup->timecreated.'.zip';
		  $db_dir = BACKUPS_DIR.'database/'.$domain."/".$domain.'_'.$backup->timecreated.'.sql';
            
          if (is_file($md_dir)){
              File::deleteFile($md_dir);
          }
          if (is_file($db_dir)){
              File::deleteFile($db_dir);
          }
          $backupModel->deleteBackup($backup->id);
            
          $this->redirect("backups/details/".$system->id, array("success"=>"The backup was successfully deleted"));
        }else{
          $this->redirect("backups", array("danger"=>"Invalid backup id"));
        }
      }
      
      $this->view->heading("Delete Backup");
      $this->view->set("backup", $backup);
      $this->view->set("system", $system);
      $this->view->render('delete');
    }
    public function backup($id)
    {
      $system = $this->loadModel('Systems')->getSystem(array('id'=>$id));
      if(!$system){
        $this->redirect("backups", array("danger"=>"Invalid systems id"));
      }
      $backupModel = $this->loadModel('Backups');
      
      $backup = new stdClass();
      $backup->systemid = $system->id;
      $backup->btype = 'restore point';
      $backup->timecreated = time();
      $backup->id = $backupModel->insertBackup($backup);
      
      $this->view->set("backup", $backup);
      $this->view->set("system", $system);
      $this->view->render('backup');
    }   
    public function create_db_dump($id){
        $backup = $this->loadModel('Backups')->getBackup(array('id'=>$id));
        if ($backup->id){
            $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
            $result = $this->export_db($system->url, $backup->timecreated);
            
            $status = 1;$error = false;
            if (!$result){
                $status = 0;$error = 'Error in creating Database dump.';
            }
            $result = array('status'=>$status, 'error'=>$error);
            echo json_encode($result);
        }
        exit;
    }
    public function export_db($domain, $time) {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
		$domain = str_replace('.', '_', $domain);
        
        if (!is_dir(BACKUPS_DIR.'database/'.$domain)){
			$res = mkdir(BACKUPS_DIR.'database/'.$domain);
		}
		$sqlname = BACKUPS_DIR.'database/'.$domain.'/'.$domain.'_'.$time.'.sql';
        
        /*if (EXECTYPE == 'shell'){*/
            $command = "mysqldump -u".DB_ROOTUSER." -p'".DB_ROOTPASS."' ".$domain." > ".$sqlname;
            $result = exec($command, $output);
        /*} else {
            $this->db_connect($this->_dbhost, $this->_dbrootuser, $this->_dbrootpass, $domain);
            require_once('MySQLDump.php');
		
            $dumper = new MySQLDump($domain, $sqlname, false, false);

            if (!$dumper){
                return false;
            }
            //Make dump
            $dumper->doDump(array());
            $this->db_connect();
        }*/
        
		return true;
	}
    public function create_md_dump($id){
        $backup = $this->loadModel('Backups')->getBackup(array('id'=>$id));
        if ($backup->id){
            $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
            
            $this->export_md($system->url, $backup->timecreated);
            $status = 1; $error = false;
            $result = array('status'=>$status, 'error'=>$error);
            echo json_encode($result);
        }
        exit;
    }
    public function export_md($domain, $time) {
        set_time_limit(0);
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '1024M');
        ini_set('display_errors', 'on');
        
		$domain = str_replace('.', '_', $domain);
		if (!is_dir(BACKUPS_DIR.'moodledata/'.$domain)){
			$res = mkdir(BACKUPS_DIR.'moodledata/'.$domain);
		}
		$source_dir = MOODLEDATA_DIR.$domain."/";
		$zip_file = BACKUPS_DIR.'moodledata/'.$domain.'/'.$domain.'_'.$time.'.zip';
		
        if (EXECTYPE == 'shell'){
            $command = "cd ".$source_dir.";zip -rq ".$zip_file." * ";
            $result = exec($command, $output);
        } else {
            $file_list = $this->listDirectory($source_dir);
            $zip = new ZipArchive();
            if ($zip->open($zip_file, ZIPARCHIVE::CREATE) === true) {
                foreach ($file_list as $file) {
                    if ($file !== $zip_file) {
                        $zip->addFile($file, substr($file, strlen($source_dir)));
                    }
                }

                $zip->close();
            }
        }
        
		return true;
	}
    public function restore($id)
    {
      $confirm = Request::post('confirm');
      $backupModel = $this->loadModel('Backups');
      $backup = $backupModel->getBackup(array('id'=>$id));
      if(!$backup){
        $this->redirect("backups", array("danger"=>"Invalid backup id"));
      }
      $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
      if($id and $confirm){
          $newbackup = new stdClass();
          $newbackup->systemid = $system->id;
          $newbackup->btype = 'restore point';
          $newbackup->timecreated = time();
          $newbackup->id = $backupModel->insertBackup($newbackup);
          
          $backup->restored = time();
          $backupModel->updateBackup($backup, array('id'=>$backup->id));

          $this->view->set("backup", $backup);
          $this->view->set("newbackup", $newbackup);
          $this->view->set("system", $system);
          $this->view->render('restore');
      }
      
      $this->view->heading("Restore Backup");
      $this->view->set("backup", $backup);
      $this->view->set("system", $system);
      $this->view->render('confirmrestore');
    }   
    public function restore_db_dump($id){
        $backup = $this->loadModel('Backups')->getBackup(array('id'=>$id));
        if ($backup->id){
            $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
            $result = $this->import_db($backup, $system);
            
            $status = 1;$error = false;
            if (!$result){
                $status = 0;$error = 'Error in restoring Database dump.';
            }
            $result = array('status'=>$status, 'error'=>$error);
            echo json_encode($result);
        }
        exit;
    }
    public function import_db($backup, $system) {
		$domain = str_replace('.', '_', $system->url);
		if (EXECTYPE == 'shell'){
            $sqlname = BACKUPS_DIR.'database/'.$domain.'/'.$domain.'_'.$backup->timecreated.'.sql';
            $command = "mysql -u".DB_ROOTUSER." -p'".DB_ROOTPASS."' ".$domain." < ".$sqlname;
            $result = exec($command, $output);
            $return = true;
        } else {
            //$this->db_connect($this->_dbhost, $this->_dbrootuser, $this->_dbrootpass, $domain);
            $templine = '';
            $lines = file(BACKUPS_DIR.'database/'.$domain.'/'.$domain.'_'.$backup->timecreated.'.sql');
            foreach ($lines as $line){
                if (substr($line, 0, 2) == '--' || $line == '')
                    continue;

                $templine .= $line;
                if (substr(trim($line), -1, 1) == ';'){
                    mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
                    $templine = '';
                }
            }
            //$this->db_connect();
            if (!$lines){
                $return = false;
            } else {
                $return = true;
            }
        }
		return $return;
	}
    public function restore_md($id){
        $backup = $this->loadModel('Backups')->getBackup(array('id'=>$id));
        if ($backup->id){
            $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
            $result = $this->import_md($backup, $system);
            
            $status = 1;$error = false;
            if (!$result){
                $status = 0;$error = 'Error in restoring Data.';
            }
            $result = array('status'=>$status, 'error'=>$error);
            echo json_encode($result);
        }
        exit;
    }
    public function import_md($backup, $system){
		$domain = str_replace('.', '_', $system->url);
		
		$tempdir = 'temp_'.time();
		$ZipFileName = BACKUPS_DIR.'moodledata/'.$domain.'/'.$domain.'_'.$backup->timecreated.'.zip';
		$home_folder = BACKUPS_DIR.'temp/'.$tempdir.'/';
        
        if (EXECTYPE == 'shell'){
            $dest_dir = MOODLEDATA_DIR.$domain;
            $command = "rm -r $dest_dir;mkdir $dest_dir;unzip $ZipFileName -d $dest_dir/";
            $result = exec($command, $output);
            $return = true;
        } else {
            mkdir($home_folder);
            $zip = new ZipArchive;
            $res = $zip->open($ZipFileName); $return = true;
            if ($res === TRUE) {
                $zip->extractTo($home_folder);
                $zip->close();
                $restore = $this->copy_md($domain, $tempdir);
              if (!$restore){
                $return = false;
              }
            } else {
              $return = false;		  
            }
        }
        
		return $return;
	}
    public function copy_md($domain, $tempdir) {
		$domain = str_replace('.', '_', $domain);
		
		$source_dir = BACKUPS_DIR.'temp/'.$tempdir.'/';
		$dest_dir = MOODLEDATA_DIR.$domain."/";
		File::deleteDir($dest_dir);
		mkdir($dest_dir);
		if (is_dir($dest_dir)){
			$res = File::Copy($source_dir, $dest_dir);
			if (!$res){
				$return = false;
			} else {
				File::deleteDir($source_dir);
				$return = true;
			}
		} else {
			$return = false;
		}
		return $return;
	}
    public function download($type, $id)
    {
      $backup = $this->loadModel('Backups')->getBackup(array('id'=>$id));
      if(!$backup){
        $this->redirect("backups", array("danger"=>"Invalid backup id"));
      }
      $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
      $domain = str_replace('.', '_', $system->url);
      
      $filename = '';
      if ($type == 'database'){
          $filename = BACKUPS_DIR.'database/'.$domain.'/'.$domain.'_'.$backup->timecreated.'.sql';
      } elseif ($type == 'moodledata'){
          $filename = BACKUPS_DIR.'moodledata/'.$domain.'/'.$domain.'_'.$backup->timecreated.'.zip';
      }
      
      File::fileDownload($filename);
      exit;
    }
    
    public function clear_backups($systemid, $domain, $btype) {
		$domain = str_replace('.', '_', $domain);
		
        $backups = $this->loadModel('Backups')->getBackupsByAttr(array('systemid'=>$systemid, 'btype'=>$btype), 'ORDER BY timecreated DESC');
        
		if (count($backups) > 2){
			$i = 0;
			foreach ($backups as $item){
				$i++; if ($i <= 2) continue;
				$result = $this->delete_bakup($item->id);
			}
		}
		
		return true;
	}
    
    public function delete_bakup($id)
    {
      $backupModel = $this->loadModel('Backups');
      $backup = $backupModel->getBackup(array('id'=>$id));
      $system = $this->loadModel('Systems')->getSystem(array('id'=>$backup->systemid));
      
        if($backup and $system){  
          $domain = str_replace('.', '_', $system->url);
          $md_dir = BACKUPS_DIR.'moodledata/'.$domain."/".$domain.'_'.$backup->timecreated.'.zip';
		  $db_dir = BACKUPS_DIR.'database/'.$domain."/".$domain.'_'.$backup->timecreated.'.sql';
            
          if (is_file($md_dir)){
              File::deleteFile($md_dir);
          }
          if (is_file($db_dir)){
              File::deleteFile($db_dir);
          }
          $backupModel->deleteBackup($backup->id);
        }
    }
    
    public function cron()
    {
        ini_set('max_execution_time', 5000);
        
        $systems = $this->loadModel('Systems')->getSystemsAttr(array('backup'=>1, 'state'=>1));
        $backupModel = $this->loadModel('Backups');
        
        if(count($systems)){
            echo '<hr><hr>Backups:<hr>';
            foreach ($systems as $system){
                $process = true; $today = date('j'); $weekday = date('w');

                if ($today == 1){
                    $btype = 'monthly';
                } elseif ($weekday == 0){
                    $btype = 'weekly';
                } else {
                    $btype = 'daily';
                }

                if ($process){
                    echo $system->name.': Start process...';
                    $result0 = $this->clear_backups($system->id, $system->url, $btype);

                    $backup = new stdClass();
                    $backup->systemid = $system->id;
                    $backup->btype = $btype;
                    $backup->timecreated = time();
                    $backup->id = $backupModel->insertBackup($backup);

                    $result1 = $this->export_db($system->url, $backup->timecreated);
                    $result2 = $this->export_md($system->url, $backup->timecreated);
                    echo ' ------------ End process.<hr />';
                }
            }
        }
    }
}
