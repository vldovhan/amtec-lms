<?php

class Users extends Controller
{

	public function index()
	{
		$usersModel = $this->loadModel('Users');
		$this->view->set('system_admin_role', $usersModel::SYSTEM_ADMIN);
		$current_user = $this->user;
		$current_user->issystemadmin = $usersModel->isSystemAdmin($current_user->role);
		$this->view->set('current_user', $current_user);
		$users = $usersModel->getUsers($this->user->id);
		$this->view->set('users', $users);
		$this->view->heading("Tenant Management Administrators");
		$this->view->render();
	}

	public function add()
	{
		$usersModel = $this->loadModel('Users');
		$cfields = $this->loadModel('Customfields')->getCustomFields(array('type' => 'user'));
		$this->view->heading("Add Administrator");
		$this->view->set("user", null);
		$this->view->set("issystemadmin", $usersModel->isSystemAdmin($this->user->role));
		$this->view->set("cfields", $cfields);
		$this->view->render('edit');
	}

	public function systems($id)
	{
		$usersModel = $this->loadModel('Users');
		$user = $usersModel->getUser(array('id' => $id));
		$systems = $usersModel->getUserSystems(array('id' => $id, 'userid' => $this->user->id));

		$this->view->heading("Moodle connections");
		$this->view->set("systems", $systems);
		$this->view->set("user", $user);
		$this->view->render('systems');
	}

	public function edit($id)
	{
		$usersModel = $this->loadModel('Users');
		$user = $usersModel->getUser(array('id' => $id));

		if (!$user) {
			$this->redirect("users/", array("danger" => "Invalid user id"));
		}
		$user->issystemadmin = $usersModel->isSystemAdmin($user->role);
		$cfields = $this->loadModel('Customfields')->getCustomFields(array('type' => 'user'));
		$cfieldsData = $this->loadModel('Customfields')->getFieldsData('user', $user->id);

		$this->view->heading("Edit Administrator - " . $user->firstname . ' ' . $user->lastname);
		$this->view->set("user", $user);
		$this->view->set("issystemadmin", $usersModel->isSystemAdmin($this->user->role));
		$this->view->set("cfields", $cfields);
		$this->view->set("cfieldsData", $cfieldsData);
		$this->view->render('edit');
	}

	public function delete($id)
	{
		$confirm = Request::post('confirm');
		$usersModel = $this->loadModel('Users');

		if ($id and $confirm) {
			if ($user = $usersModel->getUser(array('id' => $id))) {
				if ($this->user->id != $user->id && !(!$usersModel->isSystemAdmin($this->user->role) && $user->role == $usersModel::SYSTEM_ADMIN)) {
					$this->loadModel('Customfields')->deleteFieldsData('user', $id);
					$usersModel->deleteUser($user->id);
					$this->redirect("users/", array("success" => "The user was successfully deleted"));
				} else {
					$this->redirect("users/", array("danger" => "You can not delete this user"));
				}
			} else {
				$this->redirect("users/", array("danger" => "Invalid USERID"));
			}
		}
		$user = $usersModel->getUser(array('id' => $id));
		$this->view->heading("Delete user");
		$this->view->set("user", $user);
		$this->view->render('delete');
	}

	public function save()
	{
		$form = Request::post('form', 'object');

		if ($form->firstname and $form->lastname) {
			$usersModel = $this->loadModel('Users');

			$is_system_admin = $usersModel->isSystemAdmin($this->user->role);
			$words = [$form->firstname, $form->lastname];

			$form->role = $usersModel::TENANT_ADMIN;
			if (isset($form->issystemadmin)) {
				if ($form->issystemadmin == 1 && $is_system_admin) {
					$form->role = $usersModel::SYSTEM_ADMIN;
				}
				unset($form->issystemadmin);
			}

			$cfields = array();
			if (isset($form->custom_fields)) {
				$cfields = $form->custom_fields;
				unset($form->custom_fields);
			}

			if (isset($form->id) and $form->id) {

				if ($user = $usersModel->getUser(array('id' => $form->id))) {

					if ($form->password && Core::validatePassword($form->password, $words)) {
						if ($form->password == $form->cpassword) {
							unset($form->cpassword);
							$form->passwordClear = $form->password;
							$form->password = md5($form->password);
						}
						else {
							$this->redirect("users/edit/$form->id", array("danger" => "Passwords do not match"));
						}
					} else {
						$this->redirect("users/edit/$form->id", array("danger" => Core::invalidPasswordMessage()));
					}

					$usersModel->updateUser($form, array('id' => $form->id));
					$this->loadModel('Customfields')->processCustomFields('user', $cfields, $form->id);
					$this->redirect("users/", array("success" => "User profile was successfully saved"));
				} else {
					$this->redirect("users/edit/$form->id", array("danger" => "Invalid user id"));
				}

			} elseif ($form->email) {

				unset($form->id);
				$existed = $usersModel->getUser(array("email" => $form->email));

				if (!empty($existed)) {
					$this->redirect("users/add", array("danger" => "User Exist"));
				}

				if ($form->password && Core::validatePassword($form->password, $words)) {
					if ($form->password == $form->cpassword) {
						unset($form->cpassword);
						$form->passwordClear = $form->password;
						$form->password = md5($form->password);
					}
					else {
						$this->redirect("users/edit/$form->id", array("danger" => "Passwords do not match"));
					}
				} else {
					$this->redirect("users/add", array("danger" => Core::invalidPasswordMessage()));
				}

				$form->registerDate = date('Y-m-d H:i:s');

				$form->id = $usersModel->insertUser($form, 'users');
				$this->loadModel('Customfields')->processCustomFields('user', $cfields, $form->id);

				if ($form->id) {
					$this->redirect("users/", array("success" => "New user was successfully created"));
				} else {
					$this->redirect("users/add", array("danger" => "Erorr code 102"));
				}
			}
		} else {
			$this->redirect("users/add", array("danger" => "Erorr code 101"));
		}
	}

	public function check_data()
	{
		$status = 'success';
		$form = Request::post('form', 'object');
		$errors = [];


		if (empty($form->email) || !filter_var($form->email, FILTER_VALIDATE_EMAIL)) {
			$status = 'error';
			$errors[] = 'Email is incorrect';
		}

		if (empty($form->firstname)) {
			$status = 'error';
			$errors[] = 'Firstname is empty';
		}

		if (empty($form->lastname)) {
			$status = 'error';
			$errors[] = 'Lastname is empty';
		}

		$words = [$form->firstname, $form->lastname];
		if (empty($form->password) || !Core::validatePassword($form->password, $words)) {
			$status = 'error';
			$errors[] = Core::invalidPasswordMessage();
		}

		if ($form->password != $form->cpassword) {
			$status = 'error';
			$errors[] = 'Passwords do not match';
		}

		$result = [
			'status' => $status,
			'form' => $form,
			'errors' => $errors
		];

		die(json_encode($result));
	}

	public function reset($id)
	{
		$user = $this->loadModel('Users')->getUser(array('id'=>$id));

		if ($user){
			if (Mail::sendResetMail($user)) {
				$this->redirect("users/", array("success" => "New password was sent to user mail"));
			}
			else {
				$this->redirect("users/", array("danger" => "Password was not reset. Try again later"));
			}
		} else {
			$this->redirect("users/", array("danger" => "Cannot find user"));
		}
	}
}
