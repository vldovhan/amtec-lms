<?php

class Reports extends Controller
{
    public function index()
    {
      $this->view->heading("Reports");
      $this->view->render();
    }
    
    public function billing()
    {
        
      $systems = $this->loadModel('Systems')->getSystems($this->user->id);
      $data = $this->loadModel('Reports')->getBillingReport($systems, $this->user->id);
      $this->view->set("data", $data);
      $this->view->heading("Billing Report");
      $this->view->render('billing');
    }
    
    public function billingdetails($id = 0)
    {
      $system = $this->loadModel('Systems')->getSystem(array('id' => $id));

		//Core::p($system, false);
      
      $this->view->set("system", $system);
      $this->view->heading("Billing Details - ".$system->name);
      $this->view->render('billingdetails');
    }
    
    public function reportdata($report, $id = 0)
    {
        $system = $this->loadModel('Systems')->getSystem(array('id' => $id));
        $params = array();
        
        $params['iDisplayStart'] = (Request::query('iDisplayStart')) ? Request::query('iDisplayStart') : 0;
        $params['iDisplayLength'] = (Request::query('iDisplayLength')) ? Request::query('iDisplayLength') : 15;
        $params['iSortingCols'] = (Request::query('iSortingCols')) ? Request::query('iSortingCols') : '';
        $params['sSearch'] = (Request::query('sSearch')) ? Request::query('sSearch') : '';
        $params['sEcho'] = (Request::query('sEcho')) ? Request::query('sEcho') : 0;
        
      
        $data = array();
        if ($report == 'billing'){
            $data = $this->loadModel('Reports')->getBillingDetails($system, $params);
        }
      
      echo json_encode( $data );
    }
}
