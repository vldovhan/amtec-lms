<?php

class Profile extends Controller
{
    
    public function index()
    {
      $this->view->set('user', $this->user);
      $this->view->heading("Profile");
      $this->view->render();
    }
    public function edit()
    {
      $this->view->heading("Edit profile");
      $user = $this->user;
      $user->issystemadmin = $this->loadModel('Users')->isSystemAdmin($user->role);
      $this->view->set("user", $user);
      
      $cfields = $this->loadModel('Customfields')->getCustomFields(array('type' => 'user'));
	  $cfieldsData = $this->loadModel('Customfields')->getFieldsData('user', $user->id);
      $this->view->set("issystemadmin", $user->issystemadmin);
	  $this->view->set("cfields", $cfields);
	  $this->view->set("cfieldsData", $cfieldsData);
      $this->view->render('edit');
    }
    
    public function save()
    {
      $form = Request::post('form', 'object');

      if($form->firstname and $form->lastname){
        $usersModel = $this->loadModel('Users');
        
        if($user = $usersModel->getUser(array('id'=>$form->id))){
          if($form->password and !empty($form->password)){
              
            $words = [$form->firstname, $form->lastname];
            
            if (Core::validatePassword($form->password, $words)){
                if ($form->password == $form->cpassword) {
                    unset($form->cpassword);
                    $form->passwordClear = $form->password;
                    $form->password = md5($form->password);
                } else {
					$this->redirect("profile/edit/$form->id", array("danger" => "Passwords do not match"));
				}
            } else {
                $this->redirect("profile/edit", array("danger" => Core::invalidPasswordMessage()));
            }
              
          }else{
            unset($form->password);
          }
            
          $cfields = array();
            if (isset($form->custom_fields)) {
                $cfields = $form->custom_fields;
                unset($form->custom_fields);
            }
  
          $usersModel->updateUser($form, array('id'=>$form->id));
          $this->redirect("", array("success"=>"User profile was successfully saved"));
        }else{
          $this->redirect("profile/edit", array("danger"=>"Invalid user id"));
        }
      }
    }
}
