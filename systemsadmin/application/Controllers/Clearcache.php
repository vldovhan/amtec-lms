<?php

class Clearcache extends Controller
{
    public function index()
    {
      $systems = $this->loadModel('Systems')->getSystems($this->user->id);
      $this->view->set("systems", $systems);
      $this->view->heading("Clear Cache");
      $this->view->render();
    }
    
    public function process()
    {
      $systems = $this->loadModel('Systems')->getSystems($this->user->id);
      $this->view->set("systems", $systems);
      $this->view->heading("Clear Cache");
      $this->view->render('process');
    }
    
    public function cron()
    {
      $systems = $this->loadModel('Systems')->getSystemsAttr(array('state'=>1));
      
        echo 'Moodle Cron:<hr>';
        
        $curl = WWWPROTOCOL.MAINHOST.'/admin/cron.php';
        $ch = curl_init($curl);
        curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Length: 0'));
        $result = curl_exec($ch);

        echo '<br />-------------------- <br /> DOMAIN: '.MAINHOST;
        curl_close($ch);
        if (!$result){
            echo '<br /> STATUS: ERROR<hr />';
        } else {
            echo '<br /> STATUS: SUCCESS<hr />';
        }
        
        foreach ($systems as $system) {
            if ($system->other_domain > 0) {
                $url = WWWPROTOCOL.$system->url;
            } else {
                $url = WWWPROTOCOL.$system->url.MAINDOMAIN;
            }
            $curl = $url.'/admin/cron.php';
            $ch = curl_init($curl);
            curl_setopt($ch,CURLOPT_HTTPHEADER, array('Content-Length: 0'));
            $result = curl_exec($ch);

            echo '<br />-------------------- <br /> DOMAIN: '.$url;
            curl_close($ch);
            if (!$result){
                echo '<br /> STATUS: ERROR<hr />';
            } else {
                echo '<br /> STATUS: SUCCESS<hr />';
            }
        }
    }
}
