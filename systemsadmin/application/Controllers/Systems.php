<?php

class Systems extends Controller
{
	public function index()
	{
		$systems = $this->loadModel('Systems')->getSystems($this->user->id);
		$this->view->set("systems", $systems);
		$this->view->heading("TalentQuest Learning Management Instances");
        
        //Mail::sendTestMail(1);
        
		$this->view->render();
	}

	public function add()
	{
		$zones = DateTimeZone::listIdentifiers();
		$this->view->heading("Add Instance");
		$cfields = $this->loadModel('Customfields')->getCustomFields(array('type' => 'system'));
		$this->view->set("countries", $this->loadModel('Countries')->getCountriesList());
		$this->view->set("zones", $zones);
		$this->view->set("system", null);
		$this->view->set("cfields", $cfields);
		$this->view->render('edit');
	}

	public function edit($id)
	{
		$zones = DateTimeZone::listIdentifiers();

		$system = $this->loadModel('Systems')->getSystem(array('id' => $id));
		if (!$system) {
			$this->redirect("systems", array("danger" => "Invalid systems id"));
		}
        
		$cfields = $this->loadModel('Customfields')->getCustomFields(array('type' => 'system'));
		$this->view->heading("Edit Instance - " . $system->name);
		$cfieldsData = $this->loadModel('Customfields')->getFieldsData('system', $system->id);
		$this->view->set("countries", $this->loadModel('Countries')->getCountriesList());
		$this->view->set("zones", $zones);
		$this->view->set("system", $system);
		$this->view->set("cfields", $cfields);
		$this->view->set("cfieldsData", $cfieldsData);
		$this->view->render('edit');
	}

	public function enable($id)
	{
		$system = $this->loadModel('Systems')->getSystem(array('id' => $id));
		if (!$system) {
			$this->redirect("systems", array("danger" => "Invalid systems id"));
		}
		$system->state = 1;
		$this->loadModel('Systems')->updateSystem($system, array('id' => $system->id));
        $this->redirect("systems/process/$system->id");
	}

	public function disable($id)
	{
		$system = $this->loadModel('Systems')->getSystem(array('id' => $id));
		if (!$system) {
			$this->redirect("systems", array("danger" => "Invalid systems id"));
		}
		$system->state = 0;
		$this->loadModel('Systems')->updateSystem($system, array('id' => $system->id));
        $this->redirect("systems/process/$system->id");
	}

	public function delete($id)
	{
		$confirm = Request::post('confirm');
		$systemModel = $this->loadModel('Systems');
		if ($id and $confirm) {
			if ($system = $systemModel->getSystem(array('id' => $id))) {
                $domain = $this->loadModel('Systems')->prepareDomain($system->url);

				if ($domain != '' and $domain != '/') {
					$this->sendrequest("delete_db", $system->id, true);

					if (file_exists(BACKUPS_DIR . 'database/' . $domain)) {
						File::deleteDir(BACKUPS_DIR . 'database/' . $domain);
					}
					if (file_exists(BACKUPS_DIR . 'moodledata/' . $domain)) {
						File::deleteDir(BACKUPS_DIR . 'moodledata/' . $domain);
					}
					if (file_exists(MOODLEDATA_DIR . $domain)) {
						File::deleteDir(MOODLEDATA_DIR . $domain);
					}
					// delete backups
					$this->loadModel('Backups')->deleteSystemBackups($system->id);

					$this->loadModel('Customfields')->deleteFieldsData('system', $system->id);
					$systemModel->deleteSystem($system->id);
				}

				$this->redirect("systems", array("success" => "The system was successfully deleted"));
			} else {
				$this->redirect("systems", array("danger" => "Invalid system id"));
			}
		}
		$system = $systemModel->getSystem(array('id' => $id));
		$system->options = $systemModel->canDelete($system);
		$this->view->heading("Delete Moodle system");
		$this->view->set("system", $system);
		$this->view->render('delete');
	}

	public function save()
	{
		$form = Request::post('form', 'object');

		if ($form->url) {
			$systemModel = $this->loadModel('Systems');
			$cfields = array();
			if (isset($form->custom_fields)) {
				$cfields = $form->custom_fields;
				unset($form->custom_fields);
			}

			$form->backup = (isset($form->backup) and $form->backup > 0) ? $form->backup : 0;
			$form->url = strtolower($form->url);
			$form->url = str_replace('www.', '', $form->url);
			$form->url = str_replace('http://', '', $form->url);
			$form->url = str_replace('https://', '', $form->url);

			$full_url = WWWPROTOCOL . $form->url . MAINDOMAIN;

			if (Core::validateUrl($full_url)) {
				$words = [$form->name, $form->username];

				if (isset($form->id) and $form->id) {
					$form->created = 1;

					if ($form->password && Core::validatePassword($form->password, $words)) {
						if ($form->password == $form->cpassword) {
							unset($form->cpassword);
						}
						else {
							$this->redirect("systems/edit/$form->id", array("danger" => "Passwords do not match"));
						}
					} else {
						$this->redirect("systems/edit/$form->id", array("danger" => Core::invalidPasswordMessage()));
					}

					$systemModel->updateSystem($form, array('id' => $form->id));
					$this->loadModel('Customfields')->processCustomFields('system', $cfields, $form->id);
					//$this->sendrequest("update_profile", $form->id, true);
					$this->redirect("systems/process/$form->id");
				} else {
                    
					if ($form->password && Core::validatePassword($form->password, $words)) {
						if ($form->password == $form->cpassword) {
							unset($form->cpassword);
						}
						else {
							$this->redirect("systems/add", array("danger" => "Passwords do not match"));
						}
					} else {
						$this->redirect("systems/add", array("danger" => Core::invalidPasswordMessage()));
					}
                    
                    $urlexists = $systemModel->getSystemsAttr(array('url'=>$form->url));
                    if (count($urlexists)){
                        $this->redirect("systems/add", array("danger" => "Landing URL already exists"));
                    }

					$form->oldurl = $form->url;
					$form->timecreated = time();
                    $form->userid = $this->user->id;
					$form->id = $systemModel->insertSystem($form);
					$this->loadModel('Customfields')->processCustomFields('system', $cfields, $form->id);
					if ($form->id) {
						$this->redirect("systems/process/$form->id");
					} else {
						$this->redirect("systems/add", array("danger" => "Erorr code 102"));
					}
				}
			}
			else {
				$this->redirect("systems/add", array("danger" => "URL is incorrect"));
			}

		} else {
			$this->redirect("systems/add", array("danger" => "Erorr code 101"));
		}
	}

	public function process($id = 0)
	{
		$system = $this->loadModel('Systems')->getSystem(array('id' => $id));
		if (!$system) {
			$this->redirect("systems", array("danger" => "Invalid systems id"));
		}

        if ($system->created == 0) {
            $this->view->heading("Creating System LMS: $system->name");
        } elseif ($system->created > 0) {
            $this->view->heading("Updating System LMS: $system->name");
        }
		
		$this->view->set("system", $system);
		$this->view->render('process');
	}

	public function check_data()
	{
		$status = 'success';
		$form = Request::post('form', 'object');
		$errors = [];
		$words = [];

		if (empty($form->name)) {
			$status = 'error';
			$errors[] = 'System name is empty';
		} else {
			$words[] = $form->name;
		}

		if (empty($form->url)) {
			$status = 'error';
			$errors[] = 'URL is empty';
		}
		else {
			$form->url = strtolower($form->url);
			$form->url = str_replace('www.', '', $form->url);
			$form->url = str_replace('http://', '', $form->url);
			$form->url = str_replace('https://', '', $form->url);
			$full_url = WWWPROTOCOL . $form->url . MAINDOMAIN;
			if (!Core::validateUrl($full_url)) {
				$status = 'error';
				$errors[] = 'Landing URL is incorrect';
			} else {
                if ($form->id == 0){
                    $urlexists = $this->loadModel('Systems')->getSystemsAttr(array('url'=>$form->url));
                    if (count($urlexists)){
                        $status = 'error';
                        $errors[] = 'Landing URL already exists';
                    }
                }
			}
		}

		$v = "/^[A-Za-z0-9_]*$/";
		if (empty($form->username) || !filter_var($form->username, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => $v)))) {
			$status = 'error';
			$errors[] = 'Username is incorrect';
		} else {
			$words[] = $form->username;
		}

		if (empty($form->password) || !Core::validatePassword($form->password, $words)) {
			$status = 'error';
			$errors[] = Core::invalidPasswordMessage();
		}

		if ($form->password != $form->cpassword) {
			$status = 'error';
			$errors[] = 'Passwords do not match';
		}

		if (!empty($form->email) && !filter_var($form->email, FILTER_VALIDATE_EMAIL)) {
			$status = 'error';
			$errors[] = 'User email is incorrect';
		}

		$result = [
			'status' => $status,
			'form' => $form,
			'errors' => $errors
		];

		die(json_encode($result));
		/*
		$id = Request::post('id', 'int');
		$data = Request::post('data', 'string');
		$type = Request::post('type', 'string');

		$result = 0;
		if ($type == 'email') {
			if (!filter_var($data, FILTER_VALIDATE_EMAIL)) {
				$result = 1;
			}
		} elseif ($type == 'username') {
			$v = "/^[A-Za-z0-9_]*$/";
			if (!filter_var($data, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => $v)))) {
				$result = 1;
			}
		} elseif ($type == 'url') {
			$other_domain = Request::post('other_domain', 'int');
			$data = strtolower($data);
			$data = str_replace('www.', '', $data);
			$data = str_replace('http://', '', $data);
			$data = str_replace('https://', '', $data);
			if ($other_domain > 0) {
				$v = "/^(http:\/\/|https:\/\/)?((www\.)?([A-Za-z0-9\-]+\.)+([A-Za-z]+){2,4})(\:(\d)+)?(\/(.*))?$/i";
				if (!filter_var($data, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>$v)))) {
					$result = 1;
				}
				$dns = dns_get_record($data);
				if (count($dns) > 0){
					$result = 1;
					foreach ($dns as $d){
						if ($d['ip'] == $this->mainip){
							$result = 0; break;
						}
					}
				} else {
					$result = 1;
				}
			} else {
				$v = "/^[A-Z0-9]+$/i";
				if (!filter_var($data, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => $v)))) {
					$result = 1;
				}
			}
		}
		if ($result == 0) {
			$system = $this->loadModel('Systems')->getSystem(array($type => "$data"));
			if (isset($system->id)) {
				$result = 1;
				if ($system->id > 0 and $id > 0 and $id == $system->id) {
					$result = 0;
				}
			}
		}
		echo $result;
		exit;
		*/
	}

	public function sendrequest($type = '', $id = 0, $return = false)
	{
		$error = false;
		$status = 1;

		$system = $this->loadModel('Systems')->getSystem(array('id' => $id));
		if (!$system) {
			$status = 0;
			$error = 'Invalid systems id';
			$result = array('status' => $status, 'error' => $error);
			echo json_encode($result);
			exit;
		}
		$domain = $system->url;

		if ($type == 'create_alias') {
			$step1 = true;
			if (!$step1) {
				$status = 0;
				$error = 'Error in creating Domain.';
			}
			$result = array('status' => $status, 'error' => $error);
		} elseif ($type == 'create_db') {
			$domain = $this->loadModel('Systems')->prepareDomain($domain);
			$step2 = $this->loadModel('Systems')->createInstance($domain);

			if (!$step2) {
				$status = 0;
				$error = 'Error in creating Database.';
			}
			$result = array('status' => $status, 'error' => $error);
		} elseif ($type == 'generate_script') {
			$domain = $this->loadModel('Systems')->prepareDomain($domain);
			$step3 = File::Copy(MOODLEDATA_DIR . DB_MASTER, MOODLEDATA_DIR . $domain);
			if (!$step3) {
				$status = 0;
				$error = 'Error in Generating Scripts.';
			}
			$result = array('status' => $status, 'error' => $error);
		} elseif ($type == 'create_profile') {
			$fields_string = '';
			foreach ($system as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
			}
			rtrim($fields_string, '&');
			if ($system->other_domain > 0) {
				$ch = curl_init(WWWPROTOCOL . $domain . "/MasterCopy/systemupd.php");
			} else {
				$ch = curl_init(WWWPROTOCOL . $domain . MAINDOMAIN . "/MasterCopy/systemupd.php");
			}
			curl_setopt($ch, CURLOPT_POST, count($system));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			$step4 = curl_exec($ch);

			curl_close($ch);
			if ($step4 != true) {
				$status = 0;
				$error = 'Error in Creating System Profile.';
			}
			$result = array('status' => $status, 'error' => $error);
		} elseif ($type == 'update_profile') {
			$fields_string = '';
			foreach ($system as $key => $value) {
				$fields_string .= $key . '=' . $value . '&';
			}
			rtrim($fields_string, '&');
			if ($system->other_domain > 0) {
				$ch = curl_init(WWWPROTOCOL . $domain . "/MasterCopy/systemupd.php");
			} else {
				$ch = curl_init(WWWPROTOCOL . $domain . MAINDOMAIN . "/MasterCopy/systemupd.php");
			}
            
			curl_setopt($ch, CURLOPT_POST, count($system));
			curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
			$step4 = curl_exec($ch);
			curl_close($ch);

			if (!$step4) {
				$status = 0;
				$error = 'Error in updating System Profile.';
			}
			$result = array('status' => $status, 'error' => $error);
		} elseif ($type == 'delete_db') {
			$domain = $this->loadModel('Systems')->prepareDomain($domain);
			
			if ($this->loadModel('Systems')->deleteDB($domain)) {
				$res = 0;
			} else {
				$res = 1;
			}
			
			if ($res != 0) {
				$status = 0;
				$error = 'Error in deleting Database.';
			}
			$result = array('status' => $status, 'error' => $error);
		}

		if (!$return) {
			echo json_encode($result);
			exit;
		}
	}
    
    public function sendconfirmation($id = 0, $status = 'success', $step = 4)
	{
        $system = $this->loadModel('Systems')->getSystem(array('id' => $id));
		if (!$system) {
			die("Invalid systems id");
		}
        
		Mail::sendConfirmationMail($system, $status, $step);
        exit;
	}
    
}
