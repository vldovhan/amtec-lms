<?php

class View {

	private $pageVars = array();
	private $template;
	private $layout = 'default';
	private $heading = 'Dashboard';
	private $user;

	public function __construct($view)
	{
		$this->template =  $view;
	}

	public function set($var, $val)
	{
		$this->pageVars[$var] = $val;
	}
	public function heading($val)
	{
		$this->heading = $val;
	}
	public function layout($val)
	{
		$this->layout = $val;
	}
	public function user($val)
	{
		$this->user = $val;
	}

	public function render($action = 'index')
	{
		extract($this->pageVars);
		$notifications = Core::db('Site','getUsersNotify', Core::user('id'));
		$alerts = Session::pull('alerts');
		$this->template =  $this->template .'/'. $action .'.php';

		require(APP .'template/'. $this->layout .'.php');
	}

}

?>
