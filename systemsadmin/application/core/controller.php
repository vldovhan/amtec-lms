<?php
class Controller
{
    public $user        = null;
    public $view        = null;
    public $settings    = null;
    public $name        = '';

    function __construct($cron = false)
    {
        $this->loadHelpers(array('Core', 'Session', 'Request', 'Form', 'File', 'Mailer', 'Mail'));
        Session::init();

        $this->name     =  strtolower(get_class($this));
        $this->view     = $this->loadView();
        $this->user     = $this->getUser();
        $this->settings = $this->getSettings();

        if(!$this->user and $this->name != 'login' and !$cron){
          $this->redirect("login");
        }

        if ($this->user && !$this->checkAccess($this->user->id, $this->name)) {
			$this->redirect("", array("danger"=>"You have no access to ".$this->name." controller!"));
		};

		if ($this->user) {
			$usersModel = $this->loadModel('Users');
			$this->view->user($this->user);
			$this->view->set("issystemadmin", $usersModel->isSystemAdmin($this->user->role));
		}

        Session::set('path', $this->name);
    }

    public function checkAccess($user_id, $access) {
		$usersModel = $this->loadModel('Users');

		if ($usersModel->hasAccess($user_id, $access)) {
			return true;
		};

		return false;
	}

    public function getSettings()
    {
      if (count($this->settings) > 0) return $this->settings;
        
      $settings = array();
      $model = $this->loadModel('Settings');
      $db_settings = $model->getSettings();
      if (count($db_settings)){
          foreach ($db_settings as $setting){
            $settings[$setting->name] = $setting;
          }
      }
      return $settings;
    }
    private function getUser()
    {
      $session = Session::get('user');
      $model = $this->loadModel('Site');
      if(isset($session->id) and $user = $model->getUserById($session->id)){
        Session::set('user', $user);
        return $user;
      }else{
        Session::set('user', null);
        return null;
      }
    }
    public function loadModel($name = '')
    {
      $model = ucfirst($name) .'Model';
      require_once(APP .'Models/'. ucfirst($name)  .'.php');
      return new $model();
    }
    public function loadView()
    {
      return new View($this->name);
    }
    public function loadPlugin($name)
    {
      require_once(APP .'Plugins/'. $name .'.php');
      return new $name;
    }
    public function loadHelpers($list)
    {
      foreach($list as $item){
        require_once(APP .'Helpers/'. $item .'.php');
      }
    }
    public function redirect($loc, $alerts = array())
    {
      if($alerts){
        Session::set("alerts", $alerts);
      }

      header('Location: '. URL . $loc);
      die();
    }
}
