<?php
  require APP . 'config/config.php';
  require APP . 'core/application.php';
  require APP . 'core/controller.php';
  require APP . 'core/model.php';
  require APP . 'core/view.php';
