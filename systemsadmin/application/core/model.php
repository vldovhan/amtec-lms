<?php

class Model {

	private $db;
	protected static $instance = null;

	function __construct()
	{
		if(self::$instance == null){
	    try {
				$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
				$this->db = self::$instance = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
			} catch (PDOException $e) {
	        exit('Database connection could not be established.');
	    }
		}else{
			$this->db = self::$instance;
		}
	}
    
    function reConnect($DB_NAME = DB_NAME, $exists = false)
	{
        if ($DB_NAME != DB_NAME and $exists){
            if(!$this->systemExists($DB_NAME)){
                exit($DB_NAME.': Database not exists');
            }
        }
	    try {
				$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);
				$this->db = self::$instance = new PDO(DB_TYPE . ':host=' . DB_HOST . ';dbname=' . $DB_NAME . ';charset=' . DB_CHARSET, DB_USER, DB_PASS, $options);
			} catch (PDOException $e) {
	        exit('Database connection could not be established.');
	    }
	}
    
    
    function createDB($dbname = '')
	{
        if (empty($dbname)) {
            die("Empty DB name");
        }
	    
        try {
            $dbh = new PDO('mysql:host=' . DB_HOST . '', DB_ROOTUSER, DB_ROOTPASS);

            $dbh->exec("CREATE DATABASE `$dbname`;") 
            or die(print_r($dbh->errorInfo(), true));

        } catch (PDOException $e) {
            die("DB ERROR: ". $e->getMessage());
        }
	}
    
    function deleteDB($dbname = '')
	{
        if (empty($dbname)) {
            die("Empty DB name");
        }
	    
        try {
            $dbh = new PDO('mysql:host=' . DB_HOST . '', DB_ROOTUSER, DB_ROOTPASS);

            $dbh->exec("DROP DATABASE IF EXISTS `$dbname`;");
            $this->reConnect();
            
            return true;
        } catch (PDOException $e) {
            die("DB ERROR: ". $e->getMessage());
        }
	}
    
    function createInstance($dbname = '')
	{
        if (empty($dbname)) {
            die("Empty DB name");
        }
        
        $this->createDB($dbname);
        $this->reConnect(DB_MASTER);
        $tables = $this->select("SHOW TABLES");
        $this->reConnect($dbname, false);
        
        if (count($tables)){
            foreach ($tables as $table){
                if (!isset($table->{'Tables_in_'.DB_MASTER})) continue;
                
                $tablename = $table->{'Tables_in_'.DB_MASTER};
                $query = "CREATE TABLE `" . $dbname . "`.`" . $tablename . "` LIKE `" . DB_MASTER . "`.`" . $tablename . "`";  
                $stmt = $this->db->prepare($query);
                $stmt->execute();
                
                $query = "INSERT INTO `" . $dbname . "`.`" . $tablename . "` SELECT * FROM `" . DB_MASTER . "`.`" . $tablename . "`";
				$stmt = $this->db->prepare($query);
                $stmt->execute();
            }
        }
        
        $this->reConnect();
        return true;
	}
	/**
     * run raw sql queries
     * @param  string $sql sql command
     * @return return query
     */
    public function toArray($data)
    {
        $options = array();
        foreach($data as $item){
            $options[$item->id] = $item->value;
        }
        return $options;
    }
    public function raw($sql)
    {
        return $this->db->query($this->pfx($sql));
    }
    /**
     * method for selecting records from a database
     * @param  string $sql       sql query
     * @param  array  $array     named params
     * @param  object $fetchMode
     * @param  string $class     class name
     * @return array            returns an array of records
     */
    public function one($sql, $array = array(), $fetchMode = PDO::FETCH_OBJ, $class = ''){
        $data = $this->select($sql, $array, $fetchMode, $class);
        return (isset($data[0])) ? $data[0] : null;
    }

    private function pfx($sql){
        return str_replace('pfx_', DB_PREFIX, $sql);
    }
    public function conditions($data){
        $conditions = array();
        foreach($data as $key=> $val){
            $conditions[] = "$key = :$key";
        }
        return (!empty($conditions)) ? implode(" AND ", $conditions) : '';
    }
    public function select($sql, $array = array(), $fetchMode = PDO::FETCH_OBJ, $class = '')
    {
            $stmt = $this->db->prepare($this->pfx($sql));
            foreach ($array as $key => $value) {
                    if (is_int($value)) {
                            $stmt->bindValue("$key", $value, PDO::PARAM_INT);
                    } else {
                            $stmt->bindValue("$key", $value);
                    }
            }
            $stmt->execute();

            if (DEBUG) {
                $error_code = $stmt->errorInfo();
                if (isset($error_code[0]) and $error_code[0] == '00000'){
                } else {
                    echo "\PDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                    die();
                }
            }

            if ($fetchMode === PDO::FETCH_CLASS) {
                    return $stmt->fetchAll($fetchMode, $class);
            } else {
                    return $stmt->fetchAll($fetchMode);
            }
    }
    /**
     * insert method
     * @param  string $table table name
     * @param  array $data  array of columns and values
     */
    public function insert($table, $data)
    {
            $data = (array) $data;
            $table = DB_PREFIX . $table;
            ksort($data);
            $fieldNames = implode(',', array_keys($data));
            $fieldValues = ':'.implode(', :', array_keys($data));
            $stmt = $this->db->prepare("INSERT INTO $table ($fieldNames) VALUES ($fieldValues)");
            foreach ($data as $key => $value) {
                    $stmt->bindValue(":$key", $value);
            }
            $stmt->execute();

            if (DEBUG) {
                $error_code = $stmt->errorInfo();
                if (isset($error_code[0]) and $error_code[0] == '00000'){
                } else {
                    echo "\PDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                    die();
                }
            }

            return $this->db->lastInsertId();
    }
    /**
     * update method
     * @param  string $table table name
     * @param  array $data  array of columns and values
     * @param  array $where array of columns and values
     */
    public function update($table, $data, $where)
    {
            $data = (array) $data;
            $table = DB_PREFIX . $table;
            ksort($data);
            $fieldDetails = null;
            foreach ($data as $key => $value) {
                    $fieldDetails .= "$key = :field_$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');
            $whereDetails = null;
            $i = 0;
            foreach ($where as $key => $value) {
                    if ($i == 0) {
                            $whereDetails .= "$key = :where_$key";
                    } else {
                            $whereDetails .= " AND $key = :where_$key";
                    }
                    $i++;
            }
            $whereDetails = ltrim($whereDetails, ' AND ');
            $stmt = $this->db->prepare("UPDATE $table SET $fieldDetails WHERE $whereDetails");
            foreach ($data as $key => $value) {
                    $stmt->bindValue(":field_$key", $value);
            }
            foreach ($where as $key => $value) {
                    $stmt->bindValue(":where_$key", $value);
            }
            $stmt->execute();

            if (DEBUG) {
                $error_code = $stmt->errorInfo();
                if (isset($error_code[0]) and $error_code[0] == '00000'){
                } else {
                    echo "\PDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                    die();
                }
            }

            return $stmt->rowCount();
    }
    /**
     * Delete method
     *
     * @param  string $table table name
     * @param  array $where array of columns and values
     * @param  integer   $limit limit number of records
     */
    public function delete($table, $where, $limit = 1)
    {
            $table = DB_PREFIX . $table;
            ksort($where);
            $whereDetails = null;
            $i = 0;
            foreach ($where as $key => $value) {
                    if ($i == 0) {
                            $whereDetails .= "$key = :$key";
                    } else {
                            $whereDetails .= " AND $key = :$key";
                    }
                    $i++;
            }
            $whereDetails = ltrim($whereDetails, ' AND ');
            //if limit is a number use a limit on the query
            if (is_numeric($limit)) {
                    $uselimit = "LIMIT $limit";
            }
            $stmt = $this->db->prepare("DELETE FROM $table WHERE $whereDetails $uselimit");
            foreach ($where as $key => $value) {
                    $stmt->bindValue(":$key", $value);
            }
            $stmt->execute();

            if (DEBUG) {
                $error_code = $stmt->errorInfo();
                if (isset($error_code[0]) and $error_code[0] == '00000'){
                } else {
                    echo "\PDO::errorInfo():\n";
                    print_r($stmt->errorInfo());
                    die();
                }
            }

            return $stmt->rowCount();
    }
    /**
     * truncate table
     * @param  string $table table name
     */
    public function truncate($table)
    {
            $table = DB_PREFIX . $table;
            return $this->db->exec("TRUNCATE TABLE $table");
    }

    public function escapeString($string)
    {
        return mysql_real_escape_string($string);
    }

    public function escapeArray($array)
    {
        array_walk_recursive($array, create_function('&$v', '$v = mysql_real_escape_string($v);'));
        return $array;
    }
    
    public function prepareDomain($domain = '')
    {
        $domain .= SUBDOMAIN;
        $domain = str_replace('.', '_', $domain);
        $domain = str_replace('-', '_', $domain);
        
        return $domain;
    }
    
    public function systemExists($system_dir = '')
    {
        if (!is_dir(MOODLEDATA_DIR.$system_dir)){
            return false;
        }
        
        return true;
    }
    

}
?>
