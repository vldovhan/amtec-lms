<?php require APP . 'template/include/head.php'; ?>

<content style="margin:0;">
  <div class="container" style="padding:0;">
    <?php if($alerts): ?>
      <?php foreach($alerts as $key=>$val): ?>
          <div class="alert alert-<?php echo $key; ?>" role="alert"><?php echo $val; ?></div>
      <?php endforeach; ?>
    <?php endif; ?>

    <?php require(APP . 'Views/' . $this->template); ?>
  </div>
</content>

<?php require APP . 'template/include/footer.php'; ?>
<?php require APP . 'template/include/foot.php'; ?>
