
        <div class="sidebar" data-color="azure">
          <div class="logo">
            <a class="logo-text" href="<?php echo URL; ?>"> SystemsAdmin<?php /*(Controller->settings['systemadmin_name']->value)? Controller->settings['systemadmin_name']->value : Controller->settings['systemadmin_name']->value;*/ ?> </a>
          </div>
          <div class="sidebar-wrapper">
            <div class="user">
                <div class="user-name">
                    <a href="<?php echo URL_SITE; ?>/profile/edit">
                        <?php echo $this->user->firstname . ' ' . $this->user->lastname; ?>
                    </a>
                </div>

                <div class="info">
                    <a class="collapsed" href="#collapseExample" data-toggle="collapse">
                        <?php echo Core::user('name'); ?>
                        <b class="caret"></b>
                    </a>
                    <div id="collapseExample" class="collapse">
                        <ul class="nav">
                            <?php /* ?><li><a href="<?php echo URL_SITE; ?>/profile">My Profile</a></li><?php */ ?>
                            <li><a href="<?php echo URL_SITE; ?>/profile/edit">Edit Profile</a></li>
                            <li><a href="<?php echo URL_SITE; ?>/login/logout">Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
           
            <ul class="nav">
              <li class="<?php echo (Session::get('path') == 'home')?'active':'';?>"><a href="<?php echo URL; ?>"><i class="ion-ios-pie-outline"></i> Dashboard</a></li>
              <li class="<?php echo (Session::get('path') == 'systems')?'active':'';?>"><a href="<?php echo URL; ?>systems"><i class="ion-ios-albums-outline"></i> Instances</a></li>
              <li class="<?php echo (Session::get('path') == 'users')?'active':'';?>"><a href="<?php echo URL; ?>users"><i class="ion-ios-people-outline"></i> Administrators</a></li>
              <?php if ($issystemadmin): ?>
                <?php /* ?><li class="<?php echo (Session::get('path') == 'backups')?'active':'';?>"><a href="<?php echo URL; ?>backups"><i class="ion-ios-download-outline"></i> Backups</a></li><?php */ ?>
                <li class="<?php echo (Session::get('path') == 'clearcache')?'active':'';?>"><a href="<?php echo URL; ?>clearcache/process"><i class="ion-ios-trash-outline"></i> Clear Cache</a></li>
                <li class="<?php echo (Session::get('path') == 'updatedb')?'active':'';?>"><a href="<?php echo URL; ?>updatedb/process"><i class="ion-ios-refresh-outline"></i> Update Databases</a></li>
                <li class="<?php echo (Session::get('path') == 'settings')?'active':'';?>"><a href="<?php echo URL; ?>settings"><i class="ion-ios-color-filter-outline"></i> Settings</a></li>
                <li class="<?php echo (Session::get('path') == 'reports')?'active':'';?>"><a href="<?php echo URL; ?>reports"><i class="ion-stats-bars"></i> Reports</a></li>
              <?php endif; ?>
                <?php /* ?>
              <li style="opacity:0.6" class="disabled <?php echo (Session::get('path') == 'analytics')?'active':'';?>"><a href="#"><i class="ion-ios-pulse"></i> Analytics</a></li>
              <li style="opacity:0.6" class="disabled <?php echo (Session::get('path') == 'reports')?'active':'';?>"><a href="#"><i class="ion-ios-cloud-upload-outline"></i> Reports</a></li><?php */ ?>
            </ul>
          </div>
        </div>
        <div class="main">
          <nav class="navbar navbar-default">
                      <div class="container-fluid">
                          <div class="navbar-header">
                              <button data-toggle="collapse" class="navbar-toggle" type="button">
                                  <span class="sr-only">Toggle navigation</span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                                  <span class="icon-bar"></span>
                              </button>
                              <a href="<?php echo URL; ?>" class="navbar-brand"><?php echo $this->heading; ?></a>
                          </div>
                          <div class="collapse navbar-collapse">

                              <?php /* ?>
                              <form action="<?php echo URL; ?>home/search" role="search" class="navbar-form navbar-left navbar-search-form">
                                  <div class="input-group">
                                      <span class="input-group-addon"><i class="ion-ios-search-strong"></i></span>
                                      <input name="keyword" type="text" placeholder="Search..." class="form-control" value="<?php echo Request::query('keyword') ?>">
                                  </div>
                              </form>
                              <?php */ ?>

                              <ul class="nav navbar-nav navbar-right hidden">
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                        <i class="ion-ios-compose-outline"></i>
                                        <p class="hidden-md hidden-lg">Actions</p>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo URL; ?>systems/add">Create New System</a></li>
                                        <li><a href="<?php echo URL; ?>users/add">Create New User</a></li>
                                    </ul>
                                </li>

                                  <li>
                                      <a href="<?php echo URL_SITE; ?>" title="Dashboard">
                                          <i class="ion-ios-pulse"></i>
                                      </a>
                                  </li>


                                  <?php /* ?>
                                  <li class="dropdown">
                                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                          <i class="ion-ios-calendar-outline"></i>
                                          <p class="hidden-md hidden-lg">Notifications</p>
                                          <span class="noty"><?php echo count($notifications); ?></span>
                                      </a>
                                      <ul class="dropdown-menu">
                                          <?php if($notifications): ?>
                                            <?php foreach($notifications as $item): ?>
                                            <li><a href="<?php echo URL; ?>users/<?php echo $item->name; ?>">
                                                <?php echo ($item->plantime > time())? "$item->name's account will expire in ". Core::days(time(), $item->plantime) : "$item->name's account expired on ".date("m/d/Y", $item->plantime); ?>
                                            </a></li>
                                            <?php endforeach; ?>
                                          <?php else: ?>
                                            <li><a href="<?php echo URL; ?>">No New Notifications</a></li>
                                          <?php endif; ?>
                                      </ul>
                                  </li>
                                  <?php */ ?>

                                  <li class="dropdown dropdown-with-icons">
                                      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                          <i class="ion-navicon"></i>
                                          <p class="hidden-md hidden-lg">More</p>
                                      </a>
                                      <ul class="dropdown-menu dropdown-with-icons">

                                          <li>
                                              <a href="<?php echo URL_SITE; ?>">
                                                  <i class="ion-ios-pulse"></i> Dashboard
                                              </a>
                                          </li>
                                          <?php /* ?><li>
                                              <a href="<?php echo URL_SITE; ?>/profile/edit">
                                                  <i class="ion-ios-gear-outline"></i> Settings
                                              </a>
                                          </li><?php */ ?>
                                          <li>
                                              <a class="text-danger" href="<?php echo URL_SITE; ?>/login/logout">
                                                  <i class="ion-ios-arrow-thin-right"></i> Log out
                                              </a>
                                          </li>
                                          <li class="divider"></li>
                                          <li>
                                              <a href="<?php echo URL_SITE; ?>/privacy">
                                                  <i class="ion-ios-list-outline"></i> Privacy
                                              </a>
                                          </li>
                                          <li>
                                              <a href="<?php echo URL_SITE; ?>/terms-of-use">
                                                  <i class="ion-ios-list-outline"></i> Terms of Use
                                              </a>
                                          </li>
                                      </ul>
                                  </li>

                              </ul>
                          </div>
                      </div>
                  </nav>
