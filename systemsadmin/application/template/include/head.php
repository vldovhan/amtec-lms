<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SITE; ?></title>

	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600" type="text/css">
	<link href="//fonts.googleapis.com/css?family=Roboto:400,300,400italic,700,900,700italic,500italic,500" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
	<!--<link rel="stylesheet" href="https://cdn.datatables.net/r/bs/dt-1.10.9/datatables.min.css">-->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.css"/>
	<link rel="stylesheet" href="<?php echo URL; ?>assets/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo URL; ?>assets/css/bootstrap-datepicker.css">
	<link rel="stylesheet" href="<?php echo URL; ?>assets/css/light.css">
	<link rel="stylesheet" href="<?php echo URL; ?>assets/css/core.css">
	<link rel="icon" type="image/png" href="images/favicon.png">

	<script src="<?php echo URL; ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo URL; ?>assets/js/bootstrap.min.js"></script>
    <!--<script src="https://cdn.datatables.net/r/bs/dt-1.10.9/datatables.min.js"></script>-->
	<script type="text/javascript" src="//cdn.datatables.net/v/dt/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.12/b-1.2.2/b-html5-1.2.2/b-print-1.2.2/r-2.1.0/datatables.min.js"></script>
	<script src="<?php echo URL; ?>assets/js/bootstrap-datepicker.js"></script>
	<script>
				var url = "<?php echo URL; ?>";
		</script>
	    <script src="<?php echo URL; ?>assets/js/application.js"></script>
    
</head>
<body>
