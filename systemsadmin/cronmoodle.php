<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('memory_limit', '-1');

define('ROOT', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('APP', ROOT . 'application' . DIRECTORY_SEPARATOR);

if (file_exists(APP . 'core/autoload.php')) {
    require APP . 'core/autoload.php';
}

$app = new Application('moodle');
