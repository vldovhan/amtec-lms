<?php
require('../../config.php');
require_once("$CFG->dirroot/enrol/approval/library.php");
require_once("$CFG->dirroot/enrol/approval/users_forms.php");
require_once("$CFG->dirroot/enrol/renderer.php");

$id		  = required_param('id', PARAM_INT);
$filter  = optional_param('ifilter', 0, PARAM_INT);
$search  = optional_param('search', '', PARAM_RAW);
$role    = optional_param('role', 0, PARAM_INT);
$fgroup  = optional_param('filtergroup', 0, PARAM_INT);
$status  = optional_param('status', -1, PARAM_INT);
$action  = optional_param('action', '', PARAM_ALPHANUMEXT);

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id);

if (!$enrol_approval = enrol_get_plugin('approval')) {
    throw new coding_exception('Can not instantiate enrol_approval');
}

$res = $DB->get_record_sql("select * from ".$CFG->prefix."enrol where courseid=".$id." and enrol='approval'");
$instance = $res->id;
$instancename = $enrol_approval->get_instance_name($res);

if ($course->id == SITEID) {
    redirect(new moodle_url('/'));
}

require_login($course);
$PAGE->set_pagelayout('course');
$PAGE->set_context($context);

$manager = new course_enrolment_manager($PAGE, $course, $filter, $role, $search, $fgroup, $status);
$table = new course_enrolment_users_table($manager, $PAGE);
$table->attributes['class'] .= ' sorting';
$PAGE->set_url('/enrol/approval/users.php', $manager->get_url_params()+$table->get_url_params());
navigation_node::override_active_url(new moodle_url('/enrol/instances.php', array('id' => $course->id)));
$PAGE->navbar->add($instancename);
$PAGE->navbar->add(get_string('pendingusers', 'enrol_approval'), new moodle_url('/enrol/approval/users.php', array('id' => $id)));

// Check if there is an action to take
if ($action == 'remove') {

    $userid  = required_param('user', PARAM_INT);
    $enrol_approval->unenrol_user($res, $userid);
    
    redirect($PAGE->url);
    
} elseif ($action == 'approve') {

    $userid  = required_param('user', PARAM_INT);
    $enrol_approval->update_user_enrol($res, $userid, 0);
    redirect($PAGE->url);

}

$renderer = $PAGE->get_renderer('core_enrol');
$userdetails = array (
    'picture' => false,
    'firstname' => get_string('firstname'),
    'lastname' => get_string('lastname'),
);
$extrafields = get_extra_user_fields($context);
foreach ($extrafields as $field) {
    $userdetails[$field] = get_user_field_name($field);
}

$fields = array(
    'userdetails' => $userdetails,
    'actions' => get_string('actions')
);

$filterform = new enrol_users_filter_form('users.php', array('manager' => $manager, 'id' => $id),
        'get', '', array('id' => 'filterform'));
$filterform->set_data(array('search' => $search));

$table->set_fields($fields, $renderer);

$users = $manager->get_users_for_display($manager, 'sortorder', 'ASC', $table->page, $table->perpage, $instance);
foreach ($users as $userid=>&$user) {
    $user['picture'] = $OUTPUT->render($user['picture']);
    $user['email'] = $user['email'];
    
    $icon = html_writer::empty_tag('img', array('alt'=>get_string('approve'), 'title'=>get_string('approve'), 'src'=>$OUTPUT->pix_url('t/approve')));
    $url = new moodle_url($CFG->wwwroot.'/enrol/approval/users.php?id='.$course->id, array('action'=>'approve', 'user'=>$userid));
    $user['actions'] = html_writer::link($url, $icon);
    
    $icon = html_writer::empty_tag('img', array('alt'=>get_string('remove'), 'title'=>get_string('remove'), 'src'=>$OUTPUT->pix_url('t/delete')));
    $url = new moodle_url($CFG->wwwroot.'/enrol/approval/users.php?id='.$course->id, array('action'=>'remove', 'user'=>$userid));
    $user['actions'] .= '&nbsp;&nbsp;'.html_writer::link($url, $icon);
    
}
$table->set_total_users($manager->get_total_users());
$table->set_users($users);

$PAGE->set_title($PAGE->course->fullname);
$PAGE->set_heading($PAGE->title);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pendingusers', 'enrol_approval'));
echo html_writer::start_tag('div', array('class' => 'enrollment-users-table'));

if (count($users) > 0 or !empty($search)){
    echo $renderer->render_course_enrolment_users_table($table, $filterform);
} else {
    echo html_writer::tag('div', get_string('nousers', 'enrol_approval'), array('class' => 'alert alert-info'));
}

echo html_writer::end_tag('div');
echo $OUTPUT->footer();

