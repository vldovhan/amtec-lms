<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_approval', language 'en'.
 *
 * @package    enrol_approval
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['canntenrol'] = 'Enrollment is disabled or inactive';
$string['canntenrolearly'] = 'You cannot enrol yet; enrollment starts on {$a}.';
$string['canntenrollate'] = 'You cannot enrol any more, since enrollment ended on {$a}.';
$string['cohortnonmemberinfo'] = 'Only members of cohort \'{$a}\' can approval-enrol.';
$string['cohortonly'] = 'Only cohort members';
$string['enrolme'] = 'Send Request';
$string['cohortonly_help'] = 'Approval enrollment may be restricted to members of a specified cohort only. Note that changing this setting has no effect on existing enrollments.';
$string['customwelcomemessage'] = 'Custom welcome message';
$string['customwelcomemessage_help'] = 'A custom welcome message may be added as plain text or Moodle-auto format, including HTML tags and multi-lang tags.

The following placeholders may be included in the message:

* Course name {$a->coursename}
* Link to user\'s profile page {$a->profileurl}
* User email {$a->email}
* User fullname {$a->fullname}';
$string['defaultrole'] = 'Default role assignment';
$string['defaultrole_desc'] = 'Select role which should be assigned to users during approval enrollment';
$string['enrolenddate'] = 'End date';
$string['enrolenddate_help'] = 'If enabled, users can enrol themselves until this date only.';
$string['enrolenddaterror'] = 'Enrollment end date cannot be earlier than start date';
$string['enrollme'] = 'Enrol me';
$string['enrolperiod'] = 'Enrollment duration';
$string['enrolperiod_desc'] = 'Default length of time that the enrollment is valid. If set to zero, the enrollment duration will be unlimited by default.';
$string['enrolperiod_help'] = 'Length of time that the enrollment is valid, starting with the moment the user enrols themselves. If disabled, the enrollment duration will be unlimited.';
$string['enrolstartdate'] = 'Start date';
$string['enrolstartdate_help'] = 'If enabled, users can enrol themselves from this date onward only.';
$string['expiredaction'] = 'Enrollment expiration action';
$string['expiredaction_help'] = 'Select action to carry out when user enrollment expires. Please note that some user data and settings are purged from course during course unenrollment.';
$string['expirymessageenrollersubject'] = 'Approval enrollment expiry notification';
$string['expirymessageenrollerbody'] = 'Approval enrollment in the course \'{$a->course}\' will expire within the next {$a->threshold} for the following users:

{$a->users}

To extend their enrollment, go to {$a->extendurl}';
$string['expirymessageenrolledsubject'] = 'Approval enrollment expiry notification';
$string['expirymessageenrolledbody'] = 'Dear {$a->user},

This is a notification that your enrollment in the course \'{$a->course}\' is due to expire on {$a->timeend}.

If you need help, please contact {$a->enroller}.';
$string['groupkey'] = 'Use group enrollment keys';
$string['groupkey_desc'] = 'Use group enrollment keys by default.';
$string['groupkey_help'] = 'In addition to restricting access to the course to only those who know the key, use of group enrollment keys means users are automatically added to groups when they enrol in the course.

Note: An enrollment key for the course must be specified in the approval enrollment settings as well as group enrollment keys in the group settings.';
$string['keyholder'] = 'You should have received this enrollment key from:';
$string['longtimenosee'] = 'Unenrol inactive after';
$string['longtimenosee_help'] = 'If users haven\'t accessed a course for a long time, then they are automatically unenrolled. This parameter specifies that time limit.';
$string['maxenrolled'] = 'Max enrolled users';
$string['maxenrolled_help'] = 'Specifies the maximum number of users that can approval enrol. 0 means no limit.';
$string['maxenrolledreached'] = 'Maximum number of users allowed to approval-enrol was already reached.';
$string['messageprovider:expiry_notification'] = 'Approval enrollment expiry notifications';
$string['newenrols'] = 'Allow new enrollments';
$string['newenrols_desc'] = 'Allow users to approval enrol into new courses by default.';
$string['newenrols_help'] = 'This setting determines whether a user can enrol into this course.';
$string['nopassword'] = 'No enrollment key required.';
$string['password'] = 'Enrollment key';
$string['password_help'] = 'An enrollment key enables access to the course to be restricted to only those who know the key.

If the field is left blank, any user may enrol in the course.

If an enrollment key is specified, any user attempting to enrol in the course will be required to supply the key. Note that a user only needs to supply the enrollment key ONCE, when they enrol in the course.';
$string['passwordinvalid'] = 'Incorrect enrollment key, please try again';
$string['passwordinvalidhint'] = 'That enrollment key was incorrect, please try again<br />
(Here\'s a hint - it starts with \'{$a}\')';
$string['pluginname'] = 'Approval enrollment';
$string['pluginname_desc'] = 'The approval enrollment plugin allows users to choose which courses they want to participate in. The courses may be protected by an enrollment key. Internally the enrollment is done via the manual enrollment plugin which has to be enabled in the same course.';
$string['requirepassword'] = 'Require enrollment key';
$string['requirepassword_desc'] = 'Require enrollment key in new courses and prevent removing of enrollment key from existing courses.';
$string['role'] = 'Default assigned role';
$string['approval:config'] = 'Configure approval enrol instances';
$string['approval:holdkey'] = 'Appear as the approval enrollment key holder';
$string['approval:manage'] = 'Manage enrolled users';
$string['approval:unenrol'] = 'Unenrol users from course';
$string['approval:unenrolapproval'] = 'Unenrol approval from the course';
$string['sendcoursewelcomemessage'] = 'Send course welcome message';
$string['sendcoursewelcomemessage_help'] = 'If enabled, users receive a welcome message via email when they approval-enrol in a course.';
$string['showhint'] = 'Show hint';
$string['showhint_desc'] = 'Show first letter of the guest access key.';
$string['status'] = 'Allow existing enrollments';
$string['status_desc'] = 'Enable approval enrollment method in new courses.';
$string['status_help'] = 'If enabled together with \'Allow new enrollments\' disabled, only users who approval enrolled previously can access the course. If disabled, this approval enrollment method is effectively disabled, since all existing approval enrollments are suspended and new users cannot approval enrol.';
$string['unenrol'] = 'Unenrol user';
$string['unenrolapprovalconfirm'] = 'Do you really want to unenrol yourapproval from course "{$a}"?';
$string['unenroluser'] = 'Do you really want to unenrol "{$a->user}" from course "{$a->course}"?';
$string['usepasswordpolicy'] = 'Use password policy';
$string['usepasswordpolicy_desc'] = 'Use standard password policy for enrollment keys.';
$string['welcometocourse'] = 'Welcome to {$a}';
$string['welcometocoursetext'] = 'Welcome to {$a->coursename}!

If you have not done so already, you should edit your profile page so that we can learn more about you:

  {$a->profileurl}';
$string['pendingmessage'] = 'You have submitted request for enrollment. Your enrollment is pending';
$string['approve_users'] = 'Approve users';
$string['pendingusers'] = 'Users pending for approval';
$string['nousers'] = 'No users for approval';
