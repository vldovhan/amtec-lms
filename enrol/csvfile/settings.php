<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * csvfile enrolments plugin settings and presets.
 *
 * @package    enrol_csvfile
 * @copyright  2010 Eugene Venter
 * @author     Eugene Venter - based on code by Petr Skoda and others
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once(__DIR__.'/adminlib.php');

if ($ADMIN->fulltree) {

    //--- general settings -----------------------------------------------------------------------------------
    $settings->add(new admin_setting_heading('enrol_csvfile_settings', '', get_string('pluginnameadmin_desc', 'enrol_csvfile')));
    
    $a = $CFG->wwwroot.'/enrol/csvfile/adminimport.php';
    $settings->add(new admin_setting_heading('enrol_csvfile_import', '', get_string('adminimportdescription', 'enrol_csvfile', $a)));

    $options = core_text::get_encodings();
    $settings->add(new admin_setting_configselect('enrol_csvfile/encoding', get_string('encoding', 'enrol_csvfile'), '', 'UTF-8', $options));

    $settings->add(new admin_setting_configcheckbox('enrol_csvfile/mailstudents', get_string('notifyenrolled', 'enrol_csvfile'), '', 0));

    $settings->add(new admin_setting_configcheckbox('enrol_csvfile/mailteachers', get_string('notifyenroller', 'enrol_csvfile'), '', 0));

    //--- mapping -------------------------------------------------------------------------------------------
    if (!during_initial_install()) {
        $settings->add(new admin_setting_heading('enrol_csvfile_mapping', get_string('mapping', 'enrol_csvfile'), ''));

        $roles = role_fix_names(get_all_roles());

        foreach ($roles as $role) {
            $settings->add(new enrol_csvfile_role_setting($role));
        }
        unset($roles);
    }
}
