<?php

require('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/csvlib.class.php');
require_once('adminimport_form.php');
require_once('importlib.php');

$instanceid  = optional_param('id', 0, PARAM_INT); // instanceid
$iid         = optional_param('iid', '', PARAM_INT);
$previewrows = optional_param('previewrows', 10, PARAM_INT);

@set_time_limit(60*60); // 1 hour should be enough
raise_memory_limit(MEMORY_HUGE);

$context = context_system::instance();
require_login();
require_capability('enrol/csvfile:manage', $context);

$PAGE->set_url('/enrol/csvfile/adminimport.php');
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_heading(get_string('enrolusers', 'enrol_csvfile'));

$returnurl = new moodle_url('/enrol/csvfile/adminimport.php');
if (!enrol_is_enabled('csvfile')) {
    redirect(new moodle_url('/'));
}

$strrecordadded             = get_string('recordadded', 'enrol_csvfile');
$strrecordnotaddederror     = get_string('recordnotaddederror', 'enrol_csvfile');
$errorstr                   = get_string('error');
$stryes                     = get_string('yes');
$strno                      = get_string('no');

$plugin = enrol_get_plugin('csvfile');

$roles = role_fix_names(get_all_roles());
$roleslist = array();
if(count($roles)){
    foreach ($roles as $role){
        $roleslist[$role->shortname] = $role->id;
    }
}

// array of all valid fields for validation
$FIELDS = array('operation', 'coursecode', 'userid', 'role', 'starttime', 'endtime');

if (empty($iid)) {
    $mform1 = new import_form1(null, array('data'=>array('id'=>$instanceid, 'courseid'=>$courseid)));

    if ($formdata = $mform1->get_data()) {
        $iid = csv_import_reader::get_new_iid('importfile');
        $cir = new csv_import_reader($iid, 'importfile');

        $content = $mform1->get_file_content('importfile');

        $readcount = $cir->load_csv_content($content, $formdata->encoding, $formdata->delimiter_name);
        $csvloaderror = $cir->get_error();
        unset($content);

        if (!is_null($csvloaderror)) {
            print_error('csvloaderror', '', $returnurl, $csvloaderror);
        }
        // test if columns ok
        $filecolumns = fi_validate_file_upload_columns($cir, $FIELDS, $returnurl);
        // continue to form2

    } else {
        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('enrolusers', 'enrol_csvfile'));
        
        echo $OUTPUT->box(highlight('', markdown_to_html(get_string('pluginnameadmin_desc', 'enrol_csvfile'))), 'generalbox formsettingheading');
        
        $mform1->display();
        echo $OUTPUT->footer();
        die;
    }
} else {
    $cir = new csv_import_reader($iid, 'importfile');
    $filecolumns = fi_validate_file_upload_columns($cir, $FIELDS, $returnurl);
}

$mform2 = new import_form2(null, array('columns'=>$filecolumns, 'data'=>array('iid'=>$iid, 'previewrows'=>$previewrows)));

// If a file has been uploaded, then process it
if ($formdata = $mform2->is_cancelled()) {
    $cir->cleanup(true);
    redirect($returnurl);

} else if ($formdata = $mform2->get_data()) {
    // Print the header
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('enrolusers', 'enrol_csvfile'));
    
    $recordsnew     = 0;
    $recordsprc     = 0;
    $recordsupd     = 0;
    $recordsdel     = 0;
    $recordserrors  = 0;
    
    // init csv import helper
    $cir->init();
    $linenum = 1; //column header is first line

    // init upload progress tracker
    $upt = new fi_admin_progress_tracker();
    $upt->start(); // start table

    while ($line = $cir->next()) {
        $upt->flush();
        $linenum++;

        $upt->track('line', $linenum);

        $record = new stdClass();

        // add fields to user object
        foreach ($line as $keynum => $value) {
            if (!isset($filecolumns[$keynum])) {
                // this should not happen
                continue;
            }
            $key = $filecolumns[$keynum];
            
            switch ($key) {
                case 'operation':
                    $record->operation = ($value != '') ? trim($value) : '';
                    break;
                case 'coursecode':
                    $record->coursecode = ($value != '') ? trim($value) : '';
                    break;
                case 'userid':
                    $record->idnumber = ($value != '') ? trim($value) : '';
                    break;
                case 'role':
                    $record->role = ($value != '' and isset($roleslist[trim($value)])) ? $roleslist[trim($value)] : $instance->roleid;
                    break;
                case 'starttime':
                    $record->starttime = ($value != '') ? strtotime(trim($value)) : time();
                    break;
                case 'endtime':
                    $record->endtime = ($value != '') ? strtotime(trim($value)) : 0;
                    break;
                default:
                    $record->$key = trim($value);       
            }

            if (in_array($key, $upt->columns)) {
                // default value in progress tracking table, can be changed later
                $upt->track($key, s($value), 'normal');
            }
        }
        
        if (empty($record->coursecode)) {
            $upt->track('status', get_string('missingfield', 'error', 'coursecode'), 'error');
            $upt->track('coursecode', $errorstr, 'error');
            $recordserrors++;
            continue;
        }
        
        if (empty($record->idnumber)) {
            $upt->track('status', get_string('missingfield', 'error', 'userID'), 'error');
            $upt->track('userID', $errorstr, 'error');
            $recordserrors++;
            continue;
        }
        
        if (!$course = $DB->get_record('course', array('idnumber'=>$record->coursecode))) {
            $upt->track('id', $errorstr, 'error');
        }
       
        if (!$user = $DB->get_record('user', array('idnumber'=>$record->idnumber))) {
            $upt->track('id', $errorstr, 'error');
        }
        
        if ($user and $course){
            $result = $plugin->process_record($record, $user, $course);
            
            $upt->track('status', $strrecordadded);
            $upt->track('id', '', 'normal', false);
            if ($result > 1){
                $recordsnew++;    
            } elseif ($result > 0) {
                $recordsprc++;    
            } elseif ($result < 0) {
                $recordsdel++;
            } else {
                $recordsupd++;
            }
            
        } else {
            if (!$course){
                $upt->track('status', get_string('coursenotexists', 'enrol_csvfile'), 'error');
            }
            if (!$user){
                $upt->track('status', get_string('usernotexists', 'enrol_csvfile'), 'error');
            }
            $recordserrors++;
        }        
    }
    $upt->close(); // close table

    $cir->close();
    $cir->cleanup(true);

    echo $OUTPUT->box_start('boxwidthnarrow boxaligncenter generalbox', 'uploadresults');
    echo '<p>';
    
    echo get_string('recordscreated', 'enrol_csvfile').': '.$recordsnew.'<br />';
    echo get_string('recordsupdated', 'enrol_csvfile').': '.$recordsupd.'<br />';
    echo get_string('recordsdeleted', 'enrol_csvfile').': '.$recordsdel.'<br />';
    echo get_string('recordsprocessed', 'enrol_csvfile').': '.$recordsprc.'<br />';
    echo get_string('recordserrors', 'enrol_csvfile').': '.$recordserrors.'</p>';
    echo $OUTPUT->box_end();

    echo $OUTPUT->continue_button($returnurl);

    echo $OUTPUT->footer();
    die;
}


$PAGE->set_heading(get_string('pluginname', 'enrol_csvfile'));
$PAGE->set_title(get_string('pluginname', 'enrol_csvfile'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('enrolusers', 'enrol_csvfile'));

// NOTE: this is JUST csv processing preview, we must not prevent import from here if there is something in the file!!
//       this was intended for validation of csv formatting and encoding, not filtering the data!!!!
//       we definitely must not process the whole file!

// preview table data
$data = array();
$cir->init();
$linenum = 1; //column header is first line
$noerror = true; // Keep status of any error.
while ($linenum <= $previewrows and $fields = $cir->next()) {
    $linenum++;
    $rowcols = array();
    $rowcols['line'] = $linenum;
    foreach($fields as $key => $field) {
        if (!isset($filecolumns[$key])) continue;
        $rowcols[$filecolumns[$key]] = s(trim($field));
    }
    $rowcols['status'] = array();
    if (!isset($rowcols['coursecode']) or (isset($rowcols['coursecode']) and $rowcols['coursecode'] == '')) {
        $rowcols['status'][] = get_string('missingcoursecode', 'enrol_csvfile');
    }
    if (!isset($rowcols['userid']) or (isset($rowcols['userid']) and $rowcols['userid'] == '')) {
        $rowcols['status'][] = get_string('missinguserID', 'enrol_csvfile');
    }
    if (!count($rowcols['status'])){
        $rowcols['status'][] = get_string('canimport', 'enrol_csvfile');
    }
    $rowcols['status'] = implode('<br />', $rowcols['status']);
    $data[] = $rowcols;
}
if ($fields = $cir->next()) {
    $data[] = array_fill(0, count($filecolumns) + 3, '...');
}
$cir->close();

$table = new html_table();
$table->id = "uupreview";
$table->attributes['class'] = 'generaltable';
$table->tablealign = 'center';
$table->summary = get_string('uploaduserspreview', 'tool_uploaduser');
$table->head = array();
$table->data = $data;

$table->head[] = get_string('uucsvline', 'tool_uploaduser');
foreach ($filecolumns as $column) {
    $table->head[] = $column;
}
$table->head[] = get_string('status');

echo html_writer::tag('div', html_writer::table($table), array('class'=>'flexible-wrap', 'style'=>'overflow:auto; margin-bottom:25px;'));

// Print the form if valid values are available
if ($noerror) {
    $mform2->display();
}
echo $OUTPUT->footer();
