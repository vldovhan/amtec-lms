<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_csvfile', language 'en'.
 *
 * @package    enrol_csvfile
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['encoding'] = 'File encoding';
$string['expiredaction'] = 'Enrollment expiration action';
$string['expiredaction_help'] = 'Select action to carry out when user enrollment expires. Please note that some user data and settings are purged from course during course unenrollment.';
$string['csvfile:manage'] = 'Manage user enrollments manually';
$string['csvfile:unenrol'] = 'Unenroll users from the course manually';
$string['csvfilesync'] = 'Flat file enrollment sync';
$string['location'] = 'File location';
$string['location_desc'] = 'Specify full path to the enrollment file. The file is automatically deleted after processing.';
$string['notifyadmin'] = 'Notify administrator';
$string['notifyenrolled'] = 'Notify enrolled users';
$string['notifyenroller'] = 'Notify user responsible for enrollments';
$string['messageprovider:csvfile_enrolment'] = 'CSV file enrollment messages';
$string['mapping'] = 'CSV file role mapping';
$string['pluginname'] = 'CSV file';
$string['pluginnameadmin_desc'] = 'This method will process a specially-formatted uploaded csv file.
The file is a comma separated file assumed to have four or five fields per line:

    operation, coursecode, userID, role, , [, starttime [, endtime]]

where:

* operation - add | del - required
* coursecode - Course ID number - required
* userID - user ID Number - required
* role - student | teacher | teacheredit - required
* starttime - start time - optional
* endtime - end time - optional

It could look something like this:
<pre class="informationbox">
   add, course1, john.doe, student
   add, course1, john.smith, teacheredit
   del, course56, jane.air, student
   add, ABSaa, jane.air, student, 6/30/2016, 9/30/2016
</pre>';
$string['adminimportdescription'] = '<br><p><a href="{$a}" style="font-size:15px;"><i class="fa fa-download"></i> Import user enrollments now</a></p>';

$string['pluginname_desc'] = 'This method will process a specially-formatted uploaded csv file.
The file is a comma separated file assumed to have four or five fields per line:

    operation, userID, role, [, starttime [, endtime]]

where:

* operation - add | del - required
* userID - user ID Number - required
* role - student | teacher | teacheredit - required
* starttime - start time - optional
* endtime - end time - optional

It could look something like this:
<pre class="informationbox">
   add, john.doe, student
   add, john.smith, teacheredit
   del, jane.air, student
   add, jane.air, student, 6/30/2016, 9/30/2016
</pre>';

$string['role'] = 'Assign role';
$string['csvfile:manage'] = 'Manage CSV file enrollments';
$string['csvfile:unenrol'] = 'Unenroll users from course';
$string['status'] = 'Allow CSV file enrollments';
$string['enrolperiod'] = 'Enrollment duration';
$string['enrolperiod_desc'] = 'Default length of time that the enrollment is valid (in seconds). If set to zero, the enrollment duration will be unlimited by default.';
$string['enrolperiod_help'] = 'Length of time that the enrollment is valid, starting with the moment the user was enrolled. If disabled, the enrollment duration will be unlimited.';
$string['enrolusers'] = 'Enroll Users';
$string['importusers'] = 'Import Users';
$string['import'] = 'Import';
$string['recordadded'] = 'Enrollment processed';
$string['recordnotaddederror'] = 'Record not created';
$string['uploadresult'] = 'Upload results';
$string['operation'] = 'Operation';
$string['username'] = 'Username';
$string['starttime'] = 'Start Time';
$string['endtime'] = 'End Time';
$string['recordscreated'] = 'Users enrolled';
$string['recordsdeleted'] = 'Users unenrolled';
$string['recordsupdated'] = 'Users unenrollment updated';
$string['recordserrors'] = 'Errors';
$string['usernotexists'] = 'User not exists in system';
$string['coursenotexists'] = 'Course not exists in system';
$string['canimport'] = 'Can process';
$string['recordsprocessed'] = 'Enrollment processed';
$string['example'] = 'Import file example';
$string['coursecode'] = 'Course ID number';
$string['userID'] = 'User ID Number';
$string['missinguserID'] = 'Missing userID';
$string['missingcoursecode'] = 'Missing Course Code';


