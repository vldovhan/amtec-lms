<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

class enrol_csvfile_edit_form extends moodleform {

    function definition() {
        $mform = $this->_form;

        list($instance, $plugin, $context) = $this->_customdata;

        $mform->addElement('header', 'header', get_string('pluginname', 'enrol_csvfile'));

        $mform->addElement('text', 'name', get_string('custominstancename', 'enrol'));
		$mform->setType('name', PARAM_TEXT);
		
        $options = array(ENROL_INSTANCE_ENABLED  => get_string('yes'),
                         ENROL_INSTANCE_DISABLED => get_string('no'));
        $mform->addElement('select', 'status', get_string('status', 'enrol_csvfile'), $options);
        $mform->addHelpButton('status', 'status', 'enrol_csvfile');
        $mform->setDefault('status', $plugin->get_config('status'));
        
        if ($instance->id) {
            $roles = get_default_enrol_roles($context, $instance->roleid);
        } else {
            $roles = get_default_enrol_roles($context, $plugin->get_config('roleid'));
        }
        $mform->addElement('select', 'roleid', get_string('role', 'enrol_waitlist'), $roles);
        $mform->setDefault('roleid', 5);
        
        $mform->addElement('duration', 'enrolperiod', get_string('enrolperiod', 'enrol_csvfile'), array('optional' => true, 'defaultunit' => 86400));
        $mform->setDefault('enrolperiod', $plugin->get_config('enrolperiod'));
        $mform->addHelpButton('enrolperiod', 'enrolperiod', 'enrol_csvfile');
		
        $mform->addElement('advcheckbox', 'customint1', get_string('notifyenrolled', 'enrol_csvfile'));
        $mform->setDefault('customint1', $plugin->get_config('mailstudents'));
        
        $mform->addElement('advcheckbox', 'customint2', get_string('notifyenroller', 'enrol_csvfile'));
        $mform->setDefault('customint4', $plugin->get_config('mailteachers'));
        
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);

        $this->add_action_buttons(true, ($instance->id ? null : get_string('addinstance', 'enrol')));

        $this->set_data($instance);
    }

    function validation($data, $files) {
        global $DB, $CFG;
        $errors = parent::validation($data, $files);

        list($instance, $plugin, $context) = $this->_customdata;
        
        return $errors;
    }
}