<?php
require('../../config.php');
require_once("$CFG->dirroot/enrol/waitlist/library.php");
require_once("$CFG->dirroot/enrol/waitlist/users_forms.php");
require_once("$CFG->dirroot/enrol/renderer.php");

$id		  = required_param('id', PARAM_INT);
$filter  = optional_param('ifilter', 0, PARAM_INT);
$search  = optional_param('search', '', PARAM_RAW);
$role    = optional_param('role', 0, PARAM_INT);
$fgroup  = optional_param('filtergroup', 0, PARAM_INT);
$status  = optional_param('status', -1, PARAM_INT);
$action  = optional_param('action', '', PARAM_ALPHANUMEXT);

if (optional_param('resetbutton', '', PARAM_RAW) !== '') {
    redirect('users.php?id=' . $id);
}

$course = $DB->get_record('course', array('id'=>$id), '*', MUST_EXIST);
$context = context_course::instance($course->id);

if (!$enrol_manual = enrol_get_plugin('waitlist')) {
    throw new coding_exception('Can not instantiate enrol_manual');
}

$res = $DB->get_record_sql("select id from ".$CFG->prefix."enrol where courseid=".$id." and enrol='waitlist'");
$instance = $res->id;
$instancename = $enrol_manual->get_instance_name($res);

if ($course->id == SITEID) {
    redirect(new moodle_url('/'));
}

require_login($course);
$PAGE->set_pagelayout('admin');

$manager = new course_enrolment_manager($PAGE, $course, $filter, $role, $search, $fgroup, $status);
$table = new course_enrolment_users_table($manager, $PAGE);
$table->attributes['class'] .= ' sorting';
$PAGE->set_url('/enrol/waitlist/users.php', $manager->get_url_params()+$table->get_url_params());
navigation_node::override_active_url(new moodle_url('/enrol/instances.php', array('id' => $course->id)));
$PAGE->navbar->add($instancename);
$PAGE->navbar->add(get_string('users_on_waitlist', 'enrol_waitlist'), new moodle_url('/enrol/waitlist/users.php', array('id' => $id)));

// Check if there is an action to take
if ($action == 'remove') {

    $userid  = required_param('user', PARAM_INT);
    $DB->delete_records('user_enrol_waitlist', array('userid'=>$userid, 'instanceid'=>$instance));
    
    redirect($PAGE->url);
    
} elseif($action == 'move'){
    
    $eid         = optional_param('eid', 0, PARAM_RAW);
    $moveafter   = optional_param('moveafter', 0, PARAM_RAW);
    
    $eid = str_replace('user_', '', $eid);
    $moveafter = ($moveafter == '0') ? 0 : str_replace('user_', '', $moveafter);
    
    $moving_user = $DB->get_record("user_enrol_waitlist", array('userid'=>$eid, 'instanceid'=>$instance));
    
    if ($moveafter == 0){
        
        $moving_user->sortorder = 0;
        $DB->update_record("user_enrol_waitlist", $moving_user);
        
        $wusers = $DB->get_records_sql("SELECT id, sortorder FROM {user_enrol_waitlist} WHERE userid != $eid AND instanceid = $instance ORDER BY sortorder");
        
        $i = 1;
        foreach ($wusers as $user){
            $user->sortorder = $i++;
            $DB->update_record("user_enrol_waitlist", $user);
        }    
    } else {
        
        $moveafter_user = $DB->get_record("user_enrol_waitlist", array('userid'=>$moveafter, 'instanceid'=>$instance));
        $moving_user->sortorder = ++$moveafter_user->sortorder;
        $DB->update_record("user_enrol_waitlist", $moving_user);
        
        $wusers = $DB->get_records_sql("SELECT id, sortorder FROM {user_enrol_waitlist} WHERE userid != $eid AND instanceid = $instance ORDER BY sortorder");
        
        $i = 0;
        foreach ($wusers as $user){
            $user->sortorder = $i++;
            $DB->update_record("user_enrol_waitlist", $user);
            if ($user->id == $moveafter_user->id) $i++;
        }    
    }
    
    exit;
}

$renderer = $PAGE->get_renderer('core_enrol');
$userdetails = array (
    'picture' => false,
    'firstname' => get_string('firstname'),
    'lastname' => get_string('lastname'),
);
$extrafields = get_extra_user_fields($context);
foreach ($extrafields as $field) {
    $userdetails[$field] = get_user_field_name($field);
}

$fields = array(
    'userdetails' => $userdetails,
    'actions' => get_string('actions')
);

$filterform = new enrol_users_filter_form('users.php', array('manager' => $manager, 'id' => $id),
        'get', '', array('id' => 'filterform'));
$filterform->set_data(array('search' => $search));

$table->set_fields($fields, $renderer);

$users = $manager->get_users_for_display($manager, 'sortorder', 'ASC', $table->page, $table->perpage, $instance);
foreach ($users as $userid=>&$user) {
    $user['picture'] = $OUTPUT->render($user['picture']);
    $user['email'] = '<a href=mailto:'.$user['email'].'>'.$user['email'].'</a>';
    
    $icon = html_writer::empty_tag('img', array('alt'=>get_string('remove'), 'title'=>get_string('remove'), 'src'=>$OUTPUT->pix_url('t/delete')));
    $url = new moodle_url($CFG->wwwroot.'/enrol/waitlist/users.php?id='.$course->id, array('action'=>'remove', 'user'=>$userid));
    $user['actions'] = html_writer::link($url, $icon);
    
    $icon = html_writer::empty_tag('img', array('alt'=>get_string('move'), 'title'=>get_string('move'), 'src'=>$OUTPUT->pix_url('t/move'), 'class'=>'move-action'));
    $url = 'javascript:void(0)';
    $user['actions'] .= '&nbsp;&nbsp;'.html_writer::link($url, $icon);
}
$table->set_total_users($manager->get_total_users());
$table->set_users($users);

$PAGE->set_title($PAGE->course->fullname);
$PAGE->set_heading($PAGE->title);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('users_on_waitlist', 'enrol_waitlist'));
echo html_writer::start_tag('div', array('class' => '   enrollment-users-table'));

if (count($users) > 0 or !empty($search)){
    echo $renderer->render_course_enrolment_users_table($table, $filterform);
} else {
    echo html_writer::tag('div', 'No users in waitlist', array('class' => 'alert alert-info'));
}

echo html_writer::end_tag('div');
echo $OUTPUT->footer();
?>
<script src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/jquery-sortable.js" type="text/javascript"></script>
<script>
    jQuery('.col_userdetails.header span').each(function(){
        var title = jQuery(this).find('a').text();
        jQuery(this).text(title);
    });
    jQuery('table.sorting').sortable({
        containerSelector: 'table',
        itemPath: '> tbody',
        itemSelector: 'tr',
        handle: '.move-action',
        horizontal: false,
        placeholder: '<tr class=\"placeholder\"><td colspan="2"></td></tr>',
        onDrag: function (item, group, _super) {
            item.addClass('active');
            jQuery('table.sorting tr').addClass('not-active');
        },
        onDrop: function  (item, container, _super) {
            jQuery('table.sorting tr').removeClass('not-active');
            item.removeClass('active');
            newIndex = item.index()
            var eid = item.attr('id').replace(" dragged", ""); var prep = newIndex;
            if (prep > 0){
                var row = jQuery('table.sorting tr').eq(prep);
                var new_eid = row.attr('id');
            } else {
                new_eid = 0;
            }
            
            $.ajax({
                type: 'GET',
                url: '<?php echo $CFG->wwwroot; ?>/enrol/waitlist/users.php?action=move&id=<?php echo $id; ?>&eid='+eid+'&moveafter='+new_eid
            });
            
            _super(item)
        },
        onCancel: function (item, container, _super, event) {
            jQuery('table.sorting tr').removeClass('not-active');
            item.removeClass('active');
            _super(item)
        },
    });
</script>
<?php
