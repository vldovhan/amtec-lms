<?php
/**
 * *************************************************************************
 * *                  Waitlist Enrol                                      **
 * *************************************************************************
 * @copyright   emeneo.com                                                **
 * @link        emeneo.com                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************
*/
class waitlist{
	public function add_wait_list($instanceid, $userid, $roleid, $timestart, $timeend){
		global $DB, $CFG;

        $is_record = $DB->get_record('user_enrol_waitlist', array('instanceid'=>$instanceid, 'userid'=>$userid));
        $last_record = $DB->get_record_sql("SELECT ue.id, ue.sortorder FROM {user_enrol_waitlist} ue LEFT JOIN {user} u ON u.id = ue.userid WHERE u.deleted = 0 AND u.suspended = 0 AND ue.instanceid = $instanceid ORDER BY ue.sortorder DESC LIMIT 1");
        
        $waitlist = new stdClass();
		$waitlist->userid = $userid;
		$waitlist->instanceid = $instanceid;
		$waitlist->roleid = $roleid;
		$waitlist->timestart = $timestart;
		$waitlist->timeend = $timeend;
		
        if ($is_record){
            $waitlist->id = $is_record->id;
            $DB->update_record('user_enrol_waitlist', $waitlist);   
            $id = $waitlist->id;
        } else {
            $waitlist->timecreated = time();
            $waitlist->sortorder = (isset($last_record->sortorder)) ? $last_record->sortorder+1 : 0;
            $id = $DB->insert_record('user_enrol_waitlist', $waitlist);
        }
        
        return $id;
	}

	public function vaildate_wait_list($instanceid, $userid){
		global $DB;
		global $CFG;

		$res = $DB->get_records_sql("select * from ".$CFG->prefix."user_enrol_waitlist where instanceid=".$instanceid." and userid=".$userid.' ORDER BY sortorder');
		if(count($res)){
			return false;
		}else{
			return true;
		}
	}

	public function get_wait_list(){
		global $DB;
		global $CFG;
		
		return $DB->get_records_sql("select ew.* from ".$CFG->prefix."user_enrol_waitlist ew LEFT JOIN {user} u ON u.id = ew.userid WHERE u.deleted = 0 AND u.suspended = 0 ORDER BY ew.sortorder");
	}
}