<?php
/**
 * *************************************************************************
 * *                  Waitlist Enrol                                      **
 * *************************************************************************
 * @copyright   emeneo.com                                                **
 * @link        emeneo.com                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************
*/
$string['customwelcomemessage'] = 'Custom welcome message';
$string['defaultrole'] = 'Default role assignment';
$string['defaultrole_desc'] = 'Select role which should be assigned to users during waitlist enrollment';
$string['enrolenddate'] = 'End date';
$string['enrolenddate_help'] = 'If enabled, users can enroll themselves until this date only.';
$string['enrolenddaterror'] = 'Enrollment end date cannot be earlier than start date';
$string['enrolme'] = 'Enroll me';
$string['enrolperiod'] = 'Enrollment duration';
$string['enrolperiod_desc'] = 'Default length of time that the enrollment is valid (in seconds). If set to zero, the enrollment duration will be unlimited by default.';
$string['enrolperiod_help'] = 'Length of time that the enrollment is valid, starting with the moment the user enrolls themselves. If disabled, the enrollment duration will be unlimited.';
$string['enrolstartdate'] = 'Start date';
$string['enrolstartdate_help'] = 'If enabled, users can enroll themselves from this date onward only.';
$string['groupkey'] = 'Use group enrollment keys';
$string['groupkey_desc'] = 'Use group enrollment keys by default.';
$string['groupkey_help'] = 'In addition to restricting access to the course to only those who know the key, use of a group enrollment key means users are automatically added to the group when they enrol in the course.

To use a group enrollment key, an enrollment key must be specified in the course settings as well as the group enrollment key in the group settings.';
$string['longtimenosee'] = 'Unenroll inactive after';
$string['longtimenosee_help'] = 'If users haven\'t accessed a course for a long time, then they are automatically unenrolled. This parameter specifies that time limit.';
$string['maxenrolled'] = 'Max enrolled users';
$string['maxenrolled_help'] = 'Specifies the maximum number of users that can waitlist enroll. 0 means no limit.';
$string['maxenrolledreached'] = 'Maximum number of users allowed to waitlist-enroll was already reached.';
$string['password'] = 'Enrollment key';
$string['password_help'] = 'An enrollment key enables access to the course to be restricted to only those who know the key.

If the field is left blank, any user may enroll in the course.

If an enrollment key is specified, any user attempting to enrol in the course will be required to supply the key. Note that a user only needs to supply the enrollment key ONCE, when they enroll in the course.';
$string['passwordinvalid'] = 'Incorrect enrollment key, please try again';
$string['passwordinvalidhint'] = 'That enrollment key was incorrect, please try again<br />
(Here\'s a hint - it starts with \'{$a}\')';
$string['pluginname'] = 'Waitlist enrollment';
$string['pluginname_desc'] = 'The waitlist enrollment plugin allows users to choose which courses they want to participate in. The courses may be protected by an enrollment key. Internally the enrollment is done via the manual enrollment plugin which has to be enabled in the same course.';
$string['requirepassword'] = 'Require enrollment key';
$string['requirepassword_desc'] = 'Require enrollment key in new courses and prevent removing of enrollment key from existing courses.';
$string['role'] = 'Assign role';
$string['waitlist:config'] = 'Configure waitlist enroll instances';
$string['waitlist:manage'] = 'Manage enrolled users';
$string['waitlist:unenrol'] = 'Unenroll users from course';
$string['waitlist:unenrolwaitlist'] = 'Unenroll waitlist from the course';
$string['sendcoursewelcomemessage'] = 'Send course welcome message';
$string['sendcoursewelcomemessage_help'] = 'If enabled, users receive a welcome message via email when they waitlist-enroll in a course.';
$string['showhint'] = 'Show hint';
$string['showhint_desc'] = 'Show first letter of the guest access key.';
$string['status'] = 'Allow waitlist enrollments';
$string['status_desc'] = 'Allow users to waitlist enroll into course by default.';
$string['status_help'] = 'This setting determines whether a user can enroll (and also unenroll if they have the appropriate permission) themselves from the course.';
$string['unenrolwaitlistconfirm'] = 'Do you really want to unenroll "{$a}"?';
$string['usepasswordpolicy'] = 'Use password policy';
$string['usepasswordpolicy_desc'] = 'Use standard password policy for enrollment keys.';
$string['welcometocourse'] = 'Welcome to {$a}';
$string['welcometocoursetext'] = '<p>Welcome to {$a->coursename}!</p>



<p>Start date: {$a->startdate}</p>

<p>Course description: </p>
	
	<p>{$a->summary}</p>
  ';
$string['confirmation'] = 'If you proceed you will be enrolled to this course.<br><br>Are you absolutely sure you want to do this?';
$string['confirmationfull'] = '<strong>This course is presently booked.</strong> If you proceed you will be placed on a waiting list and will be enrolled automatically and informed by email in case enough spaces becomes available.<br>';
$string['confirmation_yes'] = 'Yes';
$string['confirmation_no'] = 'No';
$string['waitlistinfo'] = '<b>This course is presently booked</b>. <br/><br/>Thank you for your application request. You have been placed on a waiting list and will be informed via email in case a space becomes available.';
$string['waitlist:unenrolself'] = 'Unenrol self from the course';
$string['lineinfo'] = '<br>Number of persons waiting in front of you: ';
$string['lineconfirm'] = '<br>Are you absolutely sure you want to do this?';
$string['confirmation_cancel'] = 'Cancel';
$string['enrolusers'] = 'Enrol users';
$string['disable'] = 'You can not enrol yourself in this course.';
$string['continue'] = 'continue';
$string['users_on_waitlist'] = 'Users on waitlist';
$string['waitlisted_users'] = 'Waitlisted users';

$string['enroll_after_completed'] = 'Enroll after completion';
$string['enroll_after_completed_desc'] = 'Enroll student to course from waitlist after other student completed course';
$string['enroll_after_completed_desc_help'] = 'Enroll student to course from waitlist after other student completed course';
$string['enroll_add_waitlist'] = 'Number of users which can be enrolled is over. If you click "Add", student will be placed on the waitlist.';
$string['number_in_waitlist'] = 'Number of users in wait list: ';


