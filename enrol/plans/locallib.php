<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

function unenrol_fromplan($planid, $courseid = 0, $userid = 0, $reset = false)
{
    global $DB;

    if (!enrol_is_enabled('plans')) {
        return;
    }

    $plugin = enrol_get_plugin('plans');

    $sql = '';
    if($courseid){
      $sql .= " AND e.courseid = $courseid";
    }
    $sql2 = '';
    if($userid){
      $sql2 .= " AND ue.userid = $userid";
    }

    $enrols = $DB->get_records_sql("SELECT DISTINCT e.courseid FROM {enrol} e, {user_enrolments} ue WHERE
            e.customint1 = $planid AND e.enrol='plans' AND ue.enrolid = e.id $sql");

    foreach($enrols as $e){
      $instances = $DB->get_records_sql("SELECT ue.userid, e.* FROM {enrol} e, {user_enrolments} ue WHERE
        e.customint1 = $planid AND e.enrol='plans' AND e.courseid = $e->courseid AND ue.enrolid = e.id $sql2");
        foreach ($instances as $instance) {
            $properties=array(
                'objectid'=>$planid,
                'userid'=>$instance->userid,
                'relateduserid'=>$instance->userid,
                'context'=>context_course::instance($e->courseid),
                'courseid'=>$e->courseid
            );

            $event = \local_plans\event\local_plans_unenrolled::create($properties);
            $event->trigger();
          $plugin->unenrol_user($instance, $instance->userid);
          if($reset){
            $mods = $DB->get_records('course_modules', array('course'=> $instance->courseid));
            foreach($mods as $mod){
                $DB->delete_records('course_modules_completion', array('coursemoduleid'=> $mod->id, 'userid'=>$instance->userid));
            }
            $DB->delete_records('course_completions', array('course'=> $instance->courseid, 'userid'=>$instance->userid));

            $student = get_archetype_roles('student');
            $student = reset($student);
            $plugin->enrol_user($instance, $instance->userid, $student->id, time());

            $properties=array(
                'objectid'=>$planid,
                'userid'=>$instance->userid,
                'relateduserid'=>$instance->userid,
                'context'=>context_course::instance($instance->courseid),
                'courseid'=>$instance->courseid
            );
            $event = \local_plans\event\local_plans_enrolled::create($properties);
            $event->trigger();
          }
        }
    }
}
function enrol_plans_sync_courses($courses, $planid)
{
  global $DB;

    if (!enrol_is_enabled('plans')) {
        return;
    }

    $plugin = enrol_get_plugin('plans');
    $plan = $DB->get_record('local_plans', array('id'=> $planid));
    $users = $DB->get_records('local_plans_users', array('planid'=> $planid));

    foreach($courses as $c){
        if($instance = $DB->get_record('enrol', array('courseid'=>$c, 'enrol'=>'plans', 'customint1'=>$planid))){
        }else{
            $instance = new stdClass();
            $instance->enrol = 'plans';
            $instance->status = 0;
            $instance->name = get_string("criteria_9_".$plan->type, 'local_plans', $plan->name);
            $instance->courseid = $c;
            $instance->customint1 = $planid;
            $instance->timemodified = time();
            $instance->timecreated = time();
            $instance->id = $DB->insert_record('enrol', $instance);
        }
        if($instance->id){
          foreach ($users as $u) {
              if($enrol = $DB->get_record('user_enrolments', array('enrolid'=>$instance->id, 'userid'=> $u->userid))){
                  // already enrolled
              }else{
                $student = get_archetype_roles('student');
                $student = reset($student);
                $plugin->enrol_user($instance, $u->userid, $student->id, time());

                $properties=array(
                    'objectid'=>$planid,
                    'userid'=>$u->userid,
                    'relateduserid'=>$u->userid,
                    'context'=>context_course::instance($instance->courseid),
                    'courseid'=>$instance->courseid
                );

                $event = \local_plans\event\local_plans_enrolled::create($properties);
                $event->trigger();
              }
          }
        }
    }
}
function enrol_plans_sync_users($data, $courses, $cohortid = 0)
{
    global $DB;

    $users= $data->users;
    $planid= $data->id;


    if (!enrol_is_enabled('plans')) {
        return;
    }
    $plan = $DB->get_record('local_plans', array('id'=> $planid));

    $startdate = intval($data->timestart);
    $timeend = intval($data->timeend);

    if(!$data->defaultrole){
        $student = get_archetype_roles('student');
        $student = reset($student);
        $data->defaultrole = $student->id;
    }
    $plugin = enrol_get_plugin('plans');
    foreach($courses as $c){
        if($instance = $DB->get_record('enrol', array('courseid'=>$c->courseid, 'enrol'=>'plans', 'customint1'=>$planid))){
        }else{
            $instance = new stdClass();
            $instance->enrol = 'plans';
            $instance->status = 0;
            $instance->name = get_string("criteria_9_".$plan->type, 'local_plans', $plan->name);
            $instance->courseid = $c->courseid;
            $instance->customint1 = $planid;
            $instance->customint2 = $cohortid;
            $instance->timemodified = time();
            $instance->timecreated = time();
            $instance->id = $DB->insert_record('enrol', $instance);
        }
        if($instance->id){
          foreach ($users as $u) {
              if($enrol = $DB->get_record('user_enrolments', array('enrolid'=>$instance->id, 'userid'=> $u))){
              }else{
                  $plugin->enrol_user($instance, $u, intval($data->defaultrole), $startdate, $timeend);

                  $properties=array(
                    'objectid'=>$planid,
                    'userid'=>$u,
                    'relateduserid'=>$u,
                    'context'=>context_course::instance($instance->courseid),
                    'courseid'=>$instance->courseid
                );

                $event = \local_plans\event\local_plans_enrolled::create($properties);
                $event->trigger();
              }
          }
        }
    }
}

function enrol_plans_sync_course($course) {
    return;
}

function enrol_plans_sync_full(progress_trace $trace) {
    return;
}
