<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'enrol_plans', language 'en'.
 *
 * @package    enrol_plans
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['plans:config'] = 'Configure plans enroll instances';
$string['plans:synchronised'] = 'Role assignments synchronised to course enrollment';
$string['pluginname'] = 'Plans enrollments';
$string['pluginname_desc'] = 'Plans enrollment plugin is a legacy solution for enrollments at the course plans level via role assignments. It is recommended to use cohort synchronisation instead.';
$string['plans:enrol'] = 'Enroll into plan';
$string['plans:manage'] = 'Manage plans';
$string['plans:unenrol'] = 'Unenroll from plan';
