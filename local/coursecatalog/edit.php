<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require('edit_form.php');
require('locallib.php');
require_once($CFG->libdir.'/adminlib.php');

$id        = optional_param('id', 0, PARAM_INT);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$show      = optional_param('show', 0, PARAM_BOOL);
$hide      = optional_param('hide', 0, PARAM_BOOL);
$confirm   = optional_param('confirm', 0, PARAM_BOOL);
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$type = optional_param('type', 0, PARAM_INT);

require_login();
coursecatalog_enable($type);
$context = context_system::instance();
require_capability('local/coursecatalog:manage', $context);
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);
$USER->editing = 0;

$PAGE->set_url('/local/coursecatalog/index.php', array('type'=>$type));

if ($id) {
    $catalog = $DB->get_record('local_coursecatalog', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->url->param('id', $id);
    $catalog->descriptionformat = 1;
} else {
    $catalog = new stdClass();
    $catalog->id = 0;
    $catalog->name = '';
    $catalog->description = '';
    $catalog->descriptionformat = 1;
}



if ($returnurl) {
    $returnurl = new moodle_url($returnurl);
} else {
    $returnurl = new moodle_url($CFG->wwwroot.'/local/coursecatalog/index.php');
}


if ($delete and $catalog->id) {
    $PAGE->url->param('delete', 1);
    if ($confirm and confirm_sesskey()) {
        delete_catalog($catalog->id);
        redirect($returnurl);
    }
    $strheading = get_string('deletecatalog', 'local_coursecatalog');
    $PAGE->navbar->add(get_string('deletecatalog', 'local_coursecatalog'), new moodle_url('/local/coursecatalog/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);
    
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/coursecatalog/edit.php', array('id' => $catalog->id, 'delete' => 1, 'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => $returnurl->out()));
    $message = get_string('deletecatalogmsg', 'local_coursecatalog', format_string($catalog->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($show && $catalog->id && confirm_sesskey()) {
    if (!$catalog->visible) {
        $record = (object)array('id' => $catalog->id, 'visible' => 1);
        save_catalog($record);
    }
    redirect($returnurl);
}

if ($hide && $catalog->id && confirm_sesskey()) {
    if ($catalog->visible) {
        $record = (object)array('id' => $catalog->id, 'visible' => 0);
        save_catalog($record);
    }
    redirect($returnurl);
}

$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES,
    'maxbytes' => $SITE->maxbytes, 'context' => $context, 'descriptionformat' => 1);
if ($catalog->id) {
    // Edit existing.
    $catalog = file_prepare_standard_editor($catalog, 'description', $editoroptions,
            $context, 'local_catalog', 'description', $catalog->id);
    $strheading = get_string('edit', 'local_coursecatalog');

} else {
    // Add new.
    $catalog = file_prepare_standard_editor($catalog, 'description', $editoroptions,
            $context, 'local_catalog', 'description', null);
    $strheading = get_string('create', 'local_coursecatalog');
}

$editform = new catalog_edit_form(null, array('editoroptions'=>$editoroptions, 'data'=>$catalog, 'returnurl'=>$returnurl));

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    $data->id = save_catalog($data);

    if($data->id){
        $data = file_postupdate_standard_editor($data, 'description', $editoroptions,
                $context, 'local_plans', 'description', $data->id);
        save_catalog($data);
    }
    redirect($returnurl);
}

$PAGE->navbar->add(get_string('managecatalog', 'local_coursecatalog'), new moodle_url('/local/coursecatalog/index.php', array('type'=>$type)));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);

echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);

echo $editform->display();
echo $OUTPUT->footer();

