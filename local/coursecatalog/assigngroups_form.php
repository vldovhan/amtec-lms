<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class catalog_assigncohorts_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB, $CFG, $PAGE, $USER;

        $mform = $this->_form;

        $catalog = $this->_customdata['data'];
        
        $options = array();
        
        if (has_capability('local/manager:manageallcohorts', context_system::instance())){
            $cohorts = $DB->get_records_sql("SELECT c.id, c.name FROM {cohort} c LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id WHERE c.visible > 0 AND c.id NOT IN (SELECT DISTINCT cohortid FROM {local_catalog_users} WHERE catalogid = $catalog->id) ORDER BY name ASC");    
        } elseif (has_capability('local/manager:manageowncohorts', context_system::instance())){
            $cohorts = $DB->get_records_sql("SELECT c.id, c.name FROM {cohort} c LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id WHERE c.visible > 0 AND c.id NOT IN (SELECT DISTINCT cohortid FROM {local_catalog_users} WHERE catalogid = $catalog->id) AND cm.userid = $USER->id ORDER BY name ASC");
        } else {
            $cohorts = array();
        }
        
        if (count($cohorts)){
            foreach($cohorts as $cohort){
                $options[$cohort->id] = "$cohort->name";
            }
        }
        
        $mform->addElement('header', 'assignplans', get_string("selectgroups", 'local_coursecatalog'), array('class'=>'collapsed'));

        $mform->addElement('select', 'cohorts', '', $options, array('multiple'=>'multiple', 'class'=>'multi-select'));

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out_as_local_url());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($catalog);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

