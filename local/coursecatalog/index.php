<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

require($CFG->dirroot.'/local/coursecatalog/locallib.php');
require_once($CFG->libdir.'/adminlib.php');

$view = optional_param('view', 1, PARAM_INT);
$download = optional_param('download', '', PARAM_ALPHA);

require_login();
coursecatalog_enable();

$context = context_system::instance();
$manager = has_capability('local/coursecatalog:manage', $context);
$canassign = has_capability('local/coursecatalog:assign', $context);

require_capability('local/coursecatalog:view', $context);

$USER->editing = 0;
$title = get_string('managecatalog', 'local_coursecatalog');
$PAGE->set_url('/local/coursecatalog/index.php', array('view'=>$view));
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new coursecatalog_table('table', $view);
$table->show_download_buttons_at(array());
$table->is_downloading($download, get_string('courses'), get_string('courses'));
$table->is_collapsible = false;

if (!$table->is_downloading()) {
  	echo $OUTPUT->header();
	echo $OUTPUT->heading($title);
    
    echo html_writer::start_tag('div', array('class'=>'clearfix'));
    
    echo html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/edit.php'), html_writer::tag('i', '', array('class'=>'fa fa-plus')).get_string('create', 'local_coursecatalog'), array('class'=>'btn btn-warning', 'style'=>'float:right; margin-left:5px;'));
    
	echo html_writer::start_tag("form",  array("action"=>$PAGE->url, 'style'=>'float:right'));
		echo html_writer::start_tag('select', array('name'=>'view', 'onchange'=>'this.form.submit()'));
		$options = array(2=>get_string('all', 'local_coursecatalog'), 1=>get_string('active', 'local_coursecatalog'), 0=>get_string('inactive', 'local_coursecatalog'));
		foreach ($options as $key => $value) {
			$params = array('value'=>$key);
			if($view == $key){
				$params['selected'] = 'selected';
			}
			echo html_writer::tag('option',$value, $params);
		}
		echo html_writer::end_tag('select');
	echo html_writer::end_tag("form");
                                
    echo html_writer::end_tag("div");
}

$table->out(20, true);

if (!$table->is_downloading()) {
    echo $OUTPUT->footer();
}
