<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class plan_assign_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB;

        $mform = $this->_form;
        $catalog = $this->_customdata['data'];
        $options = array();
        $defaults = array();

        $plans = $DB->get_records_sql("SELECT id, name FROM {local_plans} WHERE visible = 1 AND type = 0 AND id NOT IN (SELECT planid FROM {local_catalog_courses} WHERE catalogid=$catalog->id AND planid > 0) ORDER BY name ASC");

        foreach($plans as $p){
            $options[$p->id] = $p->name;
        }
        $mform->addElement('header', 'assignplans', get_string("selectplans", 'local_coursecatalog'), array('class'=>'collapsed'));

        $mform->addElement('select', 'plans', '', $options, array('multiple'=>'multiple', 'class'=>'multi-select'));
        $mform->setDefault('plans', $defaults);


        $mform->addElement('hidden', 'type');
        $mform->setType('type', PARAM_INT);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out_as_local_url());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($catalog);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

