<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

function delete_catalog_cohorts($cohortid, $catalogid){
    global $DB;

    $DB->delete_records('local_catalog_users', array('catalogid'=> $catalogid,'cohortid'=>$cohortid));
}
function delete_catalog_users($userid, $catalogid){
    global $DB;

    $DB->delete_records('local_catalog_users', array('catalogid'=> $catalogid,'userid'=>$userid));
}
function delete_catalog_plans($planid, $catalogid){
    global $DB;

    if (has_capability('local/coursecatalog:manage', context_system::instance())){
        $DB->delete_records('local_catalog_courses', array('planid'=> $planid,'catalogid'=> $catalogid));
    }
}
function delete_catalog_course($catalogid, $courseid){
    global $DB;

    if (has_capability('local/coursecatalog:manage', context_system::instance())){
        $catalog = $DB->get_record('local_coursecatalog', array('id'=> $catalogid));
        $DB->delete_records('local_catalog_courses', array('catalogid'=> $catalogid,'courseid'=> $courseid));
    }
}
function delete_catalog($catalogid){
	global $DB;

    if (has_capability('local/coursecatalog:manage', context_system::instance())){
        $catalog = $DB->get_record('local_coursecatalog', array('id'=> $catalogid));
        $DB->delete_records('local_catalog_users', array('catalogid'=> $catalogid));
        $DB->delete_records('local_catalog_courses', array('catalogid'=> $catalogid));
        $DB->delete_records('local_coursecatalog', array('id'=> $catalogid));
    }
}

function save_catalog_cohorts($data){
    global $DB, $CFG;

    if(empty($data->cohorts)){
        return false;
    }
    foreach($data->cohorts as $cohort){
        $d = new stdClass();
        $d->catalogid = $data->id;
        $d->cohortid = $cohort;
        $d->timecreated = time();
        $DB->insert_record('local_catalog_users', $d);
    }
}

function save_catalog_users($data){
    global $DB, $CFG;

    if(empty($data->users)){
        return false;
    }
    foreach($data->users as $user){
        $d = new stdClass();
        $d->catalogid = $data->id;
        $d->userid = $user;
        $d->timecreated = time();
        $DB->insert_record('local_catalog_users', $d);
    }
}

function save_catalog_plans($data){
    global $DB, $CFG;

    if($data->id){
        if(empty($data->plans)){
            return false;
        }
        foreach($data->plans as $p){
            $d = new stdClass();
            $d->catalogid = $data->id;
            $d->planid = $p;
            $d->timecreated = time();
            $DB->insert_record('local_catalog_courses', $d);
        }
    }
}

function save_catalog_courses($data){
	global $DB, $CFG;

	if($data->id){
		if(empty($data->courses)){
			return false;
		}
		foreach($data->courses as $course){
			$d = new stdClass();
			$d->catalogid = $data->id;
            $d->courseid = $course;
			$d->timecreated = time();
			$DB->insert_record('local_catalog_courses', $d);
		}
	}
}

function coursecatalog_enable()
{
    global $OUTPUT, $PAGE;

    $enabled = get_config('local_coursecatalog', 'enabled');

    if(!$enabled){
        $PAGE->set_context(context_system::instance());
        $PAGE->set_heading(get_string('pluginname', 'local_coursecatalog'));

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('pluginname', 'local_coursecatalog'));
        $url = new moodle_url($CFG->wwwroot.'/index.php');
        echo $OUTPUT->confirm(get_string('returntohome', 'local_transcripts'), $url, $url);
        echo $OUTPUT->footer();
        die;
    }
}
function save_catalog($data){
	global $DB;

	if($data->id){
        $data->timemodified = time();
        $DB->update_record('local_coursecatalog', $data);
	    return $data->id;
	}else{
		$data->timemodified = time();
		return $DB->insert_record('local_coursecatalog', $data);
	}
}

class catalog_cohorts_table extends table_sql {

    function __construct($uniqueid, $catalog) {
        global $CFG, $PAGE, $USER;

        parent::__construct($uniqueid);

        $columns = array('name','users', 'timecreated', 'actions');
        $headers = array(get_string('name'), get_string('users'), get_string('assigned', 'local_coursecatalog'), get_string('actions', 'local_coursecatalog'));

        $this->define_headers($headers);
        $this->define_columns($columns);
        
        $fields = "DISTINCT c.name, cmu.users, cu.timecreated, cu.catalogid, '' as actions";
        $from = "{local_catalog_users} cu 
                    LEFT JOIN {cohort} c ON c.id = cu.cohortid
                    LEFT JOIN (SELECT cohortid, COUNT(id) as users FROM {cohort_members}) cmu ON cmu.cohortid = c.id
                    LEFT JOIN {cohort_members} cm ON cm.cohortid = c.id ";
        $where = "cu.catalogid = $catalog->id AND c.visible > 0";
        
        if (has_capability('local/manager:manageallcohorts', context_system::instance())){
        } elseif (has_capability('local/manager:manageowncohorts', context_system::instance())){
            $where .= " AND cm.userid = $USER->id";
        } else {
            $where .= " AND c.id = 0";
        }

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assigngroups.php', array('id' => $values->catalogid, 'delete'=>1, 'userid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

    }
}

class catalog_users_table extends table_sql {

    function __construct($uniqueid, $catalog) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('firstname','lastname', 'email', 'timecreated', 'actions');
        $headers = array(get_string('firstname'), get_string('lastname'), get_string('email'), get_string('assigned', 'local_coursecatalog'), get_string('actions', 'local_coursecatalog'));

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT u.id, u.firstname, u.lastname, cu.timecreated, cu.catalogid, email, '' as actions";
        $from = "{local_catalog_users} cu, {user} u";
        $where = "u.id = cu.userid AND cu.catalogid = $catalog->id";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assignusers.php', array('id' => $values->catalogid, 'delete'=>1, 'userid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

    }
}

class catalog_plans_table extends table_sql {

    function __construct($uniqueid, $catalog) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('name', 'timecreated', 'actions');
        $headers = array(get_string("plan", 'local_coursecatalog'), get_string('assigned', 'local_coursecatalog'), get_string('actions', 'local_coursecatalog'));

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT p.id, p.name, pc.timecreated, pc.catalogid, '' as actions";
        $from = "{local_plans} p, {local_catalog_courses} pc";
        $where = "p.id = pc.planid AND pc.catalogid = $catalog->id";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assignplans.php', array('id' => $values->catalogid, 'delete'=>1,'planid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

    }
}

class catalog_courses_table extends table_sql {
    var $index = 1;
    function __construct($uniqueid, $catalog) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('course', 'timecreated', 'actions');
        $headers = array(get_string('course'), get_string('assigned', 'local_coursecatalog'), get_string('actions', 'local_coursecatalog'));

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT c.id, c.fullname as course, cc.catalogid, cc.timecreated, '' as asctions";
        $from = "{course} c, {local_catalog_courses} cc";
        $where = "c.category > 0 AND c.visible = 1 AND c.id = cc.courseid AND cc.catalogid = $catalog->id ORDER BY cc.timecreated ASC";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);
        $html = "";
       
        $html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assigncourses.php', array('id' => $values->catalogid, 'delete'=>1,'courseid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

        return $html;

    }
}
class coursecatalog_table extends table_sql {
    function __construct($uniqueid, $view = 1) {
		global $CFG, $PAGE, $USER;

        parent::__construct($uniqueid);

        $columns = array('name', 'description');
        $headers = array(get_string('name', 'local_coursecatalog'), get_string('description', 'local_coursecatalog'));

        if (get_config('local_plans', 'enabled_plans')){
            $columns[] = 'plans';
            $headers[] = get_string('plans', 'local_coursecatalog');
        }
        $columns[] = 'courses';
        $headers[] = get_string('courses');
        $columns[] = 'users';
        $headers[] = get_string('users', 'local_coursecatalog');
        $columns[] = 'groups';
        $headers[] = get_string('groups', 'local_coursecatalog');
        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_coursecatalog');


        $this->define_headers($headers);
        $this->define_columns($columns);

        $where = 'c.id > 0';
        $where .= ($view == 2)?'':" AND c.visible =$view";
        $fields = "c.*, cc.courses, '' as actions, cp.plans, u.users, g.groups";
        $from = "{local_coursecatalog} c
            LEFT JOIN (SELECT catalogid, count(id) as plans FROM {local_catalog_courses} WHERE planid > 0 GROUP BY catalogid) cp ON cp.catalogid = c.id
            LEFT JOIN (SELECT catalogid, COUNT(distinct courseid) as courses FROM {local_catalog_courses} WHERE courseid > 0 GROUP BY catalogid) cc ON cc.catalogid = c.id
        	LEFT JOIN (SELECT cu.catalogid, COUNT(cu.userid) as users FROM {local_catalog_users} cu LEFT JOIN {user} u ON u.id = cu.userid WHERE u.deleted = 0 AND u.suspended = 0 GROUP BY catalogid) u ON u.catalogid = c.id";
        
            if (has_capability('local/manager:manageallcohorts', context_system::instance())){
                $from .= " LEFT JOIN (SELECT catalogid, COUNT(cohortid) as groups FROM {local_catalog_users} WHERE cohortid > 0 GROUP BY catalogid) g ON g.catalogid = c.id";
            } elseif (has_capability('local/manager:manageowncohorts', context_system::instance())){
                $from .= " LEFT JOIN (SELECT cu.catalogid, COUNT(cu.cohortid) as groups FROM {local_catalog_users} cu LEFT JOIN {cohort_members} cm ON cm.cohortid = cu.cohortid WHERE cu.cohortid > 0 AND cm.userid = $USER->id GROUP BY cu.catalogid) g ON g.catalogid = c.id";
            } else {
                $from .= " EFT JOIN (SELECT catalogid, COUNT(cohortid) as groups FROM {local_catalog_users} WHERE cohortid = 0 GROUP BY catalogid) g ON g.catalogid = c.id";
            }
        	
        
            /*if (!is_siteadmin()){
                $where .= " AND c.type = 'custom'";
            }*/

		    $this->set_sql($fields, $from, $where, array());
		    $this->define_baseurl($PAGE->url);
    }


    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $values->id);
        $showhideurl = new moodle_url($CFG->wwwroot.'/local/coursecatalog/edit.php', $urlparams + array('sesskey' => sesskey()));

		$context = context_system::instance();
		$manager = has_capability('local/coursecatalog:manage', $context);
        $canassign = has_capability('local/coursecatalog:assign', $context);
		
        if ((is_siteadmin() and $values->type == 'system') or $values->type == 'custom'){
            if ($manager) {
                if ($values->visible) {
                    $showhideurl->param('hide', 1);
                    $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('inactive'), 'class' => 'iconsmall'));
                    $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('inactive')));
                } else {
                    $showhideurl->param('show', 1);
                    $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'alt' => get_string('active'), 'class' => 'iconsmall'));
                    $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('inactive')));
                }

                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/edit.php', $urlparams),
                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
                    array('title' => get_string('edit')));
            }
            if ($canassign){
                if (get_config('local_plans', 'enabled_plans')){
                    $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assignplans.php', $urlparams),
                        html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/folder'), 'alt' => get_string('enrolplans', 'local_coursecatalog'), 'class' => 'iconsmall')),
                        array('title' => get_string('enrolplans', 'local_coursecatalog')));
                }

                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assigncourses.php', $urlparams),
                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/course'), 'alt' => get_string('assigncourses', 'local_coursecatalog'), 'class' => 'iconsmall')),
                    array('title' => get_string('assigncourses', 'local_coursecatalog')));

                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assignusers.php', $urlparams),
                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/users'), 'alt' => get_string('assignusers', 'local_coursecatalog'), 'class' => 'iconsmall')),
                    array('title' => get_string('assignusers', 'local_coursecatalog')));

                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/assigngroups.php', $urlparams),
                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/groups'), 'alt' => get_string('assigngroups', 'local_coursecatalog'), 'class' => 'iconsmall')),
                    array('title' => get_string('assigngroups', 'local_coursecatalog')));
            }
            if ($manager) {
                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/coursecatalog/edit.php', $urlparams + array('delete' => 1)),
                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                    array('title' => get_string('delete')));
            }
        }
	    return implode(' ', $buttons);
    }
    
    function col_courses($values) {
      return intval($values->courses);
    }
    function col_users($values) {
      return intval($values->users);
    }
    function col_plans($values) {
      return intval($values->plans);
    }
    function col_groups($values) {
      return intval($values->groups);
    }
}

function catalog_get_user_catalogs($type = 'list', $userid = 0){
    global $CFG, $DB, $USER;
    $catalogs = array();
    $userid = ($userid) ? $userid : $USER->id;
    
    $mycatalogs = $DB->get_records_sql("SELECT lcc.* FROM {local_coursecatalog} lcc 
                                            LEFT JOIN {local_catalog_users} lcu ON lcu.catalogid = lcc.id
                                            LEFT JOIN {local_catalog_users} lcg ON lcg.catalogid = lcc.id
                                        WHERE lcc.visible > 0 AND (lcu.userid = $userid OR lcg.cohortid IN (SELECT cohortid FROM {cohort_members} WHERE userid = $userid))
                                            GROUP BY lcc.id ORDER BY lcc.name");
    if (count($mycatalogs)){
        if ($type == 'list'){
            foreach ($mycatalogs as $catalog){
                $catalogs[$catalog->id] = $catalog->name;
            }
        } else {
           $catalogs = $mycatalogs;
        }
    }
    
    return $catalogs;
}

