<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cohort related management functions, this file needs to be included manually.
 *
 * @package    core_cohort
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('locallib.php');
require('assignusers_form.php');
require_once($CFG->libdir.'/adminlib.php');

$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$type = optional_param('type', 0, PARAM_INT);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$userid = optional_param('userid', 0, PARAM_INT);


require_login();
coursecatalog_enable();
$context = context_system::instance();
require_capability('local/coursecatalog:assign', $context);
$USER->editing = 0;

$PAGE->set_url('/local/coursecatalog/assignusers.php', array('id'=>$id));
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);
$PAGE->requires->jquery();
$PAGE->requires->js('/theme/talentquest/javascript/jquery.multiple.select.js', true);
$PAGE->requires->css('/theme/talentquest/style/multiple-select.css', true);
$strheading = get_string("assignusers", 'local_coursecatalog');
$PAGE->navbar->add(get_string('managecatalog', 'local_coursecatalog'), new moodle_url('/local/coursecatalog/index.php'));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);


$catalog = $DB->get_record('local_coursecatalog', array('id'=>$id));
if (!$catalog->id) {
    redirect($returnurl);
}
$PAGE->url->param('id', $catalog->id);
if ($returnurl) {
    $returnurl = new moodle_url($returnurl);
} else {
    $returnurl = new moodle_url($CFG->wwwroot.'/local/coursecatalog/index.php');
}

if (optional_param('cancel', false, PARAM_BOOL)) {
    redirect($returnurl);
}

$editform = new catalog_assignusers_form(null, array('data'=>$catalog, 'returnurl'=>$returnurl));
if ($delete and $catalog->id and $userid) {
    delete_catalog_users($userid, $catalog->id);
    redirect($PAGE->url);
}

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    save_catalog_users($data);
    redirect($returnurl);
}

$table = new catalog_users_table('table', $catalog);
$table->show_download_buttons_at(array());
$table->is_downloading('', get_string('courses'), get_string('courses'));
$table->is_collapsible = false;


echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);

echo $editform->display();
$table->out(20, true);
echo $OUTPUT->footer();

