<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class catalog_assignusers_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB,$CFG, $PAGE;

        $mform = $this->_form;

        $catalog = $this->_customdata['data'];
        
        $options = array();
        $users = $DB->get_records_sql("SELECT id, firstname, lastname, email FROM {user} WHERE id > 1 and deleted = 0 AND confirmed = 1 AND suspended = 0 AND id NOT IN (SELECT DISTINCT u.id FROM {local_catalog_users} cu, {user} u WHERE
            cu.catalogid = $catalog->id AND u.id > 1 AND u.deleted = 0 AND u.confirmed = 1 AND u.suspended = 0 AND u.id = cu.userid) ORDER BY firstname ASC");
        foreach($users as $user){
            $options[$user->id] = "$user->firstname $user->lastname";
        }
        
        $mform->addElement('header', 'assignplans', get_string("selectusers", 'local_coursecatalog'), array('class'=>'collapsed'));

        $mform->addElement('select', 'users', '', $options, array('multiple'=>'multiple', 'class'=>'multi-select'));

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out_as_local_url());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($catalog);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

