<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage plans
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_coursecatalog', get_string('pluginname', 'local_coursecatalog'));
$ADMIN->add('localplugins', $settings);

if ($ADMIN->locate('localplugins') and $ADMIN->locate('root')){
  if(get_config('local_coursecatalog', 'enabled')){
    $ADMIN->add('root', new admin_category('coursecatalog', get_string('pluginname', 'local_coursecatalog')));
  }

}

$name = 'local_coursecatalog/enabled';
$title = get_string('enabled', 'local_coursecatalog');
$description = get_string('enabled_desc', 'local_coursecatalog');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$settings->add($setting);

