<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage coursecatalog
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Course Catalog';
$string['defaultsettings'] = 'Default Settings';
$string['defaultenable'] = 'Enable Default Settings';
$string['managecatalog'] = 'Manage Course Catalog';

$string['coursecatalog:view'] = 'Course Catalog Dashboard';
$string['coursecatalog:manage'] = 'Course Catalog Manage';
$string['coursecatalog:edit'] = 'Course Catalog edit';
$string['coursecatalog:assign'] = 'Course Catalog assign users and courses';
$string['coursecatalog:myaddinstance'] = "Course Catalog";
$string['coursecatalog:managesystem'] = "Manage System Catalogs";

$string['enabled'] = 'Enable Course Catalog';
$string['enabled_desc'] = '';

$string['create'] = 'Create New Catalog';
$string['all'] = 'All';
$string['active'] = 'Active';
$string['inactive'] = 'Inactive';
$string['name'] = 'Catalog Name';
$string['users'] = 'Users';
$string['description'] = 'Description';
$string['plans'] = 'Learning Plans';
$string['plan'] = 'Learning Plan';
$string['groups'] = 'User Groups';
$string['actions'] = 'Actions';
$string['edit'] = 'Edit Course Catalog';
$string['selecttype'] = 'Select type';
$string['system'] = 'System';
$string['custom'] = 'Tenant';
$string['catalogtype'] = 'Catalog Type';
$string['color'] = 'Catalog Color';
$string['deletecatalog'] = 'Delete Catalog';
$string['deletecatalogmsg'] = 'Do you really want to delete catalog \'{$a}\'?';
$string['allcategories'] = 'All categories';
$string['assigngroups'] = 'Assign groups';
$string['returntohome'] = 'Return to home page';
$string['assigncourses'] = 'Assign courses';
$string['assignusers'] = 'Assign users';
$string['selectusers'] = 'Select Users';
$string['selectplans'] = 'Select Learning Plans';
$string['selectcourses'] = 'Select Courses';
$string['selectgroups'] = 'Select User Groups';
$string['enrolusers'] = 'Enrol users';
$string['enrolplans'] = 'Assign learning plans';
$string['availability'] = 'Availability restriction';
$string['expby'] = 'Expiration';
$string['number'] = 'Certification Period';
$string['type'] = ' ';
$string['renumber'] = 'Retraining Period After Expiration';
$string['retype'] = ' ';
$string['timeoptions'] = 'Expiration option';
$string['timeoptions2'] = 'Retraining Options';
$string['category'] = 'Category';
$string['nocourses'] = 'No Courses Assigned';
$string['courses'] = 'Courses';
$string['plandescription'] = 'Description';
$string['startplan'] = 'Start Plan';
$string['planprogress'] = 'Progress';
$string['plans:enrol'] = 'Enroll into plan';
$string['plans:manage'] = 'Manage plan';
$string['plans:unenrol'] = 'Unenroll from plan';
$string['enable_selfenrollment'] = 'Enable self-enrollment';
$string['search'] = 'Search...';
$string['filter'] = 'Filter';
$string['assigned'] = 'Assigned';
