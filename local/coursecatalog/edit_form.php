<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->libdir. '/coursecatlib.php');

class catalog_edit_form extends moodleform {

    /**
     * Define the catalog edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $editoroptions = $this->_customdata['editoroptions'];
        $catalog = $this->_customdata['data'];

        $mform->addElement('text', 'name', get_string('name'), 'maxlength="254" size="50"');
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);
        
        /*if (is_siteadmin()){
            $options = array(''=>get_string('selecttype', 'local_coursecatalog'), 'system'=>get_string('system', 'local_coursecatalog'), 'custom'=>get_string('custom', 'local_coursecatalog'));
            $mform->addElement('select', 'type', get_string('catalogtype', 'local_coursecatalog'), $options);
            $mform->addRule('type', get_string('required'), 'required', null);
            $mform->setType('type', PARAM_RAW);
        } else {*/
            $mform->addElement('hidden', 'type', 'custom');
            $mform->setType('type', PARAM_RAW);
        //}
        
        /*$mform->addElement('text', 'color', get_string('color', 'local_coursecatalog'), 'maxlength="100" size="10" class="ColorPicker"');
        $mform->setType('color', PARAM_RAW);*/
        
        $mform->addElement('advcheckbox', 'visible', get_string('active'));
        $mform->setDefault('visible', 1);
        
        $mform->addElement('editor', 'description_editor', get_string('description'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($catalog);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

