<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

function get_announcements(){
    global $CFG, $DB, $USER;
    
    $limit = (int)get_config('local_sb_announcements', 'number');
    
    $announcements = $DB->get_records_sql(
        "SELECT a.*, f.filename 
            FROM {local_sb_announcements} a 
                LEFT JOIN {files} f ON f.itemid = a.id AND f.component = 'local_sb_announcements' AND f.filearea = 'imagefile' AND f.mimetype IS NOT NULL
                LEFT JOIN {local_kaltura_ann_view} av ON av.instanceid = a.id AND av.userid = $USER->id 
            WHERE a.startdate <= ".time()." AND (a.enddate = 0 OR a.enddate >= ".time().") AND a.state > 0 AND av.id IS NULL ORDER BY a.startdate ASC, a.id DESC LIMIT 0, $limit");
    
    return $announcements;
}

function get_allannouncements_count(){
    global $CFG, $DB, $USER;
    
    $announcements = $DB->get_record_sql(
        "SELECT COUNT(a.id) as count
            FROM {local_sb_announcements} a 
            WHERE a.startdate <= ".time()." AND (a.enddate = 0 OR a.enddate >= ".time().") AND a.state > 0");
    
    return ($announcements->count) ? $announcements->count : 0;
}

function cleanup_announcements(){
    global $DB, $CFG;

    $pluginconfig = get_config('local_sb_announcements', 'cleanup');

    if ((int)$pluginconfig > 0){
        $cleanuptime = time() + ((int)$pluginconfig * 86400);
        $sql_count = "SELECT COUNT(id) as count
                        FROM {local_sb_announcements}
                    WHERE enddate < ".$cleanuptime." AND enddate > 0";
        $count = $DB->get_record_sql($sql_count);

        $sql_delete = "DELETE
                            FROM {local_sb_announcements}
                        WHERE enddate < ".$cleanuptime." AND enddate > 0";
        $DB->execute($sql_delete);
    }
    
    return ($count->count) ? $count->count : 0;
}

function local_sb_announcements_cron(){
    global $CFG, $DB;
    // We are going to measure execution times
    $starttime =  microtime();

    mtrace('Announcements CRON START...');

    $count = cleanup_announcements();
    mtrace('    '.$count . ' announcements removed (took ' . microtime_diff($starttime, microtime()) . ' seconds)');

    mtrace('Announcements CRON DONE.');

    // And return $status
    return true;
}


