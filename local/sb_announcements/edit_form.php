<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $announcement;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform          = $this->_form;
        $id             = $this->_customdata['id'];
        $announcement   = $this->_customdata['announcement']; // this contains the data of this form
        $editoroptions  = $this->_customdata['editoroptions']; // this contains the data of this form
        
        $systemcontext   = context_system::instance();
        
        $this->id  = $id;
        $this->context = $systemcontext;
        $this->announcement = $announcement;

        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text','title', get_string('title', 'local_sb_announcements'), 'maxlength="254"  size="50"');
        $mform->addRule('title', get_string('required_field', 'local_sb_announcements'), 'required', null, 'client');
        $mform->setType('title', PARAM_TEXT);
        
        if (is_siteadmin()){
            $mform->addElement('select', 'type', 'Announcement type:', array(''=>'Select type...', 'course'=>'Course', 'system'=>'System'), ((isset($announcement->id) and $announcement->id > 0) ? 'disabled="disabled"' : ''));
		    $mform->setType('type', PARAM_RAW);    
        } else {
            $mform->addElement('hidden', 'type', 'course');
            $mform->setType('type', PARAM_RAW);
        }
        
        $mform->addElement('select', 'color', 'Announcement color:', array(''=>'Select color...', 'info'=>'Blue', 'warning'=>'Orange', 'danger'=>'Red', 'green'=>'Green'));
        $mform->setType('color', PARAM_RAW); 
        
        
        $mform->addElement('date_time_selector', 'startdate', get_string('startdate', 'local_sb_announcements'));
        $mform->setDefault('startdate', time());
        $mform->addRule('startdate', get_string('required_field', 'local_sb_announcements'), 'required', null, 'client');
        
        $mform->addElement('date_time_selector', 'enddate', get_string('enddate', 'local_sb_announcements'), array('optional'=>true));
        $mform->setDefault('enddate', time()+3600*24);
        
        $group = array();
		$available_courses = $this->announcements_get_courses();
		foreach ($available_courses as $cid=>$cname){
			$group[] = $mform->createElement('checkbox', 'courses['.$cid.']', '', $cname, ((isset($announcement->id) and $announcement->id > 0) ? 'disabled="disabled"' : ''));
		}
		$mform->addGroup($group, 'coursegrp', 'Courses:', null, false);
		if (isset($announcement->id) and $announcement->type and $announcement->type == 'course' and $announcement->data != ''){
			$scourses = unserialize($announcement->data);
			foreach ($scourses as $citem){
				$mform->setDefault('courses['.$citem.']', 1);
			}
		}
        
        if (is_siteadmin()){
            $group = array();
            $roles = $this->announcements_get_roles();
            foreach ($roles as $roleid=>$rolename){
                $group[] = $mform->createElement('checkbox', 'roles['.$roleid.']', '', $rolename, ((isset($announcement->id) and $announcement->id > 0) ? 'disabled="disabled"' : ''));
            }
            $mform->addGroup($group, 'rolesgrp', 'Roles:', null, false);
            if (isset($announcement->id) and $announcement->data != ''){
                $sroles = unserialize($announcement->data);
                foreach ($sroles as $ritem){
                    $mform->setDefault('roles['.$ritem.']', 1);
                }
            }
        }
        
        $mform->addElement('editor', 'body_editor', get_string('body', 'local_sb_announcements'), null, $editoroptions);
        $mform->setType('body_editor', PARAM_RAW);
        $mform->addRule('body_editor', get_string('required_field', 'local_sb_announcements'), 'required', null, 'client');
        
        $choices = array();
        $choices['0'] = get_string('hide');
        $choices['1'] = get_string('show');
        $mform->addElement('select', 'state', get_string('visibility', 'local_sb_announcements'), $choices);
        $mform->setDefault('state', 1);
        $mform->setType('state', PARAM_INT);
        
        $mform->addElement('checkbox', 'send', 'Send notification');
        $mform->setDefault('send', 0);
        $mform->setType('send', PARAM_INT);
    
        $this->add_action_buttons(get_string('cancel'), (($id > 0) ? get_string('save', 'local_sb_announcements') : get_string('create', 'local_sb_announcements')));

        // Finally set the current form data
        $this->set_data($announcement);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
    
    function announcements_get_roles(){
		global $DB, $USER;
		$db_roles = $DB->get_records_sql("SELECT * FROM {role} WHERE status = 1 ORDER BY sortorder ASC");
		list($assignableroles, $assigncounts, $nameswithcounts) = get_assignable_roles(context_system::instance(), ROLENAME_BOTH, true);
        
		$roles = array();
		foreach ($db_roles as $role){
			if ((has_capability('moodle/role:assign', context_system::instance()) and isset($assignableroles[$role->id])) || is_siteadmin()) {
				$roles[$role->id] = $role->name;
			}
		}
		return $roles;
	}
    
    function announcements_get_courses(){
		global $DB, $USER;
		$courses = array();
	    $db_courses = enrol_get_my_courses('', 'fullname');
        
		if (count($db_courses) > 0){
			foreach ($db_courses as $course){
                if(!has_capability('moodle/course:manageactivities', context_course::instance($course->id))) continue;
				$courses[$course->id] = $course->fullname;
			}
		}
		return $courses;
	}
}

