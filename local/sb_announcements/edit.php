<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../config.php');
require_once('lib.php');
require_once('edit_form.php');

$systemcontext   = context_system::instance();
require_login();
//require_capability('local/sb_announcements:manage', $systemcontext);

$id     = optional_param('id', 0, PARAM_INT); // Announcement id.
$action = optional_param('action', '', PARAM_RAW); // Announcement action.

$PAGE->set_pagelayout('admin');
$pageparams = array('id' => $id);
$PAGE->set_url('/local/sb_announcements/edit.php', $pageparams);

if ($action == 'delete'){
	$announcement = $DB->get_record('local_sb_announcements', array('id'=>$id));
	if ($announcement->id){
		$DB->delete_records('local_sb_announcements', array('parentid'=>$announcement->id));
		$DB->delete_records('local_sb_announcements', array('id'=>$announcement->id));
	}
	$url = new moodle_url($CFG->wwwroot.'/local/sb_announcements/announcements.php');
    redirect($url);
} elseif(($action == 'hide' or $action == 'show') and $id){
    $announcement = $DB->get_record('local_sb_announcements', array('id'=>$id));
    $announcements = $DB->get_records('local_sb_announcements', array('parentid'=>$id));
    if ($announcement){
        $announcement->state = ($action == 'show') ? 1 : 0;
        $DB->update_record('local_sb_announcements', $announcement);
    }
    if (count($announcements)){
        foreach ($announcements as $announcement){
            $announcement->state = ($action == 'show') ? 1 : 0;
            $DB->update_record('local_sb_announcements', $announcement);
        }
    }
    redirect(new moodle_url("/local/sb_announcements/announcements.php"));
} elseif ($action == 'read'){
	$announcement = $DB->get_record('local_sb_announcements', array('id'=>$id));
	if ($announcement->id){
		$announcement->new = 0;
		$DB->update_record('local_sb_announcements', $announcement);
	}
	$url = new moodle_url($CFG->wwwroot.'/local/sb_announcements/announcements.php');
    redirect($url);
}

// Prepare course and the editor.
$editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$systemcontext, 'subdirs'=>0);
if ($id > 0) {
    
    $announcement = $DB->get_record('local_sb_announcements', array('id'=>$id));
    
} else {
    
    $announcement = new stdClass();
    $announcement->id = null;
    
}

$announcement = file_prepare_standard_editor($announcement, 'body', $editoroptions, $systemcontext, 'local_sb_announcements', 'bodyfile', $announcement->id);

// First create the form.
$args = array(
    'id' => $id,
    'announcement' => $announcement,
    'editoroptions' => $editoroptions
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/sb_announcements/announcements.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    
    // save and relink embedded images and save attachments
    $data = file_postupdate_standard_editor($data, 'body', $editoroptions, $systemcontext, 'local_sb_announcements', 'bodyfile', $announcement->id);
    
    if ($data->id > 0) {
		$announcement = $DB->get_record('local_sb_announcements', array('id'=>$data->id));
		if ($announcement->id){
			$announcement->startdate = $data->startdate;
			$announcement->enddate = $data->enddate;
			$announcement->title = $data->title;
			$announcement->color = $data->color;
			$announcement->body = $data->body;
			$DB->update_record('local_sb_announcements', $announcement);
			$child_ann = $DB->get_records('local_sb_announcements', array('parentid'=>$announcement->id));
			if (count($child_ann) > 0){
				foreach ($child_ann as $item){
					$item->startdate = $data->startdate;
					$item->enddate = $data->enddate;
					$item->title = $data->title;
					$item->body = $data->body;
					$DB->update_record('local_sb_announcements', $item);
				}
			}
		}
	} else {
		if ($data->type == 'course' and count($data->courses) > 0){
			$courses = array();
			foreach ($data->courses as $key=>$val){
				$courses[] = $key;
			}
			$data->data = serialize($courses);
			$data->userto = $USER->id;
			$data->userfrom = $USER->id;
			$data->timecreated = time();
			
			$data->parentid = $DB->insert_record('local_sb_announcements', $data);
            
			foreach ($courses as $course){
				$data->data = $course;
                
                $users = $DB->get_records_sql("SELECT u.*
                    FROM {$CFG->prefix}user_enrolments as ue
                        LEFT JOIN {$CFG->prefix}user as u ON u.id = ue.userid
                        LEFT JOIN {$CFG->prefix}enrol as e ON e.id = ue.enrolid
                        LEFT JOIN {$CFG->prefix}course as c ON c.id = e.courseid
                        LEFT JOIN {$CFG->prefix}context as cx ON cx.instanceid = e.courseid
                        LEFT JOIN {$CFG->prefix}role_assignments ra ON ra.contextid = cx.id AND ra.userid = ue.userid 
                            WHERE e.courseid = $course and u.id > 2 AND u.deleted = 0 GROUP BY ue.userid, e.courseid");
                
				if (count($users) > 0){
					foreach ($users as $user){
						$data->userto = $user->id;
						$DB->insert_record('local_sb_announcements', $data);
					}
				}
			}
		} elseif ($data->type == 'system' and count($data->roles) > 0){
            $roles = array();
			foreach ($data->roles as $key=>$val){
				$roles[] = $key;
			}
			$data->data = serialize($roles);
			$data->userto = $USER->id;
			$data->userfrom = $USER->id;
			$data->timecreated = time();
			
			$data->parentid = $DB->insert_record('local_sb_announcements', $data);
			$data->data = '';
            $users = array();
            
            if (count($roles)){
                $sql = "SELECT DISTINCT u.id
							FROM  {user} u
						LEFT JOIN {role_assignments} ra ON ra.userid = u.id 
							WHERE u.id > 2 AND u.deleted = 0 AND ra.roleid IN (".implode(', ', $roles).")";
            	$users = $DB->get_records_sql($sql);
			}
            
            if (count($users)){
                foreach ($users as $user){
                    $data->userto = $user->id;
                    $DB->insert_record('local_sb_announcements', $data);
                }
            }
		}
	}
    
    if ($data->send){
        require_once($CFG->dirroot.'/local/notifications/lib.php');
        $params = array(); $item = array();
    }
    
    redirect(new moodle_url('/local/sb_announcements/announcements.php'));
}

// Print the form.

$site = get_site();
$title = ($id > 0) ? get_string('announcementsedit', 'local_sb_announcements') : get_string('announcementscreate', 'local_sb_announcements');

$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer(); ?>
<style>
.mform .felement.fgroup span {display: inline-block; width: 45%; max-height:40px; min-height:25px; overflow:hidden; vertical-align:top;}
.mform .felement.fgroup span span {display: inline;width: auto;}
</style>
<script>
    <?php if (is_siteadmin()) : ?>
        <?php if (isset($announcement->type) and $announcement->type == 'course') : ?>
            jQuery('#fgroup_id_rolesgrp').addClass('hidden');
        <?php elseif (isset($announcement->type) and $announcement->type == 'system') : ?>
            jQuery('#fgroup_id_coursegrp').addClass('hidden');
        <?php else : ?>
            jQuery('#fgroup_id_coursegrp').addClass('hidden');
            jQuery('#fgroup_id_rolesgrp').addClass('hidden');    
        <?php endif; ?>
    <?php else : ?>
        jQuery('#fgroup_id_rolesgrp').addClass('hidden');    
    <?php endif; ?>
	jQuery('#id_type').change(function(event){
		var type = jQuery(this).val();
		if (type){
			if (type == 'course'){
				jQuery('#fgroup_id_coursegrp').removeClass('hidden');
                jQuery('#fgroup_id_rolesgrp').addClass('hidden');
			} else if (type == 'system') {
				jQuery('#fgroup_id_coursegrp').addClass('hidden');
                jQuery('#fgroup_id_rolesgrp').removeClass('hidden');
			}
		} else {
			jQuery('#fgroup_id_rolesgrp').addClass('hidden');
            jQuery('#fgroup_id_coursegrp').addClass('hidden');
		}
	});
	
</script>
<?php 