<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Announcements';
$string['announcements'] = 'Announcements';
$string['announcementsmanage'] = 'Manage announcements';
$string['announcementscreate'] = 'Create announcement';
$string['announcementsedit'] = 'Edit announcement';
$string['settings'] = 'Settings';
$string['announcementsdroot'] = 'Announcements';
$string['enabled'] = 'Enabled announcements';
$string['enabled_desc'] = 'Enable announcements';
$string['cleanup'] = 'Delete announcements after (days)';
$string['cleanup_desc'] = 'Delete announcements after expiration date (0 - do not delete)';
$string['number'] = 'Number of announcements';
$string['number_desc'] = 'Number of announcements to show on dashboard';
$string['order'] = 'Announcements ordering';
$string['order_desc'] = 'Show latest announcements or randomly';
$string['order_last'] = 'Latest announcements';
$string['order_random'] = 'Randomly';
$string['sb_announcements:view'] = 'Announcements view';
$string['sb_announcements:manage'] = 'Announcements manage'; 
$string['title'] = 'Announcement title'; 
$string['required_field'] = 'Field is required!'; 
$string['description'] = 'Announcement text'; 
$string['announcements_image'] = 'Announcement image'; 
$string['announcementdate'] = 'Announcement date'; 
$string['startdate'] = 'Publish start date'; 
$string['enddate'] = 'Publish end date'; 
$string['state'] = 'Publish state'; 
$string['visibility'] = 'Visibility'; 
$string['create_new'] = 'Сreate New'; 
$string['created'] = 'Created by'; 
$string['actions'] = 'Actions'; 
$string['showall'] = 'Show all'; 
$string['nothing'] = 'Nothing to display'; 
$string['YOUR_URI'] = 'YOUR_URI'; 
$string['YOUR_URI_TITLE'] = 'YOUR_URI_TITLE'; 
$string['save'] = 'Save'; 
$string['create'] = 'Create'; 
$string['search'] = 'Search'; 
$string['close'] = 'Close'; 
$string['addtofeed'] = 'Add to Feed'; 
$string['body'] = 'Body'; 
$string['type'] = 'Type'; 
$string['data'] = 'Data'; 
