<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class announcements_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);

        $columns = array('type', 'data', 'title', 'startdate', 'enddate', 'created', 'actions');
        $this->define_columns($columns);
        $headers = array(
            get_string('type', 'local_sb_announcements'),
            get_string('data', 'local_sb_announcements'),
            get_string('title', 'local_sb_announcements'),
            get_string('startdate', 'local_sb_announcements'),
            get_string('enddate', 'local_sb_announcements'),
            get_string('created', 'local_sb_announcements'),
            get_string('actions', 'local_sb_announcements'));
       
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (a.title LIKE '%$search%' OR a.body LIKE '%$search%' OR a.type LIKE '%$search%')" : "";
        $fields = "a.id, a.type, a.data, a.title, a.startdate, a.enddate, a.timecreated, CONCAT(u.firstname, ' ', u.lastname) as created, a.state, a.id as actions ";
        $from = "{local_sb_announcements} a LEFT JOIN {user} u ON u.id = a.userfrom";
        $where = 'a.id > 0 AND a.parentid = 0'.$sql_search;

        if (!is_siteadmin()){
            $where .= ' AND a.userfrom = '.$USER->id.' ';
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/sb_announcements/announcements.php");
    }
    
    function col_type($values) {
       return ($values->type) ? ucfirst($values->type) : '-';
    }
    function col_startdate($values) {
       return ($values->startdate) ? date("g:i a, d M Y", $values->startdate) : '-';
    }
    function col_enddate($values) {
       return ($values->enddate) ? date("g:i a, d M Y", $values->enddate) : '-';
    }
    function col_timecreated($values) {
       return ($values->timecreated) ? date("d M Y", $values->timecreated) : '-';
    }
    function col_data($values) {
        global $DB;
       $data = '';
        if ($values->type == 'course'){
            if ($values->data != ''){
                $data = '';
                $courses = unserialize($values->data);
                if ($courses and count($courses) > 0){
                    $ccourses = $DB->get_records_sql('SELECT * FROM {course} WHERE id IN ('.implode(', ', $courses).')');
                    if (count($ccourses) > 0){
                        $cn = array();
                        foreach ($ccourses as $course){
                            $cn[] = $course->fullname;
                        }
                        $data = implode(', ', $cn);
                    }
                } elseif ($values->data != ''){
                    $course = $DB->get_record_sql('SELECT * FROM {course} WHERE id = '.$values->data);
                    $data = $course->fullname;
                }
            }
        } elseif ($values->type == 'system'){
            if ($values->data != ''){
                $aroles = unserialize($values->data);
                if (count($aroles) > 0){
                    $roles = $DB->get_records_sql("SELECT * FROM {role} WHERE status = 1 AND id IN (".implode(', ', $aroles).") ORDER BY sortorder ASC");
                    if ($roles){
                        $r = array();
                        foreach ($roles as $role){
                            $r[] = $role->name;
                        }
                        $data = implode(', ', $r);
                    }
                }
            }
        }
				
        return $data;
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strshow  = get_string('show');
      $strhide  = get_string('hide');

        $edit = array();
        $aurl = new moodle_url('/local/sb_announcements/edit.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/sb_announcements/edit.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')));
        
        if ($values->state > 0){
            $aurl = new moodle_url('/local/sb_announcements/edit.php', array('action'=>'hide', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/sb_announcements/edit.php', array('action'=>'show', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall')));    
        }
        
      return implode('', $edit);
    }
}
