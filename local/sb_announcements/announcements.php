<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');
require ('announcements_table.php');
require_once('lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

$systemcontext   = context_system::instance();
require_login();
//require_capability('local/sb_announcements:manage', $systemcontext);
$title = get_string('announcementsmanage', 'local_sb_announcements');

$PAGE->set_url(new moodle_url("/local/sb_announcements/announcements.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context(context_system::instance());
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new announcements_table('table', $search);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'announcements-search-form'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_sb_announcements').' ...', 'value' => $search));
echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('search', 'local_sb_announcements')));
echo html_writer::end_tag("label");

$url = new moodle_url('/local/sb_announcements/edit.php');
echo html_writer::link($url, get_string('create_new', 'local_sb_announcements'), array('class'=>'btn'));

echo html_writer::end_tag("form");

$table->out(20, true);

echo $OUTPUT->footer();
