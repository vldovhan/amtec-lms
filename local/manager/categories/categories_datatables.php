<?php

define('AJAX_SCRIPT', true);

require_once('../../../config.php');

$id		= optional_param('id', 0, PARAM_INT);
$iDisplayStart = optional_param('iDisplayStart', 0, PARAM_INT);
$iDisplayLength = optional_param('iDisplayLength', 10, PARAM_INT);
$iSortingCols = optional_param('iSortingCols', '', PARAM_RAW);
$sSearch = optional_param('sSearch', '', PARAM_RAW);
$sSearch_0 = optional_param('sSearch_0', '', PARAM_RAW);
$sEcho = optional_param('sEcho', 0, PARAM_INT);

$systemcontext = context_system::instance();

$group = ' GROUP BY cat.id ';

$aColumns = array( 'id', 'name', /*'parentcategory',*/ 'coursecount', 'actions');
$bColumns = array('id'=>'cat.id', 'name'=>'cat.name', /*'parentcategory'=>'p.name',*/ 'coursecount'=>'cat.coursecount');

/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "id";

/* 
 * Paging
 */
$sLimit = "";
if ( isset( $iDisplayStart ) && $iDisplayLength != '-1' ){
	$sLimit = "LIMIT ".intval( $iDisplayStart ).", ".$iDisplayLength;
}

/*
 * Ordering
 */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) )
{
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
			$sOrder .= ((is_array($bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]])) ? $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]][0] : $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]]).
				($_GET['sSortDir_'.$i]==='asc' ? ' asc' : ' desc') .", ";
		}
	}
	
	$sOrder = substr_replace( $sOrder, "", -2 );
	if ( $sOrder == "ORDER BY" ){
		$sOrder = "";
	}
}

/* 
 * Filtering
 */

$sWhere = " WHERE cat.id > 0 ";
if ($sSearch_0 == '' or $sSearch_0 == 1){
    $sWhere .= " AND cat.visible = 1 ";
} elseif ($sSearch_0 != '-1'){
    $sWhere .= " AND cat.visible = $sSearch_0 ";
}

if ( $sSearch != "" ){
	$sWhere .= "AND (";
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ($aColumns[$i] == 'actions') continue;
		if (is_array($bColumns[$aColumns[$i]])){
			foreach ($bColumns[$aColumns[$i]] as $col){
				$sWhere .= $col." LIKE '%$sSearch%' OR ";
			}
		} else {
			$sWhere .= $bColumns[$aColumns[$i]]." LIKE '%$sSearch%' OR ";
		}
	}
	$sWhere = substr_replace( $sWhere, "", -3 );
	$sWhere .= ')';
}
$select = "SELECT DISTINCT
			cat.id, 
			cat.name,
			p.name as parentcategory,
			cat.type as categorytype,
			cat.visible,
			cat.coursecount";

if (!is_siteadmin()){
    $sWhere .= " AND (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) ";
}
$sql = "
	FROM {course_categories} cat
		LEFT JOIN {course_categories} p ON p.id = cat.parent";
    
//echo $select.$sql.$sWhere.' '.$group.' '.' '.$sOrder.' '.$sLimit;
$rResult = $DB->get_records_sql($select.$sql.$sWhere.' '.$group.' '.' '.$sOrder.' '.$sLimit);
$count_rResult = $DB->get_record_sql("SELECT COUNT(DISTINCT cat.id) as count ".$sql.$sWhere);

$iFilteredTotal = $count_rResult->count;
$iTotal = $count_rResult->count;

/*
 * Output
 */
$output = array(
	"sEcho" => $sEcho,
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);

foreach ($rResult as $aRow){
	$aRow = (array)$aRow;
	$row = array();
    
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( $aColumns[$i] == "version" ){
			/* Special output formatting for 'version' column */
			$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
		} elseif ( $aColumns[$i] != ' '){
			if ($aColumns[$i] == 'actions'){
                $edit_link = ''; $delete_link = ''; $showhide_link = '';
                if(is_siteadmin() or $aRow['categorytype'] == 'custom'){
                    $edit_link = '<a href="'.$CFG->wwwroot.'/course/editcategory.php?id='.$aRow['id'].'" title="'.get_string('edit').'" class="tb-actions"><i class="fa fa-pencil"></i></a>';
                
                    if ($aRow['id'] > 1){
                        $delete_link = '&nbsp;<a href="'.$CFG->wwwroot.'/course/management.php?sesskey='.sesskey().'&action=deletecategory&categoryid='.$aRow['id'].'" title="'.get_string('delete').'" class="tb-actions"><i class="fa fa-trash-o"></i></a>';
                    }
                    
                    $showhide_link = '&nbsp;<a href="javascript:void(0)"; class="tb-actions hideitem_'.$aRow['id'].((($aRow['visible'] > 0)) ? '' : ' hidden').'" onclick="toggleCategory('.$aRow['id'].', \'hidecategory\');" title="'.get_string('inactive').'"><i class="fa fa-eye"></i></a>';    
                    $showhide_link .= '&nbsp;<a href="javascript:void(0)"; class="tb-actions showitem_'.$aRow['id'].((($aRow['visible'] > 0)) ? ' hidden' : '').'" onclick="toggleCategory('.$aRow['id'].', \'showcategory\');" title="'.get_string('active').'"><i style="margin-left:-3px;" class="fa fa-eye-slash"></i></a>'; 
                }
                $row[] = $edit_link.$delete_link.$showhide_link;
			} else {
				$row[] = ($aRow[ $aColumns[$i] ]) ? $aRow[ $aColumns[$i] ] : '-';
			}
		}
	}
	$output['aaData'][] = $row;
}

echo json_encode( $output );

exit;