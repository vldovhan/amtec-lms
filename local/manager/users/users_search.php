<?php

define('AJAX_SCRIPT', true);

require_once('../../../config.php');

$id		= optional_param('id', 0, PARAM_INT);
$role		= optional_param('role', 0, PARAM_INT);
$iDisplayStart = optional_param('iDisplayStart', 0, PARAM_INT);
$iDisplayLength = optional_param('iDisplayLength', 0, PARAM_INT);
$iSortingCols = optional_param('iSortingCols', '', PARAM_RAW);
$sSearch = optional_param('search', '', PARAM_RAW);

$systemcontext = context_system::instance();

$group = '';
$aColumns = array('id', 'name', 'username', 'email', 'city', 'country', 'idnumber', 'roles', 'manager', 'actions');
$bColumns = array('id'=>'u.id', 'idnumber'=>'u.idnumber', 'name'=>array ('u.firstname', 'u.lastname'), 'username'=>'u.username', 'email'=>'u.email', 'city'=>'u.city', 'country'=>'u.country', 'roles'=>'r.name', 'manager'=>'m.manager');

/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "id";

/* 
 * Paging
 */
$sLimit = "";
if ( isset( $iDisplayStart ) && $iDisplayLength != '-1' ){
	$sLimit = "LIMIT ".intval( $iDisplayStart ).", ".$iDisplayLength;
}

/*
 * Ordering
 */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) ){
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
			$sOrder .= ((is_array($bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]])) ? $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]][0] : $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]]).
				($_GET['sSortDir_'.$i]==='asc' ? ' asc' : ' desc') .", ";
		}
	}
	
	$sOrder = substr_replace( $sOrder, "", -2 );
	if ( $sOrder == "ORDER BY" ){
		$sOrder = "";
	}
}

/* 
 * Filtering
 */

$sWhere = " WHERE u.id > 2 AND u.deleted = 0 ";

// see only own users
if (!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance())){
    $sWhere .= " AND u.managerid = $USER->id ";
}

if ( $sSearch != "" ){
	$sWhere .= "AND (";
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ($aColumns[$i] == 'actions' or $aColumns[$i] == 'active') continue;
		if (is_array($bColumns[$aColumns[$i]])){
			foreach ($bColumns[$aColumns[$i]] as $col){
				$sWhere .= $col." LIKE '%$sSearch%' OR ";
			}
		} else {
			$sWhere .= $bColumns[$aColumns[$i]]." LIKE '%$sSearch%' OR ";
		}
	}
	$sWhere = substr_replace( $sWhere, "", -3 );
	$sWhere .= ')';
}

$group = ' GROUP BY u.id ';

$select = "SELECT DISTINCT
            u.id, 
            u.idnumber,
            CONCAT(u.firstname, ' ', u.lastname) as name, 
            u.email,
            u.username,
            u.city, 
            u.country,
            GROUP_CONCAT(DISTINCT r.name ORDER BY r.name ASC SEPARATOR ', ') AS roles,
            m.manager";
$sql = "
    FROM  {user} u
        LEFT JOIN (SELECT r.name, ra.userid FROM {role_assignments} ra LEFT JOIN {role} r ON r.id = ra.roleid WHERE ra.contextid = 2 ORDER BY r.sortorder) r ON r.userid = u.id 
        LEFT JOIN (SELECT id, CONCAT(firstname, ' ', lastname) as manager FROM {user}) m ON m.id = u.managerid ";
        

//echo $select.$sql.$sWhere.' '.$group.' '.' '.$sOrder.' '.$sLimit; exit;
$rResult = $DB->get_records_sql($select.$sql.$sWhere.' '.$group.' '.' '.$sOrder.' '.$sLimit);
$count_rResult = $DB->get_record_sql("SELECT COUNT(DISTINCT u.id) as count ".$sql.$sWhere);

//$iFilteredTotal = count($rResult);
$iFilteredTotal = $count_rResult->count;
$iTotal = $count_rResult->count;
/*
 * Output
 */
$output = array(
	"sEcho" => intval($_GET['sEcho']),
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);

foreach ($rResult as $aRow){
	$aRow = (array)$aRow;
	$row = array();
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( $aColumns[$i] == "version" ){
			/* Special output formatting for 'version' column */
			$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
		} elseif ( $aColumns[$i] != ' '){
			if ($aColumns[$i] == 'country'){
                $row[] = (!empty($aRow[ $aColumns[$i] ])) ? get_string($aRow[ $aColumns[$i] ], 'countries') : '';
            } elseif ($aColumns[$i] == 'actions'){
                 $edit_link = ''; $delete_link = '';
                
                if (has_capability('moodle/user:update', $systemcontext)){
                    $edit_link = '&nbsp;<a href="'.$CFG->wwwroot.'/user/editadvanced.php?id='.$aRow['id'].'" title="'.get_string('edit').'"><i class="fa fa-pencil"></i></a>';
                }
				if (has_capability('moodle/user:delete', $systemcontext)){
					$delete_link = '&nbsp;<a href="javascript:void(0)" id="user_'.$aRow['id'].'" onclick="delete_user('.$aRow['id'].');" title="'.get_string('delete').'"><i class="fa fa-trash-o"></i></a>';
				}
				if (\core\session\manager::is_loggedinas() || !has_capability('moodle/user:loginas', $systemcontext)){
					$loginas_link = '';
				} else {
					$loginas_link = '&nbsp;<a href="javascript:void(0)" onclick="location=\''.$CFG->wwwroot.'/course/loginas.php?id=1&user='.$aRow['id'].'&sesskey='.sesskey().'\'" title="'.get_string('loginas', 'local_manager').'"><i class="fa fa-key"></i></a>';
				}
				$row[] = $edit_link.$delete_link.$loginas_link;
			} else {
				$row[] = $aRow[ $aColumns[$i] ];
			}
		}
        $row['DT_RowId'] = "row_".$aRow['id'];
	}
	$output['aaData'][] = $row;
}

echo json_encode( $output );

exit;