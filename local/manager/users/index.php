<?php

require_once('../../../config.php');

require_once($CFG->dirroot . '/' . $CFG->admin . '/roles/lib.php');
require_once($CFG->dirroot.'/user/editlib.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // user id
$action = optional_param('action', '', PARAM_RAW);

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:manageusers', $systemcontext);

$title = get_string('manageusers', 'local_manager');
$PAGE->set_url('/local/manager/users/index.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);

$roles = $DB->get_records_sql("SELECT * FROM {role} WHERE status = 1 AND type = 'system' ORDER BY sortorder ASC");
list($assignableroles, $assigncounts, $nameswithcounts) = get_assignable_roles($systemcontext, ROLENAME_BOTH, true);
$PAGE->set_pagelayout('admin');

/*if (!count($assignableroles)){
    $assignableroles[]
}*/


$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('div', array('class'=>'users-manage-box'));

?>
<div class="search-allusers-box">
    <label>
        <input type="text" id="all-search" class="all_search" value="" placeholder="<?php echo get_string('search', 'local_manager'); ?>" />
        <span class="search-actions"><i class="fa fa-times" style="display:none;" onclick="jQuery('.all_search').val('');"></i><i class="fa fa-search"></i></span>
    </label>    
</div>
<div class="tq-users-tabs">
	<div class="users-tabs-header nav-tabs-header">
		<ul class="nav-tabs nav-tabs-simple clearfix">
            <?php $i = 0; $first_role = 1; ?>
			<?php foreach ($roles as $role) : ?>
				<?php if (!is_siteadmin() and !isset($assignableroles[$role->id])) continue;?>
				<li class="<?php echo ($i == 0) ? 'active' : '' ?>" id="role_<?php echo $role->id; ?>"><a href="#roleid_<?php echo $role->id; ?>" data-toggle="tab"><?php echo $role->name; ?>s</a></li>
                <?php if ($i == 0){$first_role = $role->id;} $i++; ?>
			<?php endforeach; ?>
			<li id="role_0"><a href="#roleid_0" data-toggle="tab"><?php echo get_string('notassignedusers', 'local_manager'); ?></a></li>
			<li id="search-tab" class="hidden"><a href="#roleid_search" data-toggle="tab"><?php echo get_string('searchresults', 'local_manager'); ?></a></li>
		</ul>
	</div>
	<div class="users-tabs tab-content">
        <?php $i = 0; ?>
		<?php foreach ($roles as $role) : ?>
			<?php if (!is_siteadmin() and !isset($assignableroles[$role->id])) continue;?>
            <div class="users-tabs-item tab-pane<?php echo ($i == 0) ? ' active' : ''?>" id="roleid_<?php echo $role->id; ?>"></div>
            <?php $i++; ?>
		<?php endforeach; ?>
		<div class="users-tabs-item other-users-tab tab-pane" id="roleid_0">
		</div>
        <div class="users-tabs-item search-users-tab tab-pane" id="roleid_search">
        </div>
	</div>
</div>

<?php
echo html_writer::end_tag('div');
?>

<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/datatables/DataTables.min.js"></script>
<link href="<?php echo $CFG->wwwroot; ?>/theme/talentquest/style/jquery.dataTables.css" type="text/css" media="screen" rel="stylesheet" />
<script>
	jQuery('.users-tabs-header li').click(function(event){
		jQuery('.users-tabs-header li').removeClass('active');
		jQuery(this).addClass('active');
        var role_id = jQuery(this).attr('id');
		var index = jQuery(".users-tabs-header li").index(this);
		jQuery('.users-tabs .users-tabs-item').removeClass('active');
		jQuery('.users-tabs .users-tabs-item').eq(index).addClass('active');
		jQuery('.users-tabs .users-tabs-item').eq(index).addClass('load');
        if (role_id != 'search-tab'){
            var roleid = role_id.replace("role_", "");
            jQuery('.users-tabs .users-tabs-item').eq(index).html('<i class="fa fa-spin fa-spinner"></i>').load("<?php echo $CFG->wwwroot;?>/local/manager/ajax.php?action=load_users&roleid="+roleid,  function() {
                jQuery('.users-tabs .users-tabs-item').eq(index).removeClass('load');
            });
        }
	});
    
    jQuery('#all-search').keyup( function () {
        var filter = $(this).val().toLowerCase();
        var input = $(this);

        if (filter) {
            jQuery('#search-tab').removeClass('hidden');
            jQuery('#search-tab').trigger('click');
            jQuery('.search-users-tab').addClass('load');
            jQuery('.search-users-tab').html('<i class="fa fa-spin fa-spinner"></i>').load("<?php echo $CFG->wwwroot;?>/local/manager/ajax.php?action=search_users&search="+filter,  function() {
                jQuery('.search-users-tab').removeClass('load');
            });
        } else {
            jQuery('.users-tabs-header li').eq(0).trigger('click');
            jQuery('#search-tab').addClass('hidden');
        }
        
        if (input.val().length){
            input.parent().find('.search-actions .fa-times').show();
            input.parent().find('.search-actions .fa-search').hide();
        } else {
            input.parent().find('.search-actions .fa-times').hide();
            input.parent().find('.search-actions .fa-search').show();
        }
    });
    
    jQuery('.search-allusers-box i').click( function () {
        jQuery('#all-search').trigger('keyup');
    });
	
	function delete_user(id){
		if (confirm('<?php echo get_string('userdeletemsg', 'local_manager'); ?>')){
			jQuery.get("<?php echo $CFG->wwwroot; ?>/local/manager/ajax.php?action=user_delete&id="+id, function(data){
				jQuery('#user_'+id).parent().parent().remove();
			});
		}
	}
	
	jQuery(document).ready(function(){
		jQuery('.users-tabs .users-tabs-item#roleid_<?php echo $first_role; ?>').addClass('load').html('<i class="fa fa-spin fa-spinner"></i>').load("<?php echo $CFG->wwwroot;?>/local/manager/ajax.php?action=load_users&roleid=<?php echo $first_role; ?>",  function() {
            jQuery('.users-tabs .users-tabs-item#roleid_<?php echo $first_role; ?>').removeClass('load');
        });
	});
</script>

<?php

/// and proper footer
echo $OUTPUT->footer();

