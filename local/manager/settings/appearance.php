<?php
require_once('../../../config.php');
require_once('appearance_form.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // course id

require_login();
$systemcontext = context_system::instance();
//require_capability('local/manager:appearance', $systemcontext);

$styles = $DB->get_records_sql('SELECT s.*, c.name as cat_name FROM {local_manager_styles} s LEFT JOIN {local_manager_cats} c ON c.id = s.category WHERE s.state = 1 ORDER BY s.sortorder, s.id');
$data = new StdClass();
foreach($styles as $style){
    $data->{$style->name} = $style->value;
}

$title = get_string('manageappearance', 'local_manager');
$PAGE->set_url('/local/manager/settings/appearance.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'context' => $systemcontext);

$data->logo = appearance_file(1, 'logo');
if (!empty($data->logo)){
    $data = file_prepare_standard_filemanager($data, 'logo', $attachmentoptions, $systemcontext, 'theme_talentquest', 'logo', 1);
}
$data->favicon = appearance_file(2, 'favicon');
if (!empty($data->favicon)){
    $data = file_prepare_standard_filemanager($data, 'favicon', $attachmentoptions, $systemcontext, 'theme_talentquest', 'favicon', 2);
}
$data->faviconpng = appearance_file(3, 'faviconpng');
if (!empty($data->faviconpng)){
    $data = file_prepare_standard_filemanager($data, 'faviconpng', $attachmentoptions, $systemcontext, 'theme_talentquest', 'faviconpng', 3);
}
$data->loginbackground = appearance_file(4, 'loginbackground');
if (!empty($data->loginbackground)){
    $data = file_prepare_standard_filemanager($data, 'loginbackground', $attachmentoptions, $systemcontext, 'theme_talentquest', 'loginbackground', 4);
}

//create form
$mform = new edit_styles_form(null, array(
	'styles'=>$styles, 'data'=>$data));

if ($mform->is_cancelled()) {
} elseif ($stylesnew = $mform->get_data()) {

	if($stylesnew->logo_filemanager){
		$file = $DB->get_record('files', array('itemid'=> $stylesnew->logo_filemanager));
		if($file->id){
			$DB->delete_records('files', array('component'=>'theme_talentquest', 'itemid' => 1));
	        file_postupdate_standard_filemanager($stylesnew,
                    'logo', $attachmentoptions, $systemcontext, 'theme_talentquest', 'logo', 1);
		}
	}
    
    if($stylesnew->favicon_filemanager){
        $file = $DB->get_record('files', array('itemid'=> $stylesnew->favicon_filemanager));
		if($file->id){
			$DB->delete_records('files', array('component'=>'theme_talentquest', 'itemid' => 2));
	        file_postupdate_standard_filemanager($stylesnew,
                    'favicon', $attachmentoptions, $systemcontext, 'theme_talentquest', 'favicon', 2);
		}
	}
    
    if($stylesnew->faviconpng_filemanager){
		$file = $DB->get_record('files', array('itemid'=> $stylesnew->faviconpng_filemanager));
		if($file->id){
			$DB->delete_records('files', array('component'=>'theme_talentquest', 'itemid' => 3));
	        file_postupdate_standard_filemanager($stylesnew,
                    'faviconpng', $attachmentoptions, $systemcontext, 'theme_talentquest', 'faviconpng', 3);
		}
	}
    
    if($stylesnew->loginbackground_filemanager){
		$file = $DB->get_record('files', array('itemid'=> $stylesnew->loginbackground_filemanager));
		if($file->id){
			$DB->delete_records('files', array('component'=>'theme_talentquest', 'itemid' => 4));
	        file_postupdate_standard_filemanager($stylesnew,
                    'loginbackground', $attachmentoptions, $systemcontext, 'theme_talentquest', 'loginbackground', 4);
		}
	}
	
	foreach ($stylesnew as $name=>$value){
		$db_style = $DB->get_record('local_manager_styles', array('name'=>$name));
		if ($db_style->id){
			$db_style->value = $value;
			$DB->update_record('local_manager_styles', $db_style);
            
            if ($db_style->name == 'system_name'){
                $system_course = new stdClass();
                $system_course->id = 1;
                $system_course->fullname = (!empty($db_style->value)) ? $db_style->value : $db_style->defaultvalue;
                $system_course->shortname = $system_course->fullname;
                $DB->update_record('course', $system_course);
            }
		}
	}
    theme_reset_all_caches();
    redirect($PAGE->url);
}

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

/// Finally display THE form
echo html_writer::start_tag('div', array('class'=>'styles-form'));
	$mform->display();
echo html_writer::end_tag('div');

?>

<script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/colorpicker.js"></script>
<script>
	jQuery('.ColorPicker').each(function(e){
		var currentId = jQuery(this).attr('id');
		jQuery(this).ColorPicker({
			onShow: function (colpkr) {
				jQuery(colpkr).fadeIn(500);
				return false;
			},
			onHide: function (colpkr) {
				jQuery(colpkr).fadeOut(500);
				return false;
			},
			onChange: function (hsb, hex, rgb) {
				jQuery('#'+currentId).val(hex);
			},
			onBeforeShow: function () {
				if (this.value.length){
					jQuery(this).ColorPickerSetColor(this.value);
				} else if(jQuery(this).attr('placeholder').length){
					jQuery(this).ColorPickerSetColor(jQuery(this).attr('placeholder'));
				}
			}			
		});
	});
</script>

<?php
/// and proper footer
echo $OUTPUT->footer();

function appearance_file($id, $name, $component = 'theme_talentquest') {
	global $DB, $CFG, $USER, $SITE;

	$fs = get_file_storage();
	$files = $fs->get_area_files(context_system::instance()->id, $component, $name, $id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
	if (count($files) < 1) {
		return '';
	} else {
		$file = reset($files);
		unset($files);
		return $file->get_filename();
	}
}
