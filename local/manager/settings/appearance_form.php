<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/lib/formslib.php');

class edit_styles_form extends moodleform {
    // Define the form
    function definition () {
        global $CFG, $COURSE, $USER;

        $mform =& $this->_form;
        $systemcontext   = context_system::instance();
        
        if (is_array($this->_customdata)) {
            if (array_key_exists('styles', $this->_customdata)) {
                $styles = $this->_customdata['styles'];
            }
            if (array_key_exists('data', $this->_customdata)) {
                $data = $this->_customdata['data'];
            }
        }
        //Accessibility: "Required" is bad legend text.
        $strgeneral  = get_string('general');
        $strrequired = get_string('required');
        
        $filemanager_options = array(
            'maxfiles' => 1,
            'maxbytes' => $CFG->maxbytes,
            'subdirs' => 0,
            'accepted_types' => '*',
            'context' => $systemcontext
        );

		$category = 0;
		foreach ($styles as $style){
			if ($style->category != $category){
                $filemanager_options['accepted_types'] = '*';
				$mform->addElement('header', $style->cat_name.'hdr', get_string($style->cat_name, 'local_manager'));
				$mform->setExpanded($style->cat_name.'hdr', false);
			}
			if ($style->category == 1 and $category != 1){
                $filemanager_options['accepted_types'] = '*';
				$mform->addElement('filemanager', 'logo_filemanager', get_string('logo', 'local_manager'), null, $filemanager_options);
			}
            if ($style->category == 4 and $category != 4){
                $filemanager_options['accepted_types'] = array('.ico');
				$mform->addElement('filemanager', 'favicon_filemanager', get_string('favicon_ico', 'local_manager'), null, $filemanager_options);
                
                $filemanager_options['accepted_types'] = array('.png');
				$mform->addElement('filemanager', 'faviconpng_filemanager', get_string('favicon_png', 'local_manager'), null, $filemanager_options);
			}
            if ($style->category == 7 and $category != 7){
                $filemanager_options['accepted_types'] = '*';
				$mform->addElement('filemanager', 'loginbackground_filemanager', get_string('loginbackground', 'local_manager'), null, $filemanager_options);
			}
			
            if ($style->type == 'enable_disable'){
				$options = array('enabled'=>get_string('enabled', 'local_manager'), 'disabled'=>get_string('disabled', 'local_manager'));
				$mform->addElement('select', $style->name, get_string($style->name, 'local_manager'), $options);
			} else {
				$options = ($style->type == 'color') ? 'class="ColorPicker"' : '';
				$options .= ($style->defaultvalue) ? ' placeholder="'.$style->defaultvalue.'"' : '';
				$mform->addElement('text', $style->name, get_string($style->name, 'local_manager'), $options);
			}
			$mform->setType($style->name, PARAM_TEXT);
			if ($style->value){
				$mform->setDefault($style->name, $style->value);
			}
			$category = $style->category;
		}
		
        $this->add_action_buttons(false, get_string('save', 'local_manager'));
        
        $this->set_data($data);
    }
	
	
}


