<?php
require_once('../../../config.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // course id

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:languages', $systemcontext);

$langs = array();
$settings = $DB->get_records('local_manager_langs');
if (count($settings)){
    foreach ($settings as $setting){
        $langs[$setting->name] = $setting->state;
    }
}
$installedlangs = get_string_manager()->get_list_of_translations(false);

$title = get_string('managelanguages', 'local_manager');
$PAGE->set_url('/local/manager/settings/languages.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('ul', array('class'=>'admin-menu-manage'));

    foreach ($installedlangs as $langkey=>$langname){
        $value = ((isset($langs[$langkey]) and $langs[$langkey] > 0) or (!isset($langs[$langkey])));
        echo html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
            echo $langname;
            echo html_writer::start_tag('div', array('class'=>'onoff-switch'));
                echo html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'lang_'.$langkey, 'value'=>1, 'onchange'=>'toggleLang(this, "'.$langkey.'");');
                    if ($value) $params['checked'] = 'checked';
                    echo html_writer::tag('input', '', $params);
                    echo html_writer::tag('span', '');
                echo html_writer::end_tag('label');
            echo html_writer::end_tag('div');
        echo html_writer::end_tag('li');
    }

echo html_writer::end_tag('ul');

?>
<script>
    function toggleLang(obj, lang){
        var state = (jQuery(obj).prop("checked"))?1:0;

        jQuery.get("<?php echo $CFG->wwwroot; ?>/local/manager/ajax.php?action=set_lang&state="+state+"&name="+lang);
        jQuery(obj).parent().parent().parent().toggleClass("disabled");
    }
</script>

<?php
/// and proper footer
echo $OUTPUT->footer();
