<?php
require_once('../../../config.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // course id

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:blogsettings', $systemcontext);

$title = get_string('blogsettings', 'local_manager');
$PAGE->set_url('/local/manager/settings/blog.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_tag('ul', array('class'=>'admin-menu-manage'));

    $value = get_config('blog', 'markhelpful');
    echo html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
        echo get_string('markhelpful', 'local_manager');
        echo html_writer::start_tag('div', array('class'=>'onoff-switch'));
            echo html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'markhelpful', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "blog", "markhelpful");');
                    if ($value) $params['checked'] = 'checked';
                echo html_writer::tag('input', '', $params);
                echo html_writer::tag('span', '');
            echo html_writer::end_tag('label');
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('li');

    $value = get_config('blog', 'rate');
    echo html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
        echo get_string('canrate', 'local_manager');
        echo html_writer::start_tag('div', array('class'=>'onoff-switch'));
            echo html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'rate', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "blog", "rate");');
                    if ($value) $params['checked'] = 'checked';
                echo html_writer::tag('input', '', $params);
                echo html_writer::tag('span', '');
            echo html_writer::end_tag('label');
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('li');

    $value = get_config('blog', 'likes');
    echo html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
        echo get_string('canlike', 'local_manager');
        echo html_writer::start_tag('div', array('class'=>'onoff-switch'));
            echo html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'likes', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "blog", "likes");');
                    if ($value) $params['checked'] = 'checked';
                echo html_writer::tag('input', '', $params);
                echo html_writer::tag('span', '');
            echo html_writer::end_tag('label');
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('li');

    $value = get_config('blog', 'commentshelpful');
    echo html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
        echo get_string('commentshelpful', 'local_manager');
        echo html_writer::start_tag('div', array('class'=>'onoff-switch'));
            echo html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'commentshelpful', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "blog", "commentshelpful");');
                    if ($value) $params['checked'] = 'checked';
                echo html_writer::tag('input', '', $params);
                echo html_writer::tag('span', '');
            echo html_writer::end_tag('label');
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('li');

    $value = get_config('blog', 'commentslikes');
    echo html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled')));
        echo get_string('commentslikes', 'local_manager');
        echo html_writer::start_tag('div', array('class'=>'onoff-switch'));
            echo html_writer::start_tag('label');
                    $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'commentslikes', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "blog", "commentslikes");');
                    if ($value) $params['checked'] = 'checked';
                echo html_writer::tag('input', '', $params);
                echo html_writer::tag('span', '');
            echo html_writer::end_tag('label');
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('li');

echo html_writer::end_tag('ul');

?>
<script>
    function toggleFunctionality(obj, plugin, config){
        var state = (jQuery(obj).prop("checked"))?1:0;

        jQuery.get("<?php echo $CFG->wwwroot; ?>/local/manager/ajax.php?action=set_config&state="+state+"&config="+config+"&plugin="+plugin);
        jQuery(obj).parent().parent().parent().toggleClass("disabled");
    }
</script>

<?php
/// and proper footer
echo $OUTPUT->footer();
