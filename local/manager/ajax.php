<?php

define('AJAX_SCRIPT', true);

require_once('../../config.php');

$id		= optional_param('id', 0, PARAM_INT);
$type	= optional_param('type', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_RAW);
$search = optional_param('search', '', PARAM_RAW);
$form	= (object)optional_param_array('form', array(), PARAM_RAW);
$msg	= optional_param_array('msg', array(), PARAM_RAW);
$start	= optional_param('start', 0, PARAM_INT);

if ($action == 'user_delete' and $id > 0){
	$user = $DB->get_record('user', array('id'=>$id));
	if ($user){
		 delete_user($user);
         \core\session\manager::gc(); // Remove stale sessions.
	}
}else if ($action == 'delete_users'){
    $items = optional_param('items', '', PARAM_RAW);
    
    if (!empty($items)){
        $items = json_decode($items);
        if (count($items) > 0){
            foreach ($items as $item){
                if (strstr($item, 'row_')){
                    $userid = str_replace('row_', '', $item);
                    $user = $DB->get_record('user', ['id'=>$userid]);
                    if ($user) {
                        delete_user($user);
                        \core\session\manager::gc(); // Remove stale sessions.
                        $result = 'success';
                    }
                }
            }
        }
    }
    echo json_encode(array('result'=>$result));

} elseif ($action == 'load_users'){
 
    $roleid = optional_param('roleid', 0, PARAM_INT);
    
    $systemcontext = context_system::instance();
    $roles = $DB->get_records_sql("SELECT * FROM {role} WHERE status = 1 AND type = 'system' ORDER BY sortorder ASC");
    list($assignableroles, $assigncounts, $nameswithcounts) = get_assignable_roles($systemcontext, ROLENAME_BOTH, true);
    $role = $DB->get_record("role", array('id'=>$roleid));
    ?>
    <?php if ($roleid > 0) : ?>
        <div class="users-create-panel clearfix">
            <?php if (((has_capability('moodle/role:assign', $systemcontext) and isset($assignableroles[$role->id])) || is_siteadmin()) and has_capability('moodle/user:create', $systemcontext)) : ?>
                <button class="button" onclick="location = '<?php echo $CFG->wwwroot;?>/admin/roles/assign.php?contextid=1&roleid=<?php echo $role->id; ?>'"><i class="fa fa-plus"></i> <?php echo get_string('assignrole', 'local_manager');?></button>
            <?php endif;?>
            <?php /*if (has_capability('moodle/user:create', $systemcontext)) : ?>
                    <button class="button" onclick="location = '<?php echo $CFG->wwwroot;?>/user/editadvanced.php?id=-1'"><i class="fa fa-pencil-square-o"></i> <?php echo get_string('createnew', 'local_manager');?> <?php echo $role->name; ?></button>
            <?php endif; */?>
            </div>
            <table title="<?php echo $role->name; ?>s" id="datatable_<?php echo $role->id; ?>" class="generaltable sorting data-table datatable_<?php echo $role->id; ?> selecting" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>								
                        <th align="center" value="id" class="active asc"><span><?php echo get_string('ID', 'local_manager');?></span></th>
                        <th align="center" value="name"><span><?php echo get_string('name', 'local_manager');?></span></th>
                        <th align="center" value="username"><span><?php echo get_string('username', 'local_manager');?></span></th>
                        <th align="center" value="email"><span><?php echo get_string('email', 'local_manager');?></span></th>
                        <th align="center" value="city"><span><?php echo get_string('city', 'local_manager');?></span></th>
                        <th align="center" value="country"><span><?php echo get_string('country', 'local_manager');?></span></th>
                        <th align="center" value="idnumber"><span><?php echo get_string('user_ID', 'local_manager');?></span></th>
                        <th align="center" value="manager"><span><?php echo get_string('manager', 'local_manager');?></span></th>
                        <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager');?></span></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        <script>
            var role_<?php echo $role->id; ?> = jQuery('#datatable_<?php echo $role->id; ?>').dataTable( {
			"bAutoWidth": false,
			"bServerSide": true,
			"bStateSave": true,
			"sAjaxSource": "<?php echo $CFG->wwwroot.'/local/manager/users/users_datatables.php';?>?role=<?php echo $role->id; ?>",
			"sDom": 'T<"clear">lfrtip',
			"sPaginationType": "full_numbers",
            "aLengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
            "iDisplayLength": 50,
			"oLanguage": {
				"sSearch": "_INPUT_",
				"sLengthMenu": "<span>_MENU_</span>",
				"oPaginate": { "sFirst": "First", "sLast": "Last" }
			},
			"aoColumnDefs": [ 
						{ "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
						{ "bSortable": false, "aTargets": [ -1 ] },
					],
					"oTableTools": {
						"aButtons": []
					},
            });
            
            jQuery('#datatable_<?php echo $role->id; ?>_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
            if (jQuery('input[type="search"]').length){
                jQuery('input[type="search"]').each(function(e){
                    var input = jQuery(this);
                    var el = '<span class="search-actions"><i class="fa fa-times" style="'+((input.val().length)?"":"display:none;")+'" onclick="searchProcess(this, 0);"></i><i class="fa fa-search" style="'+((input.val().length)?"display:none;":"")+'" onclick="searchProcess(this, 1);"></i></span>';

                    input.after(el);

                    input.keyup(function(e){
                        if (input.val().length){
                            input.parent().find('.search-actions .fa-times').show();
                            input.parent().find('.search-actions .fa-search').hide();
                        } else {
                            input.parent().find('.search-actions .fa-times').hide();
                            input.parent().find('.search-actions .fa-search').show();
                        }
                    });
                });
            }
            
        </script>
    <?php elseif ($roleid == 0) : ?>
        <table title="<?php echo get_string('notassignedusers', 'local_manager');?>" id="datatable-users" class="generaltable sorting data-table datatable-users selecting" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th align="center" value="id" class="active asc"><span><?php echo get_string('ID', 'local_manager');?></span></th>
                <th align="center" value="name"><span><?php echo get_string('name', 'local_manager');?></span></th>
                <th align="center" value="username"><span><?php echo get_string('username', 'local_manager');?></span></th>
                <th align="center" value="email"><span><?php echo get_string('email', 'local_manager');?></span></th>
                <th align="center" value="city"><span><?php echo get_string('city', 'local_manager');?></span></th>
                <th align="center" value="country"><span><?php echo get_string('country', 'local_manager');?></span></th>
                <th align="center" value="idnumber"><span><?php echo get_string('user_ID', 'local_manager');?></span></th>
                <th align="center" value="manager"><span><?php echo get_string('manager', 'local_manager');?></span></th>
                <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager');?></span></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script>
            var users = jQuery('.datatable-users').dataTable( {
                "bAutoWidth": false,
                "bServerSide": true,
                "bStateSave": true,
                "sAjaxSource": "<?php echo $CFG->wwwroot.'/local/manager/users/users_datatables.php';?>",
                "sDom": 'T<"clear">lfrtip',
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "iDisplayLength": 50,
                "oLanguage": {
                    "sSearch": "_INPUT_",
                    "sLengthMenu": "<span>_MENU_</span>",
                    "oPaginate": { "sFirst": "First", "sLast": "Last" }
                },
                
                "aoColumnDefs": [ 
                            { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                            { "bSortable": false, "aTargets": [ -1 ] },
                        ],
                        "oTableTools": {
                            "aButtons": [
                            ]
                        },
            });
            
            jQuery('#datatable-users_wrapper .dataTables_filter input').attr('placeholder', '<?php echo get_string('search', 'local_manager'); ?>');
            
            if (jQuery('input[type="search"]').length){
                jQuery('input[type="search"]').each(function(e){
                    var input = jQuery(this);
                    var el = '<span class="search-actions"><i class="fa fa-times" style="'+((input.val().length)?"":"display:none;")+'" onclick="searchProcess(this, 0);"></i><i class="fa fa-search" style="'+((input.val().length)?"display:none;":"")+'" onclick="searchProcess(this, 1);"></i></span>';

                    input.after(el);

                    input.keyup(function(e){
                        if (input.val().length){
                            input.parent().find('.search-actions .fa-times').show();
                            input.parent().find('.search-actions .fa-search').hide();
                        } else {
                            input.parent().find('.search-actions .fa-times').hide();
                            input.parent().find('.search-actions .fa-search').show();
                        }
                    });
                });
            }
        </script>
    <?php endif; ?>
    <?php
    
} elseif ($action == 'search_users'){
    
    $search = optional_param('search', 0, PARAM_RAW);
    ?>
        <table title="<?php echo get_string('searchusersresults', 'local_manager'); ?>" id="datatable-search" class="generaltable sorting data-table datatable-search selecting" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th align="center" value="id" class="active asc"><span><?php echo get_string('ID', 'local_manager');?></span></th>
                <th align="center" value="name"><span><?php echo get_string('name', 'local_manager');?></span></th>
                <th align="center" value="username"><span><?php echo get_string('username', 'local_manager');?></span></th>
                <th align="center" value="email"><span><?php echo get_string('email', 'local_manager');?></span></th>
                <th align="center" value="city"><span><?php echo get_string('city', 'local_manager');?></span></th>
                <th align="center" value="country"><span><?php echo get_string('country', 'local_manager');?></span></th>
                <th align="center" value="idnumber"><span><?php echo get_string('user_ID', 'local_manager');?></span></th>
                <th align="center" value="role"><span><?php echo get_string('role', 'local_manager');?></span></th>
                <th align="center" value="manager"><span><?php echo get_string('manager', 'local_manager');?></span></th>
                <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager');?></span></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script>
            var search_users = jQuery('.datatable-search').dataTable( {
                "bAutoWidth": false,
                "bServerSide": true,
                "bStateSave": true,
                "sAjaxSource": "<?php echo $CFG->wwwroot;?>/local/manager/users/users_search.php?search=<?php echo $search; ?>",
                "sDom": 'T<"clear">lfrtip',
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "iDisplayLength": 50,
                "oLanguage": {
                    "sSearch": "_INPUT_",
                    "sLengthMenu": "<span>_MENU_</span>",
                    "oPaginate": { "sFirst": "First", "sLast": "Last" }
                },
                "aoColumnDefs": [ 
                            { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                            { "bSortable": false, "aTargets": [ -1 ] },
                        ],
                "oTableTools": {
                        "aButtons": []
                    },
            });
            
            jQuery('#datatable-search_filter').hide();
        </script>
    <?php
}else if ($action == 'set_lang'){
    $lang	= optional_param('name', '', PARAM_RAW);
    $state	= optional_param('state', 0, PARAM_INT);
    
    if (!empty($lang)){
        $lang_setting = $DB->get_record('local_manager_langs', array('name'=>$lang));
        if ($lang_setting){
            $lang_setting->state = $state;
            $DB->update_record('local_manager_langs', $lang_setting);
        } else {
            $lang_setting = new stdClass();
            $lang_setting->name = $lang;
            $lang_setting->state = $state;
            $DB->insert_record('local_manager_langs', $lang_setting);
        }
    }
    echo time();
}else if ($action == 'set_config'){
    $config       = optional_param('config', '', PARAM_RAW);
    $plugin       = optional_param('plugin', '', PARAM_RAW);
    $state        = optional_param('state', 0, PARAM_INT);
    
    set_config($config, $state, $plugin);
    
    echo '1';
    exit;
}


exit;