<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * manager version file.
 *
 * @package    local_manager
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'System Manager';
$string['search'] = 'Search'; 
$string['manager:appearance'] = 'Manage appearance'; 
$string['manager:languages'] = 'Manage languages'; 
$string['manager:managecategories'] = 'Manage categories'; 
$string['enabled_desc'] = 'Enable System Manager';
$string['manager:view'] = 'View System Manager';
$string['manager:manage'] = 'Control System Manager';
$string['manager:manageusers'] = 'Manage Users';
$string['manager:managecourses'] = 'Manage Courses';
$string['manager:manageallcourses'] = 'Manage All Courses';
$string['manager:uploadcohorts'] = 'Import User Groups';
$string['manager:manageallcohorts'] = 'Manage All User Groups';
$string['manager:manageowncohorts'] = 'Manage Own User Groups';
$string['manager:manageapprovals'] = 'Manage Approvals';
$string['filter'] = 'Filter'; 
$string['enabled'] = 'Enabled System Manager';
$string['managecourses'] = 'Manage Courses';
$string['ID'] = 'ID';
$string['coursename'] = 'Course Name';
$string['category'] = 'Category';
$string['coursedates'] = 'Course Dates';
$string['teacher'] = 'Teacher';
$string['actions'] = 'Actions';
$string['deletemsg'] = 'Are you sure you want to delete this course?';
$string['enrollments'] = 'Enrollments';
$string['startdate'] = 'Start Date';
$string['enddate'] = 'End Date';
$string['manageusers'] = 'Manage Users';
$string['notassignedusers'] = 'Not Assigned Users';
$string['searchresults'] = 'Search results';
$string['userdeletemsg'] = 'Are you sure you want to delete this user?';
$string['name'] = 'Name';
$string['username'] = 'Username';
$string['user_name'] = 'User Name';
$string['email'] = 'Email';
$string['city'] = 'City';
$string['country'] = 'Country';
$string['user_ID'] = 'User ID';
$string['managerole'] = 'Manage Role';
$string['createnew'] = 'Create New';
$string['assignrole'] = 'Assign Users';
$string['role'] = 'Role';
$string['searchusersresults'] = 'Search Users results';
$string['managecategories'] = 'Manage Categories';
$string['parentcategory'] = 'Parent Category';
$string['coursecount'] = 'Number of Courses';
$string['createnewcourse'] = 'Create New Course';
$string['createnewcategory'] = 'Create New Category';
$string['loginas'] = 'Login as this user';
$string['manageappearance'] = 'Manage Appearance';
$string['manager:languages'] = 'Manage Languages';
$string['managelanguages'] = 'Manage Languages';
$string['header_styles'] = 'Header Styles';
$string['sidebar_styles'] = 'Sidebar Styles';
$string['dashboard_styles'] = 'Dashboard Styles';
$string['link_styles'] = 'Link Styles';
$string['buttons'] = 'Buttons Styles';
$string['course_styles'] = 'Course Styles';
$string['save'] = 'Save';
$string['logo'] = 'Logo';
$string['favicon_ico'] = 'Favicon (.ico)';
$string['favicon_png'] = 'Mobile Favicon (.png)';
$string['enabled'] = 'Enabled';
$string['disabled'] = 'Disabled';
$string['footer_styles'] = 'Footer Styles';
$string['system_name'] = 'System Name';
$string['color1'] = 'Header backgroud color';
$string['color2'] = 'Header text color';
$string['color3'] = 'Search panel backgroud color';
$string['color4'] = 'Search panel title color';
$string['color5'] = 'Search panel active background color';
$string['color6'] = 'Search panel icons color';
$string['color7'] = 'Search panel textfield background color';
$string['color8'] = 'Search panel textfield text color';
$string['color9'] = 'Breadcrumbs background color';
$string['color10'] = 'Breadcrumbs text color';
$string['color11'] = 'Breadcrumbs link color';
$string['color12'] = 'Breadcrumbs link hover color';
$string['color13'] = 'Footer border color';
$string['color14'] = 'Footer text color';
$string['color15'] = 'Footer link color';
$string['color16'] = 'Footer link hover color';
$string['color17'] = 'Sidebar background color';
$string['color18'] = 'Sidebar active/hover background color';
$string['color19'] = 'Sidebar icon color';
$string['color20'] = 'Sidebar active/hover icon color';
$string['color21'] = 'Sidebar popup background color';
$string['color22'] = 'Sidebar header color';
$string['color23'] = 'Sidebar link color';
$string['color24'] = 'Sidebar link hover background color';
$string['color25'] = 'Sidebar link hover text color';
$string['color26'] = 'Dashboard background color';
$string['color27'] = 'Dashboard main block background color';
$string['color28'] = 'Blocks background color';
$string['color29'] = 'Blocks header color';
$string['color30'] = 'Primary button background color';
$string['color31'] = 'Primary button text color';
$string['color32'] = 'Secondary button background color';
$string['color33'] = 'Secondary button text color';
$string['color34'] = 'Warning button background color';
$string['color35'] = 'Warning button text color';
$string['color36'] = 'Success button background color';
$string['color37'] = 'Success button text color';
$string['color38'] = 'Danger button background color';
$string['color39'] = 'Danger button text color';
$string['color40'] = 'Active/hover tab color';
$string['color41'] = 'Top menu background color';
$string['color42'] = 'Top menu text color';
$string['color43'] = 'Top menu buttons background color';
$string['color44'] = 'Top menu buttons text color';
$string['color45'] = 'Link text color';
$string['color46'] = 'Link hover text color';
$string['color47'] = 'Drop menu background color';
$string['color48'] = 'Drop menu text color';
$string['color49'] = 'Drop menu background hover color';
$string['color50'] = 'Drop menu text hover color';
$string['color51'] = 'Course section header background color';
$string['color52'] = 'Course section heading text color';
$string['color53'] = 'Course section header highlight color';
$string['color54'] = 'Course section highlighted heading text color';
$string['color55'] = 'Course activity/resource link color';
$string['color56'] = 'Course activity/resource hover link color';
$string['text1'] = 'Copyrights text';
$string['text2'] = 'Terms of use link';
$string['text3'] = 'Privacy Policy link';
$string['text4'] = 'Powered by text';
$string['text5'] = 'Powered by link';
$string['loginpage'] = 'Login page Styles';
$string['text6'] = 'Welcome text';
$string['color57'] = 'Login page background color';
$string['color58'] = 'Welcome box background color';
$string['color59'] = 'Welcome box text color';
$string['color60'] = 'Form text color';
$string['color61'] = 'Button background color';
$string['color62'] = 'Button text color';
$string['loginbackground'] = 'Login background image';
$string['assignedusers'] = 'Assigned Users';
$string['manageapprovals'] = 'Manage Approvals';
$string['nothingtoapprove'] = 'Nothing to Approve';
$string['manageapprovalsetraining'] = 'Approval Queue - External Training Activities';
$string['manageapprovalsecourses'] = 'Approval Queue - External Training Courses';
$string['manageapprovalseinstitutes'] = 'Approval Queue - External Training Institutes';
$string['manageapprovalsenrollments'] = 'Approval Queue - Enrollments';
$string['course'] = 'Course Name';
$string['requestdate'] = 'Request Date';
$string['status'] = 'Status';
$string['pending'] = 'Pending';
$string['confirmed'] = 'Confirmed';
$string['tabrenrollments'] = 'Enrollments';
$string['tabetraining'] = 'External Training Activities';
$string['tabecourses'] = 'External Training Courses';
$string['tabeinstitutes'] = 'External Training Institutes';
$string['manager:seeonlyownusers'] = 'Manager can see only own users';
$string['manager'] = 'Manager';
$string['usermanager'] = 'User Manager';
$string['manager:blogsettings'] = 'Manage blog settings';
$string['blogsettings'] = 'Blog Settings';
$string['markhelpful'] = 'Allow users to mark blog post as helpful';
$string['canrate'] = 'Allow users to rate blog post';
$string['canlike'] = 'Allow users to like blog post';
$string['commentshelpful'] = 'Allow users to mark post comment as helpful';
$string['commentslikes'] = 'Allow users to like post comments';
$string['manager:blogeditown'] = 'Can edit own blogs';
$string['manager:blogeditcourseblogs'] = 'Can edit all course blogs';


