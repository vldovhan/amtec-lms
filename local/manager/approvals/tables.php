<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * etraining
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class etraining_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $columns = array('username', 'coursename', 'institutename', 'trainingdate', 'score', 'credits', 'certification', 'creatorname', 'certificate');
        $headers = array(
            get_string('username', 'local_etraining'),
            get_string('coursename', 'local_etraining'),
            get_string('institute', 'local_etraining'),
            get_string('trainingdate', 'local_etraining'),
            get_string('score', 'local_etraining'),
            get_string('credits', 'local_etraining'),
            get_string('certification', 'local_etraining'),
            get_string('creatorname', 'local_etraining'),
            get_string('certificate', 'local_etraining'));    

        if (get_config('local_etraining', 'confirm')){
            $columns[] = 'status';
            $headers[] = get_string('status', 'local_etraining');
        }
        if (has_capability('local/etraining:manage', $systemcontext) or get_config('local_etraining', 'create')){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_etraining');
        }
        
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (c.coursename LIKE '%$search%' OR i.name LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR p.name LIKE '%$search%' OR uс.firstname LIKE '%$search%' OR uс.lastname LIKE '%$search%' OR t.instructor LIKE '%$search%')" : "";
        
        $fields = "t.*, c.coursename, i.name as institutename, p.name as certification, CONCAT(u.firstname, ' ', u.lastname) as username, CONCAT(uс.firstname, ' ', uс.lastname) as creatorname";
        $from = "{local_etraining} t 
                    LEFT JOIN {local_et_courses} c ON c.id = t.courseid
                    LEFT JOIN {local_et_institutes} i ON i.id = t.instituteid
                    LEFT JOIN {local_plans} p ON p.id = t.planid
                    LEFT JOIN {user} u ON u.id = t.userid 
                    LEFT JOIN {user} uс ON uс.id = t.creator ";
        $where = 't.id > 0 '.$sql_search;
        
        if (!has_capability('local/etraining:manage', $systemcontext)){
            $where .= ' AND t.userid = '.$USER->id;
        }
        
        $where .= ' AND t.status = 0';
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/manager/approvals/etraining.php?search=".$search);
    }
    
    function col_trainingdate($values) {
      return ($values->trainingdate) ? date('m/d/Y', $values->trainingdate) : '-';
    }
    
    function col_status($values) {
        if ($values->status > 1){
            return get_string('rejected', 'local_etraining');
        } elseif ($values->status > 0){
            return get_string('confirmed', 'local_etraining');
        } else {
            return get_string('pending', 'local_etraining');
        }
    }
    
    function col_certificate($values) {
    	global $CFG, $OUTPUT, $USER;

       	$buttons = array();
        $urlparams = array('id' => $values->id);
        
        if($values->certificate){
            $url = moodle_url::make_pluginfile_url(context_system::instance()->id,'local_etraining', 'certificate', $values->id,'/', $values->certificate);
            $buttons[] = html_writer::link(new moodle_url($url),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/backup'), 'alt' => $values->certificate, 'class' => 'iconsmall')),
            array('title' => $values->certificate, 'target'=>'_blank'));
        }
        
	    return implode(' ', $buttons);
    }
    
    function col_actions($values) {
    	global $CFG, $OUTPUT, $USER;

       	$buttons = array();
        
        $systemcontext   = context_system::instance();
        if (!has_capability('local/etraining:manage', $systemcontext) and $values->status){
            return '';
        }

        $urlparams = array('id' => $values->id);
        
        if (has_capability('local/etraining:manage', $systemcontext) and get_config('local_etraining', 'confirm')){
            
            $showhideurl = new moodle_url($CFG->wwwroot.'/local/manager/approvals/etraining.php', $urlparams + array('approve'=>1));
            $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => get_string('approve', 'local_etraining'), 'class' => 'iconsmall'));
            $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve', 'local_etraining')));
            
            $showhideurl = new moodle_url($CFG->wwwroot.'/local/manager/approvals/etraining.php', $urlparams);
            $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('reject', 'local_etraining'), 'class' => 'iconsmall'));
            $form = '<form action=\"'.$showhideurl.'\" method=\"POST\">';
            $form .= '<div class=\"form-element-box clearfix\"><div class=\"form-element-title\">'.get_string('actions', 'local_etraining').'</div><div class=\"form-element-field\"><label><input type=\"radio\" name=\"form[action]\" value=\"reject\" checked>'.get_string('rejectactivity', 'local_etraining').'</label><label><input type=\"radio\" name=\"form[action]\" value=\"delete\">'.get_string('deleteactivity', 'local_etraining').'</label></div></div>';
            $form .= '<div class=\"form-element-box clearfix\"><div class=\"form-element-title\">'.get_string('notes', 'local_etraining').'</div><div class=\"form-element-field\"><textarea placeholder=\"'.get_string('rejectmessage', 'local_etraining').'\" name=\"form[notes]\">'.$values->notes.'</textarea></div></div>';
            $form .= '<div class=\"submitbuttons\"><input type=\"submit\" value=\"'.get_string('submit').'\" class=\"submitbutton\" name=\"submitbutton\"><input type=\"button\" value=\"'.get_string('cancel').'\" class=\"addcancel\" name=\"addcancel\" onclick=\"closeCustomPopup(\'reject_'.$values->id.'\');\"></div>';
            $form .= '</form>';
            $buttons[] = html_writer::link('javascript:void(0);', $visibleimg, array('title' => get_string('reject', 'local_etraining'), 'onclick'=>'openCustomPopup("reject_'.$values->id.'", "'.get_string('rejectdelete', 'local_etraining').'", "'.$form.'");', ));
        }
        
	    return implode(' ', $buttons);
    }
}

class courses_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $columns = array('coursename', 'institute', 'username');
        $headers = array(
            get_string('name', 'local_etraining'),
            get_string('institute', 'local_etraining'),
            get_string('username', 'local_etraining'));
        
        if (get_config('local_etraining', 'confirmcourses')){
            $columns[] = 'status';
            $headers[] = get_string('status', 'local_etraining');
        }
        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_etraining');
        
       
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (c.name LIKE '%$search%' OR i.name LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')" : "";
        $fields = "c.*, CONCAT(u.firstname, ' ', u.lastname) as username, c.id as actions, i.name as institute";
        $from = "{local_et_courses} c LEFT JOIN {local_et_institutes} i ON i.id = c.instituteid LEFT JOIN {user} u ON u.id = c.userid ";
        $where = 'c.id > 0'.$sql_search;
        
        $where .= ' AND c.status = 0';
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/manager/approvals/ecourses.php?search=".$search);
    }
    
    function col_status($values) {
        if ($values->status > 0){
            return get_string('confirmed', 'local_etraining');
        } else {
            return get_string('pending', 'local_etraining');
        }
    }
    
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $values->id);
        
        if (get_config('local_etraining', 'confirmcourses')){
            $showhideurl = new moodle_url($CFG->wwwroot.'/local/manager/approvals/ecourses.php', $urlparams);
            $showhideurl->param('approve', 1);
            $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => get_string('approve', 'local_etraining'), 'class' => 'iconsmall'));
            $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve', 'local_etraining')));
        }
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/manager/approvals/ecourses.php', $urlparams + array('delete' => 1)),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        
        $editcolumnisempty = false;
       
	    return implode(' ', $buttons);
    }
}



class institutes_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $columns = array('name', 'username');
        $headers = array(
            get_string('name', 'local_etraining'),
            get_string('username', 'local_etraining'),
            get_string('status', 'local_etraining'),
            get_string('actions', 'local_etraining'));
        
        if (get_config('local_etraining', 'confirminstitutes')){
            $columns[] = 'status';
            $headers[] = get_string('status', 'local_etraining');
        }
        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_etraining');
        
       
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (i.name LIKE '%$search%')" : "";
        $fields = "i.*, CONCAT(u.firstname, ' ', u.lastname) as username, i.id as actions";
        $from = "{local_et_institutes} i LEFT JOIN {user} u ON u.id = i.userid ";
        $where = 'i.id > 0'.$sql_search;
        $where .= ' AND i.status = 0';
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/manager/approvals/einstitutes.php?search=".$search);
    }
    
    function col_status($values) {
        if ($values->status > 0){
            return get_string('confirmed', 'local_etraining');
        } else {
            return get_string('pending', 'local_etraining');
        }
    }
    
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $values->id);
        
        if (get_config('local_etraining', 'confirminstitutes')){
            $showhideurl = new moodle_url($CFG->wwwroot.'/local/manager/approvals/einstitutes.php', $urlparams);
            $showhideurl->param('approve', 1);
            $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => get_string('approve', 'local_etraining'), 'class' => 'iconsmall'));
            $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve', 'local_etraining')));
        }
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/manager/approvals/einstitutes.php', $urlparams + array('delete' => 1)),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        $editcolumnisempty = false;
       
	    return implode(' ', $buttons);
    }
}


class enrollment_table extends table_sql {
    function __construct($uniqueid, $search) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $columns = array('username', 'email', 'course', 'requestdate');
        $headers = array(
            get_string('username', 'local_manager'),
            get_string('email', 'local_manager'),
            get_string('course', 'local_manager'),
            get_string('requestdate', 'local_manager'),
            );
        
        $columns[] = 'status';
        $headers[] = get_string('status', 'local_manager');

        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_manager');
        
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR u.email LIKE '%$search%' OR c.fullname LIKE '%$search%')" : "";
        $fields = "ue.id, ue.enrolid, ue.userid, ue.status, CONCAT(u.firstname, ' ', u.lastname) as username, u.email, ue.timemodified, ue.id as actions, c.fullname, c.id as courseid";
        $from = "{user_enrolments} ue LEFT JOIN {enrol} e ON e.id = ue.enrolid LEFT JOIN {course} c ON c.id = e.courseid LEFT JOIN {user} u ON u.id = ue.userid ";
        $where = 'ue.status > 0 AND e.enrol = "approval" '.$sql_search;
        
        if (!is_siteadmin()){
            if(has_capability('local/manager:manageallcourses', $systemcontext)) {
                $where .= " AND c.id IN (
                                SELECT c.id FROM  {course} c
                                    LEFT JOIN {course_categories} cat ON cat.id = c.category
                                WHERE (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) 
                                    )";
            } else {
                $where .= " AND c.id IN (
                    SELECT c.id FROM {user_enrolments} ue 
                        LEFT JOIN {enrol} e ON e.id = ue.enrolid
                        LEFT JOIN {course} c ON c.id = e.courseid
                        LEFT JOIN {course_categories} cat ON cat.id = c.category
                    WHERE ue.userid = $USER->id AND ue.status = 0 AND (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) 
                    )";
            }
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/manager/approvals/enrollments.php?search=".$search);
    }
    
    function col_course($values) {
        return html_writer::link(new moodle_url('/course/view.php', array('id' => $values->courseid)), $values->fullname);
    }
    function col_requestdate($values) {
        return date('m/d/Y h:i a', $values->timemodified);
    }
    
    function col_status($values) {
        if ($values->status == 0){
            return get_string('confirmed', 'local_manager');
        } else {
            return get_string('pending', 'local_manager');
        }
    }
    
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $values->courseid, 'userid'=>$values->userid);
        
        $showhideurl = new moodle_url($CFG->wwwroot.'/local/manager/approvals/enrollments.php', $urlparams);
        $showhideurl->param('approve', 1);
        $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => get_string('approve'), 'class' => 'iconsmall'));
        $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve')));
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/manager/approvals/enrollments.php', $urlparams + array('delete' => 1)),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        $editcolumnisempty = false;
       
	    return implode(' ', $buttons);
    }
}


