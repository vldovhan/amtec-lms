<?php
require_once('../../../config.php');
require ('tables.php');

$id         = optional_param('id', $USER->id, PARAM_INT);    // course id
$search     = optional_param('search', '', PARAM_RAW);
$delete     = optional_param('delete', 0, PARAM_BOOL);
$approve    = optional_param('approve', 0, PARAM_BOOL);
$disapprove = optional_param('disapprove', 0, PARAM_BOOL);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:manageapprovals', $systemcontext);
require_capability('local/etraining:manage', $systemcontext);

$returnurl = new moodle_url("/local/manager/approvals/etraining.php");
if ($delete and $id) {  
    $training = $DB->get_record('local_etraining', array('id'=>$id), '*', MUST_EXIST);
    if (isset($training->id)) {
        $DB->delete_records('local_etraining', array('id'=>$training->id));
    }
    redirect($returnurl);
    
} elseif ($approve and $id) {
    
    $training = $DB->get_record('local_etraining', array('id'=>$id), '*', MUST_EXIST);
    if (isset($training->id)) {
        $record = (object)array('id' => $training->id, 'status' => 1);
        $DB->update_record('local_etraining', $record);
    }
    redirect($returnurl);
    
} elseif (isset($form->action)  && $id){
    
    $training = $DB->get_record('local_etraining', array('id'=>$id), '*', MUST_EXIST);
    
    if (isset($training->id)) {
        if ($form->action == 'reject'){
            $record = (object)array('id' => $training->id, 'status' => 2, 'notes'=>$form->notes);
            $DB->update_record('local_etraining', $record);
        } elseif ($form->action == 'delete') {
            $DB->delete_records('local_etraining', array('id'=>$training->id));
        }
    }
    
    redirect($returnurl);
}

$USER->editing = 0;
$title = get_string('manageapprovalsetraining', 'local_manager');
$PAGE->set_url('/local/manager/approvals/etraining.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add(get_string('manageapprovals', 'local_manager'));
$PAGE->navbar->add($title);

$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new etraining_table('table', $search);
$table->is_collapsible = false;
$renderer = $PAGE->get_renderer('local_manager');
    
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer->manager_print_approvalstabs('etraining');

echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'etraining-search-form'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_etraining').' ...', 'value' => $search));
echo html_writer::end_tag("label",  array());
echo html_writer::end_tag("form");


$table->out(20, true);
    
echo $OUTPUT->footer();


exit;
