<?php
require_once('../../../config.php');

$id         = optional_param('id', $USER->id, PARAM_INT);    // course id
$search     = optional_param('search', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$status     = optional_param('status', -1, PARAM_INT);

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:manageapprovals', $systemcontext);

$USER->editing = 0;
$title = get_string('manageapprovals', 'local_manager');
$PAGE->set_url('/local/manager/approvals/index.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);

$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_heading($title);

if (has_capability('enrol/approval:manage', $systemcontext)){
    redirect(new moodle_url($CFG->wwwroot.'/local/manager/approvals/enrollments.php'));
} else if (get_config('local_etraining', 'enabled')){
    if (has_capability('local/etraining:manage', $systemcontext) and get_config('local_etraining', 'confirm')){
        redirect(new moodle_url($CFG->wwwroot.'/local/manager/approvals/etraining.php'));
    }
    if (has_capability('local/etraining:managecourses', $systemcontext) and get_config('local_etraining', 'confirmcourses')){
        redirect(new moodle_url($CFG->wwwroot.'/local/manager/approvals/ecourses.php'));
    }
    if (has_capability('local/etraining:manageinstitutes', $systemcontext) and get_config('local_etraining', 'confirminstitutes')){
        redirect(new moodle_url($CFG->wwwroot.'/local/manager/approvals/einstitutes.php'));
    }
} else {
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    echo html_writer::tag("div", get_string('nothingtoapprove', 'local_manager'), array('class'=>'alert alert-warning'));
    echo $OUTPUT->footer();
}


exit;
