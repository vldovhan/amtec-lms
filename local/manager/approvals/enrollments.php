<?php
require_once('../../../config.php');
require ('tables.php');
require_once("$CFG->dirroot/enrol/approval/library.php");
require_once("$CFG->dirroot/enrol/renderer.php");

$id         = optional_param('id', $USER->id, PARAM_INT);    // course id
$search     = optional_param('search', '', PARAM_RAW);
$delete     = optional_param('delete', 0, PARAM_BOOL);
$approve    = optional_param('approve', 0, PARAM_BOOL);
$disapprove = optional_param('disapprove', 0, PARAM_BOOL);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:manageapprovals', $systemcontext);
require_capability('enrol/approval:manage', $systemcontext);

$returnurl = new moodle_url("/local/manager/approvals/enrollments.php");
if ($delete and $id) {  
    $userid  = required_param('userid', PARAM_INT);
    if (!$enrol_approval = enrol_get_plugin('approval')) {
        throw new coding_exception('Can not instantiate enrol_approval');
    }

    $res = $DB->get_record_sql("SELECT * FROM {enrol} WHERE courseid = ".$id." AND enrol ='approval'");
    $instance = $res->id;
    $instancename = $enrol_approval->get_instance_name($res);
    
    $enrol_approval->unenrol_user($res, $userid);
    
    redirect($returnurl);
    
} elseif ($approve and $id) {
    $userid  = required_param('userid', PARAM_INT);
    
    if (!$enrol_approval = enrol_get_plugin('approval')) {
        throw new coding_exception('Can not instantiate enrol_approval');
    }

    $res = $DB->get_record_sql("SELECT * FROM {enrol} WHERE courseid = ".$id." AND enrol ='approval'");
    $instance = $res->id;
    $instancename = $enrol_approval->get_instance_name($res);
    
    $enrol_approval->update_user_enrol($res, $userid, 0);
    redirect($returnurl);
}

$USER->editing = 0;
$title = get_string('manageapprovalsenrollments', 'local_manager');
$PAGE->set_url('/local/manager/approvals/enrollments.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add(get_string('manageapprovals', 'local_manager'));
$PAGE->navbar->add($title);

$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new enrollment_table('table', $search);
$table->is_collapsible = false;
$renderer = $PAGE->get_renderer('local_manager');
    
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer->manager_print_approvalstabs('enrollment');

echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'etraining-search-form'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_etraining').' ...', 'value' => $search));
echo html_writer::end_tag("label",  array());
echo html_writer::end_tag("form");


$table->out(20, true);
    
echo $OUTPUT->footer();


exit;
