<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * manager version file.
 *
 * @package    local_manager
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

function manager_get_status_filter($status = 0){
    global $DB, $USER;
    $output = '';
    
    $output .= '<select name = "status">';
    $output .= html_writer::tag('option', get_string('showall', 'local_manager'), array('value' => '-1'));
    
    $params = array();
    $params['value'] = 0;
    if ($status == 0) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('pending', 'local_manager'), $params);
    
    $params = array();
    $params['value'] = 1;
    if ($status == 1) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('confirmed', 'local_manager'), $params);
                          
    $output .= '</select>';
        
    return $output;
}

function manager_get_intituteslist($courseid = 0, $with_own = true){
    global $DB, $USER;
    $items = array();
    
    if ($courseid > 0){
        $where = " AND c.id = $courseid";
        if ($with_own) {
            $institutes = $DB->get_records_sql("SELECT DISTINCT(i.id), i.* FROM {local_et_institutes} i LEFT JOIN {local_et_courses} c ON c.instituteid = i.id WHERE (i.status > 0 OR (i.status = 0 AND i.userid = $USER->id)) $where ORDER BY i.name");
        } else {
            $institutes = $DB->get_records_sql("SELECT DISTINCT(i.id), i.* FROM {local_et_institutes} i LEFT JOIN {local_et_courses} c ON c.instituteid = i.id WHERE i.status > 0 $where ORDER BY i.name");
        }
        if (count($institutes)){
            foreach($institutes as $institute){
                $items[$institute->id] = $institute->name;
            }
        } 
    } else {
        if ($with_own){
            $institutes = $DB->get_records_sql("SELECT * FROM {local_et_institutes} WHERE status > 0 OR (status = 0 AND userid = $USER->id) ORDER BY name");
            if (count($institutes)){
                foreach($institutes as $institute){
                    $items[$institute->id] = $institute->name;
                }
            }    
        } else {
            $institutes = $DB->get_records_menu('local_et_institutes', array('status'=>1), 'name', 'id, name');
            if (count($institutes)){
                $items = $institutes;
            }    
        }    
    }
    
    return $items;
}

function manager_get_courseslist($instituteid = 0, $with_own = true){
    global $DB, $USER;
    $items = array(); $where = '';
    
    if ($instituteid > 0){
        $where = " AND instituteid = $instituteid";
        if ($with_own) {
            $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE (status > 0 OR (status = 0 AND userid = $USER->id)) $where ORDER BY coursename");
        } else {
            $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE status > 0 $where ORDER BY coursename");
        }
        if (count($courses)){
            foreach($courses as $course){
                $items[$course->id] = $course->coursename;
            }
        }  
    } else {
        if ($with_own){
            $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE status > 0 OR (status = 0 AND userid = $USER->id) ORDER BY coursename");
            if (count($courses)){
                foreach($courses as $course){
                    $items[$course->id] = $course->coursename;
                }
            }    
        } else {
            $courses = $DB->get_records_menu('local_et_courses', array('status'=>1), 'coursesname', 'id, coursesname');
            if (count($courses)){
                $items = $courses;
            }    
        }
    }
    
    return $items;
}

function manager_get_certificationslist(){
    global $DB, $USER;
    $items = array();
    
    $plans = $DB->get_records_menu('local_plans', array('type'=>1, 'visible'=>1), 'name', 'id, name');
    if (count($plans)){
        $items = $plans;
    }    
    
    return $items;
}

function manager_get_userslist(){
    global $DB, $USER, $CFG;
    $items = array();
    
    $users = $DB->get_records_sql("SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 AND id != $USER->id");
    if (count($users)){
        foreach ($users as $user){
            $items[$user->id] = fullname($user);
        }
    }
    
    return $items;
}


function local_manager_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    $itemid = array_shift($args);
    $filename = array_pop($args);
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_manager', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


function manager_import_institute($institutename = ''){
    global $USER, $DB;
    $record = 0;
    
    if (!empty($institutename)){
        $institute = $DB->get_record('local_et_institutes', array('name'=>$institutename));
        if (!$institute){
            $new_record = new stdClass();
            $new_record->name = $institutename;
            $new_record->userid = $USER->id;
            $new_record->status = 0;
            $new_record->timemodified = time();
            $record = $DB->insert_record('local_et_institutes', $new_record);
        } else {
            $record = $institute->id;
        } 
    }
    
    return $record;
}

function manager_import_course($coursename = '', $coursetype = 0, $instituteid = 0){
    global $USER, $DB;
    $record = 0;
    
    if (!empty($coursename)){
        $course = $DB->get_record('local_et_courses', array('coursename'=>$coursename));
        if (!$course){
            $new_record = new stdClass();
            $new_record->coursename = $coursename;
            $new_record->coursetype = $coursetype;
            $new_record->instituteid = $instituteid;
            $new_record->userid = $USER->id;
            $new_record->status = 0;
            $new_record->timemodified = time();
            $record = $DB->insert_record('local_et_courses', $new_record);
        } else {
            $record = $course->id;
        }  
    }
    
    return $record;
}


function manager_enable()
{
    global $OUTPUT, $PAGE;
    
    if(!get_config('local_manager', 'enabled')){
        $PAGE->set_context(context_system::instance());
        $PAGE->set_heading(get_string('pluginname', 'local_manager'));

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('pluginname', 'local_manager'));
        $url = new moodle_url($CFG->wwwroot.'/index.php');
        echo $OUTPUT->confirm(get_string('returntohome', 'local_manager'), $url, $url);
        echo $OUTPUT->footer();
        die;
    }
}


function get_approvals_count($type){
    global $DB, $USER;
    $count = 0;
    
    if ($type == 'etraining'){
        $records = $DB->get_record_sql("SELECT COUNT(id) as count FROM {local_etraining} WHERE status = 0");
        if ($records->count) $count = $records->count;
    } elseif ($type == 'ecourses'){
        $records = $DB->get_record_sql("SELECT COUNT(id) as count FROM {local_et_courses} WHERE status = 0");
        if ($records->count) $count = $records->count;
    } elseif ($type == 'einstitutes'){
        $records = $DB->get_record_sql("SELECT COUNT(id) as count FROM {local_et_institutes} WHERE status = 0");
        if ($records->count) $count = $records->count;
    } elseif ($type == 'enrollment'){

        $sql = "SELECT COUNT(ue.id) as count FROM {user_enrolments} ue LEFT JOIN {enrol} e ON e.id = ue.enrolid WHERE ue.status > 0 AND e.enrol = 'approval'";
        
        if (is_siteadmin()){    
        } elseif(has_capability('local/manager:manageallcourses', $systemcontext)) {
            $sql .= " AND e.courseid IN (
                            SELECT c.id FROM  {course} c
                                LEFT JOIN {course_categories} cat ON cat.id = c.category
                            WHERE (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) 
                                )";
        } else {
            $sql .= " AND e.courseid IN (
                SELECT c.id FROM {user_enrolments} ue 
                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                    LEFT JOIN {course} c ON c.id = e.courseid
                    LEFT JOIN {course_categories} cat ON cat.id = c.category
                WHERE ue.userid = $USER->id AND ue.status = 0 AND (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) 
                )";
        }
        
        $records = $DB->get_record_sql($sql);
        if ($records->count) $count = $records->count;
    }
        
    
    return $count;
}


