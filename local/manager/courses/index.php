<?php
require_once('../../../config.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // course id

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:managecourses', $systemcontext);

$USER->editing = 0;
$title = get_string('managecourses', 'local_manager');
$PAGE->set_url('/local/manager/courses/index.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);

$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
/*$active_button = 'managecourses';
include('course_buttons.php');*/
echo $OUTPUT->heading($title);
echo html_writer::start_tag('div', array('class'=>'users-manage-box'));
?>
<div class="tq-users-tabs">
	<div class="users-tabs">
		<div class="users-tabs-item course-tab visible">
            <div class="users-create-panel clearfix">
                <button class="button" onclick="location = '<?php echo $CFG->wwwroot;?>/course/edit.php?category=1'"><i class="fa fa-plus"></i> <?php echo get_string('createnewcourse', 'local_manager');?></button>
            </div>
			<table title="Courses" id="datatable_courses" class="generaltable sorting data-table" cellpadding="0" cellspacing="0">
				<thead>
					<tr>
						<th align="center" value="id" class="asc"><span><?php echo get_string('ID', 'local_manager'); ?></span></th>
						<th align="center" value="course_name"><span><?php echo get_string('coursename', 'local_manager'); ?></span></th>
						<th align="center" value="course_category"><span><?php echo get_string('category', 'local_manager'); ?></span></th>
						<th align="center" value="course_start"><span><?php echo get_string('startdate', 'local_manager'); ?></span></th>
						<th align="center" value="course_end"><span><?php echo get_string('enddate', 'local_manager'); ?></span></th>
						<th align="center" value="teacher"><span><?php echo get_string('teacher', 'local_manager'); ?></span></th>
                        <?php if(has_capability('moodle/course:create', $systemcontext) or has_capability('moodle/course:delete', $systemcontext) or has_capability('moodle/course:reset', $systemcontext) or has_capability('enrol/manual:enrol', $systemcontext)) : ?>
                            <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager'); ?></span></th>
                        <?php else : ?>
                            <th align="center" class="nosort"><span></span></th>
                        <?php endif; ?>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php
echo html_writer::end_tag('div');
?>

<script type="text/javascript" language="javascript" src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/javascript/datatables/DataTables.min.js"></script>
<link href="<?php echo $CFG->wwwroot; ?>/theme/talentquest/style/jquery.dataTables.css" type="text/css" media="screen" rel="stylesheet" />
<script>
    
    var courses = $('#datatable_courses').dataTable( {
        "bAutoWidth": false,
        "bProcessing": false,
        "bServerSide": true,
        "bStateSave": true,
        "sAjaxSource": "<?php echo $CFG->wwwroot; ?>/local/manager/courses/courses_datatables.php",
        "sDom": 'T<"clear">lfrtip',
        "sPaginationType": "full_numbers",
        "oLanguage": {
            "sSearch": "_INPUT_",
            "sLengthMenu": "<span>_MENU_</span>",
            "oPaginate": { "sFirst": "First", "sLast": "Last" }
        },
        "aoColumnDefs": [ 
                    { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                    { "bSortable": false, "aTargets": [ -1 ] },
                ],
                "oTableTools": {
                    "aButtons": []
                },
    });

    jQuery('.dataTables_filter input').attr('placeholder', '<?php echo get_string('search', 'local_manager'); ?>');
    var filter = '1';
    if (courses.fnSettings().aoPreSearchCols[0].sSearch.length){
        filter = courses.fnSettings().aoPreSearchCols[0].sSearch;
    }
    
    jQuery('.dataTables_filter input').before('<select id="table_filter" style="margin-right: 7px;" onchange="courses.fnFilter( $(this).val(), 0 )"><option value="-1" '+((filter=='-1') ? "selected" : "")+'><?php echo get_string('show_all'); ?></option><option value="1" '+((filter=='1') ? "selected" : "")+'><?php echo get_string('active'); ?></option><option value="0" '+((filter=='0') ? "selected" : "")+'><?php echo get_string('inactive'); ?></option></select>');
   
	function toggleCourse(id, action){
        if (action == 'hidecourse'){
            jQuery('.hideitem_'+id).addClass('hidden');
            jQuery('.showitem_'+id).removeClass('hidden');
        } else {
            jQuery('.hideitem_'+id).removeClass('hidden');
            jQuery('.showitem_'+id).addClass('hidden');
        }
        jQuery.get('<?php echo $CFG->wwwroot; ?>/course/ajax/management.php?courseid='+id+'&action='+action+'&ajax=1&sesskey=<?php echo sesskey(); ?>', function(data){
        });
    }
</script>
<style>
    .users-manage-box .course-tab .dataTables_wrapper {padding-top: 0;}
    .users-manage-box .course-tab .dataTables_wrapper .dataTables_filter {margin-top: -54px;right: 178px;}
</style>
<?php 
/// and proper footer
echo $OUTPUT->footer();
