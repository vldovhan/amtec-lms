<?php

define('AJAX_SCRIPT', true);

require_once('../../../config.php');

$id		= optional_param('id', 0, PARAM_INT);
$iDisplayStart = optional_param('iDisplayStart', 0, PARAM_INT);
$iDisplayLength = optional_param('iDisplayLength', 10, PARAM_INT);
$iSortingCols = optional_param('iSortingCols', '', PARAM_RAW);
$sSearch = optional_param('sSearch', '', PARAM_RAW);
$sSearch_0 = optional_param('sSearch_0', '', PARAM_RAW);
$sEcho = optional_param('sEcho', 0, PARAM_INT);

$systemcontext = context_system::instance();

$group = ' GROUP BY c.id ';

$aColumns = array( 'id', 'course_name', 'course_category', 'course_start', 'course_end', 'teacher', 'actions');
$bColumns = array('id'=>'c.id', 'course_name'=>'c.fullname', 'course_category'=>'cat.name', 'course_start'=>'c.startdate', 'course_end'=>'c.enddate', 'teacher'=>'t.teacher');

/* Indexed column (used for fast and accurate table cardinality) */
$sIndexColumn = "id";

/* 
 * Paging
 */
$sLimit = "";
if ( isset( $iDisplayStart ) && $iDisplayLength != '-1' ){
	$sLimit = "LIMIT ".intval( $iDisplayStart ).", ".$iDisplayLength;
}

/*
 * Ordering
 */
$sOrder = "";
if ( isset( $_GET['iSortCol_0'] ) )
{
	$sOrder = "ORDER BY  ";
	for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
		if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
			$sOrder .= ((is_array($bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]])) ? $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]][0] : $bColumns[$aColumns[ intval( $_GET['iSortCol_'.$i] ) ]]).
				($_GET['sSortDir_'.$i]==='asc' ? ' asc' : ' desc') .", ";
		}
	}
	
	$sOrder = substr_replace( $sOrder, "", -2 );
	if ( $sOrder == "ORDER BY" ){
		$sOrder = "";
	}
}

/* 
 * Filtering
 */

$sWhere = " WHERE c.id > 1 ";

if ($sSearch_0 == '' or $sSearch_0 == 1){
    $sWhere .= " AND c.visible = 1 ";
} elseif ($sSearch_0 != '-1'){
    $sWhere .= " AND c.visible = $sSearch_0 ";
}

if ( $sSearch != "" ){
	$sWhere .= "AND (";
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ($aColumns[$i] == 'actions') continue;
		if (is_array($bColumns[$aColumns[$i]])){
			foreach ($bColumns[$aColumns[$i]] as $col){
				$sWhere .= $col." LIKE '%$sSearch%' OR ";
			}
		} else {
			$sWhere .= $bColumns[$aColumns[$i]]." LIKE '%$sSearch%' OR ";
		}
	}
	$sWhere = substr_replace( $sWhere, "", -3 );
	$sWhere .= ')';
}
$select = "SELECT DISTINCT
			c.id, 
			c.fullname as course_name, 
			c.idnumber, 
			cat.name as course_category,
			cat.type as categorytype,
			c.startdate, 
			c.enddate, 
            c.visible,
			t.teacher, 
			t.id as teacherid";

if (is_siteadmin()){
    $sql = "
	FROM  {course} c
		LEFT JOIN {course_categories} cat ON cat.id = c.category
		LEFT JOIN (SELECT u.id, CONCAT(u.firstname,' ',u.lastname) as teacher, cx.instanceid 
						FROM {role_assignments} ra, {user} u, {context} cx 
					WHERE u.id = ra.userid AND ra.roleid = 3 AND cx.id = ra.contextid and cx.contextlevel = 50 ORDER BY ra.id ASC LIMIT 1) t ON t.instanceid = c.id ";
} else if(has_capability('local/manager:manageallcourses', $systemcontext)) {
    $sWhere .= " AND (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) ";
    $sql = "
        FROM  {course} c
            LEFT JOIN {course_categories} cat ON cat.id = c.category
            LEFT JOIN (SELECT u.id, CONCAT(u.firstname,' ',u.lastname) as teacher, cx.instanceid 
                            FROM {role_assignments} ra, {user} u, {context} cx 
                        WHERE u.id = ra.userid AND ra.roleid = 3 AND cx.id = ra.contextid and cx.contextlevel = 50 ORDER BY ra.id ASC LIMIT 1) t ON t.instanceid = c.id ";
} else {
    $sWhere .= " AND ue.userid = $USER->id AND ue.status = 0 ";
    $sWhere .= " AND (cat.type = 'custom' or (cat.type = 'system' AND cat.visible > 0)) ";
    
    $sql = "
        FROM {user_enrolments} ue 
            LEFT JOIN {enrol} e ON e.id = ue.enrolid
            LEFT JOIN {course} c ON c.id = e.courseid
            LEFT JOIN {course_categories} cat ON cat.id = c.category
            LEFT JOIN (SELECT u.id, CONCAT(u.firstname,' ',u.lastname) as teacher, cx.instanceid 
                            FROM {role_assignments} ra, {user} u, {context} cx 
                        WHERE u.id = ra.userid AND ra.roleid = 3 AND cx.id = ra.contextid and cx.contextlevel = 50 ORDER BY ra.id ASC) t ON t.instanceid = c.id";
}
//echo $select.$sql.$sWhere.' '.$group.' '.' '.$sOrder.' '.$sLimit;
$rResult = $DB->get_records_sql($select.$sql.$sWhere.' '.$group.' '.' '.$sOrder.' '.$sLimit);
$count_rResult = $DB->get_record_sql("SELECT COUNT(DISTINCT c.id) as count ".$sql.$sWhere);

$iFilteredTotal = $count_rResult->count;
$iTotal = $count_rResult->count;

/*
 * Output
 */
$output = array(
	"sEcho" => $sEcho,
	"iTotalRecords" => $iTotal,
	"iTotalDisplayRecords" => $iFilteredTotal,
	"aaData" => array()
);

foreach ($rResult as $aRow){
	$aRow = (array)$aRow;
	$row = array();
    $coursecontext = context_course::instance($aRow['id']);
	for ( $i=0 ; $i<count($aColumns) ; $i++ ){
		if ( $aColumns[$i] == "version" ){
			/* Special output formatting for 'version' column */
			$row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
		} elseif ( $aColumns[$i] != ' '){
			if ($aColumns[$i] == 'course_name'){
				$row[] = '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$aRow['id'].'">'.$aRow[ $aColumns[$i] ].'</a>';
			} elseif ($aColumns[$i] == 'course_start'){
				$row[] = ($aRow['startdate']) ? date('M d Y', $aRow['startdate']) : '-' ;
            } elseif ($aColumns[$i] == 'course_end'){
                $row[] = ($aRow['enddate']) ? date('M d Y', $aRow['enddate']) : '-' ;
			} elseif ($aColumns[$i] == 'actions'){
                $edit_link = ''; $delete_link = ''; $restore_link = ''; $enroll_link = ''; $showhide_link = '';
                if(has_capability('moodle/course:create', $systemcontext)){
                    $edit_link = '<a href="'.$CFG->wwwroot.'/course/edit.php?id='.$aRow['id'].'" title="'.get_string('edit').'"><i class="fa fa-pencil"></i></a>';
                }
                if(has_capability('moodle/course:delete', $systemcontext)){
                    $delete_link = '&nbsp;<a href="'.$CFG->wwwroot.'/course/delete.php?id='.$aRow['id'].'" title="'.get_string('delete').'"><i class="fa fa-trash-o"></i></a>';
                }
                if(has_capability('moodle/course:reset', $systemcontext)){
                    $restore_link = '&nbsp;<a href="'.$CFG->wwwroot.'/course/reset.php?id='.$aRow['id'].'" title="'.get_string('reset').'"><i class="fa fa-refresh"></i></a>';
                }
                if(has_capability('enrol/manual:enrol', $systemcontext)){
                    $enroll_link = '&nbsp;<a href="'.$CFG->wwwroot.'/enrol/users.php?id='.$aRow['id'].'" title="'.get_string('enrollments', 'local_manager').'"><i class="fa fa-user"></i></a>';
                }
                
                $showhide_link = '&nbsp;<a href="javascript:void(0)"; class="tb-actions hideitem_'.$aRow['id'].((($aRow['visible'] > 0)) ? '' : ' hidden').'" onclick="toggleCourse('.$aRow['id'].', \'hidecourse\');" title="'.get_string('inactive').'"><i class="fa fa-eye"></i></a>';    
                $showhide_link .= '&nbsp;<a href="javascript:void(0)"; class="tb-actions showitem_'.$aRow['id'].((($aRow['visible'] > 0)) ? ' hidden' : '').'" onclick="toggleCourse('.$aRow['id'].', \'showcourse\');" title="'.get_string('active').'"><i style="margin-left:-3px;" class="fa fa-eye-slash"></i></a>'; 
                
                if (!has_capability('moodle/course:update', $coursecontext)){
                    $edit_link = ''; $enroll_link = '';
                }
                $row[] = $edit_link.$delete_link.$restore_link.$enroll_link.$showhide_link;
			} else {
				$row[] = ($aRow[ $aColumns[$i] ]) ? $aRow[ $aColumns[$i] ] : 'Not available';
			}
		}
	}
	$output['aaData'][] = $row;
}

echo json_encode( $output );

exit;