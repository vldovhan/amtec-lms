<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');
require ('report_table.php');
require_once('lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$userid     = optional_param('userid', 0, PARAM_INT);
$id         = optional_param('id', 0, PARAM_INT);

$systemcontext   = context_system::instance();
require_login();
require_capability('local/gamification:view', $systemcontext);
$title = get_string('leaderboards', 'local_gamification');
gamification_enable();

$PAGE->set_url(new moodle_url("/local/gamification/index.php", array()));
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new report_table('table', $id, $search, $userid, $download);
$table->is_collapsible = false;
$table->is_downloading($download, $title, $title);

if (!$download){
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'gamification-search-form'));
    
    if (has_capability('local/gamification:submit', $systemcontext)){
        echo html_writer::start_tag("label", array('style'=>'margin-right: 5px;'));
        echo gamification_get_users_select($userid, 'course');
        echo html_writer::end_tag("label");
    }
    
    echo html_writer::start_tag("label", array('style'=>'margin-right: 5px;'));
        echo gamification_get_contests_select($id);
    echo html_writer::end_tag("label");
    
    echo html_writer::start_tag("label",  array());
    echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_gamification').' ...', 'value' => $search));
    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('filter', 'local_gamification')));
    echo html_writer::end_tag("label");
    echo html_writer::end_tag("form");
}

$table->out(20, true);

if (!$download){
    echo $OUTPUT->footer();
}
