<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Gamification';
$string['leaderboards'] = 'Leaderboards';
$string['contests'] = 'Contests';
$string['gcontests'] = 'Manage Contests';
$string['leaderboards'] = 'Leaderboards';
$string['gamificationmanage'] = 'Manage announcements';
$string['settings'] = 'Settings';
$string['required_field'] = 'Field is required!'; 
$string['description'] = 'Description'; 
$string['startdate'] = 'Start date'; 
$string['enddate'] = 'End date'; 
$string['state'] = 'Publish state'; 
$string['visibility'] = 'Visibility'; 
$string['create_new'] = 'Сreate New'; 
$string['created'] = 'Created by'; 
$string['actions'] = 'Actions'; 
$string['nothing'] = 'Nothing to display'; 
$string['save'] = 'Save'; 
$string['create'] = 'Create'; 
$string['name'] = 'Name'; 
$string['search'] = 'Search'; 
$string['close'] = 'Close'; 
$string['body'] = 'Body'; 
$string['type'] = 'Type'; 
$string['data'] = 'Data'; 
$string['enabled'] = 'Enabled Gamification';
$string['enabled_desc'] = 'Enable Gamification';
$string['number'] = 'Number of leaders';
$string['number_desc'] = 'Number of users to show on leaderboard';
$string['goal'] = 'Contest Goal';
$string['criteria'] = 'Criteria Type';
$string['timecreated'] = 'Created';
$string['courses'] = 'Courses';
$string['createcontest'] = 'Create Contest';
$string['editcontest'] = 'Edit Contest';
$string['status'] = 'Status';
$string['active'] = 'Active';
$string['inactive'] = 'Inactive';
$string['goal1'] = 'Highest gamification scorer';
$string['goal2'] = 'Complete highest number of courses';
$string['goal3'] = 'Highest number of forum "likes"';
$string['goal4'] = 'Highest course score';
$string['goal5'] = 'Most New Blog Comments';
$string['goal6'] = 'Most New Blog Posts';
$string['formcourses'] = 'Courses:';
$string['any'] = 'Any Course';
$string['manual'] = 'Manual Courses';
$string['goal1points'] = 'Points';
$string['goal2points'] = 'Courses completed';
$string['goal3points'] = 'Forum "likes"';
$string['goal4points'] = 'Average course score';
$string['goal5points'] = 'Blog Comments';
$string['goal6points'] = 'Blog Posts';
$string['submitandaward'] = 'Submit and Award';
$string['place_1'] = '1st place';
$string['place_2'] = '2nd place';
$string['place_3'] = '3rd place';
$string['place_4'] = '4rth place';
$string['place_5'] = '5th place';
$string['submitted'] = 'Submitted';
$string['selectuser'] = 'Select user...';
$string['selectleaderboard'] = 'Select contests...';
$string['username'] = 'User Name';
$string['contestname'] = 'Contest';
$string['place'] = 'Place';
$string['points'] = 'Points';
$string['creatorname'] = 'Submitted by';
$string['timecreated'] = 'Submitted on';
$string['filter'] = 'Filter';
$string['create_new_contest'] = 'Create New Contest';
$string['open'] = 'Open';
$string['closed'] = 'Closed';
$string['formusers'] = 'Assign Users';
$string['formgroups'] = 'Assign Groups';

$string['event_gm_submitted'] = 'Leaderboard Submitted';
$string['event_gm_submitted_description'] = 'Leaderboard was Submitted';
$string['gamification:manage'] = 'Gamification manage';
$string['gamification:submit'] = 'Gamification submit and award badges';
$string['gamification:view'] = 'Gamification view';
$string['gamification:viewallcontests'] = 'Gamification view all contests';
$string['gamification:managebadges'] = 'Manage Badges';


