<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Gamification version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */




function gamification_get_goals(){
    global $DB, $USER;
    $goals = array(
        1 => get_string('goal1', 'local_gamification'),
        2 => get_string('goal2', 'local_gamification'),
        3 => get_string('goal3', 'local_gamification'),
        4 => get_string('goal4', 'local_gamification'),
        5 => get_string('goal5', 'local_gamification'),
        6 => get_string('goal6', 'local_gamification'),
    );

    return $goals;
}

function gamification_get_leaderboards() {
    global $CFG, $DB, $USER, $OUTPUT, $PAGE;

    $sql = "SELECT gc.* 
                    FROM {local_gm_contests} gc
                LEFT JOIN {local_gm_courses} gcc ON gcc.instanceid = gc.id
                LEFT JOIN {local_gm_users} gu ON gu.instanceid = gc.id
                    WHERE  (gc.startdate <=".time()." AND gc.enddate >= ".time().") AND
                            gc.status > 0 AND 
                            (
                                ((gc.goal = 1 OR (gc.goal > 1 AND (gc.criteria = 'any'))) AND gu.userid = $USER->id OR gu.groupid IN (SELECT cm.cohortid FROM {cohort_members} cm WHERE cm.userid = $USER->id)) 
                            OR
                                (gc.goal > 1 AND gc.criteria = 'manual' AND gcc.courseid IN (
                                    SELECT c.id FROM {user_enrolments} ue
                                        LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                        LEFT JOIN {course} c ON c.id = e.courseid
                                            WHERE ue.userid = $USER->id
                                    )
                                )
                            )
                        GROUP BY gc.id
                        ORDER BY gc.timecreated ASC";
    $leaderboards = $DB->get_records_sql($sql);
    return $leaderboards;
}

function gamification_print_leaderboard($leaderboard = null) {
    global $CFG, $DB, $COURSE, $PAGE, $OUTPUT;
    $output = '';
    
    $users = gamification_get_users($leaderboard);
    $output .= html_writer::start_tag('div', array('class' => 'leaderboard-box'));
    
    if (!empty($leaderboard->description)){
        $output .= html_writer::tag('div', $leaderboard->description, array('class' => 'leaderboard-description'));
    }
    
    if (count($users)){
        $i = 1; $old_points = 0;
        $output .= html_writer::start_tag('ul', array('class' => 'leaderboard-list'));
        foreach ($users as $user){
            if ($i == 1 and $old_points == 0) {    
                $old_points = (isset($user->points) and $user->points > 0) ? round($user->points) : 0;
            } elseif ($old_points > 0 and (isset($user->points) and $user->points != $old_points)){
                $old_points = (isset($user->points) and $user->points > 0) ? round($user->points) : 0;
                $i++;                
            }
            
            $params = array();
            $output .= html_writer::start_tag('li', array('class' => 'leaderboard-list-item clearfix'));

                $output .= html_writer::tag('div', $OUTPUT->user_picture($user), array('class' => 'leaderboard-table-picture'));
                $output .= html_writer::start_tag('div', array('class' => 'leaderboard-table-user'));
                    $output .= html_writer::tag('div', fullname($user), array('class' => 'user-name'));
                    $output .= html_writer::tag('span', (isset($user->points) and $user->points > 0) ? round($user->points).' '.get_string('goal'.$leaderboard->goal.'points', 'local_gamification') : '', array('class' => 'user-points'));   
                $output .= html_writer::end_tag('div');
                $output .= html_writer::tag('div', '<span class="trophy place-'.$i.'"><i class="fa fa-trophy"></i></span>', array('class' => 'leaderboard-table-icon', 'title'=>($i <= 5) ? get_string('place_'.$i, 'local_gamification') : ''));
            
            $output .= html_writer::end_tag('li');
        }
        $output .= html_writer::end_tag('ul');
        
        if (has_capability('local/gamification:submit', context_system::instance())){
            $output .= html_writer::start_tag('div', array('class' => 'leaderboard-submit-box'));
                $output .= html_writer::tag('button', get_string('submitandaward', 'local_gamification'), array('class' => 'leaderboard-submit-btn', 'onclick'=>'leaderboardSubmit('.$leaderboard->id.');'));
            $output .= html_writer::end_tag('div');
        }
    } else {
        $output .= html_writer::tag('div', get_string('noratings', 'block_sb_leaderboard'), array('class'=>'alert alert-success'));
    }

    $output .= html_writer::end_tag('div');

    return $output;
}

function gamification_get_users($leaderboard) {
    global $CFG, $DB, $COURSE, $PAGE, $USER;
    $users = array();
    $limit = get_config('local_gamification', 'number');
    $enrol_courses = enrol_get_my_courses();
    $courses_in_sql = ''; $courses_in = array(); $users_in_sql = ''; $users_in = array(); $groups_in = array(); $users_in_array = array();
    
    if (count($enrol_courses)){
        foreach ($enrol_courses as $course){
            $courses_in[$course->id] = $course->id;
        }
    }
    
    if ($leaderboard->goal > 1 and $leaderboard->criteria == 'manual'){
        $leaderboard_courses = $DB->get_records('local_gm_courses', array('instanceid'=>$leaderboard->id));
        $l_courses = array();
        if (count($leaderboard_courses)){
            foreach ($leaderboard_courses as $course){
                $l_courses[$course->id] = $course->id;
            }
            $courses_in = array_intersect($courses_in, $l_courses);
        }
        
        if (count($courses_in)){
            $courses_in_sql = implode(', ', $courses_in);    
        } else {
            $courses_in_sql = "SELECT c.id FROM {user_enrolments} ue
                                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                    LEFT JOIN {course} c ON c.id = e.courseid
                                        WHERE ue.userid = $USER->id";
        }
        $users_in_sql = "AND u.id IN (
                                SELECT ue.userid FROM {user_enrolments} ue
                                        LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                            WHERE e.courseid IN ($courses_in_sql)
                            )";
    } elseif ($leaderboard->goal == 1 or ($leaderboard->goal > 1 and $leaderboard->criteria == 'any')){
        $leaderboard_users = $DB->get_records_sql('SELECT * FROM {local_gm_users} WHERE instanceid = '.$leaderboard->id.' AND userid > 0');
        $leaderboard_groups = $DB->get_records_sql('SELECT * FROM {local_gm_users} WHERE instanceid = '.$leaderboard->id.' AND groupid > 0');
        if (count($leaderboard_users)){
            foreach ($leaderboard_users as $lu){
                $users_in[$lu->userid] = $lu->userid;
            }
            $users_in_array[] = " u.id IN (".implode(', ', $users_in).")";
        }
        if (count($leaderboard_groups)){
            foreach ($leaderboard_groups as $lg){
                $groups_in[$lu->id] = $lu->id;
            }
            $users_in_array[] = " u.id IN (SELECT userid FROM {cohort_members} WHERE id IN (".implode(', ', $groups_in).")) ";
        }
        if (count($leaderboard_users) or count($leaderboard_groups)){
           $users_in_sql .= " AND (".implode(' OR ', $users_in_array).") ";
        } else {
           $users_in_sql .= " AND (u.id = 0) ";
        }
    }
    
    if ($leaderboard->goal == 1){
        $sql = "SELECT u.*, bi.sumpoints as points
                    FROM {user} u
                        LEFT JOIN (SELECT userid, SUM(points) as sumpoints FROM {badge_issued} WHERE (dateexpire IS NULL OR dateexpire < ".time().") AND visible > 0 GROUP BY userid) bi ON bi.userid = u.id
                    WHERE u.id > 1 AND u.deleted = 0 AND u.suspended = 0 AND (bi.sumpoints > 0) $users_in_sql
                        GROUP BY u.id ORDER BY bi.sumpoints DESC LIMIT $limit";
    } elseif ($leaderboard->goal == 2){
        $sql = "SELECT u.*, cc.points 
                    FROM {user} u
                        LEFT JOIN (SELECT userid, COUNT(id) as points FROM {course_completions} WHERE timecompleted > 0 GROUP BY userid) cc ON cc.userid = u.id
                    WHERE u.id > 1 AND u.deleted = 0 AND u.suspended = 0 AND cc.points > 0 $users_in_sql GROUP BY u.id ORDER BY cc.points DESC, u.timecreated ASC LIMIT $limit";
    } elseif ($leaderboard->goal == 3){
        $courses_select = ($leaderboard->criteria == 'manual') ? "AND f.course IN ($courses_in_sql)" : "";
        $sql = "SELECT u.*, fl.points 
                    FROM {user} u
                        LEFT JOIN (SELECT ua.userid, COUNT(ua.id) AS points FROM {forum_user_activity} ua LEFT JOIN {forum} f ON f.id = ua.forumid WHERE ua.type = 'like' $courses_select GROUP BY ua.userid) fl ON fl.userid = u.id
                    WHERE u.id > 1 AND u.deleted = 0 AND u.suspended = 0 AND fl.points > 0 $users_in_sql GROUP BY u.id ORDER BY fl.points DESC, u.timecreated ASC";
    } elseif ($leaderboard->goal == 4){
        $courses_select = ($leaderboard->criteria == 'manual') ? "AND gi.courseid IN ($courses_in_sql)" : "";
        $sql = "SELECT u.*, cg.points 
                    FROM {user} u
                        LEFT JOIN (SELECT g.userid, AVG( (g.finalgrade/g.rawgrademax)*100) AS points FROM {grade_items} gi, {grade_grades} g WHERE gi.itemtype = 'course' AND g.itemid = gi.id AND g.finalgrade IS NOT NULL  $courses_select GROUP BY g.userid) cg ON cg.userid = u.id
                    WHERE u.id > 1 AND u.deleted = 0 AND u.suspended = 0 AND cg.points > 0 $users_in_sql GROUP BY u.id ORDER BY cg.points DESC, u.timecreated ASC LIMIT $limit";
    } elseif ($leaderboard->goal == 5){
        $courses_select = ($leaderboard->criteria == 'manual') ? "AND p.courseid IN ($courses_in_sql)" : "";
        $sql = "SELECT u.*, bc.points 
                    FROM {user} u
                        LEFT JOIN (SELECT c.userid, COUNT(c.id) AS points FROM {comments} c LEFT JOIN {post} p ON p.id = c.itemid WHERE c.component = 'blog' AND p.module = 'blog' $courses_select GROUP BY c.userid) bc ON bc.userid = u.id
                    WHERE u.id > 1 AND u.deleted = 0 AND u.suspended = 0 AND bc.points > 0 $users_in_sql GROUP BY u.id ORDER BY bc.points DESC, u.timecreated ASC";
    } elseif ($leaderboard->goal == 6){
        $courses_select = ($leaderboard->criteria == 'manual') ? "AND p.courseid IN ($courses_in_sql)" : "";
        $sql = "SELECT u.*, bl.points 
                    FROM {user} u
                        LEFT JOIN (SELECT p.userid, COUNT(p.id) AS points FROM {post} p WHERE module = 'blog' $courses_select GROUP BY p.userid) bl ON bl.userid = u.id
                    WHERE u.id > 1 AND u.deleted = 0 AND u.suspended = 0 AND bl.points > 0 $users_in_sql GROUP BY u.id ORDER BY bl.points DESC, u.timecreated ASC";
    }
    
    $users = $DB->get_records_sql($sql);
    
    return $users;
}


function gamification_enable()
{
    global $OUTPUT, $PAGE;
    
    if(!get_config('local_gamification', 'enabled')){
        $PAGE->set_context(context_system::instance());
        $PAGE->set_heading(get_string('pluginname', 'local_gamification'));

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('pluginname', 'local_gamification'));
        $url = new moodle_url($CFG->wwwroot.'/index.php');
        echo $OUTPUT->confirm(get_string('returntohome', 'local_transcripts'), $url, $url);
        echo $OUTPUT->footer();
        die;
    }
}

function gamification_submit_leaderboard($leaderboard = null) {
    global $CFG, $DB, $USER, $SITE;
    $output = '';
    
    $users = gamification_get_users($leaderboard);
    $limit = get_config('local_gamification', 'number');
    
    if (count($users)){
        $i = 1; $old_points = 0;
        foreach ($users as $user){
            if ($i == 1 and $old_points == 0) {    
                $old_points = (isset($user->points) and $user->points > 0) ? round($user->points) : 0;
            } elseif ($old_points > 0 and (isset($user->points) and $user->points != $old_points)){
                $old_points = (isset($user->points) and $user->points > 0) ? round($user->points) : 0;
                $i++;                
            }
            
            $existing_records = $DB->get_records('local_gm_results', array('contestid'=>$leaderboard->id, 'userid'=>$user->id, 'status'=>0));
            if (count($existing_records)){
                foreach ($existing_records as $record){
                    $record->status = 1;
                    $DB->update_record('local_gm_results', $record);
                }
            }
            
            $new_gm_submit = new stdClass();
            $new_gm_submit->contestid = $leaderboard->id;
            $new_gm_submit->userid = $user->id;
            $new_gm_submit->creator = $USER->id;
            $new_gm_submit->place = $i;
            $new_gm_submit->points = (isset($user->points) and $user->points > 0) ? round($user->points) : 0;
            $new_gm_submit->timecreated = time();
            $new_gm_submit->id = $DB->insert_record('local_gm_results', $new_gm_submit);
            
            if ($new_gm_submit->id){
              
              $event_properties = array('userid' => $new_gm_submit->userid, 'relateduserid'=> $new_gm_submit->userid, 'context'=>context_system::instance(), 'courseid'=>$SITE->id, 'objectid'=>$new_gm_submit->id);
              $event = \local_gamification\event\local_gamification_submitted::create($event_properties);
              $event->trigger();   
            }
        }
    }

    return get_string('submitted', 'local_gamification');
}

function gamification_get_users_select($id = 0){
    global $DB, $USER;
    $output = '';
    
    $where = '';
    if(!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance())){
        $where .= " AND u.managerid = $USER->id";
    }
    
    $gamification_users = $DB->get_records_sql("SELECT DISTINCT(u.id), u.* FROM {local_gm_results} gr LEFT JOIN {user} u ON u.id = gr.userid WHERE u.deleted = 0 AND u.suspended = 0 $where ORDER BY u.firstname, u.lastname");
    
    $output .= '<select name = "userid">';
    $output .= html_writer::tag('option', get_string('selectuser', 'local_gamification'), array('value' => '0'));
    
    if (count($gamification_users)){
        foreach ($gamification_users as $user){
            $params = array();
            $params['value'] = $user->id;
            if ($id == $user->id) $params['selected'] = 'selected';
            $output .= html_writer::tag('option', fullname($user), $params);
        }
    }
                          
    $output .= '</select>';
        
    return $output;
}

function gamification_get_contests_select($id = 0){
    global $DB, $USER;
    $output = '';
    
    $gamification_contests = $DB->get_records_sql("SELECT DISTINCT(gc.id), gc.* FROM {local_gm_results} gr LEFT JOIN {local_gm_contests} gc ON gc.id = gr.contestid WHERE gc.id > 0 ORDER BY gc.name");
    
    $output .= '<select name = "id">';
    $output .= html_writer::tag('option', get_string('selectleaderboard', 'local_gamification'), array('value' => '0'));
    
    if (count($gamification_contests)){
        foreach ($gamification_contests as $contest){
            $params = array();
            $params['value'] = $contest->id;
            if ($id == $contest->id) $params['selected'] = 'selected';
            $output .= html_writer::tag('option', $contest->name, $params);
        }
    }
                          
    $output .= '</select>';
        
    return $output;
}

function gamification_get_status_filter($status = 1){
    global $DB, $USER;
    $output = '';
    
    $output .= html_writer::start_tag('select', array("name" => "status", "onchange"=>"this.form.submit()"));
    $output .= html_writer::tag('option', get_string('show_all'), array('value' => '-1'));
    
    $params = array();
    $params['value'] = 0;
    if ($status == 0) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('inactive'), $params);
    
    $params = array();
    $params['value'] = 1;
    if ($status == 1) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('active'), $params);
    $output .= html_writer::end_tag('select');
        
    return $output;
}

function gamification_get_dates_filter($filter = 1){
    global $DB, $USER;
    $output = '';
    
    $output .= html_writer::start_tag('select', array("name" => "filter", "onchange"=>"this.form.submit()"));
    $output .= html_writer::tag('option', get_string('show_all'), array('value' => '-1'));
    
    $params = array();
    $params['value'] = 0;
    if ($filter == 0) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('closed', 'local_gamification'), $params);
    
    $params = array();
    $params['value'] = 1;
    if ($filter == 1) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('open', 'local_gamification'), $params);
                          
    $output .= html_writer::end_tag('select');
        
    return $output;
}

