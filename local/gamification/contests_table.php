<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class contests_table extends table_sql {
    function __construct($uniqueid, $search, $status, $filter) {
		global $CFG, $USER, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('name', 'startdate', 'enddate', 'goal', 'created', 'timecreated', 'actions');
        $this->define_columns($columns);
        $headers = array(
            get_string('name', 'local_gamification'),
            get_string('startdate', 'local_gamification'),
            get_string('enddate', 'local_gamification'),
            get_string('goal', 'local_gamification'),
            get_string('created', 'local_gamification'),
            get_string('timecreated', 'local_gamification'),
            get_string('actions', 'local_gamification'));
       
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (c.name LIKE '%$search%' OR c.description LIKE '%$search%')" : "";
        $fields = "c.id, c.name, c.startdate, c.enddate, c.goal, c.criteria, c.timecreated, CONCAT(u.firstname, ' ', u.lastname) as created, c.status, c.id as actions ";
        $from = "{local_gm_contests} c LEFT JOIN {user} u ON u.id = c.userid ";
        $where = 'c.id > 0'.$sql_search;
        
        if (!has_capability('local/gamification:viewallcontests', context_system::instance())){
            $where .= ' AND c.userid = '.$USER->id;
        }
        
        if ($status >= 0){
            $where .= ' AND c.status = '.$status;
        }
        
        if ($filter > 0){
            $now = time();
            $where .= " AND (c.startdate <= $now AND c.enddate >= $now) ";
        } elseif ($filter == 0){
            $now = time();
            $where .= " AND (c.startdate > $now OR c.enddate < $now) ";
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$PAGE->url/local/gamification/contests.php?search=$search&filter=$filter&status=$status");
    }
    
    function col_type($values) {
       return ($values->type) ? ucfirst($values->type) : '-';
    }
    function col_startdate($values) {
       return ($values->startdate) ? date("d M, Y", $values->startdate) : '-';
    }
    function col_enddate($values) {
       return ($values->enddate) ? date("d M, Y", $values->enddate) : '-';
    }
    function col_goal($values) {
       require_once('lib.php');
       $goals = gamification_get_goals();
       return (isset($goals[$values->goal])) ? $goals[$values->goal] : '';
    }
    function col_timecreated($values) {
       return ($values->timecreated) ? date("d M, Y", $values->timecreated) : '-';
    }
    
    function col_actions($values) {
      global $OUTPUT, $PAGE;
        
        if ($this->is_downloading()){
            return '';
        }
        
      $strdelete  = get_string('delete');
      $stredit  = get_string('edit');
      $strshow  = get_string('show');
      $strhide  = get_string('hide');

        $return_url = $PAGE->url;
        $edit = array();
        $aurl = new moodle_url('/local/gamification/edit_contest.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/gamification/edit_contest.php', array('action'=>'delete', 'id'=>$values->id, 'return'=>$return_url));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')));
        
        if ($values->status > 0){
            $aurl = new moodle_url('/local/gamification/edit_contest.php', array('action'=>'hide', 'id'=>$values->id, 'return'=>$return_url));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/gamification/edit_contest.php', array('action'=>'show', 'id'=>$values->id, 'return'=>$return_url));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall')));    
        }
        
      return implode('', $edit);
    }
}
