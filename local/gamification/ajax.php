<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../config.php');
require_once('lib.php');

$action = optional_param('action', '', PARAM_RAW);
$id     = optional_param('id', 0, PARAM_INT);

$PAGE->set_url(new moodle_url("/local/gamification/ajax.php", array()));
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());

if ($action == 'load-lb' and $id > 0){
    
    $leaderboard = $DB->get_record('local_gm_contests', array('id'=>$id));
    echo gamification_print_leaderboard($leaderboard);
    
} elseif ($action == 'submit' and $id > 0){
    $leaderboard = $DB->get_record('local_gm_contests', array('id'=>$id));
    echo gamification_submit_leaderboard($leaderboard);
}

exit;