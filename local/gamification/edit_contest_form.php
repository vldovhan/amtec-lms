<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir.'/formslib.php');
require_once('lib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform {
    protected $id;
    protected $contest;
    protected $context;
    
    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE;

        $mform              = $this->_form;
        $id                 = $this->_customdata['id'];
        $contest            = $this->_customdata['contest'];
        $contest_courses    = $this->_customdata['contest_courses'];
        $contest_users      = $this->_customdata['contest_users'];
        $contest_groups     = $this->_customdata['contest_groups'];
        $editoroptions      = $this->_customdata['editoroptions'];
        
        $systemcontext   = context_system::instance();
        
        $this->id  = $id;
        $this->context = $systemcontext;
        $this->contest = $contest;

        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', get_string('name', 'local_gamification'), 'maxlength="254"  size="50"');
        $mform->addRule('name', get_string('required_field', 'local_gamification'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);
        
        $choices = array();
        $choices['0'] = get_string('inactive');
        $choices['1'] = get_string('active');
        $mform->addElement('select', 'status', get_string('status', 'local_gamification'), $choices);
        $mform->setDefault('status', 1);
        $mform->setType('status', PARAM_INT);
        
        $mform->addElement('date_selector', 'startdate', get_string('startdate', 'local_gamification'));
        $mform->addRule('startdate', get_string('required_field', 'local_gamification'), 'required', null, 'client');
        
        $mform->addElement('date_selector', 'enddate', get_string('enddate', 'local_gamification'));
        $mform->addRule('enddate', get_string('required_field', 'local_gamification'), 'required', null, 'client');
        
        $goals = gamification_get_goals();
        array_unshift($goals, '');
        $mform->addElement('select', 'goal', get_string('goal', 'local_gamification'), $goals);
        $mform->addRule('goal', get_string('required_field', 'local_gamification'), 'required', null, 'client');
        $mform->setDefault('goal', 0);
        $mform->setType('goal', PARAM_INT);
        
        $choices = array();
        $choices['any'] = get_string('any', 'local_gamification');
        $choices['manual'] = get_string('manual', 'local_gamification');
        $mform->addElement('select', 'criteria', get_string('criteria', 'local_gamification'), $choices);
        $mform->setDefault('criteria', 'any');
        $mform->setType('criteria', PARAM_RAW);
        
        $choices = array();
        $choices = $this->get_courses();
        $mform->addElement('select', 'courses', get_string('formcourses', 'local_gamification'), $choices, array('multiple'=>'multiple', 'class'=>'multi-select'));
        $mform->setType('courses', PARAM_RAW);
        
        if (isset($contest->id) and $contest->criteria == 'manual' and count($contest_courses)){
            $mform->getElement('courses')->setSelected($contest_courses);
		}
        
        $choices = array();
        $choices = $this->get_users();
        $mform->addElement('select', 'users', get_string('formusers', 'local_gamification'), $choices, array('multiple'=>'multiple', 'class'=>'multi-select'));
        $mform->setType('users', PARAM_RAW);
        
        if (isset($contest->id) and $contest->criteria == 'any' and count($contest_users)){
            $mform->getElement('users')->setSelected($contest_users);
		}
        
        $choices = array();
        $choices = $this->get_groups();
        $mform->addElement('select', 'groups', get_string('formgroups', 'local_gamification'), $choices, array('multiple'=>'multiple', 'class'=>'multi-select'));
        $mform->setType('groups', PARAM_RAW);
        
        if (isset($contest->id) and $contest->criteria == 'any' and count($contest_groups)){
            $mform->getElement('groups')->setSelected($contest_groups);
		}
        
        
        $mform->addElement('editor', 'description_editor', get_string('description', 'local_gamification'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);
        
        $this->add_action_buttons(get_string('cancel'), (($id > 0) ? get_string('save', 'local_gamification') : get_string('create', 'local_gamification')));

        // Finally set the current form data
        $this->set_data($contest);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data() {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }
    
    function get_courses(){
		global $DB, $USER;
		$courses = array();
        
        if (is_siteadmin() or has_capability('local/gamification:viewallcontests', context_system::instance())){
            $db_courses = $DB->get_records_sql('SELECT c.* FROM {course} c LEFT JOIN {course_format_options} co ON co.courseid = c.id AND co.format = "talentquest" AND co.name = "status" WHERE c.id > 1 AND c.visible > 0 AND (co.value = "1" OR co.value IS NULL)');
            if (count($db_courses) > 0){
                foreach ($db_courses as $course){
                    $courses[$course->id] = $course->fullname;
                }
            }    
        } else {
            $db_courses = enrol_get_my_courses('', 'fullname');
            if (count($db_courses) > 0){
                foreach ($db_courses as $course){
                    if(!has_capability('moodle/course:manageactivities', context_course::instance($course->id))) continue;
                    $courses[$course->id] = $course->fullname;
                }
            }    
        }
        
		return $courses;
	}
    
    function get_users(){
		global $DB, $USER;
		$users = array();
        
        $usersrc = $DB->get_records_sql("SELECT * FROM {user} WHERE id > 1 AND deleted = 0 AND confirmed = 1 AND suspended = 0 ORDER BY firstname ASC");
        foreach($usersrc as $user){
            $users[$user->id] = fullname($user);
        }
        
		return $users;
	}
    
    function get_groups(){
		global $DB, $USER;
		$cohorts = array();
        
        $sql = "SELECT id, name FROM {cohort} WHERE visible = :visible ORDER BY name ASC";
        $cohorts = $DB->get_records_sql_menu($sql, array('visible'=>1));
        
		return $cohorts;
	}
}

