<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Transcripts
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class report_table extends table_sql {
    
    public $id = 0;
    public $search = '';
    public $userid = 0;
    public $url_params = '';
    
    function __construct($uniqueid, $id = 0, $search, $userid = 0, $download = false) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $this->id = $id;
        $this->search = $search;
        $this->userid = $userid;
        
        $columns = array('username', 'contestname', 'place', 'points', 'creatorname', 'timecreated');
        $headers = array(
            get_string('username', 'local_gamification'),
            get_string('contestname', 'local_gamification'),
            get_string('place', 'local_gamification'),
            get_string('points', 'local_gamification'),
            get_string('creatorname', 'local_gamification'),
            get_string('timecreated', 'local_gamification'));
        
        if (!has_capability('local/gamification:submit', $systemcontext)){
            unset($columns[0]);
            $headers = array(
                get_string('contestname', 'local_gamification'),
                get_string('place', 'local_gamification'),
                get_string('points', 'local_gamification'),
                get_string('creatorname', 'local_gamification'),
                get_string('timecreated', 'local_gamification'));
        }
        
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (gc.name LIKE '%$search%' OR gc.description LIKE '%$search%')" : "";
        $fields = "gr.id, gr.userid, gc.name as contestname, CONCAT(u.firstname, ' ', u.lastname) as username, CONCAT(uc.firstname, ' ', uc.lastname) as creatorname, gr.place, gr.points, gr.timecreated, gc.goal";
        $from = "{local_gm_results} gr LEFT JOIN {local_gm_contests} gc ON gc.id = gr.contestid LEFT JOIN {user} u ON u.id = gr.userid LEFT JOIN {user} uc ON uc.id = gr.creator ";
        $where = 'gr.id > 0 AND u.deleted = 0 AND u.suspended = 0 AND u.id > 1'.$sql_search;
        
        if (!has_capability('local/gamification:submit', $systemcontext)){
            $where .= ' AND gr.userid = '.$USER->id;
        } elseif(!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance())){
            $where .= " AND u.managerid = $USER->id";
        }
        if ($userid > 0){
            $where .= ' AND gr.userid = '.$userid;
        }        
        if ($id > 0){
            $where .= ' AND gr.contestid = '.$id;
        }
        
        $url_params = array();
        if ($userid > 0) $url_params['userid'] = $userid;
        if ($search != '') $url_params['search'] = $search;
        if ($id > 0) $url_params['id'] = $id;
        $this->url_params = $url_params;
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl(new moodle_url($CFG->wwwroot.'/local/gamification/index.php', $this->url_params));
    }
    
    function col_timecreated($values) {
      return ($values->timecreated) ? date('h:i a, m/d/Y', $values->timecreated) : '-';
    }
    
    function col_points($values) {
      return ($values->points > 0) ? $values->points.' '.get_string('goal'.$values->goal.'points', 'local_gamification') : 0;
    }
}
