<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');
require ('contests_table.php');
require_once('lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$status     = optional_param('status', 1, PARAM_INT);
$filter     = optional_param('filter', 1, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/gamification:manage', $systemcontext);
$title = get_string('gcontests', 'local_gamification');

$PAGE->set_url(new moodle_url("/local/gamification/contests.php", array('search'=>$search, 'status'=>$status, 'filter'=>$filter)));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new contests_table('table', $search, $status, $filter);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);
echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'gamification-search-form'));
echo html_writer::tag("label", gamification_get_dates_filter($filter), array('style'=>'margin-right: 5px;'));
echo html_writer::tag("label", gamification_get_status_filter($status), array('style'=>'margin-right: 5px;'));
echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_gamification').' ...', 'value' => $search));
echo html_writer::end_tag("label");

$url = new moodle_url('/local/gamification/edit_contest.php');
echo html_writer::link($url, html_writer::tag('i', '', array('class'=>'fa fa-plus')).get_string('create_new_contest', 'local_gamification'), array('class'=>'btn btn-warning'));

echo html_writer::end_tag("form");

$table->out(20, true);

echo $OUTPUT->footer();
