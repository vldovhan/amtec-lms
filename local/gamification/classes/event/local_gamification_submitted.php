<?php
/**
 * The local_gamification viewed event.
 *
 * @package   local_gamification
 * @author    SEBALE
 * @copyright 2016 TalentQuest
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 */

namespace local_gamification\event;
defined('MOODLE_INTERNAL') || die();

class local_gamification_submitted extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'с';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'local_gm_results';
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_gm_submitted', 'local_gamification');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        $a = (object) array('userid' => $this->userid, 'planid' => $this->objectid);
        return get_string('event_gm_submitted_description', 'local_plans', $a);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array
     */
    protected function get_legacy_logdata() {
        return(array($this->courseid, 'course', 'gm submitted',
                'view.php?pageid=' . $this->objectid, get_string('event_gm_submitted', 'local_plans'), $this->contextinstanceid));
    }

    /**
     * Get URL related to the action.
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/local/gamification/index.php', array('id' => $this->objectid));
    }
}