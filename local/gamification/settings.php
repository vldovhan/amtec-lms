<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_gamification', get_string('pluginname', 'local_gamification'));
$ADMIN->add('localplugins', $settings);

if (get_config('local_gamification', 'enabled')){

    $ADMIN->add('root', new admin_category('gamificationroot', get_string('pluginname', 'local_gamification')));

    $ADMIN->add('gamificationroot', new admin_externalpage('contests', get_string('contests', 'local_gamification'), $CFG->wwwroot.'/local/gamification/contests.php', 'local/gamification:manage'));
    
}

$name = 'local_gamification/enabled';
$title = get_string('enabled', 'local_gamification');
$description = get_string('enabled_desc', 'local_gamification');
$default = true;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$settings->add($setting);

$name = 'local_gamification/number';
$title = get_string('number', 'local_gamification');
$description = get_string('number_desc', 'local_gamification');
$default = '10';
$setting = new admin_setting_configtext($name, $title, $description, $default, PARAM_RAW, 5);
$settings->add($setting);

