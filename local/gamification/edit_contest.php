<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_gamification
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


require('../../config.php');
require_once('lib.php');
require_once('edit_contest_form.php');

$systemcontext   = context_system::instance();
require_login();
require_capability('local/gamification:manage', $systemcontext);
gamification_enable();

$id     = optional_param('id', 0, PARAM_INT); 
$action = optional_param('action', '', PARAM_RAW);
$return = optional_param('return', '', PARAM_RAW);

$PAGE->set_context($systemcontext);
$PAGE->set_pagelayout('admin');
$PAGE->requires->jquery();
$PAGE->requires->js('/theme/talentquest/javascript/jquery.multiple.select.js', true);
$PAGE->requires->css('/theme/talentquest/style/multiple-select.css', true);
$pageparams = array('id' => $id);
$PAGE->set_url('/local/gamification/edit_contest.php', $pageparams);

if ($action == 'delete'){
	$contest = $DB->get_record('local_gm_contests', array('id'=>$id));
	if ($contest->id){
		$DB->delete_records('local_gm_courses', array('instanceid'=>$contest->id));
	}
    $DB->delete_records('local_gm_contests', array('id'=>$contest->id));
    redirect((!empty($return)) ? $return : new moodle_url("/local/gamification/contests.php"));
} elseif(($action == 'hide' or $action == 'show') and $id){
    $contest = $DB->get_record('local_gm_contests', array('id'=>$id));
    if ($contest){
        $contest->status = ($action == 'show') ? 1 : 0;
        $DB->update_record('local_gm_contests', $contest);
    }
    redirect((!empty($return)) ? $return : new moodle_url("/local/gamification/contests.php"));
}

// Prepare course and the editor.
$editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$systemcontext, 'subdirs'=>0);
$contest_courses = null;
$contest_users   = null;
$contest_groups  = null;
if ($id > 0) {
    $contest = $DB->get_record('local_gm_contests', array('id'=>$id));
    if (isset($contest->id)){
        $contest_courses = $DB->get_records_menu('local_gm_courses', array('instanceid'=>$contest->id), 'id', 'id, courseid');         
        $contest_users = $DB->get_records_sql_menu("SELECT id, userid FROM {local_gm_users} WHERE instanceid = :instanceid AND userid > 0", array('instanceid'=>$contest->id));
        $contest_groups = $DB->get_records_sql_menu("SELECT id, groupid FROM {local_gm_users} WHERE instanceid = :instanceid AND groupid > 0", array('instanceid'=>$contest->id));
    }
} else {
    $contest = new stdClass();
    $contest->id = null;
}

$contest = file_prepare_standard_editor($contest, 'description', $editoroptions, $systemcontext, 'local_gamification', 'descriptionfile', $contest->id);

// First create the form.
$args = array(
    'id' => $id,
    'contest' => $contest,
    'contest_courses' => $contest_courses,
    'contest_users' => $contest_users,
    'contest_groups' => $contest_groups,
    'editoroptions' => $editoroptions
);
$editform = new edit_form(null, $args);

if ($editform->is_cancelled()) {
    // The form has been cancelled, take them back to what ever the return to is.
    redirect(new moodle_url('/local/gamification/contests.php'));
} else if ($data = $editform->get_data()) {
    // Process data if submitted.
    
    // save and relink embedded images and save attachments
    $data = file_postupdate_standard_editor($data, 'description', $editoroptions, $systemcontext, 'local_gamification', 'descriptionfile', $contest->id);
   
    $data->userid = $USER->id;
    $data->timecreated = time();
    
    if ($data->id > 0) {
		$contest->id = $data->id;	
        $DB->update_record('local_gm_contests', $data);
	} else {
        $data->id = $DB->insert_record('local_gm_contests', $data);
	}
    if (count($data->courses) and $data->criteria == 'manual'){
        $DB->delete_records_select('local_gm_users', 'instanceid=:instanceid AND groupid > 0', array('instanceid'=>$data->id));
        $DB->delete_records_select('local_gm_users', 'instanceid=:instanceid AND userid > 0', array('instanceid'=>$data->id));
        $DB->delete_records('local_gm_courses', array('instanceid'=>$data->id));
        foreach ($data->courses as $course){
            $contest_course = new stdClass();
            $contest_course->instanceid = $data->id;
            $contest_course->courseid = $course;
            $DB->insert_record('local_gm_courses', $contest_course);
        }
    }
    
    if (count($data->users) and $data->criteria == 'any' or $data->goal == 1){
        $DB->delete_records('local_gm_courses', array('instanceid'=>$data->id));
        $DB->delete_records_select('local_gm_users', 'instanceid=:instanceid AND userid > 0', array('instanceid'=>$data->id));
        foreach ($data->users as $user){
            $contest_user = new stdClass();
            $contest_user->instanceid = $data->id;
            $contest_user->userid = $user;
            $DB->insert_record('local_gm_users', $contest_user);
        }
    }
    
    if (count($data->groups) and $data->criteria == 'any' or $data->goal == 1){
        $DB->delete_records('local_gm_courses', array('instanceid'=>$data->id));
        $DB->delete_records_select('local_gm_users', 'instanceid=:instanceid AND groupid > 0', array('instanceid'=>$data->id));
        foreach ($data->groups as $group){
            $contest_group = new stdClass();
            $contest_group->instanceid = $data->id;
            $contest_group->groupid = $group;
            $DB->insert_record('local_gm_users', $contest_group);
        }
    }
    
    redirect(new moodle_url('/local/gamification/contests.php'));
}

// Print the form.

$site = get_site();
$title = ($id > 0) ? get_string('editcontest', 'local_gamification') : get_string('createcontest', 'local_gamification');
$PAGE->navbar->add(get_string('gcontests', 'local_gamification'), new moodle_url("/local/gamification/contests.php"));
$PAGE->navbar->add($title);

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$editform->display();

echo $OUTPUT->footer(); ?>
<style>
.mform .felement.fgroup span {display: inline-block; width: 45%; max-height:40px; min-height:25px; overflow:hidden; vertical-align:top;}
.mform .felement.fgroup span span {display: inline;width: auto;}
</style>
<script>
    
    <?php if (!isset($contest->goal) or (isset($contest->goal) and $contest->goal == 1)) : ?>
        jQuery('#fitem_id_criteria').addClass('hidden');
        jQuery('#fitem_id_courses').addClass('hidden');
    <?php endif; ?>
    <?php if (isset($contest->goal) and $contest->goal != 1 and $contest->criteria == 'manual') : ?>
        jQuery('#fitem_id_users').addClass('hidden');
        jQuery('#fitem_id_groups').addClass('hidden');
    <?php endif; ?>
    <?php if (isset($contest->goal) and $contest->goal != 1 and $contest->criteria != 'manual') : ?>
        jQuery('#fitem_id_courses').addClass('hidden');
    <?php endif; ?>
    
	jQuery('#id_goal').change(function(event){
		var goal = jQuery(this).val();
		if (goal){
			if (goal == 1){
				jQuery('#fitem_id_criteria').addClass('hidden');
                jQuery('#fitem_id_courses').addClass('hidden');
			} else {
				jQuery('#fitem_id_criteria').removeClass('hidden');
			}
		} else {
			jQuery('#fitem_id_criteria').addClass('hidden');
            jQuery('#fitem_id_courses').addClass('hidden');
		}
	});
    
    jQuery('#id_criteria').change(function(event){
		var criteria = jQuery(this).val();
		if (criteria == 'manual'){
			jQuery('#fitem_id_courses').removeClass('hidden');
            jQuery('#fitem_id_users').addClass('hidden');
            jQuery('#fitem_id_groups').addClass('hidden');
		} else {
            jQuery('#fitem_id_courses').addClass('hidden');
            jQuery('#fitem_id_users').removeClass('hidden');
            jQuery('#fitem_id_groups').removeClass('hidden');
		}
	});
	
</script>
<?php 