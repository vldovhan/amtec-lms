<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');


class issue_certificate_form extends moodleform {

    function definition() {
        global $DB;

        $mform =& $this->_form;
        $id             = $this->_customdata['id'];
        $record = $DB->get_record('local_certificate_issues',array('id'=>$id));
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        if(isset($record->id))
            $mform->setDefault('id', $record->id);
        
        $users_obj = get_users_listing('firstname');
        $students = array();
        foreach($users_obj as $student)
            $students[$student->id] = fullname($student);

        $mform->addElement('select', 'user', get_string('user'), $students);
        $mform->addRule('user', null, 'required', null, 'client');
        $mform->setType('user', PARAM_INT);
        if(isset($record->id))
            $mform->setDefault('user', $record->userid);

        $certificates_obj = $DB->get_records('local_certificate',array('manual'=>1));
        $certificates = array();
        foreach($certificates_obj as $item)
            $certificates[$item->id] = $item->name;

        $mform->addElement('select', 'certificate', get_string('certificate','local_certificate'), $certificates);
        $mform->addRule('certificate', null, 'required', null, 'client');
        $mform->setType('certificate', PARAM_INT);
        if(isset($record->id))
            $mform->setDefault('certificate', $record->certificateid);

        $mform->addElement('text', 'code', get_string('code','local_certificate'));
        $mform->setType('code', PARAM_TEXT);
        if(isset($record->id))
            $mform->setDefault('code', $record->code);

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return ;
    }
}

