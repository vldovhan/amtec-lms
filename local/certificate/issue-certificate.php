<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require('lib.php');
require ('issue_certificate_form.php');

$id = optional_param('id',0,PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

$title = get_string('issue_certificate','local_certificate');

$PAGE->set_url(new moodle_url("/local/certificate/issue-certificate.php", array()));
$PAGE->navbar->add(get_string('user_certificates', 'local_certificate'),new moodle_url("/local/certificate/user-certificate.php", array()));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new issue_certificate_form(null,array('id'=>$id));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    if(empty($data->code))
        $data->code = certificate_generate_code();

    if($record = $DB->get_record('local_certificate_issues',array('id'=>$data->id))){
        $record->userid = $data->user;
        $record->code = $data->code;
        $record->certificateid = $data->certificate;
        $DB->update_record('local_certificate_issues',$record);

        redirect(new moodle_url('/local/certificate/user-certificate.php'));
    }else{
        $record = new stdClass();
        $record->userid = $data->user;
        $record->code = $data->code;
        $record->certificateid = $data->certificate;
        $record->timecreated = time();
        $DB->insert_record('local_certificate_issues', $record);

        redirect(new moodle_url('/local/certificate/user-certificate.php'));
    }
}elseif($form->is_cancelled()){
    redirect(new moodle_url('/local/certificate/user-certificate.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
