<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class user_certificate_table extends table_sql {
    protected $is_teacher = false;

    function __construct($uniqueid, $student,$is_teacher) {
        global $CFG, $USER;

        $this->is_teacher = $is_teacher;

        parent::__construct($uniqueid);

        $where = ' ci.id>0 ';
        $params = array();
        $columns = array('username');
        $header = array(get_string('user'));
        if(!has_capability('local/certificate:view_all_cert',context_system::instance())){
            $where = " ci.userid=:userid ";
            $params = array('userid'=>$student);
            $columns = array();
            $header = array();
        }
        
        if(!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance()) and has_capability('local/certificate:view_all_cert',context_system::instance())){
            $where .= " AND u.managerid = $USER->id";
        }

        $columns[] = 'name';
        $columns[] = 'timecreated';
        $columns[] = 'actions';

        $header[] = get_string('name');
        $header[] = get_string('issued','local_certificate');
        $header[] = get_string('actions','local_certificate');

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "ci.id, ci.userid, ci.code, ci.timecreated, c.name, CONCAT(u.firstname,' ', u.lastname) as username";
        $from = "{local_certificate_issues} ci
                    LEFT JOIN {local_certificate} c ON ci.certificateid=c.id
                    LEFT JOIN {user} u ON ci.userid=u.id";

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function col_timecreated($values){
        return date('l, j F Y, g:i A',usertime($values->timecreated));
    }

    function col_actions($values) {
        global $OUTPUT;
        if ($this->is_downloading())
            return '';
        $edit = array();
        if($this->is_teacher){
            $aurl = new moodle_url('/local/certificate/user-certificate.php', array('action'=>'delete', 'id'=>$values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));
        }

        $aurl = new moodle_url('/local/certificate/view.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', get_string('get_certificate', 'local_certificate'), 'core', array('class' => 'iconsmall')), null, array('target'=>'_blank'));
        
        return implode(' ',$edit);
    }
}
