<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');


class edit_certificate_form extends moodleform {

    function definition() {
        global $DB,$USER;

        $mform =& $this->_form;
        $certificate             = $this->_customdata['certificate'];

        if(has_capability('local/certificate:edit_all_data', context_system::instance(), $USER->id, false))
            $courses = get_courses();
        else
            $courses = enrol_get_my_courses();



        $all_courses = array();
        foreach($courses as $course){
            if($course->id == 1) continue;
            $all_courses[$course->id] = $course->fullname;
        }

        $templates = $DB->get_records('local_certificate_template');
        $all_templates = array();
        foreach($templates as $template){
            $all_templates[$template->id] = $template->name;
        }

        $plans = $DB->get_records('local_plans',array('type'=>0,'visible'=>1));
        $all_plans = array();
        foreach($plans as $plan){
            $all_plans[$plan->id] = $plan->name;
        }
        
        $record = $DB->get_record('local_certificate',array('id'=>$certificate));
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        if(isset($record->id))
            $mform->setDefault('id', $record->id);

        $mform->addElement('text', 'name', get_string('name','local_certificate'));
        $mform->addRule('name', null, 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        $mform->addElement('select', 'template', get_string('template','local_certificate'), $all_templates);
        $mform->setType('template', PARAM_INT);
        $mform->addRule('template', null, 'required', null, 'client');

        $select = $mform->addElement('select', 'courses', get_string('courses'), $all_courses,array('class'=>'multi-select'));
        $select->setMultiple(true);
        $mform->setType('courses', PARAM_INT);

        if(get_config('local_plans', 'enabled_plans')){
            $select_p = $mform->addElement('select', 'plans', get_string('plans', 'local_certificate'), $all_plans,array('class'=>'multi-select'));
            $select_p->setMultiple(true);
            $mform->setType('plans', PARAM_INT);
        }

        $mform->addElement('advcheckbox', 'manual', get_string('awarding_manual', 'local_certificate'), '', array('group' => 1), array(0, 1));
        $mform->addElement('advcheckbox', 'auto', get_string('awarding_auto', 'local_certificate'), '', array('group' => 2), array(0, 1));

        if(isset($record->id)){
            $mform->setDefault('name', $record->name);
            $mform->setDefault('template', $record->template);
            $mform->setDefault('manual', $record->manual);
            $mform->setDefault('auto', $record->auto);
            $select->setSelected(explode(',',$record->courses));
            if(get_config('local_certificate', 'enabled')){
                $select_p->setSelected(explode(',', $record->plans));
            }
        }



        $this->add_action_buttons();
    }

    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }
}

