<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('certificate_table.php');
require ('lib.php');

$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$confirm   = optional_param('confirm', 0, PARAM_BOOL);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

// check if enabled
certificate_enabled();

$PAGE->set_url(new moodle_url("/local/certificate/certificates.php", array()));
$PAGE->set_context($systemcontext);
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('certificates', 'local_certificate'), new moodle_url('/local/certificate/certificates.php'));

if ($action == 'delete' and $id) {
    if ($confirm and confirm_sesskey()) {
        $DB->delete_records('local_certificate',array('id'=>$id));
        $DB->delete_records('local_certificate_issues',array('certificateid'=>$id));
    }else{
        $PAGE->set_title(get_string('delete_certificate','local_certificate'));
        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('delete_certificate','local_certificate'));
        $yesurl = new moodle_url($CFG->wwwroot . '/local/certificate/certificates.php', array('id' => $id, 'delete' => 1, 'action'=>'delete',
            'confirm' => 1, 'sesskey' => sesskey()));
        $message = get_string('confirm_delete_certificate','local_certificate');
        echo $OUTPUT->confirm($message, $yesurl, $CFG->wwwroot."/local/certificate/certificates.php");
        echo $OUTPUT->footer();
        die;
    }
}

$title = get_string('managecertificates','local_certificate');

$PAGE->set_pagelayout('report');
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new certificates_table('table');
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_certificate');
$renderer->certificate_print_tabs('certificates');

echo html_writer::start_div('certificate-btn-box clearfix');
echo html_writer::link(new moodle_url("/local/certificate/edit-certificate.php"), html_writer::tag('i', '', array('class'=>'fa fa-plus')).get_string('create_new_certificate', 'local_certificate'), array('class' => 'btn btn-warning'));
echo html_writer::end_div();
$table->out(20, true);

echo $OUTPUT->footer();

