<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('template_fields_table.php');

$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$template   = optional_param('template', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

$template_obj = $DB->get_record('local_certificate_template',array('id'=>$template));
if($template_obj->is_system && !has_capability('local/certificate:edit_all_data', $systemcontext, $USER->id, false))
    redirect(new moodle_url('/local/certificate/templates.php'));

if ($action == 'delete' and $id) {
    $DB->delete_records('local_cert_template_field',array('id'=>$id));
}

$title = get_string('template_fields','local_certificate');

$PAGE->set_url(new moodle_url("/local/certificate/template-fields.php", array()));
$PAGE->navbar->add(get_string('templates', 'local_certificate'),new moodle_url("/local/certificate/templates.php", array()));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new fields_table('table',$template);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo html_writer::start_div('certificate-btn-box clearfix');
echo html_writer::link(new moodle_url("/local/certificate/edit-field.php", array("id" => $template,'type'=>'img')),get_string('create_field_img', 'local_certificate'), array('class' => 'btn btn-warning'));
echo html_writer::link(new moodle_url("/local/certificate/edit-field.php", array("id" => $template,'type'=>'text')),get_string('create_field_text', 'local_certificate'), array('class' => 'btn btn-warning'));
echo html_writer::link(new moodle_url("/local/certificate/preview.php", array("template" => $template)),get_string('preview'), array('class' => 'btn btn-warning','target'=>'_blank'));
echo html_writer::end_div();

$table->out(20, true);

echo $OUTPUT->footer();

