<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$handlers = array(
    'course_completed' => array (
        'handlerfile'     => '/local/certificate/locallib.php',
        'handlerfunction' => array('events_handler', 'course_completed'),
        'schedule'        => 'instant',
        'internal'        => 1,
    ),
);

$observers = array(

    array(
        'eventname'   => '\local_plans\event\local_plans_completed',
        'callback'    => 'local_certificate_observer::local_plans_completed',
    ),

);