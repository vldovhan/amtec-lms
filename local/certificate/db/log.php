<?php

/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$logs = array(
    array('local'=>'certificate', 'action'=>'view', 'mtable'=>'local_certificate', 'field'=>'name'),
    array('local'=>'certificate', 'action'=>'add', 'mtable'=>'local_certificate', 'field'=>'name'),
    array('local'=>'certificate', 'action'=>'update', 'mtable'=>'local_certificate', 'field'=>'name'),
    array('local'=>'certificate', 'action'=>'received', 'mtable'=>'local_certificate', 'field'=>'name'),
);
