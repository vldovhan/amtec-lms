<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class fields_table extends table_sql {

    function __construct($uniqueid, $template) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('id','z_index','name','type','actions');
        $header = array(get_string('id','local_certificate'),get_string('z-index','local_certificate'), get_string('fieldname','local_certificate'),get_string('fieldtype','local_certificate') ,get_string('actions','local_certificate'));

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "ctf.*";
        $from = "{local_cert_template_field} ctf";

        $where = " ctf.template_id=:template ";

        $this->set_sql($fields, $from, $where, array('template'=>$template));
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function col_actions($values) {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }
        $edit = array();

        $aurl = new moodle_url('/local/certificate/template-fields.php', array('action'=>'delete', 'id'=>$values->id, 'template'=>$values->template_id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));

        $aurl = new moodle_url('/local/certificate/edit-field.php', array('field'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', get_string('manage','local_certificate'), 'core', array('class' => 'iconsmall')), null, array());

        return implode('', $edit);
    }
}
