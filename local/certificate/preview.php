<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once("lib.php");
require_once($CFG->libdir."/pdflib.php");

$template = required_param('template',PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:view', $systemcontext);

$data = $DB->get_record("local_certificate_template",array('id'=>$template));
$data->name = 'Preview: '.$data->name;
$data->courses = 1;
$data->time_issued = time();
$data->code = certificate_generate_code();

$fields = $DB->get_records_sql("SELECT ctf.*
                             FROM {local_cert_template_field} ctf
                              WHERE ctf.template_id=:id",array('id'=>$template));
$fields_sort = array();
foreach($fields as $field){
    $fields_sort[$field->z_index.$field->id] = $field;
}
ksort($fields_sort);
$fields = $fields_sort;
unset($fields_sort);

$pdf = new PDF($data->orientation, 'mm', 'A4', true, 'UTF-8', false);


$pdf->SetTitle($data->name);
$pdf->SetProtection(array('modify'));
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
$pdf->SetAutoPageBreak(false, 0);
$pdf->AddPage();

foreach($fields as $template_field){
    $option = unserialize($template_field->value);

    $pdf->SetAlpha((int)$option->alpha/100);
    if($template_field->type == "text"){
        $font_color = hex2rgb($option->font_color);
        $pdf->SetTextColor($font_color['r'], $font_color['g'], $font_color['b']);
        certificate_print_text($pdf, $data, $option->x, $option->y, $option->text_leveling, $option->text_font, $option->font_style, $option->font_size, $option->text_area, $option->w, $option->text_types, $option->rotate, $option->spacing);
    }elseif($template_field->type == "img"){
        certificate_print_image($pdf, $data, $option->image_name, $option->x, $option->y, $option->w, $option->h,$template_field->id);
    }
    $pdf->SetAlpha(1);

}


$pdf->Output(clean_filename(rtrim($data->name, '.').".pdf"), 'I');


