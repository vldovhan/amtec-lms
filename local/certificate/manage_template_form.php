<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');


class manage_template_form extends moodleform {

    function definition() {
        global $DB,$USER;

        $mform =& $this->_form;
        $template_id             = $this->_customdata['template'];
        $record = $DB->get_record('local_certificate_template',array('id'=>$template_id));
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        if(isset($record->id))
            $mform->setDefault('id', $record->id);

        $mform->addElement('text', 'template_name', get_string('name','local_certificate'));
        $mform->addRule('template_name', null, 'required', null, 'client');
        $mform->setType('template_name', PARAM_TEXT);
        if(isset($record->id))
            $mform->setDefault('template_name', $record->name);

        $orientation = array( 'L' => get_string('landscape', 'local_certificate'), 'P' => get_string('portrait', 'local_certificate'));
        $mform->addElement('select', 'orientation', get_string('orientation', 'local_certificate'), $orientation);
        $mform->setDefault('orientation', 'landscape');
        $mform->addHelpButton('orientation', 'orientation', 'local_certificate');
        $mform->addRule('orientation', null, 'required', null, 'client');
        if(isset($record->id))
            $mform->setDefault('orientation', $record->orientation);

        if(has_capability('local/certificate:edit_all_data', context_system::instance(), $USER->id, false)){
            $mform->addElement('advcheckbox', 'is_system', get_string('is_system_template', 'local_certificate'), '', array('group' => 1), array(0, 1));
            if(isset($record->id))
                $mform->setDefault('is_system', $record->is_system);
        }

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return ;
    }
}

