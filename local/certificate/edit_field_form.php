<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');


class edit_field_text_form extends moodleform {

    function definition() {
        global $CFG;

        $field_param  = $this->_customdata['field_param'];
        $template_id  = $this->_customdata['template_id'];

        $text_types = array(
            "date" => get_string('date'),
            //"grade" => get_string('grade'),
            //"teacher" => get_string('teacher','local_certificate'),
            "user_lastname" => get_string('userlastname','local_certificate'),
            "user_firstname" => get_string('userfirstname','local_certificate'),
            "user_name" => get_string('username','local_certificate'),
            "course_name" => get_string('coursename','local_certificate'),
            "code" => get_string('code','local_certificate'),
            "certname" => get_string('certificatename','local_certificate'),
            "custom_text" => get_string('customtext','local_certificate')
        );

        $text_leveling = array(
            "L" => get_string('left','local_certificate'),
            "C" => get_string('center','local_certificate'),
            "R" => get_string('right','local_certificate'),
            "J" => get_string('justify','local_certificate')
        );
        $font_style = array(
            "" => "Normal",
            "B" => "Bold",
            "U" => "Underline",
            "D" => "Line Through",
            "O" => "Overline",
            "I" => "Italic"
        );
        $text_fonts = array(
            "freeserif" => "Free Serif",
            "times" => "Times-Roman",
            "helvetica" => "Helvetica",
            "courier" => "Courier",
            "symbol" => "Symbol",
            "zapfdingbats" => "ZapfDingbats",
            "freesans" => "Free Sans"
        );
        $mform =& $this->_form;
        $mform->addElement('text', 'field_name', get_string('fieldname','local_certificate'));
        $mform->addRule('field_name', null, 'required', null, 'client');
        $mform->setType('field_name', PARAM_TEXT);

        $mform->addElement('text', 'z', get_string('z-index','local_certificate'));
        $mform->setType('z', PARAM_INT);
        $mform->addRule('z', null, 'required', null, 'client');

        $mform->addElement('select', 'text_types', get_string('texttypes','local_certificate'), $text_types,array("onmouseup"=>"schow_text_input();"));
        $mform->addElement('html', '<div id="text_area" style="display:none;" class="clearfix">');
        $mform->addElement('textarea', 'text_area', get_string('customtext','local_certificate'));
        $mform->setType('text_area', PARAM_RAW);
        $mform->addElement('html', '</div>');
        $mform->addElement('select', 'text_leveling', get_string('textalign','local_certificate'), $text_leveling);
        $mform->addElement('select', 'text_font', get_string('textfont','local_certificate'), $text_fonts);
        $mform->addElement('select', 'font_style', get_string('fontstyle','local_certificate'), $font_style);
        $mform->addElement('text', 'font_size', get_string('fontsize','local_certificate'));
        $mform->setType('font_size', PARAM_INT);
        $mform->addElement('text', 'font_color', get_string('fontcolor','local_certificate'),array('class'=>"ColorPicker",'placeholder'=>'545454'));
        $mform->setType('font_color', PARAM_RAW);
        $mform->addElement('text', 'x', get_string('positionx','local_certificate'));
        $mform->setType('x', PARAM_INT);
        $mform->addElement('text', 'y', get_string('positiony','local_certificate'));
        $mform->setType('y', PARAM_FLOAT);
        $mform->addElement('text', 'w', get_string('width','local_certificate'));
        $mform->setType('w', PARAM_INT);
        $mform->addElement('text', 'rotate', get_string('rotate','local_certificate'));
        $mform->setType('rotate', PARAM_INT);
        $mform->addElement('text', 'alpha', get_string('alpha','local_certificate'));
        $mform->setType('alpha', PARAM_INT);
        $mform->setDefault('alpha', "100");
        $mform->addElement('text', 'spacing', get_string('spacing','local_certificate'));
        $mform->setType('spacing', PARAM_INT);
        $mform->setDefault('spacing', "0");

        $mform->addElement('hidden', 'field_type', 'field_type');
        $mform->setType('field_type', PARAM_RAW);
        $mform->setDefault('field_type', "text");

        $mform->addElement('hidden', 'id', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'template_id', $template_id);
        $mform->setType('template_id', PARAM_INT);


        if(isset($field_param->id)){
            $value = unserialize($field_param->value);

            $mform->setDefault('field_name', $field_param->name);
            $mform->setDefault('z', $field_param->z_index);
            $mform->setDefault('font_color', $value->font_color);
            $mform->setDefault('w', $value->w);
            $mform->setDefault('y', $value->y);
            $mform->setDefault('x', $value->x);
            $mform->setDefault('alpha', $value->alpha);
            $mform->setDefault('rotate', $value->rotate);
            $mform->setDefault('font_size', $value->font_size);
            $mform->setDefault('font_style', $value->font_style);
            $mform->setDefault('text_font', $value->text_font);
            $mform->setDefault('text_leveling', $value->text_leveling);
            $mform->setDefault('text_area', $value->text_area);
            $mform->setDefault('text_types', $value->text_types);
            $mform->setDefault('spacing', $value->spacing);

            $mform->setDefault('id', $field_param->id);
        }

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return parent::validation($data, $files);
    }
}
class edit_field_img_form extends moodleform {

    function definition() {
        global $CFG;
        $mform =& $this->_form;

        $field_param  = $this->_customdata['field_param'];
        $template_id  = $this->_customdata['template_id'];

        $mform->addElement('text', 'field_name', get_string('fieldname','local_certificate'));
        $mform->addRule('field_name', null, 'required', null, 'client');
        $mform->setType('field_name', PARAM_TEXT);

        $mform->addElement('text', 'z', get_string('z-index','local_certificate'));
        $mform->setType('z', PARAM_INT);
        $mform->addRule('z', null, 'required', null, 'client');

        $mform->addElement('filepicker', 'image', get_string('image','local_certificate'), null, array( 'accepted_types' => array('jpe' => 'image/jpeg',
            'jpeIE' => 'image/pjpeg',
            'jpeg' => 'image/jpeg',
            'jpegIE' => 'image/pjpeg',
            'jpg' => 'image/jpeg',
            'jpgIE' => 'image/pjpeg',
            'png' => 'image/png',
            'pngIE' => 'image/x-png')));

        $mform->addElement('text', 'x', get_string('positionx','local_certificate'));
        $mform->setType('x', PARAM_INT);
        $mform->addElement('text','y', get_string('positiony','local_certificate'));
        $mform->setType('y', PARAM_FLOAT);
        $mform->addElement('text', 'w', get_string('width','local_certificate'));
        $mform->setType('w', PARAM_INT);
        $mform->addElement('text','h', get_string('height','local_certificate'));
        $mform->setType('h', PARAM_FLOAT);
        $mform->addElement('text', 'alpha', get_string('alpha','local_certificate'));
        $mform->setType('alpha', PARAM_INT);
        $mform->setDefault('alpha', "100");

        $mform->addElement('hidden','field_type', 'field_type');
        $mform->setType('field_type', PARAM_RAW);
        $mform->setDefault('field_type', "img");

        $mform->addElement('hidden', 'id', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'template_id', $template_id);
        $mform->setType('template_id', PARAM_INT);


        if(isset($field_param->id)){
            $value = unserialize($field_param->value);

            $mform->setDefault('id', $field_param->id);
            $mform->setDefault('field_name', $field_param->name);
            $mform->setDefault('z', $field_param->z_index);
            $mform->setDefault('image', $value->image);
            $mform->setDefault('h', $value->h);
            $mform->setDefault('w', $value->w);
            $mform->setDefault('y', $value->y);
            $mform->setDefault('x', $value->x);
            $mform->setDefault('alpha', $value->alpha);
        }

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return parent::validation($data, $files);
    }
}

