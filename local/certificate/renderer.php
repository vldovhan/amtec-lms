<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer file.
 *
 * @package    local_certificate
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

/**
 * Standard HTML output renderer for badges
 */
class local_certificate_renderer extends plugin_renderer_base {

    // Prints tabs
    public function certificate_print_tabs($current = 'issues') {
        global $DB;

        $systemcontext   = context_system::instance();
        $row = array();
        
        if (has_capability('local/certificate:manage', $systemcontext)) {
            $row[] = new tabobject('certificates',
                        new moodle_url('/local/certificate/certificates.php'),
                        get_string('managecertificates', 'local_certificate')
                    );
        }

        if (has_capability('local/certificate:view', $systemcontext)){
            $row[] = new tabobject('issues',
                        new moodle_url('/local/certificate/user-certificate.php'),
                        get_string('user_certificates', 'local_certificate')
                    );
        }

        if (has_capability('local/certificate:manage', $systemcontext)) {
            $row[] = new tabobject('templates',
                        new moodle_url('/local/certificate/templates.php'),
                        get_string('templates', 'local_certificate')
                    );
        }

        echo $this->tabtree($row, $current);
    }

}
