<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('manage_template_form.php');

$template = optional_param('template',0,PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

if($template)
    $title = get_string('update_templete','local_certificate');
else
    $title = get_string('create_templete','local_certificate');

$PAGE->set_url(new moodle_url("/local/certificate/manage-template.php", array()));
$PAGE->navbar->add(get_string('templates', 'local_certificate'),new moodle_url("/local/certificate/templates.php", array()));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new manage_template_form(null,array('template'=>$template));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    if($record = $DB->get_record('local_certificate_template',array('id'=>$data->id))){
        $record->name = $data->template_name;
        $record->is_system = (isset($data->is_system))?$data->is_system:0;
        $record->orientation =$data->orientation;
        $DB->update_record('local_certificate_template',$record);
        
        redirect(new moodle_url('/local/certificate/templates.php'));
    }else{
        $record = new stdClass();
        $record->name = $data->template_name;
        $record->orientation = $data->orientation;
        $record->is_system = (isset($data->is_system))?$data->is_system:0;
        $record->timecreate = time();
        $DB->insert_record('local_certificate_template', $record);

        redirect(new moodle_url('/local/certificate/templates.php'));
    }
}elseif($form->is_cancelled()){
    redirect(new moodle_url('/local/certificate/templates.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
