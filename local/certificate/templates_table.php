<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class templates_table extends table_sql {

    protected $is_manager;

    function __construct($uniqueid) {
        global $CFG,$USER;

        parent::__construct($uniqueid);

        $columns = array('id','name','timecreate','actions');
        $header = array(get_string('id','local_certificate'),get_string('name'), get_string('created','local_certificate'), get_string('actions','local_certificate'));

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "ct.*";
        $from = "{local_certificate_template} ct";

        $where = " ct.id>0 ";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);

        $this->is_manager = has_capability('local/certificate:edit_all_data', context_system::instance(), $USER->id, false);
    }

    function col_timecreate($values){
        return date('m/d/Y',$values->timecreate);
    }

    function col_actions($values) {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }
        $edit = array();

        if(!$values->is_system || ($values->is_system && $this->is_manager)){
            $aurl = new moodle_url('/local/certificate/templates.php', array('action' => 'delete', 'id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall')), null, array('onclick' => "if (!confirm('Are you sure want to delete this redord?')) return false;"));

            $aurl = new moodle_url('/local/certificate/manage-template.php', array('template' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/editstring', get_string('edit'), 'core', array('class' => 'iconsmall')), null, array());

            $aurl = new moodle_url('/local/certificate/template-fields.php', array('template' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', get_string('manage_fields', 'local_certificate'), 'core', array('class' => 'iconsmall')), null, array());
        }
        $aurl = new moodle_url('/local/certificate/templates.php', array('id'=>$values->id,'action'=>'copy'));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/copy', get_string('copy'), 'core', array('class' => 'iconsmall')), null);

        $aurl = new moodle_url('/local/certificate/preview.php', array('template'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/hide', get_string('preview'), 'core', array('class' => 'iconsmall')), null, array('target'=>'_blank'));

        return implode('', $edit);
    }
}
