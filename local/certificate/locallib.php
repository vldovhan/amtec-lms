<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->dirroot.'/local/certificate/lib.php');

class events_handler{
    public static function course_completed ($data){
        if(get_config('local_certificate', 'enabled')){
            $data->id = $data->course;
            certificate_issue($data);
        }
    }
}


