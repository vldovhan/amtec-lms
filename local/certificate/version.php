<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2016070105; // The current module version (Date: YYYYMMDDXX)
$plugin->requires  = 2014111000; // Requires this Moodle version
$plugin->cron      = 0; // Period for cron to check this module (secs)
$plugin->component = 'local_certificate';

$plugin->maturity  = MATURITY_STABLE;
$plugin->release   = "Stable"; // User-friendly version number
