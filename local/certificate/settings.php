<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_certificate', get_string('pluginname', 'local_certificate'));
$ADMIN->add('localplugins', $settings);

if (get_config('local_certificate', 'enabled')){

    $ADMIN->add('root', new admin_category('certificateroot', get_string('certificateroot', 'local_certificate')));

    $ADMIN->add('certificateroot', new admin_externalpage('certificates', get_string('certificates', 'local_certificate'), $CFG->wwwroot.'/local/certificate/certificates.php', 'local/certificate:manage'));

    $ADMIN->add('certificateroot', new admin_externalpage('templates', get_string('templates', 'local_certificate'), $CFG->wwwroot.'/local/certificate/templates.php', 'local/certificate:manage'));

    $ADMIN->add('certificateroot', new admin_externalpage('user_certificates', get_string('user_certificates', 'local_certificate'), $CFG->wwwroot.'/local/certificate/user-certificate.php', 'local/certificate:view'));

}

$name = 'local_certificate/enabled';
$title = get_string('enabled', 'local_certificate');
$description = get_string('enabled_desc', 'local_certificate');
$default = true;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$settings->add($setting);

