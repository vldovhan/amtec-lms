<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('edit_field_form.php');

$template_id = optional_param('id',0,PARAM_INT);
$field_id = optional_param('field',0,PARAM_INT);
$type = optional_param('type','',PARAM_TEXT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

if($template_id)
    $title = get_string('update_field','local_certificate');
else
    $title = get_string('create_field','local_certificate');

$tempdir = 'certificate_img';
make_temp_directory($tempdir);
$PAGE->set_context($systemcontext);
$PAGE->set_url(new moodle_url("/local/certificate/edit-field.php", array('field'=>$field_id,'type'=>$type)));
$PAGE->navbar->add(get_string('templates', 'local_certificate'),new moodle_url("/local/certificate/templates.php", array()));
$PAGE->navbar->add(get_string('template_fields', 'local_certificate'),new moodle_url("/local/certificate/template-fields.php", array('template'=>$template_id)));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($title);
$PAGE->set_heading($title);

if($field_id > 0){
    $field = $DB->get_record('local_cert_template_field',array('id'=>$field_id));
    $template_obj = $DB->get_record('local_certificate_template',array('id'=>$field->template_id));
    if($template_obj->is_system && !has_capability('local/certificate:edit_all_data', $systemcontext, $USER->id, false))
        redirect(new moodle_url('/local/certificate/templates.php'));

    if($field->type == 'img'){
        $form = new edit_field_img_form("/local/certificate/edit-field.php?type=$field->type&field=$field_id", array('field_param' => $field, 'template_id' => $field->template_id));
        $title = get_string('update_field_img','local_certificate');
    }else{
        $form = new edit_field_text_form("/local/certificate/edit-field.php?type=$field->type&field=$field_id", array('field_param' => $field, 'template_id' => $field->template_id));
        $title = get_string('update_field_text','local_certificate');
    }
}else{
    $template_obj = $DB->get_record('local_certificate_template',array('id'=>$template_id));
    if($template_obj->is_system && !has_capability('local/certificate:edit_all_data', $systemcontext, $USER->id, false))
        redirect(new moodle_url('/local/certificate/templates.php'));

    if($type == 'img'){
        $form = new edit_field_img_form("/local/certificate/edit-field.php?type=$type", array('field_param' => '', 'template_id' => $template_id));
        $title = get_string('create_field_img','local_certificate');
    }else{
        $form = new edit_field_text_form("/local/certificate/edit-field.php?type=$type", array('field_param' => '', 'template_id' => $template_id));
        $title = get_string('create_field_text','local_certificate');
    }
}
$template_id = (isset($field->template_id))?$field->template_id:$template_id;

if (!$form->is_cancelled() && $data = $form->get_data()) {
    $template_id = $data->template_id;
    if($data->field_type == 'text'){
        if($data->id && $field = $DB->get_record('local_cert_template_field',array('id'=>$data->id))){
            $field->template_id = $data->template_id;
            $field->name = $data->field_name;
            $field->z_index = $data->z;
            unset ($data->id);
            unset ($data->z);
            unset ($data->template_id);
            unset ($data->field_name);
            unset ($data->field_type);
            unset ($data->submitbutton);

            $field->value = serialize($data);
            $DB->update_record('local_cert_template_field',$field);
        }else{
            $field = new stdClass();
            $field->template_id = $data->template_id;
            $field->name = $data->field_name;
            $field->z_index = $data->z;
            $field->type = $data->field_type;
            $insert->timecreate = time();
            unset ($data->id);
            unset ($data->z);
            unset ($data->template_id);
            unset ($data->field_name);
            unset ($data->field_type);
            unset ($data->submitbutton);

            $field->value = serialize($data);
            $DB->insert_record('local_cert_template_field',$field);
        }
        redirect(new moodle_url('/local/certificate/template-fields.php', array("template" =>$template_id)));
    }elseif($data->field_type == 'img'){
        $data->image_name = $form->get_new_filename('image');
        $file = $CFG->tempdir . '/' . $tempdir . '/' . $data->image_name;


        if($data->id && $field = $DB->get_record('local_cert_template_field',array('id'=>$data->id))){
            $id = $data->id;
            $field->template_id = $data->template_id;
            $field->name = $data->field_name;
            $field->z_index = $data->z;
            unset ($data->id);
            unset ($data->z);
            unset ($data->template_id);
            unset ($data->field_name);
            unset ($data->field_type);
            unset ($data->submitbutton);

            $field->value = serialize($data);
            $DB->update_record('local_cert_template_field',$field);
        }else{
            $field = new stdClass();
            $field->template_id = $data->template_id;
            $field->name = $data->field_name;
            $field->z_index = $data->z;
            $field->type = $data->field_type;
            $insert->timecreate = time();
            unset ($data->id);
            unset ($data->z);
            unset ($data->template_id);
            unset ($data->field_name);
            unset ($data->field_type);
            unset ($data->submitbutton);

            $field->value = serialize($data);
            $id = $DB->insert_record('local_cert_template_field',$field);
        }
        $form->save_stored_file('image',$systemcontext->id,'local_certificate','certificate_img',$id,'/',$data->image_name,true);
        
        redirect(new moodle_url('/local/certificate/template-fields.php', array("template" =>$template_id)));
    }
}elseif($form->is_cancelled()){
    redirect(new moodle_url('/local/certificate/template-fields.php', array("template" =>$template_id)));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();

?>
    <script type="text/javascript" src="<?php echo $CFG->wwwroot;?>/theme/talentquest/layout/js/colorpicker.js"></script>
    <script>
        jQuery('.ColorPicker').each(function(e){
            var currentId = jQuery(this).attr('id');
            jQuery(this).ColorPicker({
                onShow: function (colpkr) {
                    jQuery(colpkr).fadeIn(500);
                    return false;
                },
                onHide: function (colpkr) {
                    jQuery(colpkr).fadeOut(500);
                    return false;
                },
                onChange: function (hsb, hex, rgb) {
                    jQuery('#'+currentId).val('#'+hex);
                },
                onBeforeShow: function () {
                    if (this.value.length){
                        jQuery(this).ColorPickerSetColor(this.value);
                    } else if(jQuery(this).attr('placeholder').length){
                        jQuery(this).ColorPickerSetColor(jQuery(this).attr('placeholder'));
                    }
                }
            });
        });
    </script>
    <script>
        function schow_text_input(){
            if($('#id_text_types').val() == 'custom_text'){
                $('#text_area').css({"display":"block"});
            }else{
                $('#text_area').css({"display":"none"});
            }
        }
        $(window).ready(function () {
            schow_text_input();
        });
    </script>
<?php
