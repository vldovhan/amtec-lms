<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('user_certificate_table.php');
require ('lib.php');

$user_id = optional_param('user_id',0,PARAM_INT);
$id = optional_param('id',0,PARAM_INT);
$action = optional_param('action','',PARAM_TEXT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:view', $systemcontext);

// check if enabled
certificate_enabled();

$title = get_string('user_certificates','local_certificate');

$PAGE->set_url(new moodle_url("/local/certificate/user-certificate.php", array()));
$PAGE->set_pagelayout('report');
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('user_certificates', 'local_certificate'), new moodle_url('/local/certificate/user-certificate.php'));
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$is_teacher = has_capability('local/certificate:manage', $systemcontext, $USER->id, false);
if (!$is_teacher)
    $user_id = $USER->id;
elseif($is_teacher && $action == 'delete' && $id)
    $DB->delete_records('local_certificate_issues',array('id'=>$id));


$table = new user_certificate_table('table',$user_id,$is_teacher);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

if (has_capability('local/certificate:view', $systemcontext)){
    $renderer = $PAGE->get_renderer('local_certificate');
    $renderer->certificate_print_tabs('issues');
}

if (has_capability('local/certificate:manage', $systemcontext)){
    echo html_writer::start_div('certificate-btn-box clearfix');
    echo html_writer::link(new moodle_url("/local/certificate/issue-certificate.php"), html_writer::tag('i', '', array('class'=>'fa fa-check')).get_string('issue_certificate', 'local_certificate'), array('class' => 'btn btn-warning'));
    echo html_writer::end_div();
}
$table->out(20, true);


echo $OUTPUT->footer();

