<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class certificates_table extends table_sql {

    function __construct($uniqueid) {
        global $CFG,$USER,$DB;

        parent::__construct($uniqueid);

        $columns = array('id','name','template','courses','plans','actions');
        $header = array(get_string('id','local_certificate'), get_string('name'),get_string('template','local_certificate'), get_string('courses','local_certificate'),get_string('plans','local_certificate'),get_string('actions','local_certificate'));

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "c.*, ct.name as template";
        $from = "{local_certificate} c
                    LEFT JOIN {local_certificate_template} ct ON ct.id=c.template";

        if(!has_capability('local/certificate:manage_all_certificates',context_system::instance())){
            $where = " c.userid=:userid ";

            $courses = enrol_get_my_courses();
            foreach($courses as $course){
                $where .= ' OR c.courses LIKE "%'.$course->id.'%" ';
            }

            $plans = $DB->get_records('local_plans_users',array('userid'=>$USER->id));
            foreach($plans as $plan){
                $where .= ' OR c.plans LIKE "%'.$plan->planid.'%" ';
            }

            $params = array('userid'=>$USER->id);
        }else{
            $where = " c.id>0 ";
            $params = array();
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }

    function col_courses($values) {
        global $OUTPUT, $PAGE, $DB;
        $courses = array_filter(explode(',',$values->courses),function($el){ return !empty($el);});
        if ($this->is_downloading()){
            $html_arr = array();
            foreach($courses as $id){
                $course = get_course($id);
                $html_arr[] = html_writer::tag('li',$course->fullname);
            }
            return implode(',', $html_arr);
        }
        $html = html_writer::start_tag('ul');
        foreach($courses as $id){
            $course = get_course($id);
            $html .= html_writer::tag('li',$course->fullname);
        }
        $html .= html_writer::end_tag('ul');

        return $html;
    }

    function col_plans($values) {
        global $OUTPUT, $PAGE, $DB;
        $plans = array_filter(explode(',',$values->plans),function($el){ return !empty($el);});
        if ($this->is_downloading()){
            $html_arr = array();
            foreach($plans as $id){
                $html_arr[] = $DB->get_record('local_plans',array('id'=>$id))->name;
            }
            return implode(',', $html_arr);
        }
        $html = html_writer::start_tag('ul');
        foreach($plans as $id){
            $html .= html_writer::tag('li',$DB->get_record('local_plans',array('id'=>$id))->name);
        }
        $html .= html_writer::end_tag('ul');

        return $html;
    }

    function col_actions($values) {
        global $OUTPUT, $PAGE;

        if ($this->is_downloading()){
            return '';
        }
        $edit = array();

        $aurl = new moodle_url('/local/certificate/certificates.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', get_string('delete'), 'core', array('class' => 'iconsmall')), null);

        $aurl = new moodle_url('/local/certificate/edit-certificate.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', get_string('edit'), 'core', array('class' => 'iconsmall')), null, array());

        return implode('', $edit);
    }

    public function get_sort_columns() {
        $data = parent::get_sort_columns();
        if(isset($data['template'])){
            $data['ct.name'] = $data['template'];
            unset($data['template']);
        }
        return $data;
    }
}
