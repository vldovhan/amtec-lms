<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('templates_table.php');
require ('lib.php');

$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

// check if enabled
certificate_enabled();

if ($action == 'delete' and $id) {
    $DB->delete_records('local_certificate_template',array('id'=>$id));
    $DB->delete_records('local_cert_template_field',array('template_id'=>$id));
}elseif ($action == 'copy' and $id) {
    $template = $DB->get_record('local_certificate_template',array('id'=>$id));
    unset($template->id);
    $template->is_system = 0;
    $template->timecreate = time();
    $template->name = $template->name.' '.get_string('copy');
    $template->id = $DB->insert_record('local_certificate_template',$template);

    $fields = $DB->get_records('local_cert_template_field',array('template_id'=>$id));
    foreach($fields as $field){
        $old_field_id = $field->id;
        unset($field->id);
        $field->template_id = $template->id;
        $field->timecreate = time();
        $field->id = $DB->insert_record('local_cert_template_field',$field);

        if($field->type == 'img'){
            $params = unserialize($field->value);
            $fs = get_file_storage();
            if($file = $fs->get_file(context_system::instance()->id, 'local_certificate', 'certificate_img', $old_field_id, '/', $params->image_name)){$table->is_collapsible = false;
                $file_record = array('contextid'=>context_system::instance()->id, 'component'=>'local_certificate', 'filearea'=>'certificate_img', 'itemid'=>$field->id,
                    'filepath'=>'/', 'filename'=>$params->image_name, 'userid'=>$USER->id);
                $fs->create_file_from_storedfile($file_record, $file);
            }
        }
    }
}


$title = get_string('templates','local_certificate');

$PAGE->set_url(new moodle_url("/local/certificate/templates.php", array()));
$PAGE->set_pagelayout('report');
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('templates', 'local_certificate'), new moodle_url('/local/certificate/templates.php'));
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new templates_table('table');
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_certificate');
$renderer->certificate_print_tabs('templates');

echo html_writer::start_div('certificate-btn-box clearfix');
echo html_writer::link(new moodle_url("/local/certificate/manage-template.php"),html_writer::tag('i', '', array('class'=>'fa fa-plus')).get_string('create_new_template', 'local_certificate'), array('class' => 'btn btn-warning'));
echo html_writer::end_div();
$table->out(20, true);

echo $OUTPUT->footer();

