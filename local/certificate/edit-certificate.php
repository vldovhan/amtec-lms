<?php
/**
 * @package   local_certificate
 * @copyright 2016 TalentQuest, talentquest.com
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require ('edit_certificate_form.php');
require ('lib.php');

$certificate = optional_param('id',0,PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/certificate:manage', $systemcontext);

// check if enabled
certificate_enabled();

if($certificate)
    $title = get_string('update_certificate','local_certificate');
else
    $title = get_string('create_certificate','local_certificate');

$PAGE->set_url(new moodle_url("/local/certificate/edit-certificate.php", array()));
$PAGE->navbar->add(get_string('certificates', 'local_certificate'),new moodle_url("/local/certificate/certificates.php", array()));
$PAGE->navbar->add($title);
$PAGE->requires->jquery();
$PAGE->requires->js('/theme/talentquest/javascript/jquery.multiple.select.js', true);
$PAGE->requires->css('/theme/talentquest/style/multiple-select.css', true);
$PAGE->set_pagelayout('admin');
$PAGE->set_context($systemcontext);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$form = new edit_certificate_form(null,array('certificate'=>$certificate));
if (!$form->is_cancelled() && $data = $form->get_data()) {
    if($record = $DB->get_record('local_certificate',array('id'=>$data->id))){
        $record->name = $data->name;
        $record->template = $data->template;
        $record->courses = implode(',',$data->courses);
        $record->plans = implode(',',$data->plans);
        $record->manual = $data->manual;
        $record->auto = $data->auto;
        $record->timemodified = time();
        $DB->update_record('local_certificate',$record);

        redirect(new moodle_url('/local/certificate/certificates.php'));
    }else{
        $record = new stdClass();
        $record->name = $data->name;
        $record->template = $data->template;
        $record->courses = implode(',',$data->courses);
        $record->plans = implode(',',$data->plans);
        $record->manual = $data->manual;
        $record->auto = $data->auto;
        $record->timemodified = 0;
        $record->timecreated = time();
        $record->userid = $USER->id;
        $DB->insert_record('local_certificate', $record);

        redirect(new moodle_url('/local/certificate/certificates.php'));
    }
}elseif($form->is_cancelled()){
    redirect(new moodle_url('/local/certificate/certificates.php'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
