<?php
require_once('curl.php');
$curl = new curl(array('debug'=>true));

$token = 'b9073f8ba3929d99940ed90fc824a94f';
$domainname = 'http://talentquest.sebale.net';

// Get users
$functionname = 'local_msync_get_users';
$params = array('criteria'=> array(
    array(
        'key'=>'id',
        'value'=>'3'
    )
));
// END


// Create list of users
$functionname = 'local_msync_create_users';

$user1 = new stdClass();
$user1->username = 'test1';
$user1->firstname = 'test1';
$user1->lastname = 'test1';
$user1->email = 'test1@moodle.com';
$user1->password = 'Moodle_2016';

$user2 = new stdClass();
$user2->username = 'test22';
$user2->firstname = 'test22';
$user2->lastname = 'test22';
$user2->email = 'test22@moodle.com';
$user2->password = 'Moodle_2016';

$users = array($user1, $user2);
$params = array('users' => $users);
$params = array('users' => $users);
//END


// Update list of users
$functionname = 'local_msync_update_users';

$user1 = new stdClass();
$user1->id = 16;
$user1->firstname = 'test111';
$user1->lastname = 'test111';

$user2 = new stdClass();
$user2->id = 17;
$user2->firstname = 'test222';
$user2->lastname = 'test222';

$users = array($user1, $user2);
$params = array('users' => $users);
//END

//Create cohorts
$functionname = 'local_msync_create_cohorts';

$cohort1 = new stdClass();
$cohort1->categorytype = array('type'=>'system', 'value'=>'1');
$cohort1->name = 'test1';
$cohort1->idnumber = 'test1';
$cohort1->description = 'test1';
$cohort1->descriptionformat =0;
$cohort1->visible = 1;

$cohort2 = new stdClass();
$cohort2->categorytype = array('type'=>'system', 'value'=>'1');
$cohort2->name = 'test2';
$cohort2->idnumber = 'test2';
$cohort2->description = 'test2';
$cohort2->descriptionformat = 0;
$cohort2->visible = 1;

$cohorts = array($cohort1, $cohort2);
$params = array('cohorts' => $cohorts);
//END


// Update cohort
$functionname = 'local_msync_update_cohorts';

$cohort1 = new stdClass();
$cohort1->id = 1;
$cohort1->categorytype = array('type'=>'system', 'value'=>'1');
$cohort1->name = 'test11';
$cohort1->idnumber = 'test11';
$cohort1->description = 'test11';
$cohort1->descriptionformat =0;
$cohort1->visible = 1;

$cohort2 = new stdClass();
$cohort2->id= 2;
$cohort2->categorytype = array('type'=>'system', 'value'=>'1');
$cohort2->name = 'test22';
$cohort2->idnumber = 'test22';
$cohort2->description = 'test22';
$cohort2->descriptionformat = 0;
$cohort2->visible = 1;

$cohorts = array($cohort1, $cohort2);
$params = array('cohorts' => $cohorts);
//END


// Get list of cohorts
$functionname = 'local_msync_get_cohorts';
$params = array('cohortids'=> array(1,2));
// END

// Add list of users to cohort
$functionname = 'local_msync_add_cohort_members';
$params = array('members'=> array(
        array(
            'cohorttype'=>array('type'=>'id', 'value'=>1),
            'usertype'=>array('type'=>'id', 'value'=>16)
        ),
        array(
            'cohorttype'=>array('type'=>'id', 'value'=>1),
            'usertype'=>array('type'=>'id', 'value'=>14)
        )
    ));
// END


// Remove list of users from cohort
$functionname = 'local_msync_delete_cohort_members';
$params = array('members'=> array(
        array('cohortid'=>1, 'userid'=>16)
    ));
// END


// Delete cohorts
$functionname = 'local_msync_delete_cohorts';
$params = array('cohortids'=> array(2));
// END


// Get list of users within cohort
$functionname = 'local_msync_get_cohort_members';
$params = array('cohortids'=> array(1));
// END


// Get list of courses
$functionname = 'local_msync_get_courses';
$params = array('options'=> array('ids'=>array(2,3,4)));
// END

// Get list of courses (SEARCH)
$functionname = 'local_msync_search_courses';
$params = array(
        'criterianame'=> 'search',
        'criteriavalue'=> 'course',
        'page'=> 1,
        'perpage'=> 10,
    );
// END

// Get all enrollments within instance
$functionname = 'local_msync_get_enrolled_users';
$params = array('courseid'=> 12);
// END



// Create a new enrollment
$functionname = 'local_msync_manual_enrol_users';
$params = array('enrolments'=> array(
        array(
            'roleid'=>5,
            'userid'=>16,
            'courseid'=>12,
            'timestart'=>0,
            'timeend'=>0,
            'suspend'=>0
        ),
    ));
// END

// Delete enrollment
$functionname = 'local_msync_manual_unenrol_users';
$params = array('enrolments'=> array(
        array(
            'roleid'=>5,
            'userid'=>16,
            'courseid'=>12
        ),
    ));
// END

// Get all enrollments for user
$functionname = 'local_msync_get_users_courses';
$params = array('userid'=> 2);
// END

// Get all enrollments
$functionname = 'local_msync_get_enrollements';
$params = array(
    'timestart'=> '03/22/2016',
    'timeend'=> '07/07/2016',
    'courseid'=>0,
    'userid'=>0
);
// END


$functionname = 'local_msync_create_cohorts';

$cohort1 = new stdClass();
$cohort1->categorytype = array('type'=>'system', 'value'=>'1');
$cohort1->name = 'test1111';
$cohort1->idnumber = 'test1111';
$cohort1->description = 'test1111';
$cohort1->descriptionformat =0;
$cohort1->visible = 1;

$cohort2 = new stdClass();
$cohort2->categorytype = array('type'=>'system', 'value'=>'1');
$cohort2->name = 'test23111';
$cohort2->idnumber = 'test2311';
$cohort2->description = 'test2311';
$cohort2->descriptionformat = 0;
$cohort2->visible = 1;

$cohorts = array($cohort1, $cohort2);
$params = array('cohorts' => $cohorts);



//$curl->debug = true;
$response = $curl->post($domainname .'/webservice/rest/server.php'. '?wstoken=' . $token . '&wsfunction=' . $functionname .'&moodlewsrestformat=json',
    $params);



print("Results: <hr>");
print("<pre>");
print_r($curl->info);
print_r(json_decode($response));

print("<hr>-");
//print_r($curl->info);

