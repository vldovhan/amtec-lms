<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Web service local plugin msync external functions and service definitions.
 *
 * @package    localmsync
 * @copyright  2016 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// We defined the web service functions to install.
$functions = array(
        'local_msync_get_enrollements' => array(
                'classname'   => 'local_msync_external',
                'methodname'  => 'get_enrollements',
                'classpath'   => 'local/msync/externallib.php',
                'description' => 'Return enrollements between [timestart] and [timeend]',
                'capabilities'=> 'enrol/manual:enrol',
                'type'        => 'write',
        ),
        'local_msync_create_users' => array(
                'classname' => 'core_user_external',
                'methodname' => 'create_users',
                'classpath' => 'user/externallib.php',
                'description' => 'Create users.',
                'type' => 'write',
        ),
        'local_msync_get_users' => array(
                'classname' => 'core_user_external',
                'methodname' => 'get_users',
                'classpath' => 'user/externallib.php',
                'description' => 'search for users matching the parameters',
                'type' => 'read',
                'capabilities' => 'moodle/user:viewdetails, moodle/user:viewhiddendetails, moodle/course:useremail, moodle/user:update'
        ),
        'local_msync_update_users' => array(
                'classname' => 'core_user_external',
                'methodname' => 'update_users',
                'classpath' => 'user/externallib.php',
                'description' => 'Update users.',
                'type' => 'write',
                'capabilities' => 'moodle/user:update',
        ),
        'local_msync_add_cohort_members' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'add_cohort_members',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Adds cohort members.',
                'type' => 'write',
                'capabilities' => 'moodle/cohort:assign'
        ),
        'local_msync_create_cohorts' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'create_cohorts',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Creates new cohorts.',
                'type' => 'write',
                'capabilities' => 'moodle/cohort:manage'
        ),
        'local_msync_delete_cohort_members' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'delete_cohort_members',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Deletes cohort members.',
                'type' => 'write',
                'capabilities' => 'moodle/cohort:assign'
        ),
        'local_msync_delete_cohorts' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'delete_cohorts',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Deletes all specified cohorts.',
                'type' => 'write',
                'capabilities' => 'moodle/cohort:manage'
        ),
        'local_msync_get_cohort_members' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'get_cohort_members',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Returns cohort members.',
                'type' => 'read',
                'capabilities' => 'moodle/cohort:view'
        ),
        'local_msync_get_cohorts' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'get_cohorts',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Returns cohort details.',
                'type' => 'read',
                'capabilities' => 'moodle/cohort:view'
        ),
        'local_msync_update_cohorts' => array(
                'classname' => 'core_cohort_external',
                'methodname' => 'update_cohorts',
                'classpath' => 'cohort/externallib.php',
                'description' => 'Updates existing cohorts.',
                'type' => 'write',
                'capabilities' => 'moodle/cohort:manage'
        ),
        'local_msync_get_courses' => array(
                'classname' => 'core_course_external',
                'methodname' => 'get_courses',
                'classpath' => 'course/externallib.php',
                'description' => 'Return course details',
                'type' => 'read',
                'capabilities' => 'moodle/course:view, moodle/course:update, moodle/course:viewhiddencourses',
                'ajax' => true,
                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
        ),
        'local_msync_search_courses' => array(
                'classname' => 'core_course_external',
                'methodname' => 'search_courses',
                'classpath' => 'course/externallib.php',
                'description' => 'Search courses by (name, module, block, tag)',
                'type' => 'read',
                'ajax' => true,
                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
        ),
        'local_msync_get_enrolled_users' => array(
                'classname' => 'core_enrol_external',
                'methodname' => 'get_enrolled_users',
                'classpath' => 'enrol/externallib.php',
                'description' => 'Get enrolled users by course id.',
                'type' => 'read',
                'capabilities' => 'moodle/user:viewdetails, moodle/user:viewhiddendetails, moodle/course:useremail, moodle/user:update, '
                    . 'moodle/site:accessallgroups',
                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
        ),
        'local_msync_get_users_courses' => array(
                'classname' => 'core_enrol_external',
                'methodname' => 'get_users_courses',
                'classpath' => 'enrol/externallib.php',
                'description' => 'Get the list of courses where a user is enrolled in',
                'type' => 'read',
                'capabilities' => 'moodle/course:viewparticipants',
                'services' => array(MOODLE_OFFICIAL_MOBILE_SERVICE),
        ),
        'local_msync_manual_enrol_users' => array(
                'classname'   => 'enrol_manual_external',
                'methodname'  => 'enrol_users',
                'classpath'   => 'enrol/manual/externallib.php',
                'description' => 'Manual enrol users',
                'capabilities'=> 'enrol/manual:enrol',
                'type'        => 'write',
        ),
        'local_msync_manual_unenrol_users' => array(
                'classname'   => 'enrol_manual_external',
                'methodname'  => 'unenrol_users',
                'classpath'   => 'enrol/manual/externallib.php',
                'description' => 'Manual unenrol users',
                'capabilities'=> 'enrol/manual:unenrol',
                'type'        => 'write',
        ),
);

// We define the services to install as pre-build services. A pre-build service is not editable by administrator.
$services = array(
        'Moodle Synchronization Service' => array(
                'functions' => array(
                        'local_msync_user_sync',
                        'local_msync_create_users',
                        'local_msync_update_users',
                        'local_msync_get_users',
                        'local_msync_add_cohort_members',
                        'local_msync_create_cohorts',
                        'local_msync_delete_cohort_members',
                        'local_msync_delete_cohorts',
                        'local_msync_get_cohort_members',
                        'local_msync_get_cohorts',
                        'local_msync_update_cohorts',
                        'local_msync_get_courses',
                        'local_msync_search_courses',
                        'local_msync_get_enrolled_users',
                        'local_msync_get_users_courses',
                        'local_msync_manual_enrol_users',
                        'local_msync_manual_unenrol_users',
                        'local_msync_get_enrollements',
                ),
                'restrictedusers' => 0,
                'enabled'=>1,
        )
);
