<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service msync plugin
 *
 * @package    localmsync
 * @copyright  2016 SEBALE
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir . "/externallib.php");

class local_msync_external extends external_api {

    /**
     * Returns description of get_enrollements() parameters
     *
     * @return external_function_parameters
     */
    public static function get_enrollements_parameters() {
        return new external_function_parameters(
            array(
                'timestart' => new external_value(PARAM_RAW, '', VALUE_OPTIONAL),
                'timeend' => new external_value(PARAM_RAW, '', VALUE_OPTIONAL),
                'userid' => new external_value(PARAM_INT, '', VALUE_OPTIONAL),
                'courseid' => new external_value(PARAM_INT, '', VALUE_OPTIONAL)
            )
        );
    }

    /**
     * Get list of active course enrolment methods for current user.
     *
     * @param int $courseid
     * @return array of course enrolment methods
     * @throws moodle_exception
     */
    public static function get_enrollements($timestart = '', $timeend = '', $courseid = 0, $userid = 0) {
        global $DB;

        $params = self::validate_parameters(self::get_enrollements_parameters(),
                array(
                    'timestart' => $timestart,
                    'timeend' => $timeend,
                    'courseid' => $courseid,
                    'userid' => $userid
                ));
        self::validate_context(context_system::instance());

        $sql = "";
        if($courseid){
            $sql .= " AND e.courseid = $courseid";
        }
        if($userid){
            $sql .= " AND ue.userid = $userid";
        }
        if(!empty($timestart)){
            $timestart = strtotime($timestart);
            $sql .= " AND ue.timecreated > $timestart";
        }
        if(!empty($timeend)){
            $timeend = strtotime($timeend);
            $sql .= " AND ue.timecreated < $timeend";
        }
        $data = $DB->get_records_sql("SELECT ue.id, e.courseid, ue.userid,  e.enrol, ue.timecreated
            FROM {user_enrolments} ue, {enrol} e
            WHERE e.id = ue.enrolid AND e.status = 0 AND ue.status = 0 $sql");

        return $data;
    }

    /**
     * Returns description of get_enrollements() result value
     *
     * @return external_description
     */
    public static function get_enrollements_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'id of enrolment'),
                    'courseid' => new external_value(PARAM_INT, 'id of course'),
                    'userid' => new external_value(PARAM_INT, 'id of user'),
                    'enrol' => new external_value(PARAM_RAW, 'enrol methods'),
                    'timecreated' => new external_value(PARAM_INT, 'timecreated')
                )
            )
        );
    }
}
