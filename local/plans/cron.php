<?php
function local_plans_cron()
{
	global $CFG, $DB, $SITE;

	require_once($CFG->dirroot . '/enrol/plans/locallib.php');

    $items = $DB->get_records_sql("SELECT p.id, p.name, p.startdate, p.enddate, p.expby, p.exptype, p.expnum, p.cronstamp FROM {local_plans} p WHERE p.expby > 0 AND p.type = 1 AND p.visible = 1");

    //print("<br>Data to prosess". count($items));
    foreach($items as $item){
    	//print("<br>Certification $item->name prossed!");
    	$users = array();
    	if($item->expby == 2){
    		if(date('mdY', $item->enddate) == date('mdY') ){
    			$users = $DB->get_records("local_plans_users", array('planid'=>$item->id));
    			//print("<br>Certification Success! RANGE cronstamp: ".date('m-d-Y').", enddate: ". date('m-d-Y', $item->enddate));
    		}else{
    			//print("<br>Certification failed! RANGE cronstamp: ".date('m-d-Y').", enddate: ". date('m-d-Y', $item->enddate));
    		}

    		$item->expby = 0;
    		$DB->update_record('local_plans', $item);
    	}elseif($item->expby == 1){
    		$users = $DB->get_records("local_plans_users", array('planid'=>$item->id));
    		foreach($users as $user){
 				if($item->exptype == 1){
 					$type = "week";
 				}elseif($item->exptype == 2){
 					$type = "month";
 				}elseif($item->exptype == 3){
 					$type = "year";
 				}else{
 					$type = "day";
 				}

 				$date = strtotime("+{$item->expnum} $type", $user->timecreated);
 				if(date('mdY', $date) == date('mdY')){
 					//print("<br>Certification Success!U:$user->userid, P:$user->planid R:  ".date('m-d-Y', $user->timecreated)." - ".date('m-d-Y',$date));

 				}else{
 					//print("<br>Certification failed! U:$user->userid, P:$user->planid R: ".date('m-d-Y', $user->timecreated)." - ".date('m-d-Y',$date));
 					unset($users[$user->id]);
 				}
    		}
    	}
    	if(!empty($users)){
    		foreach($users as $u){
    			if(date('mdY', $u->cronstamp) != date('mdY')){
    				$u->cronstamp = time();
    				$DB->update_record('local_plans_users', $u);

    				$properties=array(
						'objectid'=>$item->id,
						'userid'=>$u->userid,
						'relateduserid'=>$u->userid,
						'context'=>context_system::instance(),
						'courseid'=>$SITE->id
					);

					$event = \local_plans\event\local_plans_expired::create($properties);
					$event->trigger();

    				//UNENROL USER FROM PLAN AND ENROL AGAIN HERE!!!
					unenrol_fromplan($item->id, 0, $u->userid, true);
					$DB->delete_records('local_plans_completions', array('planid'=> $item->id, 'userid'=> $u->userid));

                    $plans = $DB->get_records("local_plans_certifications", array('certification'=>23));
                    foreach($plans as $pl){
                        $DB->delete_records('local_plans_completions', array('planid'=> $pl->planid, 'userid'=> $u->userid));
                    }

    				//print("<hr><pre>SUCCESS:");
    				//print_r($u);
    			}else{
    				//print("<hr><pre>DONE BEFORE:");
    				//print_r($u);
    			}
    		}
    	}
    }

    $plans = $DB->get_records_sql("SELECT pu.*, p.reexpnum, p.reexptype FROM {local_plans} p, {local_plans_users} pu where pu.cronstamp > 0 AND p.id = pu.planid and p.type = 1 AND p.reexpnum > 0");

    foreach ($plans as $p) {
        if($p->reexptype == 1){
            $type = "week";
        }elseif($p->reexptype == 2){
            $type = "month";
        }elseif($p->reexptype == 3){
            $type = "year";
        }else{
            $type = "day";
        }

        $date = strtotime("+{$p->reexpnum} $type", $p->cronstamp);
        if(date('mdY', $date) == date('mdY')){
            $d = new stdClass();
            $d->id = $p->id;
            $d->cronstamp = strtotime('-1 day');
            $DB->update_record('local_plans_users', $d);

            $instances = $DB->get_records_sql("SELECT ue.* FROM {user_enrolments} ue, {enrol} e WHERE e.id = ue.enrolid AND e.enrol = 'plans' AND e.customint1 = $p->id");
            foreach($instances as $instance){
                $instance->status = 1;
                $instance->timeend = time();
                $DB->update_record('user_enrolments', $instance);
                //print("<br>Plan $p->planid, USER - $instance->userid updated!");
            }
            //print("<br>Plan $p->planid completed!");
        }else{
            //print("<br>Plan $p->planid NOT completed!");
        }
    }
}
