<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class plan_courses_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB;

        $mform = $this->_form;
        $search = $this->_customdata['search'];
        $category = $this->_customdata['category'];
        $plan = $this->_customdata['data'];
        $courseid = $this->_customdata['courseid'];
        $options = array();
        $defaults = array();

        $sql = "";
        $sql .= ($search)?" AND c.fullname LIKE '%$search%'":"";
        $sql .= ($category)?" AND c.category=$category":"";

        $courses = $DB->get_records_sql("SELECT c.id, c.fullname FROM {course} c, {local_plans_courses} pc WHERE c.category > 0 AND c.visible = 1 AND c.id = pc.courseid AND pc.planid = $plan->id AND pc.courseid != $courseid $sql ORDER BY pc.sortorder");
        foreach($courses as $course){
            $options[$course->id] = $course->fullname;
        }
        $rs = $DB->get_record("local_plans_courses", array("planid"=> $plan->id, "courseid"=> $courseid));
        if($rs->restriction){
           $defaults = explode(",", $rs->restriction);
        }
        //print_r($defaults); exit;
        $mform->addElement('header', 'assignplans', get_string("availability", 'local_plans'), array('class'=>'collapsed'));

        $mform->addElement('select', 'courses', '', $options, array('multiple'=>'multiple', 'class'=>'multi-select'));
        $mform->setDefault('courses', $defaults);


        $mform->addElement('hidden', 'type');
        $mform->setType('type', PARAM_INT);

        $mform->addElement('hidden', 'courseid');
        $mform->setType('courseid', PARAM_INT);
        $mform->setDefault('courseid', $courseid);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out_as_local_url());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($plan);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

