<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require('edit_form.php');
require('locallib.php');
require_once($CFG->libdir.'/adminlib.php');

$id        = optional_param('id', 0, PARAM_INT);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$show      = optional_param('show', 0, PARAM_BOOL);
$hide      = optional_param('hide', 0, PARAM_BOOL);
$confirm   = optional_param('confirm', 0, PARAM_BOOL);
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$type = optional_param('type', 0, PARAM_INT);

require_login();
plans_enable($type);
$context = context_system::instance();
require_capability('local/plans:manage', $context);
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);

$PAGE->set_url('/local/plans/index.php', array('type'=>$type));

if ($id) {
    $plan = $DB->get_record('local_plans', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->url->param('id', $id);
    $plan->descriptionformat = 1;
} else {
    $plan = new stdClass();
    $plan->id = 0;
    $plan->type = $type;
    $plan->name = '';
    $plan->description = '';
    $plan->descriptionformat = 1;
}



if ($returnurl) {
    $returnurl = new moodle_url($returnurl);
} else {
    $returnurl = new moodle_url($CFG->wwwroot.'/local/plans/index.php', array('type'=>$type));
}


if ($delete and $plan->id) {
    $PAGE->url->param('delete', 1);
    if ($confirm and confirm_sesskey()) {
        delete_plan($plan->id);
        redirect($returnurl);
    }
    $strheading = get_string('criteria_3_'.$type, 'local_plans');
    $PAGE->navbar->add(get_string('criteria_0_'.$type, 'local_plans'), new moodle_url('/local/plans/index.php', array('type'=>$type)));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/plans/edit.php', array('type'=>$type, 'id' => $plan->id, 'delete' => 1,
        'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => $returnurl->out()));
    $message = get_string('criteria_4_'.$type, 'local_plans', format_string($plan->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
}

if ($show && $plan->id && confirm_sesskey()) {
    if (!$plan->visible) {
        $record = (object)array('id' => $plan->id, 'visible' => 1);
        save_plan($record);
    }
    redirect($returnurl);
}

if ($hide && $plan->id && confirm_sesskey()) {
    if ($plan->visible) {
        $record = (object)array('id' => $plan->id, 'visible' => 0);
        save_plan($record);
    }
    redirect($returnurl);
}

$editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES,
    'maxbytes' => $SITE->maxbytes, 'context' => $context, 'descriptionformat' => 1);
if ($plan->id) {
    // Edit existing.
    $plan = file_prepare_standard_editor($plan, 'description', $editoroptions,
            $context, 'plan', 'description', $plan->id);
    $strheading = get_string('criteria_2_'.$type, 'local_plans');

} else {
    // Add new.
    $plan = file_prepare_standard_editor($plan, 'description', $editoroptions,
            $context, 'local_plans', 'description', null);
    $strheading = get_string('criteria_1_'.$type, 'local_plans');
}

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes);
$plan = file_prepare_standard_filemanager($plan, 'image', $attachmentoptions, $context, 'local_plans', 'image', (($plan->id)?$plan->id:null));
$editform = new plan_edit_form(null, array('editoroptions'=>$editoroptions, 'data'=>$plan, 'returnurl'=>$returnurl));

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    $data->id = save_plan($data);

    if($data->id){
        $data = file_postupdate_standard_editor($data, 'description', $editoroptions,
                $context, 'local_plans', 'description', $data->id);
        $data = file_postupdate_standard_filemanager($data,
            'image', $attachmentoptions, $context, 'local_plans', 'image', $data->id);

        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'local_plans', 'image', $data->id);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if(!empty($filename) and $filename != '.'){
                $data->image = $filename;
            }
        }
        save_plan($data);
    }
    redirect($returnurl);
}

$PAGE->navbar->add(get_string('criteria_0_'.$type, 'local_plans'), new moodle_url('/local/plans/index.php', array('type'=>$type)));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);
$PAGE->requires->css('/local/plans/style.css');


echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);

echo html_writer::start_tag('div', array('class'=>'plans-edit-form'));
echo $editform->display();
echo html_writer::end_tag('div');
echo $OUTPUT->footer();

