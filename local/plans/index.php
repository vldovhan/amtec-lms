<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

require($CFG->dirroot.'/local/plans/locallib.php');
require_once($CFG->libdir.'/adminlib.php');

$type = optional_param('type', 0, PARAM_INT);
$view = optional_param('view', 1, PARAM_INT);
$download = optional_param('download', '', PARAM_ALPHA);

require_login();
plans_enable($type);


$strplans = get_string('criteria_0_'.$type, 'local_plans');
$context = context_system::instance();
$manager = has_capability('local/plans:manage', $context);
$canassign = has_capability('local/plans:assign', $context);

require_capability('local/plans:view', $context);

$title = get_string('criteria_0_'.$type, 'local_plans');
$PAGE->set_url('/local/plans/index.php', array('view'=>$view, 'type'=>$type));
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new plans_table('table', $view, $type);
$table->show_download_buttons_at(array());
$table->is_downloading($download, get_string('courses'), get_string('courses'));
$table->is_collapsible = false;

if (!$table->is_downloading()) {
  	echo $OUTPUT->header();
	echo $OUTPUT->heading($title);
    
    echo html_writer::start_tag('div', array('class'=>'clearfix'));
    
    echo html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/edit.php', array('type'=>$type)), html_writer::tag('i', '', array('class'=>'fa fa-plus')).get_string('criteria_1_'.$type, 'local_plans'), array('class'=>'btn btn-warning', 'style'=>'float:right; margin-left:5px;'));
    
	echo html_writer::start_tag("form",  array("action"=>$PAGE->url, 'style'=>'float:right'));
		echo html_writer::start_tag('select', array('name'=>'view', 'onchange'=>'this.form.submit()'));
		$options = array(2=>get_string('show_all'), 1=>get_string('active'), 0=>get_string('inactive'));
		foreach ($options as $key => $value) {
			$params = array('value'=>$key);
			if($view == $key){
				$params['selected'] = 'selected';
			}
			echo html_writer::tag('option',$value, $params);
		}
		echo html_writer::end_tag('select');
	echo html_writer::end_tag("form");
                                
    echo html_writer::end_tag("div");
}

$table->out(20, true);

if (!$table->is_downloading()) {
    echo $OUTPUT->footer();
}
