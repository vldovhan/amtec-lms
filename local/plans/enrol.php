<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cohort related management functions, this file needs to be included manually.
 *
 * @package    core_cohort
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('locallib.php');
require('enrol_form.php');
require_once($CFG->libdir.'/adminlib.php');

$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$type = optional_param('type', 0, PARAM_INT);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$userid = optional_param('userid', 0, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);


require_login();
plans_enable($type);
$context = context_system::instance();
require_capability('local/plans:enrol', $context);
$PAGE->set_url('/local/plans/enrol.php', array('id'=>$id, 'type'=>$type));
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);
$PAGE->requires->jquery();
$PAGE->requires->js('/theme/talentquest/javascript/jquery.multiple.select.js', true);
$PAGE->requires->css('/theme/talentquest/style/multiple-select.css', true);
$strheading = get_string("criteria_8_$type", 'local_plans');
$PAGE->navbar->add(get_string('criteria_0_'.$type, 'local_plans'), new moodle_url('/local/plans/index.php', array('type'=>$type)));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);


$plan = $DB->get_record('local_plans', array('id'=>$id));
if (!$plan->id) {
    redirect($returnurl);
}
$PAGE->url->param('id', $plan->id);
if ($returnurl) {
    $returnurl = new moodle_url($returnurl);
} else {
    $returnurl = new moodle_url($CFG->wwwroot.'/local/plans/index.php', array('type'=>$type));
}

if (optional_param('cancel', false, PARAM_BOOL)) {
    redirect($returnurl);
}

$editform = new plan_enrol_form(null, array('data'=>$plan, 'search'=> $search, 'returnurl'=>$returnurl));
if ($delete and $plan->id and $userid) {
    delete_plan_users($userid, $plan->id);
    redirect($PAGE->url);
}

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    save_plans_enrols($data);
    redirect($returnurl);
}

$table = new plans_users_table('table', $plan);
$table->show_download_buttons_at(array());
$table->is_downloading('', get_string('courses'), get_string('courses'));
$table->is_collapsible = false;


echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);

echo $editform->display();

$table->out(20, true);


echo $OUTPUT->footer();

