<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->libdir. '/coursecatlib.php');

class plan_edit_form extends moodleform {

    /**
     * Define the plan edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $editoroptions = $this->_customdata['editoroptions'];
        $plan = $this->_customdata['data'];

        $mform->addElement('text', 'name', get_string('name'), 'maxlength="254" size="50"');
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);

        // Verify permissions to change course category or keep current.
        if ($plan->type){

            $mform->addElement('static', 's',' ');

            $mform->addElement('hidden', 'category', 0);
            $mform->setType('hidden', PARAM_INT);
        } else {
            if (empty($plan->id)) {
                $displaylist = coursecat::make_categories_list('moodle/course:create');
                $displaylist[0] = '';
                $mform->addElement('select', 'category', get_string('coursecategory'), $displaylist);
            } else {
                $displaylist = coursecat::make_categories_list('moodle/course:create');
                if (!isset($displaylist[$plan->category])) {
                    //always keep current
                    $displaylist[$plan->category] = coursecat::get($plan->category, MUST_EXIST, true)->get_formatted_name();
                }
                $displaylist[0] = '';
                $mform->addElement('select', 'category', get_string('category', 'local_plans'), $displaylist);
            }
        }

        $mform->addElement('advcheckbox', 'visible', get_string('active'));
        $mform->setDefault('visible', 1);

        $mform->addElement('advcheckbox', 'selfenrollment', get_string('enable_selfenrollment', 'local_plans'));
        $mform->setDefault('selfenrollment', 1);

        $attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes);
        $mform->addElement('filemanager', 'image_filemanager', 'Image', null, $attachmentoptions);

        $mform->addElement('editor', 'description_editor', get_string('description'), null, $editoroptions);
        $mform->setType('description_editor', PARAM_RAW);



        if($plan->type){
            $mform->addElement('header', 'exptime', get_string("timeoptions", 'local_plans'));

            $mform->addElement('advcheckbox', 'expby', get_string('enable'));
            $mform->setType('expby', PARAM_TEXT);
            $mform->setDefault('expby', 1);

            $mform->addElement('text', 'expnum', get_string('number', 'local_plans'));
            $mform->setType('expnum', PARAM_TEXT);
            $mform->disabledIf('expnum', 'expby', 'eq', 0);

            $options = array(0=>'Days', 1=>'Weeks', 2=>'Months', 3=>'Years');
            $mform->addElement('select', 'exptype', get_string('type', 'local_plans'), $options);
            $mform->setType('exptype', PARAM_TEXT);
            $mform->disabledIf('exptype', 'expby', 'eq', 0);

            $mform->addElement('header', 'exptime', get_string("criteria_15_2", 'local_plans'));

            $mform->addElement('advcheckbox', 'reby', get_string('enable'));
            $mform->setType('reby', PARAM_TEXT);
            $mform->setDefault('reby', 1);

            $mform->addElement('advcheckbox', 'redatetype', get_string('redatetype', 'local_plans'));
            $mform->setType('redatetype', PARAM_TEXT);
            $mform->setDefault('redatetype', 1);
            $mform->disabledIf('redatetype', 'reby', 'eq', 0);

            $mform->addElement('header', 'exptime', get_string("timeoptions2", 'local_plans'));

            $mform->addElement('text', 'reexpnum', get_string('renumber', 'local_plans'));
            $mform->setType('reexpnum', PARAM_TEXT);
            $mform->disabledIf('reexpnum', 'reby', 'eq', 0);
            $mform->disabledIf('reexpnum', 'redatetype', 'eq', 1);

            $options = array(0=>'Days', 1=>'Weeks', 2=>'Months', 3=>'Years');
            $mform->addElement('select', 'reexptype', get_string('retype', 'local_plans'), $options);
            $mform->setType('reexptype', PARAM_TEXT);
            $mform->disabledIf('reexptype', 'reby', 'eq', 0);
            $mform->disabledIf('reexptype', 'redatetype', 'eq', 1);

        }else{
            $mform->addElement('date_time_selector', 'startdate', get_string("criteria_10_$plan->type", 'local_plans'), array('optional' => true));
            $mform->addElement('date_time_selector', 'enddate', get_string("criteria_11_$plan->type", 'local_plans'), array('optional' => true));
        }

        $mform->addElement('text', 'idnumber', get_string("idnumber_$plan->type", 'local_plans'));
        $mform->setType('idnumber', PARAM_TEXT);

        if($plan->type){
            $mform->addElement('header', 'exptime', get_string("criteria_15_3", 'local_plans'));
            $mform->addElement('date_time_selector', 'startdate', get_string("criteria_15_0", 'local_plans'), array('optional' => true));
             $mform->disabledIf('startdate', 'reby', 'eq', 0);
             $mform->disabledIf('startdate', 'redatetype', 'eq', 0);
            $mform->addElement('date_time_selector', 'enddate', get_string("criteria_15_1", 'local_plans'), array('optional' => true));
             $mform->disabledIf('enddate', 'reby', 'eq', 0);
             $mform->disabledIf('enddate', 'redatetype', 'eq', 0);
        }

        $mform->addElement('hidden', 'type');
        $mform->setType('type', PARAM_INT);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($plan);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

