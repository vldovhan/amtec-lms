<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');

require($CFG->dirroot.'/local/plans/locallib.php');
require_once($CFG->libdir.'/adminlib.php');

$type = optional_param('type', 0, PARAM_INT);
$view = optional_param('view', 2, PARAM_INT);
$download = optional_param('download', '', PARAM_ALPHA);

error_reporting(E_ALL);
ini_set('display_errors', '1');

$courseid = 44;
$userid = 82;


//require_once('cron.php');
//local_plans_cron();
//exit;

require_login();
error_reporting(E_ALL);
ini_set('display_errors', '1');


$courseid = 44;
$userid = 16;



$plans = $DB->get_records_sql("SELECT pu.*, p.reexpnum, p.reexptype FROM mdl_local_plans p, mdl_local_plans_users pu where pu.cronstamp > 0 AND p.id = pu.planid and p.type = 1 AND p.reexpnum > 0");

foreach ($plans as $p) {
	if($p->reexptype == 1){
		$type = "week";
	}elseif($p->reexptype == 2){
		$type = "month";
	}elseif($p->reexptype == 3){
		$type = "year";
	}else{
		$type = "day";
	}

	$date = strtotime("+{$p->reexpnum} $type", $p->cronstamp);
	if(date('mdY', $date) == date('mdY')){
		$d = new stdClass();
        $d->id = $p->id;
        $d->cronstamp = strtotime('-1 day');
        $DB->update_record('local_plans_users', $d);

		$instances = $DB->get_records_sql("SELECT ue.* FROM mdl_user_enrolments ue, mdl_enrol e WHERE e.id = ue.enrolid AND e.enrol = 'plans' AND e.customint1 = $p->id");
		foreach($instances as $instance){
			$instance->status = 1;
			$instance->timeend = time();
			$DB->update_record('user_enrolments', $instance);
			//print("<br>Plan $p->planid, USER - $instance->userid updated!");
		}
		//print("<br>Plan $p->planid completed!");
	}else{
		//print("<br>Plan $p->planid NOT completed!");
	}
}


print('<pre>-');
print_r($plans);

exit;






$plans = $DB->get_records_sql("SELECT pu.planid FROM {local_plans} p, {local_plans_users} pu WHERE p.type = 0 AND p.id = pu.planid AND pu.userid = $userid AND pu.planid IN (SELECT planid as id FROM {local_plans_courses} WHERE courseid=$courseid)");


foreach ($plans as $p) {
	$courses = $DB->get_records_sql("SELECT
			pc.id, pc.courseid, cc.timecompleted
			FROM {local_plans_courses} pc
				LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.userid = $userid WHERE pc.planid = $p->planid");

	$isComplete = true;
	foreach($courses as $c){
		if(!$c->timecompleted){
			$isComplete = false;
		}
	}
	if($isComplete){
		if($d = $DB->get_record('local_plans_completions', array('planid'=>$p->planid, 'userid'=>$userid))){
			//$d->timecreated = time();
	        //$DB->update_record('local_plans_completions', $d);
		}else{
			$d = new stdClass();
	        $d->planid = $p->planid;
	        $d->userid = $userid;
	        $d->timecreated = time();
	        $DB->insert_record('local_plans_completions', $d);
		}
		print("<br>Plan $p->planid completed!");
	}else{
		$DB->delete_records('local_plans_completions', array('planid' => $p->planid, 'userid'=>$userid));
		print("<br>Plan $p->planid NOT completed!");
	}
}



$certifications = $DB->get_records_sql("SELECT pc.certification FROM {local_plans_certifications} pc, {local_plans_users} pu WHERE pu.userid = $userid AND pu.planid = pc.certification");

foreach ($certifications as $c) {
	$plans = $DB->get_records_sql("SELECT
			pc.planid, cc.timecreated
			FROM {local_plans_certifications} pc
				LEFT JOIN {local_plans_completions} cc ON cc.planid = pc.planid AND cc.userid = $userid AND cc.timecreated > 0
					WHERE pc.certification = $c->certification");

	$isComplete = true;
	foreach ($plans as $plan){
		if(!$plan->timecreated){
			$isComplete = false;
		}
	}
	if($isComplete){
		$courses = $DB->get_records_sql("SELECT
			pc.id, pc.courseid, cc.timecompleted
			FROM {local_plans_courses} pc
				LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.userid = $userid WHERE pc.planid = $c->certification");

		$isComplete = true;
		foreach($courses as $course){
			if(!$course->timecompleted){
				$isComplete = false;
			}
		}
		if($isComplete){
			if($d = $DB->get_record('local_plans_completions', array('planid'=>$c->certification, 'userid'=>$userid))){
				//$d->timecreated = time();
		        //$DB->update_record('local_plans_completions', $d);
			}else{
				$d = new stdClass();
		        $d->planid = $c->certification;
		        $d->userid = $userid;
		        $d->timecreated = time();
		        $DB->insert_record('local_plans_completions', $d);
			}
			print("<br>Certification $c->certification completed!");
		}else{
			$DB->delete_records('local_plans_completions', array('planid'=>$c->certification, 'userid'=>$userid));
			print("<br>Certification ($c->certification) COURSES  NOT completed!");
		}
	}else{
		print("<br>Certification ($c->certification) PLANS NOT completed!");
	}
}

print('<pre>-');
print_r($plans);
print_r($certifications);

exit;
