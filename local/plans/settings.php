<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage plans
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_plans', get_string('pluginname', 'local_plans'));
$ADMIN->add('localplugins', $settings);

if ($ADMIN->locate('localplugins') and $ADMIN->locate('root')){
  if(get_config('local_plans', 'enabled_plans')){
    $ADMIN->add('root', new admin_category('plans', get_string('criteria_0_0', 'local_plans')));

    $ADMIN->add('plans', new admin_externalpage('plans1', get_string('criteria_0_0', 'local_plans'),
            $CFG->wwwroot.'/local/plans/index.php', 'local/plans:view'));

    $ADMIN->add('plans', new admin_externalpage('plans2', get_string('criteria_6_0', 'local_plans'),
            $CFG->wwwroot.'/local/plans/edit.php', 'local/plans:assign'));

    $ADMIN->add('plans', new admin_externalpage('plans3', get_string('assigncourses', 'local_plans'),
            $CFG->wwwroot.'/local/plans/assign.php', 'local/plans:manage'));

    $ADMIN->add('plans', new admin_externalpage('plans4', get_string('enrolusers', 'local_plans'),
            $CFG->wwwroot.'/local/plans/enrol.php', 'local/plans:enrol'));
  }


  if(get_config('local_plans', 'enabled_certifications')){
    $ADMIN->add('root', new admin_category('certifications', get_string('criteria_0_1', 'local_plans')));

    $ADMIN->add('certifications', new admin_externalpage('certification1', get_string('criteria_0_1', 'local_plans'),
            $CFG->wwwroot.'/local/plans/index.php?type=1', 'local/plans:view'));

    $ADMIN->add('certifications', new admin_externalpage('certification2', get_string('criteria_6_1', 'local_plans'),
            $CFG->wwwroot.'/local/plans/edit.php?type=1', 'local/plans:assign'));

    $ADMIN->add('certifications', new admin_externalpage('certification5', get_string('enrolplans', 'local_plans'),
            $CFG->wwwroot.'/local/plans/plans.php', 'local/plans:assign'));

    $ADMIN->add('certifications', new admin_externalpage('certification3', get_string('assigncourses', 'local_plans'),
            $CFG->wwwroot.'/local/plans/assign.php?type=1', 'local/plans:manage'));

    $ADMIN->add('certifications', new admin_externalpage('certification4', get_string('enrolusers', 'local_plans'),
            $CFG->wwwroot.'/local/plans/enrol.php?type=1', 'local/plans:enrol'));
  }
}

$name = 'local_plans/enabled_plans';
$title = get_string('enabled_plans', 'local_plans');
$description = get_string('enabled_plans_desc', 'local_plans');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$settings->add($setting);

$name = 'local_plans/enabled_certifications';
$title = get_string('enabled_certifications', 'local_plans');
$description = get_string('enabled_certifications_desc', 'local_plans');
$setting = new admin_setting_configcheckbox($name, $title, $description, true, true, false);
$settings->add($setting);
