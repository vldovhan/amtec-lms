<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require('../../config.php');
require_once('locallib.php');

$id = required_param('id', PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);

require_login();
$context = context_system::instance();
require_capability('local/plans:view', $context);

$plan = $DB->get_record('local_plans', array('id'=>$id));
$title = $plan->name;
$PAGE->set_url(new moodle_url("/local/plans/view.php", array('id'=>$plan->id)));
$PAGE->navbar->add(get_string('criteria_0_'.$plan->type, 'local_plans'));
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('report');
$PAGE->set_context(context_system::instance());
$PAGE->set_title($title);
$PAGE->set_heading($title);

if ($action == 'enroll' and confirm_sesskey()){
    self_plan_enrol($plan);
    redirect(new moodle_url("/local/plans/view.php", array('id'=>$plan->id)));
}

$courses = $DB->get_records_sql("SELECT c.*, cc.name as categoryname, cc.color  
                                  FROM {local_plans_courses} as pc
                                    LEFT JOIN {course} as c ON c.id=pc.courseid
                                    LEFT JOIN {course_categories} cc ON cc.id = c.category
                                  WHERE pc.planid=:id",array('id'=>$id));

$enrolled = $DB->get_record_sql("SELECT e.id FROM {enrol} e JOIN {user_enrolments} ue ON ue.enrolid = e.id WHERE ue.userid = $USER->id AND e.enrol = 'plans' AND e.customint1 = $plan->id");
$is_enrolled = (!$enrolled) ? false : true;

$coursecatalogrenderer = $PAGE->get_renderer('block_course_catalog');
$user_view = get_user_preferences('catalog-view-type');

echo $OUTPUT->header();

echo html_writer::start_tag('div', array('class'=>'course-content plan-page'.(($is_enrolled) ? ' enrolled' : '')));

echo html_writer::start_tag('div', array('class'=>'course-title-box'));
$image_url = get_plan_image_url($plan);
echo html_writer::tag('div', '', array('class'=>'course-image', 'style'=>'background-image: url("'.$image_url.'");'));
echo html_writer::start_tag('div', array('class'=>'course-title'));
echo $plan->name;
echo html_writer::end_tag('div');
echo html_writer::end_tag('div');

echo plan_print_rating($plan, $is_enrolled);

if($is_enrolled){
    $completion = $DB->get_record('local_plans_completions', array('userid'=>$USER->id, 'planid'=>$plan->id));
    $progress = (isset($completion->timecreated)) ? 100 : get_plan_progress($plan);
    $progress_info = html_writer::tag('label', get_string('planprogress', 'local_plans').':') .
        html_writer::tag('div', 
        html_writer::start_tag('div', array('class' => 'progres-bar')).
        html_writer::tag('div', html_writer::tag('span', intval($progress).'%',array('class' => 'procent')), array('class' => 'progres','style'=>'width:'.$progress.'%;')).
        html_writer::end_tag('div'),
            array('class'=>'clearfix progress-wrapper'));
    echo html_writer::tag('div', $progress_info, array('class' => 'course-progress'));
} elseif($plan->selfenrollment) {
    echo html_writer::tag('div', get_string('startplan', 'local_plans'), array('class' => 'enroll-button btn', 'onclick'=>'location="'.$CFG->wwwroot.'/local/plans/view.php?id='.$plan->id.'&action=enroll&sesskey='.sesskey().'"' ));
}

if (!empty($plan->startdate) or !empty($plan->enddate)){
    echo html_writer::start_tag('div', array('class'=>'course-dates clearfix'));
        if (!empty($plan->startdate)){
            echo html_writer::start_tag('div', array('class'=>'course-date-item'));
                echo html_writer::tag('label', get_string('startdate', 'format_talentquest').':');
                echo html_writer::tag('span', date('m/d/Y', $plan->startdate), array('class'=>'course-date'));
            echo html_writer::end_tag('div');
        }
        if (!empty($plan->enddate)){
            echo html_writer::start_tag('div', array('class'=>'course-date-item'));
                echo html_writer::tag('label', get_string('enddate', 'format_talentquest').':');
                echo html_writer::tag('span', date('m/d/Y', $plan->enddate), array('class'=>'course-date'));
            echo html_writer::end_tag('div');
        }
    echo html_writer::end_tag('div');
}


echo html_writer::start_tag('div', array('class'=>'nav-tabs-header'));
    echo html_writer::start_tag('ul', array('class'=>'nav-tabs nav-tabs-simple clearfix'));
        echo html_writer::tag('li', '<a data-toggle="tab" href="#courses">'.get_string('courses', 'local_plans').'</a>', array('class'=>'header-courses active'));
        
        if (!empty($plan->description)){
            echo html_writer::tag('li', '<a data-toggle="tab" href="#plan-description">'.get_string('plandescription', 'local_plans').'</a>', array('class'=>'header-plan-description'));
        }

        echo html_writer::start_tag('li', array('class'=>'view-toggler'));
            echo html_writer::start_tag('span', array('class' => 'action-view', 'onclick'=>'toggleView();'));
            echo html_writer::tag('i', '', array('class'=>'fa fa-th-list'.(($user_view != 'list') ? ' active' : ''), 'title'=>get_string('listview', 'block_course_catalog')));
            echo html_writer::tag('i', '', array('class'=>'fa fa-th-large'.(($user_view == 'list') ? ' active' : ''), 'title'=>get_string('gridview', 'block_course_catalog')));
            echo html_writer::end_tag('span');
        echo html_writer::end_tag('li');

    echo html_writer::end_tag('ul');
echo html_writer::end_tag('div');

echo html_writer::start_tag('div', array('class'=>'tab-content list-view'));

    echo html_writer::start_tag('div', array('id'=>'courses', 'class'=>'tab-pane active'));
        echo html_writer::start_tag('div', array('class'=>'course-curriculum-box'));
        
        echo html_writer::start_div('frontpage-courses-list');
        echo html_writer::start_div('courses');

        if(count($courses)){
            foreach($courses as $course){
                $course->context = context_course::instance($course->id);
                $course->coursetype = (is_enrolled($course->context,$USER->id,'',true))?'enrolled':'';
                echo $coursecatalogrenderer->catalog_print_course($course);
            }
        }else{
            echo html_writer::tag('div', get_string('nocourses', 'local_plans'), array('class'=>'alert alert-block alert-success'));
        }

        echo html_writer::end_div();
        echo html_writer::end_div();
        
        echo html_writer::end_tag('div');
    echo html_writer::end_tag('div');
    
    if (!empty($plan->description)){
        echo html_writer::start_tag('div', array('id'=>'plan-description', 'class'=>'tab-pane'));
            echo $plan->description;
        echo html_writer::end_tag('div');
    }

echo html_writer::end_tag('div');
echo '<script>
    function toggleView(){
        if (jQuery(".view-toggler .fa-th-list").hasClass("active")){
            jQuery(".tab-content").addClass("list-view");
            jQuery(".tab-content").removeClass("grid-view");
        } else {
            jQuery(".tab-content").removeClass("list-view");
            jQuery(".tab-content").addClass("grid-view");
        }
        
        jQuery(".view-toggler i").toggleClass("active");
    }
</script>';

// end box
echo html_writer::end_tag('div');

echo $OUTPUT->footer();
