<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');

class plan_enrol_form extends moodleform {

    /**
     * Define the cohort edit form
     */
    public function definition() {
        global $DB,$CFG, $PAGE;

        $mform = $this->_form;

        $plan = $this->_customdata['data'];
        $search = $this->_customdata['search'];


        $sql = ($search)?" AND (firstname LIKE '%$search%' OR lastname LIKE '%$search%' OR email LIKE '%$search%')":"";
        $options = array();
        $users = $DB->get_records_sql("SELECT id, firstname, lastname, email FROM {user} WHERE id > 1 and deleted = 0 AND confirmed = 1 AND id NOT IN (SELECT DISTINCT ue.userid FROM {enrol} e, {user_enrolments} ue WHERE
            e.customint1 = $plan->id AND e.enrol='plans' AND ue.enrolid = e.id) $sql ORDER BY firstname ASC");
        foreach($users as $user){
            $options[$user->id] = "$user->firstname $user->lastname";
        }

        $mform->addElement('select', 'users', get_string("criteria_8_$plan->type", 'local_plans'), $options, array('multiple'=>'multiple', 'class'=>'multi-select'));

        $options = get_default_enrol_roles(context_system::instance());
        $student = get_archetype_roles('student');
        $student = reset($student);
        $mform->addElement('select', 'defaultrole', get_string('defaultrole', 'role'), $options);
        $mform->setDefault('defaultrole', array($student->id));


        $options = array(ENROL_USER_ACTIVE    => get_string('participationactive', 'enrol'),
                         ENROL_USER_SUSPENDED => get_string('participationsuspended', 'enrol'));

        $mform->addElement('select', 'status', get_string('participationstatus', 'enrol'), $options);


        $mform->addElement('date_time_selector', 'timestart', get_string('enroltimestart', 'enrol'), array('optional' => true));

        $mform->addElement('date_time_selector', 'timeend', get_string('enroltimeend', 'enrol'), array('optional' => true));


        $mform->addElement('hidden', 'type');
        $mform->setType('type', PARAM_INT);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        if (isset($this->_customdata['returnurl'])) {
            $mform->addElement('hidden', 'returnurl', $this->_customdata['returnurl']->out_as_local_url());
            $mform->setType('returnurl', PARAM_LOCALURL);
        }

        $this->add_action_buttons();

        $this->set_data($plan);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

