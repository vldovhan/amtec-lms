<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require('../../config.php');
require_once('locallib.php');
require('assign_form.php');
require_once($CFG->libdir.'/adminlib.php');

$id = required_param('id', PARAM_INT);
$returnurl = optional_param('returnurl', '', PARAM_LOCALURL);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$courseid  = optional_param('courseid', 0, PARAM_INT);
$type = optional_param('type', 0, PARAM_INT);
$sort = optional_param('sort', 'up', PARAM_RAW);
$search = optional_param('search', '', PARAM_RAW);
$category = optional_param('category', 0, PARAM_INT);

require_login();
$context = context_system::instance();
require_capability('local/plans:assign', $context);

$strheading = get_string("criteria_7_$type", 'local_plans');
$PAGE->set_url('/local/plans/assign.php', array('id'=>$id, 'type'=>$type));
$PAGE->set_pagelayout('report');
$PAGE->set_context($context);
$PAGE->requires->jquery();
$PAGE->requires->js('/theme/talentquest/javascript/jquery.multiple.select.js', true);
$PAGE->requires->css('/theme/talentquest/style/multiple-select.css', true);
$PAGE->navbar->add(get_string('criteria_0_'.$type, 'local_plans'), new moodle_url('/local/plans/index.php', array('type'=>$type)));
$PAGE->navbar->add($strheading);
$PAGE->set_title($strheading);
$PAGE->set_heading($strheading);


$plan = $DB->get_record('local_plans', array('id'=>$id));
if (!$plan->id) {
    redirect($returnurl);
}


if ($returnurl) {
    $returnurl = new moodle_url($returnurl);
} else {
    $returnurl = new moodle_url($CFG->wwwroot.'/local/plans/index.php', array('type'=>$type));
}

if (optional_param('cancel', false, PARAM_BOOL)) {
    redirect($returnurl);
}

$editform = new plan_assign_form(null, array('data'=>$plan, 'search'=> $search, 'category'=> $category, 'returnurl'=>$returnurl));

if ($sort and $plan->id and $courseid) {
	sort_plan_courses($plan->id, $courseid, $sort);
}
if ($delete and $plan->id and $courseid) {
    delete_plan_course($plan->id, $courseid);
    redirect($PAGE->url);
}
if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    save_plans_courses($data);
    redirect($returnurl);
}

$table = new plans_courses_table('table', $plan);
$table->show_download_buttons_at(array());
$table->is_downloading('', get_string('courses'), get_string('courses'));
$table->sortable(false);
$table->is_collapsible = false;


echo $OUTPUT->header();
echo $OUTPUT->heading($strheading);

echo html_writer::start_tag('div', array('class'=>'clearfix'));
echo html_writer::start_tag("form", array("action"=>$PAGE->url, 'method'=>'GET', 'style'=>'float:right; margin-bottom:-50px;'));
echo html_writer::start_tag('select', array('name'=>'category', 'style'=>'margin-right:5px;'));
echo html_writer::tag('option', 'All categories', array('value'=>0));
$categories = $DB->get_records("course_categories", array("visible"=>1));
foreach ($categories as $key => $value) {
	$params = array('value'=>$value->id);
	if($category == $value->id){
		$params['selected'] = 'selected';
	}
	echo html_writer::tag('option', $value->name, $params);
}
echo html_writer::end_tag('select');
echo html_writer::empty_tag('input', array('type'=>'submit','value'=>get_string('filter', 'local_plans'), 'style'=>'margin-left:5px;'));
echo html_writer::empty_tag('input', array('type'=>'hidden','name'=>'id', 'value'=>$id));
echo html_writer::empty_tag('input', array('type'=>'hidden','name'=>'type', 'value'=>$type));
echo html_writer::end_tag("form");
echo html_writer::end_tag("div");

echo $editform->display();
$table->out(20, true);
echo $OUTPUT->footer();
