<?php
// This file is part of the Local plans plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This plugin sends users a plans message after logging in
 * and notify a moderator a new user has been added
 * it has a settings page that allow you to configure the messages
 * send.
 *
 * @package    local
 * @subpackage plans
 * @copyright  2015
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Learning Plans and Certifications';
$string['defaultsettings'] = 'Default Settings';
$string['defaultenable'] = 'Enable Default Settings';

$string['plans:view'] = 'Learning plans Dashboard';
$string['plans:manage'] = 'Learning plans manage';
$string['plans:edit'] = 'Learning plans edit';
$string['plans:assign'] = 'Learning plans assign';
$string['plans:enrol'] = 'Learning plans enroll';
$string['plans:myaddinstance'] = "Learning plans";

$string['cr_0'] = 'plans{$a}';
$string['cr_1'] = 'certification{$a}';
$string['criteria_0_0'] = 'Learning Plans';
$string['criteria_0_1'] = 'Certifications';
$string['criteria_1_0'] = 'Create New Learning Plan';
$string['criteria_1_1'] = 'Create New Certification';
$string['criteria_2_0'] = 'Edit Learning Plan';
$string['criteria_2_1'] = 'Edit Certification';
$string['criteria_3_0'] = 'Delete Learning Plan';
$string['criteria_3_1'] = 'Delete Certification';
$string['criteria_4_0'] = 'Do you really want to delete learning plan \'{$a}\'?';
$string['criteria_4_1'] = 'Do you really want to delete certification \'{$a}\'?';
$string['criteria_5_0'] = 'Learning Plan';
$string['criteria_5_1'] = 'Certification';
$string['criteria_6_0'] = 'Add new learning plan';
$string['criteria_6_1'] = 'Add new certification';
$string['criteria_7_0'] = 'Assign to courses learning plan';
$string['criteria_7_1'] = 'Assign to courses certification';
$string['criteria_8_0'] = 'Enroll users to learning plan';
$string['criteria_8_1'] = 'Enroll users to certification';
$string['criteria_9_0'] = 'Learning Plan Enrollment: {$a}';
$string['criteria_9_1'] = 'Certification Enrollment: {$a}';
$string['criteria_10_0'] = 'Enrollment Start Date & Time';
$string['criteria_10_1'] = 'Certification Start Date & Time';
$string['criteria_11_0'] = 'Enrollment End Date & Time';
$string['criteria_11_1'] = 'Certification End Date & Time';
$string['criteria_12_0'] = 'Plans is not enabled';
$string['criteria_12_1'] = 'Certification is not enabled';
$string['criteria_14_0'] = 'Enroll groups to learning plan';
$string['criteria_14_1'] = 'Enroll groups to certification';
$string['criteria_15_0'] = 'Retraining start date';
$string['criteria_15_1'] = 'Retraining end date';
$string['criteria_15_2'] = 'Retraining Status';
$string['criteria_15_3'] = 'Retraining Date Range';
$string['redatetype'] = 'If Enabled use Retraining Period. If Disabled use Date Range';


$string['enabled_plans'] = 'Enable plans';
$string['enabled_plans_desc'] = '';
$string['enabled_certifications'] = 'Enable certifications';
$string['enabled_certifications_desc'] = '';


$string['event_plans_completed'] = 'Learning plan and Certification Completion';
$string['event_plans_completed_description'] = 'Learning plan and Certification completed Event';
$string['event_plans_expired'] = 'Learning plan and Certification Expiration';
$string['event_plans_expired_description'] = 'Learning plan and Certification Expiration event';
$string['event_plans_enrolled'] = 'Learning plan and Certification Enroll';
$string['event_plans_enrolled_description'] = 'Learning plan and Certification Enroll event';
$string['event_plans_unenrolled'] = 'Learning plan and Certification Unenroll';
$string['event_plans_unenrolled_description'] = 'Learning plan and Certification Unenroll event';

$string['returntohome'] = 'Return to home page';
$string['assigncourses'] = 'Assign courses';
$string['enrolgroups'] = 'Assign groups';
$string['enrolusers'] = 'Enroll users';
$string['enrolplans'] = 'Assign learning plans';
$string['availability'] = 'Availability restriction';
$string['expby'] = 'Expiration';
$string['number'] = 'Certification Period After Completion';
$string['type'] = ' ';
$string['renumber'] = 'Retraining Period After Expiration';
$string['retype'] = ' ';
$string['timeoptions'] = 'Expiration';
$string['timeoptions2'] = 'Retraining';
$string['category'] = 'Category';
$string['nocourses'] = 'No Courses Assigned';
$string['courses'] = 'Courses';
$string['plandescription'] = 'Description';
$string['startplan'] = 'Start Plan';
$string['planprogress'] = 'Progress';
$string['plans:enrol'] = 'Enroll into plan';
$string['plans:manage'] = 'Manage plan';
$string['plans:unenrol'] = 'Unenroll from plan';
$string['enable_selfenrollment'] = 'Enable self-enrollment';
$string['search'] = 'Search...';
$string['filter'] = 'Filter';
$string['idnumber_0'] = 'Learning Plan Code';
$string['idnumber_1'] = 'Certification Code';
$string['pendingmessage'] = 'Required prerequisites were not completed yet';
