<?php

require('../../config.php');
require_once('locallib.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$state	  = optional_param('state', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'set-rating' and $id){	
    
	$rate	          = optional_param('rate', 0, PARAM_INT);
    
    if ($rate > 0){
        $rating = $DB->get_record('local_plans_rating', array('planid'=>$id, 'userid'=>$USER->id)); 
        if ($rating){
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->update_record('local_plans_rating', $rating);
        } else {
            $rating = new stdClass();
            $rating->planid = $id;
            $rating->userid = $USER->id;
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->insert_record('local_plans_rating', $rating);     
        }
    }
       
	echo time();
    
}

exit;
