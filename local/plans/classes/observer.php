<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * Event handler for category enrolment plugin.
 *
 * We try to keep everything in sync via listening to events,
 * it may fail sometimes, so we always do a full sync in cron too.
 */
class local_plans_observer {

   /**
     * Event processor - cohort member added.
     * @param \core\event\cohort_member_added $event
     * @return bool
     */
    public static function cohort_member_added(\core\event\cohort_member_added $event) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/enrol/plans/locallib.php');

        if ($plans = $DB->get_records_sql("SELECT planid FROM {local_plans_groups} WHERE groupid={$event->objectid}")) {
          $users = array($event->relateduserid);
          foreach($plans as $plan){
            if($u =  $DB->get_records('local_plans_users',array("planid"=>$plan->planid,'userid'=>$event->relateduserid))){

            }else{
                $d = new stdClass();
                $d->planid = $plan->planid;
                $d->userid = $event->relateduserid;
                $d->timecreated = time();
                $DB->insert_record('local_plans_users', $d);
            }
            if (!$courses = $DB->get_records_sql("SELECT DISTINCT courseid FROM {local_plans_courses} WHERE planid = {$plan->planid}")) {
                continue;
            }
            $data = new stdClass();
            $data->id = $plan->planid;
            $data->users = $users;
            enrol_plans_sync_users($data, $courses, $event->objectid);
          }
        }
        return true;
    }
    public static function cohort_member_removed(\core\event\cohort_member_removed $event) {
        global $DB, $CFG;

        require_once($CFG->dirroot . '/enrol/plans/locallib.php');

        if ($plans = $DB->get_records_sql("SELECT planid FROM {local_plans_groups} WHERE groupid={$event->objectid}")) {
          foreach($plans as $plan){
            $DB->delete_records('local_plans_users', array('planid'=> $plan->planid,'userid'=>$event->relateduserid));
            unenrol_fromplan($plan->planid, 0, $event->relateduserid);
          }
        }
        return true;
    }

     public static function cohort_deleted(\core\event\cohort_deleted $event) {
       global $DB, $CFG;

        require_once($CFG->dirroot . '/enrol/plans/locallib.php');

        if ($plans = $DB->get_records_sql("SELECT planid FROM {local_plans_groups} WHERE groupid={$event->objectid}")) {
          $DB->delete_records('local_plans_groups', array('groupid'=>$event->objectid));
          if($members =  $DB->get_records('cohort_members',array("cohortid"=>$event->objectid))){
              foreach($plans as $plan){
                foreach($members as $member){
                    $DB->delete_records('local_plans_users', array('planid'=> $plan->planid,'userid'=>$member->userid));
                    unenrol_fromplan($plan->planid, 0, $member->userid);
                }
            }
          }
        }
        return true;
    }


    /**
     * Triggered when course is completed.
     *
     * @param \core\event\course_completed $event
     */
    public static function course_completed(\core\event\course_completed $event) {
        global $DB, $SITE;

        $courseid = $event->courseid;
        $userid = $event->relateduserid;


        $plans = $DB->get_records_sql("SELECT pu.planid FROM {local_plans} p, {local_plans_users} pu WHERE p.type = 0 AND p.id = pu.planid AND pu.userid = $userid AND pu.planid IN (SELECT planid as id FROM {local_plans_courses} WHERE courseid=$courseid)");

        $event_properties = array('userid' => $userid, 'relateduserid'=> $userid, 'context'=>context_system::instance(), 'courseid'=>$SITE->id);

        foreach ($plans as $p) {
          $courses = $DB->get_records_sql("SELECT
              pc.id, pc.courseid, cc.timecompleted
              FROM {local_plans_courses} pc
                LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.userid = $userid WHERE pc.planid = $p->planid");

          $isComplete = true;
          foreach($courses as $c){
            if(!$c->timecompleted){
              $isComplete = false;
            }
          }
          if($isComplete){
            if($d = $DB->get_record('local_plans_completions', array('planid'=>$p->planid, 'userid'=>$userid))){
              //$d->timecreated = time();
                  //$DB->update_record('local_plans_completions', $d);
            }else{
              $d = new stdClass();
                  $d->planid = $p->planid;
                  $d->userid = $userid;
                  $d->timecreated = time();
                  $DB->insert_record('local_plans_completions', $d);

                  $event_properties['objectid'] = $p->planid;
                  $event = \local_plans\event\local_plans_completed::create($event_properties);
                  $event->trigger();
            }
           // print("<br>Plan $p->planid completed!");
          }else{
            $DB->delete_records('local_plans_completions', array('planid' => $p->planid, 'userid'=>$userid));
            //print("<br>Plan $p->planid NOT completed!");
          }
        }



        $certifications = $DB->get_records_sql("SELECT pc.certification FROM {local_plans_certifications} pc, {local_plans_users} pu WHERE pu.userid = $userid AND pu.planid = pc.certification");

        foreach ($certifications as $c) {
          $plans = $DB->get_records_sql("SELECT
              pc.planid, cc.timecreated
              FROM {local_plans_certifications} pc
                LEFT JOIN {local_plans_completions} cc ON cc.planid = pc.planid AND cc.userid = $userid AND cc.timecreated > 0
                  WHERE pc.certification = $c->certification");

          $isComplete = true;
          foreach ($plans as $plan){
            if(!$plan->timecreated){
              $isComplete = false;
            }
          }
          if($isComplete){
            $courses = $DB->get_records_sql("SELECT
              pc.id, pc.courseid, cc.timecompleted
              FROM {local_plans_courses} pc
                LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.userid = $userid WHERE pc.planid = $c->certification");

            $isComplete = true;
            foreach($courses as $course){
              if(!$course->timecompleted){
                $isComplete = false;
              }
            }
            if($isComplete){
              if($d = $DB->get_record('local_plans_completions', array('planid'=>$c->certification, 'userid'=>$userid))){
                //$d->timecreated = time();
                    //$DB->update_record('local_plans_completions', $d);
              }else{
                $d = new stdClass();
                $d->planid = $c->certification;
                $d->userid = $userid;
                $d->timecreated = time();
                $DB->insert_record('local_plans_completions', $d);

                $event_properties['objectid'] = $c->certification;
                $event = \local_plans\event\local_plans_completed::create($event_properties);
                $event->trigger();
              }
              //print("<br>Certification $c->certification completed!");
            }else{
              $DB->delete_records('local_plans_completions', array('planid'=>$c->certification, 'userid'=>$userid));
              //print("<br>Certification ($c->certification) COURSES  NOT completed!");
            }
          }else{
            //print("<br>Certification ($c->certification) PLANS NOT completed!");
          }
        }
        $plans = $DB->get_records_sql("SELECT p.* FROM {local_plans} p
          LEFT JOIN {local_plans_users} pu ON pu.planid = p.id AND pu.userid = $userid
          WHERE pu.id IS NULL AND p.type = 1 AND p.id IN (SELECT planid as id FROM {local_plans_courses} WHERE courseid=$courseid)");

        foreach ($plans as $p) {
          $courses = $DB->get_records_sql("SELECT
              pc.id, pc.courseid, cc.timecompleted
              FROM {local_plans_courses} pc
                LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.userid = $userid WHERE
                pc.planid = $p->id OR pc.planid IN (SELECT planid FROM {local_plans_certifications} WHERE certification = $p->id)");

          $isComplete = true;
          foreach($courses as $c){
            if(!$c->timecompleted){
              $isComplete = false;
            }
          }
          if($isComplete){
            $d = new stdClass();
            $d->planid = $p->id;
            $d->userid = $userid;
            $d->timecreated = time();

            if(!$f = $DB->get_record('local_plans_users', array('planid'=>$p->id, 'userid'=>$userid))){
                  $DB->insert_record('local_plans_users', $d);
            }
            if(!$f = $DB->get_record('local_plans_completions', array('planid'=>$p->id, 'userid'=>$userid))){
                  $DB->insert_record('local_plans_completions', $d);
            }
          }
        }
    }
}
