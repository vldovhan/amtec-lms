<?php
/**
 * The local_plans viewed event.
 *
 * @package   local_plans
 * @author    SEBALE
 * @copyright 2016 TalentQuest
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v2 or later
 */

namespace local_plans\event;
defined('MOODLE_INTERNAL') || die();

class local_plans_completed extends \core\event\base {
    /**
     * Init method.
     *
     * @return void
     */
    protected function init() {
        $this->data['crud'] = 'с';
        $this->data['edulevel'] = self::LEVEL_PARTICIPATING;
        $this->data['objecttable'] = 'local_plans';
    }

    /**
     * Return localised event name.
     *
     * @return string
     */
    public static function get_name() {
        return get_string('event_plans_completed', 'local_plans');
    }

    /**
     * Returns description of what happened.
     *
     * @return string
     */
    public function get_description() {
        $a = (object) array('userid' => $this->userid, 'planid' => $this->objectid);
        return get_string('event_plans_completed_description', 'local_plans', $a);
    }

    /**
     * Return the legacy event log data.
     *
     * @return array
     */
    protected function get_legacy_logdata() {
        return(array($this->courseid, 'course', 'plan completed',
                'view.php?pageid=' . $this->objectid, get_string('event_plans_completed', 'local_plans'), $this->contextinstanceid));
    }

    /**
     * Get URL related to the action.
     *
     * @return \moodle_url
     */
    public function get_url() {
        return new \moodle_url('/local/plans/view.php', array('id' => $this->objectid));
    }

    public function get_id() {
        return $this->objectid;
    }

    public function get_userid() {
        return $this->userid;
    }
}
