<?php
defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->dirroot . '/enrol/plans/locallib.php');


function sort_plan_courses($planid, $courseid, $sort)
{
    global $DB;

    $current = 0; $i = 1; $list = array();
    $rows = $DB->get_records_sql("SELECT * FROM {local_plans_courses} WHERE planid = $planid ORDER BY sortorder ASC");
    foreach($rows as $r){
        $r->sortorder = $i;
        if($courseid == $r->courseid){
            $current = $i;
        }
        $list[$i] = $r;
        $i++;
    }
    if($sort == 'up' and $current > 0){
        if(isset($list[$current]))
            $list[$current]->sortorder = ($list[$current]->sortorder > 0)?($list[$current]->sortorder-1):0;
        if(isset($list[$current-1]))
            $list[$current-1]->sortorder = $list[$current-1]->sortorder + 1;
    }else{
        if(isset($list[$current]))
            $list[$current]->sortorder = $list[$current]->sortorder + 1;;
        if(isset($list[$current+1]))
            $list[$current+1]->sortorder = $list[$current+1]->sortorder - 1;
    }
    foreach ($list as $item) {
        $DB->update_record('local_plans_courses', $item);
    }
}
function save_plans_courses_restriction($data){
    global $DB;

    if (has_capability('local/plans:manage', context_system::instance())){

        $rs = $DB->get_record("local_plans_courses", array("planid"=> $data->id, "courseid"=> $data->courseid));
        if($rs->id){
            $rs->restriction = implode(",", $data->courses);
            $DB->update_record('local_plans_courses', $rs);
        }
    }
}
function delete_certification_plans($planid, $certification){
    global $DB;

    if (has_capability('local/plans:manage', context_system::instance())){
        $users = $DB->get_records("local_plans_users", array("planid"=> $certification));
        $courses = $DB->get_records("local_plans_courses", array("planid"=>$planid));
        foreach($courses as $course){
            foreach($users as $user){
                unenrol_fromplan($certification, $course->courseid, $user->userid);
            }
        }
        $DB->delete_records('local_plans_certifications', array('planid'=> $planid,'certification'=> $certification));
    }
}
function delete_plan_users($userid, $planid){
    global $DB;

    $plan = $DB->get_record('local_plans', array('id'=> $planid));
    if($plan->type and false){ // disabled!
        $plans = $DB->get_records_sql("SELECT DISTINCT p.id FROM {local_plans} p, {local_plans_certifications} pc WHERE p.id = pc.planid AND pc.certification = $plan->id");
        foreach($plans as $p){
            $DB->delete_records('local_plans_users', array('planid'=> $p->id,'userid'=>$userid));

            unenrol_fromplan($p->id, 0, $userid);
        }
    }
    $DB->delete_records('local_plans_users', array('planid'=> $planid,'userid'=>$userid));
    unenrol_fromplan($planid, 0, $userid);
}
function delete_plan_course($planid, $courseid){
    global $DB;

    if (has_capability('local/plans:manage', context_system::instance())){
        $plan = $DB->get_record('local_plans', array('id'=> $planid));
        if($plan->type and false){// disabled!
            $plans = $DB->get_records_sql("SELECT DISTINCT p.id FROM {local_plans} p, {local_plans_certifications} pc WHERE p.id = pc.planid AND pc.certification = $plan->id");
            foreach($plans as $p){
                unenrol_fromplan($p->id, $courseid);
            }
        }

        $DB->delete_records('local_plans_courses', array('planid'=> $planid,'courseid'=> $courseid));
        unenrol_fromplan($planid, $courseid);
    }
}
function delete_plan($planid){
	global $DB;

    if (has_capability('local/plans:manage', context_system::instance())){
        $plan = $DB->get_record('local_plans', array('id'=> $planid));
        if($plan->type and false){// disabled!
            $plans = $DB->get_records_sql("SELECT DISTINCT p.id FROM {local_plans} p, {local_plans_certifications} pc WHERE p.id = pc.planid AND pc.certification = $plan->id");
            foreach($plans as $p){
                unenrol_fromplan($p->id);
            }
        }
        unenrol_fromplan($planid);
        $DB->delete_records('local_plans_certifications', array('planid'=> $planid));
        $DB->delete_records('local_plans_users', array('planid'=> $planid));
        $DB->delete_records('local_plans_courses', array('planid'=> $planid));
        $DB->delete_records('local_plans_completions', array('planid'=> $planid));
        $DB->delete_records('local_plans', array('id'=> $planid));
    }
}

function save_plans_enrols($data){
    global $DB, $CFG;

    if(empty($data->users)){
        return false;
    }
    foreach($data->users as $user){
        $d = new stdClass();
        $d->planid = $data->id;
        $d->userid = $user;
        $d->timecreated = time();
        $DB->insert_record('local_plans_users', $d);
    }

    $courses = $DB->get_records_sql("SELECT DISTINCT courseid FROM {local_plans_courses} WHERE planid IN (SELECT planid FROM {local_plans_certifications} WHERE certification = $data->id) or planid = $data->id");

    enrol_plans_sync_users($data, $courses);
}

function save_plans_enrols_groups($data){
    global $DB, $CFG;

    if(empty($data->groups)){
        return false;
    }
    $courses = $DB->get_records_sql("SELECT DISTINCT courseid FROM {local_plans_courses} WHERE planid IN (SELECT planid FROM {local_plans_certifications} WHERE certification = $data->id) or planid = $data->id");


    foreach($data->groups as $group){
        $d = new stdClass();
        $d->planid = $data->id;
        $d->groupid = $group;
        $d->timecreated = time();
        $DB->insert_record('local_plans_groups', $d);

        $users = array();
        if($members =  $DB->get_records('cohort_members',array("cohortid"=>$group))){
            foreach($members as $member){
                if($u =  $DB->get_records('local_plans_users',array("planid"=>$data->id,'userid'=>$member->userid))){

                }else{
                    $d = new stdClass();
                    $d->planid = $data->id;
                    $d->userid = $member->userid;
                    $d->timecreated = time();
                    $DB->insert_record('local_plans_users', $d);

                    $users[] = (int)$member->userid;
                }
            }
            if($users and $courses){
                $data->users = $users;
                enrol_plans_sync_users($data, $courses, $group);
            }
        }
    }
}
function delete_plan_groups($groupid, $planid){
    global $DB;


    $DB->delete_records('local_plans_groups', array('planid'=> $planid, 'groupid'=>$groupid));

    if($members =  $DB->get_records('cohort_members',array("cohortid"=>$groupid))){
        foreach($members as $member){
            $DB->delete_records('local_plans_users', array('planid'=> $planid,'userid'=>$member->userid));
            unenrol_fromplan($planid, 0, $member->userid);
        }
    }
}

function save_plans_certifications($data){
    global $DB, $CFG;

    if($data->id){
        if(empty($data->plans)){
            return false;
        }
        foreach($data->plans as $p){
            $d = new stdClass();
            $d->certification = $data->id;
            $d->planid = $p;
            $d->timecreated = time();
            $DB->insert_record('local_plans_certifications', $d);


            $result = $DB->get_records("local_plans_courses", array("planid"=>$p));
            $courses = array();
            foreach($result as $c){
                $courses[] = $c->courseid;
            }
            enrol_plans_sync_courses($courses, $p);
        }
    }
}


function save_plans_courses($data){
	global $DB, $CFG;

	if($data->id){
		if(empty($data->courses)){
			return false;
		}
		foreach($data->courses as $course){
			$d = new stdClass();
			$d->planid = $data->id;
            $d->courseid = $course;
			$d->timecreated = time();
			$DB->insert_record('local_plans_courses', $d);
		}

        enrol_plans_sync_courses($data->courses, $data->id);
	}
}

function plans_enable($type)
{
    global $OUTPUT, $PAGE;

    $name = ($type)?"certifications":"plans";
    $enabled = get_config('local_plans', 'enabled_'.$name);

    if(!$enabled){
        $PAGE->set_context(context_system::instance());
        $PAGE->set_heading(get_string('criteria_12_'.$type, 'local_plans'));

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('criteria_12_'.$type, 'local_plans'));
        $url = new moodle_url($CFG->wwwroot.'/index.php');
        echo $OUTPUT->confirm(get_string('returntohome', 'local_plans'), $url, $url);
        echo $OUTPUT->footer();
        die;
    }
}
function save_plan($data){
	global $DB;

    //print_r($data); exit;
	if($data->id){
        $DB->update_record('local_plans', $data);
	    return $data->id;
	}else{
		$data->timecreated = time();
		return $DB->insert_record('local_plans', $data);
	}
}

class plans_users_table extends table_sql {

    function __construct($uniqueid, $plan) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('firstname','lastname', 'email', 'timecreated', 'actions');
        $headers = array(get_string('firstname'), get_string('lastname'), get_string('email'), 'Enrolled', 'Actions');

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT u.id, u.firstname, u.lastname, pl.timecreated, pl.planid, email, '' as asctions";
        $from = "{local_plans_users} pl, {user} u";
        $where = "u.id = pl.userid AND pl.planid = $plan->id";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/enrol.php', array('id' => $values->planid, 'delete'=>1,'type'=>$type, 'userid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

    }
}

class plans_groups_table extends table_sql {

    function __construct($uniqueid, $plan) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('name', 'timecreated', 'actions');
        $headers = array('Group name', 'Enrolled', 'Actions');

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT c.id, c.name, p.timecreated, p.planid, '' as asctions";
        $from = "{local_plans_groups} p, {cohort} c";
        $where = "c.id = p.groupid AND p.planid = $plan->id";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/enrolgroups.php', array('id' => $values->planid, 'delete'=>1,'type'=>$type, 'groupid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

    }
}

class plans_certification_table extends table_sql {

    function __construct($uniqueid, $plan) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('name', 'timecreated', 'actions');
        $headers = array(get_string("criteria_5_0", 'local_plans'), 'Assigned', 'Actions');

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT p.id, p.name, pc.timecreated, pc.certification, '' as asctions";
        $from = "{local_plans} p, {local_plans_certifications} pc";
        $where = "p.id = pc.planid AND pc.certification = $plan->id";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/plans.php', array('id' => $values->certification, 'delete'=>1,'type'=>$type, 'planid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));

    }
}

class plans_courses_table extends table_sql {
    var $index = 1;
    function __construct($uniqueid, $plan) {
        global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('course', 'restriction', 'timecreated', 'actions');
        $headers = array('Course', get_string('availability'), 'Assigned', 'Actions');

        $this->define_headers($headers);
        $this->define_columns($columns);

        $fields = "DISTINCT c.id, c.fullname as course, pc.restriction, pc.planid, pc.timecreated, '' as asctions";
        $from = "{course} c, {local_plans_courses} pc";
        $where = "c.category > 0 AND c.visible = 1 AND c.id = pc.courseid AND pc.planid = $plan->id ORDER BY pc.sortorder ASC";

        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl($PAGE->url);
    }
    function col_restriction($values) {
        $courses = ($values->restriction) ? explode(",", $values->restriction): array();

        return "Courses: ".count($courses);
    }
    function col_timecreated($values) {
      return date('m/d/Y', $values->timecreated);
    }
    function col_actions($values) {
        global $CFG, $OUTPUT;

        $type = optional_param('type', 0, PARAM_INT);
        $html = "";
        if($this->totalrows > 1){
            if($this->index > 1){
                $html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/assign.php', array('id' => $values->planid, 'sort'=>'up','type'=>$type, 'courseid'=> $values->id)), html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/sort_asc'), 'alt' => get_string('asc'), 'class' => 'iconsmall')), array('title' => get_string('asc')));
            }
            if($this->index < $this->totalrows){
                $html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/assign.php', array('id' => $values->planid, 'sort'=>'down','type'=>$type, 'courseid'=> $values->id)), html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/sort_desc'), 'alt' => get_string('desc'), 'class' => 'iconsmall')), array('title' => get_string('desc')));
            }
        }
        $html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/courses.php', array('id' => $values->planid, 'sort'=>'down','type'=>$type, 'courseid'=> $values->id)), html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('availability'), 'class' => 'iconsmall')), array('title' => get_string('availability')));

        $html .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/assign.php', array('id' => $values->planid, 'delete'=>1,'type'=>$type, 'courseid'=> $values->id)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));


        $this->index++;
        return $html;

    }
}
class plans_table extends table_sql {
    function __construct($uniqueid, $view = 1, $type = 1) {
		global $CFG, $PAGE;

        parent::__construct($uniqueid);

        $columns = array('name');
        $headers = array(get_string('criteria_5_'.$type, 'local_plans'));

        if($type){
            $columns[] = 'plans';
            $headers[] = get_string('criteria_0_0', 'local_plans');
        }

        $columns[] = 'courses';
        $headers[] = get_string('courses');
        $columns[] = 'users';
        $headers[] = 'Enrolled users';
        $columns[] = 'actions';
        $headers[] = 'Actions';




        $this->define_headers($headers);
        $this->define_columns($columns);

        $sql = ($view == 2)?'':" AND p.visible =$view";
        $fields = "p.*, c.courses, '' as asctions, pl.plans, e.users";
        $from = "{local_plans} p
            LEFT JOIN (SELECT certification, count(id) as plans FROM {local_plans_certifications} GROUP BY certification) pl ON pl.certification = p.id
            LEFT JOIN (SELECT planid, COUNT(distinct courseid) as courses FROM {local_plans_courses} GROUP BY planid) c ON c.planid = p.id
        	LEFT JOIN (SELECT planid, COUNT(userid) as users FROM {local_plans_users} GROUP BY planid) e ON e.planid = p.id";
        $where = "p.type = $type $sql";

		    $this->set_sql($fields, $from, $where, array());
		    $this->define_baseurl($PAGE->url);
    }


    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $type = optional_param('type', 0, PARAM_INT);
        $urlparams = array('id' => $values->id, 'type' => $type);
        $showhideurl = new moodle_url($CFG->wwwroot.'/local/plans/edit.php', $urlparams + array('sesskey' => sesskey()));

		$context = context_system::instance();
		$manager = has_capability('local/plans:manage', $context);
        $canassign = has_capability('local/plans:assign', $context);
		$canenrol = has_capability('local/plans:enrol', $context);

        if ($manager) {
            if ($values->visible) {
                $showhideurl->param('hide', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('inactive'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('inactive')));
            } else {
                $showhideurl->param('show', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'alt' => get_string('active'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('active')));
            }

            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/edit.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
                array('title' => get_string('edit')));
        }
        if ($canassign){
            if($type){
                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/plans.php', $urlparams),
                    html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/folder'), 'alt' => get_string('enrolplans', 'local_plans'), 'class' => 'iconsmall')),
                    array('title' => get_string('enrolplans', 'local_plans')));
            }
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/assign.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/course'), 'alt' => get_string('assigncourses', 'local_plans'), 'class' => 'iconsmall')),
                array('title' => get_string('assigncourses', 'local_plans')));

        }
        if ($canenrol and !$type) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/enrol.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/users'), 'alt' => get_string('enrolusers', 'local_plans'), 'class' => 'iconsmall')),
                array('title' => get_string('enrolusers', 'local_plans')));
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/enrolgroups.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('i/groups'), 'alt' => get_string('enrolgroups', 'local_plans'), 'class' => 'iconsmall')),
                array('title' => get_string('enrolgroups', 'local_plans')));
        }
        if ($manager) {
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/plans/edit.php', $urlparams + array('delete' => 1)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));
        }

	    return implode(' ', $buttons);
    }
    function col_name($values) {
        $context = context_system::instance();

        $image = '';
        if($values->image){
            $url = moodle_url::make_pluginfile_url($context->id,'local_plans','image', $values->id,'/', $values->image);
            $image = html_writer::empty_tag('img', array('src' => $url, 'alt' => $values->name, 'style'=>'width:60px'), array('title' => $values->name));
        }
        return $image . ' ' . $values->name;
    }

    function col_courses($values) {
      return intval($values->courses);
    }
    function col_users($values) {
      return intval($values->users);
    }
    function col_plans($values) {
      return intval($values->plans);
    }
}


function get_plan_image_url($plan){
    global $CFG, $DB;
    $context = context_system::instance();

    if ($plan->image != ''){
        $url = moodle_url::make_pluginfile_url($context->id,'local_plans','image', $plan->id,'/', $plan->image);
    } else {
        $url = $CFG->wwwroot."/theme/talentquest/pix/frontcoursebg.jpg";
    }

    return $url;
}

function plan_print_rating($plan, $is_enrolled = false){
    global $CFG, $DB, $USER;
    $output = '';

    $plan_rating = $DB->get_record_sql("SELECT ROUND(SUM(r.value)/COUNT(r.id)) as rate FROM {local_plans_rating} r LEFT JOIN {user} u ON u.id = r.userid WHERE r.planid = $plan->id AND u.deleted = 0 GROUP BY r.planid");

    if ($is_enrolled){
        $output .= html_writer::start_tag('div', array('class'=>'star-rating course-star-rating', 'id'=>'course_star_rating'));
        $params = array('type'=>'radio', 'name'=>'rate', 'class'=>'rating');

        for ($i = 1; $i <=5; $i++){
            $params['value'] = $i;
            if (isset($params['checked'])) unset($params['checked']);
            if ($plan_rating->rate == $i) $params['checked'] = 'checked';
            $output .= html_writer::empty_tag('input', $params);
        }

        $output .= html_writer::end_tag('div');

        $output .= '<script>
            jQuery("#course_star_rating").rating(function(vote, event){
                 jQuery.ajax({
                    url: "'.$CFG->wwwroot.'/local/plans/ajax.php?action=set-rating&id='.$plan->id.'",
                    type: "GET",
                    data: {rate: vote},
                });
            });
        </script>';
    } else {
        $output .= html_writer::start_tag('div', array('class'=>'star-rating course-star-rating'));
        $output .= html_writer::start_tag('div', array('class'=>'stars'));

        for ($i = 1; $i <=5; $i++){
            $params = array('class'=>'stars', 'title'=>$i, 'class'=>'star');
            if (isset($plan_rating->rate) and $plan_rating->rate >= $i) $params['class'] .= ' fullStar';
            $output .= html_writer::tag('a', '', $params);
        }

        $output .= html_writer::end_tag('div');
        $output .= html_writer::end_tag('div');
    }

    return $output;
}

function get_plan_progress($plan){
    global $DB, $CFG, $USER;
    $progress = 0;

    $courses = $DB->get_records_sql("SELECT pc.id, pc.courseid, cc.timecompleted
                                            FROM {local_plans_courses} pc
                                        LEFT JOIN {course_completions} cc ON cc.course = pc.courseid AND cc.timecompleted > 0 AND cc.userid = $USER->id
                                            WHERE pc.planid = $plan->id");

    $all_courses = 0; $completed_courses = 0;
    if (count($courses)){
        foreach ($courses as $course){
            $all_courses++;
            if ($course->timecompleted) $completed_courses++;
        }
        if ($completed_courses > 0){
            $progress = ($completed_courses/$all_courses)*100;
        }
    }

    return $progress;
}

function self_plan_enrol($plan){
    global $DB, $CFG, $USER;

    $d = new stdClass();
    $d->planid = $plan->id;
    $d->userid = $USER->id;
    $d->timecreated = time();
    $DB->insert_record('local_plans_users', $d);

    $courses = $DB->get_records_sql("SELECT DISTINCT courseid FROM {local_plans_courses} WHERE planid IN (SELECT planid FROM {local_plans_certifications} WHERE certification = $plan->id) or planid = $plan->id");

    $data = new stdClass();
    $data->id = $plan->id;
    $data->users = array($USER->id);

    enrol_plans_sync_users($data, $courses);
}

