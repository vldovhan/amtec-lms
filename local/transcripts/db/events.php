<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade code containing changes to the plugin data table.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$observers = array(

    // User logging out.
    array(
        'eventname' => '\core\event\user_enrolment_created',
        'callback'  => 'local_transcripts_observer::transcripts_user_enrolment_created',
    ),
    array(
        'eventname' => '\core\event\user_enrolment_deleted',
        'callback'  => 'local_transcripts_observer::transcripts_user_enrolment_deleted',
    ),
    array(
        'eventname' => '\core\event\course_completed',
        'callback'  => 'local_transcripts_observer::transcripts_course_completed',
    ),
    array(
        'eventname' => '\core\event\course_module_completion_updated',
        'callback'  => 'local_transcripts_observer::transcripts_course_module_completion_updated',
    ),
    array(
        'eventname' => '\local_plans\event\local_plans_enrolled',
        'callback'  => 'local_transcripts_observer::transcripts_user_plan_enrolled',
    ),
    array(
        'eventname' => '\local_plans\event\local_plans_completed',
        'callback'  => 'local_transcripts_observer::transcripts_user_plan_completed',
    ),
    array(
        'eventname' => '\local_plans\event\local_plans_unenrolled',
        'callback'  => 'local_transcripts_observer::transcripts_user_plan_unenrolled',
    ),
);
