<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'Transcripts';
$string['transcripts'] = 'Transcripts';
$string['search'] = 'Search'; 
$string['filter'] = 'Filter'; 
$string['enabled'] = 'Enabled Transcripts';
$string['enabled_desc'] = 'Enable Transcripts';
$string['username'] = 'User Name'; 
$string['coursename'] = 'Course Name'; 
$string['learningplan'] = 'Learning Plan'; 
$string['learningplans'] = 'Learning Plans'; 
$string['certification'] = 'Certification'; 
$string['certifications'] = 'Certifications & Retraining'; 
$string['enrolldate'] = 'Enroll Date'; 
$string['unenrolldate'] = 'Unenroll Date'; 
$string['expirationdate'] = 'Expiration Date'; 
$string['grade'] = 'Grade'; 
$string['maxgrade'] = 'Max Grade'; 
$string['status'] = 'Status'; 
$string['completeddate'] = 'Completed Date'; 
$string['completed'] = 'Completed'; 
$string['completeddate'] = 'Completed Date'; 
$string['selectuser'] = 'Select user...'; 
$string['inprogress'] = 'In progress'; 
$string['closed'] = 'Closed'; 
$string['returntohome'] = 'Return to Dashboard'; 
$string['tabcourses'] = 'Courses'; 
$string['tabplans'] = 'Plans'; 
$string['tabcertifications'] = 'Certifications'; 
$string['actions'] = 'Actions'; 
$string['coursedetails'] = ' details'; 
$string['modulename'] = 'Module Name'; 
$string['moduletype'] = 'Module Type'; 
$string['timecreated'] = 'Time Created'; 
$string['for'] = ' for '; 
$string['transcripts:report'] = 'Transcripts view report'; 
$string['transcripts:view'] = 'View Transcripts'; 


