<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once('../../config.php');
require_once('details_table.php');
require_once('lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$download   = optional_param('download', '', PARAM_ALPHA);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$instanceid = optional_param('instanceid', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/transcripts:view', $systemcontext);

$transcript = $DB->get_record('local_transcripts', array('id'=>$id));
$user = $DB->get_record('user', array('id'=>$transcript->userid));

$title = $transcript->coursename.get_string('coursedetails', 'local_transcripts').get_string('for', 'local_transcripts').fullname($user);

// check if plugin is enabled
transcripts_enable();

if ($action == 'delete' and $instanceid){
    delete_transcript($instanceid);
}

$PAGE->set_url(new moodle_url("/local/transcripts/details.php", array('id'=>$id)));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new details_table('table', $id, $search, $transcript->userid, $download);
$table->is_collapsible = false;
$table->is_downloading($download, $title, $title);

if (!$download){
    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    
    $renderer = $PAGE->get_renderer('local_transcripts');
    $renderer->transcripts_print_tabs('courses');
    
    echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'transcripts-search-form'));
    
    echo html_writer::start_tag("label",  array());
    echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_transcripts').' ...', 'value' => $search));
    echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('filter', 'local_transcripts')));
    echo html_writer::end_tag("label");
    echo html_writer::end_tag("form");
}

$table->out(20, true);

if (!$download){
    echo $OUTPUT->footer();
}
