<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Transcripts
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class transcripts_table extends table_sql {
    
    public $search = '';
    public $userid = '';
    public $url_params = '';
    
    function __construct($uniqueid, $id = 0, $search, $userid = 0, $download = false, $planid = 0, $certificationid = 0) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $this->search = $search;
        $this->userid = $userid;
        
        if (has_capability('local/transcripts:report', $systemcontext)){
            $columns = array('username', 'coursename', 'learningplan', 'certification', 'enrolldate', 'unenrolldate', 'grade', 'maxgrade', 'status', 'completeddate');
            $headers = array(
                get_string('username', 'local_transcripts'),
                get_string('coursename', 'local_transcripts'),
                get_string('learningplan', 'local_transcripts'),
                get_string('certification', 'local_transcripts'),
                get_string('enrolldate', 'local_transcripts'),
                get_string('unenrolldate', 'local_transcripts'),
                get_string('grade', 'local_transcripts'),
                get_string('maxgrade', 'local_transcripts'),
                get_string('status', 'local_transcripts'),
                get_string('completeddate', 'local_transcripts'));
        } else {
            $columns = array('coursename', 'learningplan', 'certification', 'enrolldate', 'unenrolldate', 'grade', 'maxgrade', 'status', 'completeddate');
            $headers = array(
                get_string('coursename', 'local_transcripts'),
                get_string('learningplan', 'local_transcripts'),
                get_string('certification', 'local_transcripts'),
                get_string('enrolldate', 'local_transcripts'),
                get_string('unenrolldate', 'local_transcripts'),
                get_string('grade', 'local_transcripts'),
                get_string('maxgrade', 'local_transcripts'),
                get_string('status', 'local_transcripts'),
                get_string('completeddate', 'local_transcripts'));
        }
        if (!$download){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_transcripts');
        }
        
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (t.coursename LIKE '%$search%' OR t.learningplan LIKE '%$search%' OR t.certification LIKE '%$search%')" : "";
        $fields = "t.id, t.userid, t.coursename, t.learningplan, t.learningplanid, t.certification, t.certificationid, t.enrolldate, t.unenrolldate, CONCAT(u.firstname, ' ', u.lastname) as username, t.grade, t.maxgrade, t.status, t.completeddate, t.id as actions";
        $from = "{local_transcripts} t LEFT JOIN {user} u ON u.id = t.userid ";
        $where = 't.id > 0'.$sql_search;
        
        $where .= ' AND t.type = "course"';
        
        if (!has_capability('local/transcripts:report', $systemcontext)){
            $where .= ' AND t.userid = '.$USER->id;
        } elseif (!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance())){
            $where .= " AND u.managerid = $USER->id";
        }
        if ($userid > 0){
            $where .= ' AND t.userid = '.$userid;
        }        
        if ($planid > 0){
            $where .= ' AND t.learningplanid = '.$planid;
        }
        if ($certificationid > 0){
            $where .= ' AND t.certificationid = '.$certificationid;
        }
        
        $url_params = array();
        if ($userid > 0) $url_params['userid'] = $userid;
        if ($search != '') $url_params['search'] = $search;
        if ($planid > 0) $url_params['planid'] = $planid;
        if ($certificationid > 0) $url_params['certificationid'] = $certificationid;
        $this->url_params = $url_params;
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl(new moodle_url($CFG->wwwroot.'/local/transcripts/index.php', $this->url_params));
    }
    function col_enrolldate($values) {
      return ($values->enrolldate) ? date('m/d/Y', $values->enrolldate) : '-';
    }
    function col_unenrolldate($values) {
      return ($values->unenrolldate) ? date('m/d/Y', $values->unenrolldate) : '-';
    }
    function col_completeddate($values) {
      return ($values->completeddate) ? date('m/d/Y', $values->completeddate) : '-';
    }
    function col_status($values) {
        if ($values->status == 3){
            return get_string('closed', 'local_transcripts');
        } elseif ($values->status == 2){
            return get_string('completed', 'local_transcripts');
        } elseif ($values->status == 1){
            return get_string('inprogress', 'local_transcripts');
        } else {
            return '';
        }
    }
    function col_grade($values) {
      return ($values->grade) ? round($values->grade, 2) : '';
    }
    function col_maxgrade($values) {
      return ($values->maxgrade) ? round($values->maxgrade, 2) : '';
    }
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = $this->url_params + array('id' => $values->id);
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/transcripts/index.php', $urlparams + array('action' => 'delete')),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/transcripts/details.php', $urlparams),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('view'), 'class' => 'iconsmall')),
            array('title' => get_string('view')));
        
	    return implode(' ', $buttons);
    }
}
