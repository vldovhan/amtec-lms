<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer file.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

/**
 * Standard HTML output renderer for badges
 */
class local_transcripts_renderer extends plugin_renderer_base {

    // Prints tabs
    public function transcripts_print_tabs($current = 'courses') {
        global $DB;

        $systemcontext   = context_system::instance();
        $row = array();

        $row[] = new tabobject('courses',
                    new moodle_url('/local/transcripts/index.php'),
                    get_string('tabcourses', 'local_transcripts')
                );

        if (get_config('local_plans', 'enabled_plans')){
            $row[] = new tabobject('plans',
                        new moodle_url('/local/transcripts/plans.php'),
                        get_string('tabplans', 'local_transcripts')
                    );
        }

        if (get_config('local_plans', 'enabled_certifications')){
            $row[] = new tabobject('certifications',
                        new moodle_url('/local/transcripts/certifications.php'),
                        get_string('tabcertifications', 'local_transcripts')
                    );
        }
        
        echo $this->tabtree($row, $current);
    }

}
