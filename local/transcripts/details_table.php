<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Transcripts
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class details_table extends table_sql {
    
    public $search = '';
    public $id = 0;
    public $userid = 0;
    
    function __construct($uniqueid, $id = 0, $search, $userid = 0, $download = false) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $this->id = $id;
        $this->search = $search;
        $this->userid = $userid;
        
        $columns = array('modulename', 'moduletype', 'grade', 'maxgrade', 'status', 'completeddate');
        $headers = array(
            get_string('modulename', 'local_transcripts'),
            get_string('moduletype', 'local_transcripts'),
            get_string('grade', 'local_transcripts'),
            get_string('maxgrade', 'local_transcripts'),
            get_string('status', 'local_transcripts'),
            get_string('completeddate', 'local_transcripts')
        );
        
        if (has_capability('local/transcripts:report', $systemcontext) and !$download){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_transcripts');
        }
        
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (t.modulename LIKE '%$search%' OR t.moduletype LIKE '%$search%')" : "";
        $fields = "t.id, t.userid, t.modulename, t.moduletype, t.grade, t.maxgrade, t.status, t.completeddate, t.timemodified, t.id as actions";
        $from = "{local_transcripts} t LEFT JOIN {user} u ON u.id = t.userid ";
        $where = 't.id > 0'.$sql_search;
        
        $where .= ' AND t.type = "module"';
        
        if ($userid > 0){
            $where .= ' AND t.userid = '.$userid;
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/transcripts/details.php?id=".$id."&search=".$search);
    }
    function col_moduletype($values) {
      return (!empty($values->moduletype)) ? get_string('pluginname', 'mod_'.$values->moduletype) : '';
    }
    function col_timemodified($values) {
      return ($values->timemodified) ? date('m/d/Y', $values->timemodified) : '-';
    }
    function col_completeddate($values) {
      return ($values->completeddate) ? date('m/d/Y', $values->completeddate) : '-';
    }
    function col_status($values) {
        if ($values->status == 3){
            return get_string('closed', 'local_transcripts');
        } elseif ($values->status == 2){
            return get_string('completed', 'local_transcripts');
        } elseif ($values->status == 1){
            return get_string('inprogress', 'local_transcripts');
        } else {
            return '';
        }
    }
    function col_grade($values) {
      return ($values->grade) ? round($values->grade, 2) : '';
    }
    function col_maxgrade($values) {
      return ($values->maxgrade) ? round($values->maxgrade, 2) : '';
    }
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $this->id, 'instanceid' => $values->id, 'search'=>$search);
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/transcripts/details.php', $urlparams + array('action' => 'delete')),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        
	    return implode(' ', $buttons);
    }
}
