<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Gamification version file.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

function transcript_get_users_select($id = 0, $type = 'course'){
    global $DB, $USER;
    $output = '';
    
    $where = '';
    if(!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance())){
        $where .= " AND u.managerid = $USER->id";
    }
    
    $transcripts_user = $DB->get_records_sql("SELECT DISTINCT(u.id), u.* FROM {local_transcripts} t LEFT JOIN {user} u ON u.id = t.userid WHERE t.type = '$type' AND u.deleted = 0 AND u.suspended = 0 $where ORDER BY u.firstname, u.lastname");
    
    $output .= '<select name = "userid">';
    $output .= html_writer::tag('option', get_string('selectuser', 'local_transcripts'), array('value' => '0'));
    
    if (count($transcripts_user)){
        foreach ($transcripts_user as $user){
            $params = array();
            $params['value'] = $user->id;
            if ($id == $user->id) $params['selected'] = 'selected';
            $output .= html_writer::tag('option', fullname($user), $params);
        }
    }
                          
    $output .= '</select>';
        
    return $output;
}

function transcripts_enable()
{
    global $OUTPUT, $PAGE;
    
    if(!get_config('local_transcripts', 'enabled')){
        $PAGE->set_context(context_system::instance());
        $PAGE->set_heading(get_string('pluginname', 'local_transcripts'));

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('pluginname', 'local_transcripts'));
        $url = new moodle_url($CFG->wwwroot.'/index.php');
        echo $OUTPUT->confirm(get_string('returntohome', 'local_transcripts'), $url, $url);
        echo $OUTPUT->footer();
        die;
    }
}

function delete_transcript($id){
    global $DB, $USER, $CFG;
    
    $transcript = $DB->get_record('local_transcripts', array('id'=>$id));
    if ($transcript){
        $DB->delete_records('local_transcripts', array('id'=>$id));
        if ($transcript->type == 'course'){
            $other_course = $DB->get_record('local_transcripts', array('courseid'=>$transcript->courseid, 'userid'=>$transcript->userid, 'type'=>'course'));
            if (!$other_course){
                $DB->delete_records('local_transcripts', array('courseid'=>$transcript->courseid, 'userid'=>$transcript->userid, 'type'=>'module'));
            }    
        }
    }
}


