<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Local stuff for category enrolment plugin.
 *
 * @package    local_transcripts
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Event observer for transcripts.
 */
class local_transcripts_observer {

    /**
     * Triggered when 'course_completed' event is triggered.
     *
     * @param \core\event\course_completed $event
     */
    public static function transcripts_course_completed(\core\event\course_completed $event) {
        global $DB, $CFG, $USER;

        if(get_config('local_transcripts', 'enabled')){

            require_once($CFG->libdir . '/completionlib.php');
            require_once($CFG->dirroot . '/grade/querylib.php');
            require_once($CFG->libdir . '/gradelib.php');

            $course = $DB->get_record('course', array('id'=>$event->courseid));
            $user = $DB->get_record('user', array('id'=>$event->relateduserid));

            $data = new stdClass();
            $data->userid = $user->id;
            $data->courseid = $course->id;
            $data->status = 2;
            $data->type = 'course';

            // update course completion
            $grade = grade_get_course_grade($user->id, $course->id);

            $transcripts = $DB->get_records_sql("SELECT * FROM {local_transcripts} WHERE userid = $user->id AND courseid = $course->id AND unenrolldate = 0 AND type = 'course' ORDER BY timecreated DESC");

            if (count($transcripts)){
                foreach ($transcripts as $transcript){
                    if ($grade){
                        $transcript->grade = $grade->grade;
                        $transcript->maxgrade = $grade->item->grademax;
                    }

                    $transcript->status = $data->status;
                    $transcript->timemodified = time();
                    $DB->update_record('local_transcripts', $transcript);
                }
            } else {
                $enrollments = $DB->get_records_sql("SELECT ue.*, e.enrol, e.customint1, p.name as planname, p.type as plantype
                                                FROM {user_enrolments} ue
                                                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                                    LEFT JOIN {local_plans} p ON p.id = e.customint1
                                                WHERE ue.userid = $user->id AND e.courseid = $course->id");
                if (count($enrollments)){
                    foreach ($enrollments as $enrollment){
                        if ($enrollment->enrol == 'plans'){
                            if ($enrollment->plantype == 1){
                                $data->certificationid = $enrollment->customint1;
                                $data->certification = $enrollment->planname;
                            } else {
                                $data->learningplanid = $enrollment->customint1;
                                $data->learningplan = $enrollment->planname;
                            }
                        }
                        $data->enrolid = $enrollment->enrolid;
                        $data->coursename = $course->fullname;
                        $data->enrolldate = ($enrollment->timestart) ? $enrollment->timestart : 0;
                        $data->timecreated = time();
                        $data->timemodified = time();

                        if ($grade){
                            $data->grade = $grade->grade;
                            $data->maxgrade = $grade->item->grademax;
                        }

                        $id = $DB->insert_record('local_transcripts', $data);
                    }
                }
            }
        }
    }

    /**
     * Triggered when 'course_module_completion_updated' event is triggered.
     *
     * @param \core\event\course_module_completion_updated $event
     */
    public static function transcripts_course_module_completion_updated(\core\event\course_module_completion_updated $event) {
        global $DB, $CFG, $USER;

        if(get_config('local_transcripts', 'enabled')){
            require_once($CFG->libdir . '/completionlib.php');
            require_once($CFG->dirroot . '/grade/querylib.php');
            require_once($CFG->libdir . '/gradelib.php');

            $eventdata = $event->get_record_snapshot('course_modules_completion', $event->objectid);
            $userid = $event->relateduserid;
            $modid = $event->objectid;
            $cmid = $event->contextinstanceid;

            $course = $DB->get_record('course', array('id'=>$event->courseid));
            $userid = ($event->relateduserid) ? $event->relateduserid : $event->userid;
            $user = $DB->get_record('user', array('id'=>$userid));

            $data = new stdClass();
            $data->userid = $user->id;
            $data->courseid = $course->id;
            $data->status = 1;
            $data->type = 'course';

            // update course completion
            $grade = grade_get_course_grade($user->id, $course->id);

            $transcripts = $DB->get_records_sql("SELECT * FROM {local_transcripts} WHERE userid = $user->id AND courseid = $course->id AND unenrolldate = 0 AND type = 'course' ORDER BY timecreated DESC");

            if (count($transcripts)){
                foreach ($transcripts as $transcript){
                    if ($grade){
                        $transcript->grade = $grade->grade;
                        $transcript->maxgrade = $grade->item->grademax;
                    }

                    $transcript->status = $data->status;
                    $transcript->timemodified = time();
                    $DB->update_record('local_transcripts', $transcript);
                }
            } else {
                $enrollments = $DB->get_records_sql("SELECT ue.*, e.enrol, e.customint1, p.name as planname, p.type as plantype
                                                FROM {user_enrolments} ue
                                                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                                    LEFT JOIN {local_plans} p ON p.id = e.customint1
                                                WHERE ue.userid = $user->id AND e.courseid = $course->id");
                if (count($enrollments)){
                    foreach ($enrollments as $enrollment){
                        if ($enrollment->enrol == 'plans'){
                            if ($enrollment->plantype == 1){
                                $data->certificationid = $enrollment->customint1;
                                $data->certification = $enrollment->planname;
                            } else {
                                $data->learningplanid = $enrollment->customint1;
                                $data->learningplan = $enrollment->planname;
                            }
                        }
                        $data->enrolid = $enrollment->enrolid;
                        $data->coursename = $course->fullname;
                        $data->enrolldate = ($enrollment->timestart) ? $enrollment->timestart : time();
                        $data->timecreated = time();
                        $data->timemodified = time();

                        if ($grade){
                            $data->grade = $grade->grade;
                            $data->maxgrade = $grade->item->grademax;
                        }

                        $id = $DB->insert_record('local_transcripts', $data);
                    }
                }
            }

            // update cm completion
            $data = new stdClass();
            $data->userid = $user->id;
            $data->type = 'module';
            $data->courseid = $course->id;
            $data->coursename = $course->fullname;
            $data->cmid = $cmid;
            $data->moduleid = $modid;
            $data->timecreated = time();
            $data->timemodified = time();
            $data->completeddate = time();
            $data->status = 2;

            if ($eventdata->completionstate == COMPLETION_COMPLETE
                || $eventdata->completionstate == COMPLETION_COMPLETE_PASS) {

                $transcripts = $DB->get_records_sql("SELECT * FROM {local_transcripts} WHERE userid = $user->id AND courseid = $course->id AND cmid = $cmid AND type = 'module' ORDER BY timecreated DESC");
                if (count($transcripts)){
                    foreach ($transcripts as $transcript){
                        $transcript->completeddate = time();
                        $transcript->status = 2;
                        $DB->update_record('local_transcripts', $transcript);
                    }
                } else {
                    $cm = $DB->get_record('course_modules', array('id'=>$cmid));
                    if ($cm){
                        $module = $DB->get_record('modules', array('id'=>$cm->module));
                        $data->moduletype = (isset($module->name)) ? $module->name : '';
                        if (!empty($data->moduletype)){
                            $instance = $DB->get_record($module->name, array('id'=>$cm->instance));
                        }
                        $data->modulename = (isset($instance->name)) ? $instance->name : '';
                        if ($instance and $module){
                            $grade = $DB->get_record_sql("SELECT gg.id, gg.userid, gg.rawgrademax, gg.finalgrade FROM {grade_grades} gg LEFT JOIN {grade_items} gi ON gi.id = gg.itemid WHERE gg.userid = $user->id AND gi.courseid = $course->id AND gi.itemtype = 'mod' AND gi.iteminstance = $instance->id GROUP BY gg.id");
                            $data->grade = (isset($grade->finalgrade)) ? $grade->finalgrade : 0;
                            $data->maxgrade = (isset($grade->rawgrademax)) ? $grade->rawgrademax : 0;

                        }
                    }

                    $DB->insert_record('local_transcripts', $data);
                }
            } else {
                $transcripts = $DB->get_records_sql("SELECT * FROM {local_transcripts} WHERE userid = $user->id AND courseid = $course->id AND cmid = $cmid ORDER BY timecreated DESC");
                if (count($transcripts)){
                    foreach ($transcripts as $transcript){
                        $transcript->completeddate = 0;
                        $transcript->status = 1;
                        $DB->update_record('local_transcripts', $transcript);
                    }
                }
            }
        }
    }

    /**
     * Triggered when 'user_enrolment_created' event happens.
     *
     * @param \core\event\user_enrolment_created $event event generated when user profile is updated.
     */
    public static function transcripts_user_enrolment_created(\core\event\user_enrolment_created $event) {
        global $DB, $USER, $CFG;

        if(get_config('local_transcripts', 'enabled')){
            $course = $DB->get_record('course', array('id'=>$event->contextinstanceid));
            $user = $DB->get_record('user', array('id'=>$event->relateduserid));
            $enrolid = $event->objectid;

            $enrollment = $DB->get_record_sql("SELECT ue.*, e.enrol, e.customint1, p.name as planname, p.type as plantype
                                                FROM {user_enrolments} ue
                                                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                                                    LEFT JOIN {local_plans} p ON p.id = e.customint1
                                                WHERE ue.userid = $user->id AND e.courseid = $course->id AND ue.id = $enrolid");

            $data = new stdClass();
            $data->type = 'course';
            $data->enrolid = $enrollment->enrolid;
            $data->userid = $user->id;
            $data->status = 1;
            $data->courseid = $course->id;
            $data->coursename = $course->fullname;
            $data->enrolldate = ($enrollment->timestart) ? $enrollment->timestart : time();
            $data->timecreated = time();
            $data->timemodified = time();

            $type = $data->type;

            if ($enrollment->enrol == 'plans'){
                if ($enrollment->plantype == 1){
                    $data->certificationid = $enrollment->customint1;
                    $data->certification = $enrollment->planname;
                    $type = 'certification';
                } else {
                    $data->learningplanid = $enrollment->customint1;
                    $data->learningplan = $enrollment->planname;
                    $type = 'plan';
                }
            }

            $DB->insert_record('local_transcripts', $data);
        }
    }

    /**
     * Triggered when 'user_enrolment_deleted' event happens.
     *
     * @param \core\event\user_enrolment_deleted $event event generated when user profile is updated.
     */
    public static function transcripts_user_enrolment_deleted(\core\event\user_enrolment_deleted $event) {
        global $DB, $USER;

        if(get_config('local_transcripts', 'enabled')){

            $course = $DB->get_record('course', array('id'=>$event->contextinstanceid));
            $user = $DB->get_record('user', array('id'=>$event->relateduserid));
            $enrolid = $event->objectid;

            $data = new stdClass();
            $data->userid = $user->id;
            $data->enrolid = $enrolid;
            $data->courseid = $course->id;
            $data->status = 3;

            $transcripts = $DB->get_records_sql("SELECT * FROM {local_transcripts} WHERE userid = $data->userid AND courseid = $data->courseid AND enrolid = $enrolid AND type = 'course' ORDER BY timecreated");
            if (count($transcripts)){
                foreach ($transcripts as $transcript){
                    $transcript->coursename = $course->fullname;
                    $transcript->unenrolldate = time();
                    $transcript->timemodified = time();
                    $transcript->status = ($transcript->status == 2) ? 2 : 3;
                    $DB->update_record('local_transcripts', $transcript);
                }
            } else {
                $data->coursename = $course->fullname;
                $data->unenrolldate = time();
                $data->timemodified = time();
                $data->timecreated = time();
                $DB->insert_record('local_transcripts', $data);
            }
        }
    }

    /**
     * Triggered when 'local_plans_enrolled' event happens.
     *
     * @param \local_plans\event\local_plans_enrolled $event event generated when user profile is updated.
     */
    public static function transcripts_user_plan_enrolled(\local_plans\event\local_plans_enrolled $event) {
        global $DB, $USER, $CFG;

        if(get_config('local_transcripts', 'enabled')){

            $plan = $DB->get_record('local_plans', array('id'=>$event->objectid));
            $user = $DB->get_record('user', array('id'=>$event->userid));

            if ($plan) {
                $data = new stdClass();
                $data->userid = $user->id;
                $data->type = ($plan->type > 0) ? "certification" : "plan";
                $data->status = 1;
                $data->enrolldate = time();
                $data->timecreated = time();
                $data->timemodified = time();

                if ($data->type == 'certification'){
                    $transcripts = $DB->get_records('local_transcripts', array('userid'=>$user->id, 'certificationid'=>$plan->id, 'type'=>'certification', 'unenrolldate'=>0));
                    if (count($transcripts) == 0){
                        $data->certificationid = $plan->id;
                        $data->certification = $plan->name;
                        $DB->insert_record('local_transcripts', $data);
                    }
                } elseif ($data->type == 'plan'){
                    $transcripts = $DB->get_records('local_transcripts', array('userid'=>$user->id, 'learningplanid'=>$plan->id, 'type'=>'plan', 'unenrolldate'=>0));
                    if (count($transcripts) == 0){
                        $data->learningplanid = $plan->id;
                        $data->learningplan = $plan->name;
                        $DB->insert_record('local_transcripts', $data);
                    }
                }
            }
        }
    }

    /**
     * Triggered when 'local_plans_unenrolled' event happens.
     *
     * @param \local_plans\event\local_plans_unenrolled $event event generated when user profile is updated.
     */
    public static function transcripts_user_plan_unenrolled(\local_plans\event\local_plans_unenrolled $event) {
        global $DB, $USER, $CFG;

        if(get_config('local_transcripts', 'enabled')){

            $plan = $DB->get_record('local_plans', array('id'=>$event->objectid));
            $user = $DB->get_record('user', array('id'=>$event->userid));

            if ($plan) {
                $data = new stdClass();
                $data->type = ($plan->type > 0) ? "certification" : "plan";
                $data->unenrolldate = time();
                $data->timemodified = time();

                if ($data->type == 'certification'){
                    $transcripts = $DB->get_records('local_transcripts', array('userid'=>$user->id, 'certificationid'=>$plan->id, 'type'=>'certification', 'unenrolldate'=>0));
                } elseif ($data->type == 'plan'){
                    $transcripts = $DB->get_records('local_transcripts', array('userid'=>$user->id, 'learningplanid'=>$plan->id, 'type'=>'plan', 'unenrolldate'=>0));
                }
                if (count($transcripts)){
                    foreach ($transcripts as $transcript){
                        $data->id = $transcript->id;
                        $data->status = ($transcript->status == 2) ? 2 : 3;
                        $DB->update_record('local_transcripts', $data);
                    }
                }
            }
        }
    }

    /**
     * Triggered when 'local_plans_completed' event happens.
     *
     * @param \local_plans\event\local_plans_completed $event event generated when user profile is updated.
     */
    public static function transcripts_user_plan_completed(\local_plans\event\local_plans_completed $event) {
        global $DB, $USER, $CFG;

        if(get_config('local_transcripts', 'enabled')){

            $plan = $DB->get_record('local_plans', array('id'=>$event->objectid));
            $user = $DB->get_record('user', array('id'=>$event->userid));

            if ($plan) {
                $data = new stdClass();
                $data->type = ($plan->type > 0) ? "certification" : "plan";
                $data->status = 2;
                $data->completeddate = time();
                $data->timemodified = time();

                if ($data->type == 'certification'){
                    $transcripts = $DB->get_records('local_transcripts', array('userid'=>$user->id, 'certificationid'=>$plan->id, 'type'=>'certification', 'unenrolldate'=>0));
                } elseif ($data->type == 'plan'){
                    $transcripts = $DB->get_records('local_transcripts', array('userid'=>$user->id, 'learningplanid'=>$plan->id, 'type'=>'plan', 'unenrolldate'=>0));
                }
                if (count($transcripts)){
                    foreach ($transcripts as $transcript){
                        $data->id = $transcript->id;
                        $DB->update_record('local_transcripts', $data);
                    }
                }
            }
        }
    }

}
