<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('classes/forms.php');
require_once('lib.php');

$id         = optional_param('id', 0, PARAM_INT);
$delete     = optional_param('delete', 0, PARAM_BOOL);
$approve    = optional_param('approve', 0, PARAM_BOOL);
$disapprove = optional_param('disapprove', 0, PARAM_BOOL);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/etraining:view', $systemcontext);

// check if plugin is enabled
entraining_enable();

if ($id > 0){
    $title = get_string('edittraining', 'local_etraining');
} else {
    $title = get_string('createtraining', 'local_etraining');
}

$PAGE->set_url(new moodle_url("/local/etraining/edit.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
if (has_capability('local/etraining:manage', $systemcontext)){
    $PAGE->navbar->add(get_string('pluginname', 'local_etraining'), new moodle_url("/local/etraining/index.php"));
} else {
    $PAGE->navbar->add(get_string('myetraining', 'local_etraining'), new moodle_url("/local/etraining/index.php"));
}

$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

if ($id) {
    $training = $DB->get_record('local_etraining', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->url->param('id', $id);
} else {
    $training = new stdClass();
    $training->id = 0;
}

$returnurl = new moodle_url("/local/etraining/index.php");

if ($delete and $training->id) {
    $PAGE->url->param('delete', 1);
    if ($confirm and confirm_sesskey()) {
        $DB->delete_records('local_etraining', array('id'=>$training->id));
        redirect($returnurl);
    }
    
    $strheading = get_string('deletetraining', 'local_etraining');
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit.php', array('id' => $training->id, 'delete' => 1,
        'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => $returnurl->out()));
    $message = get_string('deletetrainingmsg', 'local_etraining', '');
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
    
} elseif ($approve && $training->id && confirm_sesskey()) {
    
    if (!$training->status) {
        $record = (object)array('id' => $training->id, 'status' => 1);
        $DB->update_record('local_etraining', $record);
    }
    redirect($returnurl);
    
} elseif ($disapprove && $training->id && confirm_sesskey()) {
    
    if ($training->status) {
        $record = (object)array('id' => $training->id, 'status' => 0);
        $DB->update_record('local_etraining', $record);
    }
    redirect($returnurl);
} elseif (isset($form->action)  && $training->id && confirm_sesskey()){
    
    if ($form->action == 'reject'){
        $record = (object)array('id' => $training->id, 'status' => 2, 'notes'=>$form->notes);
        $DB->update_record('local_etraining', $record);
    } elseif ($form->action == 'delete') {
        $DB->delete_records('local_etraining', array('id'=>$training->id));
    }
    
    redirect($returnurl);
}

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes);
$training = file_prepare_standard_filemanager($training, 'certificate', $attachmentoptions, $systemcontext, 'local_etraining', 'certificate', (($training->id)?$training->id:null));
$editform = new training_edit_form(null, array('data'=>$training));

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    
    $data->courseid = (isset($data->courseid)) ? $data->courseid : $data->hiddencourse;
    $data->instituteid = (isset($data->instituteid)) ? $data->instituteid : $data->hiddeninstitute;
    $data->timemodified = time();
    
    if ($data->instituteid == '-1' and $data->institutename != ''){
        $institute = new stdClass();
        $institute->name = $data->institutename;
        $institute->userid = $USER->id;
        $institute->status = (get_config('local_etraining', 'confirminstitutes')) ? 0 : 1;
        $institute->timemodified = time();
        $data->instituteid = $DB->insert_record('local_et_institutes', $institute);
    }
    if ($data->courseid == '-1' and $data->coursename != ''){
        $course = new stdClass();
        $course->coursename = $data->coursename;
        $course->coursetype = (isset($data->coursetype)) ? $data->coursetype : 1;
        $course->instituteid = ($data->instituteid) ? $data->instituteid : 0;
        $course->userid = $USER->id;
        $course->status = (get_config('local_etraining', 'confirmcourses')) ? 0 : 1;
        $course->timemodified = time();
        $data->courseid = $DB->insert_record('local_et_courses', $course);
    }
    
    if ($data->id > 0){
        $data->status = ($USER->id == $data->userid) ? 0 : $data->status;
        $DB->update_record('local_etraining', $data);
    } else {
        $data->creator = $USER->id;
        $data->timecreated = time();
        $data->id = $DB->insert_record('local_etraining', $data);
    }
    
    if($data->id){
        $data = file_postupdate_standard_filemanager($data,
            'certificate', $attachmentoptions, $systemcontext, 'local_etraining', 'certificate', $data->id);

        $fs = get_file_storage();
        $files = $fs->get_area_files($systemcontext->id, 'local_etraining', 'certificate', $data->id);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if(!empty($filename) and $filename != '.'){
                $data->certificate = $filename;
            }
        }
        $DB->update_record('local_etraining', $data);
    }

    redirect($returnurl);
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_etraining');
$renderer->etraining_print_tabs('records');

echo html_writer::start_tag('div', array('class'=>'training-form'));
echo $editform->display();
echo html_writer::end_tag('div');

?>
<link href="<?php echo $CFG->wwwroot; ?>/theme/talentquest/style/select2.css" rel="stylesheet" />
<script src="<?php echo $CFG->wwwroot; ?>/theme/talentquest/layout/js/select2.full.min.js"></script>
<script>    
jQuery(window).ready(function(){
    jQuery('#id_userid').select2();
    
    jQuery('#fitem_id_institutename').hide();
    jQuery('#fitem_id_coursename').hide();
    jQuery('#fitem_id_coursetype').hide();
    
    jQuery('#id_instituteid').change(function(e){
        if (jQuery(this).val() == '-1'){
            jQuery('#fitem_id_institutename').show();
        } else {
            if (jQuery(this).val() != ''){
                jQuery.ajax({ 
                     type: "GET",   
                     url: M.cfg.wwwroot+'/local/etraining/ajax.php?action=load-courses&id='+jQuery(this).val(),   
                     beforeSend: function() {
                        jQuery('#id_courseid option').remove();
                        jQuery('#id_courseid').css('opacity', '0.6');
                     },
                     success : function(html){
                         jQuery('#id_courseid').append(html);
                         jQuery('#id_courseid').css('opacity', '1');
                     }
                });
            }
            jQuery('#fitem_id_institutename').hide();
        }
        jQuery('input[name="hiddeninstitute"]').val(jQuery(this).val());
    });
    jQuery('#id_courseid').change(function(e){
        if (jQuery(this).val() == '-1'){
            jQuery('#fitem_id_coursename').show();
            jQuery('#fitem_id_coursetype').show();
        } else {
            jQuery('#fitem_id_coursename').hide();
            jQuery('#fitem_id_coursetype').hide();
        }
        jQuery('input[name="hiddencourse"]').val(jQuery(this).val());
    });
});
</script>
<?php

echo $OUTPUT->footer();
