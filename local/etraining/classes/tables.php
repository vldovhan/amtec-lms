<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * etraining
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class etraining_table extends table_sql {
    function __construct($uniqueid, $search, $status = -1) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        if (has_capability('local/etraining:manage', $systemcontext)){
            $columns = array('username', 'coursename', 'institutename', 'trainingdate', 'score', 'credits', 'certification', 'creatorname', 'certificate', 'notes');
            $headers = array(
                get_string('username', 'local_etraining'),
                get_string('coursename', 'local_etraining'),
                get_string('institute', 'local_etraining'),
                get_string('trainingdate', 'local_etraining'),
                get_string('score', 'local_etraining'),
                get_string('credits', 'local_etraining'),
                get_string('certification', 'local_etraining'),
                get_string('creatorname', 'local_etraining'),
                get_string('certificate', 'local_etraining'),
                get_string('notes', 'local_etraining'));      
        } else {
            $columns = array('coursename', 'institutename', 'trainingdate', 'score', 'credits', 'certification', 'certificate', 'notes');
            $headers = array(
                get_string('coursename', 'local_etraining'),
                get_string('institute', 'local_etraining'),
                get_string('trainingdate', 'local_etraining'),
                get_string('score', 'local_etraining'),
                get_string('credits', 'local_etraining'),
                get_string('certification', 'local_etraining'), 
                get_string('certificate', 'local_etraining'),
                get_string('notes', 'local_etraining'));    
        }

        $columns[] = 'status';
        $headers[] = get_string('status', 'local_etraining');

        if (has_capability('local/etraining:manage', $systemcontext) or get_config('local_etraining', 'create')){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_etraining');
        }
        
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        
        $sql_search = ($search) ? " AND (c.coursename LIKE '%$search%' OR i.name LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%' OR p.name LIKE '%$search%' OR uс.firstname LIKE '%$search%' OR uс.lastname LIKE '%$search%' OR t.instructor LIKE '%$search%')" : "";
        
        $fields = "t.*, c.coursename, i.name as institutename, p.name as certification, CONCAT(u.firstname, ' ', u.lastname) as username, CONCAT(uс.firstname, ' ', uс.lastname) as creatorname";
        $from = "{local_etraining} t 
                    LEFT JOIN {local_et_courses} c ON c.id = t.courseid
                    LEFT JOIN {local_et_institutes} i ON i.id = t.instituteid
                    LEFT JOIN {local_plans} p ON p.id = t.planid
                    LEFT JOIN {user} u ON u.id = t.userid 
                    LEFT JOIN {user} uс ON uс.id = t.creator ";
        $where = 't.id > 0'.$sql_search;
        
        if (!has_capability('local/etraining:manage', $systemcontext)){
            $where .= ' AND t.userid = '.$USER->id;
        }
        
        if(!is_siteadmin() and has_capability('local/manager:seeonlyownusers', context_system::instance())){
            $where .= " AND u.managerid = $USER->id";
        }
        
        if ($status >= 0){
            $where .= ' AND t.status = '.$status;
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/etraining/index.php?status=".$status."&search=".$search);
    }
    function col_trainingdate($values) {
      return ($values->trainingdate) ? date('m/d/Y', $values->trainingdate) : '-';
    }
    
    function col_status($values) {
        if ($values->status > 1){
            return get_string('rejected', 'local_etraining');
        } elseif ($values->status > 0){
            return get_string('confirmed', 'local_etraining');
        } else {
            return get_string('pending', 'local_etraining');
        }
    }
    
    function col_certificate($values) {
    	global $CFG, $OUTPUT, $USER;

       	$buttons = array();
        $urlparams = array('id' => $values->id);
        
        if($values->certificate){
            $url = moodle_url::make_pluginfile_url(context_system::instance()->id,'local_etraining', 'certificate', $values->id,'/', $values->certificate);
            $buttons[] = html_writer::link(new moodle_url($url),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/backup'), 'alt' => $values->certificate, 'class' => 'iconsmall')),
            array('title' => $values->certificate, 'target'=>'_blank'));
        }
        
	    return implode(' ', $buttons);
    }
    
    function col_actions($values) {
    	global $CFG, $OUTPUT, $USER;

       	$buttons = array();
        
        $systemcontext   = context_system::instance();
        if (!has_capability('local/etraining:manage', $systemcontext) and $values->status == 1){
            return '';
        }

        $urlparams = array('id' => $values->id);
        
        if (has_capability('local/etraining:manage', $systemcontext) and get_config('local_etraining', 'confirm')){
            if ($values->status != 1) {
                $showhideurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit.php', $urlparams + array('sesskey' => sesskey()));
                $showhideurl->param('approve', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/approve'), 'alt' => get_string('approve', 'local_etraining'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve', 'local_etraining')));
            }
            if ($values->status == 0) {
                $showhideurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit.php', $urlparams + array('sesskey' => sesskey()));
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('reject', 'local_etraining'), 'class' => 'iconsmall'));
                $form = '<form action=\"'.$showhideurl.'\" method=\"POST\">';
                $form .= '<div class=\"form-element-box clearfix\"><div class=\"form-element-title\">'.get_string('actions', 'local_etraining').'</div><div class=\"form-element-field\"><label><input type=\"radio\" name=\"form[action]\" value=\"reject\" checked>'.get_string('rejectactivity', 'local_etraining').'</label><label><input type=\"radio\" name=\"form[action]\" value=\"delete\">'.get_string('deleteactivity', 'local_etraining').'</label></div></div>';
                $form .= '<div class=\"form-element-box clearfix\"><div class=\"form-element-title\">'.get_string('notes', 'local_etraining').'</div><div class=\"form-element-field\"><textarea placeholder=\"'.get_string('rejectmessage', 'local_etraining').'\" name=\"form[notes]\">'.$values->notes.'</textarea></div></div>';
                $form .= '<div class=\"submitbuttons\"><input type=\"submit\" value=\"'.get_string('submit').'\" class=\"submitbutton\" name=\"submitbutton\"><input type=\"button\" value=\"'.get_string('cancel').'\" class=\"addcancel\" name=\"addcancel\" onclick=\"closeCustomPopup(\'reject_'.$values->id.'\');\"></div>';
                $form .= '</form>';
                $buttons[] = html_writer::link('javascript:void(0);', $visibleimg, array('title' => get_string('reject', 'local_etraining'), 'onclick'=>'openCustomPopup("reject_'.$values->id.'", "'.get_string('rejectdelete', 'local_etraining').'", "'.$form.'");', ));
            }
        }
        
        if (has_capability('local/etraining:manage', $systemcontext) or get_config('local_etraining', 'create')){
            
            $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit.php', $urlparams),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
                array('title' => get_string('edit')));
            
            if (has_capability('local/etraining:manage', $systemcontext) and ((get_config('local_etraining', 'confirm') and $values->status > 0) or !get_config('local_etraining', 'confirm') or $values->status != 0)){
                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit.php', $urlparams + array('delete' => 1)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete')));     
            } elseif (!has_capability('local/etraining:manage', $systemcontext) and $values->status == 0 or $values->status == 2){
                $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit.php', $urlparams + array('delete' => 1)),
                html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
                array('title' => get_string('delete'))); 
            }
            $editcolumnisempty = false;
        }
        
	    return implode(' ', $buttons);
    }
}

class courses_table extends table_sql {
    function __construct($uniqueid, $search, $status = -1) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $columns = array('coursename', /*'coursetype',*/ 'institute', 'username');
        $headers = array(
            get_string('name', 'local_etraining'),
            /*get_string('coursetype', 'local_etraining'),*/
            get_string('institute', 'local_etraining'),
            get_string('username', 'local_etraining'));
        
        
        $columns[] = 'status';
        $headers[] = get_string('status', 'local_etraining');
        
        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_etraining');
        
       
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (c.coursename LIKE '%$search%' OR i.name LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')" : "";
        $fields = "c.*, CONCAT(u.firstname, ' ', u.lastname) as username, c.id as actions, i.name as institute";
        $from = "{local_et_courses} c LEFT JOIN {local_et_institutes} i ON i.id = c.instituteid LEFT JOIN {user} u ON u.id = c.userid ";
        $where = 'c.id > 0'.$sql_search;
        
        if ($status >= 0){
            $where .= ' AND c.status = '.$status;
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/etraining/courses.php?status=".$status."&search=".$search);
    }
    
    function col_status($values) {
        if ($values->status > 0){
            return get_string('confirmed', 'local_etraining');
        } else {
            return get_string('pending', 'local_etraining');
        }
    }
    
    /*function col_coursetype($values) {
        return ($values->coursetype > 0) ? get_string('coursetype'.$values->coursetype, 'local_etraining') : '-';
    }*/
    
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $values->id);
        
        if (get_config('local_etraining', 'confirmcourses')){
            if ($values->status) {
                $showhideurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit_course.php', $urlparams + array('sesskey' => sesskey()));
                $showhideurl->param('disapprove', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('disapprove', 'local_etraining'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('disapprove', 'local_etraining')));
            } else {
                $showhideurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit_course.php', $urlparams + array('sesskey' => sesskey()));
                $showhideurl->param('approve', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'alt' => get_string('approve', 'local_etraining'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve', 'local_etraining')));
            }
        }
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit_course.php', $urlparams + array('delete' => 1)),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit_course.php', $urlparams),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
            array('title' => get_string('edit')));
        $editcolumnisempty = false;
       
	    return implode(' ', $buttons);
    }
}



class institutes_table extends table_sql {
    function __construct($uniqueid, $search, $status = -1) {
		global $CFG, $USER;

        parent::__construct($uniqueid);
        $systemcontext   = context_system::instance();
        
        $columns = array('name', 'username');
        $headers = array(
            get_string('name', 'local_etraining'),
            get_string('username', 'local_etraining'),
            get_string('status', 'local_etraining'),
            get_string('actions', 'local_etraining'));
        
        
        $columns[] = 'status';
        $headers[] = get_string('status', 'local_etraining');
        
        $columns[] = 'actions';
        $headers[] = get_string('actions', 'local_etraining');
        
       
        $this->define_columns($columns);
        
        $this->define_headers($headers);
        $sql_search = ($search) ? " AND (i.name LIKE '%$search%')" : "";
        $fields = "i.*, CONCAT(u.firstname, ' ', u.lastname) as username, i.id as actions";
        $from = "{local_et_institutes} i LEFT JOIN {user} u ON u.id = i.userid ";
        $where = 'i.id > 0'.$sql_search;
        
        if ($status >= 0){
            $where .= ' AND i.status = '.$status;
        }
        
        $this->set_sql($fields, $from, $where, array());
        $this->define_baseurl("$CFG->wwwroot/local/etraining/institutes.php?status=".$status."&search=".$search);
    }
    
    function col_status($values) {
        if ($values->status > 0){
            return get_string('confirmed', 'local_etraining');
        } else {
            return get_string('pending', 'local_etraining');
        }
    }
    
    function col_actions($values) {
    	global $CFG, $OUTPUT;

       	$buttons = array();

        $urlparams = array('id' => $values->id);
        
        if (get_config('local_etraining', 'confirminstitutes')){
            if ($values->status) {
                $showhideurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit_institute.php', $urlparams + array('sesskey' => sesskey()));
                $showhideurl->param('disapprove', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/hide'), 'alt' => get_string('disapprove', 'local_etraining'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('disapprove', 'local_etraining')));
            } else {
                $showhideurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit_institute.php', $urlparams + array('sesskey' => sesskey()));
                $showhideurl->param('approve', 1);
                $visibleimg = html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/show'), 'alt' => get_string('approve', 'local_etraining'), 'class' => 'iconsmall'));
                $buttons[] = html_writer::link($showhideurl, $visibleimg, array('title' => get_string('approve', 'local_etraining')));
            }
        }
        
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit_institute.php', $urlparams + array('delete' => 1)),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/delete'), 'alt' => get_string('delete'), 'class' => 'iconsmall')),
            array('title' => get_string('delete')));
        $buttons[] = html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit_institute.php', $urlparams),
            html_writer::empty_tag('img', array('src' => $OUTPUT->pix_url('t/edit'), 'alt' => get_string('edit'), 'class' => 'iconsmall')),
            array('title' => get_string('edit')));
        $editcolumnisempty = false;
       
	    return implode(' ', $buttons);
    }
}
