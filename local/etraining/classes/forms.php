<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot . '/local/etraining/lib.php');

class institution_edit_form extends moodleform {

    /**
     * Define the plan edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $data = $this->_customdata['data'];

        $mform->addElement('text', 'name', get_string('name'), 'maxlength="254" size="50"');
        $mform->addRule('name', get_string('required'), 'required', null, 'client');
        $mform->setType('name', PARAM_TEXT);
        
        if (get_config('local_etraining', 'confirminstitutes')){
            $options = array(1=>get_string('confirmed', 'local_etraining'), 0=>get_string('pending', 'local_etraining'));
            $mform->addElement('select', 'status', get_string('status', 'local_etraining'), $options);
            $mform->setType('status', PARAM_INT);
        } else {
            $mform->addElement('hidden', 'status');
            $mform->setType('status', PARAM_INT);
            $mform->setDefault('status', 0);
        }

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

class course_edit_form extends moodleform {

    /**
     * Define the plan edit form
     */
    public function definition() {
        global $CFG;

        $mform = $this->_form;
        $data = $this->_customdata['data'];

        $mform->addElement('text', 'coursename', get_string('name'), 'maxlength="254" size="50"');
        $mform->addRule('coursename', get_string('required'), 'required', null, 'client');
        $mform->setType('coursename', PARAM_TEXT);
        
        if (get_config('local_etraining', 'confirmcourses')){
            $options = array(1=>get_string('confirmed', 'local_etraining'), 0=>get_string('pending', 'local_etraining'));
            $mform->addElement('select', 'status', get_string('status', 'local_etraining'), $options);
            $mform->setType('status', PARAM_INT);
        } else {
            $mform->addElement('hidden', 'status');
            $mform->setType('status', PARAM_INT);
            $mform->setDefault('status', 0);
        }
        
        /*$options = array(''=>'', 1=>get_string('coursetype1', 'local_etraining'), 2=>get_string('coursetype2', 'local_etraining'));
        $mform->addElement('select', 'coursetype', get_string('coursetype', 'local_etraining'), $options);
        $mform->addRule('coursetype', get_string('required'), 'required', null, 'client');
        $mform->setType('coursetype', PARAM_INT);*/
        
        $options = array(''=>'')+etraining_get_intituteslist();
        $mform->addElement('select', 'instituteid', get_string('institute1', 'local_etraining'), $options);
        $mform->addRule('instituteid', get_string('required'), 'required', null, 'client');
        $mform->setType('instituteid', PARAM_INT);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}

class training_edit_form extends moodleform {

    /**
     * Define the plan edit form
     */
    public function definition() {
        global $CFG, $USER;
        
        $mform = $this->_form;
        $data = $this->_customdata['data'];

        if (has_capability('local/etraining:manage', context_system::instance())){
            $options = array(''=>'')+etraining_get_userslist(true);
            $mform->addElement('select', 'userid', get_string('username', 'local_etraining'), $options);
            $mform->addRule('userid', get_string('required'), 'required', null, 'client');
            $mform->setType('userid', PARAM_INT);
        } else {
            $mform->addElement('hidden', 'userid', $USER->id);
            $mform->setType('userid', PARAM_INT);
        }
        
        $options = array(''=>'')+etraining_get_intituteslist(0, true);
        if (!get_config('local_etraining', 'createown') or has_capability('local/etraining:manage', context_system::instance())){
            $options += array('-1'=>get_string('other', 'local_etraining'));
        }
        $mform->addElement('select', 'instituteid', get_string('institute1', 'local_etraining'), $options);
        $mform->addRule('instituteid', get_string('required'), 'required', null, 'client');
        $mform->setType('instituteid', PARAM_INT);
        
        $mform->addElement('text', 'institutename', get_string('institutename', 'local_etraining'));
        $mform->setType('institutename', PARAM_TEXT);
        
        if (isset($data->instituteid)){
            $options = array(''=>'')+etraining_get_courseslist($data->instituteid, true);
        } else {
            $options = array(''=>'');   
        }
        
        if (!get_config('local_etraining', 'createown') or has_capability('local/etraining:manage', context_system::instance())){
            $options += array('-1'=>get_string('other', 'local_etraining'));
        }
        $mform->addElement('select', 'courseid', get_string('course', 'local_etraining'), $options);
        $mform->addRule('courseid', get_string('required'), 'required', null, 'client');
        $mform->setType('courseid', PARAM_INT);
        
        $mform->addElement('text', 'coursename', get_string('coursename', 'local_etraining'));
        $mform->setType('coursename', PARAM_TEXT);
        
        /*$options = array(''=>'', 1=>get_string('coursetype1', 'local_etraining'), 2=>get_string('coursetype2', 'local_etraining'));
        $mform->addElement('select', 'coursetype', get_string('coursetype', 'local_etraining'), $options);
        $mform->setType('coursetype', PARAM_INT);*/
        
        $mform->addElement('date_selector', 'trainingdate', get_string('trainingdate', 'local_etraining'));
        $mform->addRule('trainingdate', get_string('required'), 'required', null, 'client');
        
        $mform->addElement('text', 'score', get_string('score', 'local_etraining'));
        $mform->setType('score', PARAM_INT);
        
        $mform->addElement('text', 'credits', get_string('credits', 'local_etraining'));
        $mform->setType('credits', PARAM_TEXT);
        
        $options = array(''=>'')+etraining_get_certificationslist(true);
        $mform->addElement('select', 'planid', get_string('certifications', 'local_etraining'), $options);
        $mform->setType('planid', PARAM_INT);
        
        $mform->addElement('text', 'instructor', get_string('instructor', 'local_etraining'));
        $mform->setType('instructor', PARAM_TEXT);
        
        $mform->addElement('date_selector', 'renewaldate', get_string('renewaldate', 'local_etraining'), array('optional'=>true));
        
        if (has_capability('local/etraining:manage', context_system::instance()) and get_config('local_etraining', 'confirm')){
            $options = array(1=>get_string('confirmed', 'local_etraining'), 2=>get_string('rejected', 'local_etraining'), 0=>get_string('pending', 'local_etraining'));
            $mform->addElement('select', 'status', get_string('status', 'local_etraining'), $options);
            $mform->setType('status', PARAM_INT);
        } else {
            $mform->addElement('hidden', 'status');
            $mform->setType('status', PARAM_INT);
            //if (get_config('local_etraining', 'confirm')){
                $mform->setDefault('status', 0);
            /*} else {
                $mform->setDefault('status', 1);
            }*/
        }
        
        $attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes);
        $mform->addElement('filemanager', 'certificate_filemanager', get_string('certificate', 'local_etraining'), null, $attachmentoptions);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'hiddencourse');
        $mform->setType('hiddencourse', PARAM_INT);
        
        $mform->addElement('hidden', 'hiddeninstitute');
        $mform->setType('hiddeninstitute', PARAM_INT);
        
        $this->add_action_buttons();

        $this->set_data($data);
    }

    public function validation($data, $files) {
        global $DB;

        return parent::validation($data, $files);
    }
}


class import_form1 extends moodleform {
    function definition () {
        $mform = $this->_form;

        $mform->addElement('header', 'settingsheader', get_string('upload'));
        
        $mform->addElement('static', 'template', get_string('importtemplate', 'local_etraining'), '<a href="sample-import.csv" style="color:#555;font:14px Roboto;"><i class="fa fa-download"></i> '.get_string('download').'</a>');
        
        $mform->addElement('filepicker', 'importfile', get_string('file'));
        $mform->addRule('importfile', null, 'required');

        $choices = csv_import_reader::get_delimiter_list();
        $mform->addElement('select', 'delimiter_name', get_string('csvdelimiter', 'tool_uploaduser'), $choices);
        if (array_key_exists('cfg', $choices)) {
            $mform->setDefault('delimiter_name', 'cfg');
        } else if (get_string('listsep', 'langconfig') == ';') {
            $mform->setDefault('delimiter_name', 'semicolon');
        } else {
            $mform->setDefault('delimiter_name', 'comma');
        }

        $choices = core_text::get_encodings();
        $mform->addElement('select', 'encoding', get_string('encoding', 'tool_uploaduser'), $choices);
        $mform->setDefault('encoding', 'UTF-8');

        $choices = array('10'=>10, '20'=>20, '100'=>100, '1000'=>1000, '100000'=>100000);
        $mform->addElement('select', 'previewrows', get_string('rowpreviewnum', 'tool_uploaduser'), $choices);
        $mform->setType('previewrows', PARAM_INT);

        $this->add_action_buttons(false, get_string('import', 'local_etraining'));
    }
}


/**
 * Specify user upload details
 *
 * @copyright  2007 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class import_form2 extends moodleform {
    function definition () {
        global $CFG, $USER;
        
        $mform   = $this->_form;
        $columns = $this->_customdata['columns'];
        $data    = $this->_customdata['data'];
        
        // hidden fields
        $mform->addElement('hidden', 'iid');
        $mform->setType('iid', PARAM_INT);

        $mform->addElement('hidden', 'previewrows');
        $mform->setType('previewrows', PARAM_INT);

        $this->add_action_buttons(true, get_string('import', 'local_etraining'));

        $this->set_data($data);
    }

    /**
     * Server side validation.
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        $columns = $this->_customdata['columns'];
        
        return $errors;
    }

}