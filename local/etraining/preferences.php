<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

$config       = optional_param('config', '', PARAM_RAW);
$action       = optional_param('action', '', PARAM_RAW);
$plugin       = optional_param('plugin', '', PARAM_RAW);
$state        = optional_param('state', 0, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/etraining:settings', $systemcontext);
$title = get_string('settings', 'local_etraining');

// check if plugin is enabled
entraining_enable();

if ($action == 'set_config'){
    set_config($config, $state, $plugin);
    
    if ($plugin == 'local_gamification'){
        $CFG->enablebadges = $state;
        set_config('enablebadges', $state);
    }
    
    if ($config == 'createown'){
        $state = ($state) ? 0 : 1;
        set_config('confirmcourses', $state, $plugin);
        set_config('confirminstitutes', $state, $plugin);
    }
    
    echo '1';
    exit;
}

$PAGE->set_url(new moodle_url("/local/etraining/preferences.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add(get_string('pluginname', 'local_etraining'), new moodle_url("/local/etraining/index.php"));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_etraining');
$renderer->etraining_print_tabs('settings');

echo $renderer->etraining_print_settings();


echo $OUTPUT->footer();