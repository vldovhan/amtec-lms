<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/csvlib.class.php');
require ('classes/forms.php');
require_once('importlib.php');
require_once('lib.php');

$iid         = optional_param('iid', '', PARAM_INT);
$previewrows = optional_param('previewrows', 10, PARAM_INT);

@set_time_limit(60*60); // 1 hour should be enough
raise_memory_limit(MEMORY_HUGE);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/etraining:import', $systemcontext);
$title = get_string('import', 'local_etraining');

// check if plugin is enabled
entraining_enable();

$strrecordadded             = get_string('recordadded', 'local_etraining');
$strrecordnotaddederror     = get_string('recordnotaddederror', 'local_etraining');
$errorstr                   = get_string('error');
$stryes                     = get_string('yes');
$strno                      = get_string('no');

$PAGE->set_url(new moodle_url("/local/etraining/index.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
if (has_capability('local/etraining:manage', $systemcontext)){
    $PAGE->navbar->add(get_string('pluginname', 'local_etraining'), new moodle_url("/local/etraining/index.php"));
} else {
    $PAGE->navbar->add(get_string('myetraining', 'local_etraining'), new moodle_url("/local/etraining/index.php"));
}
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$returnurl = new moodle_url('/local/etraining/import.php');

$today = time();
$today = make_timestamp(date('Y', $today), date('m', $today), date('d', $today), 0, 0, 0);

// array of all valid fields for validation
$FIELDS = array('user id', 'course name', /*'course type', */
                'training institute', 'start date', 
                'end date', 'score', 'credits'
            );

if (empty($iid)) {
    $mform1 = new import_form1();

    if ($formdata = $mform1->get_data()) {
        $iid = csv_import_reader::get_new_iid('importfile');
        $cir = new csv_import_reader($iid, 'importfile');

        $content = $mform1->get_file_content('importfile');

        $readcount = $cir->load_csv_content($content, $formdata->encoding, $formdata->delimiter_name);
        $csvloaderror = $cir->get_error();
        unset($content);

        if (!is_null($csvloaderror)) {
            print_error('csvloaderror', '', $returnurl, $csvloaderror);
        }
        // test if columns ok
        $filecolumns = uu_validate_file_upload_columns($cir, $FIELDS, $returnurl);
        // continue to form2
    } else {
        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('import', 'local_etraining'));

        $mform1->display();
        echo $OUTPUT->footer();
        die;
    }
} else {
    $cir = new csv_import_reader($iid, 'importfile');
    $filecolumns = uu_validate_file_upload_columns($cir, $FIELDS, $returnurl);
}

$mform2 = new import_form2(null, array('columns'=>$filecolumns, 'data'=>array('iid'=>$iid, 'previewrows'=>$previewrows)));

// If a file has been uploaded, then process it
if ($formdata = $mform2->is_cancelled()) {
    $cir->cleanup(true);
    redirect($returnurl);

} else if ($formdata = $mform2->get_data()) {
    // Print the header
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('importresult', 'local_etraining'));
    
    $recordsnew      = 0;
    $recordserrors   = 0;
    
    // init csv import helper
    $cir->init();
    $linenum = 1; //column header is first line

    // init upload progress tracker
    $upt = new uu_progress_tracker();
    $upt->start(); // start table

    while ($line = $cir->next()) {
        $upt->flush();
        $linenum++;

        $upt->track('line', $linenum);

        $record = new stdClass();
        

        // add fields to user object
        foreach ($line as $keynum => $value) {
            if (!isset($filecolumns[$keynum])) {
                // this should not happen
                continue;
            }
            $key = $filecolumns[$keynum];
            switch ($key) {
                case 'user id':
                    $record->uid = ($value != '') ? trim($value) : '';
                    break;
                case 'course name':
                    $record->coursename = ($value != '') ? trim($value) : '';
                    break;
                /*case 'course type':
                    if (trim($value) == 'elearning'){
                        $value = 1;
                    } elseif (trim($value) == 'classroom'){
                        $value = 2;
                    } else {
                        $value = 0;
                    }
                    $record->coursetype = $value;
                    break;*/
                case 'training institute':
                    $record->institutename = ($value != '') ? trim($value) : '';
                    break;
                case 'start date':
                    $record->trainingdate = ($value != '') ? strtotime(trim($value)) : '';
                    break;
                case 'end date':
                    $record->renewaldate = ($value != '') ? strtotime(trim($value)) : '';
                    break;
                case 'score':
                    $record->score = ($value != '') ? trim($value) : '';
                    break;
                case 'credits':
                    $record->credits = ($value != '') ? trim($value) : '';
                    break;
                default:
                    $record->$key = trim($value);       
            }

            if (in_array($key, $upt->columns)) {
                // default value in progress tracking table, can be changed later
                $upt->track($key, s($value), 'normal');
            }
        }
        
        
        if (empty($record->uid)) {
            $upt->track('status', get_string('missingfield', 'error', 'USER ID'), 'error');
            $upt->track('USER ID', $errorstr, 'error');
            $recordserrors++;
            continue;
        }
        
        if (empty($record->coursename)) {
            $upt->track('status', get_string('missingfield', 'error', 'coursename'), 'error');
            $upt->track('COURSENAME', $errorstr, 'error');
            $recordserrors++;
            continue;
        }

        if (!$user = $DB->get_record('user', array('idnumber'=>$record->uid))) {
            $upt->track('USER ID', $errorstr, 'error');
        }
        
        if ($user){
            
            $record->instituteid = etraining_import_institute($record->institutename);
            $record->courseid = etraining_import_course($record->coursename, $record->instituteid);
            
            $record->userid = $user->id;
            $record->creator = $USER->id;
            $record->timecreated = time();
            $record->timemodified = time();
            $record->status = 0;
            
            $record->id = $DB->insert_record('local_etraining', $record);
            
            $upt->track('status', $strrecordadded);
            $upt->track('uid', $record->uid, 'normal', false);
            $recordsnew++;
            
        } else {
            $upt->track('status', get_string('usernotexists', 'local_etraining'), 'error');
            $recordserrors++;
        }        
    }
    $upt->close(); // close table

    $cir->close();
    $cir->cleanup(true);

    echo $OUTPUT->box_start('boxwidthnarrow boxaligncenter generalbox', 'uploadresults');
    echo '<p>';
    
    echo get_string('recordscreated', 'local_etraining').': '.$recordsnew.'<br />';
    echo get_string('recordserrors', 'local_etraining').': '.$recordserrors.'</p>';
    echo $OUTPUT->box_end();

    echo $OUTPUT->continue_button($returnurl);

    echo $OUTPUT->footer();
    die;
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_etraining');
$renderer->etraining_print_tabs('records');


// NOTE: this is JUST csv processing preview, we must not prevent import from here if there is something in the file!!
//       this was intended for validation of csv formatting and encoding, not filtering the data!!!!
//       we definitely must not process the whole file!

// preview table data
$data = array();
$cir->init();
$linenum = 1; //column header is first line
$noerror = true; // Keep status of any error.
while ($linenum <= $previewrows and $fields = $cir->next()) {
    $linenum++;
    $rowcols = array();
    $rowcols['line'] = $linenum;
    foreach($fields as $key => $field) {
        if (!isset($filecolumns[$key])) continue;
        $rowcols[$filecolumns[$key]] = s(trim($field));
    }
    $rowcols['status'] = array();
    if (!isset($rowcols['user id']) or (isset($rowcols['user id']) and $rowcols['user id'] == '')) {
        $rowcols['status'][] = get_string('missingid', 'local_etraining');
    }
    if (!isset($rowcols['course name']) or (isset($rowcols['course name']) and $rowcols['course name'] == '')) {
        $rowcols['status'][] = get_string('missingcoursename', 'local_etraining');
    }
    if (!count($rowcols['status'])){
        $rowcols['status'][] = get_string('canimport', 'local_etraining');
    }
    $rowcols['status'] = implode('<br />', $rowcols['status']);
    $data[] = $rowcols;
}
if ($fields = $cir->next()) {
    $data[] = array_fill(0, count($filecolumns) + 3, '...');
}
$cir->close();

$table = new html_table();
$table->id = "uupreview";
$table->attributes['class'] = 'generaltable';
$table->tablealign = 'center';
$table->summary = get_string('uploaduserspreview', 'tool_uploaduser');
$table->head = array();
$table->data = $data;

$table->head[] = get_string('uucsvline', 'tool_uploaduser');
foreach ($filecolumns as $column) {
    $table->head[] = $column;
}
$table->head[] = get_string('status');

echo html_writer::tag('div', html_writer::table($table), array('class'=>'flexible-wrap', 'style'=>'overflow:auto; margin-bottom:25px;'));

// Print the form if valid values are available
if ($noerror) {
    $mform2->display();
}
echo $OUTPUT->footer();