<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Gamification version file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require('../../config.php');
require ('classes/tables.php');
require_once('lib.php');

$search     = optional_param('search', '', PARAM_RAW);
$action     = optional_param('action', '', PARAM_RAW);
$id         = optional_param('id', 0, PARAM_INT);
$status     = optional_param('status', -1, PARAM_INT);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/etraining:view', $systemcontext);

if (has_capability('local/etraining:manage', $systemcontext)){
    $title = get_string('pluginname', 'local_etraining');
} else {
    $title = get_string('myetraining', 'local_etraining');
}

// check if plugin is enabled
entraining_enable();

$PAGE->set_url(new moodle_url("/local/etraining/index.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

$table = new etraining_table('table', $search, $status);
$table->is_collapsible = false;

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_etraining');
$renderer->etraining_print_tabs('records');

echo html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'etraining-search-form'));

if (get_config('local_etraining', 'confirm')){
    echo html_writer::start_tag("label", array('style'=>'margin-right: 5px;'));
    echo etraining_get_status_filter($status, true);
    echo html_writer::end_tag("label");
}

echo html_writer::start_tag("label",  array());
echo html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_etraining').' ...', 'value' => $search));
//echo html_writer::empty_tag('input', array('type' => 'submit', 'value' => get_string('filter', 'local_etraining')));
echo html_writer::end_tag("label");

if (has_capability('local/etraining:import', $systemcontext)){
    echo html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/import.php'), html_writer::tag('i', '', array('class'=>'fa fa-download')).get_string('import', 'local_etraining'), array('class'=>'btn btn-warning'));
}

if (has_capability('local/etraining:manage', $systemcontext) or get_config('local_etraining', 'create')){
    echo html_writer::link(new moodle_url($CFG->wwwroot.'/local/etraining/edit.php'), html_writer::tag('i', '', array('class'=>'fa fa-plus')).get_string('createnew', 'local_etraining'), array('class'=>'btn btn-warning'));
}

echo html_writer::end_tag("form");


$table->out(20, true);

echo $OUTPUT->footer();
