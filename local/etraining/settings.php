<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * etraining version file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die;

$settings = new admin_settingpage('local_etraining', get_string('pluginname', 'local_etraining'));
$ADMIN->add('localplugins', $settings);

if (get_config('local_etraining', 'enabled')){

    $ADMIN->add('root', new admin_category('etrainingroot', get_string('pluginname', 'local_etraining')));

    $ADMIN->add('etrainingroot', new admin_externalpage('erecords', get_string('records', 'local_etraining'), $CFG->wwwroot.'/local/etraining/index.php', 'local/etraining:view'));
    $ADMIN->add('etrainingroot', new admin_externalpage('ecourses', get_string('courses', 'local_etraining'), $CFG->wwwroot.'/local/etraining/courses.php', 'local/etraining:view'));
    $ADMIN->add('etrainingroot', new admin_externalpage('einstitutes', get_string('institutes', 'local_etraining'), $CFG->wwwroot.'/local/etraining/institutes.php', 'local/etraining:view'));
    
}

$name = 'local_etraining/enabled';
$title = get_string('enabled', 'local_etraining');
$description = get_string('enabled_desc', 'local_etraining');
$default = true;
$setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
$settings->add($setting);
