<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

$string['pluginname'] = 'External Training';
$string['search'] = 'Search'; 
$string['filter'] = 'Filter'; 
$string['enabled'] = 'Enabled External Training';
$string['enabled_desc'] = 'Enable External Training';
$string['name'] = 'Name'; 
$string['username'] = 'User Name'; 
$string['coursename'] = 'Course Name'; 
$string['certification'] = 'Certification & Retraining'; 
$string['grade'] = 'Grade'; 
$string['status'] = 'Status'; 
$string['actions'] = 'Actions'; 
$string['action'] = 'Action'; 
$string['selectuser'] = 'Select user...'; 
$string['createnew'] = 'Create New'; 
$string['records'] = 'External Training Records'; 
$string['courses'] = 'External Training Courses'; 
$string['institutes'] = 'External Training Institutes'; 
$string['institute'] = 'Training Institutes'; 
$string['pending'] = 'Pending'; 
$string['confirmed'] = 'Confirmed'; 
$string['showall'] = 'All'; 
$string['editinstitutes'] = 'Edit Training Institute'; 
$string['createinstitutes'] = 'Create Training Institute'; 
$string['approve'] = 'Approve'; 
$string['disapprove'] = 'Disapprove'; 
$string['deleteinstinute'] = 'Delete Instinute'; 
$string['deleteinstinutemsg'] = 'Do you really want to delete instinute \'{$a}\'?';
$string['editcourses'] = 'Edit course'; 
$string['createcourses'] = 'Add New Course'; 
$string['deletecourse'] = 'Delete Course'; 
$string['deletecoursemsg'] = 'Do you really want to delete course \'{$a}\'?';
$string['coursetype'] = 'Course Type';
$string['coursetype1'] = 'E-Learning';
$string['coursetype2'] = 'Classroom';
$string['tabrecords'] = 'External Training Activities';
$string['tabcourses'] = 'Courses';
$string['tabinstitutes'] = 'Training Institutes';
$string['edittraining'] = 'Edit Activity'; 
$string['createtraining'] = 'Add New Activity'; 
$string['deletetraining'] = 'Delete Activity'; 
$string['deletetrainingmsg'] = 'Do you really want to delete training \'{$a}\'?';
$string['other'] = 'Other';
$string['institutename'] = 'Training Institute Name';
$string['course'] = 'Course';
$string['trainingdate'] = 'Training Date';
$string['score'] = 'Score';
$string['credits'] = 'Credits (CEUs)';
$string['certifications'] = 'Certifications & Retraining';
$string['instructor'] = 'Instructor';
$string['renewaldate'] = 'Renewal Date';
$string['institute1'] = 'Training Institute'; 
$string['certificate'] = 'Certificate'; 
$string['creatorname'] = 'Created by'; 
$string['import'] = 'Import'; 
$string['importresult'] = 'Import results'; 
$string['importtemplate'] = 'Import Template'; 
$string['missingcoursename'] = 'Missing Course Name'; 
$string['missingid'] = 'Missing User ID'; 
$string['id'] = 'User ID'; 
$string['userid'] = 'User ID'; 
$string['canimport'] = 'Can import'; 
$string['usernotexists'] = 'User not exists in system'; 
$string['recordadded'] = 'Records created'; 
$string['recordnotaddederror'] = 'Records not created'; 
$string['recordserrors'] = 'Import errors'; 
$string['recordscreated'] = 'Records created'; 
$string['strrecordadded'] = 'Record created'; 
$string['returntohome'] = 'Return to Dashboard'; 
$string['etraining:manage'] = 'External Training Manage';  
$string['etraining:view'] = 'External Training View'; 
$string['etraining:managecourses'] = 'External Training Manage Courses'; 
$string['etraining:manageinstitutes'] = 'External Training Manage Institutes'; 
$string['etraining:settings'] = 'External Training Settings'; 
$string['etraining:import'] = 'Import External Training Activities'; 
$string['settings'] = 'Settings'; 
$string['confirmsettings'] = 'Require approval for all external training activities submitted by Learners'; 
$string['confirmcourses'] = 'Put Courses added by users in master list as pending for admin approval'; 
$string['confirminstitutes'] = 'Put Training Institutes added by users in master list as pending for admin approval'; 
$string['createownsettings'] = 'Require users to select Training Institutes and Courses from master list'; 
$string['createsettings'] = 'Allow users to add their own external training activities'; 
$string['createnewcourse'] = 'Add New Course'; 
$string['createnewinstitute'] = 'Add New Training Institute'; 
$string['myetraining'] = 'My External Training'; 
$string['notes'] = 'Notes'; 
$string['reject'] = 'Reject'; 
$string['rejected'] = 'Rejected'; 
$string['rejectmessage'] = 'Add message for the user'; 
$string['rejectdelete'] = 'Reject/delete activity'; 
$string['rejectactivity'] = 'Reject activity'; 
$string['deleteactivity'] = 'Delete activity permanently'; 
