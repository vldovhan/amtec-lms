<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

/**
 * Standard HTML output renderer for badges
 */
class local_etraining_renderer extends plugin_renderer_base {

    // Prints tabs
    public function etraining_print_tabs($current = 'records') {
        global $DB;

        $systemcontext   = context_system::instance();
        $row = array();

        $row[] = new tabobject('records',
                    new moodle_url('/local/etraining/index.php'),
                    get_string('tabrecords', 'local_etraining')
                );

        if (has_capability('local/etraining:managecourses', $systemcontext)) {
            $row[] = new tabobject('courses',
                        new moodle_url('/local/etraining/courses.php'),
                        get_string('tabcourses', 'local_etraining')
                    );
        }

        if (has_capability('local/etraining:manageinstitutes', $systemcontext)) {
            $row[] = new tabobject('institutes',
                        new moodle_url('/local/etraining/institutes.php'),
                        get_string('tabinstitutes', 'local_etraining')
                    );
        }
        
        if (has_capability('local/etraining:settings', $systemcontext)) {
            $row[] = new tabobject('settings',
                        new moodle_url('/local/etraining/preferences.php'),
                        get_string('settings', 'local_etraining')
                    );
        }

        echo $this->tabtree($row, $current);
    }

    public function etraining_print_settings() {
        global $CFG;

        $systemcontext   = context_system::instance();

        $html = '';
        $html .= html_writer::start_tag('ul', array('class'=>'admin-menu-manage'));

            $value = get_config('local_etraining', 'create');
            $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled'), 'id'=>'etraining_create'));
                $html .= get_string('createsettings','local_etraining');
                $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                    $html .= html_writer::start_tag('label');
                        $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_etraining', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_etraining", "create");');
                        if ($value) $params['checked'] = 'checked';
                        $html .= html_writer::tag('input', '', $params);
                        $html .= html_writer::tag('span', '');
                    $html .= html_writer::end_tag('label');
                $html .= html_writer::end_tag('div');
            $html .= html_writer::end_tag('li');
        
            $value = get_config('local_etraining', 'confirm');
            $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled'), 'id'=>'etraining_confirm'));
                $html .= get_string('confirmsettings','local_etraining');
                $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                    $html .= html_writer::start_tag('label');
                        $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_etraining', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_etraining", "confirm");');
                        if ($value) $params['checked'] = 'checked';
                        $html .= html_writer::tag('input', '', $params);
                        $html .= html_writer::tag('span', '');
                    $html .= html_writer::end_tag('label');
                $html .= html_writer::end_tag('div');
            $html .= html_writer::end_tag('li');
        
            $value = get_config('local_etraining', 'createown');
            $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled'), 'id'=>'etraining_createown'));
                $html .= get_string('createownsettings','local_etraining');
                $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                    $html .= html_writer::start_tag('label');
                        $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_etraining', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_etraining", "createown");');
                        if ($value) $params['checked'] = 'checked';
                        $html .= html_writer::tag('input', '', $params);
                        $html .= html_writer::tag('span', '');
                    $html .= html_writer::end_tag('label');
                $html .= html_writer::end_tag('div');
            $html .= html_writer::end_tag('li');
        
            $value = get_config('local_etraining', 'confirmcourses');
            $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled'), 'id'=>'etraining_confirmcourses'));
                $html .= get_string('confirmcourses','local_etraining');
                $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                    $html .= html_writer::start_tag('label');
                        $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_etraining', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_etraining", "confirmcourses");');
                        if ($value) $params['checked'] = 'checked';
                        $html .= html_writer::tag('input', '', $params);
                        $html .= html_writer::tag('span', '');
                    $html .= html_writer::end_tag('label');
                $html .= html_writer::end_tag('div');
            $html .= html_writer::end_tag('li');
        
            $value = get_config('local_etraining', 'confirminstitutes');
            $html .= html_writer::start_tag('li', array('class'=>(($value) ? '' : 'disabled'), 'id'=>'etraining_confirminstitutes'));
                $html .= get_string('confirminstitutes','local_etraining');
                $html .= html_writer::start_tag('div', array('class'=>'onoff-switch'));
                    $html .= html_writer::start_tag('label');
                        $params = array('class'=>'cmn-toggle cmn-toggle-round', 'type'=>'checkbox', 'name'=>'local_etraining', 'value'=>1, 'onchange'=>'toggleFunctionality(this, "local_etraining", "confirminstitutes");');
                        if ($value) $params['checked'] = 'checked';
                        $html .= html_writer::tag('input', '', $params);
                        $html .= html_writer::tag('span', '');
                    $html .= html_writer::end_tag('label');
                $html .= html_writer::end_tag('div');
            $html .= html_writer::end_tag('li');

        $html .= html_writer::end_tag('ul');

        $html .= '<script>
            function toggleFunctionality(obj, plugin, config){
                var state = (jQuery(obj).prop("checked"))?1:0;

                jQuery.get("'.$CFG->wwwroot.'/local/etraining/preferences.php?action=set_config&state="+state+"&config="+config+"&plugin="+plugin);
                jQuery(obj).parent().parent().parent().toggleClass("disabled");
                
                if (config == "createown"){
                    if (state > 0){
                        jQuery("#etraining_confirmcourses").removeClass("disabled");
                        jQuery("#etraining_confirmcourses input").prop("checked", false);
                        jQuery("#etraining_confirminstitutes").removeClass("disabled");
                        jQuery("#etraining_confirminstitutes input").prop("checked", false);
                    } else {
                        jQuery("#etraining_confirmcourses").addClass("disabled");
                        jQuery("#etraining_confirmcourses input").prop("checked", true);
                        jQuery("#etraining_confirminstitutes").addClass("disabled");
                        jQuery("#etraining_confirminstitutes input").prop("checked", true);
                    }
                }
            }
        </script>';

        return $html;
    }
}
