<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * etraining version file.
 *
 * @package    local_etraining
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

function etraining_get_status_filter($status = 0, $rejected = false){
    global $DB, $USER;
    $output = '';
    
    $output .= '<select name = "status" onchange="jQuery(this).closest(\'form\').submit();">';
    $output .= html_writer::tag('option', get_string('showall', 'local_etraining'), array('value' => '-1'));
    
    $params = array();
    $params['value'] = 0;
    if ($status == 0) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('pending', 'local_etraining'), $params);
    
    $params = array();
    $params['value'] = 1;
    if ($status == 1) $params['selected'] = 'selected';
    $output .= html_writer::tag('option', get_string('confirmed', 'local_etraining'), $params);
    
    if ($rejected){
        $params = array();
        $params['value'] = 2;
        if ($status == 2) $params['selected'] = 'selected';
        $output .= html_writer::tag('option', get_string('rejected', 'local_etraining'), $params);
    }
                          
    $output .= '</select>';
        
    return $output;
}

function etraining_get_intituteslist($courseid = 0, $with_own = true){
    global $DB, $USER;
    $items = array();
    
    if ($courseid > 0){
        $where = " AND c.id = $courseid";
        if ($with_own) {
            $institutes = $DB->get_records_sql("SELECT DISTINCT(i.id), i.* FROM {local_et_institutes} i LEFT JOIN {local_et_courses} c ON c.instituteid = i.id WHERE (i.status > 0 OR (i.status = 0 AND i.userid = $USER->id)) $where ORDER BY i.name");
        } else {
            $institutes = $DB->get_records_sql("SELECT DISTINCT(i.id), i.* FROM {local_et_institutes} i LEFT JOIN {local_et_courses} c ON c.instituteid = i.id WHERE i.status > 0 $where ORDER BY i.name");
        }
        if (count($institutes)){
            foreach($institutes as $institute){
                $items[$institute->id] = $institute->name;
            }
        } 
    } else {
        if (has_capability('local/etraining:manage', context_system::instance())){
            $institutes = $DB->get_records_menu('local_et_institutes', array(), 'name', 'id, name');
            if (count($institutes)){
                $items = $institutes;
            }
        } else {
            if ($with_own){
                $institutes = $DB->get_records_sql("SELECT * FROM {local_et_institutes} WHERE status > 0 OR (status = 0 AND userid = $USER->id) ORDER BY name");
                if (count($institutes)){
                    foreach($institutes as $institute){
                        $items[$institute->id] = $institute->name;
                    }
                }    
            } else {
                $institutes = $DB->get_records_menu('local_et_institutes', array('status'=>1), 'name', 'id, name');
                if (count($institutes)){
                    $items = $institutes;
                }    
            }       
        }
    }
    
    return $items;
}

function etraining_get_courseslist($instituteid = 0, $with_own = true){
    global $DB, $USER;
    $items = array(); $where = '';
    
    if ($instituteid > 0){
        $where = " AND instituteid = $instituteid";
        if (has_capability('local/etraining:manage', context_system::instance())){
            $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE id > 0 $where ORDER BY coursename");
        } else {
            if ($with_own) {
                $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE (status > 0 OR (status = 0 AND userid = $USER->id)) $where ORDER BY coursename");
            } else {
                $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE status > 0 $where ORDER BY coursename");
            }
        }
        if (count($courses)){
            foreach($courses as $course){
                $items[$course->id] = $course->coursename;
            }
        }  
    } else {
        if (has_capability('local/etraining:manage', context_system::instance())){
            $courses = $DB->get_records_menu('local_et_courses', array(), 'coursesname', 'id, coursesname');
            if (count($courses)){
                $items = $courses;
            }    
        } else {
            if ($with_own){
                $courses = $DB->get_records_sql("SELECT * FROM {local_et_courses} WHERE status > 0 OR (status = 0 AND userid = $USER->id) ORDER BY coursename");
                if (count($courses)){
                    foreach($courses as $course){
                        $items[$course->id] = $course->coursename;
                    }
                }    
            } else {
                $courses = $DB->get_records_menu('local_et_courses', array('status'=>1), 'coursesname', 'id, coursesname');
                if (count($courses)){
                    $items = $courses;
                }    
            }   
        }
    }
    
    return $items;
}

function etraining_get_certificationslist(){
    global $DB, $USER;
    $items = array();
    
    $plans = $DB->get_records_menu('local_plans', array('type'=>1, 'visible'=>1), 'name', 'id, name');
    if (count($plans)){
        $items = $plans;
    }    
    
    return $items;
}

function etraining_get_userslist(){
    global $DB, $USER, $CFG;
    $items = array();
    
    $users = $DB->get_records_sql("SELECT * FROM {user} WHERE deleted = 0 AND suspended = 0 AND id > 2 AND id != $USER->id");
    if (count($users)){
        foreach ($users as $user){
            $items[$user->id] = fullname($user);
        }
    }
    
    return $items;
}


function local_etraining_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');

    $itemid = array_shift($args);
    $filename = array_pop($args);
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'local_etraining', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }
    send_stored_file($file, 86400, 0, $forcedownload, $options);
}


function etraining_import_institute($institutename = ''){
    global $USER, $DB;
    $record = 0;
    
    if (!empty($institutename)){
        $institute = $DB->get_record('local_et_institutes', array('name'=>$institutename));
        if (!$institute){
            $new_record = new stdClass();
            $new_record->name = $institutename;
            $new_record->userid = $USER->id;
            $new_record->status = 0;
            $new_record->timemodified = time();
            $record = $DB->insert_record('local_et_institutes', $new_record);
        } else {
            $record = $institute->id;
        } 
    }
    
    return $record;
}

function etraining_import_course($coursename = '', $instituteid = 0){
    global $USER, $DB;
    $record = 0;
    
    if (!empty($coursename)){
        $course = $DB->get_record('local_et_courses', array('coursename'=>$coursename));
        if (!$course){
            $new_record = new stdClass();
            $new_record->coursename = $coursename;
            $new_record->coursetype = 0;
            $new_record->instituteid = $instituteid;
            $new_record->userid = $USER->id;
            $new_record->status = 0;
            $new_record->timemodified = time();
            $record = $DB->insert_record('local_et_courses', $new_record);
        } else {
            $record = $course->id;
        }  
    }
    
    return $record;
}


function entraining_enable()
{
    global $OUTPUT, $PAGE;
    
    if(!get_config('local_etraining', 'enabled')){
        $PAGE->set_context(context_system::instance());
        $PAGE->set_heading(get_string('pluginname', 'local_etraining'));

        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('pluginname', 'local_etraining'));
        $url = new moodle_url($CFG->wwwroot.'/index.php');
        echo $OUTPUT->confirm(get_string('returntohome', 'local_etraining'), $url, $url);
        echo $OUTPUT->footer();
        die;
    }
}

