<?php

require_once('../../config.php');
require_once('lib.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);

if($action == 'load-courses' and $id > 0){	
    
    $courses = etraining_get_courseslist($id, true);
    $options = '<option value=""></option>';
    if (count($courses)){
        foreach($courses as $courseid=>$coursename){
            $options .= '<option value="'.$courseid.'">'.$coursename.'</option>';
        }
    }
    if (!get_config('local_etraining', 'createown') or has_capability('local/etraining:manage', context_system::instance())){
        $options .= '<option value="-1">'.get_string('other', 'local_etraining').'</option>';
    }
    
    echo $options;
    
}elseif($action == 'load-institute' and $id > 0){	
    
    $institutes = etraining_get_intituteslist($id, true);
    
    if (count($institutes)){
        foreach($institutes as $iid=>$name){
            $options .= '<option value="'.$iid.'">'.$name.'</option>';
        }
    }
    if (!get_config('local_etraining', 'createown') or has_capability('local/etraining:manage', context_system::instance())){
        $options .= '<option value="-1">'.get_string('other', 'local_etraining').'</option>';
    }
    
    echo $options;
} 

exit;

