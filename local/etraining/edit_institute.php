<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * plan related management functions, this file needs to be included manually.
 *
 * @package    core_plan
 * @copyright  2010 Petr Skoda  {@link http://skodak.org}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once('classes/forms.php');
require_once('lib.php');

$id        = optional_param('id', 0, PARAM_INT);
$delete    = optional_param('delete', 0, PARAM_BOOL);
$approve      = optional_param('approve', 0, PARAM_BOOL);
$disapprove      = optional_param('disapprove', 0, PARAM_BOOL);
$confirm   = optional_param('confirm', 0, PARAM_BOOL);

require_login();
$systemcontext   = context_system::instance();
require_capability('local/etraining:manageinstitutes', $systemcontext);

// check if plugin is enabled
entraining_enable();

if ($id > 0){
    $title = get_string('editinstitutes', 'local_etraining');
} else {
    $title = get_string('createinstitutes', 'local_etraining');
}

$PAGE->set_url(new moodle_url("/local/etraining/edit_institute.php", array()));
$PAGE->set_pagelayout('standard');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add(get_string('pluginname', 'local_etraining'), new moodle_url("/local/etraining/index.php"));
$PAGE->navbar->add(get_string('institutes', 'local_etraining'), new moodle_url("/local/etraining/institutes.php"));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_heading($title);

if ($id) {
    $institute = $DB->get_record('local_et_institutes', array('id'=>$id), '*', MUST_EXIST);
    $PAGE->url->param('id', $id);
} else {
    $institute = new stdClass();
    $institute->id = 0;
}

$returnurl = new moodle_url("/local/etraining/institutes.php");

if ($delete and $institute->id) {
    $PAGE->url->param('delete', 1);
    if ($confirm and confirm_sesskey()) {
        $DB->delete_records('local_et_institutes', array('id'=>$institute->id));
        redirect($returnurl);
    }
    
    $strheading = get_string('deleteinstinute', 'local_etraining');
    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/etraining/edit_institute.php', array('id' => $institute->id, 'delete' => 1,
        'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => $returnurl->out()));
    $message = get_string('deleteinstinutemsg', 'local_etraining', format_string($institute->name));
    echo $OUTPUT->confirm($message, $yesurl, $returnurl);
    echo $OUTPUT->footer();
    die;
    
} elseif ($approve && $institute->id && confirm_sesskey()) {
    
    if (!$institute->status) {
        $record = (object)array('id' => $institute->id, 'status' => 1);
        $DB->update_record('local_et_institutes', $record);
    }
    redirect($returnurl);
    
} elseif ($disapprove && $institute->id && confirm_sesskey()) {
    
    if ($institute->status) {
        $record = (object)array('id' => $institute->id, 'status' => 0);
        $DB->update_record('local_et_institutes', $record);
    }
    redirect($returnurl);
}

$editform = new institution_edit_form(null, array('data'=>$institute));

if ($editform->is_cancelled()) {
    redirect($returnurl);
} else if ($data = $editform->get_data()) {
    
    $data->timemodified = time();
    if ($data->id > 0){
        $DB->update_record('local_et_institutes', $data);
    } else {
        $data->userid = $USER->id;
        $data->id = $DB->insert_record('local_et_institutes', $data);
    }

    redirect($returnurl);
}

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_etraining');
$renderer->etraining_print_tabs('institutes');

echo $editform->display();
echo $OUTPUT->footer();

