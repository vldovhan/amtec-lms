jQuery(document).ready(function(){

    if (jQuery('.onoff-switcher').length){
        jQuery('.onoff-switcher').each(function(event){
            jQuery(this).parent().parent().addClass('onof-switcher-box');
            jQuery(this).wrapAll( "<label />");
            jQuery(this).after('<span></span>');
        });
    }
   
    jQuery('.tabs-list li').click(function(event){
        jQuery(".tabs-list li").removeClass('active');
        jQuery(this).addClass('active');

        jQuery('.mx-jtabs-item').removeClass('visible');
        var index = jQuery(".tabs-list li").index(this);
        jQuery('.mx-jtabs-item').eq(index).addClass('visible');
    });
    
});