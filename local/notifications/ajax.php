<?php

require_once('../../config.php');
require_once('lib.php');

require_login();

$id		  = optional_param('id', 0, PARAM_INT);
$type	  = optional_param('type', 0, PARAM_INT);
$action = optional_param('action', '', PARAM_RAW);
$form	= (object)clean_param_array($_POST['form'], PARAM_RAW, true);

if($action == 'save-support'){	
	if ($form->name != '' and $support_user = $DB->get_record('config', array('name'=>'supportname'))){
		$support_user->value = $form->name;
		$DB->update_record('config', $support_user);
	}
	if ($form->email != '' and $support_email = $DB->get_record('config', array('name'=>'supportemail'))){
		$support_email->value = $form->email;
		$DB->update_record('config', $support_email);
	}
	if ($form->noreply != '' and $noreply_email = $DB->get_record('config', array('name'=>'noreplyaddress'))){
		$noreply_email->value = $form->noreply;
		$DB->update_record('config', $noreply_email);
	}
	purge_all_caches();
	echo time();
} elseif($action == 'adminnot-set-status' and $id){
	$status		= optional_param('status', 0, PARAM_INT);
	if($notification = $DB->get_record("local_notifications", array('id'=>$id))){
		$notification->status = $status;
		$DB->update_record("local_notifications", $notification);
	}
	echo time();
} elseif ($action == 'adminnot-save-form'){
    $form->roles = serialize($form->roles);
	$DB->update_record("local_notifications", $form);
	$notification = $DB->get_record("local_notifications", array('id'=>$form->id));
	echo $notification->id;
	exit;
} elseif ($action == 'not-save-form'){
	
	$notification = $DB->get_record("notifications", array('id'=>$form->id));
	if ($notification){
		$available_courses = notifications_getmy_courses($notification->id);
		$selected_courses = array();
		foreach ($form->courses as $key=>$val){
			$selected_courses[$key] = $key;
		}
		foreach ($available_courses as $course){
			if (in_array($course->id, $selected_courses)){
				$nd = $DB->get_record('local_nots_data', array('not_id'=>$notification->id, 'data_type'=>'course', 'data_id'=>$course->id, 'userid'=>$USER->id));
				if (isset($nd->id) and $nd->state == 0){
					$nd->state = 1;
					$DB->update_record('local_nots_data', $nd);
				} elseif (!isset($nd->id)){
					$nd = new stdClass();
					$nd->not_id = $notification->id;
					$nd->userid = $USER->id;
					$nd->data_type = 'course';
					$nd->data_id = $course->id;
					$nd->state = 1;
					$DB->insert_record('local_nots_data', $nd);
				}
			} else {
				$nd = $DB->get_record('local_nots_data', array('not_id'=>$notification->id, 'data_type'=>'course', 'data_id'=>$course->id, 'userid'=>$USER->id));
				if (isset($nd->id) and $nd->state == 1){
					$nd->state = 0;
					$DB->update_record('local_nots_data', $nd);
				}
			}
		}
	}
	echo $notification->id;
	exit;
} elseif($action == 'not-set-status' and $id){
	$status		= optional_param('status', 0, PARAM_INT);
	if($notification = $DB->get_record("local_notifications", array('id'=>$id))){
		if ($notifications_userdata = $DB->get_record("local_nots_userdata", array('notid'=>$notification->id, 'userid'=>$USER->id))){
			$notifications_userdata->status = $status;
			$DB->update_record("local_nots_userdata", $notifications_userdata);
		} else {
			$notifications_userdata = new stdClass();
			$notifications_userdata->notid = $notification->id;
			$notifications_userdata->userid = $USER->id;
			$notifications_userdata->status = $status;
			$DB->insert_record("local_nots_userdata", $notifications_userdata);
		}
		if ($notification->id != 10 and $notification->id != 11 and $status == 1){
			$notifications_data = $DB->get_records_sql("SELECT * FROM {local_nots_data} WHERE not_id = $notification->id AND data_type = 'course' AND state = 1");
			if (!$notifications_data){
				$available_courses = notifications_getmy_courses($notification->id);
				foreach ($available_courses as $course){
					$nd = $DB->get_record('local_nots_data', array('not_id'=>$notification->id, 'data_type'=>'course', 'data_id'=>$course->id));
					if (isset($nd->id) and $nd->state == 0){
						$nd->state = 1;
						$DB->update_record('local_nots_data', $nd);
					} elseif (!isset($nd->id)){
						$nd = new stdClass();
						$nd->not_id = $notification->id;
						$nd->userid = $USER->id;
						$nd->data_type = 'course';
						$nd->data_id = $course->id;
						$nd->state = 1;
						$DB->insert_record('local_nots_data', $nd);
					}
				}
			}
		}
	}
	echo time();
} elseif($action == 'not-set-action' and $id){
	$status		= optional_param('status', 0, PARAM_INT);
	$atype		= optional_param('atype', '', PARAM_RAW);
	if($notification = $DB->get_record("local_notifications", array('id'=>$id))){
		if ($notifications_userdata = $DB->get_record("local_nots_userdata", array('notid'=>$notification->id, 'userid'=>$USER->id))){
			$notifications_userdata->$atype = $status;
			$DB->update_record("local_nots_userdata", $notifications_userdata);
		} else {
			$notifications_userdata = new stdClass();
			$notifications_userdata->notid = $notification->id;
			$notifications_userdata->userid = $USER->id;
			$notifications_userdata->status = 1;
			$notifications_userdata->$atype = $status;
			$DB->insert_record("local_nots_userdata", $notifications_userdata);
		}
		
		$DB->update_record("local_notifications", $notification);
	}
	echo time();
}
?>


<?php if ($action == 'adminnot-load-form' and $id) : ?>
	<?php $notification = $DB->get_record("local_notifications", array('id'=>$id)); ?>
	<?php $tag_description = notificatons_get_tags(); ?>
    <?php //echo serialize(array('recipient name', 'recipient email', 'support user name', 'support email', 'system name', 'course name', 'course name with link', 'module title', 'module title with link')); ?>
	<div class="not-form-inner">
		<form name="adminnot-form" class="adminnot-form clearfix" id="adminnot_form_<?php echo $id; ?>" method="POST" action="<?php echo $CFG->wwwroot;?>/local/notifications/ajax.php">
			<div class="not-form-items-f anot-form">
                <div class="not-form-item">
					<label>Subject:</label><input type="text" name="form[subject]" id="subject" value="<?php echo (isset($notification->subject)) ? $notification->subject : ''; ?>" />
				</div>
				<div class="not-form-item">
					<label>Body:</label><textarea name="form[body]" id="body"><?php echo (isset($notification->body)) ? $notification->body : ''; ?></textarea>
				</div>
                <?php if ($notification->roles_settings > 0) : ?>
                    <?php if (is_siteadmin()) {
                        $assignableroles = array();
                        $roles = $DB->get_records_sql('SELECT * FROM {role} WHERE status > 0 ORDER BY sortorder');
                        foreach ($roles as $role){
                            $assignableroles[$role->id] = $role->name;
                        }
                    } else {
                        $assignableroles = get_assignable_roles(context_system::instance(), ROLENAME_BOTH);    
                    }
                    ?>
                    <?php if (count($assignableroles)) : ?>
                        <div class="not-form-item">
                            <label>Select recipients roles:</label>
                            <select name="form[roles][]" id="copy" multiple>
                            <?php $notification->roles = unserialize($notification->roles); ?>
                                <?php foreach ($assignableroles as $roleid=>$rolename) : ?>
                                    <option value="<?php echo $roleid; ?>" <?php echo (in_array($roleid, $notification->roles)) ? 'selected' : '' ?>><?php echo $rolename; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>    
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($notification->type == 'reminder') : ?>
                    <div class="not-form-item clearfix">
                        <div class="not-form-item-part">
                            <label>Send before (days):</label>
                            <select name="form[before]" id="before">
                                <option value="0"></option>
                                <?php for ($i=1;$i<=14;$i++): ?>
                                    <option value="<?php echo $i * 3600 * 24; ?>" <?php echo ($notification->before == ($i * 3600 * 24)) ? 'selected' : '' ?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="not-form-item-part">
                            <label>Send after (days):</label>
                            <select name="form[after]" id="after">
                                <option value="0"></option>
                                <?php for ($i=1;$i<=14;$i++): ?>
                                    <option value="<?php echo $i * 3600 * 24; ?>" <?php echo ($notification->after == ($i * 3600 * 24)) ? 'selected' : '' ?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                    <div class="not-form-item clearfix">
                        <div class="not-form-item-part">
                            <label>Repeat in times:</label>
                            <select name="form[repeate_times]" id="repeate_times">
                                <option value="0"></option>
                                <?php for ($i=1;$i<=10;$i++): ?>
                                    <option value="<?php echo $i; ?>" <?php echo ($notification->repeate_times == $i) ? 'selected' : '' ?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="not-form-item-part">
                            <label>Repeate every (days):</label>
                            <select name="form[repeate_period]" id="repeate_period">
                                <option></option>
                                <?php for ($i=1;$i<=14;$i++): ?>
                                    <option value="<?php echo $i * 3600 * 24; ?>" <?php echo ($notification->repeate_period == ($i * 3600 * 24)) ? 'selected' : '' ?>><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                    </div>
                <?php endif; ?>
				<div class="not-form-item-btn">
					<input type="hidden" name="form[id]" value="<?php echo $id; ?>" />
					<input type="hidden" name="action" value="adminnot-save-form" />
					<button class="send btn btn-success" type="button">Save</button>
					<button class="cancel btn" type="button">Cancel</button>
				</div>
			</div>
			<div class="not-form-items-d">
				<div class="not-form-description">
					<span><strong>Subject</strong> - message subject</span>
					<span><strong>Body</strong> - message body text</span>
					<?php $tags = unserialize($notification->tags); ?>
					<?php if ($tags) : ?>
						<h3>Available Tags:</h3>
						<?php foreach ($tags as $tag) : ?>
							<span><strong>[<?php echo $tag; ?>]</strong> - <?php echo $tag_description[$tag]; ?></span>
						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>
		</form>
	</div>
	<script>
		jQuery("#adminnot_form_<?php echo $id; ?> #body").ClassyEdit();
		jQuery('#adminnot_form_<?php echo $id; ?> .not-form-item-btn .send').click(function(event){
			jQuery.ajax({
				url: jQuery('#adminnot_form_<?php echo $id; ?>').attr('action'),
				type: "POST",
				data: jQuery('#adminnot_form_<?php echo $id; ?>').serialize(),
				beforeSend: function(){
					jQuery('#not_item_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function( data ) {
				jQuery('#not_item_<?php echo $id; ?> .not-form-box').addClass('center');
				jQuery('#not_item_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-check green"> <span>Notification successfully saved.</span></i>');
				setTimeout(function() {
					jQuery('#not_item_<?php echo $id; ?> .not-form-box').removeClass('center');
					jQuery("#not_item_<?php echo $id; ?> .load").trigger("click");
				}, 1500);
			});
		});
		jQuery('#adminnot_form_<?php echo $id; ?> .not-form-item-btn .cancel').click(function(event){
			jQuery("#not_item_<?php echo $id; ?> .load").trigger("click");
		});
	</script>
<?php endif; ?>

<?php if ($action == 'not-load-formdata' and $id) : ?>
	<?php $notification = $DB->get_record("local_notifications", array('id'=>$id)); ?>
	<?php 
		$available_courses = notifications_getmy_courses($notification->id);
		$selected_courses = notifications_get_courses($notification->id);
	?>
	<div class="not-form-inner">
		<form name="not-form" class="not-form mform clearfix" id="not_form_<?php echo $id; ?>" method="POST" action="<?php echo $CFG->wwwroot;?>/local/notifications/ajax.php">
			<div class="not-form-items-f">
				<input type="hidden" name="form[sender]" value="supportuser" />
				<div class="not-form-item">
                    <p><br /><label>Select Courses:</label></p>
					<span class="courses-checkboxes fcheckbox clearfix">
						<?php if (count($available_courses) > 0) : ?>  
                        <span class="checkbox-box"><label class="label-box"><input type="checkbox" class="no-courses" name="form[courses][-1]" value="1" /><label class="checkbox"></label> No courses</label></span>
							<span class="checkbox-box"><label class="label-box"><input type="checkbox" class="all-courses" name="form[courses][-2]" value="1" /><label class="checkbox"></label> All courses</label></span>
						<?php endif; ?>
						<div class="clear"></div>
						<div class="divider"></div>
						<div class="clear"></div>
						<?php foreach ($available_courses as $course) : ?>
							<span class="checkbox-box"><label class="label-box"><input type="checkbox" name="form[courses][<?php echo $course->id; ?>]" value="1" <?php echo (isset($selected_courses[$course->id])) ? 'checked' : '' ?> /><label class="checkbox"></label> <?php echo $course->fullname; ?></label></span>
						<?php endforeach; ?>
					</span>
				</div>
				<div class="not-form-item-btns">
					<input type="hidden" name="form[id]" value="<?php echo $id; ?>" />
					<input type="hidden" name="action" value="not-save-form" />
					<button class="send btn-success" type="button">Save</button>
					<button class="cancel" type="button">Cancel</button>
				</div>
			</div>
		</form>
	</div>
	<script>
		jQuery('#not_form_<?php echo $id; ?> .not-form-item-btns .send').click(function(event){
			jQuery.ajax({
				url: jQuery('#not_form_<?php echo $id; ?>').attr('action'),
				type: "POST",
				data: jQuery('#not_form_<?php echo $id; ?>').serialize(),
				beforeSend: function(){
					jQuery('#not_item_formbox_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function( data ) {
				jQuery('#not_item_formbox_<?php echo $id; ?> .not-form-box').addClass('center');
				jQuery('#not_item_formbox_<?php echo $id; ?> .not-form-box').html('<i class="fa fa-check green"> <span>Notification successfully saved.</span></i>');
				setTimeout(function() {
					jQuery('#not_item_formbox_<?php echo $id; ?> .not-form-box').removeClass('center');
					jQuery("#not_item_<?php echo $id; ?> .course-selector").trigger("click");
				}, 1500);
			});
		});
		jQuery('#not_form_<?php echo $id; ?> .not-form-item-btns .cancel').click(function(event){
			jQuery("#not_item_<?php echo $id; ?> .course-selector").trigger("click");
		});
		jQuery('#not_form_<?php echo $id; ?> .not-form-item .no-courses').change(function(event){
			  if (jQuery(this).prop('checked')){
				jQuery('#not_form_<?php echo $id; ?> .not-form-item input:checkbox').removeAttr('checked');
				jQuery(this).prop('checked', true);
			  }
		});
		jQuery('#not_form_<?php echo $id; ?> .not-form-item .all-courses').click(function(event){
			jQuery('#not_form_<?php echo $id; ?> .not-form-item input:checkbox').prop('checked', true);
			jQuery('#not_form_<?php echo $id; ?> .not-form-item .no-courses').removeAttr("checked");
		});
        jQuery('#not_form_<?php echo $id; ?> .not-form-item label.checkbox').click(function(event){
			jQuery(this).parent().find('input:checkbox').trigger('click')
		});
	</script>
<?php endif; ?>

<?php exit;

